#!/bin/sh
# This is assumed to be running from the root of the repo
echo "[before] running in: $PWD"
# For debugging
echo "CI_COMMIT_BRANCH="$CI_COMMIT_BRANCH
echo "CI_DEFAULT_BRANCH="$CI_DEFAULT_BRANCH
python --version
git branch

# Needed for Sphinx (make in build-base) and pysam (build-base + others)
apk add build-base openblas-dev

# Upgrade pip first
pip install --upgrade pip

# Sphinx doc building requirements
pip install -r resources/ci_cd/sphinx.txt

# Package requirements
pip install -r requirements.txt

# Install the package being built/tested
pip install .

# Run tests
pytest tests/
