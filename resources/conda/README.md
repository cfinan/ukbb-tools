# The conda resource
This has two directories in it.

* `build` - This has sub-directories for recent Python versions, each one will have a build recipe within it.
* `envs` - This has sub-directories for recent Python versions, within each one are conda environment yaml files. There is an update file that can be used with `conda env update --file conda_update.yml` to update an existing environment, or there is a yml file that can be used to create a new environment with `conda env create --file conda_create.yml`.
