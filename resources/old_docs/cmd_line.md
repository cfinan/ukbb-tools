# Command line tools
I many instances, users may not want/need to access the UK BioBank data via the python API. In addition all the build scripts are not designed to be run from an API (although it is technically possible). In these use cases command line scripts are available to perform various tasks.

* [`ukbb_pheno_norm`](./docs/pheno_normaliser.md) - For normalising the clinical codes and event times in the Uk Biobank
* [`ukbb_wide_file_subset`](./docs/wide_file_handler.md) - For subsetting the wide UK BioBank fields file
* [`ukbb_build_mapper_db`](./docs/ukbb_mapper.md) - For building a mapping database to aid in the normalisation of clinical codes
* [`ukbb_data_dict`](./docs/data_dict.md) - For building the UK Biobank data dictionary into a proper data using files downloaded from the UK Biobank website
* [`ukbb_wide_file_index`](./docs/wide_file_index.md) - For indexing, querying and compressing the UK Biobank wide files 
* [`ukbb_wide_file_merge`](./docs/wide_file_merge.md) - For merging UKBB wide files, for example merge an old dataset with a basket refresh
