==============================================
Building the UK BioBank Tools mapping database
==============================================

One of the critical tasks when handling the UK BioBank HES and GP data is to be able to standardise the clinical coding system in someway to make it easier to pull out records matching any clinical term.

ukbb-tools provides build scripts to construct a database from the UKBB mapping file (resources 593, XXX and XXX - URL) and optionally the NHS Trud Data Migration Workbench (URL).

The mapping database can exist as a standalone entity or, if you are building a full UK BioBank database then it will be incorporated into it, either way it needs to be built as a standalone entity first.

Creating the UK BioBank code pool
---------------------------------

The build script has an optional argument of the UK BioBank code pool. This is a file that contains all of the unique clinical codes observed in the UK BioBank cohort. This is used to determine coverage of the codes by the UK BioBank mapping files and the NHS Trud files (if used).

This can be useful for finding coding errors both the health record data and/or the UK BioBank mapping files (version 1 of these had multiple errors). Clinical codes in the health record data that are not present in the various mapping files can then be added to the mapping database as orphan codes, so even if they are not officially represented you will have an idea of where potential errors reside. 

This also gives the option to create manual mappings from unrepresented codes and supply them to the mapping build script if needed.

There is a helper bash script that will create a code pool for you. This assumes that you have your configuration file setup properly and have read access to the HES diagnosis and the GP diagnosis. The ``--help`` is shown below:


.. code-block::

   $ create-code-pool.sh --help
   USAGE: /home/rmjdcfi/code/ukbb_tools/resources/bin/create-code-pool.sh [flags] <config_file> <outfile>
   flags:
     -v,--[no]verbose:  give more output (default: false)
     -T,--tmp-dir:  An alternative location for tmp files (default: '')
     -h,--help:  show this help (default: false)

An example usage where the config file is a hidden file in the home directory ``~/.ukbb.cnf`` and outputting to ``ukbb_code_pool.txt`` and using the default system tmp file location.

.. code-block::

   create-code-pool.sh -v ~/.ukbb.cnf ukbb_code_pool.txt

On our systems and datasets, the code pool contains 143294 rows (including the header). The first few lines of the output should look like:

.. code-block::

   $ head ukbb_code_pool.txt 
   cui	coding_system
   .....	read2
   0....	read2
   01...	read2
   0111.	read2
   0113.	read2
   012..	read2
   0121.	read2
   012Z.	read2
   013..	read2
