# Overview of `pandas_readers`
This module contains a set of `read_*` functions that can be used to read data from all the indexed files in the dataset. This sits on top of the general UKBB input and output parsing framework implemented `ukbb_tools`.

These functions are  useful for providing subsets of the data directly into a `pandas.DataFrame`. Whilst this is not a powerful as using a full database it is a lot easier then sub-setting the files from end-to-end parsing.

Each `pandas.DataFrame` that is returned will have the fields formatted to the correct data type. In some circumstances, missing data can force pandas to turn `int` into `float` to handle the `NaN` values. Where files have internally delimited fields the values columns will be nested lists.

All the `read_*` functions can use regular expressions with all these functions, although care should be taken with this as a pooly constructed regular expression will have your machine swapping in no time! For this reason a `dry_run` option is available where the you can get a `pandas.DataFrame` of all the matches in the index and how many rows are will be returned for each, this can be used to adjust your regular expressions accordingly.

This indexing is very similar to the approach used by [tabix](http://www.htslib.org/doc/tabix.html) and [bgenix](https://enkre.net/cgi-bin/code/bgen/dir?ci=trunk) to extract genotypes from VCF and BGEN files respectively. The indexed data files are block compressed with [bgzip](http://www.htslib.org/doc/bgzip.html) (using the [biopython](https://biopython.org/DIST/docs/api/Bio.bgzf-module.html) implementation) and the indexes are stored in [SQLite](https://www.sqlite.org/index.html) databases.

One thing to keep in mind is that it can only find values in the indexed column, so the file must be indexed on the column that you want to search the most. In some cases this will be the sample ID column and in other cases it might be the clinical code column. The readers detailed here to not care about the column that has, they will just issue warnings for all the data values they can't recover, so if searches do not retrieve any values you may be searching for values in columns that are not indexed.

If you are using files that are created during an end-to-end database build, the indexed column will always be placed just before the file extension, for example, it could be `eid_indexed.txt.gz` or `clinical_code_indexed.txt.gz`.

For an example notebook that extracts data from the `clinicial_data` file see [this example](../resources/examples/read_pandas.ipynb).

If you are generating your own indexed files, you should make sure you understand the [cardinality](https://en.wikipedia.org/wiki/Cardinality) of the index, so how many rows are associated with index value, otherwise you could make an index that is a similar size as the input file. For more information in creating index files see [here]().
