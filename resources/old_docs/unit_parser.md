# Overview of `unit_parser`
This module is responsible for the processing of clinical measures data. I aims to:

1. Distinguish between clinical measures and diagnosis in GP clinical data
2. Standardise the measures of the clinical units

The first stage actually occurs (using this module) during the event processing in `ukbb_pheno_norm` but the second stage uses a command line program within this module that will attempt to standardise the units and flag units that are problematic.

Unit processing is optional and it is possible to skip unit processing and proceed to build into a database, for example, if you think it can be done better :-) . However, if you do run it then you will need to index your file as below and update the path with the newly indexed file path, assuming you changed the file name and are using a config file rather than passing command line arguments to the script.

# Re-indexing the file for unit processing
In order to run unit processing we have to re-index the clinical data file based on clinical code. This will make it easier to process each code separately and evaluate units in a code-by-code manor. The re-indexed clinical code file can be extremely useful though as it means that you can rapidly extract codes for the whole cohort and easily drop them into a `pandas.DataFrame` for processing. Obviously, if you eventually build into the database this may not be so useful.

Below is an example using sort with the `body` function (to skip the header in the sort) defined in your `~/.bashrc` (do not forget to export the function also), the function can be seen [here](../resources/bin/index_infile.sh).
```
zcat clinical_data_20200811_0.1.0a1.recode.txt.gz |
    body sort -t$'\t' -k10,10 -k1 |
    make_file_index -e'latin1' -v -s1 -k13 -d$'\t' -c10 - \
    clinical_data_20200811_0.1.0a1.clin_code_idx.txt.gz
```

In the command above the delimiter is a tab we are skipping the header line in the index, the index column is column 10 (1-based) the index length is set to 13 characters, this could vary between builds so to determine your index length you can use `awk` as in the command below.
```
zcat clinical_data_20200811_0.1.0a1.txt.gz |
    awk 'BEGIN{FS="\t";OFS="\t";max_idx=0}{x=length($10); if (x>max_idx){max_idx=x}}END{printf("The max keylength is %s\n",max_idx)}'
```

There will eventually be a dedicated python script for building the index for unit processing it will not do the sorting though, although this is technically achievable wth a subprocess call in general I do not like subprocess calls unless it is 100% necessary.

# Running unit processing
