# Overview of `db_build`
This is the main database building module containing the `db_build.DbBuilder` class that can be used to construct the database.

The `db_build.DbBuilder` class makes one assumption that the database exists and the tables have been created, this may change in future
