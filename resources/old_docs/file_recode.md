# Overview of `file_recode`
We have noticed that some of the UKBB data files have different encoding, for example, the GP data file appear to be `latin1` encoded. This can cause unexpected `UnicodeDecodeError` errors during the build. For this reason, it is recommended that the file encoding is standardised to `UTF-8` prior to the build process. A script is available to do this. `ukbb_recode`

The re-coding process is reasonably fast and works by reading the files in as `bytes` and then encoding these as `utf-8`. If there are any errors, the offending characters are attempted to be replaced with `utf-8` equivalents. If this is not possible then they are replaced with `?` and a warning message is generated giving the line number of the input file.

