# Overview of `event_parser`
A module that contains code for handling events in the UKBB clinical data. Events are basically any interaction with the health service. This module contains a parsing class that will yield event data for each sample that has been standardised in the following ways:

1. The date for each event will be represented as an integer, formatted date string, number of days relative to baseline and as the age of the participant when the event happened.
2. The span of each event will be given. This is the number of days the event happened relative to the complete event episode. This is used at present to avoid a having second event table but might change in future.
3. Each event is tied to a PCT. If no PCT is directly associated to an event, then one is assigned to it based on the nearest data point with a PCT value

## What is a PCT value?
The PCT is a code for the primary care trust, this is the hospital region that the patient's event occurred in, or the region that their currently registered GP is in. The event parser will attempt to capture the PCT of the event `pctcode` field and if that is not defined then it will capture the PCT of the currently registered GP in the hope that the patent has kept their GP registration current as they have moved about the country. If, the event parser does fall back to the GP PCT value then `pct_is_gp` will be set to `True` to indicate a potential source of error.

## Why are we interested in the PCT?
The PCT is the nearest thing that we have to a location for the sample. Moreover, the PCT may be useful in sorting out GP clinical measurement unit issues, as the measures may well cluster with PCT values, i.e. different trusts use different measurment systems.
