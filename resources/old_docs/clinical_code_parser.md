# Overview of `clinical_code_parser`
This module is responsible for parsing of all the clinical codes from all the files into the UMLS. It uses the [ukbb_mapper](./ukbb_mapper.md) and the [umls_mapper](./umls_mapper.md) to accomplish this.
