==========================
Parsing UKBB mapping files
==========================

UK BioBank provide some files that can also be used to facilitate the mapping between clinical concept codes. One of these is an Excel file that needs to be converted into a tab delimited format to make it tractable. As with many tools in ukbb-tools, there is a python script to perform the conversion of the Excel file.

Downloading the Excel Mapping file
----------------------------------

The UK BioBank mapping file is located in `resource 592 <https://biobank.ctsu.ox.ac.uk/crystal/refer.cgi?id=592>`_. It can be downloaded from the link above of via the terminal:

.. code-block::

   mkdir ukbb_mapping_files
   cd ukbb_mapping_files/

   wget  -nd  biobank.ctsu.ox.ac.uk/crystal/crystal/auxdata/primarycare_codings.zip
   --2021-02-26 15:31:16--  http://biobank.ctsu.ox.ac.uk/crystal/crystal/auxdata/primarycare_codings.zip
   Resolving biobank.ctsu.ox.ac.uk (biobank.ctsu.ox.ac.uk)... 163.1.206.99
   Connecting to biobank.ctsu.ox.ac.uk (biobank.ctsu.ox.ac.uk)|163.1.206.99|:80... connected.
   HTTP request sent, awaiting response... 302 Found
   Location: https://biobank.ctsu.ox.ac.uk/crystal/crystal/auxdata/primarycare_codings.zip [following]
   --2021-02-26 15:31:17--  https://biobank.ctsu.ox.ac.uk/crystal/crystal/auxdata/primarycare_codings.zip
   Connecting to biobank.ctsu.ox.ac.uk (biobank.ctsu.ox.ac.uk)|163.1.206.99|:443... connected.
   HTTP request sent, awaiting response... 200 OK
   Length: 69534992 (66M) [application/zip]
   Saving to: ‘primarycare_codings.zip’

   primarycare_codings.zip                         100%[====================================================================================================>]  66.31M  4.90MB/s    in 14s     

   2021-02-26 15:31:30 (4.89 MB/s) - ‘primarycare_codings.zip’ saved [69534992/69534992]

   $ unzip primarycare_codings.zip 
   Archive:  primarycare_codings.zip
     inflating: all_lkps_maps_v2.xlsx   
     inflating: all_lkps_maps_variable_names_definitions_v2.pdf  

The file ``all_lkps_maps_v2.xlsx`` contains all the UK BioBank mappings that will be converted into tab delimited text files.

Converting the Excel spreadsheet to tab delimited text
------------------------------------------------------

The Excel mapping file can be processed with the command:

.. code-block::

   $ parse-ukbb-mappings -v ukbb_mapping_files/all_lkps_maps_v2.xlsx ukbb_mapping_files

   = Parse UKBB Mappings (ukbb_tools v0.1.0a1) =
   [info] outdir value: /data/ukbb/mapping_files/source_files/ukbb_mapping_files
   [info] ukbb_mapping_file value: all_lkps_maps_v2.xlsx
   [info] verbose value: True
   [info] converting excel file to text, this may be slow...
   [start] 2021-02-26 16:29:24
   [finish] 2021-02-26 16:29:26 [runtime] 0:00:02                     
   [start] 2021-02-26 16:29:27
   [finish] 2021-02-26 16:29:35 [runtime] 0:00:08                      
   [start] 2021-02-26 16:29:35
   [finish] 2021-02-26 16:29:35 [runtime] 0:00:00              
   [start] 2021-02-26 16:29:35
   [finish] 2021-02-26 16:29:36 [runtime] 0:00:00               
   [start] 2021-02-26 16:29:36
   [finish] 2021-02-26 16:29:36 [runtime] 0:00:00                
   [start] 2021-02-26 16:29:36
   [finish] 2021-02-26 16:29:38 [runtime] 0:00:01                         
   [start] 2021-02-26 16:29:38
   [finish] 2021-02-26 16:29:39 [runtime] 0:00:01                               
   [start] 2021-02-26 16:29:40
   [finish] 2021-02-26 16:29:41 [runtime] 0:00:01                               
   [start] 2021-02-26 16:29:41
   [finish] 2021-02-26 16:29:41 [runtime] 0:00:00                  
   [start] 2021-02-26 16:29:41
   [finish] 2021-02-26 16:29:42 [runtime] 0:00:00                   
   [start] 2021-02-26 16:29:42
   [finish] 2021-02-26 16:29:42 [runtime] 0:00:00                   
   [start] 2021-02-26 16:29:43
   [finish] 2021-02-26 16:29:46 [runtime] 0:00:03                               
   [start] 2021-02-26 16:29:47
   [finish] 2021-02-26 16:29:55 [runtime] 0:00:08                            
   [start] 2021-02-26 16:29:55
   [finish] 2021-02-26 16:29:57 [runtime] 0:00:01                            
   [start] 2021-02-26 16:29:57
   [finish] 2021-02-26 16:30:00 [runtime] 0:00:02                             
   [start] 2021-02-26 16:30:00
   [finish] 2021-02-26 16:30:01 [runtime] 0:00:01                             
   [start] 2021-02-26 16:30:03
   [finish] 2021-02-26 16:30:28 [runtime] 0:00:25                                
   *** END ***

This may take ~ 10 mins to run and will produce the following tab delimited and gzip compressed files:

.. code-block::

   bnf_lkp.txt.gz
   dmd_lkp.txt.gz
   icd10_lkp.txt.gz
   icd9_icd10.txt.gz
   icd9_lkp.txt.gz
   read_ctv3_icd10.txt.gz
   read_ctv3_icd9.txt.gz
   read_ctv3_lkp.txt.gz
   read_ctv3_opcs4.txt.gz
   read_ctv3_read_v2.txt.gz
   read_v2_drugs_bnf.txt.gz
   read_v2_drugs_lkp.txt.gz
   read_v2_icd10.txt.gz
   read_v2_icd9.txt.gz
   read_v2_lkp.txt.gz
   read_v2_opcs4.txt.gz
   read_v2_read_ctv3.txt.gz


Downloading the OPCS lookup tables
----------------------------------

In addition to the UK BioBank mapping files. UK BioBank also provides lookup files for both OPCS3 and OPCS4 clinical codes. These are separate from the Excel file and have to be downloaded from the website.

The older ``OPCS3`` codes can be downloaded `from here <http://biobank.ctsu.ox.ac.uk/showcase/coding.cgi?id=259>`_. The newer ``OPCS4`` codes can be downloaded `here <https://biobank.ndph.ox.ac.uk/showcase/coding.cgi?id=240&nl=1>`_.

Assuming they are downloaded into your downloads folder, you can now move them into the same directory as the parsed Excel files:

.. code-block::

   # OPCS3
   mv ~/Downloads/coding259.tsv ukbb_mapping_files/
   # OPCS4
   mv ~/Downloads/coding240.tsv ukbb_mapping_files/

