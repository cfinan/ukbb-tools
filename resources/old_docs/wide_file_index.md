# Overview of `wide_file_index`
This module contains tools for creating random access to sample rows in a UKBB wide phenotype file. This may be useful for efficient sample subset from a wide file, which, due to the large number of rows and columns may take several minutes to parse from top to bottom. An indexed UKBB wide file allows random access into the file to pull out specific sample rows. It also has the added benefit of compressing the wide files (which are not normally compressed). For this it uses [`Biopython`](https://biopython.org/) [`BgzfReader`](https://biopython.org/DIST/docs/api/Bio.bgzf.BgzfReader-class.html) and [`BgzfWriter`](https://biopython.org/DIST/docs/api/Bio.bgzf.BgzfWriter-class.html).

This details several aspects of handling indexed UKBB wide phenotype files

1. Creating an indexed UKBB wide file on the command line
2. Creating an indexed UKBB wide file, programatically
3. Extracting samples from an indexed UKBB wide file

## 1. Creating an indexed UKBB wide file on the command line
An index of a wide UKBB phenotype file can be created on the command line. Indeed this probably the main interface that will be used. Command line indexing is accomplished by using `ukbb_wide_file_index`, it's help is shown below. When used via the command line, the delimiter of the wide file is determined automatically with pythons [`csv.Sniffer`](https://docs.python.org/3/library/csv.html#csv.Sniffer) and has been detected in all the tests that have been carried out. If detection does not work correctly then consider using the programmatic interface detailed below.

Please note, that if `--outfile` is not specified then this may overwrite your input file. This is depending on the input file name (if it already has a `.gz` extension). Overwrites however, are done in a safe way by writing to a temp file first and then moving the file to the final location only after all writes have been finished. If you do not want file overwriting to happen, please specify an `--outfile` on the command line. Please note that all output indexed files are tab delimited `\t`. The actual index file, is a config file that is compressed with `gzip`.

```sh
$ ukbb_wide_file_index --help
usage: ukbb_wide_file_index [-h] [--index-file INDEX_FILE] [--outfile OUTFILE]
                            [-v]
                            wide_file_path

build an indexed UKBB wide file for random sample access

positional arguments:
  wide_file_path        The path to UKBB wide file

optional arguments:
  -h, --help            show this help message and exit
  --index-file INDEX_FILE
                        An alternative path to the index file name. By default
                        the index file will be the same name as the wide file
                        but with an additional .idx extension
  --outfile OUTFILE     An alternative path to the output indexed file. By
                        default, the indexed file will be the same as the
                        input file name with a .gz extension (it will be
                        bgzipped) and the input file would be removed.
                        However, if this is provided then the input file will
                        be left in place
  -v, --verbose         Give more output
```

An example usage is shown below. This will generate an output file `/path/to/latest_phenotypes.txt.gz` and an index file `/path/to/latest_phenotypes.txt.gz.idx`:
```sh
ukbb_wide_file_index -v /path/to/latest_phenotypes.txt
```

## 2. Creating an indexed UKBB wide file programatically
A simple programmatic interface also exists. The code below gives an example usage and can be used to build an index file whilst providing more options such as file delimiters. See the API documentation for the full parameter list for `build_index_file`.

```python
from ukbb_tools import wide_file_index as wfi

infile = "/path/to/ukbb/wide_file.csv"
outfile = "/path/to/ukbb/wide_file.txt.gz"
index_file = "/path/to/ukbb/wide_file.txt.gz.idx"

wfi.build_index_file(
	infile,
        outfile,
        index_file,
        verbose=True,
	delimiter=','
)
```

## 3. Extracting samples from an indexed UKBB wide file
There are three main ways to extract specific samples from an indexed wide file.

1. Creating an `wide_file_index.IndexReader` object and querying with it
2. Using a `ContextManager` `open` interface
3. Using a specific parser in `wide_file_handler` (see dedicated documentation for `wide_file_handler`). Whilst the first two options are documented below, they are of limited use unless you want to manually handle the type conversion and extraction of the data. It is anticipated that this will be the main mechanism of interfacing with an indexed (and un-indexed) wide phenotype file.

For a full list of options please see the API documentation

### Using `wide_file_index.IndexReader`
The basic level of interaction with an indexed wide file is via the `wide_file_index.IndexReader` class. This has a very simple interface and will `yield` unprocessed raw rows as lists of strings. Some example usage is shown below:

```python
from ukbb_tools import wide_file_index as wfi

wide_file = "/path/to/ukbb/wide_file.txt.gz"
index_file = "/path/to/ukbb/wide_file.txt.gz.idx"

idx_obj = wfi.IndexReader(wide_file, index_file=index_file)
idx_obj.open()

# To fetch a single row, this will throw an IndexError
# if the sample ID is not present in the index
row = idx_obj.fetch_row(1234567)

# Fetch can yield multiple rows and does not error out if
# any are not present in the index, rather, a warning is generated
# "The following eids were not found: '1'"
eids = [1, 1234567, 23453322, 47828772]
for row in idx_obj.fetch(eids):
    pp.pprint(row)

idx_obj.close()
```

### Using `wide_file_index.open`
Whilst `IndexReader` provides a simple access to the index. The user has to open and close the index themselves. To provide an even easier interface, there is also an `open` context manager that can be used.

```python
from ukbb_tools import wide_file_index as wfi

wide_file = "/path/to/ukbb/wide_file.txt.gz"
index_file = "/path/to/ukbb/wide_file.txt.gz.idx"

with wfi.open(wide_file, index_file=index_file) as idx_obj:
    eids = [1, 1234567, 23453322, 47828772]
    for row in idx_obj.fetch(eids):
        pp.pprint(row)
```

