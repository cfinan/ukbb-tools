# Overview of `wide_file_handler`
The UK BioBank wide file is a file containing UK BioBank fields in each column and each row there is a single sample. This creates a really wide matrix like file. Each column name has a name structure of `<FIELD_ID>-<INSTANCE_NO>.<ARRAY_NO>`. The data in each column can be different data types, some maybe strings, floats, integers and As this file is so hideous I have placed the parser in a separate module. There are several methods of interacting with the wide files. All of the methods will automatically use the UKBB wide file index.

1. Via the command line
2. Via a dedicated parser
3. Via a database (this is not covered in this readme)

## 1. Interacting with the wide file via the command line
Subsets can be taken from the UKBB wide file by executing `ukbb_wide_file_subset` on the command line. This uses classes within `wide_file_handler` to extract rows and columns from a UKBB wide phenotype file.

`ukbb_wide_file_subset` must be given either EIDs (sample IDs) and/or field or column names. It can't be run with no EID or column selection as it would be pointless using it. The `--help` options are shown below

```
$ ukbb_wide_file_subset --help
usage: ukbb_wide_file_subset [-h] [--fields FIELDS [FIELDS ...]]
                             [--field-file FIELD_FILE] [-v] [-l]
                             wide_file_path [eids [eids ...]]

Subset samples and columns from a UKBB wide file

positional arguments:
  wide_file_path        The path to UKBB wide file
  eids                  0 or more sample IDs to be extracted. If none are
                        supplied then it is assumed that all samples are
                        required. A file of sample IDs can also be supplied

optional arguments:
  -h, --help            show this help message and exit
  --fields FIELDS [FIELDS ...]
                        One or more fields or column IDs to extract from the
                        wide file. Suppying none will extract all of them. A
                        field ID is an integer a column name is the field ID,
                        instance ID and array number combined in a string with
                        the structure: <FID>-<INST>.<ARRAY>. Both can be
                        supplied at the same time. If a field ID is supplied,
                        all columns matching that ID will be extracted. If any
                        columns are not present, then they are ignored and a
                        warning is issued
  --field-file FIELD_FILE
                        A file with field IDs or column names in it, there
                        shoudl be one per line. This can be used inconjunction
                        with --fields
  -v, --verbose         Give more output
  -l, --long            Return the results in long format and not wide format.
                        Long format results are also supplied with dates/times
                        relative to baseline
```

Some example usage. This queries for fields and column names in the UKBB wide phenotype file for two samples. This demonstrates the syntax and the usage of whole field IDs which will get all column data for a specific field, or a specific column name can be given (in this case `47-0.0`). Two sample IDs are requested, `1000188` and `1001130`.
```
$ ukbb_wide_file_subset -v --fields 34 "47-0.0" 48 -- /data/ukbb/phenotypes/merged.txt.gz 1000188 1001130 > demo.txt
= UKBB wide file subset (ukbb_tools v0.1.0a1) =
[info] eids length: 2
[info] field_file value: None
[info] fields length: 3
[info] long value: False
[info] verbose value: True
[info] wide_file_path value: /data/ukbb/phenotypes/merged.txt.gz
[info] *** END ***
```

All verbose output `-v` is written to STDERR leaving the data to be written to STDOUT and redirected to the file `demo.txt`. The contents and format of `demo.txt` is shown below. Please note that the values are made up.

 eid     | 34-0.0 | 47-0.0 | 48-0.0 | 48-1.0 | 48-2.0 | 48-3.0 
:--------|:-------|:-------|:-------|:-------|:-------|-------:
 1000188 | 1901   | 61     | 100    | 100    | 95     |        
 1001130 | 1895   | 32     | 90     |        |        |        

There are a couple of things to note. Firstly the number of columns that have been returned. We requested 3 items, two of which were field IDs `34` and `48` and one was a column name `47-0.0`. We can see that we had a total of four columns starting with field ID `48` returned and one starting with field ID `34`. When a field ID is requested then all columns containing that field ID will be returned. On the other hand, field ID `47` also has four possible columns but we are only displaying one of them as we specifically requested column `47-0.0`, the first measure for the first instance. We also see the sparsity of the data in the wide file. There are many data fields that contain no data (`''`). When output in a wide matrix like format, it is difficult to exclude them. However, we can alter the format of the data to exclude missing data values. For this we need to use the `--long` option.

```
$ ukbb_wide_file_subset -v --long --fields 34 "47-0.0" 48 -- /data/ukbb/phenotypes/merged.txt.gz 1000188 1001130 > demo.txt
= UKBB wide file subset (ukbb_tools v0.1.0a1) =
[info] eids length: 2
[info] field_file value: None
[info] fields length: 3
[info] long value: True
[info] verbose value: True
[info] wide_file_path value: /data/ukbb/phenotypes/merged.txt.gz
[info] *** END ***
```

With the `--long` format we can see that now each value occupies a line. This affords the flexibility to tag each value with a measurement date and calculate the offset of that date from the baseline measure (in days). Positive values indicate that the value occurred/was measured after baseline and negative values indicate it occurred before baseline. Where the baseline date for a sample `53-0.0`, the date if the first assessment attendance. The date calculations are possible as the code that creates the data dictionary pulls out the most relevant date for each field ID. This is not immediately obvious in the wide file. The way this works, is that as a default, every field ID is given the baseline field ID as the most relevant date. Then for certain field IDs this is adjusted for a more meaningful date. This is accomplished via manual curation of the data dictionary.  When calculating the `relevant_date_minus_baseline` field. The `--long` format will first, locate the field ID matching the most meaningful date. Then the instance number will be matched with the field instance number. The dates are then subtracted to generate the `relevant_date_minus_baseline`. If the data for the most relevant date is not available, then the baseline field matching the same instance number is used as a replacement. If this happens, then the `suboptimal_date` field is set to `1` to highlight this to the user.

 eid     | baseline_date | baseline_date_int | field_text               | field_id | instance | array | relevant_date_field_text            | relevant_date_field_id | relevant_date_instance | relevant_date_array | relevant_date | relevant_date_int | relevant_date_minus_baseline | suboptimal_date | value 
:--------|:--------------|:------------------|:-------------------------|:---------|:---------|:------|:------------------------------------|:-----------------------|:-----------------------|:--------------------|:--------------|:------------------|:-----------------------------|:----------------|------:
 1000188 | 2009-03-10    | 39880             | WAIST_CIRCUMFERENCE      | 48       | 0        | 0     | DATE_OF_ATTENDING_ASSESSMENT_CENTRE | 53                     | 0                      | 0                   | 2009-03-10    | 39880             | 0                            | 0               |   100 
 1000188 | 2009-03-10    | 39880             | WAIST_CIRCUMFERENCE      | 48       | 1        | 0     | DATE_OF_ATTENDING_ASSESSMENT_CENTRE | 53                     | 1                      | 0                   | 2013-01-10    | 41282             | 1402                         | 0               |   100 
 1000188 | 2009-03-10    | 39880             | WAIST_CIRCUMFERENCE      | 48       | 2        | 0     | DATE_OF_ATTENDING_ASSESSMENT_CENTRE | 53                     | 2                      | 0                   | 2016-09-02    | 42613             | 2733                         | 0               |    95 
 1000188 | 2009-03-10    | 39880             | YEAR_OF_BIRTH            | 34       | 0        | 0     | DATE_OF_ATTENDING_ASSESSMENT_CENTRE | 53                     | 0                      | 0                   | 2009-03-10    | 39880             | 0                            | 0               |  1901 
 1000188 | 2009-03-10    | 39880             | HAND_GRIP_STRENGTH_RIGHT | 47       | 0        | 0     | DATE_OF_ATTENDING_ASSESSMENT_CENTRE | 53                     | 0                      | 0                   | 2009-03-10    | 39880             | 0                            | 0               |    61 
 1001130 | 2008-03-04    | 39509             | WAIST_CIRCUMFERENCE      | 48       | 0        | 0     | DATE_OF_ATTENDING_ASSESSMENT_CENTRE | 53                     | 0                      | 0                   | 2008-03-04    | 39509             | 0                            | 0               |    90 
 1001130 | 2008-03-04    | 39509             | YEAR_OF_BIRTH            | 34       | 0        | 0     | DATE_OF_ATTENDING_ASSESSMENT_CENTRE | 53                     | 0                      | 0                   | 2008-03-04    | 39509             | 0                            | 0               |  1895 
 1001130 | 2008-03-04    | 39509             | HAND_GRIP_STRENGTH_RIGHT | 47       | 0        | 0     | DATE_OF_ATTENDING_ASSESSMENT_CENTRE | 53                     | 0                      | 0                   | 2008-03-04    | 39509             | 0                            | 0               |    32 


In the example above we can see that waist circumference (field `48`) has 2 follow measures for sample `1000188` and these ocured 1201 and 2451 (made up numbers) after the baseline date for this sample. 

## 2. Interacting with the wide file via the parser
There is a dedicated parser for the UKBB wide phenotype file. This is used by the command line utility but can be used if more flexibility is required when extracting the data.

<OVERVIEW OF THE PARSER MODES AND FORMATTERS>

