# Overview of `wide_file_merge`
This module has functions and command endpoints for merging two wide files together. These are denoted the `old_file` and the `new_file`. It is designed to merge any basket refreshes into the existing wide file structure. The merged output file is compressed and indexed. This can now be used as input into future data merges.

## Algorithm
It is designed to work in low memory environments (these files can be quite large uncompressed) and it will attempt work out when samples have withdrawn from the study By their absence in the `new_file`. The algorithm will also guarantee that the columns IDs and the sample `eids` are output in sort order. Note that there will be other mechanisms to define column and row order in wide files.

The algorithm depends on both the `old_file` and the `new_file` being indexed. This is the mechanism by which it can efficiently match `EIDs` between samples and pull them out in order without loading the whole files in memory. If indexes are not detected for either of the files then they will be created prior to merging. This may involve the input files being overwritten with an indexed copy but this depends on the file name see [here](./wide_file_index.md).

The algorithm for merging works as follows:
1. Identify a union of sample identifiers between both files. The assumption is that samples in the `old_file` should be a superset of the those in the `new_file`. However, only warnings are generate if this is not the case. The union is sorted based on EID (note I may change this to keep the order if the `old_file` and drop any `new_file` EIDs (which should not exist) at the end).

2. Generate an integer sorted union of column IDs (these are `field_id`, `instance`, `array` tuples) and for each column indicate it's number in the `old_file` and the `new_file` . If the column exists in both the `old_file` and the `new_file` then the `new_file` column is flagged as the one to provide the data. Otherwise, whatever file contains the data column is flagged to provide the data. In this way, data from the `new_file` is prioritised over the `old_file`.

3. Iterate through the `EIDs` and for each `EID` extract the corresponding row from each of the indexed files. If the `EID` is not present in one of the files then a dummy row is generated for columns from that file. If the missing `EID` is from the `old_file`. A warning is generated as the `old_file` should be a super-set of sample IDs, in addition the withdrawn flag is **not** set to missing as it can't be assumed that the `old_file` is actually older given that no new samples are being added to UKBB. If the `EID` is missing from the `new_file` then the `EID` is set to missing as it is assumed that the `new_file` is missing that data due to the participant withdrawing from the study. Also, in this situation we force the data retrieval from the `old_file` as the relevant columns in the `new_file` will be missing and defined as empty.

4. For each EID, the data values are gathered in the order defined by the output header (sort order). Missing values are coded as `''`. Where data exists for the same column in both `new_file` and `old_file` the data values are compared to make sure they are the same. I am unclear if data values should be expected to change (i.e. if they will get fixed if errors are found) but I want to make sure that no different are observed for example at the index level between basket refreshes as this would compromise the whole merge process. Value checking currently doubles the runtime.

5. Each merged row is written to a tab delimited `bgzipped` file that is indexed.


## Command line usage
There is a command line entry point that can be used.
```
$ ukbb_wide_file_merge --help
usage: ukbb_wide_file_merge [-h] [-v]
                            old_file_path new_file_path out_file_path

Merge two UKBB wide files together using minimal memory

positional arguments:
  old_file_path  The path to old UKBB wide file
  new_file_path  The path to new UKBB wide file
  out_file_path  The path to output UKBB wide file

optional arguments:
  -h, --help     show this help message and exit
  -v, --verbose  Give more output
```

An example usage is shown below:
```
ukbb_wide_file_merge -v \
		      /path/to/existing/latest.txt.gz
		      /path/to/new/data.csv
		      /path/to.merged.txt.gz
```

## API usage
As with all key functions in `ukbb_tools` there is also a pythonic API entry point that can be used in scripts etc...(or if the `csv` arguments can't be sniffed out effectively)
```
from ukbb_tools import wide_file_merge

old_file = "/path/to/old_file.txt"
new_file = "/path/to/new_file.txt"
merged_file = "/path/to/merged_file.txt"

# Basic usage that assumes that the delimiter etc can be sniffed out
# and that index files (if present) are the same name as data files
# but with a .idx extension
merge_wide_files(
	old_file,
	new_file,
	merged_file,
	verbose=True
)
```
