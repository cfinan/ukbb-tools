# Overview of `config`
A module that stores functions for parsing and handling options in the configuration file.

The configuration file is at the core of option handling for the main `ukbb_pheno_norm` build script and numerous other scripts. Earlier versions of ukbb_tools allowed users to supply the config options via the command line or a config file. However, most command line options were removed and deferred to the config file as in practice they vary rarely need to be changed after initial setup and using a config only approach was cleaner. In additions, teams using UK BioBank can create a single config file within their UK BioBank data directory and this can be used for all members of the team.

The config file should be setup after the package has been installed. A configuration file details how to access the various databases that are required to standardise the UKBB to the UMLS and the locations of the data files, both the UK BioBank source files and files that have been created during the build process. The various build scripts will update/manage the config file as they create normalised files.

## Location
The command line scripts in this package that use the config file have a `--config-file` argument, where the user can specify the location of the config file. If the config file is not supplied here, then it is looked for in the environment variable `UKBB_CONFIG`, this can be defined in your `.~/.bashrc` or `~/.bash_profile` . If this is not defined then the a fallback is to look for the config file in the root of the user's home directory, the file should be called `~/.ukbb.cnf`.

## Format
The format of the config file is a standard `ini` format and it is parsed using the python [`configparser`](https://docs.python.org/3.7/library/configparser.html) package, that exists in the standard Python3 distribution.

The initial config file should have 3 database connection section headings and 7 file attribute headings for the initial UK BioBank data files. After the build process the config file will contain many more file attribute headings for the files that are generated during the build process.

### Database connection headings
The database connection parameters should be specified as [SQLAlchemy](https://www.sqlalchemy.org/) connection parameters. The `ukbb_tools` package only uses SQL alchemy for all of it's database access and building. This is to give the user's flexibility over the relational database management system (RDBMS) they use to store the mapping and phenotype data. For more information on SQLAlchemy connection URIs and parameters see [here](https://docs.sqlalchemy.org/en/13/core/engines.html#database-urls). Additionally, note that the keys to the various connection parameters are prefixed with `umls` (for the umls connection parameters) and `ukbb` or `ukbb_map` for the UK BioBank database and UKBB mapping database connections respectively, see [here](https://docs.sqlalchemy.org/en/13/core/engines.html#sqlalchemy.engine_from_config) for more information.

* `[umls_db_conn]` - The connection parameters for the UMLS database. The UMLS, needs to be downloaded and built into a relational database, in order to use `ukbb_pheno_norm`. Currently, the UMLS devs, only support `MySQL` (and by extensions `MariaDB` and variants). However, we are working on solutions for other database management systems including `SQLite` so the user does not have to run a whole database server which may be tricky in academic settings.  For more information on this, please see [`umls_tools`](https://gitlab.com/cfinan/umls_tools).

* `[ukbb_db_conn]` - The connection parameters to connect to your build of the UKBB phenotype database. This is not required for `ukbb_pheno_norm` but is required for `build_ukbb_pheno_db`. `build_ukbb_pheno_db` will only create a database if you are building into SQLite. For any other databases, an empty database should be created first using `utf-8` encoding.

* `[ukbb_mapping_db_conn]` - This is required for `ukbb_pheno_norm` and contains a build of all the mapping files that can be downloaded from the UKBB website. See [`ukbb_mapper`](./ukbb_mapper.md). This enables `read2`, `ICD9`, `OPCS3` codes to be mapped into later coding systems and then into the UMLS.

### File attribute headings
The optional sections are listed below. These are only used by `ukbb_pheno_norm` when no command line arguments of the same name are supplied. These sections allow the user to specify a `file` path and parsing information about the various files. By default. The delimiters and dialect of each file is ascertained with [`csv.Sniffer`](https://docs.python.org/3/library/csv.html#csv.Sniffer). However, sometimes this does not work and these sections allow the user to supply `csv` arguments to the various files without cluttering up the command line. In addition to the `file` parameter, the allowed parameters can be found [here](https://docs.python.org/3/library/csv.html#dialects-and-formatting-parameters). Additionally, the default encoding used to read in text in Python3 is `UTF-8`. However, some of the UKBB files (the GP ones) seem to be encoded in `latin1` (probably a database export), so the file `encoding` can also be specified here.
* `[wide_file]` - Parameters for the UKBB wide phenotype field file
* `[hes_events_file]` - Parameters for the HES events file
* `[hes_diagnosis_file]` - Parameters for the HES diagnosis file
* `[hes_operations_file]` - Parameters for the HES operations field file
* `[gp_clinical_file]` - Parameters for the GP clinical diagnosis and measures file
* `[gp_registrations_file]` - Parameters for the GP registrations file
* `[gp_prescriptions_file]` - Parameters for the GP prescriptions file

## Gotchas
There are some things to remember when putting together the config file. This first is that you will be putting an un-encrypted database password into the configuration file. So please do _not_ commit to git or share the config in any way. Additionally make sure that your config file is locked down in your home directory (the same way you would do for an SSH key) i.e. `chmod o-rwx ~/.ukbb.cnf`

Remember that if using database connection URLs that these are URLs so special character must be escaped please see [here](https://docs.python.org/3/library/urllib.parse.html#urllib.parse.quote_plus), also see [here](https://www.w3.org/International/questions/qa-escapes) for a general overview.

Specifying delimiters. The delimiter of each of the input files can be specified under the respective heading in the config file. However, if you have a `tab` or a `space` delimiter, please use the characters `\t` and `\s` respectively.

## Example
```
# A hash '#' is a comment
# Make sure no one else can see this file but you
# chmod o-rwx ~/.ukbb.cnf
[umls_db_conn]
# An SQLAlchemy connection URL, see:
# https://docs.sqlalchemy.org/en/13/core/engines.html#sqlalchemy.engine_from_config
# All connection options here should be prefixed with umls
# Make sure passwords are URL escaped:
# import urllib.parse
# PW = urllib.parse.quote_plus(PW)
# See below for info in connection URLS:
# https://docs.sqlalchemy.org/en/13/core/engines.html#database-urls
# Also, don't forget to escape any % that are in the URL (with a second %)
umls.url=mysql+pymysql://user:password@127.0.0.1:3306/umls_2020aa

[ukbb_db_conn]
# An SQLAlchemy connection URL, see:
# https://docs.sqlalchemy.org/en/13/core/engines.html#sqlalchemy.engine_from_config
# All connection options here should be prefixed with ukbb
ukbb.url=sqlite:////scratch/ukbb/ukbb_pheno.db

[ukbb_mapping_db_conn]
# An SQLAlchemy connection URL, see:
# https://docs.sqlalchemy.org/en/13/core/engines.html#sqlalchemy.engine_from_config
# All connection options here should be prefixed with ukbb_map
ukbb_map.url=sqlite:////scratch/ukbb/mapping_files/ukbb_mapping.db

[wide_file]
file = /scratch/ukbb/phenotypes/wide_files/latest.txt
delimiter = \t

[hes_events_file]
file = /scratch/ukbb/phenotypes/gp_data/gp_clinical.txt.gz
delimiter = \t

[hes_diagnosis_file]
file = /scratch/ukbb/phenotypes/hes/hes_diagnosis.txt.gz
delimiter = \t

[hes_operations_file]
file = /scratch/ukbb/phenotypes/hes/hes_operations.txt.gz
delimiter = \t
encoding=utf-8

[gp_clinical_file]
file = /scratch/ukbb/phenotypes/gp_data/gp_clinical.txt.gz
delimiter = \t
encoding=latin1

[gp_registrations_file]
file = /scratch/ukbb/phenotypes/gp_data/gp_registrations.txt.gz
delimiter = \t

[gp_prescriptions_file]
file = /scratch/ukbb/phenotypes/gp_data/gp_prescriptions.txt.gz
delimiter = \t
```
