=====================
Parsing NHS Trud data
=====================

The NHS does provide some mappings that can be used instead of or to augment the UK BioBank mapping files. In UK BioBank tools, there are scripts to work with the MS Access release of the SnomedCT Data Migration Workbench. These are converted in to tab-delimited files and used to build a mapping database.

Downloading the Data Migration Workbench
----------------------------------------

You will have to register with the `NHS Trud website <https://isd.digital.nhs.uk/trud3/user/guest/group/0/home>`_ first before downloading any of the data.

The DMWB can be found `here <https://isd.digital.nhs.uk/trud3/user/authenticated/group/0/pack/7/subpack/98/releases>`_

.. image:: images/trud_screenshot.png
  :width: 400
  :alt: DMWB

Extracting and converting to tab-delimited
------------------------------------------

There is a bash wrapper script to convert the TRUD MS Access database to tab delimited files. This in turn calls a Java script to perform the actual conversion. The Java Script is called ``ReadAccess.jar`` and should be located in the ``ukbb_tools/resources/bin`` directory. ``ReadAccess.jar`` can be run as a standalone script in which case it will accept 4 arguments:

.. code-block::

   ReadAccess.jar <input .mdb file> <user defined namespace for .mdb> <output directory> <output delimiter>

The name space argument is a string that is suffixed onto every table extracted from the ``.mdb`` MS Access file.

So whilst ReadAccess can be run directly it is probably easier to run it via the bash wrapper script ``parse-dmwb.sh``. It should take around 10 mins to run and can be run as follows:

.. code-block::

   # <input downloaded ZIP file> <output dir for tab delimied files>
   parse-dmwb.sh -v downloads/nhs_dmwb_31.3.0_20210120000001.zip nhs_dmwb_31.3.0/

This will produce the following files:

.. code-block::

   CTV3EQV.read.txt.gz
   CTV3HIER.read.txt.gz
   CTV3RCTMAP.xmap.txt.gz
   CTV3.read.txt.gz
   ctv3_rmf.read.txt.gz
   CTV3SCTMAP.xmap.txt.gz
   CTV3TC.read.txt.gz
   ctv3_term.read.txt.gz
   ICDHIER.xmap.txt.gz
   ICDSCTMAP.xmap.txt.gz
   ICDTC.xmap.txt.gz
   ICD.xmap.txt.gz
   MAPVERSIONS.xmap.txt.gz
   OPCSHIER.xmap.txt.gz
   OPCSSCTMAP.xmap.txt.gz
   OPCSTC.xmap.txt.gz
   OPCS.xmap.txt.gz
   RCTCTV3MAP.xmap.txt.gz
   RCTEQV.read.txt.gz
   RCTHIER.read.txt.gz
   RCT.read.txt.gz
   RCTSCTMAP.xmap.txt.gz
   RCTTC.read.txt.gz
   SCTCHIST.snomed_hist.txt.gz
   SCTEQV.snomed_hist.txt.gz
   SCTHIER.snomed.txt.gz
   SCTHIST.snomed_hist.txt.gz
   SCTHREL.snomed_hist.txt.gz
   SCTICDMAP.xmap.txt.gz
   SCTMETA.snomed_hist.txt.gz
   SCTMODREL.snomed.txt.gz
   SCTOPCSMAP.xmap.txt.gz
   SCTQT.snomed_query.txt.gz
   SCTREL.snomed.txt.gz
   SCT.snomed.txt.gz
   SCTSUBST.snomed_hist.txt.gz
   SCTTC.snomed_trans_closure.txt.gz
   SUBSETLIST.snomed_hist.txt.gz
   SUBSETS_RD.snomed_hist.txt.gz
   SUBSETS.snomed_hist.txt.gz
   TOKENINDEX.lexicon.txt.gz
   TOKENS.lexicon.txt.gz
   VERSIONS.snomed.txt.gz
   WEQ.lexicon.txt.gz
