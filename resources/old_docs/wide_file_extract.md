# Overview of `wide_file_extract`
This is a module of functions that are designed to extract specific information from rows of data parsed from the UKBB wide file. Rows should be parsed out containing `NoneType` values `nonetype=True` and using the nested format. For many of the functions the `numeric` mode is sufficient however, to be sure they always work a `full` parse might be best.

## Examples of using the functions in `wide_file_extract`

