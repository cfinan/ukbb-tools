# Overview of `ukbb_mapper`
This module contains classes, functions and a command line tools for building and interacting with a mapping database that facilitates the mapping of clinical terms into the [UMLS](https://www.nlm.nih.gov/research/umls/index.html). Note that, this database is absolutely required to use `ukbb_pheno_norm` and even with this database you will still need access to a database containing the [UMLS](https://www.nlm.nih.gov/research/umls/implementation_resources/scripts/index.html). In order to build the ukbb_mapping database you will need to define it's connection string in the [ukbb config file](./config.py).

## Building the UKBB mapping database
The UKBB mapping database is a no frills database containing indexed copies of the worksheets in the `all_lkps_maps.xlsx` file and the OPCS files that can be downloaded from the UKBB website. The SQLAlchemy interface for this database can be found in `ukbb_mapper_orm.py`, if programmatic access is required. 

The older `OPCS3` codes can be downloaded [here](http://biobank.ctsu.ox.ac.uk/showcase/coding.cgi?id=259). The newer `OPCS4` codes can be downloaded [here](https://biobank.ndph.ox.ac.uk/showcase/coding.cgi?id=240&nl=1)

A summary of the data providers can be found [here](https://biobank.ctsu.ox.ac.uk/crystal/exinfo.cgi?src=Data_providers_and_dates)

The `--help` for `build_ukbb_mapper` is shown below
```
$ build_ukbb_mapper --help
usage: build_ukbb_mapper [-h] [--config-file CONFIG_FILE] [--tmp-dir TMP_DIR]
                         [--keep-files KEEP_FILES] [-v]
                         ukbb_data_dict ukbb_data_codings ukbb_mapping_file
                         opcs3_file opcs4_file

build a UKBB mapping database

positional arguments:
  ukbb_data_dict        The path to the UKBB data dictionary
  ukbb_data_codings     The path to the UKBB data codings
  ukbb_mapping_file     The path to the UKBB mapping spreadsheet
  opcs3_file            The path to the OPCS3 lookup file
  opcs4_file            The path to the OPCS4 lookup file

optional arguments:
  -h, --help            show this help message and exit
  --config-file CONFIG_FILE
                        An alternative path to the config file
  --tmp-dir TMP_DIR     an alternative location to write temp files
  --keep-files KEEP_FILES
                        A location to store the files used to build
                        thedatabase, if not provided then the files will be
                        deleted
  -v, --verbose         give more output
```

If the config file outlines above was used to build the mapping files, then an SQLite database will be built at this location: `/home/user/ukbb_mapping.db`

```
build_ukbb_mapper -v \
                  --config-file ~/.ukbb.cnf \
				  --tmp-dir /data/tmp \
				  --keep-files /data/ukbb/mapping_files/processed_mapping_files \
				  /data/ukbb/mapping_files/source_files/Data_Dictionary_Showcase.tsv \
				  /data/ukbb/mapping_files/source_files/Codings_Showcase.tsv \
				  /data/ukbb/mapping_files/source_files/all_lkps_maps.xlsx \
				  /data/ukbb/mapping_files/source_files/coding259.tsv \
				  /data/ukbb/mapping_files/source_files/coding240.tsv
```

The resulting database can't be described as optimal in any way, it is not normalised and simply represents a vehicle to provide some sort of common interface to mapping files and data descriptions that not available in the UMLS and to have them indexed in someway. In future this database will likely be optimised properly.
