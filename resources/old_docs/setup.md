
### shflags
If you are using any of the helper bash scripts in [resources](.resources/bin) then you will also need to install the [`shflags`](https://github.com/kward/shflags) library. This is a simple but very effective library for handling command line arguments in bash.

```
# /your/external/software is a path where you might put external software
# you download
cd /your/external/software
git clone https://github.com/kward/shflags
```

The main `shflags` script that is sourced in should be in your `PATH` in `~/.bashrc` or `~/.bash_profile`

```
PATH="${PATH}:/your/external/software/shflags/shflags
```

## The config file
A configuration file must be provided that details how to access the various databasesand resources. For more detailed information on setting it up, please see [here](./docs/config.md).

## The mapping database
To use `ukbb_pheno_norm` you will need to download some files from the UKBB website and [build](./docs/ukbb_mapper.md) the `ukbb_mapping` database.

# Next Steps....
So if you have everything installed you are probably thinking _"now what?"_. Well, this largely depends on what you want from the UK BioBank data but most users will probably want to at least build a set of normalised files from the UK BioBank data. This is the first step to building a full database and will aid your interaction with the dataset.

## For all users
* Make sure your [config file](./docs/config.md) is setup.

## For admin users or data managers
* Building the UK BioBank dataset

## For end users/analysists
* Interacting with the UK BioBank dataset

# Contributing
Contribution guidelines
