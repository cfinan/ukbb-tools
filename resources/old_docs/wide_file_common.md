# Overview of `wide_file_common`
This module is a collection of functions that are used by many of the wide file modules. It provides a centralised point of import to prevent circular import issues. Please see the API documentation for more information.
