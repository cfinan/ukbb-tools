# MeRIT -  Mendelian Randomization for Identification of Targets
__version__: `0.2.2a0`

This package allows users to perform Mendelian randomisation (MR) analysis and all the data manipulation steps that are required before the analysis. Whilst the focus is on *cis*-MR it can also be used for traditional genomewide-MR. The MR methods currently implemented in MeRIT are:

* IVW
  - With and without correlated variants
TBC...

In addition, data pre-processing functions

* `read_gwas` - A comprehensive wrapper around Pandas `read_csv`, that will standardise the input data, allow filtering on read (to save memory) and use tabix indexes where available.
TBC...

As it is not yet publicly available, currently there is no online documentation in of MeRIT. However, offline static webpages can be [built](package_management.md) from the Sphinx sources `./docs`, where `.` is referenced from the root of the repository. However, if that is too much effort, a [PDF copy](`./resources/pdf/merit.pdf`) of the documentation is included in the repository.

## Installation instructions
At present, MeRIT is undergoing development and no packages exist yet on PyPi or in Conda. Therefore it is recommended that MeRIT is installed in either of the two ways listed below.

### Required repositories not in pypy or conda
All of the methods listed below will require the installation of some packages that are not yet in in the public domain.

Both of the installation methods below will also install console script endpoints that will be executable from the command line. For an overview of all the Python console endpoints (scripts) that are installed with MeRIT please see [here](scripts.rst). 

* In order to run the console script `simple-prs`, the [`simple progress`](https://gitlab.com/cfinan/simple_progress) repository has to be cloned, and installed. For instructions see [here](https://gitlab.com/cfinan/simple_progress).

## Installation not using any conda dependencies
If you are not using conda in any way then install the dependencies via `pip` and install MeRIT as an editable install also via pip:

```
python -m pip install --upgrade -r ./requirements.txt
```

For an editable (developer) install (recommended at this stage) run the command below from the root of the MeRIT repository:

```
python -m pip install -e .
```

## Installation using conda dependencies
A conda environment is provided in a `yaml` file in the directory `./resources/conda_env`. A new conda environment called `merit` can be built using the command:

```
# From the root of the MeRIT repository
conda env create --file ./resources/conda_env/merit_conda_env.yml
```

To add to an existing environment use:
```
# From the root of the MeRIT repository
conda env update --file ./resources/conda_env/merit_conda_update.yml
```

For an editable (developer) install (recommended at this stage) run the command below from the root of the MeRIT repository:
```
python -m pip install -e .
```

This will also install `simple_prs` as a console script endpoint that will be executable from the command line

Installing MeRIT through `conda env create --file conda_env/merit_conda_env.yml` may results in conflicts - particularly with [pysam](https://pysam.readthedocs.io/en/latest/). If this occurs please try the working environment dump file:

```
conda create --name merit --file ./resources/conda_env/working_env.text
```

## Testing
After installation, it is good practice to run the MeRIT tests to make sure it is working will on your system. Should any tests fail please review the error message, some tests might fail because of a incorrectly installed dependency or they could indicate a bug. Please [contact us](mailto:c.finan@ucl.ac.uk) if you are unsure.

```
pytest ./tests
```

## Contributing
We welcome user contributions in all forms:

1. Documentation fixes/updates
2. Example notebooks to illustrate features/methods
3. Bug fixes
4. New data handling code
5. New MR methods

All contributors will be included in the contributors list and acknowledged future MeRIT update publications. A contributors guide can be viewed [here](docs/source/contributing.rst).

Details of other project admin can be found in the [package management](package_management.md) section.
