# Overview of `pheno_normaliser`
The UKBioBank (UKBB) phenotype normaliser is a tool that complete the following tasks:

1. Parse the wide UKBB data file into several long format files that are subset by numeric (float and integer), enums (categorical), dates and strings
2. Extract sample vital statistics from the UKBB wide format file
3. Extract all event dates from the HES data, HES operations data, GP data
4. Express all the events from steps 1 and 3. as integer dates (from a fixed date: 01/0x1/1900), days relative to the baseline (field code 53-0.0 - first attendance at the assessment centre), age the event occurred and finally a nice uniform string formatted date. 
5. Map all the clinical codes from the HES (ICD9, ICD10, OPCS3, OPCS4) and GP data (read2, read3 (ctv)) into the [UMLS](https://www.nlm.nih.gov/research/umls/quickstart.html) and represent them as UMLS concepts (CUIs)
6. Parse the HES, HES Operations and GP data into three tables:
   * Diagnosis and operations
   * Clinical measures
   * Clinical notes
7. Parse the GP registration data and represent the registration dates in the same way as the events
8. Normalise all the GP prescription data and make it computer tractable
9. Map all the drug names back to [ChemblIDs](https://www.ebi.ac.uk/chembl/) and represent prescribed dosages in a uniform way and also with respect to the drug potency.

## Running the UKBB phenotype normaliser
Whilst is is technically possible to run the phenotype normaliser from the API, it is really designed to be used via the command line.

### Prerequisites
In order to use the `ukbb_pheno_norm` script, you will need to have:

1. Index all of the input file (see below)
2. A [config file](./config.md) in place
3. Build the [mapping database](./ukbb_mapper.md)
4. Have access to a [UMLS database](https://www.nlm.nih.gov/research/umls/implementation_resources/scripts/index.html)
4. **IN FUTURE** Have access to [Chembl](ftp://ftp.ebi.ac.uk/pub/databases/chembl/ChEMBLdb/latest/). Chembl are very flexible with their database distribution and a handy SQLite version is available [here](ftp://ftp.ebi.ac.uk/pub/databases/chembl/ChEMBLdb/latest/chembl_27_sqlite.tar.gz)

### Indexing the input files
Currently, the `ukbb_pheno_norm` script uses the following input files:

* HES events file
* HES diagnonsis file
* HES operations file
* GP clinical file
* The 'wide' UKBB phenotype file

All of these files with the exception of the wide file, _must_ be compressed and indexed before being used with `ukbb_pheno_norm`. Indexing will allow random access to specific sample rows within the file and makes the code for parsing them simpler since one can reply on the sample ordering within the file. The indexing is accomplished by used a tool called [`make_index_file`](https://gitlab.com/cfinan/pyaddons/-/blob/master/docs/file_index.md) in the `pyaddons`(https://gitlab.com/cfinan/pyaddons/-/tree/master) package. The files must have a single line header and be presorted on sample ID (eid) before being indexed. A no frills [bash script](../resources/bin/index_infile.sh) is provided to help with this. This should work under most scenarios, if not it is relatively easy to adapt.

It assumes that the sample column (eid) is the first column in the file and that the file is tab delimited and uncompressed (these are the defaults of the HES and GP data). The usage is:

```
ukbb_tools/resourses/bin/index_infile.sh <encoding> <input_file> <output_file>
```

An example:
```
ukbb_tools/resourses/bin/index_infile.sh 'latin1' 'hes_diag.txt' 'hes_diagnosis.txt.gz'
```

In tests, the GP files appeared to be encoded as `latin1` and everything else seemed to be `utf-8`

### Usage
With the prerequisites in place the mapping and normalising can be run. The help is shown below.
```
$ ukbb_pheno_norm --help
usage: ukbb_pheno_norm [-h] [--tmp-dir TMP_DIR] [--high-mem]
                       [--config-file CONFIG_FILE] [--hes-events HES_EVENTS]
                       [--hes-diag HES_DIAG] [--hes-oper HES_OPER]
                       [--gp-clinic GP_CLINIC]
                       [--ukbb-data-fields UKBB_DATA_FIELDS]
                       [--ukbb-sample-file UKBB_SAMPLE_FILE] [-v]
                       outdir

process UKBB phenotypes

positional arguments:
  outdir                The output directory to write the finished files

optional arguments:
  -h, --help            show this help message and exit
  --tmp-dir TMP_DIR     A temp directory location (default: system temp
  --config-file CONFIG_FILE
                        An alternative path to the config file
  --hes-events HES_EVENTS
                        The HES event date file
  --hes-diag HES_DIAG   The HES diagnonsis file
  --hes-oper HES_OPER   The HES operations file
  --gp-clinic GP_CLINIC
                        The GP clinical file
  --ukbb-data-fields UKBB_DATA_FIELDS
                        The 'wide' UKBB phenotype file
  -v, --verbose         give more output
```

An example run is shown below:
```
ukbb_pheno_norm -v \
	            --tmp-dir /data/tmp/ \
				--config-file ~/.ukbb.cnf \
				--hes-events /data/ukbb/phenotypes/hes/hesin.txt \
				--hes-diag /data/ukbb/phenotypes/hes/hesin_diag.txt \
				--hes-oper /data/ukbb/phenotypes/hes/hesin_oper.txt \
				--gp-clinic /data/ukbb/phenotypes/gp_data/gp_clinical.txt \
				--ukbb-data-fields /data/ukbb/phenotypes/latest.txt \
				/data/ukbb/processed_phenotypes
```

#### Command line arguments or config file?
`ukbb_pheno_norm` allows users to specify input files on the command line or via the [config file](./config.md#format). In general, it is recommended to supply files and thier processing parameters via the config file as when supplied via the command line the user has to rely on the default `UTF-8` encoding and Python's [`csv.Sniffer`]() to ascertain the correct delimiter and dialect. If a file is specified on the command line and in the config file then the version of the file in the config_file and the processing parameters are ignored in preference for the command line file.


### The import/normalisation process
This details the sequence of events that occurs when `ukbb_pheno_norm` is run. Whilst it is not necessary to know this it might make understanding of any error messages easier to understand.

The first stages involve parsing the configuration file and the initialisation of database connections to:

1. UMLS (governed by the `umls_db_conn` parameters in the config file)
2. The database where the final processed UKBB will be written (governed by the `ukbb_db_conn` parameters in the config file)
3. The database that contains the UKBB mapping files built with `build_ukbb_mapper` (governed by the `ukbb_mapping_db_conn` parameters in the config file)
4. A connection to a temp database that is used to store data during processing. If `--high-mem` flag is active, this will be an in-memory SQLite database, otherwise it will be an on-disk SQLite database

A temporary directory is then created with the prefix `.ukbb` with the default location under the system temp location. This can be changed with the `--tmp-dir` flat to `--ukbb_pheno_norm`. For those users that want programmatic access. The config and databases can be initialised by calling `initialise` in `pheno_normaliser.py`. The initialisation of temp and the processing process can be initialised with `process_files` in `pheno_normaliser.py`.

