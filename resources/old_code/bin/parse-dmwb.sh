#!/bin/bash
# Parse the NHS trud Data Migration Workbench database into TAB separated text
# files
. shflags

# configure shflags
DEFINE_boolean 'verbose' false 'give more output' 'v'
DEFINE_string \
    'tmp-dir' \
    "" \
    'An alternative location for tmp files' \
    'T'

FLAGS_HELP="USAGE: $0 [flags] <input file> <output dir>"

# parse the command-line
FLAGS "$@" || exit $?
eval set -- "${FLAGS_ARGV}"

# The positional arguments are a list of files that want concatenating
# Run the initialisation, this creates a bunch of functions some globals that
# are useful for general tasks that the script want to perform
# *** IMPORTANT *** - This must be done after shflags has finished
VERBOSE_TO_STDERR=0
. common.sh
. bh_init.sh

echo_prog_name "$0"
set_verbose_arg

# info_msg respects the verbosity that will be defined in FLAGS_verbose
# if shflags is being used. If not set the global VERBOSE argument
info_msg "temp directory: $TMPDIR"

# The working directory is a temp directory within TMPDIR created by
# bh_init.sh and deleted upon exit (either clean or error)
info_msg "working temp directory: $WORKING_DIR"

# Grab the output file and then remove it from $@
infile=$(readlink -f "$1")
check_file_readable "$infile" "DMWB not readable"
info_msg "Input file: $infile"

outdir=$(readlink -f "$2")
check_dir_writable "$outdir" "Output directory not writable"
info_msg "Output directory: $outdir"

dmwb_dir=$(mktemp -d -p"$WORKING_DIR")
unzip "$infile" -d"$dmwb_dir"
convert_nhs_dmwb "$dmwb_dir" "$outdir"
