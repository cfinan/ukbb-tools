# Functions common to all bash helper scripts. source in from helper script
START=$(date)
START_SECONDS=$(date '+%s')

# 1 is False 0 is True as per shflags
VERBOSE=1

# The default location of the config file
CONFIG_DEFAULT="${HOME}/.ukbb.cnf"


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
body() {
    IFS= read -r header
    printf '%s\n' "$header"
    "$@"
}


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
error_msg() {
    # Output an error message
    local msg="[error] ""$1"

    # We output to STDERR and STDOUT
    echo "$msg" 1>&2
}


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
info_msg() {
    if [[ $VERBOSE -eq 0 ]]; then
        # Output an error message
        local msg="[info] ""$1"

        # We output to STDOUT
        echo "$msg"
    fi
}


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
calc_run_time() {
    local start_seconds="$1"
    if [[ -z $start_seconds ]]; then
	      info_msg "runtime: unable to calculate runtime"
    else
	      local end=$(date)
	      local end_seconds=$(date '+%s')

	      # To get the floating point numbers easier than bc
	      runtime_hours=$(awk -vstart="$start_seconds" -vend="$end_seconds" 'BEGIN{time=(end-start)/3600; printf "%.2fh", time}')
	      info_msg "ended at $end"
	      info_msg "runtime: $runtime_hours"
    fi
}


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
check_outdir() {
    if [ $# -eq 0 ]; then
        echo 'error: outdir is missing' >&2
        flags_help
        exit 1
    fi
    OUTDIR=$1
    mkdir -p "$OUTDIR"
    info_msg "output directory: $OUTDIR"
}


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
check_dir_readable() {
    local dir_name="$1"

    if [[ ! -d "$dir_name" ]] || [[ ! -r "$dir_name" ]]; then
       error_msg "not readable: $dir_name"
       exit 1
    fi
}


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
check_dir_writable() {
    local dir_name="$1"

    if [[ ! -d "$dir_name" ]] || [[ ! -w "$dir_name" ]]; then
        error_msg "not readable: $dir_name"
        exit 1
    fi
}


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
echo_prog_name() {
    local prog_path=$1

    # The program name
    if [[ $VERBOSE -eq 0 ]]; then
        echo "=== "$(basename $prog_path)" ==="
    fi
    info_msg "started at $START"
}


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
check_config_file() {
    # This sets a global variable called CONFIG_FILE that contains the
    # location of the config file for ukbb_tools. It will fail if a config
    # file can't be found.
    #
    # Parameters
    # ----------
    # config : str
    #     A known location of a config file
    # Sets
    # ----
    # CONFIG_FILE : str
    #     A global config file variable with the location of the config file

    local config="$1"
    CONFIG_FILE="$CONFIG_DEFAULT"

    # The config is empty so we have to find it
    if [[ -z "$config" ]]; then
        # Has the UKBB_CONFIG environment variable been set? We have to turn
        # unbound variable errors of temporarily to handle this
        set +u
        if [[ -z "$UKBB_CONFIG" ]]; then
            # Finally check the default location and error of we can't find
            # anything
            if [[ ! -r "$CONFIG_DEFAULT" ]]; then
                error_msg "unable to locate config file: $CONFIG_DEFAULT"
                error_exit $LINENO
            fi
        else
            CONFIG_FILE="$UKBB_CONFIG"
        fi
        set -u
    else
        CONFIG_FILE="$config"
    fi

    if [[ ! -r "$CONFIG_FILE" ]]; then
        error_msg "config file not readable: $CONFIG_FILE"
        error_exit $LINENO
    fi
    info_msg "config file: $CONFIG_FILE"
}


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
get_config_section() {
    # Read a section from the UKBB config file into an associative array. The
    # associative array can't be returned from the function as such so has to
    # be parsed as a name reference, hence we need to use bash >= v4.3. This
    # calls `ukbb_config` to carry out the section query on the config file.
    # This will throw an error if the section does not exist.
    #
    # Parameters
    # ----------
    # config : str
    #     The path to the config file
    # section : str
    #     The section in the config file that we want to read
    # aarr : associative_array
    #     A pre-declared associative array, that we will create a name
    #     reference to. This means that the associative array that it points
    #     to will essentially be modified in place
    local config="$1"
    local section="$2"

    # Named reference pointing at an associative array that has been
    # declared in the calling script
    declare -n aarr="$3"
    args=($(ukbb_config -c "$config" "$section"))
    for ((c=0; c<${#args[*]}; c=c+2)); do
        key="${args[$c]}"
        value="${args[$c+1]}"

        # A fudge to turn \t into a real TAB
        if [[ "$key" == 'delimiter' ]] && [[ "$value" == '\t' ]]; then
            value=$'\t'
        elif [[ "$key" == 'delimiter' ]] && [[ "$value" == '\s' ]]; then
            value=' '
        fi
        # echo "$key"
        # echo "$value"
        aarr["$key"]="$value"
    done
}


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
get_key_length() {
    infile="$1"
    colno="$2"
    delimiter="$3"

    read_prog='cat'
    if [[ "$infile" =~ \.b?gz$ ]]; then
        read_prog='zcat'
    fi

    # Use awk to get the key size from the file
    "$read_prog" "$infile" |
        awk \
            -vdelim="$delimiter" \
            -vcolno="$colno" 'BEGIN{FS=delim;key_size=0}{if (length($colno)>key_size) {key_size=length($colno)}}END{print key_size}'
}


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
eid_index_raw_ukbb_file() {
    # Index all the raw UKBB files on their EID column
    local config_file="$1"
    local outdir="$2"
    local tmpdir="$3"
    local wide_idx="$4"

    info_msg "tmp_dir: $tmpdir"
    info_msg "wide file index: $wide_idx"

    # Make sure the output directory exists
    if [[ ! -d "$outdir" ]] && [[ ! -w "$outdir" ]]; then
        error_msg "outdir not writable: $outdir"
        error_exit $LINENO
    fi

    local sections=(
        gp_clinical
        ukbb_wide
        hes_events
        hes_diagnosis
        hes_operations
        gp_registrations
        # gp_prescriptions
    )

    index_col="eid"
    for i in ${sections[@]}; do
        info_msg "processing ${i}...."
        declare -A section_info
        # I am not sure the declaration overwrites the previous one
        # so after declaring I initialise to the default delimiter
        # before re-filling
        section_info['delimiter']=$'\t'
        section_info['compression']=""
        get_config_section "$config_file" "$i" section_info
        # for KEY in "${!section_info[@]}"; do
        #     echo "$KEY = ${section_info[$KEY]}"
        # done
        if [[ "$i" == "ukbb_wide" ]]; then
            eid_col_no=1
        else
            eid_col_no=$(ukbb_info -n "$i" "$index_col")
        fi
        info_msg "$i: $index_col column number: $eid_col_no"

        key_size=$(get_key_length "${section_info['file']}" "$eid_col_no" "${section_info['delimiter']}")

        sort_args=(
            -k${eid_col_no}nr,${eid_col_no}
        )
        indexer_args=(
            --key-size "$key_size"
            --outdir "$outdir"
            --config-file "$config_file"
            --stdin
        )

        if [[ ! -z "$tmpdir" ]]; then
            sort_args+=(-T"$tmpdir")
            indexer_args+=(--tmp-dir "$tmpdir")
        fi

        read_prog='cat'
        if [[ "${section_info['file']}" =~ \.b?gz$ ]]; then
            read_prog='zcat'
        fi

        if [[ "$VERBOSE" -eq 0 ]]; then
            indexer_args+=(--verbose)
        fi
        # head -n1000 "${section_info['file']}" |

        info_msg "key size=${key_size}"
        "$read_prog" "${section_info['file']}" |
                body sort -t"${section_info['delimiter']}" "${sort_args[@]}" |
                ukbb_indexer "${indexer_args[@]}" "$i" "$index_col"
    done
}


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
convert_nhs_dmwb() {
    # Convert the NHS data migration workbench access database files into
    # tab delimited text files
    #
    # Parameters
    # ----------
    # input_dir : str
    #   The input NHS data migration workbench
    # output_dir : str
    #   The output directory to write files to
    local indir="$1"
    local outdir="$2"
    local run_path="$(dirname "$0")"
    echo "$0"
    echo "$PWD"
    declare -A dmwb=( [xmap]='DMWB NHS Data Migration Maps.mdb'
                      [lexicon]='DMWB NHS Lexicon.mdb'
                      [read]='DMWB NHS READ.mdb'
                      [snomed_hist]='DMWB NHS SNOMED History.mdb'
                      [snomed]='DMWB NHS SNOMED.mdb'
                      [snomed_query]='DMWB NHS SNOMED Query Table.mdb'
                      [snomed_trans_closure]='DMWB NHS SNOMED Transitive Closure.mdb')
    # First make sure all the expected files exist
    for namespace in "${!dmwb[@]}"; do
        local infile="$indir"/"${dmwb[$namespace]}"
        if [[ ! -r "$infile" ]]; then
            error_msg "not readable: ""$infile"
            exit 1
        fi
    done

    # Now conver
    for namespace in "${!dmwb[@]}"; do
        local infile="$indir"/"${dmwb[$namespace]}"
        info_msg "extracting: ""$infile"
        java -jar "$run_path"/ReadAccess.jar "$infile" "$namespace" "$outdir" $'\t'
    done
}


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
create_ukbb_code_idx() {
    # Generate a list of unique clinical codes that appear in the various UKBB
    # clinical data files
    #
    # Parameters
    # ----------
    # config_file : str
    #   The config files with the UKBB data files detailed in them
    # output_file : str
    #   The output directory to write files to
    # tmp_doc : str
    #   The location of the directory to store temp files, if system temp is
    #   being used pass an empty string ""
    local config_file="$1"
    local output_file="$2"
    local tmp_loc="$3"

    # Here the keys are the file sections in the config file and the values are
    # comma separated strings of columns with code data in them
    declare -A sections=(
        [gp_clinical]="read_2,read_3"
        [hes_diagnosis]="diag_icd9,diag_icd10"
        [hes_operations]="oper3,oper4"
    )

    # The keys here are column names with codes in the various files the values
    # are coding system names as defined in the ukbb_tools package
    # (in constants.py)
    declare -A coding_systems=(
        [read_2]="read2"
        [read_3]="ctv3"
        [diag_icd9]="icd9"
        [diag_icd10]="icd10"
        [oper3]="opcs3"
        [oper4]="opcs4"
    )

    echo -e "cui\tcoding_system" > "$output_file"
    for i in "${!sections[@]}"; do
        info_msg "processing ${i}...."
        declare -A section_info
        # I am not sure the declaration overwrites the previous one
        # so after declaring I initialise to the default delimiter
        # before re-filling
        section_info['delimiter']=$'\t'
        section_info['compression']=""
        get_config_section "$config_file" "$i" section_info
        # for KEY in "${!section_info[@]}"; do
        #     echo "$KEY = ${section_info[$KEY]}"
        # done

        read_prog='cat'
        if [[ "${section_info['file']}" =~ \.b?gz$ ]]; then
            read_prog='zcat'
        fi

        local code_cols=($(echo "${sections[$i]}" | tr ',' ' '))
        for col in "${code_cols[@]}"; do
            col_no=$(ukbb_info -n "$i" "$col")
            info_msg "extracting ${i}.${col}: column number: $col_no"
            "$read_prog" "${section_info['file']}" |
                cut -f"$col_no" -d"${section_info['delimiter']}" |
                tail -n+2 |
                sort -u |
                awk -vcs="${coding_systems[$col]}" \
                    'BEGIN{OFS="\t"}{if ($1!="") {print $1,cs}}' \
                    >> "$output_file"
        done
    done
}
