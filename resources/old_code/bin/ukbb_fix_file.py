#!/usr/bin/env python
"""
A small script to produce a fix file - mappings between bad ukbb codes and what they should be using the NHS digital data as a reference
"""
import argparse
import gzip
import csv
import re
import pprint as pp


parser = argparse.ArgumentParser(
    description="produce a ukbb fix file")

parser.add_argument(
    'ukbb_read2', type=str,
    help="The path to the UKBB read2 lookup file"
)
parser.add_argument(
    'ukbb_ctv3', type=str,
    help="The path to the UKBB read ctv3 lookup file"
)
parser.add_argument(
    'nhs_read2', type=str,
    help="The path to the NHS read2 lookup file"
)
parser.add_argument(
    'nhs_ctv3', type=str,
    help="The path to the NHS read ctv3 lookup file"
)
parser.add_argument(
    'new_ctv3', type=str,
    help="The path to the new read ctv3 lookup file created from the UKBB "
    "mapping files"
)

args = parser.parse_args()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def read_ukbb_read2(file_name):
    ukbb_read2 = {}
    with gzip.open(file_name, 'rt') as infile:
        reader = csv.reader(infile, delimiter="\t")
        # Skip the header
        next(reader)

        for row in reader:
            if row[1] == '0':
                row[1] = '00'
            row[2] = clean_term(row[2])
            key = (row[2], row[1])
            try:
                ukbb_read2[key].append(row[0])
            except KeyError:
                ukbb_read2[key] = [row[0]]
    return ukbb_read2


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def read_nhs_read2(file_name):
    """
    """
    nhs_data = {}
    with gzip.open(file_name, 'rt') as infile:
        reader = csv.reader(infile, delimiter="\t")
        (reader)
        for row in reader:
            term = ""
            for i in [4, 3, 2]:
                if row[i] != '':
                    term = row[i]
                    break
            term = clean_term(term)
            key = (term, row[1])

            try:
                nhs_data[key].append(row[0])
            except KeyError:
                nhs_data[key] = [row[0]]
    return nhs_data


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def read_nhs_ctv3(file_name):
    """
    """
    nhs_data = {}
    with gzip.open(file_name, 'rt') as infile:
        reader = csv.reader(infile, delimiter="\t")
        (reader)
        for row in reader:
            term = ""
            for i in [6, 5, 4]:
                if row[i] != '':
                    term = row[i]
                    break
            term = clean_term(term)
            # using status deliberately
            key = (term, )

            try:
                nhs_data[key].add(row[0])
            except KeyError:
                nhs_data[key] = set([row[0]])
    for k in nhs_data.keys():
        nhs_data[k] = list(nhs_data[k])
    return nhs_data


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def read_ukbb_ctv3(file_name):
    """
    """
    data = {}
    with gzip.open(file_name, 'rt') as infile:
        reader = csv.reader(infile, delimiter="\t")
        # Skip the header
        next(reader)

        for row in reader:
            row[1] = clean_term(row[1])
            key = (row[1], )
            try:
                data[key].append(row[0])
            except KeyError:
                data[key] = [row[0]]
    return data


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def read_new_ctv3_lookup(file_name):
    """
    The read2/3 codes seem ok and should have the same descriptions as the
    lookup (which is screwed), so I might be able to use this to fix rather
    than the NHS data.

    produced with:
    cat <(zcat read_v2_read_ctv3.txt.gz |
    cut -f7-10) <(zcat read_ctv3_read_v2.txt.gz |
    tail -n+2 | cut -f1-4) | body sort -u | gzip > ctv3_new_lookup.txt.gz

    Has the header:
    1 READV3_CODE
    2 TERMV3_CODE
    3 TERMV3_TYPE
    4 TERMV3_DESC
    """
    data = {}
    with gzip.open(file_name, 'rt') as infile:
        reader = csv.reader(infile, delimiter="\t")
        # Skip the header
        next(reader)

        for row in reader:
            row[3] = clean_term(row[3])
            key = (row[3], )
            try:
                data[key].add(row[0])
            except KeyError:
                data[key] = set([row[0]])
    for k in data.keys():
        data[k] = list(data[k])
    return data


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def clean_term(term):
    term = term.strip()
    term = term.lower()
    term = re.sub('[\'"-]', '', term)
    return term


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def produce_fix(ukbb_def, nhs_def):
    """
    """
    fix = []
    not_found = {}
    unresolved = []

    # Loop through all the UKBB mappings
    for ukbb_term, ukbb_cui in ukbb_def.items():
        try:
            nhs_cui = nhs_def[ukbb_term]
        except KeyError:
            not_found[ukbb_term] = ukbb_cui
            continue

        if len(nhs_cui) == len(ukbb_cui):
            try:
                for nhs, ukbb, fix_type in _resolve_equal(nhs_cui, ukbb_cui):
                    fix.append(
                        (ukbb_term[:] + (ukbb, ) + (nhs, fix_type))
                    )
            except RuntimeError:
                pass
        else:
            unresolved.append((ukbb_term, ukbb_cui, nhs_cui))
    return fix, not_found, unresolved


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _resolve_equal(nhs_cui, ukbb_cui):
    """
    """
    nhs_missing = []
    for i in nhs_cui:
        try:
            cui_idx = ukbb_cui.pop(ukbb_cui.index(i))
        except ValueError:
            nhs_missing.append(i)

    if len(ukbb_cui) == 1:
        return [(nhs_missing[0], ukbb_cui[0], 'F')]
    elif len(ukbb_cui) > 1:
        nhs_missing.sort()
        ukbb_cui.sort()
        tentative = []
        for idx in range(len(nhs_missing)):
            tentative.append((nhs_missing[idx], ukbb_cui[idx], 'M'))
        return tentative
    else:
        raise RuntimeError("no fix needed")


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# def produce_fix(ukbb_def, nhs_def):
#     """
#     """
#     fix = []
#     not_found = {}
#     # unresolved = []

#     # Loop through all the UKBB mappings
#     for ukbb_term, ukbb_cui in ukbb_def.items():
#         try:
#             nhs_cui = nhs_def[ukbb_term]
#         except KeyError:
#             not_found[ukbb_term] = ukbb_cui

#         if len(nhs_cui) == len(ukbb_cui):
#             try:
#                 for nhs, ukbb, fix_type in _resolve_equal(nhs_cui, ukbb_cui):
#                     fix.append(
#                         (ukbb_term[:] + (ukbb, ) + (nhs, fix_type))
#                     )
#             except RuntimeError:
#                 pass
#         else:
#             not_found[ukbb_term] = ukbb_cui
#             # for i in ukbb_cui:
#             #     if len(i) != 5:
#             #         unresolved.append((ukbb_term[:] + (i,)))
#     return fix, not_found#, unresolved

# Read the UKBB codes
print("loading UKBB read2 data...")
ukbb_read2 = read_ukbb_read2(args.ukbb_read2)
print("loading NHS read2 data...")
nhs_read2 = read_nhs_read2(args.nhs_read2)
print("mapping UKBB read2 to NHS read2 data...")
read2_fix, read2_not_found, read2_unresolved = produce_fix(
    ukbb_read2, nhs_read2
)
print("number of UKBB read2 fixes: {0}".format(len(read2_fix)))
print("number of UKBB read2 not found: {0}".format(len(read2_not_found)))
print("number of UKBB read2 unresolved: {0}".format(len(read2_unresolved)))


print("loading NHS CTV3 data...")
nhs_ctv3 = read_nhs_ctv3(args.nhs_ctv3)

print("loading UKBB CTV3 lookup data...")
ukbb_ctv3 = read_ukbb_ctv3(args.ukbb_ctv3)

print("loading UKBB mapping CTV3 data...")
new_ctv3 = read_new_ctv3_lookup(args.new_ctv3)

print("mapping ukbb CTV3 to ukbb mapping CTV3 data...")
ctv3_fix, ctv3_not_found, ctv3_unresolved = produce_fix(
    ukbb_ctv3, new_ctv3
)

print("number of ukbb CTV3 fixes: {0}".format(len(ctv3_fix)))
print("number of ukbb CTV3 not found: {0}".format(len(ctv3_not_found)))
print("number of ukbb CTV3 unresolved: {0}".format(len(ctv3_unresolved)))


nhs_ctv3_fix, nhs_ctv3_not_found, nhs_unresolved = produce_fix(
    ctv3_not_found, nhs_ctv3
)

print("number of nhs CTV3 fixes: {0}".format(len(nhs_ctv3_fix)))
print("number of nhs CTV3 not found: {0}".format(len(nhs_ctv3_not_found)))
print("number of nhs CTV3 unresolved: {0}".format(len(nhs_unresolved)))

remap_terms = {}
remap_not_found = {}
for k, v in nhs_ctv3_not_found.items():
    term = re.sub(
        r'^see',
        clean_term('Retired CTV3 Code - no display term available; use'),
        k[0]
    )
    remap_terms[term] = k[0]
    remap_not_found[(term, )] = v

remap_ctv3_fix, remap_ctv3_not_found, remap_unresolved = produce_fix(
    remap_not_found, nhs_ctv3
)

print("number of remap CTV3 fixes: {0}".format(len(remap_ctv3_fix)))
print("number of remap CTV3 not found: {0}".format(len(remap_ctv3_not_found)))
print("number of remap CTV3 unresolved: {0}".format(len(remap_unresolved)))

# pp.pprint(remap_ctv3_not_found)
# pp.pprint(remap_ctv3_fix)

with open('/home/rmjdcfi/ukbb_mapping_fix.txt', 'wt') as outfile:
    writer = csv.writer(outfile, delimiter="\t")
    writer.writerow(
        [
            'ukbb_cui',
            'ukbb_tui',
            'ukbb_term',
            'fix_cui',
            'fix_tui',
            'fix_term',
            'fix_type',
            'coding_system'
        ]
    )
    for ukbb_term, ukbb_tui, ukbb_code, fix_code, fix_type in read2_fix:
        writer.writerow(
            [
                ukbb_code,
                ukbb_tui,
                ukbb_term,
                fix_code,
                ukbb_tui,
                ukbb_term,
                fix_type,
                'read2'
            ]
        )

    for ukbb_term, ukbb_code, fix_code, fix_type in ctv3_fix:
        writer.writerow(
            [
                ukbb_code,
                '',
                ukbb_term,
                fix_code,
                '',
                ukbb_term,
                fix_type,
                'ctv3'
            ]
        )

    for ukbb_term, ukbb_code, fix_code, fix_type in nhs_ctv3_fix:
        writer.writerow(
            [
                ukbb_code,
                '',
                ukbb_term,
                fix_code,
                '',
                ukbb_term,
                fix_type,
                'ctv3'
            ]
        )

    for ukbb_term, ukbb_code, fix_code, fix_type in remap_ctv3_fix:
        writer.writerow(
            [
                ukbb_code,
                '',
                remap_terms[ukbb_term],
                fix_code,
                '',
                ukbb_term,
                'R'+fix_type,
                'ctv3'
            ]
        )


pp.pprint(ctv3_unresolved)
pp.pprint(read2_unresolved)
