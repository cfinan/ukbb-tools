#!/bin/bash
# Download the UK BioBank mapping file and convert to tab delimited text files
. shflags

# configure shflags
DEFINE_boolean 'verbose' false 'give more output' 'v'
DEFINE_string \
    'tmp-dir' \
    "" \
    'An alternative location for tmp files' \
    'T'

FLAGS_HELP="USAGE: $0 [flags] <config_file> <outfile>"

# parse the command-line
FLAGS "$@" || exit $?
eval set -- "${FLAGS_ARGV}"

# The positional arguments are a list of files that want concatenating
# Run the initialisation, this creates a bunch of functions some globals that
# are useful for general tasks that the script want to perform
# *** IMPORTANT *** - This must be done after shflags has finished
VERBOSE_TO_STDERR=0
. common.sh
. bh_init.sh

echo_prog_name "$0"
set_verbose_arg

# info_msg respects the verbosity that will be defined in FLAGS_verbose
# if shflags is being used. If not set the global VERBOSE argument
info_msg "temp directory: $TMPDIR"

# The working directory is a temp directory within TMPDIR created by
# bh_init.sh and deleted upon exit (either clean or error)
info_msg "working temp directory: $WORKING_DIR"

config_file=$(readlink -f "$1")
check_file_readable "$config_file" "Config file not readable"
info_msg "Config file: $config_file"

outfile=$(readlink -f "$2")
check_file_writable "$outfile" "Output file not writable"
info_msg "Output file: $outfile"

# Create the codes
create_ukbb_code_idx "$config_file" "$outfile" "$TMPDIR"
