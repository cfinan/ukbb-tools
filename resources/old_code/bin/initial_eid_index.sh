#!/bin/bash
# This is a helper bash script designed to index all of the raw UKBB input
# files on their EID columns
# You will need to install shflags to use this, see ukbb_tools/README.md
# Also, see https://github.com/kward/shflags
. ./common.sh

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
error_exit() {
    # Called if we exit with an error, takes the line number of where the error
    # took place
    local LN="$1"

    # Make sure we clean up all the temp files before exiting with an error
    # exit code
    rmtemp_files

    # Errors are output to STDERR and STDOUT
    echo "[fatal] error on or near, '$LN'" 1>&2
    echo "!!!! ERROR EXIT !!!!" 1>&2

    exit 1
}

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
clean_exit() {
    # Only run the exit cleanup if we have exited cleanly, this condition has
    # been added as the EXIT trap is fireing when it is erroring out
    if [[ $? -eq 0 ]]; then
	      # Called if we exit cleanly
	      rmtemp_files

	      # calculate the runtime
	      calc_run_time "$START_SECONDS"
	      info_msg "*** END ***"
    fi

    exit $?
}

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
rmtemp_files() {
    # Ensure all the temp directories and files created in this script are
    # removed when it exists
    # Not strictly necessary but just in case the above fails
    for i in ${TEMPFILES[@]}; do
	      if [[ -e "$i" ]]; then
	          info_msg "deleting $i"
	          rm "$i"
	      fi
    done
}

# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# Store all the paths to the intermediate files, these will all be deleted when
# exitting cleanly or with an error. The "" is to stop unbound errors
TEMPFILES=("")

. shflags

# configure shflags
DEFINE_boolean 'verbose' false 'give more output' 'v'
DEFINE_boolean 'idx-wide-file' false 'when indexing files, index the wide file as well' 'w'
DEFINE_string 'config-file' "" 'An alternative path to the config file' 'c'
DEFINE_string 'tmp-dir' "" 'An alternative path to the config file' 't'
FLAGS_HELP="USAGE: $0 [flags] outdir"

# parse the command-line
FLAGS "$@" || exit $?
eval set -- "${FLAGS_ARGV}"

# Also, don't forget set -x if you want to debug
set -Euo pipefail

# If the script works, the function will be called and all the tmp files
# removed
trap 'clean_exit' EXIT

# If any command fail, or cntrl+C, then the exit function will be
# called.
trap 'error_exit $LINENO' SIGINT ERR SIGTERM

# Assign to our global VERBOSE variable
VERBOSE=$FLAGS_verbose
echo_prog_name $0
check_outdir "$@"

# After this call a global variable called CONFIG_FILE will be set, it is the
# location of the configuration file
check_config_file "$FLAGS_config_file"

declare -A hes_events
get_config_section "$CONFIG_FILE" "hes_events" hes_events

eid_index_raw_ukbb_file "$CONFIG_FILE" \
                        "$OUTDIR" \
                        "$FLAGS_tmp_dir" \
                        "$FLAGS_idx_wide_file"
