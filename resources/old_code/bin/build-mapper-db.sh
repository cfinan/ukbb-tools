#!/bin/bash
# This is a helper bash script designed to build the ukbb_mapper database
# it collects all the known clinical terms from the UKBB data files and
# makes them accessible to the python build script
# You will need to install shflags to use this, see ukbb_tools/README.md
# Also, see https://github.com/kward/shflags
. ./common.sh

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
error_exit() {
    # Called if we exit with an error, takes the line number of where the error
    # took place
    local LN="$1"

    # Make sure we clean up all the temp files before exiting with an error
    # exit code
    rmtemp_files

    # Errors are output to STDERR and STDOUT
    echo "[fatal] error on or near, '$LN'" 1>&2
    echo "!!!! ERROR EXIT !!!!" 1>&2

    exit 1
}

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
clean_exit() {
    # Only run the exit cleanup if we have exited cleanly, this condition has
    # been added as the EXIT trap is fireing when it is erroring out
    if [[ $? -eq 0 ]]; then
	      # Called if we exit cleanly
	      rmtemp_files

	      # calculate the runtime
	      calc_run_time "$START_SECONDS"
	      info_msg "*** END ***"
    fi

    exit $?
}

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
rmtemp_files() {
    # Ensure all the temp directories and files created in this script are
    # removed when it exists

    if [[ $REMOVE_OUT -eq 1 ]] && [[ "$OUTDIR" != "" ]]; then
       rm -r "$OUTDIR"
    fi

    if [[ "$tmp_loc" != "" ]] && [[ -d "$tmp_loc" ]]; then
       rm -r "$tmp_loc"
    fi

    # Not strictly necessary but just in case the above fails
    for i in ${TEMPFILES[@]}; do
	      if [[ -e "$i" ]]; then
	          info_msg "deleting $i"
	          rm "$i"
	      fi
    done
}

# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# Store all the paths to the intermediate files, these will all be deleted when
# exitting cleanly or with an error. The "" is to stop unbound errors
TEMPFILES=("")

. shflags

# configure shflags
DEFINE_boolean 'verbose' false 'give more output' 'v'
DEFINE_string 'config-file' "" 'An alternative path to the config file' 'c'
DEFINE_string 'nhs-digital' "" 'The path to the NHS digital access database file' 'n'
DEFINE_string 'tmp-dir' "" 'An alternative location for tmp files' 't'
DEFINE_string 'keep-files' "" 'Do you want to keep all the extracted mapping files? If so, enter the path here where you want them to be written to. If it does not exist it will be created' 'k'

FLAGS_HELP="USAGE: $0 [flags] <mapping_file> <opcs3_file> <opcs4_file>"

# parse the command-line
FLAGS "$@" || exit $?
eval set -- "${FLAGS_ARGV}"

# Also, don't forget set -x if you want to debug
set -Euo pipefail

# If the script works, the function will be called and all the tmp files
# removed
trap 'clean_exit' EXIT

# If any command fail, or cntrl+C, then the exit function will be
# called.
trap 'error_exit $LINENO' SIGINT ERR SIGTERM

# Initialise just incase we error out early and error_exit is called
OUTDIR=""
tmp_loc=""

# Assign to our global VERBOSE variable
VERBOSE=$FLAGS_verbose
echo_prog_name $0

MAPPING_FILE=""
OPCS3_FILE=""
OPCS4_FILE=""

# Process the positional arguments
if [[ $# -eq 3 ]]; then
    MAPPING_FILE=shift
    OPCS3_FILE=shift
    OPCS4_FILE=shift
else
    MAPPING_FILE=shift
    OPCS3_FILE=shift
fi

# If the user has supplied a keep fields dir make sure it is available
# if not then we use
REMOVE_OUT=0
if [[ "$FLAGS_keep_files" != "" ]]; then
    OUTDIR="$FLAGS_keep_files"
    mkdir -p "$FLAGS_keep_files"
else
    REMOVE_OUT=1
    if [[ "$FLAGS_tmp_dir" != "" ]]; then
        OUTDIR="$(mktemp -d -p "$FLAGS_tmp_dir")"
    else
        OUTDIR="$(mktemp -d)"
    fi
fi

NHS_OUT_DIR="$OUTDIR"/"nhs_digital_mappings"
UKBB_MAPPER="$OUTDIR"/"ukbb_mappings"
UKBB_CODES="$OUTDIR"/"ukbb_code_index"
check_dir_writable "$OUTDIR"
mkdir -p "$NHS_OUT_DIR"
mkdir -p "$UKBB_MAPPER"
mkdir -p "$UKBB_CODES"

# If we have some NHS digital data then process it
if [[ "$FLAGS_nhs_digital" != "" ]]; then
    info_msg "converting NHS digital files to txt"
    # convert_nhs_dmwb "$FLAGS_nhs_digital" "$NHS_OUT_DIR"
fi

# After this call a global variable called CONFIG_FILE will be set, it is the
# location of the configuration file
check_config_file "$FLAGS_config_file"

# Now we will generate the UKBB code index file, we will create into a temp
# file first and then copy into the final location
if [[ "$FLAGS_tmp_dir" != "" ]]; then
    temp_idx_file="$(mktemp -p "$FLAGS_tmp_dir")"
    tmp_loc="$(mktemp -d -p "$FLAGS_tmp_dir")"
else
    temp_idx_file="$(mktemp)"
    tmp_loc="$(mktemp -d)"
fi

# Will be removed on exit/error
TEMPFILES+=("$temp_idx_file")

info_msg "creating a UKBB code index file (this may take some time)..."
create_ukbb_code_idx "$CONFIG_FILE" "$temp_idx_file" "$tmp_loc"
# declare -A hes_events
# get_config_section "$CONFIG_FILE" "hes_events" hes_events

code_file="$UKBB_CODES"/"ukbb_index_codes.txt"
mv "$temp_idx_file" "$code_file"
gzip "$code_file"
