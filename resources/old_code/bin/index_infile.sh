#!/bin/bash
# This is a no frills bash script for indexing an input file.
# It assumes that the sample column is the first column in the
# file and that the file is tab delimited and uncompressed.
# Usage
# index_infile.sh <input_file> <encoding>


body() {
    IFS= read -r header
    printf '%s\n' "$header"
    "$@"
}

ENCODING="$1"
INFILE="$2"
OUTFILE="$3"

# Make the index file
cat "$INFILE" |
    body sort -t$'\t' -k1n,1 |
    make_index_file -d$'\t' -c1 -v -e"$ENCODING" - "$OUTFILE"
