# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Read2DrugsToBnfDef(object):
    """
    A handler for the GP registrations file
    """
    NAME = READ2_DRUGS_BNF

    COLUMNS = [
        col.R2_DRUGS_TO_BNF_READ2_CODE,
        col.R2_DRUGS_TO_BNF_BNF_CODE
    ]

    HEADER = [i.name for i in COLUMNS]

    CLINICAL_CODES = [
        (col.R2_DRUGS_TO_BNF_READ2_CODE.name, con.READ_TWO_CODE_TYPE_NAME)
    ]
