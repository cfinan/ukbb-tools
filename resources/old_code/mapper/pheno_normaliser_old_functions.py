
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def output_event_dates(event_dates, event_date_file):
    """

    """
    with gzip.open(event_date_file, 'wt') as event_out:
        event_writer = csv.writer(event_out,
                                  delimiter=ukbbc.GLOBAL_OUT_DELIMITER)
        event_writer.writerow(ukbbc.EVENT_TABLE_HEADER)
        for eid in sorted(event_dates.keys()):
            for idx, evt in enumerate(sorted(event_dates[eid].values(),
                                             key=lambda x: x['age_of_event']),
                                      1):
                try:
                    start_date = evt['epistart'].pretty_date
                    start_date_int = evt['epistart'].int_date
                except AttributeError:
                    start_date = None
                    start_date_int = None

                try:
                    end_date = evt['epiend'].pretty_date
                    end_date_int = evt['epiend'].int_date
                except AttributeError:
                    end_date = None
                    end_date_int = None

                outrow = [evt['eid'], idx, evt['is_incident'],
                          evt['age_of_event'], start_date, start_date_int,
                          end_date, end_date_int, evt['epidur'],
                          evt['dsource'], evt['source'], evt['ins_index']]
                event_writer.writerow(outrow)


# End of event helper functions


# ########################## Column Handling ################################


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def find_column_numbers(header, code):
    """
    Find column numbers for specific UKBB field codes

    Parameters
    ----------
    header : list or str
        The header of the file, each element is a column name in the header
    code : str
        The UKBB field code - note that this should just be the field code and
        no other information, i.e, the assessment number of replicate number

    Returns
    -------
    column_data : list of tuple
        The number of tuples represents the product of number of assessents
        and number of replicates in each assessment. Each tuple has:
        [0] column index       (int)
        [1] column name        (str)
        [2] field code         (str)
        [3] assessmment number (int)
        [4] replication number (int)

    Raises
    ------
    IndexError
        If we can't find any matches to the UKBB field code in the header
    """
    # The regular expression to match a UKBB field code i.e. 40001-0.0,
    # 40001-1.0 . The code is substituted in there at run time. I am doing
    # this at the moment as I am not 100% sure that all codes will be numeric
    # but I think thay are. When tested I will generalise the regexp
    match_reg = re.compile(r'''^(?P<CODE>{0})-         # Field code
                               (?P<ASSESS_NO>\d+)      # Assesssment number
                                \.(?P<REP>\d+)$'''.format(code), re.VERBOSE)

    # Will hold all the matches to the code regexp
    match_index = []

    # Loop through the columns in the header and see if they match
    # our current UKBB field code
    for c, i in enumerate(header):
        # matching from the start and the regexp has an end enchor
        match = match_reg.match(i)

        # If we have a match then we store the values we want.
        if match_reg.match(i):
            match_index.append((c, i, match.group('CODE'),
                                int(match.group('ASSESS_NO')),
                                int(match.group('REP'))))

    # Error out if we have not matched anything
    if len(match_index) == 0:
        raise IndexError("have not found any matches for code '{0}'".format(
            code))

    return match_index


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_colmap(header):
    """
    Attempt to find all the columns that we are interested in in the UKBB
    wide fields file. This operates over the module level variable:
    UKBB_SAMPLE_FIELDS

    Parameters
    ----------
    header : list of str
        A list of the header values for the wide file

    Returns
    -------
    column_map : dict of list of tuple
        The keys for the dictionary are human readable names for the UKBB
        field codes that we are interested in the tuples in the list values
        have the following structure:
        [0] column index       (int)
        [1] column name        (str)
        [2] field code         (str)
        [3] assessmment number (int)
        [4] replication number (int)
    """
    column_map = {}
    for fn, fc in UKBB_SAMPLE_FIELDS:
        column_map[fn] = find_column_numbers(header, fc)
    return column_map


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def valid_date(s):
    """
    from: https://stackoverflow.com/questions/25470844
    """
    try:
        return datetime.datetime.strptime(s, "%Y-%m-%d")
    except ValueError:
        msg = "Not a valid date: '{0}'.".format(s)
        raise argparse.ArgumentTypeError(msg)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def make_none_type(row):
    """
    Make '' into NoneType
    """
    return [None if i == '' else i for i in row]


def process_date(d):
    """
    d : :obj: `datetime.DateTime()`
    """
    date_pretty = d.strftime("%Y-%m-%d")
    date_int = (d - ukbbc.EPOCH_DATE).days
    return date_pretty, date_int


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_first_date(date_list):
    """

    """
    first_date = sorted(date_list)[0]
    first_date_pretty, first_date_int = process_date(first_date)
    return first_date, first_date_pretty, first_date_int


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def process_death_date(row, colmap):
    """
    Extract a death date if available
    """
    valid_death_date = []
    for idx, colname, code, assess, rep in colmap[DATE_OF_DEATH_NAME]:
        try:
            death_date = valid_date(row[idx])
            valid_death_date.append(death_date)
        except TypeError:
            pass

    if len(valid_death_date) == 0:
        return None, None

    first_date, first_date_pretty, first_date_int = get_first_date(
        valid_death_date)
    return first_date_pretty, first_date_int


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def process_assess_date(row, assess_idx):
    """
    Process the assessment date and return in sort order with early date first
    """
    # first_assess_date = valid_date(row[assess_idx[0][0]])
    all_assess_dates = [valid_date(row[i[0]]) for i in assess_idx
                        if row[i[0]] is not None]

    all_dates = []
    for d in all_assess_dates:
        date_pretty, date_int = process_date(d)
        all_dates.append((d, date_pretty, date_int))

    return all_dates


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def extract_event_dates(temp_db_session_maker, sample_set, hes_events_file,
                        hes_operations_file, gp_clinical_file, verbose=False,
                        commit_every=DEFAULT_COMMIT_LINES):
    """
    Extract all the event dates from the various data sources. These are
    currently held in memory but I might change this in future and do a write
    to file of most of the event data and keep the rest in memory

    Parameters
    ----------
    sample_set : dict
        The UKBB samples IDs (eids) as keys and a sample information `dict` as
        a value
    hes_events_file : str
        The path the the HES events file
    hes_diagnonis_file : str
        The path to the HES diagnonsis file
    gp_clinical_file : str
        The GP clinical diagnosis and measures file
    verbose, optional, default: False
        Output the progress at all stages
    commit_every : int, optional, default: 50000
        How often to commit the extracted events to the database. Note that
        this represents a maximum value and depending on how the file is
        sorted, commits may occur more often than this

    Returns
    -------
    n_events : int
        The total number of unique events that have been extracted
    """
    m = progress.Msg(verbose=verbose, prefix='[info]', file=sys.stderr)
    total_events = 0
    event_eids = set()

    # Create a session and make sure that the temp table exists where we
    # will load the temp event data
    session = temp_db_session_maker()

    # # TODO: comment back in during production
    # torm.TempEvent.__table__.create(bind=session.get_bind())
    session.close()

    # gp_events, gp_eids = extract_gp_events(
    #     temp_db_session_maker, gp_clinical_file, sample_set, verbose=verbose,
    #     nlines=gp_lines
    # )
    # total_events += gp_events
    # event_eids = event_eids.union(gp_eids)
    # m.msg('GP events: {0}'.format(gp_events))

    # Process the HES events file, this is different from the other files in
    # that it only has unique date episodes where as the others have dates
    # and codes with the dates not being unique
    # hes_events, hes_eid = parse_hes_events(
    #     temp_db_session_maker,
    #     hes_events_file,
    #     sample_set,
    #     verbose=verbose
    # )
    # total_events += hes_events
    # event_eids = event_eids.union(hes_eid)
    # m.msg('HES hospital events: {0}'.format(hes_events))

    # hes_oper_events, hes_oper_eid = extract_hes_operation_events(
    #     temp_db_session_maker,
    #     hes_operations_file,
    #     sample_set,
    #     verbose=verbose,
    #     # nlines=hes_oper_lines,
    #     commit_every=commit_every
    # )
    # total_events += hes_oper_events
    # event_eids = event_eids.union(hes_oper_eid)
    # m.msg('HES operation events: {0}'.format(hes_oper_events))
    # m.msg('total extracted events: {0}'.format(total_events))

    return total_events


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def extract_gp_events(temp_db_session_maker, gp_clinical_file, sample_set,
                      verbose=False, nlines=0, commit_every=DEFAULT_COMMIT_LINES):
    """
    Extract all the events from the GP data

    Parameters
    ----------
    temp_db_session_maker : :obj:`sqlalchemy.Sessionmaker`
        Used to create sessions that are bound to the temp SQLite database.
        This database will store all the event information that has been
        extracted
    gp_clinical_file : str
        The GP clinical diagnosis and measures file
    sample_set : dict
        A cache of the sample info, the keys are eids and the values are the
        basic sample information. This function utilises the first assessment
        date to calculate incidence and the date of birth to calculate age
        when the event occurred.
    verbose, optional, default: False
        Output the progress at all stages
    commit_every : int, optional, default: 50000
        How often to commit the extracted events to the database. Note that
        this represents a maximum value and depending on how the file is
        sorted, commits may occur more often than this
    nlines : int, optional, default: 0
        The number of lines in the file. If known this is passed to the
        progress monitor so the remaining time can be calculated. If not
        then a rate progress monitor is used

    Returns
    -------
    n_events : int
        The total number of unique events that have been extracted
    seen_eid : set of str
        The sample EIDs that have had events
    """
    return _extract_events(
        temp_db_session_maker, file_handler.GpClinicalFile,
        gp_clinical_file, sample_set,
        ukbbc.DATA_GP_VISIT, ukbbc.EID,
        ukbbc.GP_EVENT_DATE,
        ukbbc.GP_EVENT_DATE,
        event_provider_col=ukbbc.GP_DATA_PROVIDER,
        commit_every=commit_every, verbose=verbose,
        default_msg="extracting GP event dates",
        nlines=nlines
    )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def extract_hes_operation_events(temp_db_session_maker, hes_operations_file,
                                 sample_set, verbose=False,
                                 commit_every=DEFAULT_COMMIT_LINES, nlines=0):
    """
    Extract all of the HES operation events

    Parameters
    ----------
    temp_db_session_maker : :obj:`sqlalchemy.Sessionmaker`
        Used to create sessions that are bound to the temp SQLite database.
        This database will store all the event information that has been
        extracted
    hes_operations_file :str
        The path to the HES operations file
    sample_set : dict
        A cache of the sample info, the keys are eids and the values are the
        basic sample information. This function utilises the first assessment
        date to calculate incidence and the date of birth to calculate age
        when the event occured.
    verbose, optional, default: False
        Output the progress at all stages
    commit_every : int, optional, default: 50000
        How often to commit the extracted events to the database. Note that
        this represents a maximum value and depending on how the file is
        sorted, commits may occur more often than this
    nlines : int, optional, default: 0
        The number of lines in the file. If known this is passed to the
        progress monitor so the remaining time can be calculated. If not
        then a rate progress monitor is used

    Returns
    -------
    n_events : int
        The total number of unique events that have been extracted
    seen_eid : set of str
        The sample EIDs that have had events
    """
    return _extract_events(
        temp_db_session_maker, file_handler.HesOperationsFile,
        hes_operations_file, sample_set,
        ukbbc.DATA_HES_OPER, ukbbc.EID,
        ukbbc.OPERATION_DATE,
        ukbbc.OPERATION_DATE,
        event_instance_col=ukbbc.INSTANCE_IDX,
        event_pre_days_col=ukbbc.PRE_OPERATIVE_DURATION,
        event_post_days_col=ukbbc.POST_OPERATIVE_DURATION,
        commit_every=commit_every, verbose=verbose,
        default_msg="extracting HES operation event dates",
        nlines=nlines
    )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_hes_events(temp_db_session_maker, hes_events_file, sample_set,
                     verbose=False, commit_every=DEFAULT_COMMIT_LINES):
    """
    Parse out all the hospital visits from the HES events file. This file does
    not contain any codes, just hosplital episode statistics. This will only
    utilise a very small proportion of this file. The files this parses is
    typically called `hesin.txt` in UKBB and each unique event is identifiable
    by the eid (sample_id), ins_index (the event for that person) 

    Parameters
    ----------
    temp_db_session_maker : :obj:`sqlalchemy.Sessionmaker`
        Used to create sessions that are bound to the temp SQLite database.
        This database will store all the event information that has been
        extracted
    hes_events_file : str
        The hospital episodes file called `hesin.txt` in the UKBB docs
    sample_set : dict
        A cache of the sample info, the keys are eids and the values are the
        basic sample information. This function utilises the first assessment
        date to calculate incidence and the date of birth to calculate age
        when the HES event occurred.
    verbose, optional, default: False
        Output the progress at all stages
    commit_every : int, optional, default: 50000
        How often to commit the extracted events to the database. Note that
        this represents a maximum value and depending on how the file is
        sorted, commits may occur more often than this

    Returns
    -------
    n_events : int
        The total number of unique events that have been extracted
    seen_eid : set of str
        The sample EIDs that have had events
    """
    # Arguments
    # ---------
    # 1) Temp database connection
    # 2) parser class for the HES events
    # 3) Path to the HES events file
    # 4) The cached UKBB sample vital statistics
    # 5) The event type (constant for this table)
    # 6) EID column name
    # 7) Event start column
    # 8) Event end column
    # 9) Event provider column
    # 10) Event instance column
    # 11) A hash function to generate a unique hash for the event
    # 12) Commits to the temp database every X rows
    # 13) Verbosity
    # 14) Message for the verbose output
    return _extract_events(
        temp_db_session_maker, file_handler.HesEventsFile,
        hes_events_file, sample_set, ukbbc.DATA_HES_VISIT,
        ukbbc.EID, ukbbc.HES_EVT_EPISODE_START,
        ukbbc.HES_EVT_EPISODE_END,
        event_provider_col=ukbbc.HES_EVT_DATA_SUB_SOURCE,
        event_instance_col=ukbbc.INSTANCE_IDX,
        hash_func=hash_on_instance_id,
        commit_every=commit_every, verbose=verbose,
        default_msg="extracting HES hospital event dates"
    )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def hash_on_instance_id(evt):
    """
    Generate a hash based on the eid, event instance ID and the event type.
    This is used for the HES Events table instead of the whole Event type as
    the hash that is returned from this will be used to match to the codes in
    the diagnosis table

    Parameters
    ----------
    evt : :obj:`Event`
        The `Event` named tuple that we need to generate the hash for

    Returns
    -------
    hash : int
        The hash of the eid,event_instance and event type fields
    """
    return hash((evt.eid, evt.event_instance, evt.event_type_id))


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _extract_events(temp_db_session_maker, parser_cls, events_file, sample_set,
                    event_type, eid_col, event_start_col, event_end_col,
                    event_provider_col=-1, event_instance_col=-1,
                    event_pre_days_col=0, event_post_days_col=0,
                    hash_func=hash, verbose=False,
                    commit_every=DEFAULT_COMMIT_LINES,
                    nlines=0, default_msg=None):
    """
    A generalisation of the event processing

    Parameters
    ----------
    temp_db_session_maker : :obj:`sqlalchemy.Sessionmaker`
        Used to create sessions that are bound to the temp SQLite database.
        This database will store all the event information that has been
        extracted
    parser_cls : :obj:`file_handler.BaseFileHandler`
        A specific parser class that has the `get_column_index` class method
    events_file : str
        The path to the events file that we want to extract from
    sample_set : dict
        A cache of the sample info, the keys are eids and the values are the
        basic sample information. This function utilises the first assessment
        date to calculate incidence and the date of birth to calculate age
        when the event occured.
    event_type : :obj:`constants.EventType`
        An event type named tuple
    eid_col : :obj:`constants.ColName`
        A `ColName` namedtuple for the EID column
    event_start_col : :obj:`constants.ColName`
        A `ColName` namedtuple for the event start column
    event_end_col : :obj:`constants.ColName`
        A `ColName` namedtuple for the event end column. If there is no end
        column then supply the start here
    event_provider_col : :obj:`constants.ColName`, optional, default: -1
        A `ColName` namedtuple for the event provider column. This will have
        different meaning depending on the `EventType`. If there is no event
        provider column then supply a default integer value here
    event_instance_col : :obj:`constants.ColName`, optional, default: -1
        A `ColName` namedtuple for the event instance column. This is used in
        some cases to link back to the codes tables (hes_diagnonis_file). If
        not supplied then a value of -1 is used
    event_pre_days_col : :obj:`constants.ColName`, optional, default: 0
        Relevent days pre-event. This is designed to be a generalisation of the
        pre-operative stay columns, so will be 0 in most cases apart from
        events from the procedures file.
    event_post_days_col : :obj:`constants.ColName`, optional, default: 0
        Relevent days post-event. This is designed to be a generalisation of
        the post-operative stay columns, so will be 0 in most cases apart from
        events from the procedures file.
    hash_func : :obj:`function`, optional, default: `hash`
        A function to generate a hash of the event. Hashes will be used to
        determine unique events
    verbose, optional, default: False
        Output the progress at all stages
    commit_every : int, optional, default: 50000
        How often to commit the extracted events to the database. Note that
        this represents a maximum value and depending on how the file is
        sorted, commits may occur more often than this
    nlines : int, optional, default: 0
        The number of lines in the file. If known this is passed to the
        progress monitor so the remaining time can be calculated. If not
        then a rate progress monitor is used

    Returns
    -------
    n_events : int
        The number of unique events processed
    seen_eid : set of str
        The sample EIDs that have had events
    """
    # Get a session to handle database interaction
    session = temp_db_session_maker()
    seen_eid = set([])

    # If the user has not supplied a default message then make a general one
    if default_msg is None:
        default_msg = "processing '{0}'".format(os.path.basename(events_file))
    skipped = 0
    with parser_cls(events_file) as infile:
        # The type of progress monitor we use will change depending on if
        # we know how many lines we have to process or not, hence it is
        # functioned out
        prog = get_progress_monitor(nlines=nlines, verbose=verbose,
                                    file=sys.stderr, default_msg=default_msg)

        # Get the indexes of the columns that we are interested in
        eid_idx = get_column_idx(parser_cls, eid_col)
        event_start_idx = get_column_idx(parser_cls, event_start_col)
        event_end_idx = get_column_idx(parser_cls, event_end_col)
        event_provider_idx = get_column_idx(parser_cls, event_provider_col)
        event_instance_idx = get_column_idx(parser_cls, event_instance_col)
        event_pre_days_idx = get_column_idx(parser_cls, event_pre_days_col)
        event_post_days_idx = get_column_idx(parser_cls, event_post_days_col)

        db_calls = 0
        curr_eid = -1
        curr_events = set([])
        nevents = 0
        nadded = 0

        # precommit events - was used to understand some row dropping 
        # precommit_events = 0
        # Infile is an iterator, so will return each line in a loop such
        # as below
        for idx, row in prog.progress(enumerate(infile, 1)):
            eid = row[eid_idx]
            # if row[event_start_idx] is None:
            #     pp.pprint(row)
            try:
                # TODO: Remove KeyError catch in production
                cur_sample = sample_set[row[eid_idx]]
            except KeyError:
                print("can't find '{0}'".format(row[eid_idx]))
                skipped += 1
                continue

            if curr_eid != eid:
                # print("Switched from {0} to {1}".format(curr_eid, eid))
                # Switched to a new eid. So the current eid needs to be
                # processed. If we have not seen it before then all the events
                # get loaded into the database. If we have seen it before then
                # all of the existing events get queried from the database and
                # we check if any of the hashes do not exist in the database
                if curr_eid in seen_eid:
                    # print("Seen {0}".format(curr_eid))
                    # print(seen_eid)
                    # We have already sen this ID, which means that the file
                    # is not sorted on eid, in this case we need to check all
                    # the existsing events int the temp database, we also
                    # log the number of database calls so we can warn the
                    # user how in-efficient it is
                    db_calls += 1

                    # We need to make sure everything is committed to the
                    # database before we check to see if we have new events
                    session.commit()

                    # Get the events that are not already existing in the
                    # database
                    curr_events = define_new_events(session, eid,
                                                    curr_events,
                                                    hash_func=hash_func)

                # This is the number of events that have been added to the
                # session since the last commit
                # precommit_events += len(curr_events)
                nadded += add_temp_events(session, curr_events)

                # If we are at or over our threshold then commit
                if nadded >= commit_every:
                    session.commit()
                    # update and re-initialise the event counters
                    nevents += nadded
                    nadded = 0

                seen_eid.add(curr_eid)

                # Ready to hold new events for another person
                curr_events = set([])
                curr_eid = eid

            try:
                # Do the event calculations
                # Attempt to calculate the age of the event
                age_at_event = get_age_at_event(
                    cur_sample[ukbbc.SAM_DATE_OF_BIRTH.name].date_obj,
                    row[event_start_idx].date_obj
                )

                days_from_baseline = get_days_from_baseline(
                    cur_sample[ukbbc.SAM_DATE_OF_ASSESS.name].date_obj,
                    row[event_start_idx].date_obj
                )
            except AttributeError:
                # No event start has been defined, this could be because of
                # missing data or the sample has withdrawn
                if cur_sample[ukbbc.SAM_HAS_WITHDRAWN.name] is True:
                    # pp.pprint(cur_sample)
                    # continue and do not store the sample
                    skipped += 1
                    continue
                elif row[event_start_idx] is None:
                    # Genuine missing data, in which case make the age of
                    # event is set to None
                    age_at_event = None
                    days_from_baseline = None
                else:
                    # Genuine error
                    raise

            # Get all the values for the optional data columns
            event_provider = get_optional_data(row, event_provider_idx,
                                               event_provider_col)
            event_instance = get_optional_data(row, event_instance_idx,
                                               event_instance_col)
            event_pre_days = get_optional_data(row, event_pre_days_idx,
                                               event_pre_days_col)
            event_post_days = get_optional_data(row, event_post_days_idx,
                                                event_post_days_col)

            try:
                event_duration = (row[event_end_idx].date_obj -
                                  row[event_start_idx].date_obj).days
            except (AttributeError, TypeError):
                event_duration = None

            curr_events.add(
                Event(eid=row[eid_idx],
                      event_start=row[event_start_idx],
                      event_end=row[event_end_idx],
                      event_duration=event_duration,
                      event_type=event_type.name,
                      event_type_id=event_type.bits,
                      age_at_event=age_at_event,
                      days_from_baseline=days_from_baseline,
                      days_pre_event=event_pre_days,
                      days_post_event=event_post_days,
                      event_provider=event_provider,
                      event_instance=event_instance)
                )
            # if idx == 100000:
            #    break
            # if row[event_start_idx] is None:
            #     pp.pprint(row)
        # Outside the for loop, this is to catch events that have not been
        # added into the temp database since the last commit
        if curr_eid in seen_eid:
            db_calls += 1
            curr_events = define_new_events(session, curr_events)
        # precommit_events += len(curr_events)
        nevents += add_temp_events(session, curr_events)
        seen_eid.add(curr_eid)
        # print("[info] number of processed lines: {0}".format(idx))
        # print("[info] number of skipped lines: {0}".format(skipped))
        # print("[info] number of precommit events: {0}".format(precommit_events))
        if db_calls > 0:
            warnings.warn("'{0}' unecessary database queries - ideally this"
                          " should be 0, please pre-sort your files on 'eid'"
                          " first".format(db_calls))
    session.close()
    return nevents, seen_eid


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_progress_monitor(nlines=0, **kwargs):
    """
    Helper function to determine the correct progress monitor to use based on
    if nlines is available

    Parameters
    ----------
    nlines : int
        If nlines <= 0, then a `RateProgress` object is returned, if it is > 0
        then a `RemainingProgress` monitior is returned
    **kwargs
        Other arguments to the progress monitors

    Returns
    -------
    progress_monitor : :obj:`progress.RateProgress` or
                       :obj:`progress.RemainingProgress`
        A progress monitor
    """
    if nlines <= 0:
        return progress.RateProgress(**kwargs)
    else:
        return progress.RemainingProgress(nlines, **kwargs)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_column_idx(parser_cls, col_name):
    """
    Return a column number (index) when given a column name and a parser class.
    however, if the column name is actually a default value and not a column
    then return None

    Parameters
    ----------
    parser_cls : :obj:`file_handler.BaseFileHandler`
        A specific parser class that has the `get_column_index` class method
    col_name : :obj:`constants.ColName` or int
        A `ColName` named tuple that we want to find the column index for. Or
        if the column is not available an integer that will act as the default
        value

    Returns
    -------
    column_idx : int or NoneType
        The column index if a `ColName` was passed or `NoneType` if a default
        value was passed
    """
    # If it is a column name Namedtuple then attempt to find the column number
    # for it.
    if isinstance(col_name, ukbbc.ColName):
        return parser_cls.get_column_index(col_name)

    # Otherwise we are assuming that it is a default value to use and we
    # return NoneType
    return None


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def define_new_events(session, eid, evts, hash_func=hash):
    """
    Compare gathered events with what is already in the database. The
    comparison is based on comparising the output of the `hash_function`. If it
    is the same then the event is already present in the database

    Parameters
    ----------
    session : :obj:`sqlalchemy.Session`
        A session to query the temp database
    eid : int
        A UKBB sample ID
    evts : list of :obj:`Event`
        The events that we want to compare to the database. `Event` is a
        hashable namedtuple
    hash_func : :obj:`function`, optional, default: `hash`
        A hashing function, the output of which is compared to determine
        presence or absence. It defaults to the python `hash` function, this
        will hash a whole tuple for example (or any object with __hash__
        defined)

    Returns
    -------
    new_events : list of :obj:`Event`
        The events that do not already exist in the database
    """
    # Get the hashes for all the events
    evt_hashes = dict([(hash_func(e), e) for e in evts])

    # This query will return all the events that match the EID in the database
    # TODO: Investigate why the in_ is pointing at evt_hashes as it is a dict
    # (presumably it should be keys??)
    q = session.query(torm.TempEvent).filter(
        and_(torm.TempEvent.eid == eid,
             torm.TempEvent.event_hash.in_(evt_hashes))
    )

    # Return the events that are not in the database, i.e. events that need
    # adding
    return [evt_hashes[i] for i in set(evt_hashes.keys()).difference(
        [e.event_hash for e in q])]


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def add_temp_events(session, evts):
    """
    Add the events to the temp database. Note that this does not commit the
    events

    Parameters
    ----------
    session : :obj:`sqlalchemy.Session`
        A session to query the temp database
    evts : list of :obj:`Event`
        The events that we want to compare to the database

    Returns
    -------
    n_events : int
        The number of events that have been added to the session
    """
    for e in evts:
        try:
            # The dates might be NoneType so we need to pre handle them
            # if the start is NoneType then the end will be as well
            e_start = e.event_start.date_obj
        except AttributeError:
            e_start = None

        try:
            e_start_i = e.event_start.int_date
        except AttributeError:
            e_start_i = None

        try:
            e_end = e.event_end.date_obj
        except AttributeError:
            e_end = None

        try:
            e_end_i = e.event_end.int_date
        except AttributeError:
            e_end_i = None

        try:
            days_from_baseline = int(e.days_from_baseline)
        except TypeError:
            days_from_baseline = None

        session.add(
            torm.TempEvent(event_hash=hash(e),
                           eid=e.eid,
                           event_start_date=e_start,
                           event_start_date_int=e_start_i,
                           event_end_date=e_end,
                           event_end_date_int=e_end_i,
                           event_duration=e.event_duration,
                           event_type=e.event_type,
                           event_type_id=e.event_type_id,
                           age_at_event=e.age_at_event,
                           days_from_baseline=days_from_baseline,
                           days_pre_event=e.days_pre_event,
                           days_post_event=e.days_post_event,
                           event_provider=e.event_provider,
                           event_instance=e.event_instance,
                           event_idx=0,
                           event_order=0)
            )
    return len(evts)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_optional_data(row, col_idx, col_name):
    """
    Return the data value for the optional column. If the column index is None
    then this will return the default value in col_name. Otherwsie the correct
    element from row is returned

    Parameters
    ----------
    row : list
        A row from the file beig processed. This will have strings, numbers and
        `DateHolders` within it
    col_idx : int or NoneType
        The column number in the row that we want to extract. if this is
        NoneType then the row does not have the required column, in which case
        col_name is assumed to have the default value
    col_name : :obj:`constants.ColName` or `any`
        A `ColName` named tuple or a default value for the column
    """
    try:
        return row[col_idx]
    except TypeError:
        return col_name


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def order_and_write_event_dates(temp_db_session_maker, events_out_file,
                                sample_set, tmp_dir=None,
                                verbose=False,
                                # eids=None
                                commit_every=DEFAULT_COMMIT_LINES):
    """
    Define the event order, based on EID and event start date. Update in the
    temp database and also write to file

    Parameters
    ----------
    temp_db_session_maker : :obj:`sqlalchemy.Sessionmaker`
        Used to create sessions that are bound to the temp SQLite database.
        This database will store all the event information that has been
        extracted
    events_out_file : str
        The path to the events output file
    sample_set : dict
        A cache of the sample info, the keys are eids and the values are the
        basic sample information. This function utilises the first assessment
        date to calculate incidence and the date of birth to calculate age
        when the event occurred.
    eids : list of str or NoneType, optional, default: NoneType
        The EIDs that we want to query out and order. If `NoneType`, then all
        the EIDs in the sample set we be queried.
    tmp_dir : str or NoneType, optional, default: NoneType
        The location of the temp directory. Output files are first written here
        and then moved to the final location once complete
    """

    session = temp_db_session_maker()
    # if eids is None:
    #     eids = list(sample_set.keys())
    # else:
    #     # Could be a set
    #     eids = list(eids)
    eids = [i[0] for i in session.query(torm.TempEvent.eid).
            group_by(torm.TempEvent.eid).all()]
    session.close()
    eids.sort()
    print(len(eids))

    with file_helper.FlexiWriter(events_out_file, dir=tmp_dir,
                                 delimiter=ukbbc.GLOBAL_OUT_DELIMITER,
                                 force_gzip=True) as outfile:
        prog = get_progress_monitor(nlines=len(eids), verbose=verbose,
                                    file=MSG_OUT,
                                    default_msg="assigning event order")
        # Write the header
        outfile.writerow([i.name for i in ukbbc.EVENT_TABLE_HEADER])
        event_idx = 0
        for eid in prog.progress(eids):
            for order, evt in enumerate(get_event_for_eid(session, eid), 1):
                event_idx += 1
                evt.event_idx = event_idx
                evt.event_order = order

                outfile.writerow([
                    evt.event_idx,
                    evt.eid,
                    evt.event_order,
                    evt.days_from_baseline,
                    evt.age_at_event,
                    evt.event_start_date,
                    evt.event_start_date_int,
                    evt.event_end_date,
                    evt.event_end_date_int,
                    evt.event_duration,
                    evt.days_pre_event,
                    evt.days_post_event,
                    evt.event_type,
                    evt.event_type_id,
                    evt.event_provider,
                    evt.event_instance
                ])

                if event_idx % commit_every == 0:
                    session.commit()
    session.commit()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_event_for_eid(session, eid):
    """
    Get all the events for an eid in sort order with the first events at the
    top and all the missing data events at the bottom

    Parameters
    ----------
    session : :obj:`sqlalchemy.Session`
        An SQLAlchemy session object to perform the queries
    eid : int
        The UKBB sample id to query

    Returns
    -------
    events : list
        Event database objects from the temp events table
    """
    # Query all of the events for the required eid
    q = session.query(torm.TempEvent).filter(torm.TempEvent.eid == eid)

    # Now we perform two searches to get all the events in the correct order
    # keep in mind the first search is the secondary fields that will be
    # searched on

    # This gets the query results and sorts on a tuple with a Boolean for
    # missing duration and the event duration, this is reversed so that
    # missing duration are lower in the sort order than defined duration
    results = sorted(
        q.all(),
        key=lambda x: (x.event_duration is None, x.event_duration),
        reverse=True
    )

    # This simply resorts on the integer event start date
    results.sort(
        key=lambda x: (x.event_start_date_int is None,
                       x.event_start_date_int)
    )
    return results


