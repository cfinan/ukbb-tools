#!/usr/bin/env python
"""
A text script that will do a basic row column extraction of data from the wide
file. This is used as a sanity check to make sure that
"""
from ukbb_tools import __version__, __name__ as pkg_name, \
    wide_file_common as wfc
from Bio import bgzf
from simple_progress import progress
from contextlib import contextmanager
import builtins
import argparse
import sys
import os
import csv
import configparser
import gzip
import warnings


INDEX_SECTION = 'INDEX_SEEK'
METADATA_SECTION = 'METADATA'
MSG_PREFIX = '[info]'
MSG_OUT = sys.stderr


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main():
    """
    The main entry point to build an indexed wide file
    """
    # Initialise and parse the command line arguments
    parser = init_cmd_args()
    args = parse_cmd_args(parser)

    # Start a msg outputter, this will respect the verbosity and will output to
    # STDERR
    m = progress.Msg(prefix='', verbose=args.verbose, file=MSG_OUT)
    m.msg("= Sanity check ({0} v{1}) =".format(
        pkg_name, __version__))
    m.prefix = MSG_PREFIX
    m.msg_atts(args)

    # Extract any fields or eids. Note that some of them may be in files
    args.eids = _get_eids(args.eids)
    args.fields = _get_fields(args.fields, args.field_file)

    # We have to have something to subset
    if sum([len(args.eids), len(args.fields)]) == 0:
        raise ValueError("you do not want to subset anything? Why not "
                         "then: cp {0} <NEW_FILE>? :-)".format(
                             args.wide_file_path))
    m.msg_atts(args)

    # Now do the sanity check subset
    _do_subset(args.wide_file_path, args.eids, args.fields)

    m.msg("*** END ***")


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def init_cmd_args():
    """
    Initialise the command line arguments and return the parsed arguments

    Returns
    -------
    parser : :obj:`argparse.ArgumentParser`
        The argument parser with the arguments set up
    """
    parser = argparse.ArgumentParser(
        description="Subset samples and columns from a UKBB wide file")

    # The wide format UKBB data fields filex
    parser.add_argument(
        'wide_file_path', type=str,
        help="The path to UKBB wide file"
    )
    parser.add_argument(
        'eids', type=str, nargs="*",
        help="0 or more sample IDs to be extracted. If none are supplied"
        " then it is assumed that all samples are required. A file of sample"
        " IDs can also be supplied"
    )
    parser.add_argument(
        '--fields', type=str, nargs="+",
        help="One or more fields or column IDs to extract from the wide file."
        " Suppying none will extract all of them. A field ID is an integer a"
        " column name is the field ID, instance ID and array number combined "
        "in a string with the structure: <FID>-<INST>.<ARRAY>. Both can be "
        "supplied at the same time. If a field ID is supplied, all columns "
        "matching that ID will be extracted. If any columns are not present, "
        "then they are ignored and a warning is issued"
    )
    parser.add_argument(
        '--field-file', type=str,
        help="A file with field IDs or column names in it, there shoudl be "
        "one per line. This can be used inconjunction with --fields"
    )
    parser.add_argument('-v', '--verbose',  action="store_true",
                        help="Give more output")
    parser.add_argument(
        '-l', '--long',  action="store_true",
        help="Return the results in long format and not wide format. Long "
        "format results are also supplied with dates/times relative to "
        "baseline"
    )

    return parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_cmd_args(parser):
    """
    Parse the command line arguments

    Parameters
    ----------
    parser : :obj:`argparse.ArgumentParser`
        The argument parser with the arguments set up

    Returns
    -------
    args : :obj:`argparse.Namespace`
        The argparse namespace object
    """
    args = parser.parse_args()
    args.fields = args.fields or []

    # Make sure ~/ etc... is expanded
    for i in ['wide_file_path']:
        setattr(args, i, os.path.expanduser(getattr(args, i)))

    return args


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _get_eids(eids):
    """
    Extract all the eids into a list of unique eids. The first EID may well be
    a file of eids in which case to will be read in and added to the list

    Parameters
    ----------
    list of str
        A list of eids where the first eid in the list may well be a file
        containing eids

    Returns
    -------
    eids : list of int
        Any eids that the user has passed
    """
    all_eids = []
    try:
        first_eid = eids.pop(0)
        all_eids = [int(i) for i in _read_list_file(first_eid)]
    except FileNotFoundError:
        # Not an eid
        all_eids.append(int(first_eid))
    except IndexError:
        # pop from an empty list - no eids were provided
        pass
    all_eids.extend(eids)
    return list(set(all_eids))


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _get_fields(field_ids, field_file):
    """
    Extract all the eids into a list of unique eids. The first EID may well be
    a file of eids in which case to will be read in and added to the list

    Parameters
    ----------
    list of str
        A list of eids where the first eid in the list may well be a file
        containing eids

    Returns
    -------
    fields : list of int
        Any field IDs that the user has passed
    columns : list of str
        Any column names that the user has supplied
    """

    try:
        field_ids.extend(_read_list_file(field_file))
    except FileNotFoundError:
        # No file
        pass
    return field_ids


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _read_list_file(filename):
    """
    Attempt to read in a file that has single entries in it, one on each line.
    The file is read into memory

    Parameters
    ----------
    filename : str
        The path to the list file

    Returns
    -------
    list of str
        A list of the contents of each line
    """
    try:
        filename = os.path.expanduser(filename)
    except TypeError as e:
        raise FileNotFoundError("NoneTypes can't be found") from e

    file_rows = []
    with builtins.open(filename) as infile:
        for i in infile:
            file_rows.append(i.strip())

    return file_rows


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~--
def _do_subset(wide_file, eids, fields):
    """
    Perform the subset

    Parameters
    ----------
    wide_file : str
        The path to the wide file
    eids : list of int
        Any sample IDs that need to be extracted
    fields : list of int
        Any field IDs (groups of colnames) that need to be extracted, or any
        specific column names that need extracting
    """
    with open(wide_file, mode='full', format='flat') as infile:
        # print(infile)
        for row in infile.fetch(eids=eids, fields=fields, nonetype=False):
            pp.pprint(row)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == '__main__':
    main()
