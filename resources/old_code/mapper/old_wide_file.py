

# # @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# class WideFileParser(object):
#     """
#     Parser for the wide UKBB phenotypes file
#     """
#     FID_CACHE = {}

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     @classmethod
#     def get_field_enums(cls, field_ids, connect_uri=None):
#         """
#         This gets the field_id descriptions and enumerations. As this can be
#         quite an expensive operation they are cached at the class level for
#         future use. If field IDs are not found in the data dictionart=y then a
#         warning is issued and a dummy field ID is put in place with a string
#         data type

#         Parameters
#         ----------
#         field_ids : list of int
#             The field IDs that we want the information for
#         connect_uri : :obj:`sqlalchemy.engine.url.URL` or str or NoneType,
#         optional, default: NoneType
#             Connect to an alternative data dictionary. If NoneType then the
#             package data dictionary is used

#         Returns
#         -------
#         field_id_data : list of :obj:`data_dict.FieldId`
#             Field ID named tuple that contains information about the field
#             including any enumerations
#         """
#         data_dict_interface = dd.DataDictLookup(connect_uri=connect_uri)

#         # Get a list of the IDs that are not already cached
#         search_ids = [i for i in field_ids if i not in cls.FID_CACHE]

#         # Search for all the field Ids that have been found in the header of
#         # the file. Note this will be less than the number of columns that
#         # are present
#         for fid in data_dict_interface.get_field_id_enum(search_ids):
#             cls.FID_CACHE[fid.field_id] = fid

#         # Now loop through all the field IDs that we needed and extract to
#         # return
#         return_fids = []
#         for i in field_ids:
#             try:
#                 return_fids.append(cls.FID_CACHE[i])
#             except KeyError:
#                 # Problem with the data dictionary
#                 fid_info = dd.FieldId(
#                     field_id=fid,
#                     field_desc="possibly deprecated UKBB field ID",
#                     data_type='str',
#                     data_units=None,
#                     coding_id=None,
#                     date_field_id=dd.FID.ASSESSMENT_DATE,
#                     coding_enum=None
#                 )
#                 warnings.warn(
#                     "possibly deprecated UKBB field ID (will set to "
#                     "string): '{0}'".format(i)
#                 )
#                 cls.FID_CACHE[i] = fid_info
#                 return_fids.append(cls.FID_CACHE[i])
#         return return_fids

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     @staticmethod
#     def split_header(header_row):
#         """
#         Split a wide file header row into a list of tuples where each tuple
#         contains the integer elements of the column name. With the field_id
#         being at [0], the instance number being at [1] and the array index
#         being at [2]

#         Parameters
#         ----------
#         header_row : list of str
#             A header row from the wide file. Each element should have the
#             structure `{field_id}-{instance_no}.{array_idx}`. Except for the
#             `eid` column which must also be at the start of the row. Also, an
#             `withdrawn` optional column is allowed at element [1]

#         Returns
#         -------
#         header_tuples : list of tuple
#             The header split columns into tuples where each tuple contains the
#             integer elements of the column name. With the field_id being at
#             [0], the instance number being at [1] and the array index being
#             at [2]
#         """
#         # Split the header into a component tuple of ints, with the field_id
#         # being element [0], the n_instances being element [1] and the array
#         # value being element [2]
#         split_regexp = re.compile(r'[-.]')
#         header_tuples = []

#         # Loop through the header column names
#         for idx, i in enumerate(header_row):
#             try:
#                 # Generate the int tuple from the component parts and store
#                 # each one
#                 header_col = tuple([int(i) for i in split_regexp.split(i)])
#                 header_tuples.append(header_col)
#             except ValueError as e:
#                 # Can't split into a tuple
#                 if (i != 'eid' and idx != 0) and \
#                    (i != 'withdrawn' and idx != 1):
#                     # Can't remember what the error will be
#                     raise e.__class__("unknown header value '{0}'".format(i))
#                 # header_tuples.append((i, 0, 0))

#         return header_tuples

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     @staticmethod
#     def blank_vital_stats():
#         """
#         Return a dictionary with the vital statistics keys set but the values
#         set to NoneType

#         Returns
#         -------
#         vital_stats : dict
#             A dictionary witht the keys: `age_of_assess`, `age_of_death`,
#             `date_of_assessment`, `date_of_birth`, `date_of_death`, `eid`,
#             `genetic_sex`, `self_reported_sex` and all values set to `NoneType`
#         """
#         return {
#             con.EID.name: None,
#             con.SAM_AGE_OF_ASSESS.name: None,
#             con.SAM_AGE_OF_DEATH.name: None,
#             con.SAM_DATE_OF_ASSESS.name: None,
#             con.SAM_DATE_OF_BIRTH.name: None,
#             con.SAM_DATE_OF_DEATH.name: None,
#             con.SAM_GEN_SEX.name: None,
#             con.SAM_IS_CAUCASIAN.name: None,
#             con.SAM_SR_SEX.name: None,
#             con.SAM_N_ASSESSMENTS.name: None
#         }

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     @staticmethod
#     def sample_vital_stats(row):
#         """
#         Create vital statistics for a sample row. These are values that you
#         probably want for each sample when doing an analysis

#         Parameters
#         ----------
#         row : dict of dict
#             The keys of the first dictionary are the field IDs and the keys of
#             the nested dictionary are tuples of the (field ID n_reps and array)
#             (i.e. the content of the column header in the wide file but
#             expressed in tuple form of ints

#         Returns
#         -------
#         vital_stats
#         """
#         vital_stats = {}
#         vital_stats[con.EID.name] = WideFileParser.get_eid(row)
#         vital_stats[con.SAM_DATE_OF_BIRTH.name] = \
#             WideFileParser.get_approx_dob(row)
#         vital_stats[con.SAM_DATE_OF_ASSESS.name] = \
#             WideFileParser.get_first_assessment(row)
#         vital_stats[con.SAM_N_ASSESSMENTS.name] = \
#             len(WideFileParser.get_assessments(row))
#         vital_stats[con.SAM_AGE_OF_ASSESS.name] = \
#             WideFileParser.calc_age_of_event(
#                 vital_stats[con.SAM_DATE_OF_ASSESS.name],
#                 vital_stats[con.SAM_DATE_OF_BIRTH.name]
#             )

#         try:
#             gen_sex = WideFileParser.get_genetic_sex(row)
#         except KeyError:
#             gen_sex = ''

#         vital_stats[con.SAM_GEN_SEX.name] = gen_sex
#         vital_stats[con.SAM_SR_SEX.name] = \
#             WideFileParser.get_sex(row)

#         try:
#             # The death date is a funny one as it might be None (and filtered)
#             # if so then we capture the KeyError and treat as still alive
#             dod = WideFileParser.get_death_date(row)
#             age_of_death = WideFileParser.calc_age_of_event(
#                 dod,
#                 vital_stats[con.SAM_DATE_OF_BIRTH.name]
#             )
#         except KeyError:
#             dod = None
#             age_of_death = None

#         vital_stats[con.SAM_DATE_OF_DEATH.name] = dod
#         vital_stats[con.SAM_AGE_OF_DEATH.name] = age_of_death

#         try:
#             is_caucasian = WideFileParser.\
#                 get_is_caucasian(row)
#         except KeyError:
#             is_caucasian = '0'

#         vital_stats[con.SAM_IS_CAUCASIAN.name] = is_caucasian

#         return vital_stats

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     @staticmethod
#     def get_eid(row):
#         """
#         Return the EID (sample ID)

#         Parameters
#         ----------
#         row : dict of dict
#             The keys of the first dictionary are the field IDs and the keys of
#             the nested dictionary are tuples of the (field ID n_reps and array)
#             (i.e. the content of the column header in the wide file but
#             expressed in tuple form of ints

#         Returns
#         -------
#         eid : int
#             The sample identifier
#         """
#         return row[dd.FID.EID.value][(dd.FID.EID.value, 0, 0)]

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     @staticmethod
#     def get_assessments(row):
#         """
#         Return the assessments in the order of first assessment to latest
#         assements

#         Parameters
#         ----------
#         row : dict of dict
#             The keys of the first dictionary are the field IDs and the keys of
#             the nested dictionary are tuples of the (field ID n_reps and array)
#             (i.e. the content of the column header in the wide file but
#             expressed in tuple form of ints

#         Returns
#         -------
#         vital_stats

#         """
#         return [row[dd.FID.ASSESSMENT_DATE.value][i] for i in
#                 sorted(row[dd.FID.ASSESSMENT_DATE.value].keys())]

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     @staticmethod
#     def get_first_assessment(row):
#         """
#         Return the date of the first assessment. Note this is based on the
#         field codes and not the actual date

#         Parameters
#         ----------
#         row : dict of dict
#             The keys of the first dictionary are the field IDs and the keys of
#             the nested dictionary are tuples of the (field ID n_reps and array)
#             (i.e. the content of the column header in the wide file but
#             expressed in tuple form of ints

#         Returns
#         -------
#         first_assessement : `file_handler.DateHolder`
#             The first assessment date
#         """
#         return WideFileParser.get_assessments(row)[0]

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     @staticmethod
#     def get_last_assessment(row):
#         """
#         Return the date of the last assessment. Note this is based on the
#         field codes and not the actual date

#         Parameters
#         ----------
#         row : dict of dict
#             The keys of the first dictionary are the field IDs and the keys of
#             the nested dictionary are tuples of the (field ID n_reps and array)
#             (i.e. the content of the column header in the wide file but
#             expressed in tuple form of ints

#         Returns
#         -------
#         last_assessement : `file_handler.DateHolder`
#             The last assessment date
#         """
#         return WideFileParser.get_assessments(row)[-1]

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     @staticmethod
#     def get_approx_dob(row):
#         """
#         Return the date of birth based on month of birst and year of birth only
#         the day of birst is set to the first of the month

#         Parameters
#         ----------
#         row : dict of dict
#             The keys of the first dictionary are the field IDs and the keys of
#             the nested dictionary are tuples of the (field ID n_reps and array)
#             (i.e. the content of the column header in the wide file but
#             expressed in tuple form of ints

#         Returns
#         -------
#         approx_dob : `file_handler.DateHolder`
#             The approximate date fo birth
#         """
#         month = WideFileParser.\
#             get_field_id(row, dd.FID.MONTH_OF_BIRTH.value)
#         [(dd.FID.MONTH_OF_BIRTH.value, 0, 0)]
#         year = WideFileParser.\
#             get_field_id(row, dd.FID.YEAR_OF_BIRTH.value)
#         [(dd.FID.YEAR_OF_BIRTH.value, 0, 0)]
#         return file_handler.process_date(datetime(year, int(month), 1))

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     @staticmethod
#     def get_death_date(row):
#         """
#         Return the date of death, if the participant has passed away

#         Parameters
#         ----------
#         row : dict of dict
#             The keys of the first dictionary are the field IDs and the keys of
#             the nested dictionary are tuples of the (field ID n_reps and array)
#             (i.e. the content of the column header in the wide file but
#             expressed in tuple form of ints

#         Returns
#         -------
#         date_of_death : `file_handler.DateHolder`
#             The date of death

#         Raises
#         ------
#         KeyError
#             If the participant has not passed away
#         """
#         # for some reason there can be multiple death dates (for a few people)
#         # so we always return the earliest under the assumption it is the
#         # true one
#         dod = list(WideFileParser.
#                    get_field_id(row, dd.FID.DATE_OF_DEATH.value).values())
#         dod.sort(key=lambda x: x.int_date)

#         return dod[0]

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     @staticmethod
#     def get_sex(row):
#         """

#         """
#         sex = WideFileParser.\
#             get_field_id(row, dd.FID.SEX.value)
#         [(dd.FID.SEX.value, 0, 0)]
#         # TODO: To be enumerated
#         return sex

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     @staticmethod
#     def get_genetic_sex(row):
#         """

#         """
#         sex = WideFileParser.\
#             get_field_id(row, dd.FID.GEN_SEX.value)
#         [(dd.FID.GEN_SEX.value, 0, 0)]
#         # TODO: To be enumerated
#         return sex

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     @staticmethod
#     def get_is_caucasian(row):
#         """

#         """
#         return bool(
#             int(WideFileParser.
#                 get_field_id(row, dd.FID.IS_CAUCASIAN.value)
#                 [(dd.FID.IS_CAUCASIAN.value, 0, 0)]))

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     @staticmethod
#     def get_field_id(row, fid):
#         """
#         Extract a specific field ID

#         Parameters
#         ----------
#         row : dict of dict
#             The keys of the first dictionary are the field IDs and the keys of
#             the nested dictionary are tuples of the (field ID n_reps and array)
#             (i.e. the content of the column header in the wide file but
#             expressed in tuple form of ints
#         fid : int
#             The field identifier. Not ethis shoudl be without any replicates
#             of array information

#         Returns
#         -------
#         fid_data : dict
#             The specific data fields with that field ID. These are a dict with
#             (fid, n_replicates, array) tuplies as keys and the field data
#             values as values. The values should be cast into the correct data
#             version as documented in the data dictionary

#         Raises
#         ------
#         KeyError
#             If the fid is not present in the data
#         """
#         try:
#             return row[fid]
#         except KeyError as e:
#             raise KeyError("field ID '{0}' not in data: do you have access to"
#                            " this field?".format(fid)) from e

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     @staticmethod
#     def calc_age_of_event(event_date, date_of_birth):
#         """
#         Calculate the age that an event happened in decimal years

#         Parameters
#         ----------
#         event_date : :obj:`DateHolder`
#             The date the event happened
#         date_of_birth : :obj:`DateHolder`
#             The participant date of birth

#         Returns
#         -------
#         age_of_event : float
#             The decimal age in years that the event took place (rounded to 2dp)
#         """
#         return round((event_date.date_obj - date_of_birth.date_obj) /
#                      timedelta(days=365), 2)
#     #####
#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     def __init__(self, infile, connect_uri=None, **csv_kwargs):
#         """
#         Parameters
#         ----------
#         infile : str
#             The input file path
#         connect_uri : :obj:`sqlalchemy.engine.url.URL` or str or NoneType,
#         optional, default: NoneType
#             Connect to an alternative data dictionary. If NoneType then the
#             package data dictionary is used
#         **csv_kwargs
#             Arguments that will be passed through to python csv.reader
#         """
#         self._infile = os.path.expanduser(infile)
#         self._csv_kwargs = csv_kwargs
#         self._connect_uri = connect_uri

#         # If no csv keyword arguments have been supplied then we attempt to
#         # sniff them out
#         if len(self._csv_kwargs) == 0:
#             # In this wide file we need to sniff a larger amount of data to get
#             # it correct (2MB)
#             self._csv_kwargs['dialect'] = file_handler.sniff_delimiter(
#                 self._infile, read_bytes=2000000)

#         # Initialise all the variables used in the object
#         self._fobj = None
#         self._reader = None
#         self._header = []
#         self._header_len = 0
#         self._line_no = 0
#         self._is_open = False
#         self._using_context_manager = False
#         self._is_iterating = False

#         # This is the column numbers (and number of columns that will be
#         # removed) from the start of each row. So it will either be [0] (eid)
#         # or [0, 0] (eid, withdrawn) (basicially the non-field ID columns
#         self._pops = [0]

#         # mappings between datatypes in the database and functions that will
#         # convert the data to the correct type
#         self._cast_map = {
#             'int': parse_int,
#             'float': parse_float,
#             'enum_single': parse_enum,
#             'enum_multiple': parse_enum,
#             'date': parse_date,
#             'time': parse_date,
#             'str': dummy_call,
#         }

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     def __enter__(self):
#         """
#         Entry point for the context manager
#         """
#         # Opening the file, will set the header up
#         self.open()
#         self._using_context_manager = True
#         return self

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     def __exit__(self, *args):
#         """
#         Entry point for the context manager

#         *args
#             Error arguments should the context manager exit in discrace
#         """
#         self.close()
#         self._using_context_manager = False

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     @property
#     def header(self):
#         """
#         Return the full header of the file irrespective of any subsetting that
#         has been attempted
#         """
#         return self._header

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     def open(self):
#         """
#         Open the file. Note that this is agnostic for gzipped files
#         """
#         # Mkae sure the file is not open as we do not want to open twice
#         self._check_file_closed()
#         self._fobj = None

#         # Re initialise the variables associated with the file
#         self._header = []
#         self._header_len = 0
#         self._line_no = 0
#         self._pops = [0]
#         self._header_tuples = []
#         self.bad_values = []
#         self.subset_header = []
#         self.subset_header_tuples = []
#         self.subset_header_idx = []
#         self.subset_field_ids = []

#         # compression agnostic opening
#         self._fobj = gzopen.gzip_fh(self._infile)
#         self._reader = csv.reader(self._fobj, **self._csv_kwargs)
#         self._process_header()
#         self._is_open = True

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     def _check_file_closed(self):
#         """
#         Make sure that the file is not open and error out if it is

#         Raises
#         ------
#         IOError
#             If the file is open
#         """
#         if self._is_open is True:
#             raise IOError('UKBB wide file already open')

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     def _process_header(self):
#         """
#         Process the header from the file and make sure it is valid. Also,
#         compare to the data dictionary

#         Raises
#         ------
#         ValueError
#             If the header of the file does not match the expected header
#         """
#         # When we have read the header in we have also read a line, i.e. lines
#         # are referenced from the begining of the file not the data
#         self._line_no += 1
#         self._header = next(self._reader)

#         # If we have a withdrawn column as the second column then we will want
#         # to remove the first and second column from each row
#         if self._header[1] == 'withdrawn':
#             self._pops = [0, 0]
#         print(self._pops)
#         lead_cols = self._remove_lead_columns(self._header)
#         pp.pprint(self._header[:10])
#         # Make sure the first column is the eid if not then there really is
#         # something wrong
#         if lead_cols[0] != dd.FID.EID.value:
#             raise ValueError(
#                 "expecting the first column to be '{0}' not"
#                 " '{1}'".format(dd.FID.EID.value, lead_cols[0])
#             )

#         # # Remove the EID
#         # self._header.pop(0)
#         # if self._header[0] == 'withdrawn':
#         #     self._header.pop(0)
#         #     self._pops = [0, 0]

#         # We store the length so it can be compared to each row to make sure
#         # they are of the expected length
#         self._header_len = len(self._header)

#         # Split the header column into component tuples
#         self._header_tuples = self.__class__.split_header(self._header)

#         # TODO: Remove this, it was put here for debugging
#         for idx, i in enumerate(self._header_tuples):
#             wanted = "{0}-{1}.{2}".format(*i)
#             if self._header[idx] != wanted:
#                 raise IndexError("header missaligned, {0} != {1}".format(
#                     self._header[idx], wanted))

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     def _remove_lead_columns(self, row):
#         """
#         Removes the lead columns from the row and returns them. This will
#         either be a single column (eid) or two columns (eid, withdrawn). The
#         row is not returned as it happens in place

#         Parameters
#         ----------
#         row : list or str
#             The row to remove the lead column(s)

#         Returns
#         -------
#         lead_columns : list of str
#             The list will either be length 1 or length 2
#         """
#         return [row.pop(i) for i in self._pops]

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     def close(self):
#         """
#         Close the file
#         """
#         self._check_file_open()

#         self._fobj.close()
#         self._is_opened = False
#         self._fobj = None
#         self._reader = None

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     def _check_file_open(self):
#         """
#         Make sure that the file is open and error out if it is not

#         Raises
#         ------
#         IOError
#             If the file is closed
#         """
#         if self._is_open is False:
#             raise IOError('UKBB wide file not open, either use the context '
#                           'manager or obj.open()')

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     def fetch(self, eids=None, field_ids=None, colnames=None, nonetype=True):
#         """
#         fetch parsed rows from the UKBB wide file with the option to extract
#         specific eids, field and/or columns. If the wide file has been indexed
#         then this will be used to extract any specific eids. Note that both
#         field_ids and column name extractions can be used at the same time.

#         Parameters
#         ----------
#         eids : NoneType or list of int
#             Specific UKBB sample IDs that are required. If `NoneType` then all
#             sample IDs are required
#         field_ids : NoneType or list of int
#             Specific UKBB field IDs that are required. If `NoneType` then all
#             field IDs are required unless colnames is set. Note that the
#             difference between field IDs and colnames is that one field_id may
#             equate to many column names as it could have multiple instances
#             and arrays. So `field_ids` is a quality of life feature so the user
#             does not have to care
#         colnames : NoneType or list of str
#             Specific UKBB column names that are required. If `NoneType` then
#             all column names are required unless field_ids is set. A column
#             name is the specific field_id, instance number and array number
#             column in the header
#         nonetype : bool, optional, default: True
#             Return empty columns from the row. If False columns with '' will
#             be removed

#         Yields
#         ------
#         row : dict
#             A parsed and processed row from the wide file. each row represents
#             all the measures in the wide file that are available for an
#             individual sample
#         """
#         self._check_file_open()

#         # TODO: Seek back to the header position??

#         # Make sure all the fetch speecific values are initialised
#         self.bad_values = []
#         self.subset_header = []
#         self.subset_header_tuples = []
#         self.subset_header_idx = []
#         self.subset_field_ids = []

#         # This sets the function that will be used to format the row and set
#         # the data types, this function will be different depending on if we
#         # want to keep nonetype values or not
#         row_format_method = self._get_row_format_method(nonetype)

#         # The row fetch method will change depending on if we want everything
#         # or specific IDs and if the file has been indexed or not
#         row_fetch_method = self._get_row_fetch_method(eids=eids)

#         # The column subset method will chnage depending on if we want to
#         # select any specific field IDs or column names
#         col_subset_method = self._get_column_subset_method(
#             field_ids=field_ids,
#             colnames=colnames
#         )

#         # The row is all the data fields and the leading_cols is the eid and
#         # withdrawn column (if present)
#         for row, leading_cols in row_fetch_method(eids):
#             row = col_subset_method(row)
#             row = row_format_method(row, int(leading_cols[0]))

#             yield row

#         # Issue any bad values warning messages
#         self._test_bad_values(nonetype)
# ####
#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     def _get_row_format_method(self, nonetype):
#         """
#         Return the method used to format the datatypes in the row

#         Parameters
#         ----------
#         nonetype : bool
#             Do you want NoneType values in the row data, True if yes, False
#             if now

#         Returns
#         -------
#         format_method : function
#             The method call to format the row
#         """
#         if nonetype is True:
#             return self._parse_row
#         return self._parse_row_defined

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     def _get_row_fetch_method(self, eids=None):
#         """
#         Get the method used to fetch the rows based on if specific eids have
#         been requested and if the file is indexed. Note that both field_ids and
#         column name extractions can be used at the same time.

#         Parameters
#         ----------
#         eids : NoneType or list of int
#             Specific UKBB sample IDs that are required. If `NoneType` then all
#             sample IDs are required

#         Returns
#         -------
#         row_fetch_method : function
#             The function used to fetch the rows from the wide file
#         """
#         # TODO: ID indexed files
#         indexed = False
#         row_fetch_method = self._yield_all_rows
#         if eids is not None and len(eids) > 0 and indexed is True:
#             # Specific EIDs but with indexing
#             raise NotImplementedError("eid extraction not implemented")
#             # row_fetch_method = self._yield_index_row
#         elif eids is not None and len(eids) > 0 and indexed is False:
#             # Specific EIDs but with no indexing
#             row_fetch_method = self._yield_eid_rows

#         return row_fetch_method

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     def _get_column_subset_method(self, field_ids=None, colnames=None):
#         """
#         Get the method used to subset columns from the rows in the wide file.
#         Note that both field_ids and column name extractions can be used at
#         the same time. This function also sets the subset_header and other
#         variables related to the subset that are also public to the user

#         Parameters
#         ----------
#         field_ids : NoneType or list of int
#             Specific UKBB field IDs that are required. If `NoneType` then all
#             field IDs are required unless colnames is set. Note that the
#             difference between field IDs and colnames is that one field_id may
#             equate to many column names as it could have multiple instances
#             and arrays. So `field_ids` is a quality of life feature so the user
#             does not have to care
#         colnames : NoneType or list of str
#             Specific UKBB column names that are required. If `NoneType` then
#             all column names are required unless field_ids is set. A column
#             name is the specific field_id, instance number and array number
#             column in the header

#         Returns
#         -------
#         column_subset_method : function
#             The function used to subset columns from the rows from the wide
#             file
#         """
#         # The default is no subsetting
#         column_subset_method = self._no_subset

#         # If no field/column subsetting is happening then we want all the
#         # columns so we setup the subset variables to reflect that even though
#         # they will not be used in the subset process, but the user may need
#         # them
#         if field_ids is None and colnames is None:
#             # Setup the subset variables for "all columns"
#             self.subset_header = self._header.copy()
#             self.subset_header_tuples = self._header_tuples.copy()
#             self.subset_header_idx = list(range(self._header_len))
#             self.subset_field_ids = \
#                 list(set([i[0] for i in self._header_tuples]))

#             # this will get all the parse information from the data dictionary
#             # and can take some time when there are lots of columns
#             self._set_subset_parse()
#             return column_subset_method

#         # If we get here then we want to be extracting either columns that
#         # start with a field ID or a specific column name

#         # Will hold the indexes of the columns that we want to subset
#         colno = set()

#         # Will hold the IDs of fields that we can't find in the data
#         missing_fids = set()

#         # If we want columns that start with a specific field ID
#         if field_ids is not None and len(field_ids) > 0:
#             found_fids = set()

#             # We search in the header tuples as these have already been split
#             # into field IDs, we store the index of all the columns that match
#             # on field ID. We also store the field IDs that have been matched
#             # so we can use them to workout what field IDs have not been
#             # matched
#             for idx, i in enumerate(self._header_tuples):
#                 if i[0] in field_ids:
#                     found_fids.add(i[0])
#                     colno.add(idx)

#             missing_fids = set(field_ids).difference(found_fids)

#         # Will hold the names of any columns that can't be found
#         missing_colnames = []
#         if colnames is not None and len(colnames) > 0:
#             # Loop through all the user supplied columns and attempt to get
#             # their index, if we can't find it then they are stored in
#             # missing_colnames
#             for i in colnames:
#                 try:
#                     print(i)
#                     colno.add(self._header.index(i))
#                 except ValueError as e:
#                     print(e)
#                     print(self.subset_header[:10])
#                     missing_colnames.append(i)

#         # Error out of we do not find any columns or matching field IDS
#         if len(colno) == 0:
#             raise ValueError("no columns found!")

#         # If we get here then we have found something so we are safe to set
#         # the column subset method to use the column indexes to extract the
#         # column data
#         column_subset_method = self._col_subset

#         # Build all the subset lests. The subset_header_idx is the one that
#         # will actually be used to extract the data values
#         colno = sorted(list(colno))
#         self.subset_header = [self._header[idx] for idx in colno]
#         self.subset_header_tuples = [self._header_tuples[idx] for idx in colno]
#         self.subset_header_idx = colno
#         self.subset_field_ids = \
#             sorted(list(set([i[0] for i in self.subset_header_tuples])))

#         # If we can't find any columns of fields then issue warnings
#         if len(missing_fids) > 0:
#             warnings.warn(
#                 'fields not found: {0}'.format(
#                     ",".join([str(i) for i in missing_fids])
#                 )
#             )

#         if len(missing_colnames) > 0:
#             warnings.warn(
#                 'columns not found: {0}'.format(
#                     ",".join([str(i) for i in missing_colnames])
#                 )
#             )

#         # This will query the data dictionary to locate the parse data for the
#         # matching columns
#         self._set_subset_parse()

#         return column_subset_method

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     def _set_subset_parse(self):
#         """
#         After the subset field IDs have been set. This uses them to query the
#         data dictionary to get the enumeration information and data type
#         information for all of the fields. This will applied to all the
#         columns that we want to parse out of the file
#         """

#         # Use the class method to extact the data, this will also cahce the
#         # data at the class level
#         self.subset_header_field_info = \
#             self.__class__.get_field_enums(
#                 self.subset_field_ids,
#                 connect_uri=self._connect_uri
#             )

#         # This will hold the same number of columns as we have in the header
#         # and will be used to cast all the fields to the correct format
#         self._subset_header_parse = []
#         for fid, n_instances, array in self.subset_header_tuples:
#             fid_info = self.__class__.FID_CACHE[fid]
#             cast_func = self._cast_map[fid_info.data_type]
#             self._subset_header_parse.append(
#                 (fid_info, cast_func)
#             )

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     def _test_bad_values(self, nonetype):
#         """
#         This issues warnings depending on if there are any bad values stored
#         inside the object. The warning message will vary slighly depending on
#         if we are parsing out NoneType values for removing them. bad_values are
#         data values that can't be cast to the correct format
#         """
#         if len(self.bad_values) > 0:
#             msg = (
#                 "there are '{0}' bad values in the dataset. use:"
#                 " 'obj.bad_values' to see them.".format(len(self.bad_values))
#             )

#             if nonetype is True:
#                 msg = ("{0} These will be set to 'NoneType' in the "
#                        "data".format(msg))
#             else:
#                 msg = ("{0} These will be removed from the data".format(msg))
#             warnings.warn(msg)

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     def _fetch_row(self):
#         """
#         Fetch a row from the file. This also increments the line number that
#         we are on

#         Returns
#         -------
#         row : list of str
#             A row that has been read with `csv.reader`, no type conversion has
#             been applied at this point
#         leading_cols : list or str
#             A list of the non-field ID columns. This will certaily have the
#             `eid` column at [0] and may have the `withdrawn` column at [1]

#         Raises
#         ------
#         StopIteration
#             When we are at the end of the file
#         IndexError
#             If the length of the data row is not what is expected
#         """
#         self._line_no += 1

#         try:
#             row = next(self._reader)
#             leading_cols = self._remove_lead_columns(row)

#             # Check the row length
#             if len(row) != self._header_len:
#                 raise IndexError(
#                     "{0}: row has '{1}' columns expected: '{2}'".format(
#                         self._line_no,
#                         len(row), self._header_len
#                     )
#                 )

#             return row, leading_cols
#         except StopIteration:
#             # As we are preincrementing, if we reach the end we have to
#             # subtract one
#             self._line_no -= 1
#             raise

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     def _yield_all_rows(self, *args):
#         """
#         Yeild all the rows (unformatted) in order from the file

#         Parameters
#         ----------
#         *args
#             Any arguments that have to be parsed to keep the interface the
#             same between all three row yielding methods. Has no effect

#         Yields
#         ------
#         row : list of str
#             A row that has been read with `csv.reader`, no type conversion has
#             been applied at this point
#         leading_cols : list or str
#             A list of the non-field ID columns. This will certaily have the
#             `eid` column at [0] and may have the `withdrawn` column at [1]
#         """
#         while True:
#             try:
#                 yield self._fetch_row()
#             except StopIteration:
#                 break

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     def _yield_eid_rows(self, eids):
#         """
#         Fetch specific EIDs when the file has NOT been indexed

#         Parameters
#         ----------
#         eids : list of int
#             Specific UKBB sample IDs that are required. If `NoneType` then all
#             sample IDs are required

#         Yields
#         ------
#         row : list of str
#             A row that has been read with `csv.reader`, no type conversion has
#             been applied at this point
#         leading_cols : list or str
#             A list of the non-field ID columns. This will certaily have the
#             `eid` column at [0] and may have the `withdrawn` column at [1]
#         """
#         while True:
#             try:
#                 row, leading_cols = self._fetch_row()

#                 try:
#                     # Is the sample ID of the current row one of the ones
#                     # that we require? If not then this will fail with a
#                     # value error
#                     idx = eids.index(int(leading_cols[0]))
#                     eids.pop(idx)
#                     yield row, leading_cols

#                     # See if there are any more eids left to process if not
#                     # then we may as well exit
#                     if len(eids) == 0:
#                         break
#                 except ValueError:
#                     # Not needed
#                     pass
#             except StopIteration:
#                 # EOF
#                 break

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     def _yield_index_rows(self, eids):
#         """
#         Fetch specific EIDs when the file has been indexed

#         Parameters
#         ----------
#         eids : list of int
#             Specific UKBB sample IDs that are required. If `NoneType` then all
#             sample IDs are required

#         Yields
#         ------
#         row : list of str
#             A row that has been read with `csv.reader`, no type conversion has
#             been applied at this point
#         leading_cols : list or str
#             A list of the non-field ID columns. This will certaily have the
#             `eid` column at [0] and may have the `withdrawn` column at [1]
#         """
#         while True:
#             try:
#                 yield self._fetch_row()
#             except StopIteration:
#                 break

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     def _no_subset(self, row):
#         """
#         A dummy call that performs no column subsetting at all

#         Parameters
#         ----------
#         row : list of str
#             A row that will be passed through the method

#         Returns
#         -------
#         row : list of str
#             A row that has been read with `csv.reader`, no type conversion has
#             been applied at this point
#         """
#         return row

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     def _col_subset(self, row):
#         """
#         Subset specific column indexes from a row. These have been identified
#         and stored in `obj.subset_header_idx`

#         Parameters
#         ----------
#         row : list of str
#             A row that will be subset

#         Returns
#         -------
#         row : list of str
#             A row that has been read with `csv.reader`, no type conversion has
#             been applied at this point. But specific columns have been
#             extracted from the row
#         """
#         return [row[i] for i in self.subset_header_idx]

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     def _parse_row_defined(self, row, eid):
#         """
#         Parse a row of data and keep only the defined values

#         Parameters
#         ----------
#         row : list of str
#             A raw data row that have been read out of the wide file
#         eid : int
#             The sample identifier for the row. This will be embedded into
#             the row. and used to tag bad values

#         Returns
#         -------
#         row : dict
#             A row where the data types have been set and empty data slots
#             have been removed. The keys field IDs with nested dicts of column
#             names (as tuples) and formatted values.
#         """
#         # Will hold the final row of formatted data
#         processed_row = {}

#         # Loop through the row and track the column number (idx) to use to
#         # reference into the header and the data types
#         for idx, i in enumerate(row):
#             # If the row is defined
#             if i.strip() != '':
#                 # Make the key the column name and set the value to
#                 # the correct type with the function call cached in
#                 # split_header
#                 fid = self.subset_header_tuples[idx][0]
#                 n_inst = self.subset_header_tuples[idx][1]
#                 array = self.subset_header_tuples[idx][2]
#                 processed_row.setdefault(fid, {})

#                 try:
#                     processed_row[fid][(fid, n_inst, array)] = \
#                         self._subset_header_parse[idx][1](
#                             i, self._subset_header_parse[idx][0].coding_enum
#                         )
#                 except (ValueError, TypeError) as e:
#                     self._store_bad_value(
#                         eid,
#                         i,
#                         self._subset_header_parse[idx][0].data_type,
#                         fid,
#                         n_inst,
#                         array,
#                         idx+2,
#                         e
#                     )

#         # Add the EID to the row
#         processed_row[dd.FID.EID.value] = eid

#         return processed_row

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     def _store_bad_value(self, eid, value, data_type, field_id, instance,
#                          array, colno, error):
#         """
#         Store values that can't be cast. These could be data issues or issues
#         with the data dictionary

#         Parameters
#         ----------
#         eid : int
#             The sample containing the bad value
#         value : str
#             The value that could not be cast
#         data_type : str
#             The data type that the value should have been
#         field_id : int
#             The field that the value was in
#         instance : int
#             The instance that the value was in
#         array : int
#             The array that the value was in
#         colno : int
#             The column index that the value was in
#         error : :obj:`Exception`
#             The error that was raised when casting. The error message will
#             be logged
#         """
#         self.bad_values.append(
#             (eid, value, data_type, field_id,
#              instance, array, colno, str(error))
#         )

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     def _parse_row(self, row, eid):
#         """
#         Parse a row of data and keep keep all values, setting the missing
#         values to NoneType

#         Parameters
#         ----------
#         row : list of str
#             A raw data row that have been read out of the wide file
#         eid : int
#             The sample identifier for the row. This will be embedded into
#             the row. and used to tag bad values

#         Returns
#         -------
#         row : dict
#             A row where the data types have been set and empty data slots
#             have been set to NoneType. . The keys field IDs with nested
#             dicts of column names (as tuples) and formatted values.
#         """
#         # Will hold the final row of formatted data
#         processed_row = {}

#         # Loop through the row and track the column number (idx) to use to
#         # reference into the header and the data types
#         for idx, i in enumerate(row):
#             # Make the key the column name and set the value to
#             # the correct type with the function call cached in
#             # split_header
#             fid = self.subset_header_tuples[idx][0]
#             n_inst = self.subset_header_tuples[idx][1]
#             array = self.subset_header_tuples[idx][2]
#             processed_row.setdefault(fid, {})

#             # If the row is defined
#             if i.strip() != '':
#                 try:
#                     processed_row[fid][(fid, n_inst, array)] = \
#                         self._subset_header_parse[idx][1](
#                             i, self._subset_header_parse[idx][0].coding_enum
#                         )
#                 except (ValueError, TypeError) as e:
#                     self._store_bad_value(
#                         eid,
#                         i,
#                         self._subset_header_parse[idx][0].data_type,
#                         fid,
#                         n_inst,
#                         array,
#                         idx+2,
#                         e
#                     )
#                     processed_row[fid][(fid, n_inst, array)] = None
#             else:
#                 # Empty values are stored as NoneType
#                 processed_row[fid][(fid, n_inst, array)] = None

#         # Add the EID to the row
#         processed_row[dd.FID.EID.value] = eid

#         return processed_row
