"""
A script to build the data_dict.py module. This is not meant for public use
but is used to write/update the data dictionary moodule as there is a lot of
constants to include in there
"""
from ukbb_tools import __version__, __name__ as pkg_name, \
    ukbb_mapper_orm as orm, config
from simple_progress import progress
from sqlalchemy import and_
from sqlalchemy.orm.exc import NoResultFound
import sys
import os
import re
import argparse
import pprint as pp
import warnings


# Move a load of these to constants
# The prefix for verbose messages and the location to write the messages
MSG_PREFIX = '[info]'
MSG_OUT = sys.stderr


# Link to specific assessment
# 'Population characteristics > Baseline characteristics > Indices of Multiple Deprivation'
BLOOD_SAMPLE_TIME_FIELD = 3166
URINE_SAMPLE_TIME_FIELD = 20035
SALIVA_SAMPLE_TIME_FIELD = 20025
CANCER_DIAGNOSIS_DATE = 40005
ATTEND_ASSESSMENT = 53
DATE_CONSENT = 200


# These will be turned into a dict of field IDs values vs fieldIDs dates
DATE_MAPPINGS_BY_DATA_PATH = {
    ('Additional exposures > Local environment > '
     'Greenspace and coastal proximity'): 22700,
    ('Additional exposures > Local environment'
     '> Home locations'): 22700,
    ('Additional exposures > Local environment >'
     ' Residential air pollution'): 22700,
    ('Additional exposures > Local environment >'
     ' Residential noise pollution'): 22700,
    ('Additional exposures > '
     'Physical activity measurement'): 110006,
    ('Additional exposures > Physical activity measurement'
     ' > Acceleration averages'): 90010,
    ('Additional exposures > Physical activity measurement'
     ' > Acceleration intensity distribution'): 90010,
    ('Additional exposures > Physical activity measurement'
     ' > Accelerometer calibration'): 90010,
    ('Additional exposures > Physical activity measurement'
     ' > Accelerometer wear time duration'): 90010,
    ('Additional exposures > Physical activity measurement'
     ' > Raw accelerometer statistics'): 90010,
    ('Biological samples > Assay results > Blood assays'
     ' > Blood biochemistry'): BLOOD_SAMPLE_TIME_FIELD,
    ('Biological samples > Assay results > Blood assays'
     ' > Blood count'): BLOOD_SAMPLE_TIME_FIELD,
    ('Biological samples > Sample inventory > '
     'Blood sample inventory'): BLOOD_SAMPLE_TIME_FIELD,
    ('Biological samples > Sample inventory > '
     'Saliva sample inventory'): SALIVA_SAMPLE_TIME_FIELD,
    ('Biological samples > Sample inventory > '
     'Urine sample inventory'): URINE_SAMPLE_TIME_FIELD,
    ('Online follow-up > Diet by 24-hour recall'): 110002,
    ('Online follow-up > Digestive health'): 21023,
    ('Online follow-up > Food (and other) preferences'): 20750,
    ('Online follow-up > Work environment'): 22500,
    ('Online follow-up > Work environment > Employment history'): 22500,
    ('Online follow-up > Mental health'): 20400,
    ('Online follow-up > Work environment'):  22500,
    ('Biological samples > Assay results > Blood assays > '
     'Infectious Diseases'): BLOOD_SAMPLE_TIME_FIELD,
    ('Biological samples > Assay results > Blood assays > Infectious '
     'Diseases > Infectious Disease Antigens'): BLOOD_SAMPLE_TIME_FIELD,
    "Online follow-up > Cognitive function online": 20140,
    "Online follow-up > Cognitive function online > Fluid intelligence / reasoning": 20135,
    "Online follow-up > Cognitive function online > Mood": 23079,
    "Online follow-up > Cognitive function online > Numeric memory": 20138,
    "Online follow-up > Cognitive function online > Pairs matching": 20134,
    "Online follow-up > Cognitive function online > Symbol digit substitution": 20137,
    "Online follow-up > Cognitive function online > Trail making": 20136,
    "Online follow-up > Diet by 24-hour recall": 110002,
    "Online follow-up > Digestive health": 21023,
    "Online follow-up > Food (and other) preferences": 20750
}


DATE_MAPPINGS_UNDER_DATA_PATH = {
    'Online follow-up > Diet by 24-hour recall': 105010,
    'Online follow-up > Mental health': 20400,
    'Online follow-up > Work environment':  22500,
    'Population characteristics > Baseline characteristics': ATTEND_ASSESSMENT,
    'UK Biobank Assessment Centre': ATTEND_ASSESSMENT
}


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main():
    """
    The main entry point to build the data dictionary module
    """
    # Initialise and parse the command line arguments
    parser = init_cmd_args()
    args = parse_cmd_args(parser)

    # Start a msg outputter, this will respect the verbosity and will output to
    # STDERR
    m = progress.Msg(prefix='', verbose=args.verbose, file=MSG_OUT)
    m.msg("= UKBB data dict ({0} v{1}) =".format(pkg_name, __version__))
    m.prefix = MSG_PREFIX
    m.msg_atts(args)

    # setup access to the config file and the mapping database with the codings
    # and the data dictionary. queries against these will be used to build the
    # module contents
    config_obj, ukbb_map_sm = initialise(args.config_file)

    # First we initialise all the field to date field mappings
    date_mappings = init_date_mappings(ukbb_map_sm)
    # pp.pprint(date_mappings)
    # print(len(date_mappings))

    # The output file is the final module, do not output directly into the
    # the package
    outfobj = open(args.outfile, 'wt')

    # Now build the module contents
    try:
        build_data_dict_module(outfobj, ukbb_map_sm, date_mappings)
    except Exception:
        # Remove the output file if it all goes wrong
        outfobj.close()
        os.unlink(args.outfile)
        raise

    m.msg("*** END ***")


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def init_cmd_args():
    """
    Initialise the command line arguments and return the parsed arguments

    Returns
    -------
    parser : :obj:`argparse.ArgumentParser`
        The argument parser with the arguments set up
    """
    parser = argparse.ArgumentParser(
        description="build a data_dict.py module based on the current data"
        " dictionary and the current contents of data_dict.py")

    # The wide format UKBB data fields filex
    parser.add_argument('outfile', type=str,
                        help="The output file containing the module contents."
                        " Do not output directly to the module file.")
    parser.add_argument('--config-file', type=str,
                        help="An alternative path to the config file")
    parser.add_argument('-v', '--verbose',  action="store_true",
                        help="give more output")

    return parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_cmd_args(parser):
    """
    Parse the command line arguments

    Parameters
    ----------
    parser : :obj:`argparse.ArgumentParser`
        The argument parser with the arguments set up

    Returns
    -------
    args : :obj:`argparse.Namespace`
        The argparse namespace object
    """
    args = parser.parse_args()
    return args


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def initialise(config_file):
    """
    Initialise the configuration file parsing and connection to the mapping
    database containing the data dictionary

    Parameters
    ----------
    config_file : str
        The path to the configuration file to parse

    Returns
    -------
    config_obj : :obj:`configparser`
        Access to the config file
    ukbb_map_sm : :obj:`sqlalchemy.Sessionmaker`
        An SQLAlchemy session maker to provide a connection to the mapping
        database containing the data dictionary
    """
    # attempt to loacate the path to the config file, if successful then make
    # sure the permissions are set correcly (only user readable). Finally, if
    # all ok then read in the config file
    config_file = config.get_config_file(config_file=config_file)
    config.check_config_permissions(config_file)
    config_obj = config.read_config_file(config_file)

    # Get the connection to the UKBB mapping database that we want to build
    # eventually I want this to error out if it exists - as we should be
    # building from scratch
    ukbb_map_sm = config.get_ukbb_mapping_connection(config_obj,
                                                     must_exist=True)

    return config_obj, ukbb_map_sm


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def init_date_mappings(ukbb_map_sm):
    """

    """
    # Get date assignments based on date field that are applicaable to whole
    # data paths
    date_map = fetch_data_path_mappings(ukbb_map_sm)
    date_map = {**date_map, **(fetch_below_data_path_mappings(ukbb_map_sm))}
    date_map = {**date_map,
                **(assign_dates_to_blood_biochemistry_measures(ukbb_map_sm))}
    date_map = {**date_map,
                **(assign_dates_to_blood_count_measures(ukbb_map_sm))}
    date_map = {**date_map,
                **(assign_dates_to_blood_infection_measures())}
    date_map = {**date_map,
                **(assign_dates_to_urine_biochemistry_measures(ukbb_map_sm))}
    date_map = {**date_map,
                **(assign_dates_to_urine_biochemistry_measures(ukbb_map_sm))}
    date_map = {**date_map,
                **(assign_dates_to_cancer_registry())}
    date_map = {**date_map,
                **(assign_dates_to_death_registry())}
    date_map = {**date_map,
                **(assign_dates_to_algorithm_diseases(ukbb_map_sm))}
    date_map = {**date_map,
                **(assign_dates_to_first_occurences(ukbb_map_sm))}
    date_map = {**date_map,
                **(assign_dates_to_dates(ukbb_map_sm))}
    return date_map


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def assign_dates_to_dates(ukbb_map_sm):
    """
    Make sure all date/time fields are annotated with their respective date/time
    """
    date_map = {}
    session = ukbb_map_sm()
    sql = session.query(orm.UkbbDataDict.field_id).\
        filter(orm.UkbbDataDict.data_type.in_(['Date', 'Time']))

    for i in sql:
        date_map[i.field_id] = i.field_id

    session.close()
    return date_map


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def fetch_data_path_mappings(ukbb_map_sm):
    """
    Get date assignments based on date field that are applicaable to whole
    data paths. This utilises a module-level defined dictionary of paths to
    date fields

    Parameters
    ----------
    ukbb_map_sm : :obj:`sqlalchemy.Sessionmaker`
        An SQLAlchemy session maker to provide a connection to the mapping
        database containing the data dictionary
    """
    date_map = {}
    session = ukbb_map_sm()

    for data_path, date_field in DATE_MAPPINGS_BY_DATA_PATH.items():
        _check_date_field(session, date_field)
        sql = session.query(orm.UkbbDataDict.field_id).filter(
            orm.UkbbDataDict.data_path == data_path
        )
        for fid in sql:
            date_map[fid.field_id] = date_field
    session.close()
    return date_map


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def fetch_below_data_path_mappings(ukbb_map_sm):
    """
    Get date assignments based on date field that are applicaable to whole
    data paths that are children of a data_path. This is not inclusive of that
    data path. This utilises a module-level defined dictionary of parent paths
    to date fields

    Parameters
    ----------
    ukbb_map_sm : :obj:`sqlalchemy.Sessionmaker`
        An SQLAlchemy session maker to provide a connection to the mapping
        database containing the data dictionary
    """
    date_map = {}
    session = ukbb_map_sm()

    for data_path, date_field in DATE_MAPPINGS_UNDER_DATA_PATH.items():
        _check_date_field(session, date_field)

        sql = session.query(orm.UkbbDataDict.data_path,
                            orm.UkbbDataDict.field_id).filter(
            orm.UkbbDataDict.data_path.like("{0}%".format(data_path))
        )
        for result in sql:
            # This is not inclusive so we want to ignore the exact match to the
            # parent path
            if result.data_path != data_path:
                date_map[result.field_id] = date_field
    session.close()
    return date_map


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _check_date_field(session, field_id):
    """
    Make sure the field ID is a date field
    """
    try:
        date_check = session.query(orm.UkbbDataDict).\
            filter(orm.UkbbDataDict.field_id == field_id).one()
    except NoResultFound as e:
        raise ValueError(
            "no field ID found for '{0}'".format(field_id)) from e

    if date_check.data_type != 'Date' and date_check.data_type != 'Time':
        raise ValueError("'{0}' not a date/time field".format(field_id))


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def assign_dates_to_blood_biochemistry_measures(ukbb_map_sm):
    """
    Get date assignments for the blood biochemistry processing fields, note
    that there are different to the actual results that will be referenced to
    when the blood was taken

    Parameters
    ----------
    ukbb_map_sm : :obj:`sqlalchemy.Sessionmaker`
        An SQLAlchemy session maker to provide a connection to the mapping
        database containing the data dictionary
    """
    results_path = ("Biological samples > Assay results > Blood assays"
                    " > Blood biochemistry")
    measures_path = ("Biological samples > Assay results > Blood assays > "
                     "Blood biochemistry > Blood biochemistry processing")

    return _assign_dates_to_measures_based_on_results(
        ukbb_map_sm, results_path, measures_path)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def assign_dates_to_blood_count_measures(ukbb_map_sm):
    """
    Get date assignments

    Parameters
    ----------
    ukbb_map_sm : :obj:`sqlalchemy.Sessionmaker`
        An SQLAlchemy session maker to provide a connection to the mapping
        database containing the data dictionary
    """
    results_path = ("Biological samples > Assay results > Blood assays > "
                    "Blood count")
    measures_path = ("Biological samples > Assay results > Blood assays > "
                     "Blood count > Blood count processing")

    return _assign_dates_to_measures_based_on_results(
        ukbb_map_sm, results_path, measures_path, error=False)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _assign_dates_to_measures_based_on_results(ukbb_map_sm, results_path,
                                               assay_path, error=True):
    """
    This runs a small algorithem that will use the results to derive the
    names of what has been measured (results_path) and then use those names to
    find all the assay information for that result in the (measures_path). The
    date is then located in the measures path and applied to all the measures
    (but not the results - as these are dated to blood collection)

    Parameters
    ----------
    ukbb_map_sm : :obj:`sqlalchemy.Sessionmaker`
        An SQLAlchemy session maker to provide a connection to the mapping
        database containing the data dictionary
    results_path : str
        The path to where the result measures are located, this is a string
        representation of a hierarchical structure in UKBB
    assay_path : str
        The path to where the assay data are located, this is a string
        representation of a hierarchical structure in UKBB

    Returns
    -------
    date_map : dict
        The keys are `field_ids (int) and the values are the `field_id` of the
        most relevant date for it

    Raises
    ------
    ValueError
        If the query for assay information does not produce any results or
        if the assay information contains more that one field which is a
        date or time data_type
    """
    date_map = {}

    # Get the descriptions of the biochemical measures
    session = ukbb_map_sm()
    sql = session.query(orm.UkbbDataDict.field_desc).\
        filter(orm.UkbbDataDict.data_path == results_path)
    for i in sql.all():
        meas_sql = session.query(orm.UkbbDataDict).\
            filter(and_(orm.UkbbDataDict.data_path == assay_path,
                        orm.UkbbDataDict.field_desc.like(
                            "{0}%".format(i.field_desc))))
        all_measures = meas_sql.all()
        if len(all_measures) == 0 and error is True:
            raise ValueError("no measures found for '{0}'".format(i.field_desc))
        elif len(all_measures) == 0 and error is False:
            warnings.warn("no measures found for '{0}'".format(i.field_desc))
            continue

        try:
            date_measures = find_date_fields(all_measures)
        except KeyError as e:
            if error is True:
                raise KeyError("using '{0}': ".format(
                    i.field_desc, str(e))) from e
            else:
                warnings.warn("using '{0}': ".format(
                    i.field_desc, str(e)))
                continue

        if len(date_measures) != 1 and error is True:
            raise ValueError(
                "expected only 1 Date field, got '{0}' for "
                "'{1}'".format(len(date_measures), i.field_desc)
            )
        elif len(date_measures) != 1 and error is False:
            warnings.warn(
                "expected only 1 Date field, got '{0}' for "
                "'{1}'".format(len(date_measures), i.field_desc)
            )
            continue

        for m in all_measures:
            date_map[m.field_id] = date_measures[0].field_id

    session.close()
    return date_map


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def find_date_fields(fields):
    """
    Locate all date fields in a bunch of fields

    Parameters
    ----------
    fields : list of `ukbb_mapper_orm.UkbbDataDict`
        database objects to test

    Returns
    -------
    date_fields : list of `ukbb_mapper_orm.UkbbDataDict`
        Database objects that have a Date or Time data_type annotation

    Raises
    ------
    KeyError
        If no database objects with a date or time annotation are found
    """
    date_fields = []

    for i in fields:
        if i.data_type == 'Date' or i.data_type == 'Time':
            date_fields.append(i)

    if len(date_fields) == 0:
        raise KeyError("no Date or Time data_types found in the data")

    return date_fields


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def assign_dates_to_blood_infection_measures():
    """
    Get date assignments for a selected infection measure

    Parameters
    ----------
    ukbb_map_sm : :obj:`sqlalchemy.Sessionmaker`
        An SQLAlchemy session maker to provide a connection to the mapping
        database containing the data dictionary
    """
    date_map = {23048: 23049}
    return date_map


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def assign_dates_to_cancer_registry():
    """
    Get date assignments for a cancer registry data
    """
    date_map = {40006: CANCER_DIAGNOSIS_DATE,
                40008: CANCER_DIAGNOSIS_DATE,
                40009: CANCER_DIAGNOSIS_DATE,
                40011: CANCER_DIAGNOSIS_DATE,
                40013: CANCER_DIAGNOSIS_DATE,
                40019: CANCER_DIAGNOSIS_DATE,
                40021: CANCER_DIAGNOSIS_DATE}
    return date_map


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def assign_dates_to_death_registry():
    """
    Get date assignments for a cancer registry data
    """
    DATE_OF_DEATH = 40000
    date_map = {40001: DATE_OF_DEATH,
                40002: DATE_OF_DEATH,
                40007: DATE_OF_DEATH,
                40010: DATE_OF_DEATH,
                40018: DATE_OF_DEATH,
                40020: DATE_OF_DEATH}
    return date_map


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def assign_dates_to_algorithm_diseases(ukbb_map_sm):
    """

    """
    data_path = 'Health-related outcomes > Algorithmically-defined outcomes%'
    session = ukbb_map_sm()

    data = session.query(orm.UkbbDataDict).\
        filter(orm.UkbbDataDict.data_path.like(data_path)).\
        order_by(orm.UkbbDataDict.field_id).all()

    if len(data) == 0:
        raise ValueError("no data")
    date_map = _assign_stagered_date_fields(data)
    session.close()
    return date_map


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def assign_dates_to_first_occurences(ukbb_map_sm):
    """

    """
    data_path = 'Health-related outcomes > First occurrences%'
    session = ukbb_map_sm()

    data = session.query(orm.UkbbDataDict).\
        filter(orm.UkbbDataDict.data_path.like(data_path)).\
        order_by(orm.UkbbDataDict.field_id).all()

    if len(data) == 0:
        raise ValueError("no data")
    date_map = _assign_stagered_date_fields(data)
    session.close()
    return date_map


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _assign_stagered_date_fields(data):
    """

    """
    # print(data)
    date_map = {}
    for i in range(0, len(data), 2):
        if data[0].data_type != 'Date':
            raise ValueError("expecting lead element to be a date")
        date_map[data[i+1].field_id] = data[i].field_id
    return date_map


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def assign_dates_to_urine_biochemistry_measures(ukbb_map_sm, error=True):
    """
    Get date assignments

    Parameters
    ----------
    ukbb_map_sm : :obj:`sqlalchemy.Sessionmaker`
        An SQLAlchemy session maker to provide a connection to the mapping
        database containing the data dictionary
    """
    # *
    # 'Biological samples > Assay results > Urine assays'
    # 'Biological samples > Assay results > Urine assays > Urine processing'
    # *
    date_map = {}
    results_path = 'Biological samples > Assay results > Urine assays'
    assay_path = 'Biological samples > Assay results > Urine assays%'
    # Get the descriptions of the biochemical measures
    session = ukbb_map_sm()
    sql = session.query(orm.UkbbDataDict.field_id,
                        orm.UkbbDataDict.field_desc).\
        filter(and_(orm.UkbbDataDict.data_path == results_path,
                    orm.UkbbDataDict.data_type == 'Continuous'))

    for i in sql.all():
        meas_sql = session.query(orm.UkbbDataDict).\
            filter(and_(orm.UkbbDataDict.data_path.like(assay_path),
                        orm.UkbbDataDict.field_desc.like(
                            "{0}%".format(i.field_desc))))

        all_measures = meas_sql.all()
        if len(all_measures) == 0 and error is True:
            raise ValueError("no measures found for '{0}'".format(i.field_desc))
        elif len(all_measures) == 0 and error is False:
            warnings.warn("no measures found for '{0}'".format(i.field_desc))
            continue

        try:
            date_measures = find_date_fields(all_measures)
        except KeyError as e:
            if error is True:
                raise KeyError("using '{0}': ".format(
                    i.field_desc, str(e))) from e
            else:
                warnings.warn("using '{0}': ".format(
                    i.field_desc, str(e)))
                continue

        if len(date_measures) != 1 and error is True:
            raise ValueError(
                "expected only 1 Date field, got '{0}' for "
                "'{1}'".format(len(date_measures), i.field_desc)
            )
        elif len(date_measures) != 1 and error is False:
            warnings.warn(
                "expected only 1 Date field, got '{0}' for "
                "'{1}'".format(len(date_measures), i.field_desc)
            )
            continue

        for m in all_measures:
            # Some of the fields will be result field
            if m.field_id != i.field_id:
                date_map[m.field_id] = date_measures[0].field_id
            else:
                # result measures get annotated with the urine sample
                # time field
                date_map[m.field_id] = URINE_SAMPLE_TIME_FIELD

    session.close()
    return date_map


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def build_data_dict_module(outfobj, ukbb_map_sm, date_mappings):
    """
    Build the data dictionary module from the database

    Parameters
    ----------
    outfobj : :obj:`File`
        A file object to write to
    ukbb_map_sm : :obj:`sqlalchemy.Sessionmaker`
        The session maker for the connection to the UKBB mapping database that
        is created with `build_ukbb_mapper`
    date_mappings : dict
        The assigned mappings between field IDs and the most relevant date
        field_ids, these will be incorporated into the FieldId named tuples
        in the overall data dictionary
    """
    # Get and cache all the codings from the database, codings is a dict with
    # the integer coding _id as keys and lists containing the codings
    # enumeration database objects as values
    codings = cache_codings(ukbb_map_sm)

    # Cache the data dictionary in two different formats, one with the
    # field_ids as keys and the database objects as values. The other with the
    # coding_id as a key and lists of data dictionary database objects as
    # values
    data_dict_fids, data_dict_codings = cache_data_dict(ukbb_map_sm)

    # write any imports that need to be made
    build_imports(outfobj)

    # Build the named tuple definition into the file as it will be used to hold
    # the field IDs
    build_namedtuple(outfobj)

    # Now build all the code for the enumeration objects in the data dictionary
    build_enums(outfobj, codings, data_dict_codings)

    # Build all the code for the field ID named tuples (link to the enums)
    # and also date linked
    build_fid_dict(outfobj, data_dict_fids, date_mappings)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def build_imports(outfobj):
    """
    Write the import statements to the file

    Parameters
    ----------
    outfobj : :obj:`File`
        A file object to write to
    """
    docstr = ('"""\nThis module is programmatically generated with '
              'data_dict_builder.py.\nAny bugs should be fixed in '
              'data_dict_builder.py and this file should\nbe rebuilt\n"""\n\n')
    outfobj.write(docstr)
    outfobj.write("from ukbb_tools import field_id_parsers\n")
    outfobj.write("from collections import namedtuple\n")
    outfobj.write("from enum import Enum\n\n")


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def build_namedtuple(outfobj):
    """
    Write the namedtuple definition to the file

    Parameters
    ----------
    outfobj : :obj:`File`
        A file object to write to
    """
    outfobj.write("FieldId = namedtuple(\n"
                  "    'FieldId',\n"
                  "    ['field_id', 'field_desc', 'data_type', 'data_units',\n"
                  "     'n_instances', 'array', 'coding_id', 'coding_enum',\n"
                  "     'parse_func', 'date_link']"
                  ")\n\n")


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def build_enums(outfobj, codings, data_dict_codings):
    """
    Write the Enum code of the data dictionary to file

    Parameters
    ----------
    outfobj : :obj:`File`
        A file object to write to
    codings : dict
        The keys are coding_ids and the values are lists of database coding
        objects, that will provide the data for the enum code
    data_dict_codings : dict
        The keys are coding_ids and the values are lists of data base data
        dictionary fields that are used to define a data type that is then used
        to ensure that the enum values are expressed with the same data type
    """
    # Will hold a list of all the built enumeration code with each element
    # containing code for 1 enum
    all_codings = []

    # Loop through all the coding_id and there catagories
    for coding_id, entries in codings.items():
        # Get the data type, we need to check that the fields that use the
        # same coding ID are not mapped to different data types i.e. a
        # string and integer. As we will be expressing the Enums in the
        # same data type as the field ID, so that field ID data can be
        # placed straight in them. In most instances they will be strings,
        # however, there are some situations where the data type is an int
        # or float but also has an Enum present that means MACHINE ERROR
        # or something along those lines.
        # During testing none of these were found so we should be good but
        # I will leave it in for future updates
        all_data_types = set()
        try:
            for dd in data_dict_codings[coding_id]:
                # Either single or multiple categoricals are treated as the
                # same data type
                data_type = re.sub(
                    r'(Categorical) (single|multiple)',
                    r'\\1',
                    dd.data_type,
                    re.IGNORECASE
                )
                all_data_types.add(data_type)

            if len(all_data_types) > 1:
                raise TypeError("mixed data types in field_ids with coding_id:"
                                " {0} ({1})".format(coding_id, all_data_types))
            data_type = list(all_data_types)[0]
        except KeyError:
            # There is no coding_id associated with the field so it is probably
            # a straight int or float etc...
            data_type = ''

        # Build the Enum to the same format as the data
        if data_type == 'Continuous' or data_type == 'Integer':
            enum_str = build_enum(coding_id, entries,
                                  as_int=True)
        else:
            enum_str = build_enum(coding_id, entries,
                                  as_int=False)

        all_codings.append(enum_str)

    # Write all the Enums to file
    outfobj.write('CODINGS = {{\n{0}\n}}\n\n'.format(",\n".join(all_codings)))


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def build_enum(coding_id, entries, as_int=False):
    """
    Build the actual Enum code entry in a dict

    Parameters
    ----------
    coding_id : int
        The coding ID of the Enum
    entries
        Database objects representing the values the Enum can take
    as_int : bool, optional, default: False
        Shall the Enum values be treated as integers (True) or strings (False).
        This boils down to shall the values be quoted in the code

    Returns
    -------
    enum_code : str
        A string of a python Enum definition within a larger dictionary
    """
    # The class of the Enum that will be writen to file
    class_name = "Coding{0}".format(coding_id)

    # initialise as an int
    quote = ""
    if as_int is False:
        # as_int is False so change to a " quote
        quote = "'"

    # Now build a key value pair dict for each Enum value. The Enum
    # descriptions, are taken from the database descriptions and made uppercase
    # with all non-word characters removed and replaced with _ . All spaces are
    # replaced with an underscore
    key_values = []
    for e in entries:
        key = e.desc.strip()
        key = re.sub(r'[\W ]+', '_', e.desc).upper()
        key = key.strip('_')
        key_values.append("'{0}': {2}{1}{2}".format(key, e.code_enum, quote))

    return ("    {0}: Enum(\n        '{1}',\n        {{\n            {2}\n"
            "        }})".format(
                coding_id,
                class_name,
                ',\n            '.join(key_values)
            ))


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def build_fid_dict(outfobj, data_dict_fids, date_mappings):
    """
    Now build the data dictionary named tuples. These describe the field data
    and are associated with parsing functions and where necessary specific
    Enums

    Parameters
    ----------
    outfobj : :obj:`File`
        A file object to write to
    data_dict_fids : dict
        The fields that need to be represented with the field_id as keys and
        a database object as the value. The database object has all the field
        data within it
    """
    all_field_ids = []
    for field_id, entry in data_dict_fids.items():
        try:
            date_fid = date_mappings[field_id]
        except KeyError:
            date_fid = None

        fid = ("    {0}: FieldId(\n"
               "        field_id={1},\n"
               "        field_desc={2},\n"
               "        data_type={3},\n"
               "        data_units={4},\n"
               "        n_instances={5},\n"
               "        array={6},\n"
               "        coding_id={7},\n"
               "        coding_enum={8},\n"
               "        parse_func={9},\n"
               "        date_link={10})".format(
                   field_id,
                   entry.field_id,
                   format_str(entry.field_desc),
                   format_data_type(entry.data_type),
                   format_str(entry.data_units),
                   format_numeric(entry.n_instances),
                   format_numeric(entry.array),
                   format_numeric(entry.coding_id),
                   format_coding_enum(entry.coding_id),
                   format_parse_func(entry.data_type),
                   date_fid))
        all_field_ids.append(fid)

    outfobj.write('FIELD_IDS = {{\n{0}\n}}\n\n'.format(
        ",\n".join(all_field_ids)))


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def format_parse_func(data_type):
    """
    Generate the correct parse function call depending on the data_type

    Parameters
    ----------
    data_type : str
        Data type of the field

    Returns
    -------
    func_call : str
        The function call string to insert into the named tuple code
    """
    if data_type == "Integer":
        return "field_id_parsers.parse_int"
    elif data_type == "Continuous":
        return "field_id_parsers.parse_float"
    elif data_type == "Date" or data_type == "Time":
        return "field_id_parsers.parse_date"
    elif data_type == "Categorical single" or data_type == "Categorical multiple":
        return "field_id_parsers.parse_enum"
    elif data_type == "Compound" or data_type == "Text":
        return "field_id_parsers.parse_str"
    else:
        raise ValueError("unknown data type: '{0}'".format(data_type))


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def format_coding_enum(coding_id):
    """
    Format the variable name of the coding enumerator or NoneType if no
    coding_id exists (is NoneType)

    Parameters
    ----------
    coding_id : int or NoneType
        The coding ID if the field has one, if not it will be `NoneType`
    """
    try:
        return format_none(coding_id)
    except TypeError:
        return 'CODINGS[{0}]'.format(coding_id)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def format_data_type(data_type):
    """

    """
    try:
        return format_none(data_type)
    except TypeError:
        return "'{0}'".format(re.sub(r'\s+', '_', data_type.lower()))


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def format_str(value):
    """

    """
    try:
        return format_none(value)
    except TypeError:
        value = re.sub(r'["\']+', '', value)
        return "'{0}'".format(value)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def format_numeric(value):
    """

    """
    try:
        return format_none(value)
    except TypeError:
        return str(value)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def format_none(value):
    """

    """
    if value is not None:
        raise TypeError("needs NoneType")
    return str(value)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_key(entry):
    """

    """
    return re.sub(r'[\W ]+', '_', entry.desc).upper()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def cache_codings(ukbb_map_sm):
    """
    Read all the codings from the database into memory
    """
    session = ukbb_map_sm()

    sql = session.query(orm.UkbbDataEnumCodes)

    codings = {}
    for i in sql:
        try:
            codings[i.coding_id].append(i)
        except KeyError:
            codings[i.coding_id] = []
            codings[i.coding_id].append(i)

    session.close()
    return codings


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def cache_data_dict(ukbb_map_sm):
    """
    Read all the codings from the database into memory
    """
    session = ukbb_map_sm()

    sql = session.query(orm.UkbbDataDict)

    codings = {}
    field_ids = {}
    for i in sql:
        try:
            codings[i.coding_id].append(i)
        except KeyError:
            codings[i.coding_id] = []
            codings[i.coding_id].append(i)
        field_ids[i.field_id] = i

    session.close()
    return field_ids, codings


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == '__main__':
    pass
