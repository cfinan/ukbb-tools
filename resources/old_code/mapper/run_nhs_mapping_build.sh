#!/bin/bash
# rm /scratch/ukbb/mapping_files/nhs_mapping.db
ukbb_mapper_db_builder -v \
                       -c ~/.ukbb_nhs.cnf \
                       -n /scratch/ukbb/mapping_files/processed_files/nhs_digital_mappings/ \
                       -k /scratch/ukbb/mapping_files/processed_files/ukbb_mappings/ \
                       -u /scratch/ukbb/mapping_files/processed_files/ukbb_code_pool/ukbb_index_codes.txt.gz \
                       /scratch/ukbb/mapping_files/source_files/ukbb_mapping_files/all_lkps_maps.xlsx \
                       /scratch/ukbb/mapping_files/source_files/ukbb_mapping_files/coding259.tsv \
                       /scratch/ukbb/mapping_files/source_files/ukbb_mapping_files/coding240.tsv
