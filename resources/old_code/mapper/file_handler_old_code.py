# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class BaseFileHandler(object):
    """
    The base class for UKBB file handling - do not call directly
    """
    DATE_FORMATS = []
    HEADER = []
    CLINICAL_CODES = []
    INT_COLS = []
    INT_COLS_OPT = []
    FLOAT_COLS = []
    FLOAT_COLS_OPT = []
    DATE_COLS = []
    DATE_COLS_OPT = []
    BOOL_COLS = []
    BOOL_COLS_OPT = []

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @classmethod
    def extract_date(cls, date_string):
        """
        process a date string into a date object with respect to the required
        date formats in the file
        """
        error = None
        for i in cls.DATE_FORMATS:
            try:
                return process_date(datetime.strptime(date_string, i))
            except ValueError as e:
                error = e
            except TypeError as e:
                error = e
                break

        raise error.__class__("unknown date type: '{0}'".format(date_string)) from error

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @classmethod
    def get_clinical_code(cls, row):
        """
        Extract the clinical code

        Parameters
        ----------
        row : list or dict
            A single data row to extract all the clinical codes from
        """
        codes = {}
        values = []
        for key, idx, ret_key in cls.CLINICAL_CODES:
            # first treat as a dict
            try:
                try:
                    code = row[key]
                    len(code)
                    codes[ret_key] = code
                except TypeError:
                    # It is a list
                    code = row[idx]
                    len(code)
                    codes[ret_key] = code
                except KeyError:
                    pass
            except TypeError:
                # The code is NoneType so we skip
                pass

        return codes, values

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @classmethod
    def get_column_index(cls, col_names):
        """
        Return the the indexes for `ColName` names tuples

        Parameters
        ----------
        col_names : list of :obj:`ColName`
            The column names that we want to find the indexes for

        Returns
        -------
        col_name_idx : list of int
            The indexes of the column names

        Raises
        ------
        ValueError
            If any of the `ColName` can't be found in the header
        """
        if isinstance(col_names, ukbbc.ColName):
            return cls.HEADER.index(col_names.name)
        elif isinstance(col_names, str):
            return cls.HEADER.index(col_names)

        return [cls.HEADER.index(i.name) for i in col_names]

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, infile, return_dict=False, delimiter=None):
        """
        Parameters
        ----------
        infile : str
            The input file path
        return_dict : bool, optional, default: False
            Should returned rows be retuned as a dictionary
        delimiter : str, optional, default: NoneType
            The delimiter of the file, if NoneType then the delimiter is
            detected with `csv.Sniffer`. However, it can be specifed should
            sniffing not work
        """
        # Code clarity
        cls = self.__class__

        self._infile = os.path.expanduser(infile)
        self._delimiter = delimiter
        self._return_dict = return_dict

        self._fobj = None
        self._reader = None
        self._header = []
        self._header_len = 0
        self._line_no = 0
        self._is_open = False
        self._using_context_manager = False
        self._is_iterating = False

        self._row_process_method = self._row_as_list
        if self._return_dict is True:
            self._row_process_method = self._row_as_dict

        # Generate column indexes for the ins, flats and dates and
        # their optional couterparts
        self.int_idx = cls.get_column_index(cls.INT_COLS)
        self.int_idx_opt = cls.get_column_index(cls.INT_COLS_OPT)
        self.float_idx = cls.get_column_index(cls.FLOAT_COLS)
        self.float_idx_opt = cls.get_column_index(cls.FLOAT_COLS_OPT)
        self.date_idx = cls.get_column_index(cls.DATE_COLS)
        self.date_idx_opt = cls.get_column_index(cls.DATE_COLS_OPT)
        self.bool_idx = cls.get_column_index(cls.BOOL_COLS)
        self.bool_idx_opt = cls.get_column_index(cls.BOOL_COLS_OPT)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __enter__(self):
        """
        Entry point for the context manager
        """
        self.open()
        self._using_context_manager = True
        return self

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __exit__(self, *args):
        """
        Entry point for the context manager

        *args
            Error arguments should the context manager exit in discrace
        """
        self.close()
        self._using_context_manager = False

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __iter__(self):
        """
        Initialise the iterator
        """
        if self._is_open is False:
            self.open()

        # make sure we are at the start
        self._is_iterating = True
        return self

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __next__(self):
        """
        The next row in the file
        """
        if self._is_open is False:
            self.open()
        self._is_iterating = True
        return self._next_row()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def open(self):
        """
        Open the file. Note that this is agnostic for gzipped files
        """
        if self._is_open is True:
            raise IOError('file already open')

        csv_args = {}
        self._fobj = None

        if self._delimiter is not None:
            csv_args['delimiter'] = self._delimiter
        else:
            # First sniff the file handle
            csv_args['dialect'] = file_helper.get_dialect(self._infile)

        self._fobj = gzopen.gzip_fh(self._infile)
        self._reader = csv.reader(self._fobj, **csv_args)
        self._process_header()
        self._is_open = True

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def close(self):
        """
        Close the file
        """
        if self._is_open is False and self._using_context_manager is False:
            raise IOError('file not open')

        self._fobj.close()
        self._is_opened = False
        self._fobj = None
        self._reader = None
        self._header = []
        self._header_len = 0

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _process_header(self):
        """
        Process the header from the file and make sure it is valid

        Raises
        ------
        ValueError
            If the header of the file does not match the expected header
        """
        self._line_no += 1
        self._header = next(self._reader)
        self._header_len = len(self._header)

        if self._header != self.__class__.HEADER:
            raise ValueError(
                "the header in: '{0}' is not the expected format".format(
                    self._infile))

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _next_row(self):
        """
        return the next row in the file
        """
        while True:
            try:
                self._line_no += 1
                return self._row_process_method(
                    self._format_row(next(self._reader)))
            except (IndexError, UnicodeDecodeError) as e:
                warnings.warn("at line '{0}: {1}'".format(self._line_no,
                                                          str(e)))

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _row_as_dict(self, row):
        """
        Make the row into the dictionary

        Parameters
        ----------
        row : list of str
            The data row as read in by `csv.Reader`

        Returns
        -------
        row : dict
            The row formatted as a dict, each key is a column name and each
            value is a data value
        """
        return dict([(col_name, row[idx])
                     for idx, col_name in enumerate(self._header)])

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _row_as_list(self, row):
        """
        Return the row as a list
        """
        return row

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _format_row(self, row):
        """
        Format the row. This checks the row length is what is required and then
        will perform any casts or specific data reformating on the row

        Parameters
        ----------
        row : list of str
            The data row to process

        Raises
        ------
        IndexError
            If the length of the data row is not what is expected
        """
        # pp.pprint(row)
        row = make_none_type(row)

        # Check the row length
        if len(row) != self._header_len:
            raise IndexError(
                "{0}: row has '{0}' columns expected: '{1}'".format(
                    self._line_no,
                    len(row), self._header_len
                )
            )

        # Cast the ints floats and dates, these are not optional casts, so
        # if there are any NoneTypes then we will error out
        self._do_cast(row, self.int_idx, int)
        self._do_cast(row, self.float_idx, float)
        self._do_cast(row, self.date_idx, self.__class__.extract_date)

        self._do_cast(row, self.bool_idx, int)
        self._do_cast(row, self.bool_idx, bool)

        # Cast the optional ints floats and dates if there are any NoneTypes
        # then we do not error out and move silently on
        self._do_cast_opt(row, self.int_idx_opt, int)
        self._do_cast_opt(row, self.float_idx_opt, float)
        self._do_cast_opt(row, self.date_idx_opt, self.__class__.extract_date)

        self._do_cast_opt(row, self.bool_idx_opt, int)
        self._do_cast_opt(row, self.bool_idx_opt, bool)

        return row

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _do_cast(self, row, idx, cast):
        for i in idx:
            try:
                row[i] = cast(row[i])
            except Exception as e:
                raise e.__class__(
                    'data={0} not {4} {1}:{2} :{3}'.format(
                        row[i],
                        self._line_no,
                        self.__class__.HEADER[i],
                        e,
                        cast.__name__
                    )
                ) from e

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _do_cast_opt(self, row, idx, cast):
        for i in idx:
            try:
                row[i] = cast(row[i])
            except TypeError:
                pass
