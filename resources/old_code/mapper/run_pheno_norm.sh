#!/bin/bash
ukbb_pheno_norm -v \
	        --tmp-dir /data/tmp/ \
		--high-mem \
		--config-file ~/.ukbb.cnf \
		--hes-events /data/ukbb/phenotypes/hes/hesin.txt \
		--hes-diag /data/ukbb/phenotypes/hes/hesin_diag.txt \
		--hes-oper /data/ukbb/phenotypes/hes/hesin_oper.txt \
		--gp-clinic /data/ukbb/phenotypes/gp_data/gp_clinical.txt \
		--ukbb-data-fields /data/ukbb/phenotypes/latest.txt \
		--ukbb-sample-file /data/ukbb/ukb12113_imp_chr1_v3_s487324.sample \
		/data/ukbb/processed_phenotypes
