from ukbb_tools import columns as col


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
R2_DRUGS_TO_BNF_READ2_CODE = col.ColName(
    "read_code",
    str,
    "Read V2 code"
)

R2_DRUGS_TO_BNF_BNF_CODE = col.ColName(
    "read_code",
    str,
    "Associated BNF code. For this particular mapping file, the read_code - "
    "bnf_code combination is unique. The bnf_code field contains the code as "
    "a set of 8 (4 pairs) numeric characters, separated by ' . ' e.g. "
    "02.05.01.00."
)
