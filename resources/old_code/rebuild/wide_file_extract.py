"""Extract basic general useful information from the UK BioBak wide phenotype
file
"""
from ukbb_tools import (
    constants as con,
    # data_dict as dd,
    type_casts
)
from ukbb_tools.data_dict import build as dd
from datetime import datetime, timedelta
import pprint as pp


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def blank_vital_stats():
    """
    Return a dictionary with the vital statistics keys set but the values
    set to NoneType

    Returns
    -------
    vital_stats : dict
        A dictionary witht the keys: `age_of_assess`, `age_of_death`,
        `date_of_assessment`, `date_of_birth`, `date_of_death`, `eid`,
        `genetic_sex`, `self_reported_sex` and all values set to `NoneType`
    """
    return {
        con.EID.name: None,
        con.SAM_AGE_OF_ASSESS.name: None,
        con.SAM_AGE_OF_DEATH.name: None,
        con.SAM_DATE_OF_ASSESS.name: None,
        con.SAM_DATE_OF_BIRTH.name: None,
        con.SAM_DATE_OF_DEATH.name: None,
        con.SAM_GEN_SEX.name: None,
        con.SAM_IS_CAUCASIAN.name: None,
        con.SAM_SR_SEX.name: None,
        con.SAM_N_ASSESSMENTS.name: None
    }


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def sample_vital_stats(row):
    """
    Create vital statistics for a sample row. These are values that you
    probably want for each sample when doing an analysis

    Parameters
    ----------
    row : dict of dict
        The keys of the first dictionary are the field IDs and the keys of
        the nested dictionary are tuples of the (field ID n_reps and array)
        (i.e. the content of the column header in the wide file but
        expressed in tuple form of ints

    Returns
    -------
    vital_stats
    """
    vital_stats = {}
    vital_stats[con.EID.name] = get_eid(row)
    vital_stats[con.SAM_HAS_WITHDRAWN.name] = \
        get_withdrawn(row)
    vital_stats[con.SAM_DATE_OF_BIRTH.name] = \
        get_approx_dob(row)
    vital_stats[con.SAM_DATE_OF_ASSESS.name] = \
        get_first_assessment(row)
    vital_stats[con.SAM_N_ASSESSMENTS.name] = \
        len(get_assessments(row))
    vital_stats[con.SAM_AGE_OF_ASSESS.name] = \
        calc_age_of_event(
            vital_stats[con.SAM_DATE_OF_ASSESS.name],
            vital_stats[con.SAM_DATE_OF_BIRTH.name]
        )

    try:
        gen_sex = get_genetic_sex(row)
    except KeyError:
        gen_sex = ''

    vital_stats[con.SAM_GEN_SEX.name] = gen_sex
    vital_stats[con.SAM_SR_SEX.name] = \
        get_sex(row)

    try:
        # The death date is a funny one as it might be None (and filtered)
        # if so then we capture the KeyError and treat as still alive
        dod = get_death_date(row)
        age_of_death = calc_age_of_event(
            dod,
            vital_stats[con.SAM_DATE_OF_BIRTH.name]
        )
    except KeyError:
        dod = None
        age_of_death = None

    vital_stats[con.SAM_DATE_OF_DEATH.name] = dod
    vital_stats[con.SAM_AGE_OF_DEATH.name] = age_of_death

    try:
        is_caucasian = get_is_caucasian(row)
    except KeyError:
        is_caucasian = '0'

    vital_stats[con.SAM_IS_CAUCASIAN.name] = is_caucasian

    try:
        # A dummy call to induce a key error of not present
        get_genotyping_batch(row)
        vital_stats[con.SAM_IS_GENOTYPED.name] = '1'
    except KeyError:
        vital_stats[con.SAM_IS_GENOTYPED.name] = '0'

    return vital_stats


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_eid(row):
    """
    Return the EID (sample ID)

    Parameters
    ----------
    row : dict of dict
        The keys of the first dictionary are the field IDs and the keys of
        the nested dictionary are tuples of the (field ID n_reps and array)
        (i.e. the content of the column header in the wide file but
        expressed in tuple form of ints

    Returns
    -------
    eid : int
        The sample identifier
    """
    return row[dd.VITAL_STATS_FID.EID.value]


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_withdrawn(row):
    """
    Get the withdrawn status if available. This is a special field
    (field ID 0) that is created during basket merges. It might not be
    available in which case everyone is present

    Parameters
    ----------
    row : dict of dict
        The keys of the first dictionary are the field IDs and the keys of
        the nested dictionary are tuples of the (field ID n_reps and array)
        (i.e. the content of the column header in the wide file but
        expressed in tuple form of ints

    Returns
    -------
    withdrawn : bool
        `True` means they have withdrawn and `False` not withdrawn
    """
    try:
        withdrawn = row[dd.VITAL_STATS_FID.SAMPLE_WITHDRAWN.value][
            (dd.VITAL_STATS_FID.SAMPLE_WITHDRAWN.value, 0, 0)
        ]
    except KeyError:
        return False

    return bool(withdrawn)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_assessments(row):
    """
    Return the assessments in the order of first assessment to latest
    assements

    Parameters
    ----------
    row : dict of dict
        The keys of the first dictionary are the field IDs and the keys of
        the nested dictionary are tuples of the (field ID n_reps and array)
        (i.e. the content of the column header in the wide file but
        expressed in tuple form of ints

    Returns
    -------
    vital_stats

    """
    return [row[dd.VITAL_STATS_FID.ASSESSMENT_DATE.value][i] for i in
            sorted(row[dd.VITAL_STATS_FID.ASSESSMENT_DATE.value].keys())]


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_first_assessment(row):
    """
    Return the date of the first assessment. Note this is based on the
    field codes and not the actual date

    Parameters
    ----------
    row : dict of dict
        The keys of the first dictionary are the field IDs and the keys of
        the nested dictionary are tuples of the (field ID n_reps and array)
        (i.e. the content of the column header in the wide file but
        expressed in tuple form of ints

    Returns
    -------
    first_assessement : `type_casts.DateHolder`
        The first assessment date
    """
    return get_assessments(row)[0]


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_last_assessment(row):
    """
    Return the date of the last assessment. Note this is based on the
    field codes and not the actual date

    Parameters
    ----------
    row : dict of dict
        The keys of the first dictionary are the field IDs and the keys of
        the nested dictionary are tuples of the (field ID n_reps and array)
        (i.e. the content of the column header in the wide file but
        expressed in tuple form of ints

    Returns
    -------
    last_assessement : `type_casts.DateHolder`
        The last assessment date
    """
    return get_assessments(row)[-1]


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_approx_dob(row):
    """
    Return the date of birth based on month of birst and year of birth only
    the day of birst is set to the first of the month

    Parameters
    ----------
    row : dict of dict
        The keys of the first dictionary are the field IDs and the keys of
        the nested dictionary are tuples of the (field ID n_reps and array)
        (i.e. the content of the column header in the wide file but
        expressed in tuple form of ints

    Returns
    -------
    approx_dob : `type_casts.DateHolder`
        The approximate date fo birth
    """
    month = get_field_id(
        row,
        dd.VITAL_STATS_FID.MONTH_OF_BIRTH.value)[
            (dd.VITAL_STATS_FID.MONTH_OF_BIRTH.value, 0, 0)
        ]
    year = get_field_id(
        row,
        dd.VITAL_STATS_FID.YEAR_OF_BIRTH.value)[
            (dd.VITAL_STATS_FID.YEAR_OF_BIRTH.value, 0, 0)
        ]

    return type_casts.process_date(datetime(year, int(month.value), 1))


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_death_date(row):
    """
    Return the date of death, if the participant has passed away

    Parameters
    ----------
    row : dict of dict
        The keys of the first dictionary are the field IDs and the keys of
        the nested dictionary are tuples of the (field ID n_reps and array)
        (i.e. the content of the column header in the wide file but
        expressed in tuple form of ints

    Returns
    -------
    date_of_death : `file_handler.DateHolder`
        The date of death

    Raises
    ------
    KeyError
        If the participant has not passed away
    """
    # for some reason there can be multiple death dates (for a few people)
    # so we always return the earliest under the assumption it is the
    # true one
    dod = list(get_field_id(
        row,
        dd.VITAL_STATS_FID.DATE_OF_DEATH.value).values()
    )

    dod.sort(key=lambda x: x.int_date)

    return dod[0]


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_sex(row):
    """

    """
    sex = get_field_id(row, dd.VITAL_STATS_FID.SEX.value)[
        (dd.VITAL_STATS_FID.SEX.value, 0, 0)
    ]
    return sex


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_genotyping_batch(row):
    """
    Return the data for the genotyping batch

    Parameters
    ----------
    row : dict of dict
        The keys of the first dictionary are the field IDs and the keys of
        the nested dictionary are tuples of the (field ID n_reps and array)
        (i.e. the content of the column header in the wide file but
        expressed in tuple form of ints

    Returns
    -------
    genotyping_batch : `data_dict.Coding20000`
        An enumeration of the genotyping batch

    Raises
    ------
    KeyError
        If the participant has not been genotyped
    """
    batch = get_field_id(row, dd.VITAL_STATS_FID.GENOTYPING_BATCH.value)[
        (dd.VITAL_STATS_FID.GENOTYPING_BATCH.value, 0, 0)
    ]
    return batch


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_genetic_sex(row):
    """

    """
    sex = get_field_id(row, dd.VITAL_STATS_FID.GEN_SEX.value)[
        (dd.VITAL_STATS_FID.GEN_SEX.value, 0, 0)
    ]
    # TODO: To be enumerated
    return sex


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_is_caucasian(row):
    """

    """
    return bool(
        int(get_field_id(row, dd.VITAL_STATS_FID.IS_CAUCASIAN.value)
            [(dd.VITAL_STATS_FID.IS_CAUCASIAN.value, 0, 0)].value))


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_field_id(row, fid):
    """
    Extract a specific field ID

    Parameters
    ----------
    row : dict of dict
        The keys of the first dictionary are the field IDs and the keys of
        the nested dictionary are tuples of the (field ID n_reps and array)
        (i.e. the content of the column header in the wide file but
        expressed in tuple form of ints
    fid : int
        The field identifier. Not ethis shoudl be without any replicates
        of array information

    Returns
    -------
    fid_data : dict
        The specific data fields with that field ID. These are a dict with
        (fid, n_replicates, array) tuplies as keys and the field data
        values as values. The values should be cast into the correct data
        version as documented in the data dictionary

    Raises
    ------
    KeyError
        If the fid is not present in the data
    """
    try:
        return row[fid]
    except KeyError as e:
        raise KeyError("field ID '{0}' not in data: do you have access to"
                       " this field?".format(fid)) from e


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def calc_age_of_event(event_date, date_of_birth):
    """
    Calculate the age that an event happened in decimal years

    Parameters
    ----------
    event_date : :obj:`DateHolder`
        The date the event happened
    date_of_birth : :obj:`DateHolder`
        The participant date of birth

    Returns
    -------
    age_of_event : float
        The decimal age in years that the event took place (rounded to 2dp)
    """
    return round((event_date.date_obj - date_of_birth.date_obj) /
                 timedelta(days=365), 2)
