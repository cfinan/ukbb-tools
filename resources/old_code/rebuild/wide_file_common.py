"""General common functions use to parse the wide file
"""
from ukbb_tools import (
    constants as con,
    columns as col
)
from contextlib import contextmanager
# from pyaddons import gzopen
import csv
import os
import tempfile
import shutil
import re
import hashlib
import binascii
import gzip


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class SkipDataError(Exception):
    pass


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class BlankDataError(Exception):
    pass


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def filter_fields(fields):
    """
    Partition a list of mixed field IDs into two lists, one of field_ids and
    one of column names. A field_id is an ID that can be cast as an int or is
    an int. It possibly represents many columns in a wide file. A column name
    is a single column in the wide file, with the format:
    `<FIELD_ID>-<INSTANCE>.<ARRAY>`

    Parameters
    ----------
    list of int and str
        A list of field IDs and/or column names that needs to partitioned into
        field IDs and column name lists

    Returns
    -------
    fields : list of int
        Any field IDs that the user has passed
    columns : list of str
        Any column names that the user has supplied

    Raises
    ------
    ValueError
        If any of the elements are not field IDs or column names
    """
    field_ids = set([])
    columns = set([])

    # Now partition into field IDs and column names
    for i in fields:
        try:
            fid = int(i)
            field_ids.add(fid)
        except ValueError:
            # Must be a column but I will check and the structure and error
            # out
            if not re.match(r'\d+-\d+\.\d+', i):
                raise ValueError("unknown column name: '{0}'".format(i))
            columns.add(i)

    return list(field_ids), list(columns)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_csv_kwargs(file_path, read_bytes=2000000, **csv_kwargs):
    """
    Sniff out the CSV dialect only if no csv_kwargs have been supplied. If they
    have then they are just passed through the function. This is also agnostic
    to compressed files.

    Parameters
    ----------
    file_path : str
        The location of the file to test
    read_bytes : optional, default: 2000000
        How many bytes of the file to read in when attempting to define the
        csv dialect. If has been set to 2MB as a lot of the wide file needs
        to be sampled to get an accurate representation
    **csv_kwargs
        If any of these arguments have been defined then they are passed
        through and no sniffing takes place. If not then the file will be
        sniffed

    Returns
    -------
    csv_kwargs : dict
        These are either the same as was passed or the sniffed out kwargs
        with the KeyWord 'dialect' defined
    """
    # If no csv keyword arguments have been supplied then we attempt to
    # sniff them out
    if len(csv_kwargs) == 0:
        # In this wide file we need to sniff a larger amount of data to get
        # it correct (2MB)
        try:
            fobj = gzip_fh(file_path)
            csv_kwargs['dialect'] = csv.Sniffer().sniff(
                fobj.read(read_bytes)
            )
        finally:
            fobj.close()

    return csv_kwargs


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@contextmanager
def write_via_temp(outfile, write_method=open, **kwargs):
    """
    Provide a file object that will be associated with a tempfile that when
    closed (out of context scope) will be closed and copied to the required
    file location

    Parameters
    ----------
    outfile : str
        The final location of the output file
    write_method : :obj:`FileWritingMethod`, optional, default: open
        The function to use to open the tempfile, it defaults to the builtin
        open
    **kwargs
        Arguments to `tempfile.mkstemp` and to be applied to the write method
    """
    # Extract the tempfile kwargs the remainder will be passed to the
    # write method
    tmpfile_kwargs = dict([(i, kwargs.pop(i, None))
                           for i in ['dir', 'prefix', 'suffix']])

    temp_file_name = get_tempfile(**tmpfile_kwargs)
    fobj = write_method(temp_file_name, **kwargs)
    move = False

    try:
        yield fobj
        move = True
    except Exception:
        fobj.close()
        os.unlink(temp_file_name)
        move = False
        raise
    finally:
        if move is True:
            fobj.close()
            shutil.move(temp_file_name, outfile)


# TODO: A copy of this is in common now - refactor code to use common and delete ths
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_tempfile(**kwargs):
    """
    Create a temp file and return the name only

    Parameters
    ----------
    *args
        Any arguments to `tempfile.mkstemp`

    Returns
    -------
    tempfile_name : str
        The path to the tempfile, where the wide file will be written prior
        to moving it to the final location
    """
    tfobj, tfn = tempfile.mkstemp(**kwargs)
    os.close(tfobj)
    return tfn


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def check_header(header):
    """
    Make sure the header is valid

    Parameters
    ----------
    header : list of str or :obj:`csv.Reader`
        Either the header or a csv.REader object to extract it from

    Returns
    -------
    header : list of str
        The header that should be valid. Note that this is the full header and
        not just the data part of it
    """
    min_allowed_len = 1
    if not isinstance(header, list):
        # Assume it is a reader
        header = next(header)

    if header[0] != col.EID.name:
        raise KeyError("expected eid column at position 0 in the header")

    if len(header) == min_allowed_len:
        raise ValueError("The header has no data columns")

    # Finally make sure that the field columns are the correct format
    for i in header[min_allowed_len:]:
        if not re.match(r'^\d+-\d+.\d+$', i):
            raise ValueError("unknown header format: '{0}'".format(i))
    return header


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def header_md5(header):
    """
    Return the MD5 of the header line so that we can easily comparse and store
    if needed

    Parameters
    ----------
    header : list of str or :obj:`csv.Reader`
        Either the header or a csv.REader object to extract it from

    Returns
    -------
    md5sum : str
        The hexdigest MD5 of the header after " " delimiting
    """
    return hashlib.md5(" ".join(header).encode('utf-8')).hexdigest()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_index_path(wide_file, index_file=None):
    """
    Return the index file path. If index file is defined, then it is passed
    through the function (after path expansion). If not then an `.idx`
    extension is added to the wide_file path and that is used as the
    index file path

    Parameters
    ----------
    wide_file : str
        The path to the wide file that has been indexed
    index_file : str, optional, default: NoneType
        An alternative index file location. If `NoneType` then the index
        file path is the same as the wide file path but with an `.idx`
        extension

    Returns
    -------
    index_file : str, optional, default: NoneType
        The index file location. If `NoneType` then the index
        file path is the same as the wide file path but with an `.idx`
        extension
    """
    wide_file = os.path.expanduser(wide_file)
    if index_file is None:
        return '{0}.idx'.format(wide_file)
    return os.path.expanduser(index_file)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def split_header(header_row):
    """
    Split a wide file header row into a list of tuples where each tuple
    contains the integer elements of the column name. With the field_id
    being at [0], the instance number being at [1] and the array index
    being at [2]

    Parameters
    ----------
    header_row : list of str
        A header row from the wide file. Each element should have the
        structure `{field_id}-{instance_no}.{array_idx}`. Except for the
        `eid` column which must also be at the start of the row. Also, an
        `withdrawn` optional column is allowed at element [1]

    Returns
    -------
    header_tuples : list of tuple
        The header split columns into tuples where each tuple contains the
        integer elements of the column name. With the field_id being at
        [0], the instance number being at [1] and the array index being
        at [2]
    """
    # Split the header into a component tuple of ints, with the field_id
    # being element [0], the n_instances being element [1] and the array
    # value being element [2]
    split_regexp = re.compile(r'[-.]')
    header_tuples = []

    # Loop through the header column names
    for idx, i in enumerate(header_row):
        try:
            # Generate the int tuple from the component parts and store
            # each one
            header_col = tuple([int(i) for i in split_regexp.split(i)])
            header_tuples.append(header_col)
        except ValueError as e:
            # Can't split into a tuple
            if (i != 'eid' and idx != 0) and \
               (i != 'withdrawn' and idx != 1):
                # Can't remember what the error will be
                raise e.__class__("unknown header value '{0}'".format(i))
            # header_tuples.append((i, 0, 0))

    return header_tuples


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@contextmanager
def gzip_agnostic(file_name, *args, **kwargs):
    """
    wrap an compression agnostic file openner in a context manager

    Parameters
    ----------
    file_name : str
        The path to the file that might be compressed
    *args
        Arguments to the `gzip_fh`
    **kwargs
        Keyword arguments to `gzip_fh`
    """
    fobj = gzip_fh(file_name, *args, **kwargs)
    try:
        yield fobj
    except Exception:
        raise
    finally:
        fobj.close()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def gzip_fh(fh, mode='t', **kwargs):
    """
    Checks to see if a file is a gzip file and opens the correct file handle
    for gzipped files. It can either accept a file handle or file name as the
    argument. This is an attempt at a gzipped agnostic file opener

    Parameters
    ----------
    fh : :obj:`FileObject` or `str`
        Either the file name or the file object to test
    mode : str
        All the modes are read modes, so the choice here is `t` or `b`
    **kwargs
        Any other arguments such as encoding
    """

    # If the argument is not a file handle then attempt to open it
    try:
        # If we try to open and get a TypeError, we are assuming it is a file
        # like object already
        fh = open(fh, 'rb')
    except TypeError:
        pass

    # The file may be from STDIN r a file descriptor, we can test this
    # by attempting a seek, if this fails with an illegal seek then
    # there is not much we can do so we ust return the file object as if
    # we start to read it and it is not gzipped then we can't move
    # back to the beginning
    try:
        fh.tell()
    except IOError as e:
        if str(e) == "[Errno 29] Illegal seek":
            return fh
        # If anything else goes wrong then we raise it
        raise

    # We read the first two bytes and we will look at their values
    # to see if they are the gzip characters
    testchr = fh.read(2)

    # Test for the gzipped characters
    if binascii.b2a_hex(testchr).decode() == "1f8b":
        # If it is gziped, then close it and reopen as a gzipped file
        fn = fh.name
        fh.close()
        fh = gzip.open(fn, 'r{0}'.format(mode), **kwargs)
    else:
        # If not seek back to the position that we started
        fn = fh.name
        fh.close()
        fh = open(fn, 'r{0}'.format(mode), **kwargs)

    # Return the opened file
    return fh
