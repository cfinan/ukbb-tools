"""
.. include:: ../docs/db_build.md
"""
from ukbb_tools import __version__, __name__ as pkg_name, config, \
    file_readers as fr, constants as con, ukbb_orm as uo, columns as col, \
    data_dict_orm as do
from pyaddons import sqlalchemy_helper
from simple_progress import progress
import sqlalchemy
import argparse
import sys
import os
import warnings
import pprint as pp
import pkg_resources


# DEFAULT_COMMIT_LINES = 50
DEFAULT_COMMIT_LINES = 500000
# DEFAULT_COMMIT_LINES = 10000

# Move a load of these to constants
# The prefix for verbose messages and the location to write the messages
MSG_PREFIX = '[info]'
MSG_OUT = sys.stderr

# The batch names for loading the UKBB data, these should be the same as the
# names that are used to prefix the constants
UKBB_ENUM = 'UKBB_ENUM'
UKBB_DATE = 'UKBB_DATE'
UKBB_STR = 'UKBB_STR'
UKBB_NUM = 'UKBB_NUM'

SKIP_FIELDS = [(39922, 41270), (39922, 41210), (39922, 41202),
               (39922, 41200), (39922, 41272), (39922, 41204)]


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main():
    """
    The main entry point for the script
    """
    # Initialise and parse the command line arguments
    parser = init_cmd_args()
    args = parse_cmd_args(parser)

    # Start a msg output, this will respect the verbosity and will output to
    # STDERR
    m = progress.Msg(prefix='', verbose=args.verbose, file=MSG_OUT)
    m.msg("= UKBB database builder ({0} v{1}) =".format(pkg_name, __version__))
    m.prefix = MSG_PREFIX
    m.msg_atts(args)

    # Get the config object linked to the config file
    config_obj = config.initialise_config(config_file=args.config_file)
    outfiles = config.get_output_files(config_obj, error_on_missing=True)

    ukbb_sm = config.get_ukbb_connection(config_obj)

    builder = UkbbDbBuilder(
        ukbb_sm,
        outfiles[con.SAMPLE_FILE_NAME],
        outfiles[con.CODE_FILE_NAME],
        outfiles[con.CODE_MAPPING_LOG_FILE_NAME],
        outfiles[con.CLIN_DATA_FILE_NAME],
        outfiles[con.LONG_FILE_NAME_ENUM],
        outfiles[con.LONG_FILE_NAME_STRING],
        outfiles[con.LONG_FILE_NAME_DATES],
        outfiles[con.LONG_FILE_NAME_NUMERIC],
        data_dict_sm=None,
        verbose=args.verbose,
        commit_every=DEFAULT_COMMIT_LINES
    )

    # Now build
    builder.build()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def init_cmd_args():
    """
    Initialise the command line arguments and return the parsed arguments

    Returns
    -------
    args : :obj:`argparse.Namespace`
        The argparse namespace object
    """
    parser = argparse.ArgumentParser(
        description="Build the UKBB data into a database"
    )

    # The wide format UKBB data fields filex
    parser.add_argument('--config-file',
                        type=str,
                        help="An alternative path to the config file")
    parser.add_argument('-v', '--verbose',  action="store_true",
                        help="give more output")

    return parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_cmd_args(parser):
    """
    Initialise the command line arguments and return the parsed arguments

    Returns
    -------
    args : :obj:`argparse.Namespace`
        The argparse namespace object
    """
    args = parser.parse_args()

    for i in ['config_file']:
        try:
            setattr(args, i, os.path.expanduser(getattr(args, i)))
        except TypeError:
            # Not defined using default
            pass

    return args


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class UkbbDbBuilder(object):
    """
    Control the construction of the UKBiobank database
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @staticmethod
    def _get_progress(nlines, default_msg=None, verbose=False):
        """
        Get a progress monitor the type of which is dependent on the value of
        nlines, if 0 or None then a `progress.RateProgress` is returne
        otherwise a `progress.RemainingProgress` is returned
        """
        if nlines is None or nlines == 0:
            return progress.RateProgress(default_msg=default_msg)
        return progress.RemainingProgress(nlines, default_msg=default_msg,
                                          verbose=verbose)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @staticmethod
    def _insert_code_desc(code_id, all_descs, clin_desc_cache, cache, id_col,
                          code_id_col):
        """
        Both the UKBB clinical terms and the UMLS mappings store their
        descriptions in the clinical_code_desc table. This is a generic
        update method that will update the description table if needed and
        also update the joining table that maps the relationships between
        the clinical_terms/cuis and the descriptions

        Parameters
        ----------
        code_id : int
            The code ID for either a clinical_code (UKBB) for a CUI (UMLS)
        all_desc : list of str
            Descriptions associated with the clinical_code or CUI. If there
            are no descriptions then this will be an empty list, it is never
            NoneType
        clin_desc_cache : :obj:`Cache`
            The cache associated with either the `clinical_code_desc` table
            that holds the descriptions for the various clinical codes
        cache : :obj:`Cache`
            The cache associated with either the `clinical_code_to_desc`
            table or the `cui_to_desc` table
        id_col : str
            The name of the ID column in either the `clinical_code_to_desc`
            table or the `cui_to_desc` table
        code_id_col : str
            The name of the code ID column in either the the
            `clinical_code_to_desc` table or the `cui_to_desc` table
        """
        for desc in all_descs:
            clin_desc_id = clin_desc_cache.add(
                dict(
                    clinical_code_desc_id=None,
                    clinical_code_desc=desc
                ),
                desc
            )
            try:
                # We do not want NoneType descriptions being inserted
                int(clin_desc_id)
            except TypeError:
                continue

            # The key for the association between clinical code and
            # description is based on their IDs so that is what we
            # key on, we do not need the ID that is returned
            cache.add(
                {
                    id_col: None,
                    code_id_col: code_id,
                    'clinical_code_desc_id': clin_desc_id
                },
                (code_id, clin_desc_id)
            )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @staticmethod
    def _flush_caches(session, *caches):
        """
        Flush all the new cache content into the database and commit

        Parameters
        ----------
        session : `sqlalchemy.Session`
            The session to use for the inserts
        *caches
            One or more `Cache` objects
        """
        for i in caches:
            session.bulk_insert_mappings(i.table, i.flush())
        session.commit()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @staticmethod
    def _flush_lists(session, *args):
        """
        Insert data in lists into the database and commit

        Parameters
        ----------
        session : `sqlalchemy.Session`
            The session to use for the inserts
        *args
            One or more `tuple` objects, with orm class at [0] and a list
            of dict at [1]
        """
        for table, data in args:
            session.bulk_insert_mappings(table, data)
        session.commit()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @staticmethod
    def _add_numerics(numerics, row):
        """
        Parameters
        ----------
        numerics : list of float
            The data store for the numeric rows to be inserted into the
            database
        row : dict
            The row to extract the event id and numeric data points from
        """
        for idx, i in enumerate(row[col.CLIN_DATA_NUMERICS.name]):
            try:
                standard_value = row[col.CLIN_DATA_NORM_NUMERICS.name][idx]
            except IndexError:
                standard_value = None

            numerics.append(
                dict(
                    event_id=row[col.CLIN_DATA_EVENT_IDX.name],
                    raw_value=i,
                    standard_value=standard_value,
                    flag=0
                )
            )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @staticmethod
    def _add_simple_data(event_id, data_store, data, col_name):
        """
        Add simple clinical event data points to the data store

        Parameters
        ----------
        event_id : int
            The event index to associate with the data point
        data_store : list of dict
            Will hold data points that have been built
        data : list
            The data to convert into a dict and add to the data_store
        col_name : str
            The name of the column that the data point will be listed under
        """
        for i in data:
            data_store.append(
                {
                    'event_id': event_id,
                    col_name: i,
                }
            )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @staticmethod
    def _get_ukbb_load_colmap(*names):
        """
        Get a dictionary between the ukbb_data column names and the constant
        names for the various components of the UKBB data. I need to do this
        to generalise the load code to the different constant names
        """
        colmap = dict([(i, dict(data_map={}, value_map={})) for i in names])

        for i in names:
            colmap[i]['data_map'] = dict(
                eid=col.EID.name,
                field_id=getattr(col, '{0}_FIELD_ID'.format(i)).name,
                date_field_id=getattr(
                    col, '{0}_REL_DATE_FIELD_ID'.format(i)
                ).name,
                date_field_inst_id=getattr(
                    col, '{0}_REL_DATE_FIELD_INST_ID'.format(i)
                ).name,
                date_array_id=getattr(
                    col, '{0}_REL_DATE_ARRAY_ID'.format(i)
                ).name,
                date=getattr(col, '{0}_REL_DATE'.format(i)).name,
                date_int=getattr(col, '{0}_REL_INT_DATE'.format(i)).name,
                days_from_baseline=getattr(
                    col, '{0}_EVENT_DAYS_FROM_BASELINE'.format(i)
                ).name,
                age_at_event=getattr(
                    col, '{0}_AGE_YEARS_AT_EVENT'.format(i)
                ).name,
                is_fallback_date=getattr(
                    col, '{0}_IS_FALLBACK_DATE'.format(i)
                ).name
            )

            value_name = 'data_point_value'
            if i == UKBB_ENUM:
                value_name = 'coding_enum_id'

            colmap[i]['value_map'] = {
                'instance_id': getattr(col, '{0}_INST_ID'.format(i)).name,
                'array_id': getattr(col, '{0}_ARRAY_ID'.format(i)).name,
                value_name: getattr(col, '{0}_VALUE'.format(i)).name
            }

        return colmap

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @staticmethod
    def _skip_row(row, field_id, coding_id, is_fallback_date):
        """
        """
        if is_fallback_date == -1:
            return True

        if (coding_id, field_id) in SKIP_FIELDS:
            return True

        return False

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @staticmethod
    def _check_eids(eid_lookup, batch):
        """
        """
        found_batch = False
        in_samples = False
        for i in (UKBB_ENUM, UKBB_DATE, UKBB_STR, UKBB_NUM):
            try:
                eid = batch[i][0][col.EID.name]
                found_batch = True
                if eid not in eid_lookup:
                    break
                in_samples = True
                break
            except KeyError:
                # Does not have the current datatype, so we will try
                # the next one
                pass

        if in_samples is False:
            warnings.warn(
                'sample data not in samples table: {0}'.format(eid)
            )
        if found_batch is False:
            warnings.warn(
                'empty batch, this should not happen and is '
                'probably a bug'
            )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, sessionmaker, samples, clinical_codes, mapping_log,
                 clinical_data, ukbb_enum, ukbb_string, ukbb_date,
                 ukbb_numeric, data_dict_sm=None, verbose=False,
                 commit_every=100000, commit_every_samples=500,
                 pre_created=True):
        """
        Parameters
        ----------
        sessionmaker : :obj:`sqlalchemy.Sessionmaker`
            A sessionmaker object to use to interface to the UKBB database
        samples : :obj:`config.FileAttr`
            A namedtuple, detailing the file type `samples`, path, encoding
            nlines (if known) and `csv` keyword arguments with which to parse
            the file. These are as detailed in the config file.
        clinical_codes : :obj:`config.FileAttr`
            A namedtuple, detailing the file type `clinical_codes`, path,
            encoding nlines (if known) and `csv` keyword arguments with which
            to parse the file. These are as detailed in the config file.
        mapping_log : :obj:`config.FileAttr`
            A namedtuple, detailing the file type `mapping_log`, path, encoding
            nlines (if known) and `csv` keyword arguments with which to parse
            the file. These are as detailed in the config file.
        clinical_data : :obj:`config.FileAttr`
            A namedtuple, detailing the file type `clinical_data`, path,
            encoding nlines (if known) and `csv` keyword arguments with
            which to parse the file. These are as detailed in the config file.
        data_dict_sm : :obj:`sqlalchemy.Sessionmaker` or NoneType,
        optional, default: NoneType
            An alternative database for the data dictionary. If `NoneType` then
            the data dictionary that is distributed with `ukbb_tools` is used
        verbose : bool, optional, default: False
            Output build progress to STDERR
        commit_every : int, optional, default: 100000
            The number of rows to process in each table before commiting to the
            database. This relies of autocommit=False for the UKBB database
            SQLAlchemy connection
        """
        self._sessionmaker = sessionmaker
        self._samples = samples
        self._clinical_codes = clinical_codes
        self._mapping_log = mapping_log
        self._clinical_data = clinical_data
        self._ukbb_enum = ukbb_enum
        self._ukbb_string = ukbb_string
        self._ukbb_date = ukbb_date
        self._ukbb_numeric = ukbb_numeric
        self._verbose = verbose
        self._commit_every = commit_every
        self._commit_every_samples = commit_every_samples
        self._pre_created = pre_created

        # TODO: Connect to the data dictionary if not passed:
        self._data_dict = data_dict_sm
        if data_dict_sm is None:
            path = 'data/data_dict.db'  # always use slash
            filepath = pkg_resources.resource_filename(__name__, path)

            # Build an SQLalchemy connection URL
            connect_uri = sqlalchemy.engine.url.URL(
                'sqlite', database=filepath
            )
            engine = sqlalchemy.create_engine(connect_uri)

            # If we get here then we are good to continue, so we
            # create a session
            self._data_dict = sqlalchemy.orm.sessionmaker(
                bind=engine
            )

        # Make sure that all the tables are created before building
        self._create_tables()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _create_tables(self):
        """
        Create all the tables
        """
        session = self._sessionmaker()
        uo.Base.metadata.create_all(
            session.get_bind(),
            checkfirst=self._pre_created
        )
        session.close()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def build(self):
        """
        Initialise the build process
        """
        # First copy the data dictionary over
        # self._copy_data_dict()

        # Order of insertion is important
        # self._insert_samples()
        # self._insert_clinical_codes()
        # self._insert_clinical_data()
        self._insert_ukbb_data()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _copy_data_dict(self):
        """
        Copy all the tables out of the data dictionary
        """
        session = self._sessionmaker()

        # Order is important here
        for ft, tt in [(do.CodingId, uo.CodingId),
                       (do.CodingEnum, uo.CodingEnum),
                       (do.DataPath, uo.DataPath),
                       (do.DataPathRel, uo.DataPathRel),
                       (do.FieldId, uo.FieldId)]:
            sqlalchemy_helper.copy_table(
                self._data_dict(), ft, session, to_table=tt,
                commit_every=self._commit_every, verbose=self._verbose,
                checkfirst=True
            )
        session.close()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _insert_samples(self):
        """
        Insert the data from the samples table into the UKBB database
        """
        session = self._sessionmaker()

        with fr.SamplesReader(
                self._samples.path,
                encoding=self._samples.encoding,
                **self._samples.csvkwargs
        ) as infile:
            prog = UkbbDbBuilder._get_progress(
                self._samples.nlines,
                default_msg="inserting {0} data...".format(
                    self._samples.name
                ),
                verbose=self._verbose
            )
            nadded = 0
            samples = []
            for row in prog.progress(infile):
                samples.append(row)
                # session.add(uo.Sample(**row))
                nadded += 1
                if nadded == self._commit_every:
                    session.bulk_insert_mappings(uo.Sample, samples)
                    session.commit()
                    samples = []
                    nadded = 0

            if nadded > 0:
                session.bulk_insert_mappings(uo.Sample, samples)
                session.commit()
                samples = []
        session.close()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _insert_clinical_codes(self):
        """
        Insert all the clinical code data and mappings to cuis
        """
        # Create a session and all the tables we will be inserting into
        session = self._sessionmaker()

        # We will use a system of caches to store added records, so we do
        # not have to query the database each time. The number of records
        # is pretty small so should be able to get away with this
        clin_code_cache = Cache(uo.ClinicalCode, 'clinical_code_id')
        clin_desc_cache = Cache(uo.ClinicalCodeDesc, 'clinical_code_desc_id')
        cui_cache = Cache(uo.UmlsCui, 'umls_cui_id')
        sty_cache = Cache(uo.UmlsSty, 'umls_sty_id')
        sab_cache = Cache(uo.CodingSystem, 'coding_system_id')
        cui_to_sty_cache = Cache(uo.UmlsCuiToSty, 'umls_cui_to_sty_id')
        clin_code_to_desc_cache = Cache(
            uo.ClinicalCodeToDesc,
            'clinical_code_to_desc_id'
        )
        cui_to_desc_cache = Cache(uo.UmlsCuiToDesc, 'umls_cui_to_desc_id')
        clin_code_to_cui_cache = Cache(
            uo.ClinicalCodeToCui,
            'clinical_code_to_cui_id'
        )

        with fr.ClinicalCodesReader(
                self._clinical_codes.path,
                encoding=self._clinical_codes.encoding,
                **self._clinical_codes.csvkwargs
        ) as infile:
            prog = UkbbDbBuilder._get_progress(
                self._clinical_codes.nlines,
                default_msg="inserting {0} data...".format(
                    self._clinical_codes.name
                ),
                verbose=self._verbose
            )
            nadded = 0
            for row in prog.progress(infile):
                # The clinical code table references the sab table so
                # process the SAB first
                sab = dict(
                    coding_system_id=None,
                    coding_system_name=row[col.CLIN_CODE_UKBB_SAB.name]
                )
                sab_id = sab_cache.add(sab, row[col.CLIN_CODE_UKBB_SAB.name])

                clin_code = dict(
                    clinical_code_id=None,
                    clinical_code=row[col.CLIN_CODE_UKBB_CODE.name],
                    coding_system_id=sab_id,
                    ncui=0,
                    min_n_value=row[col.CLIN_CODE_MIN_N_VALUES.name],
                    max_n_value=row[col.CLIN_CODE_MAX_N_VALUES.name],
                    n_occur=row[col.CLIN_CODE_N_OCCURS.name],
                    n_is_empty=row[col.CLIN_CODE_N_IS_EMPTY.name],
                    n_is_string=row[col.CLIN_CODE_N_IS_STRING.name],
                    n_is_integer=row[col.CLIN_CODE_N_IS_INTEGER.name],
                    n_is_float=row[col.CLIN_CODE_N_IS_FLOAT.name],
                    ratio_3_0_0=row[col.CLIN_CODE_RATIO_300.name],
                    ratio_0_3_0=row[col.CLIN_CODE_RATIO_030.name],
                    ratio_0_0_3=row[col.CLIN_CODE_RATIO_003.name],
                    ratio_2_1_0=row[col.CLIN_CODE_RATIO_210.name],
                    ratio_2_0_1=row[col.CLIN_CODE_RATIO_201.name],
                    ratio_1_2_0=row[col.CLIN_CODE_RATIO_120.name],
                    ratio_0_2_1=row[col.CLIN_CODE_RATIO_021.name],
                    ratio_1_0_2=row[col.CLIN_CODE_RATIO_102.name],
                    ratio_0_1_2=row[col.CLIN_CODE_RATIO_012.name],
                    ratio_1_1_1=row[col.CLIN_CODE_RATIO_111.name]
                )
                clin_code_key = (row[col.CLIN_CODE_UKBB_CODE.name],
                                 row[col.CLIN_CODE_UKBB_SAB.name])
                clin_code_id = clin_code_cache.add(clin_code, clin_code_key)

                try:
                    # There is a chance that the clinical code ID is NoneType
                    # if the actual clinical code is NoneType. In this case
                    # there is nothing to process so we skip to the next row
                    int(clin_code_id)
                except TypeError:
                    continue

                self._insert_code_desc(
                    clin_code_id,
                    row[col.CLIN_CODE_UKBB_TERM.name],
                    clin_desc_cache,
                    clin_code_to_desc_cache,
                    'clinical_code_to_desc_id',
                    'clinical_code_id'
                )

                cui = dict(
                    umls_cui_id=None,
                    umls_cui=row[col.CLIN_CODE_UMLS_CUI.name],
                    nterms=0
                )
                cui_id = cui_cache.add(cui, row[col.CLIN_CODE_UMLS_CUI.name])

                try:
                    # Some of the UKBB clinical terms will not be mapped to
                    # UMLS concepts, in which case there is nothing to do
                    int(cui_id)
                except TypeError:
                    continue

                # If we are here then we have a UMLS cui mapping so we need
                # to associate it with a description (if it has one)
                self._insert_code_desc(
                    cui_id,
                    row[col.CLIN_CODE_UMLS_TERM.name],
                    clin_desc_cache,
                    cui_to_desc_cache,
                    'umls_cui_to_desc_id',
                    'umls_cui_id'
                )

                # Now insert the Stys (semantic types), associated with the
                # UMLS cui
                for desc in row[col.CLIN_CODE_UMLS_STY.name]:
                    sty_id = sty_cache.add(
                        dict(umls_sty_id=None, umls_sty_desc=desc), desc
                    )

                    cui_to_sty_cache.add(
                        dict(
                            umls_cui_to_sty_id=None,
                            umls_cui_id=cui_id,
                            umls_sty_id=sty_id
                        ),
                        (cui_id, sty_id)
                    )

                # Now insert the link between the UMLS concept and the
                # UKBB clinical code
                clin_code_to_cui_cache.add(
                    dict(
                        clinical_code_to_cui_id=None,
                        clinical_code_id=clin_code_id,
                        umls_cui_id=cui_id
                    ),
                    (clin_code_id, cui_id)
                )

                nadded += 1
                if nadded == self._commit_every:
                    pp.pprint(clin_code_to_desc_cache._keys)
                    UkbbDbBuilder._flush_caches(
                        session, sab_cache, clin_code_cache, clin_desc_cache,
                        cui_cache, sty_cache, clin_code_to_desc_cache,
                        cui_to_sty_cache, cui_to_desc_cache,
                        clin_code_to_cui_cache
                    )
                    nadded = 0
                    # break
            if nadded > 0:
                UkbbDbBuilder._flush_caches(
                    session, sab_cache, clin_code_cache, clin_desc_cache,
                    cui_cache, sty_cache, clin_code_to_desc_cache,
                    cui_to_sty_cache, cui_to_desc_cache,
                    clin_code_to_cui_cache
                )
                nadded = 0
        session.close()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _insert_clinical_data(self):
        """
        Insert all the clinical data into the clinical data table and
        ancillary lookup tables
        """
        # This is a dict not actually a Cache object
        code_cache = self._get_clinical_code_id_cache()
        care_cache = Cache(uo.CareType, 'care_type_id')
        record_cache = Cache(uo.RecordType, 'record_type_id')
        provider_cache = Cache(uo.DataProvider, 'data_provider_id')
        pct_cache = Cache(uo.Pct, 'pct_id')
        session = self._sessionmaker()

        with fr.ClinicalDataReader(
                self._clinical_data.path,
                encoding=self._clinical_data.encoding,
                **self._clinical_data.csvkwargs
        ) as infile:
            prog = UkbbDbBuilder._get_progress(
                self._clinical_data.nlines,
                default_msg="inserting {0}...".format(
                    self._clinical_data.name
                ),
                verbose=self._verbose
            )

            # The numerics, strings, note and units are not used via a cache
            # as there is a one:many relationship between clinical_data and
            # and data holding tables
            numerics = []
            strings = []
            units = []
            notes = []
            clinical_data = []

            nadded = 0
            for row in prog.progress(infile):
                try:
                    clin_code_id = code_cache[
                        (row[col.CLIN_DATA_CLINICAL_CODE.name],
                         row[col.CLIN_DATA_CLINICAL_CODE_SYSTEM.name])
                    ]
                except KeyError as e:
                    warnings.warn("{0}".format(e.args[0]))

                care_type = dict(
                    care_type_id=None,
                    care_type_desc=row[col.CLIN_DATA_CARE_TYPE.name]
                )
                record_type = dict(
                    record_type_id=None,
                    record_type_desc=row[col.CLIN_DATA_RECORD_TYPE.name]
                )
                data_provider = dict(
                    data_provider_id=None,
                    data_provider_desc=row[col.CLIN_DATA_DATA_PROVDER.name]
                )
                pct = dict(
                    pct_id=None,
                    nhs_pct_id=row[col.CLIN_DATA_PCT.name]
                )

                record = dict(
                    event_id=row[col.CLIN_DATA_EVENT_IDX.name],
                    episode_idx=row[col.CLIN_DATA_EPISODE_IDX.name],
                    eid=row[col.EID.name],
                    event_start=row[col.CLIN_DATA_EVENT_START_STR.name],
                    event_start_int=row[col.CLIN_DATA_EVENT_START_INT.name],
                    age_at_event=row[col.CLIN_DATA_AGE_AT_EVENT.name],
                    days_from_baseline=row[
                        col.CLIN_DATA_DAYS_FROM_BASELINE.name
                    ],
                    days_pre_event=row[col.CLIN_DATA_DAYS_PRE_EVENT.name],
                    days_post_event=row[col.CLIN_DATA_DAYS_POST_EVENT.name],
                    clinical_code_id=clin_code_id,
                    event_level=row[col.CLIN_DATA_EVENT_LEVEL.name],
                    care_type_id=care_cache.add(
                        care_type,
                        row[col.CLIN_DATA_CARE_TYPE.name]
                    ),
                    record_type_id=record_cache.add(
                        record_type,
                        row[col.CLIN_DATA_RECORD_TYPE.name]
                    ),
                    data_provider_id=provider_cache.add(
                        data_provider,
                        row[col.CLIN_DATA_DATA_PROVDER.name]
                    ),
                    data_provider_format_id=row[
                        col.CLIN_DATA_DATA_PROVDER_FORMAT.name
                    ],
                    pct_id=pct_cache.add(
                        pct,
                        row[col.CLIN_DATA_PCT.name]
                    ),
                    days_to_pct=row[col.CLIN_DATA_DAYS_TO_PCT.name],
                    pct_is_gp=row[col.CLIN_DATA_PCT_IS_GP.name],
                    is_clinical_measure=row[
                        col.CLIN_DATA_IS_CLINICAL_MEAS.name
                    ],
                    n_numeric=len(row[col.CLIN_DATA_NUMERICS.name]),
                    n_unit=len(row[col.CLIN_DATA_UNITS.name]),
                    n_string=len(row[col.CLIN_DATA_STRINGS.name]),
                    n_note=len(row[col.CLIN_DATA_CLINICAL_NOTES.name])
                )

                # Now add the data to the note, numerics, strings and units
                # tables
                self._add_numerics(numerics, row)
                self._add_simple_data(
                    row[col.CLIN_DATA_EVENT_IDX.name],
                    notes,
                    row[col.CLIN_DATA_CLINICAL_NOTES.name],
                    'note_value'
                )
                self._add_simple_data(
                    row[col.CLIN_DATA_EVENT_IDX.name],
                    units,
                    row[col.CLIN_DATA_UNITS.name],
                    'unit_value'
                )
                self._add_simple_data(
                    row[col.CLIN_DATA_EVENT_IDX.name],
                    strings,
                    row[col.CLIN_DATA_STRINGS.name],
                    'string_value'
                )

                clinical_data.append(record)
                nadded += 1
                if nadded == self._commit_every:
                    UkbbDbBuilder._flush_caches(
                        session, care_cache, record_cache,
                        provider_cache, pct_cache
                    )
                    UkbbDbBuilder._flush_lists(
                        session,
                        (uo.ClinicalData, clinical_data),
                        (uo.NumericClinData, numerics),
                        (uo.UnitClinData, units),
                        (uo.NoteClinData, notes),
                        (uo.StringClinData, strings)
                    )
                    numerics = []
                    strings = []
                    units = []
                    notes = []
                    clinical_data = []

                    nadded = 0
                if nadded == -100000:
                    break

            # Final write to the database
            if nadded > 0:
                UkbbDbBuilder._flush_caches(
                    session, care_cache, record_cache,
                    provider_cache, pct_cache
                )
                UkbbDbBuilder._flush_lists(
                    session,
                    (uo.ClinicalData, clinical_data),
                    (uo.NumericClinData, numerics),
                    (uo.UnitClinData, units),
                    (uo.NoteClinData, notes),
                    (uo.StringClinData, strings)
                )
        session.close()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _insert_ukbb_data(self):
        """
        Insert all the clinical code data and mappings to cuis
        """
        # Get some of the IDs that this data depends on
        eid_lookup = self._get_eid_cache()
        field_id_cache = self._get_field_id_cache()
        coding_enum_cache = self._get_coding_enum_cache()

        # Create a session and all the tables we will be inserting into
        session = self._sessionmaker()

        # Instantiate the readers and embed in reader definitions
        readers = self._get_ukbb_readers()

        with fr.MultiReader(*readers) as infiles:
            # prog = progress.RateProgress(
            prog = progress.RemainingProgress(
                503000,
                units="samples",
                default_msg="inserting UKBB data",
                verbose=self._verbose
            )

            colmap = UkbbDbBuilder._get_ukbb_load_colmap(
                UKBB_ENUM, UKBB_DATE, UKBB_STR, UKBB_NUM
            )
            ukbb_data_cache = Cache(uo.UkbbData, 'ukbb_data_id')
            numerics = []
            dates = []
            strings = []
            enums = []
            nsamples = 0
            for batch in prog.progress(infiles):
                # The first job is to make sure that the EID of the current
                # batch is present in the EID lookup, otherwise we will fail
                # with integrity error as the samples table is the central
                # sample lookup. I may remove this in future but for initial
                # builds, I will just throw a warning. All records in the batch
                # should have the same eid
                UkbbDbBuilder._check_eids(eid_lookup, batch)

                for name, values in ((UKBB_DATE, dates), (UKBB_STR, strings),
                             (UKBB_NUM, numerics)):
                    try:
                        self._process_file_batch(
                            name, colmap, field_id_cache, ukbb_data_cache,
                            values, batch[name]
                        )
                    except KeyError:
                        pass
                try:
                    self._process_file_batch(
                        UKBB_ENUM, colmap, field_id_cache, ukbb_data_cache,
                        enums, batch[UKBB_ENUM], value_cache=coding_enum_cache
                    )
                except KeyError:
                    pass

                nsamples += 1

                if nsamples >= self._commit_every_samples:
                    UkbbDbBuilder._flush_caches(
                        session, ukbb_data_cache
                    )
                    UkbbDbBuilder._flush_lists(
                        session, (uo.EnumUkbbData, enums),
                        (uo.StringUkbbData, strings), (uo.DateUkbbData, dates),
                        (uo.NumericUkbbData, numerics)
                    )
                    enums = []
                    numerics = []
                    strings = []
                    dates = []
                    nsamples = 0
                    ukbb_data_cache = Cache(uo.UkbbData, 'ukbb_data_id')
            if ukbb_data_cache.len() > 0:
                UkbbDbBuilder._flush_caches(session, ukbb_data_cache)
                UkbbDbBuilder._flush_lists(
                    session, (uo.EnumUkbbData, enums),
                    (uo.StringUkbbData, strings), (uo.DateUkbbData, dates),
                    (uo.NumericUkbbData, numerics)
                )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _process_file_batch(self, name, colmap, field_id_cache, row_cache,
                            value_data, batch, value_cache=None):
        """
        """
        data_map = colmap[name]['data_map']
        value_map = colmap[name]['value_map']
        for row in batch:
            try:
                # Make sure the field ID is present if not warn and skip
                # Because of the way the constants am using the database column
                # name in the colmap to get the column name in the data row
                field_id = row[data_map['field_id']]
                coding_id = field_id_cache[field_id]
            except KeyError:
                # warnings.warn(
                #     'field_id not in field IDs table: {0}'.format(
                #         row[data_map['field_id']]
                #     )
                # )
                continue

            db_row = dict([(k, row[v]) for k, v in data_map.items()])
            if UkbbDbBuilder._skip_row(row, field_id, coding_id,
                                       db_row['is_fallback_date']) is True:
                continue

            db_row['eid'] = row[col.EID.name]
            db_row['ukbb_data_id'] = None

            db_key = (
                db_row['eid'], db_row['field_id'], db_row['date_field_id'],
                db_row['date_int'], db_row['is_fallback_date']
            )
            ukbb_data_id = row_cache.add(db_row, db_key)

            if name == UKBB_ENUM:
                try:
                    enum_cache_key = (
                        coding_id,
                        row[col.UKBB_ENUM_VALUE.name]
                    )
                    # Now process the value
                    row[col.UKBB_ENUM_VALUE.name] = value_cache[enum_cache_key]
                except KeyError:
                    # There is a value cache but our data point is not in there
                    warnings.warn(
                        'Skipping, no enumerations for: {0} with {1}'
                        '(coding_id, enum_value)'.format(
                            row[col.EID.name], enum_cache_key
                        )
                    )

            value_row = dict([(k, row[v]) for k, v in value_map.items()])
            value_row['ukbb_data_id'] = ukbb_data_id
            value_data.append(value_row)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _get_ukbb_readers(self):
        """
        Instantiate the readers and embed in reader definitions/ These are
        required for the MultiReader

        Returns
        -------
        readers : list of :obj:`fr.ReaderDefinition`
            The readers that we want to read from on batches of EIDs
        """
        readers = []
        for reader, file_atts, name in [
                (fr.UkbbEnumReader, self._ukbb_enum, UKBB_ENUM),
                (fr.UkbbDateReader, self._ukbb_date, UKBB_DATE),
                (fr.UkbbStringReader, self._ukbb_string, UKBB_STR),
                (fr.UkbbNumericReader, self._ukbb_numeric, UKBB_NUM)]:
            readers.append(
                fr.ReaderDefinition(
                    reader=reader(
                        file_atts.path,
                        encoding=file_atts.encoding,
                        **file_atts.csvkwargs
                    ), name=name, key_col=col.EID.name, reset=False
                )
            )
        return readers

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _get_clinical_code_id_cache(self):
        session = self._sessionmaker()
        sql = session.query(uo.ClinicalCode)
        clin_code_id = dict([
            (
                (i.clinical_code, i.coding_systems.coding_system_name),
                i.clinical_code_id
            )
            for i in sql]
        )
        session.close()
        return clin_code_id

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _get_eid_cache(self):
        session = self._sessionmaker()
        sql = session.query(uo.Sample.eid)
        eids = [i.eid for i in sql]
        session.close()
        return eids

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _get_field_id_cache(self):
        session = self._sessionmaker()
        sql = session.query(uo.FieldId)
        field_ids = dict([(i.field_id, i.coding_id) for i in sql])
        session.close()
        return field_ids

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _get_coding_enum_cache(self):
        session = self._sessionmaker()
        sql = session.query(uo.CodingEnum)
        coding_enums = dict([
            (
                (i.coding_id, i.enum_value), i.coding_enum_id
            )
            for i in sql])
        session.close()
        return coding_enums


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == '__main__':
    main()
