"""
.. include:: ../docs/config.md
"""
from ukbb_tools import constants as con, common, temp_db_orm as ukbb_orm, \
     __version__, __name__ as pkg_name
from pyaddons import sqlalchemy_helper
from stat import S_IRWXO, S_IRWXG
from sqlalchemy.engine import url
from sqlalchemy.engine import create_engine
from sqlalchemy.orm import sessionmaker
from collections import namedtuple
import argparse
import os
import configparser
import shutil
import pprint as pp


FileAttr = namedtuple(
    'FileAttr',
    ['name', 'path', 'encoding', 'nlines', 'csvkwargs']
)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main():
    """
    The main entry point for the script
    """
    # Initialise and parse the command line arguments
    parser = init_cmd_args()
    args = parse_cmd_args(parser)
    check_config_permissions(args.config_file)
    config = read_config_file(args.config_file)
    section = get_section_data(config, args.section)

    for key, value in section.items():
        print('{0} {1}'.format(key, value))


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def init_cmd_args():
    """
    Initialise the command line arguments and return the parsed arguments

    Returns
    -------
    args : :obj:`argparse.ArgumentParser`
        The argparse parser object with arguments added
    """
    parser = argparse.ArgumentParser(
        description="Extract entries from a config file section header"
    )

    parser.add_argument('section',
                        type=str,
                        help="A required file")
    parser.add_argument('-c', '--config-file', type=str,
                        help="An alternative path to the config file")

    return parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_cmd_args(parser):
    """
    Initialise the command line arguments and return the parsed arguments

    Parameters
    ----------
    args : :obj:`argparse.ArgumentParser`
        The argparse parser object with arguments added

    Returns
    -------
    args : :obj:`argparse.Namespace`
        The argparse namespace object
    """
    args = parser.parse_args()

    # Get the valid config file name
    args.config_file = get_config_file(config_file=args.config_file)

    return args


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_config_file(config_file=None):
    """
    Test to see if the config file exists, if it does return a config object
    if not then raise an error

    Parameters
    ----------
    config_file : str or NoneType, optional, default: NoneType
        The path to the config file, if not supplied then the environment
        variable UKBB_CONFIG is checked, if that does not work then the
        default location is checked (`~/.ukbb.cnf`)

    Returns
    -------
    config : :obj`configparser.ConfigParser`
        The config file parser object

    Raises
    ------
    FileNotFoundError
        If a valid config file is not located, this function only checks that
        the file is a valid ini file. It does not actually check if the config
        file has the expected sections and keys
    """
    try:
        len(config_file)
    except TypeError:
        # NoneType
        try:
            config_file = os.environ[con.UKBB_CONFIG_ENVIR]
        except KeyError:
            config_file = con.CONFIG_FILE_PATH

    # Attempt to open to preempt any errors - as I do not think confg
    # parser errors out
    open(config_file).close()

    # Open up and return
    return config_file


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def read_config_file(config_file):
    """
    Open the config file and return the configparser object

    Parameters
    ----------
    config_file : str
        The path to the config file

    Returns
    -------
    config : :obj:`configparser`
        The config parser object to interact with the config file
    """
    # Sort out relative paths
    config_file = os.path.expanduser(config_file)

    # Attempt to open to preempt any errors - as I do not think confg
    # parser errors out
    open(config_file).close()

    # Open up and return
    config = configparser.ConfigParser()
    config.read(config_file)
    return config


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def check_config_permissions(config_file):
    """
    Make sure that the config file is only readable by you

    Parameters
    ----------
    config_file : str or NoneType, optional, default: NoneType
        The path to the config file, if not supplied then the environment
        variable UKBB_CONFIG is checked, if that does not work then the
        default location is checked

    Raises
    ------
    PermissionError
        If the config file can be read by other people
    """
    # Sort out relative paths
    config_file = os.path.expanduser(config_file)

    st = os.stat(config_file)
    if not bool(st.st_mode & (S_IRWXO | S_IRWXG)):
        raise PermissionError("ensure config file is only readable by you")


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_section_data(config, section):
    """
    Return a section from the config file (as a dict)

    Parameters
    ----------
    config : :obj:`configparser`
        The config parser object to interact with the config file
    section : str
        The section to return

    Returns
    -------
    config_section : dict
        The config parser object to interact with the config file

    Raises
    ------
    KeyError
        If the config section is not defined
    """
    try:
        # If we get here then no file is defined and we are relying on a
        # file being defined in the config file
        return config[section]
    except KeyError as e:
        # No section defined
        raise KeyError("'{0}' is not defined".format(section)) from e


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_umls_connection(config_obj):
    """
    Attempt to get a connection to the UMLS database

    Parameters
    ----------
    config : :obj`configparser.ConfigParser`
        The config file parser object

    Returns
    -------
    sesson_maker = sqlalchemy.Session
       An SQL Alchemy session maker for the UMLS database

    Raises
    ------
    KeyError
        If the UMLS section can't be found in the confg file
    FileNotFoundError
        If the UMLS database does not exist
    """
    try:
        # First make sure the section exists
        conn_info = sqlalchemy_helper.get_sqlalchemy_connection_info(
            config_obj,
            con.UMLS_SECTION_NAME,
            con.UMLS_ENTRY_PREFIX)
    except KeyError as e:
        raise KeyError("can't find UMLS section in config file") from e

    try:
        return sqlalchemy_helper.get_database_connection(
            conn_info,
            con.UMLS_ENTRY_PREFIX,
            must_exist=True)
    except KeyError as e:
        raise KeyError("are you sure the prefix in the config file is "
                       "'umls.'?") from e


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def write_config_file(config_file, config_obj, append=False):
    """
    """
    mode = 'wt'
    if append is True:
        mode = 'at'

    config_bak = common.get_tempfile()
    shutil.copy2(config_file, config_bak)
    try:
        with open(config_file, mode) as config_writer:
            config_obj.write(config_writer)
    except Exception:
        shutil.copy2(config_bak, config_file)
        os.unlink(config_bak)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def update_config_section(section_name, section_attrs, config_obj=None):
    if config_obj is None:
        config_obj = configparser.RawConfigParser()

    if not config_obj.has_section(section_name):
        config_obj.add_section(section_name)

    for key, value in section_attrs.items():
        config_obj.set(section_name, key, value)
    return config_obj


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def update_config(config_file, section_name, section_attrs, existing=None):
    """
    """
    if existing is None:
        mode = 'ab'
        config_update = configparser.RawConfigParser()
    else:
        mode = 'wb'
        config_update = existing

    config_update.add_section(section_name)

    for key, value in section_attrs.items():
        config_update.set(section_name, key, value)

    with open(config_file, mode) as f:
        config_update.write(f)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_ukbb_connection(config_obj):
    """
    Attempt to get a connection to the UMLS database. If the database does not
    exist then it is created

    Parameters
    ----------
    config : :obj`configparser.ConfigParser`
        The config file parser object

    Returns
    -------
    sesson_maker = sqlalchemy.Session
       An SQL Alchemy session maker for the UMLS database

    Raises
    ------
    KeyError
        If the UMLS section can't be found in the confg file
    """
    try:
        # First make sure the section exists
        conn_info = sqlalchemy_helper.get_sqlalchemy_connection_info(
            config_obj,
            con.UKBB_SECTION_NAME,
            con.UKBB_ENTRY_PREFIX)
    except KeyError as e:
        raise KeyError("can't find UKBB section in config file") from e

    return sqlalchemy_helper.get_database_connection(
        conn_info,
        con.UKBB_ENTRY_PREFIX,
        must_exist=False)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_ukbb_mapping_connection(config_obj, must_exist=False):
    """
    Attempt to get a connection to the UKBB mapping database. If must_exist is
    True, then it is an error if the database does not exist. If it is False
    then it is an error if the database does exist. If must_exist is False the
    database does not exist then it is created

    Parameters
    ----------
    config : :obj`configparser.ConfigParser`
        The config file parser object
    must_exist : bool, optional, default: False
        Do we require that the database exists

    Returns
    -------
    sesson_maker = sqlalchemy.Session
       An SQL Alchemy session maker for the UMLS database

    Raises
    ------
    KeyError
        If the UMLS section can't be found in the confg file
    FileExistsError
        If the database exists and must_exist is False
    FileNotFoundError
        If the database does not exist and must_exist is True
    """
    try:
        # First make sure the section exists
        conn_info = sqlalchemy_helper.get_sqlalchemy_connection_info(
            config_obj,
            con.UKBB_MAPPER_SECTION_NAME,
            con.UKBB_MAPPER_ENTRY_PREFIX)
    except KeyError as e:
        raise KeyError("can't find UKBB Mapper section in config file") from e

    # If must exist is False, we are treating it as an error if it does exist
    if must_exist is False:
        try:
            # This is to check if the database exists
            sqlalchemy_helper.get_database_connection(
                conn_info,
                con.UKBB_MAPPER_ENTRY_PREFIX,
                must_exist=True)
            # So if we get here and have not errored out then the
            # database alreadyed exists and that is an error
            raise FileExistsError("the database already exists")
        except FileNotFoundError:
            # All good
            return sqlalchemy_helper.get_database_connection(
                conn_info,
                con.UKBB_MAPPER_ENTRY_PREFIX,
                must_exist=False)

    return sqlalchemy_helper.get_database_connection(
        conn_info,
        con.UKBB_MAPPER_ENTRY_PREFIX,
        must_exist=True)


# # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# def get_temp_database(in_memory=False, tmpdir=None):
#     """
#     Create a temp database that we can use to store some intermediate files
#     """
#     temp_file_name = None
#     if in_memory is False:
#         # TODO: Comment in for production
#         # temp_file_name = common.get_tempfile(
#         #     dir=tmpdir,
#         #     suffix='.db',
#         #     prefix='ukbb_'
#         # )
#         # TODO: remove in production
#         temp_file_name = '/scratch/ukbb/processed_data/ukbb_db.db'

#         connect_url = url.URL(
#             'sqlite',
#             database=temp_file_name
#         )
#         engine = create_engine(connect_url)
#     else:
#         engine = create_engine('sqlite://')

#     # Create all the tables
#     ukbb_orm.Base.metadata.create_all(engine)

#     return sessionmaker(bind=engine), temp_file_name


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_input_files(config, **kwargs):
    """
    Take a config object and potential file paths and between the two return
    an index file object that can be used to parse the file

    Parameters
    ----------
    config : :obj`configparser.ConfigParser`
        The config file parser object
    wide_file : str, optional, default: NoneType
        The path to the wide UKBB wide fields file
    sample_file : str, optional, default: NoneType
        The path to a sample file that is associated with the BGEN files. This
        provides the "total" samples
    hes_events_file : str, optional, default: NoneType
        The path to the hes events file, this has the dates of operation events
    hes_diagnosis_file : str, optional, default: NoneType
        The path to the hes diagnosis file
    hes_operations_file : str, optional, default: NoneType
        The path to the hes operations file, this file has procedures and
        hospital stay length in it
    gp_clinical_file : str, optional, default: NoneType
        The GP clinical diagnosis and measures file
    gp_registrations_file : str, optional, default: NoneType
        The GP registration date file

    Returns
    -------
    index_files : dict
        The keys are the types of file and the values are a tuple of filename
        at [0] and a dict of csv_kwargs at [1]

    Raises
    ------
    KeyError
        if the file type is not defined as an argument or in the config file
    FileNotFoundError
        If the file is defined but can't be found on disk
    """
    index_files = {}
    for i in ['wide_file', 'hes_events_file', 'hes_diagnosis_file',
              'hes_operations_file', 'gp_clinical_file',
              'gp_registrations_file']:
        try:
            # Is it defined as an argument?
            filename = kwargs[i]

            # It has been defined but still could be None
            if filename is None:
                raise KeyError("file not defined")

            # Perform a basic open close check, to validate that the
            # file actually exists
            open(filename).close()
            index_files[i] = (filename, {})
            continue
        except KeyError:
            pass

        try:
            # If we get here then no file is defined and we are relying on a
            # file being defined in the config file
            section = config[i]
            csv_kwargs = exract_file_section_data(section)
        except KeyError as e:
            # No section defined
            raise KeyError("'{0}' is not defined") from e

        try:
            filename = section['file']
            open(filename).close()
        except KeyError as e:
            raise KeyError(
                "file path not defined in section '{0}'".format(i)
            ) from e
        index_files[i] = (filename, csv_kwargs)
    return index_files


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_output_files(config, error_on_missing=True):
    """
    Take a config object and potential file paths and between the two return
    an index file object that can be used to parse the file

    Parameters
    ----------
    config : :obj`configparser.ConfigParser`
        The config file parser object

    Returns
    -------
    output_files : list of :obj:`FileAttr`
        The keys are the types of file and the values are a tuple of filename
        at [0] and a dict of csv_kwargs at [1]

    Raises
    ------
    KeyError
        if the file type is not defined as an argument or in the config file
    FileNotFoundError
        If the file is defined but can't be found on disk
    """
    output_files = {}
    # These are the section names for the output files in the config file
    for i in [con.SAMPLE_FILE_NAME, con.CODE_FILE_NAME,
              con.CODE_MAPPING_LOG_FILE_NAME, con.CLIN_DATA_FILE_NAME,
              con.LONG_FILE_NAME_ENUM, con.LONG_FILE_NAME_STRING,
              con.LONG_FILE_NAME_NUMERIC, con.LONG_FILE_NAME_DATES]:
        try:
            # If we get here then no file is defined and we are relying on a
            # file being defined in the config file
            section = config[i]
            csvkwargs, encoding, nlines = \
                exract_file_section_data(section)
        except KeyError as e:
            # No section defined
            if error_on_missing is True:
                raise KeyError(
                    "outfile is not defined: '{0}'".format(i)
                ) from e
            continue

        try:
            filepath = os.path.expanduser(section['path'])
            open(filepath).close()
        except KeyError as e:
            if error_on_missing is True:
                raise KeyError(
                    "outfile path not defined in section '{0}'".format(i)
                ) from e
            continue

        except FileNotFoundError as e:
            if error_on_missing is True:
                raise FileNotFoundError(
                    "outfile can't be opened '{0}'".format(filepath)
                ) from e
            continue

        output_files[i] = FileAttr(
            name=i,
            path=filepath,
            encoding=encoding,
            nlines=nlines,
            csvkwargs=csvkwargs
        )
    return output_files


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def exract_file_section_data(section):
    """
    Extract the csv kwargs from the config file section. Allowing for the
    possibility that there might not be any

    Parameters
    ----------
    section : str
        A file section name from the config file

    Returns
    -------
    csv_kwargs : dict
        Arguments that can be supplied to csv.reader as **kwargs. The
        `encoding` parameter is also parsed even though it is not a csv kwarg
        it is still essential to parse the file correctly

    Raises
    ------
    TypeError
        If integer arguments can't be interpreted or if true/false arguments
        can't be interpreted
    """
    # TODO: clean this up (function out)
    nlines = None
    encoding = 'utf-8'
    csv_kwargs = {}

    # first deal this the arguments we have to interpret
    for i in ['strict', 'skipinitialspace', 'doublequote']:
        try:
            value = section[i]
        except KeyError:
            continue

        if value.lower() in ['true', 't']:
            csv_kwargs[i] = True
        elif value.lower() in ['false', 'f']:
            csv_kwargs[i] = False
        else:
            raise TypeError("unknown value '{0}' for {1}".format(value, i))

    # Numericals
    for i in ['quoting']:
        try:
            value = section[i]
            csv_kwargs[i] = int(value)
        except TypeError as e:
            raise TypeError("expected int for '{0}'".format(i)) from e
        except KeyError:
            pass

    # Strings, encoding has also been added even though it is not
    # a csv arg, it is still needed to process the file correctly
    for i in ['quotechar', 'lineterminator', 'escapechar', 'delimiter']:
        try:
            csv_kwargs[i] = section[i]
        except KeyError:
            pass

    try:
        encoding = section['encoding']
    except KeyError:
        pass

    try:
        nlines = int(section['nlines'])
    except KeyError:
        pass
    except TypeError:
        raise TypeError("expected int for '{0}'".format('nlines'))

    try:
        # This is a fudge to make sure tabs and spaces are represented
        # correctly from the config file
        # TODO: There must be a proper way of decoding them?
        if csv_kwargs['delimiter'] == r'\t':
            csv_kwargs['delimiter'] = "\t"
        elif csv_kwargs['delimiter'] == r'\s':
            csv_kwargs['delimiter'] = " "
    except KeyError:
        pass

    return csv_kwargs, encoding, nlines


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def initialise_config(config_file=None):
    """
    Initialise the config file
    """
    # attempt to loacate the path to the config file, if successful then make
    # sure the permissions are set correcly (only user readable). Finally, if
    # all ok then read in the config file
    config_file = get_config_file(config_file=config_file)
    check_config_permissions(config_file)
    return read_config_file(config_file)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == '__main__':
    main()
