"""
.. include:: ../docs/ukbb_orm.md
"""
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Boolean, Column, Integer, String, Float, Date, Text, \
    ForeignKey, SmallInteger
from sqlalchemy.orm import relationship
from pyaddons import sqlalchemy_helper

Base = declarative_base()

######################
# NAMING CONVENTIONS #
######################
# table names are plural
# classes/objects are singular
# relationships are plural


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class CodingId(Base):
    """
    A representation of objects within the `coding_ids` table. This is an
    integrity reference table
    """
    __tablename__ = 'coding_ids'

    coding_id = Column(Integer, nullable=False, primary_key=True)

    # -------------------------------------------------------------------------#
    coding_enums = relationship(
        "CodingEnum",
        back_populates='coding_ids'
    )
    field_ids = relationship(
        "FieldId",
        back_populates='coding_id_lookups'
    )
    # -------------------------------------------------------------------------#

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return sqlalchemy_helper.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class CodingEnum(Base):
    """
    A representation of objects within `coding_enums` table. These have the
    enumerations for all the coding_ids
    """
    __tablename__ = 'coding_enums'

    coding_enum_id = Column(
        Integer,
        nullable=False,
        primary_key=True,
        autoincrement=True
    )
    coding_id = Column(
        Integer,
        ForeignKey('coding_ids.coding_id'),
        index=True
    )
    # I looked at the field and the max string length is 200 so I will add a
    # bit more for good measure
    enum_value = Column(String(255), index=True)
    enum_name = Column(String(255), index=True)
    enum_desc = Column(Text)

    # ------------------------------------------------------------------------#
    coding_ids = relationship(
        "CodingId",
        back_populates='coding_enums'
    )
    enum_ukbb_data = relationship(
        "EnumUkbbData",
        back_populates='coding_enums'
    )
    # ------------------------------------------------------------------------#

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return sqlalchemy_helper.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class DataPath(Base):
    """
    A representation of the data_paths table
    """
    __tablename__ = 'data_paths'

    data_path_id = Column(
        Integer,
        nullable=False,
        primary_key=True,
        autoincrement=True
    )
    data_path_name = Column(
        String(255),
        index=True
    )

    # ------------------------------------------------------------------------#
    field_id_root_data_paths = relationship(
        'FieldId',
        primaryjoin="DataPath.data_path_id == FieldId.root_data_path_id",
        back_populates='root_data_paths'
    )
    field_id_deepest_data_paths = relationship(
        'FieldId',
        primaryjoin="DataPath.data_path_id == FieldId.deepest_data_path_id",
        back_populates='deepest_data_paths'
    )
    parent_data_paths = relationship(
        'DataPathRel',
        primaryjoin="DataPath.data_path_id == DataPathRel.parent_data_path_id",
        back_populates='parent_data_paths'
    )
    child_data_paths = relationship(
        'DataPathRel',
        primaryjoin="DataPath.data_path_id == DataPathRel.child_data_path_id",
        back_populates='child_data_paths'
    )
    # ------------------------------------------------------------------------#

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return sqlalchemy_helper.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class DataPathRel(Base):
    """
    A representation of the data_paths table
    """
    __tablename__ = 'data_path_rels'

    data_path_rel_id = Column(
        Integer,
        nullable=False,
        primary_key=True,
        autoincrement=True
    )
    parent_data_path_id = Column(
        Integer,
        ForeignKey('data_paths.data_path_id'),
        index=True
    )
    child_data_path_id = Column(
        Integer,
        ForeignKey('data_paths.data_path_id'),
        index=True
    )

    # ------------------------------------------------------------------------#
    parent_data_paths = relationship(
        'DataPath',
        foreign_keys=parent_data_path_id,
        back_populates='parent_data_paths'
    )
    child_data_paths = relationship(
        'DataPath',
        foreign_keys=child_data_path_id,
        back_populates='child_data_paths'
    )
    # ------------------------------------------------------------------------#

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return sqlalchemy_helper.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class FieldId(Base):
    """
    A representation of the field_ids table
    """
    __tablename__ = 'field_ids'

    field_id = Column(
        Integer,
        nullable=False,
        primary_key=True
    )
    field_desc = Column(String(255))
    field_text = Column(String(255))
    date_field_id = Column(Integer)
    n_instance = Column(Integer)
    array = Column(Integer)
    coding_id = Column(Integer, ForeignKey('coding_ids.coding_id'))
    data_type = Column(String(255))
    data_unit = Column(String(255))
    stability = Column(String(255))
    item_type = Column(String(255))
    strata = Column(String(255))
    sexed = Column(String(255))
    root_data_path_id = Column(Integer, ForeignKey('data_paths.data_path_id'))
    deepest_data_path_id = Column(
        Integer,
        ForeignKey('data_paths.data_path_id')
    )
    data_path_level = Column(Integer)
    data_path_str = Column(String(255))
    n_sample = Column(Integer)
    n_item = Column(Integer)
    note = Column(Text)
    url = Column(String(255))

    # ------------------------------------------------------------------------#
    root_data_paths = relationship(
        'DataPath',
        foreign_keys=root_data_path_id,
        back_populates='field_id_root_data_paths'
    )
    deepest_data_paths = relationship(
        'DataPath',
        foreign_keys=deepest_data_path_id,
        back_populates='field_id_deepest_data_paths'
    )
    coding_id_lookups = relationship(
        'CodingId',
        foreign_keys=coding_id,
        back_populates='field_ids'
    )
    ukbb_data_dates = relationship(
        "UkbbData",
        primaryjoin="FieldId.field_id == UkbbData.date_field_id",
        back_populates='date_fields'
    )
    ukbb_data = relationship(
        "UkbbData",
        primaryjoin="FieldId.field_id == UkbbData.field_id",
        back_populates='data_fields'
    )
    # ------------------------------------------------------------------------#

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return sqlalchemy_helper.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Sample(Base):
    """
    The main samples table this has the vital stats for the samples in it
    """
    __tablename__ = 'samples'

    eid = Column(Integer, nullable=False, primary_key=True)
    date_of_birth = Column(Date, nullable=False)
    date_of_birth_int = Column(Integer, nullable=False)
    date_of_assess = Column(Date, nullable=False)
    date_of_assess_int = Column(Integer, nullable=False)
    age_of_assess = Column(Float, nullable=False)
    self_reported_sex = Column(SmallInteger)
    genetic_sex = Column(SmallInteger)
    date_of_death = Column(Date)
    date_of_death_int = Column(Integer)
    age_of_death = Column(Float)
    no_of_assess = Column(SmallInteger, nullable=False)
    is_caucasian = Column(Boolean)
    is_genotyped = Column(Boolean)
    has_withdrawn = Column(Boolean)

    # ------------------------------------------------------------------------#
    clinical_data = relationship(
        "ClinicalData",
        back_populates='sample_info'
    )
    ukbb_data = relationship(
        "UkbbData",
        back_populates='sample_info'
    )
    # ------------------------------------------------------------------------#

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return sqlalchemy_helper.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class UkbbData(Base):
    """
    This holds data that appears to be clinical notes tagged to the clinical
    data record. It links back to the clinical_data table
    """
    __tablename__ = 'ukbb_data'

    ukbb_data_id = Column(Integer, nullable=False, primary_key=True,
                          autoincrement=True)
    eid = Column(Integer, ForeignKey('samples.eid'), nullable=False)
    field_id = Column(Integer, ForeignKey('field_ids.field_id'))
    date_field_id = Column(Integer, ForeignKey('field_ids.field_id'))
    date_field_inst_id = Column(Integer)
    date_array_id = Column(Integer)
    date = Column(Date)
    date_int = Column(Integer)
    days_from_baseline = Column(Integer)
    age_at_event = Column(Float)
    is_fallback_date = Column(Boolean)

    # -----------------------------------------------------------------------#
    sample_info = relationship(
        "Sample",
        back_populates="ukbb_data"
    )
    date_fields = relationship(
        "FieldId",
        foreign_keys=date_field_id,
        back_populates='ukbb_data_dates'
    )
    data_fields = relationship(
        "FieldId",
        foreign_keys=field_id,
        back_populates='ukbb_data'
    )
    enum_ukbb_data = relationship(
        "EnumUkbbData",
        back_populates='ukbb_data'
    )
    string_ukbb_data = relationship(
        "StringUkbbData",
        back_populates='ukbb_data'
    )
    date_ukbb_data = relationship(
        "DateUkbbData",
        back_populates='ukbb_data'
    )
    numeric_ukbb_data = relationship(
        "NumericUkbbData",
        back_populates='ukbb_data'
    )
    # -----------------------------------------------------------------------#


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class EnumUkbbData(Base):
    """
    This holds data that appears to be clinical notes tagged to the clinical
    data record. It links back to the clinical_data table
    """
    __tablename__ = 'enum_ukbb_data'

    data_point_id = Column(Integer, nullable=False, primary_key=True,
                           autoincrement=True)
    ukbb_data_id = Column(Integer, ForeignKey('ukbb_data.ukbb_data_id'),
                          nullable=False)
    instance_id = Column(Integer)
    array_id = Column(Integer)
    coding_enum_id = Column(Integer,
                            ForeignKey('coding_enums.coding_enum_id'))

    # -----------------------------------------------------------------------#
    ukbb_data = relationship(
        "UkbbData",
        back_populates='enum_ukbb_data'
    )
    coding_enums = relationship(
        "CodingEnum",
        back_populates='enum_ukbb_data'
    )
    # -----------------------------------------------------------------------#


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class StringUkbbData(Base):
    """
    This holds data that appears to be clinical notes tagged to the clinical
    data record. It links back to the clinical_data table
    """
    __tablename__ = 'string_ukbb_data'

    data_point_id = Column(Integer, nullable=False, primary_key=True,
                           autoincrement=True)
    ukbb_data_id = Column(Integer, ForeignKey('ukbb_data.ukbb_data_id'),
                          nullable=False)
    instance_id = Column(Integer)
    array_id = Column(Integer)
    data_point_value = Column(Text)

    # -----------------------------------------------------------------------#
    ukbb_data = relationship(
        "UkbbData",
        back_populates='string_ukbb_data'
    )
    # -----------------------------------------------------------------------#


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class DateUkbbData(Base):
    """
    This holds data that appears to be clinical notes tagged to the clinical
    data record. It links back to the clinical_data table
    """
    __tablename__ = 'date_ukbb_data'

    data_point_id = Column(Integer, nullable=False, primary_key=True,
                           autoincrement=True)
    ukbb_data_id = Column(Integer, ForeignKey('ukbb_data.ukbb_data_id'),
                          nullable=False)
    instance_id = Column(Integer)
    array_id = Column(Integer)
    data_point_value = Column(Date)

    # -----------------------------------------------------------------------#
    ukbb_data = relationship(
        "UkbbData",
        back_populates='date_ukbb_data'
    )
    # -----------------------------------------------------------------------#


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class NumericUkbbData(Base):
    """
    This holds data that appears to be clinical notes tagged to the clinical
    data record. It links back to the clinical_data table
    """
    __tablename__ = 'numeric_ukbb_data'

    data_point_id = Column(Integer, nullable=False, primary_key=True,
                           autoincrement=True)
    ukbb_data_id = Column(Integer, ForeignKey('ukbb_data.ukbb_data_id'),
                          nullable=False)
    instance_id = Column(Integer)
    array_id = Column(Integer)
    data_point_value = Column(Float)

    # -----------------------------------------------------------------------#
    ukbb_data = relationship(
        "UkbbData",
        back_populates='numeric_ukbb_data'
    )
    # -----------------------------------------------------------------------#


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class CareType(Base):
    """
    A representation of the `care_types` lookup table this has a one to many
    relationship with `clinical_data` table
    """
    __tablename__ = 'care_types'

    care_type_id = Column(
        Integer,
        nullable=False,
        primary_key=True,
        autoincrement=True
    )
    care_type_desc = Column(String(15))

    # -----------------------------------------------------------------------#
    clinical_data = relationship(
        "ClinicalData",
        back_populates='care_types'
    )
    # -----------------------------------------------------------------------#

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return sqlalchemy_helper.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class RecordType(Base):
    """
    A representation of the `record_types` lookup table this has a one to many
    relationship with `clinical_data` table
    """
    __tablename__ = 'record_types'

    record_type_id = Column(Integer, nullable=False, primary_key=True,
                            autoincrement=True)
    record_type_desc = Column(String(15))

    # -----------------------------------------------------------------------#
    clinical_data = relationship(
        "ClinicalData",
        back_populates='record_types'
    )
    # -----------------------------------------------------------------------#

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return sqlalchemy_helper.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class DataProvider(Base):
    """
    A representation of the `data_providers` lookup table this has a one to many
    relationship with `clinical_data` table
    """
    __tablename__ = 'data_providers'

    data_provider_id = Column(Integer, nullable=False, primary_key=True,
                              autoincrement=True)
    data_provider_desc = Column(String(15))

    # -------------------------------------------------------------------------#
    clinical_data = relationship(
        "ClinicalData",
        back_populates='data_providers'
    )
    # -------------------------------------------------------------------------#

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return sqlalchemy_helper.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Pct(Base):
    """
    A representation of the `pcts` lookup table this has a one to many
    relationship with `clinical_data` table
    """
    __tablename__ = 'pcts'

    pct_id = Column(
        Integer,
        nullable=False,
        primary_key=True,
        autoincrement=True
    )
    nhs_pct_id = Column(String(10))
    pct_desc = Column(String(15))

    # -------------------------------------------------------------------------#
    clinical_data = relationship(
        "ClinicalData",
        back_populates='pcts'
    )
    # -------------------------------------------------------------------------#

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return sqlalchemy_helper.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class CodingSystem(Base):
    """
    A representation of the coding_systems table
    """
    __tablename__ = 'coding_systems'

    coding_system_id = Column(
        Integer,
        nullable=False,
        primary_key=True,
        autoincrement=True
    )
    coding_system_name = Column(
        String(15)
    )

    # -------------------------------------------------------------------------#
    clinical_codes = relationship(
        "ClinicalCode",
        back_populates='coding_systems'
    )
    # -------------------------------------------------------------------------#

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return sqlalchemy_helper.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class ClinicalCode(Base):
    """
    A representation of the `clinical_codes` table
    """
    __tablename__ = 'clinical_codes'

    clinical_code_id = Column(
        Integer,
        nullable=False,
        primary_key=True,
        autoincrement=True
    )
    clinical_code = Column(
        String(255),
        index=True
    )
    coding_system_id = Column(
        Integer, ForeignKey('coding_systems.coding_system_id')
    )
    ncui = Column(Integer, nullable=False, default=0)
    min_n_value = Column(Integer, nullable=False)
    max_n_value = Column(Integer, nullable=False)
    n_occur = Column(Integer, nullable=False)
    n_is_empty = Column(Integer, nullable=False)
    n_is_string = Column(Integer, nullable=False)
    n_is_integer = Column(Integer, nullable=False)
    n_is_float = Column(Integer, nullable=False)
    ratio_3_0_0 = Column(Integer, nullable=False)
    ratio_0_3_0 = Column(Integer, nullable=False)
    ratio_0_0_3 = Column(Integer, nullable=False)
    ratio_2_1_0 = Column(Integer, nullable=False)
    ratio_2_0_1 = Column(Integer, nullable=False)
    ratio_1_2_0 = Column(Integer, nullable=False)
    ratio_0_2_1 = Column(Integer, nullable=False)
    ratio_1_0_2 = Column(Integer, nullable=False)
    ratio_0_1_2 = Column(Integer, nullable=False)
    ratio_1_1_1 = Column(Integer, nullable=False)

    # -------------------------------------------------------------------------#
    clinical_data = relationship(
        "ClinicalData",
        back_populates='clinical_codes'
    )
    coding_systems = relationship(
        "CodingSystem",
        back_populates='clinical_codes'
    )
    clinical_code_to_descs = relationship(
        "ClinicalCodeToDesc",
        back_populates='clinical_codes'
    )
    clinical_code_to_cuis = relationship(
        "ClinicalCodeToCui",
        back_populates='clinical_codes'
    )
    # -------------------------------------------------------------------------#

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return sqlalchemy_helper.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class ClinicalCodeDesc(Base):
    """
    A representation of the `clinical_code_descs` table. This is a store of
    all the clinical code descriptions
    """
    __tablename__ = 'clinical_code_descs'

    clinical_code_desc_id = Column(
        Integer,
        nullable=False,
        primary_key=True,
        autoincrement=True
    )
    clinical_code_desc = Column(
        String(255),
        index=True
    )

    # -------------------------------------------------------------------------#
    clinical_code_to_descs = relationship(
        "ClinicalCodeToDesc",
        back_populates='clinical_code_descs'
    )
    umls_cui_to_descs = relationship(
        "UmlsCuiToDesc",
        back_populates='clinical_code_descs'
    )
    # -------------------------------------------------------------------------#

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return sqlalchemy_helper.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class ClinicalCodeToDesc(Base):
    """
    A representation of the `clinical_code_to_descs` table. This is an
    association table between `clinical_codes` and `clinical_code_descs`
    """
    __tablename__ = 'clinical_code_to_descs'

    clinical_code_to_desc_id = Column(
        Integer,
        nullable=False,
        primary_key=True,
        autoincrement=True
    )
    clinical_code_id = Column(
        Integer,
        ForeignKey('clinical_codes.clinical_code_id'),
        index=True
    )
    clinical_code_desc_id = Column(
        Integer,
        ForeignKey('clinical_code_descs.clinical_code_desc_id')
    )

    # -------------------------------------------------------------------------#
    clinical_codes = relationship(
        "ClinicalCode",
        back_populates= 'clinical_code_to_descs'
    )
    clinical_code_descs = relationship(
        "ClinicalCodeDesc",
        back_populates= 'clinical_code_to_descs'
    )
    # -------------------------------------------------------------------------#

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return sqlalchemy_helper.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class UmlsCui(Base):
    """
    A representation of the `temp_event` table
    """
    __tablename__ = 'umls_cuis'

    umls_cui_id = Column(Integer, nullable=False, primary_key=True,
                    autoincrement=True)
    umls_cui = Column(String(10), nullable=False, index=True)
    nterms = Column(Integer, nullable=False, default=0)

    # -------------------------------------------------------------------------#
    umls_cui_to_stys = relationship(
        "UmlsCuiToSty",
        back_populates='umls_cuis'
    )
    clinical_code_to_cuis = relationship(
        "ClinicalCodeToCui",
        back_populates='umls_cuis'
    )
    umls_cui_to_descs = relationship(
        "UmlsCuiToDesc",
        back_populates='umls_cuis'
    )
    # -------------------------------------------------------------------------#

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return sqlalchemy_helper.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class UmlsCuiToDesc(Base):
    """
    A representation of the UMLS Cui concept to CUI concept description
    """
    __tablename__ = 'umls_cui_to_descs'

    umls_cui_to_desc_id = Column(
        Integer,
        nullable=False,
        primary_key=True,
        autoincrement=True
    )
    umls_cui_id = Column(
        Integer,
        ForeignKey('umls_cuis.umls_cui_id'),
        index=True
    )
    clinical_code_desc_id = Column(
        Integer,
        ForeignKey('clinical_code_descs.clinical_code_desc_id'),
        index=True
    )

    # -------------------------------------------------------------------------#
    umls_cuis = relationship(
        "UmlsCui",
        back_populates='umls_cui_to_descs'
    )
    clinical_code_descs = relationship(
        "ClinicalCodeDesc",
        back_populates='umls_cui_to_descs'
    )
    # -------------------------------------------------------------------------#

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return sqlalchemy_helper.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class UmlsSty(Base):
    """
    A representation of the `umls_stys` table. These are semantic types that
    occur within the UMLS
    """
    __tablename__ = 'umls_stys'

    umls_sty_id = Column(Integer, nullable=False, primary_key=True,
                    autoincrement=True)
    umls_sty_desc = Column(String(255), nullable=False)

    # -------------------------------------------------------------------------#
    umls_cui_to_stys = relationship(
        "UmlsCuiToSty",
        back_populates='umls_stys'
    )
    # -------------------------------------------------------------------------#

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return sqlalchemy_helper.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class UmlsCuiToSty(Base):
    """
    A representation of the `umls_cui_to_stys` table. This is an association
    table that models the many:many relationships between UMLS concepts and
    their semantic types.
    """
    __tablename__ = 'umls_cui_to_stys'

    umls_cui_to_sty_id = Column(
        Integer,
        nullable=False,
        primary_key=True,
        autoincrement=True
    )
    umls_cui_id = Column(
        Integer,
        ForeignKey('umls_cuis.umls_cui_id')
    )
    umls_sty_id = Column(
        Integer,
        ForeignKey('umls_stys.umls_sty_id')
    )

    # -------------------------------------------------------------------------#
    umls_cuis = relationship(
        "UmlsCui",
        back_populates='umls_cui_to_stys'
    )
    umls_stys = relationship(
        "UmlsSty",
        back_populates='umls_cui_to_stys'
    )
    # -------------------------------------------------------------------------#

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return sqlalchemy_helper.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class ClinicalCodeToCui(Base):
    """
    A representation of the `clinical_code_to_cuis` table. This is an
    association table that models the many:many relationships between UMLS
    concepts and their semantic types.
    """
    __tablename__ = 'clinical_code_to_cuis'

    clinical_code_to_cui_id = Column(Integer, nullable=False, primary_key=True,
                                     autoincrement=True)
    clinical_code_id = Column(
        Integer,
        ForeignKey('clinical_codes.clinical_code_id'),
        index=True
    )
    cui_id = Column(
        Integer,
        ForeignKey('umls_cuis.umls_cui_id'),
        index=True
    )

    # -------------------------------------------------------------------------#
    clinical_codes = relationship(
        "ClinicalCode",
        back_populates='clinical_code_to_cuis'
    )
    umls_cuis = relationship(
        "UmlsCui",
        back_populates='clinical_code_to_cuis'
    )
    # -------------------------------------------------------------------------#

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return sqlalchemy_helper.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class ClinicalData(Base):
    """
    """
    __tablename__ = 'clinical_data'

    event_id = Column(Integer, nullable=False, primary_key=True)
    episode_idx = Column(Integer, nullable=False)
    eid = Column(
        Integer,
        ForeignKey('samples.eid'),
        index=True
    )
    event_start = Column(Date)
    event_start_int = Column(Integer)
    age_at_event = Column(Float)
    days_from_baseline = Column(Integer)
    days_pre_event = Column(Integer)
    days_post_event = Column(Integer)
    clinical_code_id = Column(
        Integer,
        ForeignKey('clinical_codes.clinical_code_id'),
        index=True
    )
    event_level = Column(Integer)
    care_type_id = Column(
        Integer,
        ForeignKey('care_types.care_type_id')
    )
    record_type_id = Column(
        Integer,
        ForeignKey('record_types.record_type_id')
    )
    data_provider_id = Column(
        Integer,
        ForeignKey('data_providers.data_provider_id')
    )
    data_provider_format_id = Column(
        Integer
    )
    pct_id = Column(
        Integer,
        ForeignKey('pcts.pct_id')
    )
    days_to_pct = Column(Integer)
    pct_is_gp = Column(Boolean)
    is_clinical_measure = Column(Boolean)
    n_numeric = Column(Integer)
    n_unit = Column(Integer)
    n_string = Column(Integer)
    n_note = Column(Integer)

    # ------------------------------------------------------------------------#
    sample_info = relationship(
        "Sample",
        back_populates='clinical_data'
    )
    numeric_clin_data = relationship(
        "NumericClinData",
        back_populates='clinical_data'
    )
    unit_clin_data = relationship(
        "UnitClinData",
        back_populates='clinical_data'
    )
    string_clin_data = relationship(
        "StringClinData",
        back_populates='clinical_data'
    )
    note_clin_data = relationship(
        "NoteClinData",
        back_populates='clinical_data'
    )
    clinical_codes = relationship(
        "ClinicalCode",
        back_populates='clinical_data'
    )
    care_types = relationship(
        "CareType",
        back_populates='clinical_data'
    )
    record_types = relationship(
        "RecordType",
        back_populates='clinical_data'
    )
    data_providers = relationship(
        "DataProvider",
        back_populates='clinical_data'
    )
    pcts = relationship(
        "Pct",
        back_populates='clinical_data'
    )
    # ------------------------------------------------------------------------#

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return sqlalchemy_helper.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class NumericClinData(Base):
    """
    The numerics table stores all the numerical clinical data as floats and
    is linked back to the main clinical data table
    """
    __tablename__ = 'numeric_clin_data'

    numeric_id = Column(Integer, nullable=False, primary_key=True,
                        autoincrement=True)
    event_id = Column(Integer, ForeignKey('clinical_data.event_id'),
                      index=True)
    raw_value = Column(Float)
    standard_value = Column(Float)
    units = Column(String(30))
    flag = Column(Boolean, nullable=False)

    clinical_data = relationship(
        "ClinicalData",
        back_populates='numeric_clin_data'
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return sqlalchemy_helper.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class UnitClinData(Base):
    """
    The units table stores any units that may have been stored in the clinical
    data record
    """
    __tablename__ = 'unit_clin_data'

    units_id = Column(Integer, nullable=False, primary_key=True,
                      autoincrement=True)
    event_id = Column(Integer, ForeignKey('clinical_data.event_id'))
    unit_value = Column(String(255))

    clinical_data = relationship(
        "ClinicalData",
        back_populates='unit_clin_data'
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return sqlalchemy_helper.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class StringClinData(Base):
    """
    This holds miscellaneous string data that is associated with the clinical
    _data record and does not appear to be a clinical note or a unit of
    measurement
    """
    __tablename__ = 'string_clin_data'

    units_id = Column(Integer, nullable=False, primary_key=True,
                      autoincrement=True)
    event_id = Column(Integer, ForeignKey('clinical_data.event_id'))
    string_value = Column(String(255))

    clinical_data = relationship(
        "ClinicalData",
        back_populates='string_clin_data'
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return sqlalchemy_helper.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class NoteClinData(Base):
    """
    This holds data that appears to be clinical notes tagged to the clinical
    data record. It links back to the clinical_data table
    """
    __tablename__ = 'note_clin_data'

    units_id = Column(Integer, nullable=False, primary_key=True,
                      autoincrement=True)
    event_id = Column(Integer, ForeignKey('clinical_data.event_id'))
    note_value = Column(Text)

    clinical_data = relationship(
        "ClinicalData",
        back_populates='note_clin_data'
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return sqlalchemy_helper.print_orm_obj(self)
