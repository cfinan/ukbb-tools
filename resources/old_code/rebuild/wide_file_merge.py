"""
.. include:: ../docs/wide_file_merge.md
"""
from ukbb_tools import __version__, __name__ as pkg_name, \
    wide_file_common as wfc, wide_file_index as wfi, constants as con
from simple_progress import progress
from Bio import bgzf
import argparse
import sys
import os
import csv
import warnings
import pprint as pp


MSG_PREFIX = '[info]'
MSG_OUT = sys.stderr
EID_COL_NAME = 'eid'
# WITHDRAWN_COL_NAME = 'withdrawn'


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main():
    """
    The main entry point to merge two two wide files together
    """
    # Initialise and parse the command line arguments
    parser = init_cmd_args()
    args = parse_cmd_args(parser)

    # Start a msg outputter, this will respect the verbosity and will output to
    # STDERR
    m = progress.Msg(prefix='', verbose=args.verbose, file=MSG_OUT)
    m.msg("= UKBB wide file merge ({0} v{1}) =".format(
        pkg_name, __version__))
    m.prefix = MSG_PREFIX
    m.msg_atts(args)

    # Make sure the input files can be opened
    open(args.old_file_path).close()
    open(args.new_file_path).close()

    merge_wide_files(
        args.old_file_path, args.new_file_path, args.out_file_path,
        old_encoding=args.old_encoding, new_encoding=args.new_encoding,
        verbose=args.verbose
    )
    m.msg("*** END ***")


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def init_cmd_args():
    """
    Initialise the command line arguments and return the parsed arguments

    Returns
    -------
    parser : :obj:`argparse.ArgumentParser`
        The argument parser with the arguments set up
    """
    parser = argparse.ArgumentParser(
        description="Merge two UKBB wide files together using minimal memory")

    # The wide format UKBB data fields filex
    parser.add_argument('old_file_path', type=str,
                        help="The path to old UKBB wide file")
    parser.add_argument('new_file_path', type=str,
                        help="The path to new UKBB wide file")
    parser.add_argument('out_file_path', type=str,
                        help="The path to output UKBB wide file")
    parser.add_argument('-v', '--verbose',  action="store_true",
                        help="Give more output")
    parser.add_argument('--old-encoding', type=str, default='utf-8',
                        help="The encoding to use when reading the old file"
                        ", for some reason many UK BioBank files have latin1 "
                        "encoding and some of the characters are not "
                        "compatible with UTF8")
    parser.add_argument('--new-encoding', type=str, default='utf-8',
                        help="The encoding to use when reading the new file"
                        ", for some reason many UK BioBank files have latin1 "
                        "encoding and some of the characters are not "
                        "compatible with UTF8")

    return parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_cmd_args(parser):
    """
    Parse the command line arguments

    Parameters
    ----------
    parser : :obj:`argparse.ArgumentParser`
        The argument parser with the arguments set up

    Returns
    -------
    args : :obj:`argparse.Namespace`
        The argparse namespace object
    """
    args = parser.parse_args()

    # Make sure ~/ etc... is expanded
    for i in ['old_file_path', 'new_file_path', 'out_file_path']:
        setattr(args, i, os.path.expanduser(getattr(args, i)))

    return args


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def merge_wide_files(old_file, new_file, merged_file,
                     old_csv_kwargs=None, new_csv_kwargs=None,
                     old_index_file=None, new_index_file=None,
                     old_encoding='utf-8', new_encoding='utf-8',
                     verbose=False):
    """
    Merge two wide files togther. Columns in the new_file have preference to
    those in the old file. Samples missing in the new file that are present
    in the old_file are set to withdrawn. Samples present in the new file that
    are not in the old file will produce warnings

    Parameters
    ----------
    old_file : str
        The path to the old file. The old file may or not be
        compressed/indexed
    new_file : str
        The path to the new file. The new file may or not be
        compressed/indexed
    old_csv_kwargs : dict or NoneType, optional, default: NoneType
        Any keyword arguments that will be passed to the `csv.reader`. If not
        defined then the dialect will be sniffed by `csv`
    new_csv_kwargs : dict or NoneType, optional, default: NoneType
        Any keyword arguments that will be passed to the `csv.reader`. If not
        defined then the dialect will be sniffed by `csv`
    old_index_file : str or NoneType, optional, default: NoneType
        An alternative location for the index file to the old file. The
        default is to look in the same path as the old file but add a `.idx`
        on the end. If the old file has not been indexed then it will be prior
        to merging
    new_index_file : str or NoneType, optional, default: NoneType
        An alternative location for the index file to the new file. The
        default is to look in the same path as the new file but add a `.idx`
        on the end. If the new file has not been indexed then it will be prior
        to merging
    old_encoding : `str`, optional, default: `utf-8`
        The encoding to use when reading in the old file in the event that.
        An index needs creating on it. Some UKBioBank files are latin1 encoded.
    new_encoding : `str`, optional, default: `utf-8`
        The encoding to use when reading in the new file in the event that.
        An index needs creating on it. Some UKBioBank files are latin1 encoded.
    verbose : bool, optional, default: False
        Report the various stages of progress
    """
    m = progress.Msg(verbose=verbose, file=MSG_OUT, prefix=MSG_PREFIX)

    # Make sure the kwargs are dicts not NoneType
    old_csv_kwargs = old_csv_kwargs or {}
    new_csv_kwargs = new_csv_kwargs or {}

    # Get a valid path name for the index files _set_index ensures that both
    # the input files have been indexed prior to merging
    old_file, old_index_file = _set_index(
        old_file, old_index_file, verbose=verbose, encoding=old_encoding,
        **old_csv_kwargs
    )

    new_file, new_index_file = _set_index(
        new_file, new_index_file, verbose=verbose, encoding=new_encoding,
        **new_csv_kwargs
    )

    # The name for the merged index file
    merged_index_file = wfc.get_index_path(merged_file)

    # Get the row index readers, we do not need to do any parsing as such
    # as we are not using any of the values for anything so we will work
    # directly from the rows returned by the indexers
    with wfi.open(old_file, index_file=old_index_file) as old_idx:
        with wfi.open(new_file, index_file=new_index_file) as new_idx:
            m.msg("old index has {0} samples".format(old_idx.n_samples))
            m.msg("new index has {0} samples".format(new_idx.n_samples))

            # The samples represented in both files
            sample_union = get_sample_union(old_idx, new_idx)
            m.msg("union has {0} samples".format(len(sample_union)))

            # The header map is a list of tuples representing the what the
            # combined eader of both files will be like after merging (sorted
            # on field ID). The first element of each tuple is the index column
            # of that field in the the old file, the second element is the
            # index of that field in the second file and the third element is
            # the actual field ID used in the header. If any of the fields are
            # not represented in the old/new files then their column index
            # will be set to NoneType
            header_map = get_header_map(old_idx, new_idx)
            m.msg("old index has {0} columns".format(len(old_idx.header)))
            m.msg("new index has {0} columns".format(len(new_idx.header)))
            # pp.pprint(header_map)
            # print(len(header_map))
            _merge_files(
                merged_file,
                merged_index_file,
                old_idx,
                new_idx,
                sample_union,
                header_map,
                verbose=verbose
            )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _set_index(file_name, file_index, verbose=False, encoding='utf-8',
               **csv_kwargs):
    """
    Attempt to locate the index file and if not then and index is created

    Parameters
    ----------
    file_name : str
        The path to the wide file
    file_index : str or NoneType
        The path to the index file, or NoneType if it has not been created
    verbose : bool, optional, default: False
        If an index does need to be created then report progress
    encoding : `str`, optional, default: `utf-8`
        The encoding to use when reading in the file in for indexing. Some
        UKBioBank files are latin1 encoded.
    **csv_kwargs
        If defined then these are used as arguments to parse the wide file
        if not then the csvDialect is sniffed out

    Returns
    -------
    file_name : str
        The path to a wide file that is 100% indexed
    index_file : str
        The index file for the wide file
    index : dict
        A dictionary of index mappings, with eid (int) keys and seek
        positions (int) values
    """
    m = progress.Msg(verbose=verbose, file=MSG_OUT, prefix=MSG_PREFIX)
    m.msg("checking index for '{0}'".format(file_name))

    # This tracks how many recursive calls this function has had. If the index
    # does not exist then it is created and this function is re-called to
    # validate it's present. If it still does not exist and calls is not 0 then
    # we error out to avoid an infinte loop
    calls = csv_kwargs.pop('calls', 0)

    # Get the name of the index file to check
    index_file = wfc.get_index_path(file_name, index_file=file_index)

    try:
        # Doe the index exist
        open(index_file).close()

        # I have commented out validation as they will be validated
        # when the index is opened, and we are validating twice (which is slow)
        # idx = wfi.IndexReader.validate_index(index_file, wide_file=file_name)
        m.msg("index exists '{0}'".format(index_file))
        return file_name, index_file
    except (FileNotFoundError, TypeError) as e:
        # The TypeError should be for a NoneType index, if not then we want
        # to know about it
        if e.__class__ == TypeError and file_index is not None:
            raise

        # Index not present, so attempt to build or error out if we already
        # have attempted to build
        if calls > 0:
            raise IndexError("for some reason the index will not build")

        # Define the outfile name this will be the same as the infile name but
        # with .gz added. If the infile name already has .gz then it will be
        # overwritten. In this instance we are safe as all indexed files are
        # always written via temp and if they error out early then the temp
        # file is deleted, i.e. we do not leave half-written data files
        outfile = file_name
        if not outfile.endswith('.gz'):
            outfile = '{0}.gz'.format(outfile)

        # Now get the new index file name for our (potentially) new output file
        index_file = wfc.get_index_path(outfile)

        m.msg("building index '{0}'".format(outfile))
        wfi.build_index_file(
            file_name,
            outfile,
            index_file,
            encoding=encoding,
            verbose=verbose,
            **csv_kwargs
        )

        # Now check again
        return _set_index(outfile, index_file, verbose=verbose, calls=calls+1,
                          **csv_kwargs)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_sample_union(old_idx, new_idx):
    """
    Get an intersection of the eids and return in sort order

    Parameters
    ----------
    old_idx : :obj:`wide_file_index.IndexReader`
        The index reader for the old file
    new_idx : :obj:`wide_file_index.IndexReader`
        The index reader for the new file

    Returns
    -------
    sample_union : list of int
        A sorted union of samples from the old file and the new file
    """
    sample_union = list(set(old_idx.samples).union(new_idx.samples))
    sample_union.sort()
    return sample_union


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_header_map(old_idx, new_idx):
    """
    Get an intersection of the eids and return in sort order

    Parameters
    ----------
    old_idx : :obj:`wide_file_index.IndexReader`
        The index reader for the old file
    new_idx : :obj:`wide_file_index.IndexReader`
        The index reader for the new file

    Returns
    -------
    header_map : list of tuple
        A sorted (on field_id, instance, array) union of columns from the
        old file and the new file. At element [0] is the index of the column
        in the old file, at [1] is the index of the column in the new_file
        at [2] is the column name (as a string)

    Raises
    ------
    KeyError
        If the new file has a withdrawn column
    """
    header_map = {}
    # Get the element numbers of the respective columns in each header
    for c, i in enumerate(wfc.split_header(old_idx.header)):
        # initialise and set the index for the old file
        header_map[i] = [c, None]

    # New file header
    for c, i in enumerate(wfc.split_header(new_idx.header)):
        try:
            # Update the new file position
            header_map[i][1] = c
        except KeyError:
            # Column was not in the old file so now we added it
            header_map[i] = [None, c]

    header_str = []

    # Here we want to test if the header has our homemade withdrawn data field
    # If only the the old file as it then it is all good, however if the new
    # file has it then it is an error as the new file should not have it
    try:
        header_map[con.WITHDRAWN]
    except KeyError:
        # Not in either file so add it
        header_map[con.WITHDRAWN] = [None, None]

    if header_map[con.WITHDRAWN][1] is not None:
        raise KeyError("new file should not have a withdawn column")

    withdrawn_idx = None
    # Finally make into a sorted list
    for c, k in enumerate(sorted(header_map.keys())):
        # We are making a note of the index of the withdrawn column, I may not
        # need this but just in case
        if k == con.WITHDRAWN:
            withdrawn_idx = c

        try:
            # The toggle index is a variable that indicates if we want to
            # select from element 0 (old file) or element 1 (new file)
            # the int cast below tests if the field is present in the new file
            # if it is not then it will be NoneType and it will fail with a
            # type error. If it does not fail, we set to True (select from
            # element 1 - the new file). So if a field is present in both files
            # we default to selecting from the new file
            int(header_map[k][1])
            toggle_idx = True
        except TypeError:
            # Here the field (column) is not present in the new file so we want
            # to select from index [0] (the old file)
            toggle_idx = False

        # Build the structure of the output header
        header_str.append(
            (
                header_map[k][0],  # column number (0-based) in old file
                header_map[k][1],  # column number (0-based) in new file
                toggle_idx,  # select from old file (False) new file (True)
                k  # The file_if, instance, array value
            )
        )

    return header_str


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _merge_files(merged_file, merged_index_file, old_idx, new_idx,
                 sample_union, header_map, verbose=False):
    """
    Do the merge based on indexes

    Parameters
    ----------
    merged_file : str
        The path to output the merge file
    merged_index_file : str
        The path to output the merged index file
    old_idx : :obj:`wide_file_index.IndexReader`
        The index reader for the old file
    new_idx : :obj:`wide_file_index.IndexReader`
        The index reader for the new file
    sample_union : list of int
        A sorted union of samples from the old file and the new file
    header_map : list of tuple
        A sorted (on field_id, instance, array) union of columns from the
        old file and the new file. At element [0] is the index of the column
        in the old file, at [1] is the index of the column in the new_file
        at [2] is the column name (as a string)
    verbose : bool, optional, default: False
        Report the various stages of progress
    """
    debug = -1000
    # Build the header line for the merged file. The header map does not have
    # any eid or wothdrawn columns in it, so these are output at the begining
    # and are always output into merged files
    header = [EID_COL_NAME] + \
        ["{0}-{1}.{2}".format(*(i[3])) for i in header_map]

    # We grab the index of the withdrawn column. It should be loacted at 1
    # (after the eid) but I will not hard code that in
    withdrawn_idx = [idx for idx, i in enumerate(
        header_map) if i[3] == con.WITHDRAWN]

    # Sanity check, this should never happen
    if len(withdrawn_idx) == 1:
        # Get the scalar, we are adding 1 to it as we want to account for the
        # eid column
        withdrawn_idx = withdrawn_idx[0] + 1
    else:
        raise IndexError(
            "expected 1 withdrawn column but got '{0}'".format(
                len(withdrawn_idx))
        )

    # Will hold the mappings between the eids (as ints) and their seek
    # positions
    idx = {}

    # Write to a temp file first that is reloacted when the context manager
    # exits. This is a bgzipped file
    with wfc.write_via_temp(
            merged_file,
            write_method=bgzf.BgzfWriter
    ) as outbgz:
        # Here we know how many rows we will be outputting, so our progress
        # monitor can be more informative
        prog = progress.RemainingProgress(
            len(sample_union),
            verbose=verbose,
            file=MSG_OUT,
            default_msg="merging files..."
        )

        # The merged file will always be tab-delimited
        writer = csv.writer(outbgz, delimiter="\t")

        # Write the header line. The data will start after the header
        writer.writerow(header)
        counter = 0
        for eid in prog.progress(sample_union):
            # The seek position for a sample will be after the previous line
            # has been written in
            idx[eid] = outbgz.tell()

            # Get the row data for the old and the new file
            old_row, old_eid, old_withdrawn, force_old = \
                _fetch_row(old_idx, eid, is_old_idx=True)
            new_row, new_eid, new_withdrawn, force_old = \
                _fetch_row(new_idx, eid, is_old_idx=False)

            # # A quick sanity check that the two rows have returned the same
            # # sample IDs
            # assert old_lead_cols[0] == new_lead_cols[0], "eid missmatch"
            # assert int(old_lead_cols[0]) == eid,  "eid missmatch"

            # We place the row data in a nested list such that the index of the
            # nexted list matches the old file/new file selector
            select_row = [old_row, new_row]

            # Now we initialise the output row with the lead columns of the eid
            # (sample ID) and the withdrawn value. The withdrawn value will be
            # True if any of the old/new files has a withdrawn flag
            outrow = [eid]

            # Loop through the header
            for mappings in header_map:
                # Define the selector this will evaluate to True (select from
                # element 1 (new file)) or select from element 0 (old file)
                # force_old is an override that ensures selection from the
                # old file in the case when the row is missing from the new
                # file
                selector = mappings[2] & (not force_old)

                # This is the index in the selected data set that contains the
                # data ppoint we want
                select_idx = mappings[selector]

                try:
                    # store the data point
                    selected_value = select_row[selector][select_idx]
                except TypeError:
                    if mappings[3] != con.WITHDRAWN:
                        raise ValueError(
                            "no data for field '{0}' and '{1}'".format(
                                mappings[0], eid)
                        )
                    # If we get here then it means the error was caused by
                    # neither the old file or the new file having a withdrawn
                    # column, so we do not want to error out as it is not a
                    # real error
                    selected_value = False
                outrow.append(selected_value)

                # Compare the data points - not that this doubles the run time
                # but I will keep it in for now as I want to make surethere is
                # nothing nasty going on with data values shifting array
                # positions as this would compromise the merging algorithm and
                # I would have to think again
                try:
                    other_idx = int(mappings[not selector])
                    other_value = select_row[not selector][other_idx]
                    if other_value != selected_value:
                        warnings.warn(
                            "{3}: value missmatch in '{0}', '{1}' != "
                            "'{2}'".format(mappings[3], selected_value,
                                           other_value, eid)
                        )
                except TypeError:
                    # Not comparable as one is undefined
                    pass

            # Update the withdrawn status of the sample, here if any of the
            # values are True, we will evaluate to True
            outrow[withdrawn_idx] = int(
                outrow[withdrawn_idx] | old_withdrawn | new_withdrawn
            )

            writer.writerow(outrow)
            if counter == debug:
                break
            counter += 1
    wfi.write_index(merged_index_file, header, idx)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _fetch_row(idx_reader, eid, is_old_idx=False):
    """
    Get a sample row from the index reader

    Parameters
    ----------
    idx_reader : :obj:`wide_file_index.IndexReader`
        The index reader for the wide file
    eid : int
        The sample row that we want
    is_old_idx : bool, optional, default: False
        The govens if a warning is issued when a sample is not located. The
        assumption behind the algorithm is that the old file is a superset
        of the new file (i.e. samples are not added they can only withdraw)
        so if a sample is not present in the old file  we need to infrom the
        user that they might be doing something wrong

    Returns
    -------
    row : list of str
        The extracted field data
    lead_cols : list of int
        The first element is the eid and the second element is a boolean
        withdrawn flag
    force_old : bool
        Force data value selection from the old file. This occures when there
        is no data in the new file
    """
    # The default situation is that when we merge the data, for overlapping
    # columns we want to use the data from the new file before the old as the
    # assumption is that any erronious data points will be fixed in the new
    # file. However, if the sample is not present in the new file then a dummy
    # blank row is created for the new file. In this case it does not make
    # sense to prioritise the dummy data from the new file, so we will use the
    # data from the old file. Note that the merging does not happen in this
    # function, we only toggle the decision variable in this function
    force_old = False
    try:
        # Get the sample row
        row = idx_reader.fetch_row(eid)
        extracted_eid = int(row.pop(0))
        withdrawn = False
#         lead_cols = _pad_withdrawn(lead_cols)
        # lead_cols[1] = bool(lead_cols[1])
    except KeyError:
        # Sample not present in the index, so the assumption is that they
        # have withdrawn
        withdrawn = True
        if is_old_idx is True:
            # Although is it is an old file index we change our mind and
            # issue a warning
            withdrawn = False
            warnings.warn(
                "sample: {0} not present in old file, old file"
                " should be a superset of the new file".format(eid)
            )
        else:
            # Here the sample is not present in the new file so we want to make
            # sure that we take the data from the old file
            force_old = True
        # Hear we create the dummy data row. The header stored in the index
        # will have the lead columns in it (eid and potentially the withdrawn
        # column) so we generate a dummy data row with those removed
        row = [''] * (len(idx_reader.header) - 1)
        extracted_eid = eid

    # sanity check the eid
    assert extracted_eid == eid, ("eid missmatch in extracted row:"
                                  " {0} != {1}".format(eid, extracted_eid))

    return row, extracted_eid, withdrawn, force_old


# # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# def _pad_withdrawn(lead_cols):
    # """
    # Ensure that all the extracted lead columns have a withdrawn column. If it
    # does not exist it will be created with a 0 (False) value

    # Parameters
    # ----------
    # lead_cols : list of int
    # The list will be max length 2 and if < 2 will be made to length 2

    # Returns
    # -------
    # lead_cols : list of int
    # The lead columns with a guarenteed withdrawn column in it
    # """
    # lead_cols += [0] * (2 - len(lead_cols))
    # return lead_cols


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == '__main__':
    main()
