"""
.. include:: ../docs/wide_file_index.md
"""
from ukbb_tools import __version__, __name__ as pkg_name, \
    wide_file_common as wfc
from Bio import bgzf
from simple_progress import progress
from contextlib import contextmanager
import builtins
import argparse
import sys
import os
import csv
import configparser
import gzip
import warnings


INDEX_SECTION = 'INDEX_SEEK'
METADATA_SECTION = 'METADATA'
MSG_PREFIX = '[info]'
MSG_OUT = sys.stderr


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class IndexReader(object):
    """
    A class for handling the read interaction with an indexed flat file
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @staticmethod
    def open_idx(index_file):
        """
        Open the index file in the config manager. This just provides a route
        to the configparser that contains the index data

        Parameters
        ----------
        index_file : str
            The path to the index file

        Returns
        -------
        index : configparser
            The configparser that contains all the seek positions
        """
        config = configparser.ConfigParser()
        config.read_file(gzip.open(index_file, 'rt'))
        return config

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @staticmethod
    def has_index_section(inobj):
        """
        Determine if the index file (or config) has the correct index_seek
        section

        Parameters
        ----------
        inobj : str or :obj:`configparser.ConfigParser`
            The path to the index file or a configparser object

        Returns
        -------
        has_index_section : bool
            True if the INDEX_SEEK section is found False otherwise
        """
        config_idx = IndexReader._get_config_obj(inobj)

        if INDEX_SECTION in config_idx.sections():
            return True
        return False

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @staticmethod
    def read_index(inobj):
        """
        Read the entire index seek positions into a dictionary with eids (int)
        keys and seek positions (int) values

        Parameters
        ----------
        inobj : str or :obj:`configparser.ConfigParser`
            The path to the index file or a configparser object

        Returns
        -------
        index_data : dict
            The index data
        """
        config_idx = IndexReader._get_config_obj(inobj)

        try:
            return dict(
                [(int(k), int(v))
                 for k, v in config_idx[INDEX_SECTION].items()]
            )
        except KeyError as e:
            raise KeyError(
                "can't find the index section '{0}', are you sure"
                " this is an index file?".format(INDEX_SECTION)
            ) from e

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @staticmethod
    def validate_index(inobj, wide_file=None):
        """
        Check as many attributes about the index as possible to make sure it
        is valid

        Parameters
        ----------
        inobj : str or :obj:`configparser.ConfigParser`
            The path to the index file or a configparser object
        wide_file : str, optional, default: NoneType
            If the wide file is available more validation steps can be
            performed

        Returns
        -------
        index : dict
            The index data with eid keys (int) and seek positions (int) values

        Raises
        ------
        IndexError
            If the index can't be validated
        """
        config_idx = IndexReader._get_config_obj(inobj)
        index = IndexReader.read_index(config_idx)

        if len(index) == 0:
            raise IndexError("no data in the index")

        # Get the index metadata
        metadata = IndexReader._get_section(config_idx, METADATA_SECTION)
        if int(metadata['index_len']) != len(index):
            raise IndexError("index is not the required length")

        if wide_file is not None:
            # Open it
            csv_kwargs = wfc.get_csv_kwargs(wide_file)
            with wfc.gzip_agnostic(wide_file) as infile:
                reader = csv.reader(infile, **csv_kwargs)
                header = wfc.check_header(reader)

            header_md5 = wfc.header_md5(header)
            if len(header) != int(metadata['header_len']):
                raise IndexError("header is not the required length")

            if header_md5 != metadata['header_md5']:
                raise IndexError("header is not the required structure")

        return index

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @staticmethod
    def _get_section(inobj, section):
        """

        """
        config_idx = IndexReader._get_config_obj(inobj)

        try:
            return config_idx[section]
        except KeyError as e:
            raise KeyError(
                "can't find the index section '{0}', are you sure"
                " this is an index file?".format(section)
            ) from e

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @staticmethod
    def _get_config_obj(inobj):
        """
        Return a configparser object from the input, where the input can be
        either a file name (str) or already a configparser object

        Parameters
        ----------
        inobj : str or :obj:`configparser.ConfigParser`
            Either a string (in which case it is openned into a configparser or
            an configparser object in which case it is returned

        Returns
        -------
        config : configparser
            The configparser
        """
        if isinstance(inobj, configparser.ConfigParser):
            return inobj
        elif isinstance(inobj, str):
            return IndexReader.open_idx(inobj)
        else:
            raise TypeError("unknown input object")

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, wide_file, index_file=None):
        """
        Parameters
        ----------
        wide_file : str
            The path to the wide file that has been indexed
        index_file : str, optional, default: NoneType
            An alternative index file location. If `NoneType` then the index
            file path is the same as the wide file path but with an `.idx`
            extension
        """
        index_file = wfc.get_index_path(wide_file, index_file=index_file)
        self._wide_file = os.path.expanduser(wide_file)
        self._index_file = os.path.expanduser(index_file)

        # As we have overriden open we have to use the builtin
        builtins.open(self._wide_file).close()
        builtins.open(self._index_file).close()

        self.header = None
        self._opened = False

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def open(self):
        """
        Open the wide file and the index
        """
        self._check_closed()

        # Attempt to open and validate the index. This will return the config
        # file and the index as a dictionary
        self._config = self.__class__.open_idx(self._index_file)
        self.idx = self.__class__.validate_index(
            self._config,
            wide_file=self._wide_file
        )

        # Get the dialect for the wide file
        self._csv_kwargs = wfc.get_csv_kwargs(
            self._wide_file,
            read_bytes=2000000
        )

        # The index is read in with a bgzip reader
        self._wide_fobj = bgzf.BgzfReader(self._wide_file)
        self._reader = csv.reader(self._wide_fobj, **self._csv_kwargs)
        self.header = wfc.check_header(self._reader)
        self._wide_fobj.seek(0)
        self._opened = True

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def close(self):
        """
        Close the config file and the wide file
        """
        self._wide_fobj.close()
        self._config = None
        self._idx = None
        self._opened = False

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def seek(self, to):
        """
        Expose the underlaying file objects seek method
        """
        self._wide_fobj.seek(to)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def samples(self):
        """
        Return the number of samples in the index
        """
        return sorted(list(self.idx.keys()))

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def n_samples(self):
        """
        Return the number of samples in the index
        """
        return len(self.idx)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _check_open(self):
        """
        Error out if the files are not open
        """
        if self._opened is False:
            raise IOError("index not open")

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _check_closed(self):
        """
        Error out if the files are open
        """
        if self._opened is True:
            raise IOError("index is open")

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def fetch_row(self, eid):
        """
        Fetch an unformatted row for a single sample (eid)

        Parameters
        ----------
        eid : int
            A single UKBB sample ID

        Returns
        -------
        row : list of str
            A single unformatted row

        Raises
        ------
        KeyError
            If the sample EID is not in the index
        RuntimeError
            If the first column of the extracted row is not the same as the
            eid that has been requested
        """
        try:
            eid = int(eid)
            seek_pos = self.idx[eid]
            self._wide_fobj.seek(seek_pos)
            row = next(self._reader)
            if row[0] != str(eid):
                raise RuntimeError(
                    "bad index: expected {0}, found '{1}'".format(eid, row[0])
                )
            if len(row) != len(self.header):
                raise IndexError("{0}: row ('{1}') is not the same length as"
                                 " header ('{2}') ".format(
                                     eid, len(row), len(self.header)))
            return row
        except (TypeError, KeyError) as e:
            raise KeyError("eid '{0}' not found".format(eid)) from e

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def fetch(self, eids):
        """
        Yield multiple eids, unlike fetch row, this will not error out if the
        eids are not present

        Parameters
        ----------
        eids : list of int
            The eids to search for

        Yields
        ------
        row : list of str
            A single unformatted row
        """
        not_found = []
        for i in eids:
            try:
                yield self.fetch_row(i)
            except KeyError:
                not_found.append(str(i))

        if len(not_found) > 0:
            warnings.warn(
                "The following eids were not found: '{0}'".format(
                    ",".join(not_found))
            )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main():
    """
    The main entry point to build an indexed wide file
    """
    # Initialise and parse the command line arguments
    parser = init_cmd_args()
    args = parse_cmd_args(parser)

    # Start a msg outputter, this will respect the verbosity and will output to
    # STDERR
    m = progress.Msg(prefix='', verbose=args.verbose, file=MSG_OUT)
    m.msg("= UKBB wide file indexer ({0} v{1}) =".format(
        pkg_name, __version__))
    m.prefix = MSG_PREFIX
    m.msg_atts(args)

    # Make sure the wide file can be opened
    builtins.open(args.wide_file_path).close()

    # Setup the paths to write to the output file and the index_file
    # if the user has provided them, then the only thing we do is to
    # check that the user defined output file != input file as this is likely
    # an error as the default action when the user gives no input is to replace
    # the input file anyway, so it is assumed that if the user is giving an
    # output file then they will be trying to avoid that
    outfile, index_file = _get_outfile_paths(args)

    # Initialise the building of the index file. When used on the command line
    # the default is to sniff the csv dialect
    build_index_file(
        args.wide_file_path,
        outfile,
        index_file,
        encoding=args.encoding,
        outfile_delimiter=args.out_delimiter,
        verbose=args.verbose
    )

    m.msg("*** END ***")


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def init_cmd_args():
    """
    Initialise the command line arguments and return the parsed arguments

    Returns
    -------
    parser : :obj:`argparse.ArgumentParser`
        The argument parser with the arguments set up
    """
    parser = argparse.ArgumentParser(
        description="build an indexed UKBB wide file for random sample access")

    # The wide format UKBB data fields filex
    parser.add_argument('wide_file_path', type=str,
                        help="The path to UKBB wide file")
    parser.add_argument('--encoding', type=str, default='utf-8',
                        help="The encoding to use when reading the input file"
                        ", for some reason many UK BioBank files have latin1 "
                        "encoding and some of the characters are not "
                        "compatible with UTF8")
    parser.add_argument('--index-file', type=str,
                        help="An alternative path to the index file name. By "
                        "default the index file will be the same name as the "
                        "wide file but with an additional .idx extension")
    parser.add_argument('--outfile', type=str,
                        help="An alternative path to the output indexed file."
                        " By default, the indexed file will be the same as the"
                        " input file name with a .gz extension (it will be "
                        "bgzipped) and the input file would be removed. "
                        "However, if this is provided then the input file will"
                        " be left in place")
    parser.add_argument('--out-delimiter', type=str, default=None,
                        help="The delimiter of the output file. If not "
                        "specified then it will use the same delimiter as "
                        "the input file has.")
    parser.add_argument('-v', '--verbose',  action="store_true",
                        help="Give more output")

    return parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_cmd_args(parser):
    """
    Parse the command line arguments

    Parameters
    ----------
    parser : :obj:`argparse.ArgumentParser`
        The argument parser with the arguments set up

    Returns
    -------
    args : :obj:`argparse.Namespace`
        The argparse namespace object
    """
    args = parser.parse_args()

    # Make sure ~/ etc... is expanded
    for i in ['wide_file_path', 'index_file', 'outfile']:
        try:
            setattr(args, i, os.path.expanduser(getattr(args, i)))
        except TypeError:
            # Type errors are expected with index_file and out_file as they are
            # optional arguments and may be NoneType
            if i == 'wide_file_path':
                raise

    return args


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _get_outfile_paths(args):
    """
    Return the outfile path and the index file path with respect to the
    arguments that have been given. If the args.out_file  and args.index_file
    is set then these are used unless args.out_file == args.wide_file_path as
    it is assumes that if the user wanted to set them then they would want to
    set to something different and not just repeat the default operation of the
    program. If outfile is not set then it defulat to the input file name but a
    `.gz` extension is added (if not already present). If the index file is not
    set then it defaults to the outfile name but with a further `.idx`
    extension added.

    Parameters
    ----------
    args : :obj:`argparse.Namespace`
        The argparse namespace object

    Returns
    -------
    outfile : str
        The place to write the compressed, indexed output file
    index_file : str
        The location to write the index file
    """
    outfile = None
    index_file = None

    # The user has not defined an output file name
    if args.outfile is None:
        # So we make the output file name == the wide_file name
        # (i.e. overwrite it) but we add a .gz extension on it if it does
        # not already exist
        outfile = args.wide_file_path
        if not outfile.endswith('.gz'):
            outfile = '{0}.gz'.format(outfile)
    else:
        # The user has supplied an input file name. So we check that it
        # does not mimick the default behaviour of the program
        if args.outfile == args.wide_file_path:
            raise ValueError(
                "you have specified an output file that is the same name as "
                "the input file, did you intend this? If you want to overwrite"
                "the input file with an indexed file then do not provide any"
                " --outfile"
            )
        outfile = args.outfile

    index_file = args.index_file
    if args.index_file is None:
        index_file = '{0}.idx'.format(outfile)

    return outfile, index_file


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def build_index_file(wide_file_path, outfile, index_file, read_bytes=2000000,
                     verbose=False, encoding='utf-8', outfile_delimiter=None,
                     **csv_kwargs):
    """
    Build a compressed indexed wide file and associated index file

    Parameters
    ----------
    wide_file_path : str
        The existing location of the wide file, this may or maynot be
        compressed
    outfile : str
        The location of the BGZIP compressed output file that will have been
        indexed. The outfile is allowed to be the same as the input wide file
    index_file : str
        The location of the index file. This should not be the same as any of
        the output or wide file paths
    read_bytes : int, optional, default: 2000000
        If no csv_kwargs have been supplied then we attempt to sniff out the
        delimiter of the file. This is the number of bytes that is used to
        determine the dialect. Given the file is wide, a value of 2MB has been
        choosen but can be increated if not sufficient
    verbose : bool, optional, default: `False`
        Show progress.
    encoding : `str`, optional, default: `utf-8`
        The encoding to use when reading in the input file. Some UKBioBank
        files are latin1 encoded.
    outfile_delimiter : `NoneType` or `str`, optional, default: `NoneType`
        The output file delimiter. If not specified then the default is to
        use the input file delimiter.
    **csv_kwargs
        Any arguments that should be used by csv to read the wide file being
        indexed. These are also recycled into the arguments to write the
        indexed file, although the outfile delimiter can be explicitly given
        in ``outfile_delimiter``. If not supplied then the dialect is sniffed
        out.
    """
    # Standardise the input/output/index file paths
    wide_file_path = os.path.realpath(os.path.expanduser(wide_file_path))
    outfile = os.path.realpath(os.path.expanduser(outfile))
    index_file = os.path.realpath(os.path.expanduser(index_file))

    # Make sure we are not going to overwrite anything
    if index_file == outfile or index_file == wide_file_path:
        raise ValueError(
            "index file must not be the same name as input/output files"
        )

    # Make sure that we sniff out the csv args if they have not been supplied
    csv_kwargs = wfc.get_csv_kwargs(
        wide_file_path,
        read_bytes=read_bytes,
        **csv_kwargs
    )

    # Either use what is specified or what the input file has
    outfile_delimiter = outfile_delimiter or csv_kwargs['delimiter']
    out_csv_args = csv_kwargs.copy()
    out_csv_args['delimiter'] = outfile_delimiter

    # Will hold the index seek positions that will be written to an ini file
    # that will serve as an index
    idx = {}
    with wfc.write_via_temp(outfile,
                            write_method=bgzf.BgzfWriter) as outbgz:
        with wfc.gzip_agnostic(wide_file_path, encoding=encoding) as infile:
            reader = csv.reader(infile, **csv_kwargs)
            prog = progress.RateProgress(verbose=verbose, file=MSG_OUT)
            header = wfc.check_header(reader)
            writer = csv.writer(outbgz, **out_csv_args)
            writer.writerow(header)
            for i in prog.progress(reader):
                eid = int(i[0])
                idx[eid] = outbgz.tell()
                writer.writerow(i)
        write_index(index_file, header, idx)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def write_index(filename, header, index):
    """
    Write an index file to disk using the configparser.ConfigParser

    Parameters
    ----------
    filename : str
        The name of the file that we want to write to
    index : dict
        The index values, these are eids (int) keys and seek positions
        (int - virtual offsets) as values
    """
    metadata = {}
    metadata['header_md5'] = wfc.header_md5(header)
    metadata['header_len'] = len(header)
    metadata['index_len'] = len(index)

    # TODO: Could also take the MD5 of the entries?
    config = configparser.ConfigParser()
    config[METADATA_SECTION] = metadata
    config[INDEX_SECTION] = index
    with gzip.open(filename, 'wt') as configfile:
        configfile.write("# Wide file index: DO NOT EDIT THIS FILE!!!!\n")
        config.write(configfile)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@contextmanager
def open(*args, **kwargs):
    """
    A context wrapper for the index reading

    Parameters
    ----------
    *args
        Arguments supplied to `wide_file_index.IndexReader`
    **kwargs
        Keyword arguments supplied to `wide_file_index.IndexReader`
    """
    # Code to acquire resource, e.g.:
    idx = IndexReader(*args, **kwargs)
    try:
        # Open the index and supply it
        idx.open()
        yield idx
    finally:
        # close the index
        idx.close()


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == '__main__':
    main()
