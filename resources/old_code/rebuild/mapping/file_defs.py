"""
.. include:: ../docs/file_defs.md
"""
from ukbb_tools import constants as con, type_casts as tc
from ukbb_tools.mapper import column_defs as col
from simple_progress import progress
import sys
import os
import re
import hashlib
import pkg_resources
import csv
import pprint as pp


# The prefix for verbose messages and the location to write the messages
MSG_PREFIX = '[info]'
MSG_OUT = sys.stderr


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class ReadCuiFix(object):
    """
    A class for interacting with read version2 and CTV3 fixes for UKBB
    mappings
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self):
        self._fix = {}
        fix_file = pkg_resources.resource_filename(
            __name__, 'data/ukbb_mapping_fix.txt'
        )

        with open(fix_file, 'rt') as infile:
            reader = csv.reader(infile, delimiter="\t")
            header = next(reader)
            for row in reader:
                row = dict([i for i in zip(header, row)])
                try:
                    self._fix[row['coding_system']]
                except KeyError:
                    self._fix[row['coding_system']] = {}

                key = (row['ukbb_cui'], row['ukbb_tui'])
                self._fix[row['coding_system']][key] = row

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get(self, cui, tui, coding_system):
        """
        """
        key = ()
        if coding_system == con.READ2_CODE_TYPE_NAME:
            key = (cui, tui)
        elif coding_system == con.CTV3_CODE_TYPE_NAME:
            key = (cui, '')
        else:
            raise ValueError("unknown coding system")

        row = self._fix[coding_system][key]
        return row['fix_cui'], row['fix_tui'], row['fix_type']


# Make this a module level variable
CUI_FIX = ReadCuiFix()


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class CuiCode(object):
    def __init__(self, code, tui, term, coding_system, code_type=None,
                 code_status=None):
        self.code = code
        self.tui = tui
        self.term = term
        self.coding_system = coding_system
        self.code_type = code_type
        self.code_status = code_status

    def __eq__(self, other):
        """
        """
        if self.__hash__() == other.__hash__():
            return True
        return False

    def __hash__(self):
        """
        """
        return hash((self.code, self.tui, self.term, self.coding_system))

    def __repr__(self):
        return "<{0}({1}, {2}, {3}, {4}, {5}, {6})>".format(
            self.__class__.__name__,
            self.code,
            self.tui,
            self.term,
            self.coding_system,
            self.code_type,
            self.code_status
        )
    @property
    def key(self):
        return self.coding_system, self.code


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class ClinCodeExtractDesc(object):
    """
    """
    CLINICAL_CODES = []

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @classmethod
    def get_clinical_code(cls, row):
        """
        Extract the clinical code

        Parameters
        ----------
        row : list or dict
            A single data row to extract all the clinical codes from
        """
        codes = set()
        for cui_col, tui_col, term_col, code_type_col, code_status_col, \
            coding_system in cls.CLINICAL_CODES:
            try:
                tui = row[tui_col]
            except KeyError:
                try:
                    # TUI column might be NoneType
                    tui = hashlib.md5(row[term_col].encode()).hexdigest()
                except AttributeError:
                    # row[term_col] might be NoneType
                    tui = ''

            try:
                code_type = row[code_type_col]
            except KeyError:
                # TUI column might be NoneType
                code_type = 'U'

            try:
                code_status = row[code_status_col]
            except KeyError:
                # TUI column might be NoneType
                code_status = 'U'

            row[term_col] = row[term_col] or ''
            # TODO: Need to exclude NoneTypes from Excel shreadsheet parse
            if row[cui_col] is not None and row[cui_col] != 'None':
                codes.add(
                    CuiCode(
                        row[cui_col],
                        tui,
                        row[term_col],
                        coding_system,
                        code_type=code_type,
                        code_status=code_status
                    )
                )

        return codes


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class ReadCodeExtract(ClinCodeExtractDesc):
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @classmethod
    def get_clinical_code(cls, row):
        """
        Extract the clinical code

        Parameters
        ----------
        row : list or dict
            A single data row to extract all the clinical codes from
        """
        codes = super().get_clinical_code(row)

        # Relies on the longest term being defined first
        for i in codes:
            if i.term is not None:
                return set([i])

        # If we get here then they are all None
        return set([i])


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class MappingExtract(object):
    BIDIRECTIONAL = False
    SOURCE_CUI = None
    SOURCE_CODING_SYSTEM = None
    TARGET_CUI = None
    TARGET_CODING_SYSTEM = None
    MAP_TYPE = None

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @classmethod
    def get_mappings(cls, row):
        """
        Extract a mapping from the row

        Parameters
        ----------
        row : dict
            A single data row to extract the mapping data from
        """
        try:
            map_type = row[cls.MAP_TYPE]

            # Make sure it is defined
            map_type = map_type or 'X'
        except KeyError:
            # Some files might not have a map type column
            map_type = 'X'

        mapping = [
            (
                (cls.SOURCE_CODING_SYSTEM, row[cls.SOURCE_CUI]),
                (cls.TARGET_CODING_SYSTEM, row[cls.TARGET_CUI]),
                map_type
            )
        ]
        if cls.BIDIRECTIONAL is True:
            mapping.append(
                (
                    (cls.TARGET_CODING_SYSTEM, row[cls.TARGET_CUI]),
                    (cls.SOURCE_CODING_SYSTEM, row[cls.SOURCE_CUI]),
                    map_type
                )
            )
        return mapping


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class ManualMappingExtract(object):
    SOURCE_CUI = ''
    SOURCE_CODING_SYSTEM = ''
    TARGET_CUI = ''
    TARGET_CODING_SYSTEM = ''

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @classmethod
    def get_mapping(cls, row):
        """
        Extract a mapping from the row of a manual includes/excludes file

        Parameters
        ----------
        row : dict
            A single data row to extract the mapping data from
        """
        mapping = [
            (
                (cls.SOURCE_CODING_SYSTEM, row[cls.SOURCE_CUI]),
                (cls.TARGET_CODING_SYSTEM, row[cls.TARGET_CUI])
            )
        ]
        if row[col.MANUAL_BIDIR] is True:
            mapping.append(
                (
                    (cls.TARGET_CODING_SYSTEM, row[cls.TARGET_CUI]),
                    (cls.SOURCE_CODING_SYSTEM, row[cls.SOURCE_CUI])
                )
            )
        return mapping


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class ManualExcludesDef(ManualMappingExtract):
    """
    The definition a manual excludes file
    """
    TABLE = col.MANUAL_EXCLUDES
    NAME = TABLE.name

    COLUMNS = [
        col.MANUAL_SOURCE_CUI,
        col.MANUAL_SOURCE_CODING_SYSTEM,
        col.MANUAL_TARGET_CUI,
        col.MANUAL_TARGET_CODING_SYSTEM,
        col.MANUAL_BIDIR
    ]
    HEADER = [i.name for i in COLUMNS]

    SOURCE_CUI = col.MANUAL_SOURCE_CUI
    SOURCE_CODING_SYSTEM = col.MANUAL_SOURCE_CODING_SYSTEM
    TARGET_CUI = col.MANUAL_TARGET_CUI
    TARGET_CODING_SYSTEM = col.MANUAL_TARGET_CODING_SYSTEM


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class ManualIncludesDef(ManualMappingExtract):
    """
    The definition a manual includes file
    """
    TABLE = col.MANUAL_INCLUDES
    NAME = TABLE.name

    COLUMNS = [
        col.MANUAL_SOURCE_CUI,
        col.MANUAL_SOURCE_CODING_SYSTEM,
        col.MANUAL_TARGET_CUI,
        col.MANUAL_TARGET_CODING_SYSTEM,
        col.MANUAL_BIDIR,
        col.MANUAL_OVERWRITE
    ]
    HEADER = [i.name for i in COLUMNS]

    SOURCE_CUI = col.MANUAL_SOURCE_CUI.name
    SOURCE_CODING_SYSTEM = col.MANUAL_SOURCE_CODING_SYSTEM.name
    TARGET_CUI = col.MANUAL_TARGET_CUI.name
    TARGET_CODING_SYSTEM = col.MANUAL_TARGET_CODING_SYSTEM.name
    MAP_TYPE = 'M'

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @classmethod
    def get_mapping(cls, row):
        """
        Extract a mapping from the row of a manual includes/excludes file

        Parameters
        ----------
        row : dict
            A single data row to extract the mapping data from
        """
        mappings = super().get_mapping(row)

        # Add the map type and overwrite variable to the mappings
        for idx in range(len(mappings)):
            mappings[idx] = (
                mappings[idx][:],
                (cls.MAP_TYPE, row[col.MANUAL_OVERWRITE.name])
            )
        return mappings


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class UkbbCodePoolDef(object):
    """
    The definition a manual includes file
    """
    TABLE = col.UKBB_CODE_POOL
    NAME = TABLE.name
    COLUMNS = [
        col.UKBB_CODE_POOL_CUI,
        col.UKBB_CODE_POOL_CODING_SYSTEM
    ]
    HEADER = [i.name for i in COLUMNS]
    CASTS = {
        col.UKBB_CODE_POOL_CODING_SYSTEM.name:
        col.UKBB_CODE_POOL_CODING_SYSTEM.type
    }


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Ctv3Equ(ClinCodeExtractDesc):
    """
    The definition for the `ctv3_equivalent` table
    """
    TABLE = col.CTV3_EQV
    NAME = TABLE.name

    COLUMNS = [
        col.CTV3_EQV_CUI1,
        col.CTV3_EQV_CUI2,
        col.CTV3_EQV_PROVENANCE,
        col.CTV3_EQV_IDATE
    ]

    HEADER = [i.name for i in COLUMNS]
    CASTS = {
        col.CTV3_EQV_PROVENANCE.name: col.CTV3_EQV_PROVENANCE.type
    }


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Ctv3Hier(ClinCodeExtractDesc):
    """
    The definition for the `ctv3_hier` table
    """
    TABLE = col.CTV3_HIER
    NAME = TABLE.name
    COLUMNS = [
        col.CTV3_HIER_CHILD,
        col.CTV3_HIER_PARENT,
        col.CTV3_HIER_ORD,
        col.CTV3_HIER_PTERM,
        col.CTV3_HIER_CTERM,
        col.CTV3_HIER_DNUM
    ]
    HEADER = [i.name for i in COLUMNS]
    CASTS = {
        col.CTV3_HIER_ORD.name: col.CTV3_HIER_ORD.type,
        col.CTV3_HIER_DNUM.name: col.CTV3_HIER_DNUM.type
    }


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Ctv3Def(ReadCodeExtract):
    """
    The definition for the `ctv3` table
    """
    BASENAME = 'CTV3.read'
    TABLE = col.CTV3
    NAME = TABLE.name
    COLUMNS = [
        col.CTV3_CUI,
        col.CTV3_STAT,
        col.CTV3_TUI,
        col.CTV3_TYP,
        col.CTV3_T30,
        col.CTV3_T60,
        col.CTV3_T198,
        col.CTV3_FREQ,
        col.CTV3_RDATE
    ]
    HEADER = [i.name for i in COLUMNS]
    CASTS = {
        col.CTV3_FREQ.name: col.CTV3_FREQ.type
    }
    CLINICAL_CODES = [
        # CUI column name, TUI column name, TERM column name,
        # code_type column, code_status column, Coding System
        (
            col.CTV3_CUI.name,
            col.CTV3_TUI.name,
            col.CTV3_T198.name,
            col.CTV3_TYP.name,
            col.CTV3_STAT.name,
            con.CTV3_CODE_TYPE_NAME
        ),
        (
            col.CTV3_CUI.name,
            col.CTV3_TUI.name,
            col.CTV3_T60.name,
            col.CTV3_TYP.name,
            col.CTV3_STAT.name,
            con.CTV3_CODE_TYPE_NAME
        ),
        (
            col.CTV3_CUI.name,
            col.CTV3_TUI.name,
            col.CTV3_T30.name,
            col.CTV3_TYP.name,
            col.CTV3_STAT.name,
            con.CTV3_CODE_TYPE_NAME
        )
    ]


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Ctv3Rmf(ClinCodeExtractDesc):
    """
    The definition for the `ctv3_rmf` table
    """
    TABLE = col.CTV3_RMF
    NAME = TABLE.name
    COLUMNS = [
        col.CTV3_RMF_NEW_CUI,
        col.CTV3_RMF_OLD_CUI
    ]
    HEADER = [i.name for i in COLUMNS]
    CASTS = {
    }


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Ctv3Tc(ClinCodeExtractDesc):
    """
    The definition for the `ctv3_tc` table
    """
    TABLE = col.CTV3_TC
    NAME = TABLE.name
    COLUMNS = [
        col.CTV3_TC_SUPERTYPE_ID,
        col.CTV3_TC_SUBTYPE_ID
    ]
    HEADER = [i.name for i in COLUMNS]
    CASTS = {
    }


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Ctv3Term(ClinCodeExtractDesc):
    """
    The definition for the `ctv3_term` table
    """
    TABLE = col.CTV3_TERM
    NAME = TABLE.name
    COLUMNS = [
        col.CTV3_TERM_TUI,
        col.CTV3_TERM_STAT,
        col.CTV3_TERM_T30,
        col.CTV3_TERM_T60,
        col.CTV3_TERM_T198
    ]
    HEADER = [i.name for i in COLUMNS]
    CASTS = {
    }


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class RctEqv(ClinCodeExtractDesc):
    """
    The definition for the `rct_equivalent` table
    """
    TABLE = col.RCT_EQV
    NAME = TABLE.name
    COLUMNS = [
        col.RCT_EQV_CUI1,
        col.RCT_EQV_CUI2,
        col.RCT_EQV_PROVENANCE,
        col.RCT_EQV_IDATE
    ]
    HEADER = [i.name for i in COLUMNS]
    CASTS = {
        col.RCT_EQV_PROVENANCE.name: col.RCT_EQV_PROVENANCE.type
    }


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class RctHier(ClinCodeExtractDesc):
    """
    The definition for the `rct_hier` table
    """
    TABLE = col.RCT_HIER
    NAME = TABLE.name
    COLUMNS = [
        col.RCT_HIER_PARENT,
        col.RCT_HIER_CHILD,
        col.RCT_HIER_PTERM,
        col.RCT_HIER_CTERM,
        col.RCT_HIER_DNUM
    ]
    HEADER = [i.name for i in COLUMNS]
    CASTS = {
        col.RCT_HIER_DNUM.name: col.RCT_HIER_DNUM.type
    }


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class RctDef(ReadCodeExtract):
    """
    The definition for the `rct` table
    """
    BASENAME = 'RCT.read'
    TABLE = col.RCT
    NAME = TABLE.name
    COLUMNS = [
        col.RCT_CUI,
        col.RCT_TUI,
        col.RCT_T30,
        col.RCT_T60,
        col.RCT_T198,
        col.RCT_FREQ,
        col.RCT_RDATE,
    ]
    HEADER = [i.name for i in COLUMNS]
    CASTS = {
        col.RCT_FREQ.name: col.RCT_FREQ.type
    }
    CLINICAL_CODES = [
        # CUI column name, TUI column name, TERM column name,
        # code_type column, code_status column, Coding System
        (
            col.RCT_CUI.name,
            col.RCT_TUI.name,
            col.RCT_T198.name,
            None,
            None,
            con.READ2_CODE_TYPE_NAME
        ),
        (
            col.RCT_CUI.name,
            col.RCT_TUI.name,
            col.RCT_T60.name,
            None,
            None,
            con.READ2_CODE_TYPE_NAME
        ),
        (
            col.RCT_CUI.name,
            col.RCT_TUI.name,
            col.RCT_T30.name,
            None,
            None,
            con.READ2_CODE_TYPE_NAME
        )
    ]


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class RctTc(ClinCodeExtractDesc):
    """
    The definition for the `rct_tc` table
    """
    TABLE = col.RCT_TC
    NAME = TABLE.name
    COLUMNS = [
        col.RCT_TC_SUBTYPE_ID,
        col.RCT_TC_SUPERTYPE_ID
    ]
    HEADER = [i.name for i in COLUMNS]
    CASTS = {
    }


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Ctv3RctMap(ClinCodeExtractDesc):
    """
    The definition for the `ctv3_rct_map` table
    """
    TABLE = col.CTV3_RCT_MAP
    NAME = TABLE.name
    COLUMNS = [
        col.CTV3_RCT_MAP_SCUI,
        col.CTV3_RCT_MAP_STUI,
        col.CTV3_RCT_MAP_TCUI,
        col.CTV3_RCT_MAP_TTUI,
        col.CTV3_RCT_MAP_MAPTYP,
        col.CTV3_RCT_MAP_ASSURED
    ]
    HEADER = [i.name for i in COLUMNS]
    CASTS = {
        col.CTV3_RCT_MAP_ASSURED.name: col.CTV3_RCT_MAP_ASSURED.type
    }


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Ctv3SctMap(ClinCodeExtractDesc):
    """
    The definition for the `ctv3_sct_map` table
    """
    TABLE = col.CTV3_SCT_MAP
    NAME = TABLE.name
    COLUMNS = [
        col.CTV3_SCT_MAP_SCUI,
        col.CTV3_SCT_MAP_STUI,
        col.CTV3_SCT_MAP_TCUI,
        col.CTV3_SCT_MAP_TTUI,
        col.CTV3_SCT_MAP_MAPTYP,
        col.CTV3_SCT_MAP_ASSURED
    ]
    HEADER = [i.name for i in COLUMNS]
    CASTS = {
        col.CTV3_SCT_MAP_ASSURED.name: col.CTV3_SCT_MAP_ASSURED.type
    }


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Icd10Hier(ClinCodeExtractDesc):
    """
    The definition for the `icd10_hier` table
    """
    TABLE = col.ICD10_HIER
    NAME = TABLE.name
    COLUMNS = [
        col.ICD10_HIER_PARENT,
        col.ICD10_HIER_CHILD,
        col.ICD10_HIER_PTERM,
        col.ICD10_HIER_CTERM,
        col.ICD10_HIER_DNUM,
    ]
    HEADER = [i.name for i in COLUMNS]
    CASTS = {
        col.ICD10_HIER_DNUM.name: col.ICD10_HIER_DNUM.type
    }


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Icd10SctMap(ClinCodeExtractDesc):
    """
    The definition for the `icd10_sct_map` table
    """
    TABLE = col.ICD10_SCT_MAP
    NAME = TABLE.name
    COLUMNS = [
        col.ICD10_SCT_MAP_SCUI,
        col.ICD10_SCT_MAP_STUI,
        col.ICD10_SCT_MAP_TCUI,
        col.ICD10_SCT_MAP_TTUI,
        col.ICD10_SCT_MAP_MAPTYP,
        col.ICD10_SCT_MAP_ASSURED
    ]
    HEADER = [i.name for i in COLUMNS]
    CASTS = {
        col.ICD10_SCT_MAP_ASSURED.name: col.ICD10_SCT_MAP_ASSURED.type
    }


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Icd10Tc(ClinCodeExtractDesc):
    """
    The definition for the `icd10_tc` table
    """
    TABLE = col.ICD10_TC
    NAME = TABLE.name
    COLUMNS = [
        col.ICD10_TC_SUPERTYPE_ID,
        col.ICD10_TC_SUBTYPE_ID
    ]
    HEADER = [i.name for i in COLUMNS]
    CASTS = {
    }


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Icd10Def(ClinCodeExtractDesc):
    """
    The definition for the `icd10` table
    """
    BASENAME = 'ICD.xmap'
    TABLE = col.ICD10
    NAME = TABLE.name
    COLUMNS = [
        col.ICD10_CUI,
        col.ICD10_TERM
    ]
    HEADER = [i.name for i in COLUMNS]
    CLINICAL_CODES = [
        # CUI column name, TUI column name, TERM column name,
        # code_type column, code_status column, Coding System
        (
            col.ICD10_CUI.name,
            None,
            col.ICD10_TERM.name,
            None,
            None,
            con.ICD10_CODE_TYPE_NAME
        )
    ]


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class MapVersions(ClinCodeExtractDesc):
    """
    The definition for the `map_versions` table
    """
    TABLE = col.MAP_VERSIONS
    NAME = TABLE.name
    COLUMNS = [
        col.MAP_VERSIONS_MAP,
        col.MAP_VERSIONS_RELEASE_DATE,
        col.MAP_VERSIONS_BUILD_DATE,
        col.MAP_VERSIONS_SNAPSHOT_DATE
    ]
    HEADER = [i.name for i in COLUMNS]
    CASTS = {
        col.MAP_VERSIONS_RELEASE_DATE.name: col.MAP_VERSIONS_RELEASE_DATE.type,
        col.MAP_VERSIONS_BUILD_DATE.name: col.MAP_VERSIONS_BUILD_DATE.type,
        col.MAP_VERSIONS_SNAPSHOT_DATE.name: col.MAP_VERSIONS_SNAPSHOT_DATE.type
    }


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Opcs4Hier(ClinCodeExtractDesc):
    """
    The definition for the `opcs4_hier` table
    """
    TABLE = col.OPCS4_HIER
    NAME = TABLE.name
    COLUMNS = [
        col.OPCS4_HIER_PARENT,
        col.OPCS4_HIER_CHILD,
        col.OPCS4_HIER_PTERM,
        col.OPCS4_HIER_CTERM,
        col.OPCS4_HIER_DNUM
    ]
    HEADER = [i.name for i in COLUMNS]
    CASTS = {
        col.OPCS4_HIER_DNUM.name: col.OPCS4_HIER_DNUM.type
    }


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Opcs4SctMap(ClinCodeExtractDesc):
    """
    The definition for the `opcs4_sct_map` table
    """
    TABLE = col.OPCS4_SCT_MAP
    NAME = TABLE.name
    COLUMNS = [
        col.OPCS4_SCT_MAP_SCUI,
        col.OPCS4_SCT_MAP_STUI,
        col.OPCS4_SCT_MAP_TCUI,
        col.OPCS4_SCT_MAP_TTUI,
        col.OPCS4_SCT_MAP_MAPTYP,
        col.OPCS4_SCT_MAP_ASSURED
    ]
    HEADER = [i.name for i in COLUMNS]
    CASTS = {
        col.OPCS4_SCT_MAP_ASSURED.name: col.OPCS4_SCT_MAP_ASSURED.type
    }


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Opcs4Tc(ClinCodeExtractDesc):
    """
    The definition for the `opcs4_tc` table
    """
    TABLE = col.OPCS4_TC
    NAME = TABLE.name
    COLUMNS = [
        col.OPCS4_TC_SUPERTYPE_ID,
        col.OPCS4_TC_SUBTYPE_ID
    ]
    HEADER = [i.name for i in COLUMNS]
    CASTS = {
    }


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Opcs4Def(ClinCodeExtractDesc):
    """
    The definition for the `opcs4` table
    """
    BASENAME = 'OPCS.xmap'
    TABLE = col.OPCS4
    NAME = TABLE.name
    COLUMNS = [
        col.OPCS4_CUI,
        col.OPCS4_TERM
    ]
    HEADER = [i.name for i in COLUMNS]
    CLINICAL_CODES = [
        # CUI column name, TUI column name, TERM column name,
        # code_type column, code_status column, Coding System
        (
            col.OPCS4_CUI.name,
            None,
            col.OPCS4_TERM.name,
            None,
            None,
            con.OPCS4_CODE_TYPE_NAME
        )
    ]


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class RctCtv3Map(ClinCodeExtractDesc):
    """
    The definition for the `rct_ctv3_map` table
    """
    TABLE = col.RCT_CTV3_MAP
    NAME = TABLE.name
    COLUMNS = [
        col.RCT_CTV3_MAP_SCUI,
        col.RCT_CTV3_MAP_STUI,
        col.RCT_CTV3_MAP_TCUI,
        col.RCT_CTV3_MAP_TTUI,
        col.RCT_CTV3_MAP_MAPTYP,
        col.RCT_CTV3_MAP_ASSURED
    ]
    HEADER = [i.name for i in COLUMNS]
    CASTS = {
        col.RCT_CTV3_MAP_ASSURED.name: col.RCT_CTV3_MAP_ASSURED.type
    }


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class RctSctMap(ClinCodeExtractDesc):
    """
    The definition for the `rct_sct_map` table
    """
    TABLE = col.RCT_SCT_MAP
    NAME = TABLE.name
    COLUMNS = [
        col.RCT_SCT_MAP_SCUI,
        col.RCT_SCT_MAP_STUI,
        col.RCT_SCT_MAP_TCUI,
        col.RCT_SCT_MAP_TTUI,
        col.RCT_SCT_MAP_MAPTYP,
        col.RCT_SCT_MAP_ASSURED
    ]
    HEADER = [i.name for i in COLUMNS]
    CASTS = {
        col.RCT_SCT_MAP_ASSURED.name: col.RCT_SCT_MAP_ASSURED.type
    }


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class SctIcd10Map(ClinCodeExtractDesc):
    """
    The definition for the `sct_icd10_map` table
    """
    TABLE = col.SCT_ICD10_MAP
    NAME = TABLE.name
    COLUMNS = [
        col.SCT_ICD10_MAP_SCUI,
        col.SCT_ICD10_MAP_STUI,
        col.SCT_ICD10_MAP_TCUI,
        col.SCT_ICD10_MAP_TTUI,
        col.SCT_ICD10_MAP_MAPTYP,
        col.SCT_ICD10_MAP_ASSURED
    ]
    HEADER = [i.name for i in COLUMNS]
    CASTS = {
        col.SCT_ICD10_MAP_ASSURED.name: col.SCT_ICD10_MAP_ASSURED.type
    }


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class SctOpcs4Map(ClinCodeExtractDesc):
    """
    The definition for the `sct_opcs4_map` table
    """
    TABLE = col.SCT_OPCS4_MAP
    NAME = TABLE.name
    COLUMNS = [
        col.SCT_OPCS4_MAP_SCUI,
        col.SCT_OPCS4_MAP_STUI,
        col.SCT_OPCS4_MAP_TCUI,
        col.SCT_OPCS4_MAP_TTUI,
        col.SCT_OPCS4_MAP_MAPTYP,
        col.SCT_OPCS4_MAP_ASSURED
    ]
    HEADER = [i.name for i in COLUMNS]
    CASTS = {
        col.SCT_OPCS4_MAP_ASSURED.name: col.SCT_OPCS4_MAP_ASSURED.type
    }


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class SctHier(ClinCodeExtractDesc):
    """
    The definition for the `sct_hier` table
    """
    TABLE = col.SCT_HIER_CHILD
    NAME = TABLE.name
    COLUMNS = [
        col.SCT_HIER_CHILD,
        col.SCT_HIER_PARENT,
        col.SCT_HIER_CTERM,
        col.SCT_HIER_PTERM,
        col.SCT_HIER_DNUM
    ]
    HEADER = [i.name for i in COLUMNS]
    CASTS = {
        col.SCT_HIER_DNUM.name: col.SCT_HIER_DNUM.type
    }


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class SctModRel(ClinCodeExtractDesc):
    """
    The definition for the `sct_mod_rel` table
    """
    TABLE = col.SCT_MOD_REL
    NAME = TABLE.name
    COLUMNS = [
        col.SCT_MOD_REL_REL,
        col.SCT_MOD_REL_TERM,
        col.SCT_MOD_REL_NUM,
        col.SCT_MOD_REL_ALLOWED
    ]
    HEADER = [i.name for i in COLUMNS]
    CASTS = {
        col.SCT_MOD_REL_NUM.name: col.SCT_MOD_REL_NUM.type,
        col.SCT_MOD_REL_ALLOWED.name: col.SCT_MOD_REL_ALLOWED.type
    }


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class SctRel(ClinCodeExtractDesc):
    """
    The definition for the `sct_rel` table
    """
    TABLE = col.SCT_REL
    NAME = TABLE.name
    COLUMNS = [
        col.SCT_REL_CUI,
        col.SCT_REL_REL,
        col.SCT_REL_CUI2,
        col.SCT_REL_RG
    ]
    HEADER = [i.name for i in COLUMNS]
    CASTS = {
    }


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class SctDef(ClinCodeExtractDesc):
    """
    The definition for the `sct` table
    """
    BASENAME = 'SCT.snomed'
    TABLE = col.SCT
    NAME = TABLE.name
    COLUMNS = [
        col.SCT_CUI,
        col.SCT_PRIMITIVE,
        col.SCT_STAT,
        col.SCT_TUI,
        col.SCT_TUISTAT,
        col.SCT_TYP,
        col.SCT_TERM,
        col.SCT_F0,
        col.SCT_F1,
        col.SCT_F2,
        col.SCT_F3,
        col.SCT_F4,
        col.SCT_F5,
        col.SCT_FREQ,
        col.SCT_RDATE
    ]
    HEADER = [i.name for i in COLUMNS]
    CASTS = {
        col.SCT_PRIMITIVE.name: col.SCT_PRIMITIVE.type,
        col.SCT_STAT.name: col.SCT_STAT.type,
        col.SCT_TYP.name: col.SCT_TYP.type,
        col.SCT_F0.name: col.SCT_F0.type,
        col.SCT_F1.name: col.SCT_F1.type,
        col.SCT_F2.name: col.SCT_F2.type,
        col.SCT_F3.name: col.SCT_F3.type,
        col.SCT_F4.name: col.SCT_F4.type,
        col.SCT_F5.name: col.SCT_F5.type,
        col.SCT_FREQ.name: col.SCT_FREQ.type,
        col.SCT_RDATE.name: col.SCT_RDATE.type
    }
    CLINICAL_CODES = [
        # CUI column name, TUI column name, TERM column name,
        # code_type column, code_status column, Coding System
        (
            col.SCT_CUI.name,
            col.SCT_TUI.name,
            col.SCT_TERM.name,
            col.SCT_TYP.name,
            col.SCT_STAT.name,
            con.SNOMED_CODE_TYPE_NAME
        )
    ]


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class SctSubsetDef(ClinCodeExtractDesc):
    """
    The definition for the `sct_subset_defs` table
    """
    TABLE = col.SCT_SUBSET_DEFS
    NAME = TABLE.name
    COLUMNS = [
        col.SCT_SUBSET_DEFS_FOLDER,
        col.SCT_SUBSET_DEFS_REFSET_ID,
        col.SCT_SUBSET_DEFS_SUBSET_ID,
        col.SCT_SUBSET_DEFS_SUBSET_ORIGINAL_ID,
        col.SCT_SUBSET_DEFS_SUBSET_VERSION,
        col.SCT_SUBSET_DEFS_SUBSET_NAME,
        col.SCT_SUBSET_DEFS_SUBSET_TYPE
    ]
    HEADER = [i.name for i in COLUMNS]
    CASTS = {
        col.SCT_SUBSET_DEFS_SUBSET_VERSION.name:
        col.SCT_SUBSET_DEFS_SUBSET_VERSION.type
    }


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class SctSubsetRd(ClinCodeExtractDesc):
    """
    The definition for the `sct_subset_rds` table
    """
    TABLE = col.SCT_SUBSETS_RD
    NAME = TABLE.name
    COLUMNS = [
        col.SCT_SUBSETS_RD_SUBSET_ID,
        col.SCT_SUBSETS_RD_MEMBER_ID,
        col.SCT_SUBSETS_RD_MEMBER_STATUS,
        col.SCT_SUBSETS_RD_LINKED_ID
    ]
    HEADER = [i.name for i in COLUMNS]
    CASTS = {
        col.SCT_SUBSETS_RD_MEMBER_STATUS.name:
        col.SCT_SUBSETS_RD_MEMBER_STATUS.type
    }


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class SctSubset(ClinCodeExtractDesc):
    """
    The definition for the `sct_subsets` table
    """
    TABLE = col.SCT_SUBSETS
    NAME = TABLE.name
    COLUMNS = [
        col.SCT_SUBSETS_SUBSET_ID,
        col.SCT_SUBSETS_MEMBER_ID,
        col.SCT_SUBSETS_MEMBER_STATUS,
        col.SCT_SUBSETS_LINKED_ID
    ]
    HEADER = [i.name for i in COLUMNS]
    CASTS = {
        col.SCT_SUBSETS_MEMBER_STATUS.name: col.SCT_SUBSETS_MEMBER_STATUS.type
    }


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class SctVersion(ClinCodeExtractDesc):
    """
    The definition for the `sct_versions` table
    """
    TABLE = col.SCT_VERSIONS
    NAME = TABLE.name
    COLUMNS = [
        col.SCT_VERSIONS_ITEM,
        col.SCT_VERSIONS_RELEASE_DATE,
        col.SCT_VERSIONS_BUILD_DATE,
        col.SCT_VERSIONS_SNAPSHOT_DATE,
        col.SCT_VERSIONS_VERSION
    ]
    HEADER = [i.name for i in COLUMNS]
    CASTS = {
        col.SCT_VERSIONS_RELEASE_DATE.name: col.SCT_VERSIONS_RELEASE_DATE.type,
        col.SCT_VERSIONS_BUILD_DATE.name: col.SCT_VERSIONS_BUILD_DATE.type,
        col.SCT_VERSIONS_SNAPSHOT_DATE.name: col.SCT_VERSIONS_SNAPSHOT_DATE.type
    }


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class SctChist(ClinCodeExtractDesc):
    """
    The definition for the `sct_chist` table
    """
    TABLE = col.SCT_CHIST_CUI
    NAME = TABLE.name
    COLUMNS = [
        col.SCT_CHIST_CUI,
        col.SCT_CHIST_RELEASE_VERSION,
        col.SCT_CHIST_CHANGE_TYPE,
        col.SCT_CHIST_STAT,
        col.SCT_CHIST_REASON
    ]
    HEADER = [i.name for i in COLUMNS]
    CASTS = {
    }


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class SctEqv(ClinCodeExtractDesc):
    """
    The definition for the `sct_eqv` table
    """
    TABLE = col.SCT_EQV
    NAME = TABLE.name
    COLUMNS = [
        col.SCT_EQV_CUI1,
        col.SCT_EQV_CUI2,
        col.SCT_EQV_PROVENANCE,
        col.SCT_EQV_IDATE
    ]
    HEADER = [i.name for i in COLUMNS]
    CASTS = {
        col.SCT_EQV_PROVENANCE.name: col.SCT_EQV_PROVENANCE.type,
        col.SCT_EQV_IDATE.name: col.SCT_EQV_IDATE.type
    }


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class SctHist(ClinCodeExtractDesc):
    """
    The definition for the `sct_hist` table
    """
    TABLE = col.SCT_HIST
    NAME = TABLE.name
    COLUMNS = [
        col.SCT_HIST_OLD_CUI,
        col.SCT_HIST_OLD_STAT,
        col.SCT_HIST_NEW_CUI,
        col.SCT_HIST_NEW_STAT,
        col.SCT_HIST_IS_AMBIGUOUS,
        col.SCT_HIST_IDATE
    ]
    HEADER = [i.name for i in COLUMNS]
    CASTS = {
        col.SCT_HIST_OLD_STAT.name: col.SCT_HIST_OLD_STAT.type,
        col.SCT_HIST_NEW_STAT.name: col.SCT_HIST_NEW_STAT.type,
        col.SCT_HIST_IS_AMBIGUOUS.name: col.SCT_HIST_IS_AMBIGUOUS.type,
        col.SCT_HIST_IDATE.name: col.SCT_HIST_IDATE.type
    }


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class SctHrel(ClinCodeExtractDesc):
    """
    The definition for the `sct_hrel` table
    """
    TABLE = col.SCT_HREL
    NAME = TABLE.name
    COLUMNS = [
        col.SCT_HREL_CUI,
        col.SCT_HREL_REL,
        col.SCT_HREL_CUI2
    ]
    HEADER = [i.name for i in COLUMNS]
    CASTS = {
    }


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class SctMeta(ClinCodeExtractDesc):
    """
    The definition for the `sct_meta` table
    """
    TABLE = col.SCT_META
    NAME = TABLE.name
    COLUMNS = [
        col.SCT_META_CUI,
        col.SCT_META_STAT,
        col.SCT_META_RDATE,
        col.SCT_META_ADATE,
        col.SCT_META_IDATE,
        col.SCT_META_CTV3_ID
    ]
    HEADER = [i.name for i in COLUMNS]
    CASTS = {
        col.SCT_META_RDATE.name: col.SCT_META_RDATE.type,
        col.SCT_META_ADATE.name: col.SCT_META_ADATE.type,
        col.SCT_META_IDATE.name: col.SCT_META_IDATE.type
    }


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class SctSubst(ClinCodeExtractDesc):
    """
    The definition for the `sct_subst` table
    """
    TABLE = col.SCT_SUBST
    NAME = TABLE.name
    COLUMNS = [
        col.SCT_SUBST_OLD_CUI,
        col.SCT_SUBST_OLD_STAT,
        col.SCT_SUBST_NEW_CUI,
        col.SCT_SUBST_NEW_STAT,
        col.SCT_SUBST_IS_AMBIGUOUS
    ]
    HEADER = [i.name for i in COLUMNS]
    CASTS = {
    }


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class SctQt(ClinCodeExtractDesc):
    """
    The definition for the `sct_queries` table
    """
    TABLE = col.SCT_QT
    NAME = TABLE.name
    COLUMNS = [
        col.SCT_QT_SUPERTYPE_ID,
        col.SCT_QT_SUBTYPE_ID,
        col.SCT_QT_PROVENANCE
    ]
    HEADER = [i.name for i in COLUMNS]
    CASTS = {
        col.SCT_QT_PROVENANCE.name: col.SCT_QT_PROVENANCE.type
    }


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class SctTokenIndex(ClinCodeExtractDesc):
    """
    The definition for the `sct_token_index` table
    """
    TABLE = col.SCT_TOKEN_INDEX
    NAME = TABLE.name
    COLUMNS = [
        col.SCT_TOKEN_INDEX_TID,
        col.SCT_TOKEN_INDEX_CUI,
        col.SCT_TOKEN_INDEX_SOURCE,
        col.SCT_TOKEN_INDEX_TOKNUM
    ]
    HEADER = [i.name for i in COLUMNS]
    CASTS = {
        col.SCT_TOKEN_INDEX_TID.name: col.SCT_TOKEN_INDEX_TID.type,
        col.SCT_TOKEN_INDEX_SOURCE.name: col.SCT_TOKEN_INDEX_SOURCE.type,
        col.SCT_TOKEN_INDEX_TOKNUM.name: col.SCT_TOKEN_INDEX_TOKNUM.type
    }


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class SctTokens(ClinCodeExtractDesc):
    """
    The definition for the `sct_tokens` table
    """
    TABLE = col.SCT_TOKENS
    NAME = TABLE.name
    COLUMNS = [
        col.SCT_TOKENS_TID,
        col.SCT_TOKENS_TOKEN,
        col.SCT_TOKENS_NUM
    ]
    HEADER = [i.name for i in COLUMNS]
    CASTS = {
        col.SCT_TOKENS_TID.name: col.SCT_TOKENS_TID.type,
        col.SCT_TOKENS_NUM.name: col.SCT_TOKENS_NUM.type
    }


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class SctWeq(ClinCodeExtractDesc):
    """
    The definition for the `sct_weq` table
    """
    TABLE = col.SCT_WEQ
    NAME = TABLE.name
    COLUMNS = [
        col.SCT_WEQ_TID,
        col.SCT_WEQ_EQT
    ]
    HEADER = [i.name for i in COLUMNS]
    CASTS = {
        col.SCT_WEQ_TID.name: col.SCT_WEQ_TID.type,
        col.SCT_WEQ_EQT.name: col.SCT_WEQ_EQT.type
    }


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class BnfDef(object):
    """
    The definition for BNF lookup file
    """
    BASENAME = "bnf_lkp"
    TABLE = col.BNF
    NAME = TABLE.name
    COLUMNS = [
        col.BNF_PRES_CODE,
        col.BNF_PRES,
        col.BNF_PRODUCT,
        col.BNF_CHEMICAL_SUBSTANCE,
        col.BNF_SUB_PARA,
        col.BNF_PARA,
        col.BNF_SECTION,
        col.BNF_CHAPTER
    ]
    HEADER = [i.name for i in COLUMNS]


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class DmdDef(ClinCodeExtractDesc):
    """
    The definition for the DMD lookup table
    """
    BASENAME = "dmd_lkp"
    TABLE = col.DMD
    NAME = TABLE.name
    COLUMNS = [
        col.DMD_CUI,
        col.DMD_TERM
    ]
    HEADER = [i.name for i in COLUMNS]
    CASTS = {
        col.DMD_CUI.name: col.DMD_CUI.type
    }
    CLINICAL_CODES = [
        # CUI column name, TUI column name, TERM column name,
        # code_type column, code_status column, Coding System
        (
            col.DMD_CUI.name,
            None,
            col.DMD_TERM.name,
            None,
            None,
            con.DMD_CODE_TYPE_NAME
        )
    ]


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Icd9Def(ClinCodeExtractDesc):
    """
    A handler for the `icd9` table
    """
    BASENAME = "icd9_lkp"
    TABLE = col.ICD9
    NAME = TABLE.name
    COLUMNS = [
        col.ICD9_CUI,
        col.ICD9_TERM
    ]
    HEADER = [i.name for i in COLUMNS]
    CLINICAL_CODES = {
        (
            col.ICD9_CUI.name,
            None,
            col.ICD9_TERM.name,
            None,
            None,
            con.ICD9_CODE_TYPE_NAME
        )
    }


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Icd10UkbbDef(ClinCodeExtractDesc):
    """
    A handler for the ICD10 lookup table
    """
    BASENAME = "icd10_lkp"
    TABLE = col.ICD10_UKBB
    NAME = TABLE.name
    COLUMNS = [
        col.ICD10_UKBB_CUI,
        col.ICD10_UKBB_ALT_CUI,
        col.ICD10_UKBB_USAGE,
        col.ICD10_UKBB_USAGE_UK,
        col.ICD10_UKBB_TERM,
        col.ICD10_UKBB_MOD4,
        col.ICD10_UKBB_MOD5,
        col.ICD10_UKBB_QUAL,
        col.ICD10_UKBB_GENDER,
        col.ICD10_UKBB_MIN_AGE,
        col.ICD10_UKBB_MAX_AGE,
        col.ICD10_UKBB_TREE_DESCRIPTION
    ]
    HEADER = [i.name for i in COLUMNS]
    CLINICAL_CODES = [
        (
            col.ICD10_UKBB_CUI.name,
            None,
            col.ICD10_UKBB_TERM.name,
            None,
            None,
            con.ICD10_CODE_TYPE_NAME
        ),
        (
            col.ICD10_UKBB_ALT_CUI.name,
            None,
            col.ICD10_UKBB_TERM.name,
            None,
            None,
            con.ICD10_CODE_TYPE_NAME
        )
    ]
    NONE_VALUES = ['', 'None']
    OPT_CASTS = {
        col.ICD10_UKBB_USAGE_UK.name: col.ICD10_UKBB_USAGE_UK.type
    }


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class ReadCodeFix(ClinCodeExtractDesc):
    """
    Parses out the read codes and applied some simple fixes where possible.
    this is to handle the auto-formatting errors in the UKBB mapping files.
    This will only fix the codes that it is safe to do so and can only fix
    the ones that are not 5 bytes. There are 3852 of these in read2, 7916 in
    ctv3. How UKBB let this out the door I do not know as this could seriously
    screw someones research.
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @classmethod
    def get_clinical_code(cls, row):
        fixed_codes = set([])
        for i in super().get_clinical_code(row):
            try:
                new_code, new_tui, code_status = CUI_FIX.get(
                    i.code, i.tui, i.coding_system
                )
                # Three digits not ending in 0
                fixed_codes.add(
                    CuiCode(
                        new_code,
                        i.tui,
                        i.term,
                        i.coding_system,
                        code_type=i.code_type,
                        code_status=code_status
                    )
                )
            except KeyError:
                # does not need fixing
                fixed_codes.add(i)
                # if len(i.code) != 5:
                #     print(i)
                #     raise

        return fixed_codes


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class RctUkbbDef(ReadCodeFix):
    """
    A handler for the GP registrations file
    """
    BASENAME = "read_v2_lkp"
    TABLE = col.RCT_UKBB
    NAME = TABLE.name
    COLUMNS = [
        col.RCT_UKBB_CUI,
        col.RCT_UKBB_TUI,
        col.RCT_UKBB_TERM
    ]
    HEADER = [i.name for i in COLUMNS]
    CLINICAL_CODES = [
        (
            col.RCT_UKBB_CUI.name,
            col.RCT_UKBB_TUI.name,
            col.RCT_UKBB_TERM.name,
            None,
            None,
            con.READ2_CODE_TYPE_NAME
        )
    ]
    CASTS = {
        col.RCT_UKBB_TUI.name: col.RCT_UKBB_TUI.type
    }


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class RctDrugsUkbbDef(ClinCodeExtractDesc):
    """
    A handler for the samples file that is built from the wide file
    """
    BASENAME = "read_v2_drugs_lkp"
    TABLE = col.RCT_UKBB_DRUGS
    NAME = TABLE.name
    COLUMNS = [
        col.RCT_UKBB_DRUGS_CUI,
        col.RCT_UKBB_DRUGS_TERM,
        col.RCT_UKBB_DRUGS_STATUS
    ]
    HEADER = [i.name for i in COLUMNS]
    CLINICAL_CODES = [
        (
            col.RCT_UKBB_DRUGS_CUI,
            None,
            col.RCT_UKBB_DRUGS_TERM,
            None,
            None,
            con.READ2_DRUGS_CODE_TYPE_NAME
        )
    ]
    CASTS = {
        col.RCT_UKBB_DRUGS_STATUS.name: col.RCT_UKBB_DRUGS_STATUS.type
    }


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Ctv3UkbbDef(ReadCodeFix):
    """
    Header and data type definitions for the read_ctv3_lkp file
    """
    BASENAME = "read_ctv3_lkp"
    TABLE = col.CTV3_UKBB
    NAME = TABLE.name
    COLUMNS = [
        col.CTV3_UKBB_CUI,
        col.CTV3_UKBB_TERM,
        col.CTV3_UKBB_TYPE,
        col.CTV3_UKBB_STATUS
    ]
    HEADER = [i.name for i in COLUMNS]
    CLINICAL_CODES = [
        (
            col.CTV3_UKBB_CUI.name,
            None,
            col.CTV3_UKBB_TERM.name,
            col.CTV3_UKBB_TYPE.name,
            col.CTV3_UKBB_STATUS.name,
            con.CTV3_CODE_TYPE_NAME
        )
    ]


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class OpcsExtract(ClinCodeExtractDesc):
    """
    """
    OPCS_DESC = ""

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @classmethod
    def process_opcs_code(cls, d):
        """
        Extracts an OPCS term code from the description and returns it
        """
        return cls.OPCS_DESC.sub('', d)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @classmethod
    def get_clinical_code(cls, row):
        """
        Extract the clinical code

        Parameters
        ----------
        row : list or dict
            A single data row to extract all the clinical codes from
        """
        codes = super().get_clinical_code(row)
        updated_codes = set()
        for i in codes:
            code_match = cls.OPCS_DESC.match(i.term)

            try:
                dot_code = code_match.group('DOT')
                cleaned_term = cls.OPCS_DESC.sub('', i.term)
                cleaned_term_tui = hashlib.md5(
                    cleaned_term.encode()
                ).hexdigest()

                updated_codes.update(
                    [
                        CuiCode(
                            i.code,
                            cleaned_term_tui,
                            cleaned_term,
                            i.coding_system
                        ),
                        CuiCode(
                            dot_code,
                            i.tui,
                            i.term,
                            i.coding_system
                        ),
                        CuiCode(
                            dot_code,
                            cleaned_term_tui,
                            cleaned_term,
                            i.coding_system
                        )
                    ]
                )
            except AttributeError:
                pass
        return codes.union(updated_codes)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Opcs3Def(OpcsExtract):
    """
    Header and data type definitions for the `opcs3` table
    """
    TABLE = col.OPCS3
    NAME = TABLE.name
    COLUMNS = [
        col.OPCS3_CUI,
        col.OPCS3_TERM,
        col.OPCS3_NODE,
        col.OPCS3_PARENT,
        col.OPCS3_SELECT
    ]
    HEADER = [i.name for i in COLUMNS]
    CASTS = {
        col.OPCS3_NODE.name: col.OPCS3_NODE.type,
        col.OPCS3_PARENT.name: col.OPCS3_PARENT.type
    }
    CLINICAL_CODES = [
        # CUI column name, TUI column name, TERM column name,
        # code_type column, code_status column, Coding System
        (
            col.OPCS3_CUI.name,
            None,
            col.OPCS3_TERM.name,
            None,
            None,
            con.OPCS3_CODE_TYPE_NAME
        )
    ]
    OPCS_DESC = re.compile(
        r'^\s*(?P<DOT>\d+(?:\.\d+)?|Chapter\s*[IXV]+)\s*-?\s*'
    )


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Opcs4UkbbDef(OpcsExtract):
    """
    Header and data type definitions for the opcs4_lkp file
    """
    """
    Header and data type definitions for the `opcs3` table
    """
    TABLE = col.OPCS4_UKBB
    NAME = TABLE.name
    COLUMNS = [
        col.OPCS4_UKBB_CUI,
        col.OPCS4_UKBB_TERM,
        col.OPCS4_UKBB_NODE,
        col.OPCS4_UKBB_PARENT,
        col.OPCS4_UKBB_SELECT
    ]
    HEADER = [i.name for i in COLUMNS]
    CASTS = {
        col.OPCS4_UKBB_NODE.name: col.OPCS4_UKBB_NODE.type,
        col.OPCS4_UKBB_PARENT.name: col.OPCS4_UKBB_PARENT.type
    }
    CLINICAL_CODES = [
        # CUI column name, TUI column name, TERM column name,
        # code_type column, code_status column, Coding System
        (
            col.OPCS4_UKBB_CUI.name,
            None,
            col.OPCS4_UKBB_TERM.name,
            None,
            None,
            con.OPCS4_CODE_TYPE_NAME
        )
    ]
    OPCS_DESC = re.compile(
        r'^\s*(?P<DOT>\w+\d+(?:\.\d+)?|Chapter\s*[A-Z])\s*-?\s*'
    )


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Icd9Icd10MapDef(MappingExtract):
    """
    A handler for the `icd9_icd10_map` table
    """
    BASENAME = "icd9_icd10"
    TABLE = col.ICD9_ICD10_MAP
    NAME = TABLE.name
    COLUMNS = [
        col.ICD9_ICD10_MAP_CUI1,
        col.ICD9_ICD10_MAP_TERM1,
        col.ICD9_ICD10_MAP_CUI2,
        col.ICD9_ICD10_MAP_TERM2
    ]
    HEADER = [i.name for i in COLUMNS]
    NONE_VALUES = ['', 'None', 'UNDEF']
    BIDIRECTIONAL = True
    SOURCE_CUI = col.ICD9_ICD10_MAP_CUI1.name
    SOURCE_CODING_SYSTEM = con.ICD9_CODE_TYPE_NAME
    TARGET_CUI = col.ICD9_ICD10_MAP_CUI2.name
    TARGET_CODING_SYSTEM = con.ICD10_CODE_TYPE_NAME
    MAP_TYPE = None


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class FixMappingExtract(MappingExtract):
    """
    Extracts mappings from a row and applies a code fix to the source cui
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @classmethod
    def get_mappings(cls, row):
        """
        Extract a mapping from the row

        Parameters
        ----------
        row : dict
            A single data row to extract the mapping data from
        """
        fixed_mappings = []
        for source, target, mapping_type in super().get_mappings(row):
            try:
                # Here I have fixed the TUI to 00 this will be ignored in the
                # case of CTV3 and will be the default in the case for read2
                # I am hoping it will be good enough
                # TODO: Look into the implementation of this
                new_code, new_tui, code_status = CUI_FIX.get(
                    source[1], '00', source[0]
                )
                # Three digits not ending in 0
                fixed_mappings.append(
                    ((source[0], new_code), target, mapping_type)
                )
            except KeyError:
                # does not need fixing
                fixed_mappings.append((source, target, mapping_type))

        return fixed_mappings


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Ctv3Icd10MapDef(FixMappingExtract):
    """
    Header and data type definitions for the read_ctv3_icd10 file
    """
    BASENAME = 'read_ctv3_icd10'
    TABLE = col.CTV3_ICD10_MAP
    NAME = TABLE.name
    COLUMNS = [
        col.CTV3_ICD10_READ3_CODE,
        col.CTV3_ICD10_ICD10_CODE,
        col.CTV3_ICD10_MAP_STATUS,
        col.CTV3_ICD10_REFINE,
        col.CTV3_ICD10_ADD_CODE,
        col.CTV3_ICD10_ELEMENT_NO,
        col.CTV3_ICD10_BLOCK_NO
    ]
    HEADER = [i.name for i in COLUMNS]
    CASTS = {col.CTV3_ICD10_MAP_STATUS.name: col.CTV3_ICD10_MAP_STATUS.type}
    NONE_VALUES = ['', 'None']
    BIDIRECTIONAL = False
    SOURCE_CUI = col.CTV3_ICD10_READ3_CODE.name
    SOURCE_CODING_SYSTEM = con.CTV3_CODE_TYPE_NAME
    TARGET_CUI = col.CTV3_ICD10_ICD10_CODE.name
    TARGET_CODING_SYSTEM = con.ICD10_CODE_TYPE_NAME
    MAP_TYPE = col.CTV3_ICD10_MAP_STATUS.name


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Ctv3Icd9MapDef(FixMappingExtract):
    """
    Header and data type definitions for the read_ctv3_icd9 file
    """
    BASENAME = 'read_ctv3_icd9'
    TABLE = col.CTV3_ICD9_MAP
    NAME = TABLE.name
    COLUMNS = [
        col.CTV3_ICD9_READ3_CODE,
        col.CTV3_ICD9_ICD9_CODE,
        col.CTV3_ICD9_MAP_STATUS,
        col.CTV3_ICD9_REFINE,
        col.CTV3_ICD9_ADD_CODE,
        col.CTV3_ICD9_ELEMENT_NO,
        col.CTV3_ICD9_BLOCK_NO
    ]
    HEADER = [i.name for i in COLUMNS]
    CASTS = {col.CTV3_ICD9_MAP_STATUS.name: col.CTV3_ICD9_MAP_STATUS.type}
    NONE_VALUES = ['', 'None']
    BIDIRECTIONAL = False
    SOURCE_CUI = col.CTV3_ICD9_READ3_CODE.name
    SOURCE_CODING_SYSTEM = con.CTV3_CODE_TYPE_NAME
    TARGET_CUI = col.CTV3_ICD9_ICD9_CODE.name
    TARGET_CODING_SYSTEM = con.ICD9_CODE_TYPE_NAME
    MAP_TYPE = col.CTV3_ICD9_MAP_STATUS.name


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Ctv3Opcs4MapDef(FixMappingExtract):
    """
    Header and data type definitions for the read_ctv3_opcs4 file
    """
    BASENAME = "read_ctv3_opcs4"
    TABLE = col.CTV3_OPCS4_MAP
    NAME = TABLE.name
    COLUMNS = [
        col.CTV3_OPCS4_READ3_CODE,
        col.CTV3_OPCS4_OPCS4_CODE,
        col.CTV3_OPCS4_MAP_STATUS,
        col.CTV3_OPCS4_REFINE,
        col.CTV3_OPCS4_ADD_CODE,
        col.CTV3_OPCS4_ELEMENT_NO,
        col.CTV3_OPCS4_BLOCK_NO
    ]
    HEADER = [i.name for i in COLUMNS]
    CASTS = {col.CTV3_OPCS4_MAP_STATUS.name: col.CTV3_OPCS4_MAP_STATUS.type}
    NONE_VALUES = ['', 'None']
    BIDIRECTIONAL = False
    SOURCE_CUI = col.CTV3_OPCS4_READ3_CODE.name
    SOURCE_CODING_SYSTEM = con.CTV3_CODE_TYPE_NAME
    TARGET_CUI = col.CTV3_OPCS4_OPCS4_CODE.name
    TARGET_CODING_SYSTEM = con.OPCS4_CODE_TYPE_NAME
    MAP_TYPE = col.CTV3_OPCS4_MAP_STATUS.name


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Ctv3RctUkbbMapDef(object):
    """
    Header and data type definitions for the read_ctv3_read_v2 file
    """
    TABLE = col.CTV3_RCT_UKBB_MAP
    NAME = TABLE.name
    COLUMNS = [
        col.CTV3_RCT_UKBB_READ3_CODE,
        col.CTV3_RCT_UKBB_CTV3_CODE,
        col.CTV3_RCT_UKBB_READ3_DESC_TYPE,
        col.CTV3_RCT_UKBB_READ3_DESC,
        col.CTV3_RCT_UKBB_READ2_CODE,
        col.CTV3_RCT_UKBB_READ2_DESC,
        col.CTV3_RCT_UKBB_READ2_DESC_ORDER,
        col.CTV3_RCT_UKBB_READ2_DESC_TYPE,
        col.CTV3_RCT_UKBB_READ2_ALT_DESC,
        col.CTV3_RCT_UKBB_IS_ASSURED
    ]
    HEADER = [i.name for i in COLUMNS]
    CASTS = {
        col.CTV3_RCT_UKBB_IS_ASSURED.name: col.CTV3_RCT_UKBB_IS_ASSURED.type
    }


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class RctIcd10MapDef(object):
    """
    Header and data type definitions for the read_v2_icd10 file
    """
    BASENAME = "read_v2_icd10"
    TABLE = col.RCT_ICD10_UKBB_MAP
    NAME = TABLE.name
    COLUMNS = [
        col.RCT_ICD10_UKBB_READ2_CODE,
        col.RCT_ICD10_UKBB_ICD10_CODE,
        col.RCT_ICD10_UKBB_ICD10_DEF
    ]
    HEADER = [i.name for i in COLUMNS]
    CASTS = {
        col.RCT_ICD10_UKBB_ICD10_DEF.name: col.RCT_ICD10_UKBB_ICD10_DEF.type
    }


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class RctIcd9MapDef(object):
    """
    Header and data type definitions for the clinical_data file
    """
    TABLE = col.RCT_ICD9_UKBB_MAP
    NAME = TABLE.name
    COLUMNS = [
        col.RCT_ICD9_UKBB_READ2_CODE,
        col.RCT_ICD9_UKBB_ICD9_CODE,
        col.RCT_ICD9_UKBB_ICD9_DEF
    ]
    HEADER = [i.name for i in COLUMNS]
    CASTS = {
        col.RCT_ICD9_UKBB_ICD9_DEF.name: col.RCT_ICD9_UKBB_ICD9_DEF.type
    }


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class RctOpcs4MapDef(MappingExtract):
    """
    Header and data type definitions for the read_v2_opcs4 file
    """
    BASENAME = "read_v2_opcs4"
    TABLE = col.RCT_OPCS4_UKBB_MAP
    NAME = TABLE.name
    COLUMNS = [
        col.RCT_OPCS4_UKBB_READ2_CODE,
        col.RCT_OPCS4_UKBB_OPCS4_CODE,
        col.RCT_OPCS4_UKBB_OPCS4_DEF
    ]
    HEADER = [i.name for i in COLUMNS]
    CASTS = {
        col.RCT_OPCS4_UKBB_OPCS4_DEF.name: col.RCT_OPCS4_UKBB_OPCS4_DEF.type
    }


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class RctCtv3UkbbMapDef(object):
    """
    Header and data type definitions for the read_v2_read_ctv3 file
    """
    TABLE = col.RCT_CTV3_UKBB_MAP
    NAME = TABLE.name
    COLUMNS = [
        col.RCT_CTV3_UKBB_CHAPTER,
        col.RCT_CTV3_UKBB_READ2_CODE,
        col.RCT_CTV3_UKBB_READ2_DESC,
        col.RCT_CTV3_UKBB_READ2_ALT_DESC,
        col.RCT_CTV3_UKBB_READ2_DESC_ORDER,
        col.RCT_CTV3_UKBB_READ2_DESC_TYPE,
        col.RCT_CTV3_UKBB_READ3_CODE,
        col.RCT_CTV3_UKBB_CTV3_CODE,
        col.RCT_CTV3_UKBB_READ3_DESC_TYPE,
        col.RCT_CTV3_UKBB_READ3_DESC,
        col.RCT_CTV3_UKBB_IS_ASSURED
    ]
    HEADER = [i.name for i in COLUMNS]
    CASTS = {
        col.RCT_CTV3_UKBB_IS_ASSURED.name: col.RCT_CTV3_UKBB_IS_ASSURED.type
    }
