"""A build script and API endpoint for building the UKBB tools mapping database
"""
from ukbb_tools import config, __version__, __name__ as pkg_name, \
    constants as con, common
from ukbb_tools.mapping import readers as mr, orm, column_defs as col, \
    file_defs
from simple_progress import progress
from itertools import product
import argparse
import csv
import shutil
import sys
import os
import pkg_resources
import warnings
import glob
import hashlib
import pprint as pp


MSG_PREFIX = '[info]'
"""The prefix for verbose messages and the location to write the messages
(`str`)
"""
MSG_OUT = sys.stderr
"""The location where verbose messages are sent (`sys.stderr` or `sys.stdout`)
"""
GLOBAL_DELIMITER = "\t"
"""All files written out in this module will have this as the delimiter (`str`)
"""

STATUS_CODES = {
    'U': "unknown code status: applies to opcs3, icd10, icd9, read2, opcs4",
    '0': "snomed ct code status 0: unclear what this means",
    '1': "snomed ct code status 1: unclear what this means",
    '2': "snomed ct code status 2: unclear what this means",
    '3': "snomed ct code status 3: unclear what this means",
    '4': "snomed ct code status 4: unclear what this means",
    '5': "snomed ct code status 5: unclear what this means",
    '6': "snomed ct code status 6: unclear what this means",
    '10': "snomed ct code status 7: unclear what this means",
    '11': "snomed ct code status 8: unclear what this means",
    'C': "ctv3 code status C: i think this means current",
    'E': "ctv3 code status E: i think this means extinct",
    'O': "ctv3 code status O: i think this means optional",
    'R': "ctv3 code status R: i think this means retired",
    'F': "read/ctv3 that has been fixed due to auto-formatting errors",
    'M': ("read/ctv3 that has been fixed due to auto-formatting errors"
          " but might need some manual checking"),
    'RF': ("read/ctv3 that has been fixed due to auto-formatting errors"
           " but required some modifications to the term string to do so"),
    'P': "orphan code"
 }
"""The status codes that are available in the mapping files (`dict`)

The keys are `str` of single letter codes and the values are `str` descriptions
 of the code meaning. The U status code is a default that is applied when no
 status code is available in the file.
"""

CODE_TYPES = {
    'U': "unknown code type: applies to read2, opcs4, icd9, icd10, opcs3",
    '1': "snomed code type 1: unclear what this means",
    '2': "snomed code type 2: unclear what this means",
    '3': "snomed code type 3: unclear what this means",
    'P': "ctv3 code type P: the primary term",
    'S': "ctv3 code type S: the secondary term",
    'O': "orphan code"
}
"""The code types that are available in the mapping files (`dict`)

The keys are `str` of single letter codes and the values are `str` descriptions
 of the code meaning. The U status code is a default that is applied when no
 status code is available in the file.
"""

MAPPING_TYPES = {
    'N': "No mapping",
    'E': "Exact mapping",
    'A': "Approximate mapping",
    'U': "Unsafe mapping",
    'S': "Self mapping",
    'Q': "Equivalence mapping",
    'M': "User defined manual mapping",
    'X': "Unknown mapping type",
    'G': "The target concept is more general than the source concept",
    'D': "Best mapping in the absence of other information or partial mapping",
    'R': "Requires checking",
    'L': "Alternative mapping, alternative read/CTV3 mappings that are not D/R"
}
"""The mapping types (`dict`)

The keys are `str` of single letter codes and the values are `str` descriptions
 of the code meaning. The N, E, A are derived from the NHS mapping tables the U
 is my own creation (see mapper.column_defs.RCT_CTV3_MAP_MAPTYP) as is the S
 and Q (derived from the *EQV tables)
"""


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class CuiCollection(object):
    """
    A container class that will hold all the concept cui that are collected
    from the files and make them available in a variety of ways
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @classmethod
    def get_pref_term_longest(cls, code_set):
        """
        Get the concept with the preferred term from a set of concepts with the
        same cui and coding system (assumed not checked) where preferred term
        is defined as the concept CUI with the longest term length.

        Parameters
        ----------
        cui_set : set of `file_defs.CuiCode`
            A set `file_defs.CuiCode` to extract the preferred
            `file_defs.CuiCode` code from. Note that the these are not checked
            for coding system or that they have the same cui

        Returns
        -------
        pref_cui : `file_defs.CuiCode`
            The preferred CUI based on the longest clinical term string
        """
        return sorted(code_set, key=lambda x: len(x.term), reverse=True)[0]

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @classmethod
    def get_pref_term_snomed(cls, code_set):
        """
        Get the concept with the preferred term from a set of concepts with the
        same cui and coding system (assumed not checked) where preferred term
        is defined as the concept CUI with the lowest integer value for the
        code_type attribute. The code_type attribute will only be an integer
        for snomed clinical terms, so whilst this will not error out of non
        snomed cuis are used the results may not be what you expect.

        Parameters
        ----------
        cui_set : set of `file_defs.CuiCode`
            A set `file_defs.CuiCode` to extract the preferred
            `file_defs.CuiCode` code from. Note that the these are not checked
            for coding system or that they have the same cui

        Returns
        -------
        pref_cui : `file_defs.CuiCode`
            The preferred CUI based on the lowest code_type value. I am not 100%
            sure if this is the preferred term for Snomed CT CUIs but it is
            consistent. This will be checked in more detail in future
        """
        return sorted(code_set, key=lambda x: x.code_type)[0]

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @classmethod
    def get_pref_term_rcd(cls, code_set):
        """
        Get the concept with the preferred term from a set of concepts with the
        same cui and coding system (assumed not checked) where preferred term
        is defined as the concept CUI with the TUI value of '00'. If none are
        found then the lowest TUI in the sort order is returned. The TUI value
        of 00 11, 12 etc are only used in the read2 terms.

        Parameters
        ----------
        cui_set : set of `file_defs.CuiCode`
            A set `file_defs.CuiCode` to extract the preferred
            `file_defs.CuiCode` code from. Note that the these are not checked
            for coding system or that they have the same cui

        Returns
        -------
        pref_cui : `file_defs.CuiCode`
            The preferred CUI based on having a TUI of 00 or if none are found
            then the lowest TUI in the sort order is returned.
        """
        for i in code_set:
            if i.tui == '00':
                return i
        return sorted(code_set, key=lambda x: x.tui)[0]

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @classmethod
    def get_pref_term_ctv3(cls, code_set):
        """
        Get the concept with the preferred term from a set of concepts with the
        same cui and coding system (assumed not checked) where preferred term
        is defined as the concept CUI with the `code_type` P. The code_type P
        is the primary term in CTV3 codes. If none are found then the longest
        term is returned.

        Parameters
        ----------
        cui_set : set of `file_defs.CuiCode`
            A set `file_defs.CuiCode` to extract the preferred
            `file_defs.CuiCode` code from. Note that the these are not checked
            for coding system or that they have the same cui

        Returns
        -------
        pref_cui : `file_defs.CuiCode`
            The preferred CUI based on having a `code_type` of P. If none are
            found then the longest term is returned.
        """
        for i in code_set:
            if i.code_type == 'P':
                return i
        return cls.get_pref_term_longest(code_set)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self):
        """
        The underlying collection is stored in nested dicts with the first
        level partitioned on coding_system and below each coding system a dict
        that is keyed on a tuple of (coding_system_name, cui). The values of
        this dict are sets of `file_defs.CuiCode`.
        """
        self._codes = {}

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def length(self):
        """
        get the length of the collection

        Returns
        -------
        length : int
            The number of `file_defs.CuiCode` objects in the collection
        """
        length = 0
        for code_system in self._codes.keys():
            for code_key, code_set in self._codes[code_system].items():
                length += len(code_set)
        return length

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def add(self, new_codes):
        """
        Add a set of codes to the collection

        Parameters
        ----------
        new_codes : set of `file_defs.CuiCode`
            The codes to add to the collection
        """
        for i in new_codes:
            try:
                # Is the coding system present already
                self._codes[i.key[0]]
            except KeyError:
                # No then create it
                self._codes[i.key[0]] = {}

            try:
                # Add the CuiCodes to the coding_system, cui key
                self._codes[i.key[0]][i.key].add(i)
            except KeyError:
                # No codes defined yet so we initialise
                self._codes[i.key[0]][i.key] = set([i])

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get(self, cui, coding_system):
        """
        get a `file_defs.CuiCode` from the collection that matches the
        coding_system and cui

        Parameters
        ----------
        cui : str
            The cui code to search for
        coding_system : str
            The coding system that the cui belongs to

        Returns
        -------
        matching_code : :obj:`file_defs.CuiCode`
            The `file_defs.CuiCode` that matches the `cui` and `coding_system`

        Raises
        ------
        KeyError
            If there is no match in the collection
        """
        return self._codes[coding_system][(coding_system, cui)]

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_longest_term(self):
        """
        get the concept that contains the longest term in the collection

        Returns
        -------
        longest_term : :obj:`file_defs.CuiCode`
            The code containing the longest term string in the collection
        """
        return self._longest_attr('term')

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_longest_cui(self):
        """
        get the concept that contains the longest cui code string in the
        collection

        Returns
        -------
        longest_cui : :obj:`file_defs.CuiCode`
            The code containing the longest cui string in the collection
        """
        return self._longest_attr('code')

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _longest_attr(self, attr_name):
        """
        A helper method to extract the cui code containing the attribute value
        that has the longest length.

        Parameters
        ----------
        attr_name : str
            The attribute of a `file_defs.CuiCode` that you want the unique
            values for

        Returns
        -------
        longest_obj : :obj:`file_defs.CuiCode`
            The code containing the longest attribute value that is associated
            with the `attr_name` in the collection
        """
        # Will hold the CuiCode that has the attribute value associated with the
        # attribute name with longest length
        longest_obj = None
        longest_attr = ""
        for k, v in self._codes.items():
            for codes in self._codes[k].values():
                for code in codes:
                    try:
                        if len(getattr(code, attr_name)) > len(longest_attr):
                            longest_attr = getattr(code, attr_name)
                            longest_obj = code
                    except (AttributeError, TypeError):
                        pass
        return longest_obj

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_status_codes(self):
        """
        get all the unique status codes in the collection with the coding
        systems that they occur in

        Returns
        -------
        status_codes : dict
            The keys of the dict are the status codes and the values
            are sets of coding system names that contain them
        """
        return self._get_unique_attr('code_status')

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_code_types(self):
        """
        get all the unique code types in the collection with the coding
        systems that they occur in

        Returns
        -------
        code_types : dict
            The keys of the dict are the code types and the values
            are sets of coding system names that contain them
        """
        return self._get_unique_attr('code_type')

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _get_unique_attr(self, attr_name):
        """
        Helper method to extract unique values of an attribute and the coding
        systems that they occur in

        Parameters
        ----------
        attr_name : str
            The attribute of a `file_defs.CuiCode` that you want the unique
            values for

        Returns
        -------
        unique_attr : dict
            The keys of the dict are the attribute values for the `attr_name`
            and the values are sets of coding system names that contain them
        """
        unique_attr = {}
        for code in self.yield_all_cui():
            attr_value = getattr(code, attr_name)
            try:
                unique_attr[attr_value].add(code.coding_system)
            except KeyError:
                unique_attr[attr_value] = set([code.coding_system])
        return unique_attr

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def yield_unique_cui(self):
        """
        Yield unique CUIs with the preferred them for each one. The method to
        derive the preferred term will change depending on the coding system
        with the default being based on the longest term string.

        Yields
        ------
        cui_code : :obj:`file_defs.CuiCode`
            The cui code containing the preferred term
        """

        for code_system in self._codes.keys():
            pref_term_method = CuiCollection.get_pref_term_longest
            if code_system == con.SNOMED_CODE_TYPE_NAME:
                pref_term_method = CuiCollection.get_pref_term_snomed
            elif code_system == con.CTV3_CODE_TYPE_NAME:
                pref_term_method = CuiCollection.get_pref_term_ctv3
            elif code_system == con.READ2_CODE_TYPE_NAME:
                pref_term_method = CuiCollection.get_pref_term_rcd

            for code_key, code_set in self._codes[code_system].items():
                yield pref_term_method(code_set)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def yield_all_cui(self):
        """
        Yield all CUIs in the collection. The cuis qre yielded by coding system
        name.

        Yields
        ------
        cui_code : :obj:`file_defs.CuiCode`
            The cui code
        """

        for code_system in self._codes.keys():
            for code_key, code_set in self._codes[code_system].items():
                for code in code_set:
                    yield code


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main():
    """The main entry point for the script
    """
    # Initialise and parse all the command line arguments
    parser = init_cmd_args()
    args = parse_cmd_args(parser)

    # Write messages to the user respecting verbosity
    m = progress.Msg(prefix='', verbose=args.verbose, file=MSG_OUT)
    m.msg("= build UKBB mapper DB ({0} v{1}) =".format(pkg_name, __version__))
    m.prefix = MSG_PREFIX
    m.msg_atts(args)

    m.msg("reading config file")
    m.msg("initialising database")

    # Setup access to the config file and the database. The returned objects
    # 1. configparser
    # 2. SQLAlchemy Sessionmanager for the mapping database that is being built
    # 3. SQLAlchemy Sessionmanager for the UMLS database this could be NoneType
    #    if the UMLS is not used
    # 4. UK BioBank clinical code pool is a list of all the clinical codes in
    #    the UK BioBank clinical data
    # 5. User defined mapping inclusions - these override any computationally
    #    defined mappings
    # 6. User defined mapping exclusions - these are removed from any
    #    computationally defined mappings
    config_obj, ukbb_map_sessm, umls_sessm, ukbb_code_pool, include_cache, \
        exclude_cache = initialise(
            config_file=args.config_file,
            ukbb_code_pool_path=args.ukbb_code_pool,
            user_includes_path=args.include_file,
            user_excludes_path=args.exclude_file
        )

    # # We create a temp directory inside the tempdir we have been given
    # # (or the system temp). That way we can delete the whole thing if we
    # # have an error
    # tmp_working_dir = tempfile.mkdtemp(dir=args.tmp_dir)
    try:
        m.msg("converting excel file to text, this may be slow...")

        # TODO: uncomment in production
        # Convert the Excel workbooks into tab delimited files
        # write_worksheets(
        #     args.ukbb_mapping_file,
        #     outdir=tmp_working_dir,
        #     verbose=args.verbose
        # )
        # TODO: remove in production
        tmp_working_dir = args.keep_files
        build_mapping_db(
            ukbb_map_sessm,
            tmp_working_dir,
            args.opcs3_file,
            include_cache,
            exclude_cache,
            ukbb_code_pool=ukbb_code_pool,
            umls_sessm=umls_sessm,
            nhs_digital_dir=args.nhs_digital,
            opcs4_file=args.opcs4_file,
            verbose=args.verbose
        )
    finally:
        # TODO: uncomment in production
        # This finally block will get run in all instances (success/failure)
        # if args.keep_files:
        #     # pass
        #     try:
        #         # If we want to keep a copy of the files that have been used
        #         # to build the database
        #         m.msg("relocation of build files to: '{0}'".format(
        #             args.keep_files))
        #         move_files(tmp_working_dir, args.keep_files)
        #     except Exception:
        #         # If anything goes wrong moving the files we still need to
        #         # delete the temp working directory
        #         m.msg(
        #             "removing temp working directory: '{0}'".format(
        #                 tmp_working_dir
        #             )
        #         )
        #         # This will get called irrespective of a clean or error exit
        #         shutil.rmtree(tmp_working_dir)
        #         raise

        # Here we make sure we clean up after ourselves
        m.msg("removing temp working directory: '{0}'".format(tmp_working_dir))
        # shutil.rmtree(tmp_working_dir)

    m.prefix = ''
    m.msg("*** END ***")


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def init_cmd_args():
    """
    Initialise the command line arguments and return the parsed arguments

    Returns
    -------
    args : :obj:`argparse.Namespace`
        The argparse namespace object
    """
    parser = argparse.ArgumentParser(
        description="build a UKBB mapping database")

    parser.add_argument(
        'ukbb_mapping_file', type=str,
        help="The path to the UKBB mapping spreadsheet"
    )
    parser.add_argument('opcs3_file', type=str,
                        help="The path to the OPCS3 lookup file")
    parser.add_argument(
        'opcs4_file', type=str,
        help="The path to the OPCS4 lookup file. This is only required if"
        " UKBB is being used as the source of the mapping files. If NHS "
        "digital data is being used then this file can be omitted and is "
        "ignored if included"
    )
    parser.add_argument(
        '-c', '--config-file', type=str,
        help="An alternative path to the config file"
    )
    parser.add_argument(
        '-t', '--tmp-dir', type=str,
        help="an alternative location to write temp files"
    )
    parser.add_argument(
        '-k', '--keep-files', type=str,
        help="A location to store the files used to build the"
        " database, if not provided then the intermediate "
        "files will be deleted"
    )
    parser.add_argument(
        '-n', '--nhs-digital', type=str,
        help="The path to the directory containing the NHS digital files, if"
        " this is suppled then the lookups and mappings are taken from these"
        " in preference to the UKBB mappings, with only selected mappings "
        "that are not in NHS digital taken from the UKBB mappings"
    )
    parser.add_argument(
        '-i', '--include-file', type=str,
        help="The location of the include file. This is a user defined "
        "mapping file with 6 columns: source_cui,source_coding_system,target"
        "_cui,target_coding_system,bidirectional,overwrite. bidirectional"
        " should be 1 if the mapping can be appllied in both directions 0"
        " if not. overwrite is a boolean and indicates if this mapping should"
        " supplement excising mappings between the CUIs or overwrite them. "
        "Empty values in the boolean columns are treated as false. The "
        "coding systems must be one of opcs3,opcs4,icd9,icd10,read2,"
        "ctv3,umls,snomed. The file should be tab-delimied and have a"
        " header"
    )
    parser.add_argument(
        '-e', '--exclude-file', type=str,
        help="The location of the exclude file. This is a user defined "
        "mappings to exclude from existing mappings. These have preference "
        "over any included mappings. The columns are source_cui,"
        "source_coding_system,target_cui,target_coding_system,bidirectional."
        " bidirectional should be 1 if the exclusion can be applied in both"
        " directions 0 if not. Empty values in the boolean columns are "
        "treated as false. The coding systems must be one of opcs3,opcs4,"
        "icd9,icd10,read2,ctv3,umls,snomed.  The file should be tab-delimited"
        " and have a header"
    )
    parser.add_argument(
        '-u', '--ukbb-code-pool', type=str,
        help="A listing of all the clinical codes that are present in the "
        "UKBB clinical datafiles. This should be tab-delimited and have a"
        " header: cui,coding_system. The coding_system should be one of "
        "opcs3,opcs4,icd9,icd10,read2,ctv3 Whilst this file is optional it is"
        " a good idea to include it if the mapper is being used to build the"
        " UKBB into a relational database just in case there are codes in the"
        " clinical data that do not appear in the mapping files. These are"
        " probably errors but they will be annotated as orphan codes and may"
        " be useful for debugging"
    )

    parser.add_argument('-v', '--verbose',  action="store_true",
                        help="give more output")
    return parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_cmd_args(parser):
    """
    Initialise the command line arguments and return the parsed arguments

    Parameters
    ----------
    parser : :obj:`argparse.ArgumentParser`
        The argparseparser object with arguments defined

    Returns
    -------
    args : :obj:`argparse.Namespace`
        The argparse namespace object
    """
    args = parser.parse_args()

    # Go through all the directory arguments. If they have been defined
    # make sure they are expressed as a full path and are actually real
    # directories
    for i in ['tmp_dir', 'keep_files', 'nhs_digital']:
        dir_name = getattr(args, i)
        # First make sure that the directory exists (if it is defined).
        # We want to error out early not at the end after we have built it
        if dir_name is not None:
            # Make sure it is a full path and not relative
            dir_name = os.path.expanduser(dir_name)

            # Make sure it is actually a directory
            if not os.path.isdir(dir_name):
                raise NotADirectoryError(
                    "{0}  does not exist: '{1}'".format(
                        i,
                        dir_name
                    )
                )

    # required files
    for i in ['ukbb_mapping_file', 'opcs3_file']:
        setattr(args, i, os.path.expanduser(getattr(args, i)))
        open(getattr(args, i)).close()

    # optional files
    for i in ['opcs4_file', 'include_file', 'exclude_file', 'ukbb_code_pool']:
        if getattr(args, i) is not None:
            setattr(args, i, os.path.expanduser(getattr(args, i)))
            open(getattr(args, i)).close()

    if args.nhs_digital is not None and args.opcs4_file is not None:
        warnings.warn("ignoring OPCS4 file as --nhs-digital is defined")
    elif args.nhs_digital is None and args.opcs4_file is None:
        raise ValueError(
            "require an OPCS4 lookup file as no --nhs-digital is defined"
        )
    return args


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def initialise(config_file=None, ukbb_code_pool_path=None,
               user_includes_path=None, user_excludes_path=None):
    """
    Initialise the config file and the UKBB mapping database connection

    Parameters
    ----------
    config_file : str or NoneType, optional, default: NoneType
        The path to the config file. If `NoneType` then the `UKBB_CONFIG`
        environment variable is checked for the config file path. If that is
        not set then finally the default location of `~/.ukbb.cnf` is checked
        if the file is not there then we error out

    Returns
    -------
    config_obj : :obj: `configparser`
        A configparser object that provides access to the confg file
    ukbb_map_sm : :obj:`sqlalchemy.Sessionmaker`
        Use to generate sessions to query/write to the mapping database
    umls_sm : :obj:`sqlalchemy.Sessionmaker` or NoneType
        A session manager to interact with the UMLS database. The UMLS is
        optional so if not defined in the config file then this will be
        NoneType
    include_map : set
        User defined mapping inclusions - these override any computationally
        defined mappings
    exclude_map : set
        User defined mapping exclusions - these are removed from any
        computationally defined mappings

    """
    # attempt to loacate the path to the config file, if successful then make
    # sure the permissions are set correcly (only user readable). Finally, if
    # all ok then read in the config file
    config_obj = config.initialise_config(config_file)

    # Get the connection to the UKBB mapping database that we want to build
    # eventually I want this to error out if it exists - as we should be
    # building from scratch
    ukbb_map_sm = config.get_ukbb_mapping_connection(
        config_obj, must_exist=True
    )

    # Now we load up the mapping includes and mapping excludes files
    include_cache, exclude_cache = load_manual_maps(
        user_includes_path=user_includes_path,
        user_excludes_path=user_excludes_path
    )

    # if the code pool is defined
    if ukbb_code_pool_path is not None:
        ukbb_code_pool = load_ukbb_code_pool(ukbb_code_pool_path)

    return config_obj, ukbb_map_sm, None, ukbb_code_pool,\
        include_cache, exclude_cache


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def load_manual_maps(user_includes_path=None, user_excludes_path=None):
    """
    Loads the manual mapping files into memory. There are two types of manual
    mappings, include mappings and exclude mappings. There are a set of global
    include/exclude files in ukbb_tools that are loaded and the user can
    supplement these with their own.

    Parameters
    ----------
    user_includes_path : str or NoneType, optional, default: NoneType
        The path to the user defined includes file
    user_excludes_path : str or NoneType, optional, default: NoneType
        The path to the user defined excludes file

    Returns
    -------
    include_cache : set
        A set of user defined mappings to be included in the database. If any
        manual inclusions are defined then the set will contain tuples with
        nested tuples of coding system, cui for the source and target of the
        mapping at 0, 1 and the map type (M) at 2 with the overwrite boolean
        at 3
    exclude_cache : dict
        A set of user defined exclusions to be excluded from the database. The
        keys of the dict are a tuple of (source coding system, source cui)
        and the values are a set of tuples of (target coding system, target
        cui)
    """
    # TODO: warning, untested
    global_excludes_path = pkg_resources.resource_filename(
        __name__, 'data/exclude_mappings.txt'
    )
    global_includes_path = pkg_resources.resource_filename(
        __name__, 'data/include_mappings.txt'
    )

    include_cache = mr.load_includes_file(
        global_includes_path, delimiter="\t"
    )
    exclude_cache = mr.load_excludes_file(
        global_excludes_path, delimiter="\t"
    )

    if user_includes_path is not None:
        include_cache = mr.load_includes_file(
            user_includes_path, cache=include_cache, delimiter="\t"
        )

    if user_excludes_path is not None:
        exclude_cache = mr.load_excludes_file(
            user_excludes_path, cache=exclude_cache, delimiter="\t"
        )

    return include_cache, exclude_cache


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def load_ukbb_code_pool(ukbb_code_pool_path):
    """
    Load the UKBB code pool file into memory

    Parameters
    ----------
    ukbb_code_pool_path : str
        the path to the UKBB code pool file. This is a set of all the clinical
        codes that exist in the UKBB clinical data files. It has two columns
        cui, coding_system

    Returns
    -------
    ukbb_code_pool : set
        A set of tuples with each tuple having with the coding_system name
        at [0] and the cui at [1]
    """
    ukbb_code_pool = set([])
    with mr.UkbbCodePoolReader(ukbb_code_pool_path, delimiter="\t") as infile:
        for row in infile:
            ukbb_code_pool.add(
                (
                    row[col.UKBB_CODE_POOL_CODING_SYSTEM.name],
                    row[col.UKBB_CODE_POOL_CUI.name]
                )
            )
    return ukbb_code_pool


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def move_files(source_dir, dest_dir, file_glob="*.txt.gz"):
    """
    Relocate all the files in the source dir to the dest dir

    Parameters
    ----------
    source_dir : str
        The destination directory to move the files to
    dest_dir : str
        The destination directory to move the files to
    file_glob : str, optional, default `*.txt.gz`
        The file name pattern to glob in the source_dir
    """
    # Loop through the files
    for file_path in glob.glob(os.path.join(source_dir, file_glob)):
        file_name = os.path.basename(file_path)
        dest_file = os.path.join(dest_dir, file_name)
        shutil.move(file_path, dest_file)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def build_mapping_db(mapper_sessm, ukbb_mapping_dir, opcs3_file, include_cache,
                     exclude_cache, ukbb_code_pool=None,
                     umls_sessm=None, nhs_digital_dir=None, opcs4_file=None,
                     verbose=False, commit_every=100000):
    """
    Initiate the building of the mapping database, use this is you want to use
    via the API.

    Parameters
    ----------
    mapper_sessm : :obj:`sqlalchemy.Sessionmaker`
        A sessionmaker to create session objects for interaction with the
        mapping database that is being created
    ukbb_mapping_dir : str
        The path to the directory with all the UKBB mapping files in them.
        These are the compressed files that have been extracted from the
        Excel workbook
    opcs3_file : str
        The path to the OPCS3 lookup file that has been downloaded from the
        UKBB website (codings 259)
    include_cache : set
        A set of user defined mappings to be included in the database. If any
        manual inclusions are defined then the set will contain tuples with
        nested tuples of coding system, cui for the source and target of the
        mapping at 0, 1 and the map type (M) at 2 with the overwrite boolean
        at 3
    exclude_cache : dict
        A set of user defined exclusions to be excluded from the database. The
        keys of the dict are a tuple of (source coding system, source cui)
        and the values are a set of tuples of (target coding system, target
        cui)
    ukbb_code_pool : set or NoneType, optional, default: NoneType
        A set of tuples with each tuple having with the coding_system name
        at [0] and the cui at [1]
    umls_sessm : :obj:`sqlalchemy.Sessionmaker` or NoneType, optional, default: NoneType
        A sessionmaker to create session objects for interaction with the
        UMLS database. If the UMLS is not being used (not in the config file)
        then this will be `NoneType`
    nhs_digital_dir : str or NoneType, optional, default: NoneType
        The path to the directory that contains all the NHS digital mapping
        files that have been extracted from the mapping workbench access
        database
    opcs4_file : str or NoneType, optional, default: NoneType
        The path to the OPCS3 lookup file that has been downloaded from the
        UKBB website (codings_240)
    verbose : bool, optional, default: False
        document the building of the UKBB mapping database
    commit_every : int, optional, default: 100000
        The number of records to buffer before inserting into the database.
    """
    m = progress.Msg(prefix=MSG_PREFIX, file=MSG_OUT)

    # First do some argument checking
    # Make sure the code pool is defined even if an empty set
    if ukbb_code_pool is None:
        ukbb_code_pool = set()

    # Make sure all the tables have been created in the database prior to
    # starting the build
    engine = mapper_sessm().get_bind()
    orm.Base.metadata.create_all(engine)

    # # Build the hard coded lookup tables
    # build_coding_systems(mapper_sessm)
    # build_status_code_table(mapper_sessm)
    # build_code_type_table(mapper_sessm)

    cui_collection = None
    self_mappings = None
    orphan_self_mappings = None
    if nhs_digital_dir is None:
        m.msg("building codes using only UK BioBank data")
        if opcs4_file is None:
            raise FileNotFoundError(
                "OPCS4 file must be defined if no NHS digital data is being"
                " used"
            )
        # cui_collection, self_mappings = _build_ukbb_cui_collection(
        #     ukbb_mapping_dir,
        #     opcs3_file,
        #     opcs4_file,
        #     verbose=verbose
        # )
    else:
        # We will build using the NHS digital data and infill with UKBB
        # mappings where necessary
        m.msg("building codes using NHS digital data")

        # build the term tables: concepts, terms and the code_status and
        # code_type tables
        # cui_collection, self_mappings = _build_nhs_cui_collection(
        #     nhs_digital_dir,
        #     ukbb_mapping_dir,
        #     opcs3_file,
        #     verbose=verbose
        # )

    m.msg("adding orphan codes...")
    # orphan_self_mappings = _add_code_pool_to_cui_collection(
    #     cui_collection, ukbb_code_pool
    # )
    # _build_concepts_table(mapper_sessm, cui_collection, verbose=verbose)
    # _build_terms_table(mapper_sessm, cui_collection, verbose=verbose)

    # Needs to be in place before the mappings are inserted
    # build_mapping_types_table(mapper_sessm)

    # First add the manual includes into the mapping tables. This returns a
    # tuple of included concept_ids (ints not cuis) and coding system ids.
    # as these will not be included again if the user has inadvertently
    # specified an existing mapping as a manual mapping
    source_overwrites = _process_manual_include_maps(
        mapper_sessm, include_cache, exclude_cache
    )

    # Now we move on to building the mapping table, as with the codes we
    # separate depending on the data source
    if nhs_digital_dir is None:
        m.msg("building mappings using only UK BioBank data")
        _build_ukbb_mappings(
            mapper_sessm,
            ukbb_mapping_dir,
            self_mappings,
            orphan_self_mappings,
            exclude_cache,
            source_overwrites
        )
    else:
        # We will build using the NHS digital data and infill with UKBB
        # mappings where necessary
        m.msg("building mappings using NHS digital data")


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def build_coding_systems(sessm):
    """
    Build the coding_systems table. This is a static table build from variables
    within the constants

    Parameters
    ----------
    sessm : `sqlalchemy.Sessionmaker`
        A session maker for the mapper database to constrct sessions from
    """
    coding_sytems = [
        con.OPCS4_CODE_TYPE_NAME,
        con.OPCS3_CODE_TYPE_NAME,
        con.SNOMED_CODE_TYPE_NAME,
        con.READ2_CODE_TYPE_NAME,
        con.CTV3_CODE_TYPE_NAME,
        con.ICD9_CODE_TYPE_NAME,
        con.ICD10_CODE_TYPE_NAME,
        con.UMLS_CODE_TYPE_NAME
    ]
    session = sessm()
    for i in coding_sytems:
        session.add(orm.CodingSystem(coding_system_name=i))
    session.commit()
    session.close()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def build_status_code_table(sessm):
    """
    Build the status code table. This is a static table built from constants
    defined in this module

    Parameters
    ----------
    sessm : `sqlalchemy.Sessionmaker`
        A session maker for the mapper database to constrct sessions from
    """
    session = sessm()
    for status_code, status_code_desc in STATUS_CODES.items():
        session.add(
            orm.StatusCode(
                status_code=status_code,
                status_code_desc=status_code_desc
            )
        )
    session.commit()
    session.close()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def build_code_type_table(sessm):
    """
    Build the coding_type table. This is a static table build from variables
    defined in this module

    Parameters
    ----------
    sessm : `sqlalchemy.Sessionmaker`
        A session maker for the mapper database to constrct sessions from
    """
    session = sessm()
    for code_type, code_type_desc in CODE_TYPES.items():
        session.add(
            orm.CodeType(
                code_type=code_type,
                code_type_desc=code_type_desc
            )
        )
    session.commit()
    session.close()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _build_ukbb_cui_collection(ukbb_mapping_dir, opcs3_file, opcs4_file,
                               verbose=False):
    """
    Initiate the building of the lookup tables containing all the CUIs and
    their associated TUIs

    Parameters
    ----------
    sessm : :obj:`sqlalchemy.Sessionmaker`
        A sessionmaker to create session objects for interaction with the
        mapping database that is being created
    ukbb_mapping_dir : str
        The path to the directory with all the UKBB mapping files in them.
        These are the compressed files that have been extracted from the
        Excel workbook
    opcs3_file : str
        The path to the OPCS3 lookup file that has been downloaded from the
        UKBB website (codings 259)
    opcs4_file : str
        The path to the OPCS3 lookup file that has been downloaded from the
        UKBB website (codings 240)
    ukbb_code_pool : set
        A set of tuples with each tuple having with the coding_system name
        at [0] and the cui at [1]. This may be an empty set
    verbose : bool, optional, default: False
        document the building of the UKBB mapping database

    Returns
    -------
    cui_collection : :obj:`CuiCollection`
        A collection of all cuis that have been extracted from the lookup files
    self_mappings : dict of list or tuple
        The mappings between self terms these are also inserted into the
        mappings table. The keys of the dict are coding system names the
        values are lists of tuples where the tuples have the length of 2 and
        contain file_def.CuiCode objects of self mappings for all cui:tui
        pairs
    """
    m = progress.Msg(verbose=verbose, file=MSG_OUT, prefix=MSG_PREFIX)
    m.msg("reading all clinical codes...")
    code_files = [
        (con.OPCS3_CODE_TYPE_NAME, mr.Opcs3Reader, opcs3_file),
        (con.OPCS4_CODE_TYPE_NAME, mr.Opcs4UkbbReader, opcs4_file),
        (
            con.ICD9_CODE_TYPE_NAME,
            mr.Icd9Reader,
            _get_reader_file_name(ukbb_mapping_dir, mr.Icd9Reader.BASENAME)
        ),
        (
            con.ICD10_CODE_TYPE_NAME,
            mr.Icd10UkbbReader,
            _get_reader_file_name(
                ukbb_mapping_dir, mr.Icd10UkbbReader.BASENAME
            )
        ),
        (
            con.READ2_CODE_TYPE_NAME,
            mr.RctUkbbReader,
            _get_reader_file_name(ukbb_mapping_dir, mr.RctUkbbReader.BASENAME)
        ),
        (
            con.CTV3_CODE_TYPE_NAME,
            mr.Ctv3UkbbReader,
            _get_reader_file_name(ukbb_mapping_dir, mr.Ctv3UkbbReader.BASENAME)
        )
    ]
    cui_collection, self_mappings = _build_cui_collection(
        code_files, verbose=verbose
    )
    return cui_collection, self_mappings


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _build_nhs_cui_collection(nhs_digital_dir, ukbb_mapping_dir, opcs3_file,
                              verbose=False):
    """
    Build the tables responsible for holding the concept codes CUIs and the
    TUIs and descriptions based primarily on NHS data with infill from UKBB
    files

    Parameters
    ----------
    nhs_digital_dir : str
        The path to the NHS digital data files that have been parsed out of
        data migration workbench MS Access files
    ukbb_mapping_dir : str
        The path to the directory with all the UKBB mapping files in them.
        These are the compressed files that have been extracted from the
        Excel workbook
    opcs3_file : str
        The path to the OPCS3 lookup file that has been downloaded from the
        UKBB website (codings 259)
    verbose : bool, optional, default: False
        document the building of the UKBB mapping database

    Returns
    -------
    cui_collection : :obj:`CuiCollection`
        A collection of all cuis that have been extracted from the lookup files
    self_mappings : dict of list or tuple
        The mappings between self terms these are also inserted into the
        mappings table. The keys of the dict are coding system names the
        values are lists of tuples where the tuples have the length of 2 and
        contain file_def.CuiCode objects of self mappings for all cui:tui
        pairs
    """
    m = progress.Msg(verbose=verbose, file=MSG_OUT, prefix=MSG_PREFIX)
    m.msg("reading all clinical codes...")
    code_files = [
        (con.OPCS3_CODE_TYPE_NAME, mr.Opcs3Reader, opcs3_file),
        (
            con.ICD9_CODE_TYPE_NAME,
            mr.Icd9Reader,
            _get_reader_file_name(ukbb_mapping_dir, mr.Icd9Reader.BASENAME)
        ),
        (
            con.SNOMED_CODE_TYPE_NAME,
            mr.SctReader,
            _get_reader_file_name(nhs_digital_dir, mr.SctReader.BASENAME)
        ),
        (
            con.CTV3_CODE_TYPE_NAME,
            mr.Ctv3Reader,
            _get_reader_file_name(nhs_digital_dir, mr.Ctv3Reader.BASENAME)
        ),
        (
            con.READ2_CODE_TYPE_NAME,
            mr.RctReader,
            _get_reader_file_name(nhs_digital_dir, mr.RctReader.BASENAME)
        ),
        (
            con.ICD10_CODE_TYPE_NAME,
            mr.Icd10Reader,
            _get_reader_file_name(nhs_digital_dir, mr.Icd10Reader.BASENAME)
        ),
        (
            con.OPCS4_CODE_TYPE_NAME,
            mr.Opcs4Reader,
            _get_reader_file_name(nhs_digital_dir, mr.Opcs4Reader.BASENAME)
        )
    ]
    cui_collection, self_mappings = _build_cui_collection(
        code_files, verbose=verbose
    )
    return cui_collection, self_mappings


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _get_reader_file_name(nhs_dir, basename):
    """
    """
    return os.path.join(nhs_dir, "{0}.txt.gz".format(basename))


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _build_cui_collection(code_files, verbose=False):
    """
    Read in all the code files and place them in a CuiCollection object.

    """
    m = progress.Msg(verbose=verbose, file=MSG_OUT, prefix=MSG_PREFIX)
    cui_collection = CuiCollection()
    self_mappings = {}
    for type_name, ReaderClass, infile in code_files:
        m.msg("processing {0} codes...".format(type_name))
        self_mappings[type_name] = _parse_codes(
            cui_collection,
            ReaderClass,
            infile,
            delimiter="\t",
            quoting=csv.QUOTE_NONE
        )
    m.msg("read in {0} codes".format(cui_collection.length))
    longest_term = cui_collection.get_longest_term()
    longest_cui = cui_collection.get_longest_cui()
    m.msg("longest cui: {0}".format(len(longest_cui.code)))
    m.msg("longest term: {0}".format(len(longest_term.term)))
    return cui_collection, self_mappings


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _add_code_pool_to_cui_collection(cui_collection, ukbb_code_pool,
                                     verbose=False):
    """
    Loop through any entries in the ukbb_code_pool and add them to the
    `CuiCollection` object if not already present. Anything from the code pool
    that is added to the CuiCollection is added with a term:
    "orphan cui: <CODING_SYSTEM>.<CUI>" and a MD5 tui is generated from this

    Parameters
    ----------
    cui_collection : :obj:`CuiCollection`
        An existing collection of `file_defs.CuiCode` objects to add orphan
        codes to
    ukbb_code_pool : set
        A set of tuples with each tuple having with the coding_system name
        at [0] and the cui at [1]. This may be an empty set
    """
    self_map = {}
    for coding_system, cui in ukbb_code_pool:
        try:
            cui_collection.get(cui, coding_system)
        except KeyError:
            # Not present so we will add an orphan ID
            term = "orphan cui: {0}.{1}".format(coding_system, cui)
            tui = hashlib.md5(term.encode()).hexdigest()

            orphan_cui = file_defs.CuiCode(
                cui,
                tui,
                term,
                coding_system,
                code_type='O',
                code_status='P'
            )

            cui_collection.add(set([orphan_cui]))

            try:
                self_map[coding_system].append((orphan_cui, orphan_cui))
            except KeyError:
                self_map[coding_system] = [(orphan_cui, orphan_cui)]
    return self_map


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _parse_codes(code_collection, ReaderClass, infile, **kwargs):
    """
    """
    self_maps = []
    with ReaderClass(infile, **kwargs) as reader:
        for row in reader:
            codes = ReaderClass.get_clinical_code(row)
            self_maps.extend(get_self_codes(codes))
            code_collection.add(codes)
    return self_maps


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_self_codes(codes):
    """
    """
    return list(product(codes, codes))


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _build_concepts_table(sessm, code_collection, commit_every=100000,
                          verbose=False):
    """
    """
    cache = common.Cache(orm.Concept, orm.Concept.concept_id.name)
    coding_systems = get_coding_systems(sessm)
    # pp.pprint(coding_systems)
    session = sessm()
    idx = 1
    prog = progress.RemainingProgress(
        code_collection.length,
        verbose=verbose,
        default_msg="inserting data into concepts"
    )
    for code in prog.progress(code_collection.yield_unique_cui()):
        coding_system_id = coding_systems[code.coding_system]
        cache.add(
            {
                orm.Concept.concept_id.name: None,
                orm.Concept.coding_system_id.name: coding_system_id,
                orm.Concept.cui.name: code.code,
                orm.Concept.display_term.name: code.term
            },
            (coding_system_id, code.code)
        )
        if idx == commit_every:
            session.bulk_insert_mappings(cache.table, cache.flush())
            session.commit()
        # print(code)
    session.bulk_insert_mappings(cache.table, cache.flush())
    session.commit()
    session.close()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _build_terms_table(sessm, cui_collection, commit_every=100000,
                       verbose=False):
    """
    """
    idx = 1
    concepts = get_concepts(sessm)
    coding_systems = get_coding_systems(sessm)
    status_codes = get_status_codes(sessm)
    code_types = get_code_types(sessm)
    session = sessm()
    # pp.pprint(code_types)
    # pp.pprint(status_codes)
    # pp.pprint(concepts)
    prog = progress.RemainingProgress(
        cui_collection.length,
        verbose=verbose,
        default_msg="building terms table"
    )
    idx = 1
    row_buffer = []
    for cui in prog.progress(cui_collection.yield_all_cui()):
        coding_system_id = coding_systems[cui.coding_system]
        concept_id = concepts[(coding_system_id, cui.code)]

        status_code = _get_cui_attr(cui, status_codes, 'code_status')
        code_type = _get_cui_attr(cui, code_types, 'code_type')

        row_buffer.append(
            {
                orm.Term.term_id.name: idx,
                orm.Term.concept_id.name: concept_id,
                orm.Term.status_code_id.name: status_code,
                orm.Term.code_type_id.name: code_type,
                orm.Term.tui.name: cui.tui,
                orm.Term.term.name: cui.term
            }
        )
        if idx % commit_every == 0:
            session.bulk_insert_mappings(orm.Term, row_buffer)
            row_buffer = []
            session.commit()

        idx += 1

    session.bulk_insert_mappings(orm.Term, row_buffer)
    row_buffer = []
    session.commit()
    session.close()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _get_cui_attr(cui, lookup, attr_name):
    """
    """
    try:
        return lookup[str(getattr(cui, attr_name))]
    except KeyError:
        return lookup['U']


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_status_codes(sessm):
    """
    """
    session = sessm()
    status_codes = {}
    sql = session.query(orm.StatusCode)
    for row in sql:
        status_codes[row.status_code] = row.status_code_id

    session.close()
    return status_codes


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_code_types(sessm):
    """
    """
    session = sessm()
    code_types = {}
    sql = session.query(orm.CodeType)
    for row in sql:
        code_types[row.code_type] = row.code_type_id

    session.close()
    return code_types


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_concepts(sessm):
    """
    create a lookup cache of all the concepts in the database to allow
    the translation from coding system id/cui to the concept ID in the
    database.

    Parameters
    ----------
    sessm : :obj:`sqlalchemy.Sessionmaker`
        A sessionmaker to create session objects for interaction with the
        mapping database that is being created

    Returns
    -------
    concepts : dict
        The keys of the dict are tuples of coding_system_ids (int), cui name
        (str) and the values are database concept IDs (int)
    """
    session = sessm()
    concepts = {}
    sql = session.query(orm.Concept)
    for row in sql:
        concepts[(row.coding_system_id, row.cui)] = row.concept_id

    session.close()
    return concepts


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_coding_systems(sessm):
    """
    create a lookup cache of all the coding systems in the database to allow
    the translation from coding system name to the coding system ID in the
    database.

    Parameters
    ----------
    sessm : :obj:`sqlalchemy.Sessionmaker`
        A sessionmaker to create session objects for interaction with the
        mapping database that is being created

    Returns
    -------
    coding_systems : dict
        The keys of the dict are coding system names and the values are coding
        system IDs
    """
    session = sessm()
    coding_systems = {}
    sql = session.query(orm.CodingSystem)
    for row in sql:
        coding_systems[row.coding_system_name] = row.coding_system_id

    session.close()
    return coding_systems


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def build_mapping_types_table(sessm):
    """
    Build the mapping_types table. This is a static table built from variables
    defined in this module

    Parameters
    ----------
    sessm : `sqlalchemy.Sessionmaker`
        A session maker for the mapper database to constrct sessions from
    """
    session = sessm()
    for mapping_type, mapping_type_desc in MAPPING_TYPES.items():
        session.add(
            orm.MappingType(
                mapping_type=mapping_type,
                mapping_type_desc=mapping_type_desc
            )
        )
    session.commit()
    session.close()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_mapping_types(sessm):
    """
    create a lookup cache of all the mapping types in the database to allow
    the translation from mapping_type name to the mapping type ID in the
    database.

    Parameters
    ----------
    sessm : :obj:`sqlalchemy.Sessionmaker`
        A sessionmaker to create session objects for interaction with the
        mapping database that is being created

    Returns
    -------
    mapping_types : dict
        The keys of the dict are mapping type names and the values are mapping
        type IDs
    """
    session = sessm()
    mapping_types = {}
    sql = session.query(orm.MappingType)
    for row in sql:
        mapping_types[row.mapping_type] = row.mapping_type_id

    session.close()
    return mapping_types


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _process_manual_include_maps(sessm, include_cache, exclude_cache,
                                 commit_every=100000):
    """
    Deal with adding manual mapping inclusions into the database

    Parameters
    ----------
    sessm : :obj:`sqlalchemy.Sessionmaker`
        A sessionmaker to create session objects for interaction with the
        mapping database that is being created
    include_cache : set
        A set of user defined mappings to be included in the database. If any
        manual inclusions are defined then the set will contain tuples with
        nested tuples of coding system, cui for the source and target of the
        mapping at 0, 1 and the map type (M) at 2 with the overwrite boolean
        at 3
    exclude_cache : dict
        A set of user defined exclusions to be excluded from the database. The
        keys of the dict are a tuple of (source coding system, source cui)
        and the values are a set of tuples of (target coding system, target
        cui)
    commit_every : int, optional, default: 100000
        The number of records to buffer before inserting into the database.

    Returns
    -------
    source_overwrites : set of tuple
        Source mappings that if defined in the various mapping files will not
        be added to the database as they are "overwritten" by a user defined
        mapping. Each tuple is (source coding system ID (int), source concept
        id (int))
    """
    # TODO: warning untested
    # Will hold tuples of (coding system ID (int), concept ID (int)) that
    # represent source manual mappings that we want to overwrite all defined
    # mappings of that source
    source_overwrite = set()

    # No point needlessly querying the database of none are set
    if len(include_cache) == 0:
        return source_overwrite

    concepts = get_concepts(sessm)
    coding_systems = get_coding_systems(sessm)
    mapping_types = get_mapping_types(sessm)

    session = sessm()
    mapping_cache = []
    for source, target, map_type, overwrite in include_cache:
        try:
            if target in exclude_cache[source]:
                # The manual include is also an exclude and excludes take
                # priority
                continue
            # Here the source is present in the excludes but the target is not
            # so we add it for the same reasons as detailed below
            exclude_cache[source].add(target)
        except KeyError:
            # The include is not also in the excludes. So we will add it. That
            # way if the manual mapping is also a defined mapping from the
            # data files, it does not get included twice
            exclude_cache[source] = set([target])

        source_coding_system = coding_systems[source[0]]
        source_cui = concepts[(source_coding_system, source[1])]
        target_coding_system = coding_systems[target[0]]
        target_cui = concepts[(target_coding_system, target[1])]
        mapping_type_id = mapping_types[map_type]

        # If we are overwriting anything from the source then make sure we
        # add the IDs to the source_overwrite so we can carry through to
        # when we build the mapping tables from the data files
        if overwrite is True:
            source_overwrite.add((source_coding_system, source_cui))

        # Now we can add to the mapping cache
        mapping_cache.append(
            {
                orm.Mapping.s_concept_id.name: source_cui,
                orm.Mapping.s_coding_system_id.name: source_coding_system,
                orm.Mapping.t_concept_id.name: target_cui,
                orm.Mapping.t_coding_system_id.name: target_coding_system,
                orm.Mapping.mapping_type_id.name: mapping_type_id
            }
        )

        if len(mapping_cache) % commit_every == 0:
            session.bulk_insert_mappings(orm.Mapping, mapping_cache)
            session.commit()
            mapping_cache = []

    # final commit of what ever is left in the cache
    if len(mapping_cache) > 0:
        session.bulk_insert_mappings(orm.Mapping, mapping_cache)
        session.commit()

    session.close()
    return source_overwrite


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _build_ukbb_mappings(sessm, ukbb_mapping_dir, self_mappings,
                         orphan_self_mappings, exclude_cache,
                         source_overwrites, verbose=False,
                         commit_every=100000):
    """
    Insert all the mappings defined in the UKBB mapping files into the
    mappings table

    Parameters
    ----------
    sessm : :obj:`sqlalchemy.Sessionmaker`
        A sessionmaker to create session objects for interaction with the
        mapping database that is being created
    ukbb_mapping_dir : str
        The path to the directory with all the UKBB mapping files in them.
        These are the compressed files that have been extracted from the
        Excel workbook
    self_mappings : dict of list or tuple
        The mappings between self terms these are also inserted into the
        mappings table. The keys of the dict are coding system names the
        values are lists of tuples where the tuples have the length of 2 and
        contain file_def.CuiCode objects of self mappings for all cui:tui
        pairs
    orphan_mappings : dict of list or tuple
        The mappings between orphans terms these are self mappings between
        orphan terms and are also inserted into the mappings table. The keys
        of the dict are coding system names the values are lists of tuples
        where the tuples have the length of 2 and contain file_def.CuiCode
        objects of orphan term
    exclude_cache : dict
        A set of user defined exclusions to be excluded from the database. The
        keys of the dict are a tuple of (source coding system, source cui)
        and the values are a set of tuples of (target coding system, target
        cui)
    source_overwrites : set of tuple
        Source mappings that if defined in the various mapping files will not
        be added to the database as they are "overwritten" by a user defined
        mapping. Each tuple is (source coding system ID (int), source concept
        id (int))
    verbose : bool, optional, default: False
        document the building of the UKBB mapping database
    commit_every : int, optional, default: 100000
        The number of records to buffer before inserting into the database.
    """
    concepts = get_concepts(sessm)
    coding_systems = get_coding_systems(sessm)
    mapping_types = get_mapping_types(sessm)

    mapping_files = [
        # (
        #     mr.Icd9Icd10MapReader,
        #     _get_reader_file_name(
        #         ukbb_mapping_dir,
        #         mr.Icd9Icd10MapReader.BASENAME
        #     )
        # ),
        # (
        #     mr.Ctv3Icd10MapReader,
        #     _get_reader_file_name(
        #         ukbb_mapping_dir,
        #         mr.Ctv3Icd10MapReader.BASENAME
        #     )
        # ),
        # (
        #     mr.Ctv3Icd9MapReader,
        #     _get_reader_file_name(
        #         ukbb_mapping_dir,
        #         mr.Ctv3Icd9MapReader.BASENAME
        #     )
        # ),
        (
            mr.Ctv3Opcs4MapReader,
            _get_reader_file_name(
                ukbb_mapping_dir,
                mr.Ctv3Opcs4MapReader.BASENAME
            )
        )
    ]

    for ReaderClass, file_name in mapping_files:
        _insert_mappings(
            sessm,
            file_name,
            ReaderClass,
            exclude_cache,
            source_overwrites,
            concepts,
            coding_systems,
            mapping_types,
            commit_every=commit_every,
            verbose=verbose
        )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _build_nhs_mappings(sessm, nhs_digital_dir, ukbb_mapping_dir,
                        self_mappings, orphan_self_mappings, include_cache,
                        exclude_cache):
    """
    """
    raise NotImplementedError("need to implement")


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _insert_mappings(sessm, file_name, ReaderClass, exclude_cache,
                     source_overwrites, concepts, coding_systems,
                     mapping_types, verbose=False, commit_every=100000,
                     **kwargs):
    """
    Generic mapping insert function
    """
    session = sessm()
    mapping_buffer = []
    keyerrors = 0

    with ReaderClass(file_name, **kwargs) as infile:
        prog = progress.RateProgress(
            verbose=verbose,
            file=MSG_OUT,
            default_msg="inserting {0}...".format(ReaderClass.BASENAME)
        )
        for row in prog.progress(infile):
            for source, target, map_type in ReaderClass.get_mappings(row):
                # Skip any NoneType cuis
                if source[1] is None or target[1] is None:
                    continue

                try:
                    # Now look to see if the mapping should be excluded
                    if target in exclude_cache[source]:
                        continue
                except KeyError:
                    pass

                source_coding_system = coding_systems[source[0]]
                target_coding_system = coding_systems[target[0]]
                mapping_type_id = mapping_types[map_type]

                try:
                    source_cui = concepts[(source_coding_system, source[1])]
                    target_cui = concepts[(target_coding_system, target[1])]
                except KeyError:
                    keyerrors += 1
                    continue

                # Have we overwritten it with a manual mapping
                if (source_coding_system, source_cui) in source_overwrites:
                    continue

                # If we get here then we are ok to insert the mapping
                mapping_buffer.append(
                    {
                        orm.Mapping.s_concept_id.name: source_cui,
                        orm.Mapping.s_coding_system_id.name:
                        source_coding_system,
                        orm.Mapping.t_concept_id.name: target_cui,
                        orm.Mapping.t_coding_system_id.name:
                        target_coding_system,
                        orm.Mapping.mapping_type_id.name: mapping_type_id
                    }
                )

                if len(mapping_buffer) % commit_every == 0:
                    session.bulk_insert_mappings(orm.Mapping, mapping_buffer)
                    session.commit()
                    mapping_buffer = []

        # final commit of what ever is left in the cache
        if len(mapping_buffer) > 0:
            session.bulk_insert_mappings(orm.Mapping, mapping_buffer)
            session.commit()

    if keyerrors > 0:
        warnings.warn(
            "there are at least '{0}' codes in '{1}'"
            " that are not in the lookup files".format(
                keyerrors, ReaderClass.BASENAME
            )
        )
    session.close()


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == '__main__':
    main()
