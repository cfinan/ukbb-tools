"""cmd-line tool for processing (and fixing) the UKBB mapping Excel spreadsheet
"""
from ukbb_tools import common, __version__, __name__ as pkg_name
from ukbb_tools.mapping import readers as mr
from simple_progress import progress
import argparse
import sys
import os
import csv
import gzip
import pprint as pp


MSG_PREFIX = '[info]'
"""The prefix for verbose messages and the location to write the messages
(`str`)
"""
MSG_OUT = sys.stderr
"""The location to write verbose information messages (`sys.stderr`)
"""

# All files written out in this module will have this as the delimiter
DELIMITER = "\t"
"""The delimiter of the output text files (`str`)
"""


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main():
    """The main entry point for the script
    """
    # Initialise and parse all the command line arguments
    parser = init_cmd_args()
    args = parse_cmd_args(parser)

    # Write messages to the user respecting verbosity
    m = progress.Msg(prefix='', verbose=args.verbose, file=MSG_OUT)
    m.msg("= Parse UKBB Mappings ({0} v{1}) =".format(pkg_name, __version__))
    m.prefix = MSG_PREFIX
    m.msg_atts(args)

    # Extract the worksheets and write them to the extracted directory
    # This extracts into tab-delimited files
    m.msg("converting excel file to text, this may be slow...")
    write_worksheets(
        args.ukbb_mapping_file,
        outdir=args.outdir,
        verbose=args.verbose
    )

    m.prefix = ''
    m.msg("*** END ***")


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def init_cmd_args():
    """Initialise the command line arguments and return the parsed arguments

    Returns
    -------
    args : `argparse.Namespace`
        The argparse namespace object
    """
    parser = argparse.ArgumentParser(
        description="process and fix the UKBB mapping files")

    parser.add_argument(
        'ukbb_mapping_file', type=str,
        help="The path to the UKBB mapping Excel spreadsheet"
    )
    parser.add_argument(
        'outdir', type=str,
        help="The output directory for the processed files"
    )
    parser.add_argument('-v', '--verbose',  action="store_true",
                        help="give more output")
    return parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_cmd_args(parser):
    """Initialise the command line arguments and return the parsed arguments

    Parameters
    ----------
    parser : `argparse.ArgumentParser`
        The argparseparser object with arguments defined

    Returns
    -------
    args : `argparse.Namespace`
        The argparse namespace object
    """
    args = parser.parse_args()

    # Go through all the directory arguments. If they have been defined
    # make sure they are expressed as a full path and are actually real
    # directories
    for i in ['outdir']:
        # Make sure it is a full path and not relative
        dir_name = os.path.realpath(
            os.path.expanduser(getattr(args, i))
        )

        # Make sure it is actually a directory
        if not os.path.isdir(dir_name):
            raise NotADirectoryError(
                "{0}  does not exist: '{1}'".format(
                    i,
                    dir_name
                )
            )
        setattr(args, i, dir_name)

    # required files
    for i in ['ukbb_mapping_file']:
        setattr(args, i, os.path.expanduser(getattr(args, i)))
        open(getattr(args, i)).close()

    return args


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def write_worksheets(mapping_file, outdir, verbose=False):
    """Write all the worksheets from the Excel workbook to tab delimited files

    Parameters
    ----------
    mapping_file : `str`
        The UKBB Excel spreadsheet distributed with the primary care
        documentation it is called ``all_lkps_maps.xlsx``. Note that this must
        be an ``xlsx`` file not an older ``xls`` file
    outdir : `str`
        The directory to output the UKBB mapping files
    verbose : `bool`, optional, default: `False`
        shall we document progress?

    Raises
    ------
    IndexError
        If any of the data rows in any of the worksheets does not have the
        expected length. This is assessed by comparing to the header length
    """
    empty = [None, 'None', 'UNDEF']
    copyright_row = [
        'Contains information from NHS Digital'.lower(),
        'ICD-9 is in the public domain'.lower(),
        'BNF Code Information, NHSBSA'.lower(),
        '"This material includes SNOMED Clinical'.lower(),
        'ICD-10 codes, terms and text'.lower(),
        'The OPCS Classification of Interventions'.lower()
    ]

    # Load up the mapping file - this class will also make sure that we
    # have the correct worksheet names in the file
    with mr.MappingFile(mapping_file) as mapf:
        # Loop through all the workbooks that we want to extract and
        # load into the database
        for w in mr.MappingFile.EXPECTED_WORKSHEETS:
            # Make an output file name
            tfn = os.path.join(outdir, '{0}.txt.gz'.format(w))

            # We will monitor progress of each workbook extraction
            prog = progress.RateProgress(
                default_msg="processing {0}".format(w),
                verbose=verbose
            )

            # Write to file with a tab delimiter and gzipped
            # TODO: Replace the flexiwriter
            # with file_helper.FlexiWriter(
            #         tfn, force_gzip=True,
            #         delimiter=GLOBAL_DELIMITER) as outfile:
            with common.stdopen(tfn, mode='wt', method=gzip.open,
                                use_tmp=True) as outfile:
                writer = csv.writer(
                    outfile, delimiter=DELIMITER, lineterminator=os.linesep
                )
                # The iterate_over is a generator function that will iterate
                # over a workbook
                gener = mapf.iterate_over(w)

                # We extract the first  row with should be the header row
                # we use the length of the header to make sure that all the
                # other rows are the required length
                header = next(gener)

                # Write the header to file. Note that we are writing our nice
                # normalised header not the horrible original ones
                writer.writerow(header)

                # Measure the header length, this is done here so it is only
                # done once and we can save a few function calls
                header_len = len(header)

                # Loop through the remainder of the worksheet, we are
                # using enumerate to get a count of the number of rows so we
                # Can use it later to initialise another progress monitor for
                # the database load
                for idx, row in prog.progress(enumerate(gener)):
                    # Make sure there are no unnecessary spaces, I have
                    # noticed some fileds have '   '
                    row = [str(r).strip() for r in row]

                    skip = False
                    for c in copyright_row:
                        if row[0].lower().startswith(c):
                            skip = True
                            break
                    if skip is True:
                        continue

                    # Here we will convert any NoneTypes to ''
                    row = [r if r not in empty else '' for r in row]

                    # Here we workout if all the fields are empty and if so
                    # then we skip the row
                    sum_missing = sum([r == '' for r in row])
                    if sum_missing == header_len:
                        continue

                    # Is the row a valid length - so do we have the same number
                    # of columns as we expect from the header
                    if len(row) != header_len:
                        raise IndexError("header '{0}' columns, row has "
                                         "'{1}' columns'".format(header_len,
                                                                 len(row)))

                    writer.writerow(row)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == '__main__':
    main()
