from ukbb_tools import columns as col, type_casts as tc
from collections import namedtuple

Table = namedtuple('Table', ['name', 'desc'])


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
CTV3_EQV = Table(
    'ctv3_eqv',
    'read CTV3 terms that are functionally equivalent. I am not 100% sure if'
    'this means complete synonyms or not.'
)

CTV3_EQV_CUI1 = col.ColName(
   "CUI1",
   str,
   "The first CTV3 equivalent concept"
)
CTV3_EQV_CUI2 = col.ColName(
   "CUI2",
   str,
   "The second CTV3 equivalent concept"
)
CTV3_EQV_PROVENANCE = col.ColName(
   "PROVENANCE",
   int,
   "NOT SURE: all the values in this column are 0"
)
CTV3_EQV_IDATE = col.ColName(
   "IDATE",
   str,
   "NOT SURE: should be a date field but has no values within it"
)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
CTV3_HIER = Table(
    'ctv3_hier',
    'The parent child relationships between the terms in CTV3. This has 287970'
    ' rows in it'
)

CTV3_HIER_CHILD = col.ColName(
   "CHILD",
   str,
   "The child CTV3 term"
)
CTV3_HIER_PARENT = col.ColName(
   "PARENT",
   str,
   "The parent (ancestor) CTV3 term"
)
CTV3_HIER_ORD = col.ColName(
   "ORD",
   int,
   "NOT SURE: This looks like an index and is associated with a parent child"
   " relationship although, there are repeated values for some of the parent"
   " child relationships. This column only takes values between 01-99"
)
CTV3_HIER_PTERM = col.ColName(
   "PTERM",
   str,
   "The parent term string"
)
CTV3_HIER_CTERM = col.ColName(
   "CTERM",
   str,
   "The child term string"
)
CTV3_HIER_DNUM = col.ColName(
   "DNUM",
   int,
   "NOT SURE - there are 287970 unique values in this column with a minimum"
   " value of 1 and a maximum value of 87595"
)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
CTV3 = Table(
    'ctv3',
    'A lookup of all terms in the CTV3 terminology, there are 395650 data rows'
    ' in this table'
)

CTV3_CUI = col.ColName(
   "CUI",
   str,
   "The CTV3 concept ID. Note that the concept ID is not unique in this table."
   " There are 332113 unique CUIs (from 395650 rows) (not including the "
   "header). The primary key for this table is a combination of cui, tui "
   "columns."
)
CTV3_STAT = col.ColName(
   "STAT",
   str,
   "The CTV3 concept status, there are 4 possible values: C = current?, E = "
   ", O = , R = retired"
)
CTV3_TUI = col.ColName(
   "TUI",
   str,
   "This is referred to as term code in the documentation. For CTV3 it is a"
   " 5 character code and I think it refers to different variants of the "
   "same code with one of the specific variants being preferred"
)
CTV3_TYP = col.ColName(
   "TYP",
   str,
   "The code type. This can take 2 values, P = prefered (or primary?) and"
   " S = secondary. Where there are multiple TUIs for a code, only one will"
   " be P and the remainder will be S"
)
CTV3_T30 = col.ColName(
   "T30",
   str,
   "A maximum 30 character text description of the concept code"
)
CTV3_T60 = col.ColName(
   "T60",
   str,
   "A maximum 60 character text description of the concept code"
)
CTV3_T198 = col.ColName(
   "T198",
   str,
   "A maximum 198 character text description of the concept code"
)
CTV3_FREQ = col.ColName(
   "FREQ",
   int,
   "The frequency of the code in the EHR data. The integer values are "
   "enumerations of ranges. There are 5 possible values -2, -1, 1, 2, 3. I "
   "am not sure at this stage what ranges they map to"
)
CTV3_RDATE = col.ColName(
   "RDATE",
   str,
   "NOT SURE: This looks like a 4 character date string of MMYY but there is"
   " also a 0000 field that could mean missing. Or perhaps it is YYMM? There"
   " are 39 unique values in this field"
)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
CTV3_RMF = Table(
    "ctv3_rmf",
    "This is a 2 column table and I am not 100% sure what it's significance "
    "is. It looks to be a list of superceeded CTV3 codes. There are 55272 data"
    " rows in it"
)

CTV3_RMF_NEW_CUI = col.ColName(
   "NEWCUI",
   str,
   "The NEW CTV3 concept that has superceeded the OLD CTV3 concept"
)
CTV3_RMF_OLD_CUI = col.ColName(
   "OLDCUI",
   str,
   "The OLD CTV3 concept that has been superceeded by the NEW CTV3 concept"
)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
CTV3_TC = Table(
    "ctv3_tc",
    "This looks to be related to the CTV3HIER table or rather is a superset of"
    " it that probably forms some lookup table for the hierarchy. It just "
    "contains 2 code columns SuperTypeID, SubTypeID and has 2241382 data rows."
    " Of these 332112 rows have the same code in both columns (which is the"
    " same as the number of unique CTV3 codes) and 1909270 have different "
    "codes"
)

CTV3_TC_SUPERTYPE_ID = col.ColName(
   "SupertypeID",
   str,
   "NOT SURE: but is a CTV3 code"
)
CTV3_TC_SUBTYPE_ID = col.ColName(
   "SubtypeID",
   str,
   "NOT SURE: but is a CTV3 code"
)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
CTV3_TERM = Table(
    "ctv3_term",
    "A list of term text descriptions referenced by term TUIs. This is likely"
    " a subset of the CTV3 table and is probably not needed. There are 351470"
    " data rows in it. Because of the reduced numbers of status values "
    "(E, O), it looks like it only contains current concept descriptions "
    "(perhaps preferred TUIs?)"
)

CTV3_TERM_TUI = col.ColName(
   "TUI",
   str,
   "The CTV3 term code. This is the primary key for this table"
)
CTV3_TERM_STAT = col.ColName(
   "STAT",
   str,
   "The CTV3 concept status, there are 2 possible values: C = current?, O = "
)
CTV3_TERM_T30 = col.ColName(
   "T30",
   str,
   "A maximum 30 character text description of the TUI"
)
CTV3_TERM_T60 = col.ColName(
   "T60",
   str,
   "A maximum 60 character text description of the TUI"
)
CTV3_TERM_T198 = col.ColName(
   "T198",
   str,
   "A maximum 198 character text description of the TUI"
)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
RCT_EQV = Table(
    'rct_equivalent',
    'read V2 terms that are functionally equivalent. I am not 100% sure if '
    'this means complete synonyms or not.'
)

RCT_EQV_CUI1 = col.ColName(
   "cui1",
   str,
   "The first RCT v2 equivalent concept"
)
RCT_EQV_CUI2 = col.ColName(
   "cui2",
   str,
   "The second RCT v2 equivalent concept"
)
RCT_EQV_PROVENANCE = col.ColName(
   "provenance",
   int,
   "NOT SURE - All the values in this column are 0"
)
RCT_EQV_IDATE = col.ColName(
   "idate",
   str,
   "NOT SURE - should be a date field but has no values within it"
)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
RCT_HIER = Table(
    'rct_hier',
    'The parent child relationships between the terms in CTV3. This has 287970'
    ' rows in it'
)

RCT_HIER_PARENT = col.ColName(
   "parent",
   str,
   "The parent (ancestor) Read v2 term"
)

RCT_HIER_CHILD = col.ColName(
   "child",
   str,
   "The child Read v2 term"
)

RCT_HIER_PTERM = col.ColName(
   "pterm",
   str,
   "The parent term string"
)
RCT_HIER_CTERM = col.ColName(
   "cterm",
   str,
   "The child term string"
)
RCT_HIER_DNUM = col.ColName(
   "dnum",
   int,
   "NOT SURE"
)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
RCT = Table(
    'rct',
    'A lookup of all terms in the RCT terminology, there are 175017 data rows'
    ' in this table'
)

RCT_CUI = col.ColName(
   "CUI",
   str,
   "The RCT concept ID. Note that the concept ID is not unique in this table."
   " There are 157281 unique CUIs (from 175017 rows) (not including the "
   "header). The primary key for this table is a combination of cui, tui "
   "columns."
)
RCT_TUI = col.ColName(
   "TUI",
   str,
   "This is referred to as term code in the documentation. For RCT it is a "
   "2 character code and I think it refers to different variants of the same"
   " code with one of the specific variants being preferred. The code is 00"
   " for the preferred code"
)
RCT_T30 = col.ColName(
   "T30",
   str,
   "A maximum 30 character text description of the concept code"
)
RCT_T60 = col.ColName(
   "T60",
   str,
   "A maximum 60 character text description of the concept code"
)
RCT_T198 = col.ColName(
   "T198",
   str,
   "A maximum 198 character text description of the concept code"
)
RCT_FREQ = col.ColName(
   "FREQ",
   int,
   "The frequency of the code in the EHR data. The integer values are"
   " enumerations of ranges. There are 5 possible values -2, -1, 1, 2, 3."
   " I am not sure at this stage what ranges they map to"
)
RCT_RDATE = col.ColName(
   "RDATE",
   str,
   "NOT SURE: This looks like a 4 character date string of MMYY but there is"
   " also a 0000 field that could mean missing. Or perhaps it is YYMM? There"
   " are 36 unique values in this field"
)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
RCT_TC = Table(
    "rct_tc",
    "This looks to be related to the RCTHIER table or rather is a superset of"
    " it that probably forms some lookup table for the hierarchy. It just"
    " contains 2 code columns SuperTypeID, SubTypeID"
)

RCT_TC_SUBTYPE_ID = col.ColName(
   "SUBTYPEID",
   str,
   "NOT SURE: but is a RCT code"
)
RCT_TC_SUPERTYPE_ID = col.ColName(
   "SUPERTYPEID",
   str,
   "NOT SURE: but is a RCT code"
)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
CTV3_RCT_MAP = Table(
    "ctv3_rct_map",
    "A backwards mapping between Read CTV3 and Read v2 codes. It looks from"
    " the table that the mappings between the terminologies are based on both"
    " the CUI and the TUI codes"
)

CTV3_RCT_MAP_SCUI = col.ColName(
   "SCUI",
   str,
   "The source CTV3 CUI"
)
CTV3_RCT_MAP_STUI = col.ColName(
   "STUI",
   str,
   "The source CTV3 term code"
)
CTV3_RCT_MAP_TCUI = col.ColName(
   "TCUI",
   str,
   "The target RCD (readv2) concept code"
)
CTV3_RCT_MAP_TTUI = col.ColName(
   "TTUI",
   str,
   "The target RCD (readv2) term code (ID), a value of 00 is the preferred"
   " term"
)
CTV3_RCT_MAP_MAPTYP = col.ColName(
   "MAPTYP",
   str,
   "The mapping type, there are three possible values N, E, A"
)
CTV3_RCT_MAP_ASSURED = col.ColName(
   "ASSURED",
   bool,
   "Is the mapping clinically assured? This is a boolean but has the values"
   " -1 for True (yes) and 0 for False (no)"
)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
CTV3_SCT_MAP = Table(
    "ctv3_sct_map",
    "A forward mapping between Read CTV3 and Snomed clinical terms (CT) UK"
    " edition"
)

CTV3_SCT_MAP_SCUI = col.ColName(
   "SCUI",
   str,
   "The source CTV3 CUI"
)
CTV3_SCT_MAP_STUI = col.ColName(
   "STUI",
   str,
   "The source CTV3 term code"
)
CTV3_SCT_MAP_TCUI = col.ColName(
   "TCUI",
   str,
   "The target Snomed CT concept ID. Note that althrough these are ints we "
   "will encode them as strings"
)
CTV3_SCT_MAP_TTUI = col.ColName(
   "TTUI",
   str,
   "The target snomed CT term code, I am not sure if these perform the same"
   " role as RCD and CTV3 term codes"
)
CTV3_SCT_MAP_MAPTYP = col.ColName(
   "MAPTYP",
   str,
   "The mapping type. This only contains a single value E"
)
CTV3_SCT_MAP_ASSURED = col.ColName(
   "ASSURED",
   bool,
   "Is the mapping clinically assured? This is a boolean for True (yes) and "
   "False (no)"
)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
ICD10_HIER = Table(
    "icd10_hier",
    "The ICD10 hierarchy providing parent child mappings between clinical "
    "concepts in the ICD10 coding system"
)

ICD10_HIER_PARENT = col.ColName(
   "PARENT",
   str,
   "The parent ICD10 code, some of the parent ICD codes are hyphenated spans"
   " such as A00–A09, I am not sure if these represent a real composite code "
   "or need expanding??"
)
ICD10_HIER_CHILD = col.ColName(
   "CHILD",
   str,
   "The child ICD10 code"
)
ICD10_HIER_PTERM = col.ColName(
   "PTERM",
   str,
   "The parent ICD10 term"
)
ICD10_HIER_CTERM = col.ColName(
   "CTERM",
   str,
   "The child ICD10 term"
)
ICD10_HIER_DNUM = col.ColName(
   "DNUM",
   int,
   "NOT SURE: "
)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
ICD10_SCT_MAP = Table(
    "icd10_sct_map",
    "A mapping between ICD10 clinical codes and Snomed CT (UK edition)"
)

ICD10_SCT_MAP_SCUI = col.ColName(
   "SCUI",
   str,
   "The source ICD10 clinical code"
)
ICD10_SCT_MAP_STUI = col.ColName(
   "STUI",
   str,
   "The source ICD10 term code. This is empty as presumably ICD10 does not "
   "have a true term code concept"
)
ICD10_SCT_MAP_TCUI = col.ColName(
   "TCUI",
   str,
   "The target Snomed CT clinical code"
)
ICD10_SCT_MAP_TTUI = col.ColName(
   "TTUI",
   str,
   "The target Snomed CT term code, this is empty probably becoase no ICD10"
   " TUI exists to map against"
)
ICD10_SCT_MAP_MAPTYP = col.ColName(
   "MAPTYP",
   str,
   "The mapping type. This contains a single value E"
)
ICD10_SCT_MAP_ASSURED = col.ColName(
   "ASSURED",
   bool,
   "Is the mapping clinically assured? This is a boolean for True (yes) and"
   " False (no). For this mapping table all values are True"
)

# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
ICD10_TC = Table(
    "icd10_tc",
    "Some form of lookup table? The structure of the SuperType ID column"
    " differs from the other *TC tables int that some of what presumably are"
    " top level ICD codes map to numeric codes such as 01, 02"
)

ICD10_TC_SUPERTYPE_ID = col.ColName(
   "SupertypeID",
   str,
   ""
)
ICD10_TC_SUBTYPE_ID = col.ColName(
   "SubtypeID",
   str,
   ""
)

# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
ICD10 = Table(
    "icd10",
    "An ICD 10 lookup table mapping codes to terms"
)

ICD10_CUI = col.ColName(
   "CUI",
   str,
   "The ICD10 clinical code"
)
ICD10_TERM = col.ColName(
   "TERM",
   str,
   "The ICD10 clinical term"
)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
MAP_VERSIONS = Table(
    "map_versions",
    "A table that stores the date stamps of when each of the mapping tables"
    " was last updated"
)

MAP_VERSIONS_MAP = col.ColName(
   "MAP",
   str,
   "The file prefix of the mapping file"
)
MAP_VERSIONS_RELEASE_DATE = col.ColName(
   "RELEASEDATE",
   str,
   "The release date of the mapping in YYYYMMDD format"
)
MAP_VERSIONS_BUILD_DATE = col.ColName(
   "BUILDDATE",
   str,
   "The build date of the mapping in YYYYMMDD format"
)
MAP_VERSIONS_SNAPSHOT_DATE = col.ColName(
   "SNAPSHOTDATE",
   str,
   "The snapshot date of the mapping in YYYYMMDD format, I am not clear what"
   " the difference is between build date and snapshot date, they are the "
   "same in all cases"
)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
OPCS4_HIER = Table(
    "opcs4_hier",
    "The parent child relationships between OPCS4 codes"
)

OPCS4_HIER_PARENT = col.ColName(
   "PARENT",
   str,
   "The parent OPCS4 code"
)
OPCS4_HIER_CHILD = col.ColName(
   "CHILD",
   str,
   "The child OPCS4 code"
)
OPCS4_HIER_PTERM = col.ColName(
   "PTERM",
   str,
   "The parent OPCS4 term"
)
OPCS4_HIER_CTERM = col.ColName(
   "CTERM",
   str,
   "The child OPCS4 term"
)
OPCS4_HIER_DNUM = col.ColName(
   "DNUM",
   int,
   "NOT SURE"
)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
OPCS4_SCT_MAP = Table(
    "opcs4_sct_map",
    "Mappings between the OPCS4 terminology and Snomed CT (UK edition)"
)

OPCS4_SCT_MAP_SCUI = col.ColName(
   "SCUI",
   str,
   "The source OPCS4 CUI"
)
OPCS4_SCT_MAP_STUI = col.ColName(
   "STUI",
   str,
   "The source OPCS4 term code. These are all empty"
)
OPCS4_SCT_MAP_TCUI = col.ColName(
   "TCUI",
   str,
   "The target Snomed CT CUI"
)
OPCS4_SCT_MAP_TTUI = col.ColName(
   "TTUI",
   str,
   "The target Snomed CT term code. These are all empty"
)
OPCS4_SCT_MAP_MAPTYP = col.ColName(
   "MAPTYP",
   str,
   "The mapping type. This contains a single value E"
)
OPCS4_SCT_MAP_ASSURED = col.ColName(
   "ASSURED",
   bool,
   "Is the mapping clinically assured? This is a boolean for True (yes) and"
   " False (no). For this mapping table all values are True"
)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
OPCS4_TC = Table(
    "opcs4_tc",
    ""
)

OPCS4_TC_SUPERTYPE_ID = col.ColName(
   "SupertypeID",
   str,
   ""
)
OPCS4_TC_SUBTYPE_ID = col.ColName(
   "SubtypeID",
   str,
   ""
)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
OPCS4 = Table(
    "opcs4",
    "An OPCS4 lookup table"
)

OPCS4_CUI = col.ColName(
   "CUI",
   str,
   ""
)
OPCS4_TERM = col.ColName(
   "TERM",
   str,
   ""
)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
RCT_CTV3_MAP = Table(
    "rct_ctv3_map",
    "Forward mapping from ReadV2 to CTV3"
)

RCT_CTV3_MAP_SCUI = col.ColName(
   "SCUI",
   str,
   ""
)
RCT_CTV3_MAP_STUI = col.ColName(
   "STUI",
   str,
   ""
)
RCT_CTV3_MAP_TCUI = col.ColName(
   "TCUI",
   str,
   ""
)
RCT_CTV3_MAP_TTUI = col.ColName(
   "TTUI",
   str,
   ""
)
RCT_CTV3_MAP_MAPTYP = col.ColName(
   "MAPTYP",
   tc.parse_read2ctv3_map_type,
   "A three letter composite code indicating the quality of the mapping: "
   "i.e. aN1. The first letter can be a,b,c,z the second and third letters can"
   " be N, O, R, S, A and 1, 2, 3. The first letter is the frequency the of "
    "the read2 cui and tui are used in typical GP records a = member of set "
    "of top 1000 most frequently used RCTs, b = member of set of next 4000 "
    "most frequently used RCTs, c = member of set of next 5000 most "
    "frequently used RCTs. z = all other RCTs (ie those used very rarely or"
    " never). The second letter and associated number have the following "
    "meaning. N1 = the base V2_CONCEPTID is still a current code current in"
    " CTV3 and all permutations of that read2 code and any of its term codes"
    " carry the same meaning (map to the same CTV3_CONCEPTID). O1 = the base"
    " V2_CONCEPTID is optional in CTV3 but a single current alternative can"
    " be identified using the RMF and/or DCF tables; R1 = the base "
    "V2_CONCEPTID has been retired in CTV3 because it means the same as one"
    " other current CTV3 concept code. The current alternative has been "
    "identified using the RMF and/or DCF tables; S1 = the set of terms "
    "associated with the original V2_CONCEPTID are not true synonyms of each"
    " other. The DCF table provides a single alternative CTV3 code for the"
    " particular combination of the base Read code and a term code. An = the"
    " original V2_CONCEPTID and V2_TERMID combination is inherently ambiguous."
    " The mapping table gives the original V2 code as the suggested candidate"
    " CTV3 map, but this will usually be inaccurate and unsatisfactory (e.g."
    " it will usually be an inactive CTV3 code). The DCF provides n possible"
    " more accurate candidate mappings to current CTV3 codes, but which (if"
    " any) of these should apply can only be accurately determined on a "
    "case-by-case basis by inspecting each individual coded record item "
    "instance that uses the original V2 code and deciding which candidate"
    " CTV3 map most closely represents the intended meaning in that record"
    " item.Possible values are aN1, aO1, aR1, aS1, bA1, bA2, bN1, bO1, bR1,"
    " bS1, cA2, cN1, cO1, cR1, cS1, zA1, zA2, zA3, zN1, zO1, zR1, zS1. In "
    "summary, this will be parsed for only the middle letter with N, O, R "
    "being safe to map at the term level and S, A only at the cui/tui "
    "combination level. As the UKBB data has only cuis in it, these mappings"
    " might best be avoided."
)
RCT_CTV3_MAP_ASSURED = col.ColName(
   "ASSURED",
   bool,
   ""
)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
RCT_SCT_MAP = Table(
    "rct_sct_map",
    "A forward mapping between ReadV2 and Snomed CT (UK edition)"
)

RCT_SCT_MAP_SCUI = col.ColName(
   "SCUI",
   str,
   ""
)
RCT_SCT_MAP_STUI = col.ColName(
   "STUI",
   str,
   ""
)
RCT_SCT_MAP_TCUI = col.ColName(
   "TCUI",
   str,
   ""
)
RCT_SCT_MAP_TTUI = col.ColName(
   "TTUI",
   str,
   ""
)
RCT_SCT_MAP_MAPTYP = col.ColName(
   "MAPTYP",
   str,
   "All E"
)
RCT_SCT_MAP_ASSURED = col.ColName(
   "ASSURED",
   bool,
   "True or False"
)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
SCT_ICD10_MAP = Table(
    "sct_icd10_map",
    "Mapping table between Snomed CT (UK edition) and ICD10"
)

SCT_ICD10_MAP_SCUI = col.ColName(
   "SCUI",
   str,
   ""
)
SCT_ICD10_MAP_STUI = col.ColName(
   "STUI",
   str,
   "empty"
)
SCT_ICD10_MAP_TCUI = col.ColName(
   "TCUI",
   str,
   ""
)
SCT_ICD10_MAP_TTUI = col.ColName(
   "TTUI",
   str,
   "empty"
)
SCT_ICD10_MAP_MAPTYP = col.ColName(
   "MAPTYP",
   str,
   "All E"
)
SCT_ICD10_MAP_ASSURED = col.ColName(
   "ASSURED",
   bool,
   "All True"
)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
SCT_OPCS4_MAP = Table(
    "sct_opcs4_map",
    "Mapping between Snomed CT (UK Edition) and OPCS4"
)

SCT_OPCS4_MAP_SCUI = col.ColName(
   "SCUI",
   str,
   ""
)
SCT_OPCS4_MAP_STUI = col.ColName(
   "STUI",
   str,
   "empty"
)
SCT_OPCS4_MAP_TCUI = col.ColName(
   "TCUI",
   str,
   ""
)
SCT_OPCS4_MAP_TTUI = col.ColName(
   "TTUI",
   str,
   "empty"
)
SCT_OPCS4_MAP_MAPTYP = col.ColName(
   "MAPTYP",
   str,
   "all E"
)
SCT_OPCS4_MAP_ASSURED = col.ColName(
   "ASSURED",
   bool,
   "All True"
)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
SCT_HIER = Table(
    "sct_hier",
    "The parent child relationships for Snomed CT "
)

SCT_HIER_CHILD = col.ColName(
   "CHILD",
   str,
   ""
)
SCT_HIER_PARENT = col.ColName(
   "PARENT",
   str,
   ""
)
SCT_HIER_CTERM = col.ColName(
   "CTERM",
   str,
   ""
)
SCT_HIER_PTERM = col.ColName(
   "PTERM",
   str,
   ""
)
SCT_HIER_DNUM = col.ColName(
   "DNUM",
   int,
   ""
)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
SCT_MOD_REL = Table(
    "sct_mod_rel",
    "This is a small table that I think contains all the Snomed CT modifier"
    " and relationship terms, so not actual clinical terms"
)

SCT_MOD_REL_REL = col.ColName(
   "REL",
   str,
   "The Snomed CT CUI that is either a relationship or a modifier code"
)
SCT_MOD_REL_TERM = col.ColName(
   "TERM",
   str,
   "The Snomed CT term that is either a relationship or a modifier term"
)
SCT_MOD_REL_NUM = col.ColName(
   "NUM",
   int,
   "I am assuming that this is some kind of count, of the number of Snomed CT"
   " codes that can have each modifier"
)
SCT_MOD_REL_ALLOWED = col.ColName(
   "ALLOWED",
   bool,
   "Presumably a flag to indicate if this modifier or relationship term is"
   " allowed to be used in the NHS setting?"
)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
SCT_REL = Table(
    "sct_rel",
    "The relationship between Snomed CTs. This is a richer relationship"
    " definitions that just a standard parent child relationships"
)

SCT_REL_CUI = col.ColName(
   "CUI",
   str,
   "The first Snomed CT CUI in the relationship"
)
SCT_REL_REL = col.ColName(
   "REL",
   str,
   "The first Snomed CT relationship term linking CUI and CUI2"
)
SCT_REL_CUI2 = col.ColName(
   "CUI2",
   str,
   "The second Snomed CT CUI in the relationship"
)
SCT_REL_RG = col.ColName(
   "RG",
   str,
   "I am assuming this stands for relationship group? The values in this "
   "column range between 0-15"
)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
SCT = Table(
    "sct",
    "The Snomsd CT (UK edition) lookup table"
)

SCT_CUI = col.ColName(
   "CUI",
   str,
   ""
)
SCT_PRIMITIVE = col.ColName(
   "PRIMITIVE",
   tc.parse_str_bool,
   ""
)
SCT_STAT = col.ColName(
   "STAT",
   int,
   ""
)
SCT_TUI = col.ColName(
   "TUI",
   str,
   ""
)
SCT_TUISTAT = col.ColName(
   "TUISTAT",
   int,
   ""
)
SCT_TYP = col.ColName(
   "TYP",
   int,
   ""
)
SCT_TERM = col.ColName(
   "TERM",
   str,
   ""
)
SCT_F0 = col.ColName(
   "F0",
   tc.parse_str_bool,
   ""
)
SCT_F1 = col.ColName(
   "F1",
   tc.parse_str_bool,
   ""
)
SCT_F2 = col.ColName(
   "F2",
   tc.parse_str_bool,
   ""
)
SCT_F3 = col.ColName(
   "F3",
   tc.parse_str_bool,
   ""
)
SCT_F4 = col.ColName(
   "F4",
   tc.parse_str_bool,
   ""
)
SCT_F5 = col.ColName(
   "F5",
   tc.parse_str_bool,
   ""
)
SCT_FREQ = col.ColName(
   "FREQ",
   int,
   ""
)
SCT_RDATE = col.ColName(
   "RDATE",
   tc.parse_yymm,
   ""
)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
SCT_SUBSET_DEFS = Table(
    "sct_subset_defs",
    "NOT SURE: It looks like it defines higher level definitions of groupings"
    " of Snomed CTs. Put another way, it looks like a list of Snomed CTs that"
    " are higher level subset terms. There are 444 data rows in this table."
)

SCT_SUBSET_DEFS_FOLDER = col.ColName(
   "FOLDER",
   str,
   "empty"
)
SCT_SUBSET_DEFS_REFSET_ID = col.ColName(
   "REFSETID",
   str,
   "A Snomed CT term that defines the subset. There are 442 unique values in"
   " here"
)
SCT_SUBSET_DEFS_SUBSET_ID = col.ColName(
   "SUBSETID",
   str,
   "NOT SURE: They look like Snomed CTs but not all of them appear in the "
   "lookup table"
)
SCT_SUBSET_DEFS_SUBSET_ORIGINAL_ID = col.ColName(
   "SUBSETORIGINALID",
   str,
   "NOT SURE: They look like Snomed CTs but not all of them appear in the"
   " lookup table"
)
SCT_SUBSET_DEFS_SUBSET_VERSION = col.ColName(
   "SUBSETVERSION",
   int,
   "All values are 1"
)
SCT_SUBSET_DEFS_SUBSET_NAME = col.ColName(
   "SUBSETNAME",
   str,
   "A Snomed CT term that describes the REFSETID"
)
SCT_SUBSET_DEFS_SUBSET_TYPE = col.ColName(
   "SUBSETTYPE",
   str,
   "NOT SURE - there are two unique values in here 0, 3"
)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
SCT_SUBSETS_RD = Table(
    "sct_subset_rds",
    "I think this is the assignment between the subsets defined in the subset"
    "_defs table and Snomed CTs within the subset. This contains 2328954 data"
    " rows"
)

SCT_SUBSETS_RD_SUBSET_ID = col.ColName(
   "SUBSETID",
   str,
   "NOT SURE: I think this links back to SUBSETID in the subset_defs table"
)
SCT_SUBSETS_RD_MEMBER_ID = col.ColName(
   "MEMBERID",
   str,
   "I think these are the Snomed CTs that are members of a particular subset"
)
SCT_SUBSETS_RD_MEMBER_STATUS = col.ColName(
   "MEMBERSTATUS",
   int,
   "NOT SURE - there are two possible values in here: 1, 2"
)
SCT_SUBSETS_RD_LINKED_ID = col.ColName(
   "LINKEDID",
   str,
   "NOT SURE - all the values are 0"
)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
SCT_SUBSETS = Table(
    "sct_subsets",
    "This is very similar to the SUBSETS_RD table but I am not sure what the"
    " difference is between the two, contains 461419 data rows"
)

SCT_SUBSETS_SUBSET_ID = col.ColName(
   "SUBSETID",
   str,
   "NOT SURE - I think this links back to SUBSETID in the subset_defs table"
)
SCT_SUBSETS_MEMBER_ID = col.ColName(
   "MEMBERID",
   str,
   "I think these are the Snomed CTs that are members of a particular subset"
)
SCT_SUBSETS_MEMBER_STATUS = col.ColName(
   "MEMBERSTATUS",
   int,
   "contains 256 unique possible values, looks like a signed small int"
)
SCT_SUBSETS_LINKED_ID = col.ColName(
   "LINKEDID",
   str,
   "NOT SURE - unlike subsets_rd, this has 194492 data rows"
)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
SCT_VERSIONS = Table(
    "sct_versions",
    "This looks like a metadata table and has a single version row"
)

SCT_VERSIONS_ITEM = col.ColName(
   "ITEM",
   str,
   "A single value GLOBAL_EXCLUSIONS_LIST"
)
SCT_VERSIONS_RELEASE_DATE = col.ColName(
   "RELEASEDATE",
   str,
   "YYYYMMDD"
)
SCT_VERSIONS_BUILD_DATE = col.ColName(
   "BUILDDATE",
   str,
   "YYYYMMDD"
)
SCT_VERSIONS_SNAPSHOT_DATE = col.ColName(
   "SNAPSHOTDATE",
   str,
   "empty"
)
SCT_VERSIONS_VERSION = col.ColName(
   "VERSION",
   str,
   "The version number of the download from NHS digital"
)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
SCT_CHIST = Table(
    "sct_chist",
    "Not sure but it looks like a log table to track changes in Snomed CT "
    "terms. It is empty"
)

SCT_CHIST_CUI = col.ColName(
   "CUI",
   str,
   "The Snomed CT code"
)
SCT_CHIST_RELEASE_VERSION = col.ColName(
   "RELEASEVERSION",
   str,
   "NOT SURE"
)
SCT_CHIST_CHANGE_TYPE = col.ColName(
   "CHANGETYPE",
   str,
   "NOT SURE"
)
SCT_CHIST_STAT = col.ColName(
   "STAT",
   str,
   "NOT SURE"
)
SCT_CHIST_REASON = col.ColName(
   "REASON",
   str,
   "NOT SURE"
)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
SCT_EQV = Table(
    "sct_eqv",
    "This looks like equivalent Snomed CT terms"
)

SCT_EQV_CUI1 = col.ColName(
   "CUI1",
   str,
   "The first Snomed CT equivalent code"
)
SCT_EQV_CUI2 = col.ColName(
   "CUI2",
   str,
   "The second Snomed CT equivalent code"
)
SCT_EQV_PROVENANCE = col.ColName(
   "PROVENANCE",
   int,
   "NOT SURE, contains 3 unique values 0,1,3"
)
SCT_EQV_IDATE = col.ColName(
   "IDATE",
   str,
   "NOT SURE: but I am guessing it is the date the equivalence was defined "
   "in YYYYMMDD"
)

# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
SCT_HIST = Table(
    "sct_hist",
    "NOT SURE but it looks like a tracking table that tracks changes in the"
    " SnomedCTs, there are 310687 data rows"
)

SCT_HIST_OLD_CUI = col.ColName(
   "OLDCUI",
   str,
   "The old SnomedCT code - I am no sure if old now means superceeded? This "
   "has 279936 unique values in it"
)
SCT_HIST_OLD_STAT = col.ColName(
   "OLDSTAT",
   int,
   "NOT SURE - This looks like some sort of status code for the old Snomed CT"
   " CUI, there are 7 possible values: 1,2,3,4,5,6,10"
)
SCT_HIST_NEW_CUI = col.ColName(
   "NEWCUI",
   str,
   "The new SnomedCT code - I am not sure if new now means it has replaced "
   "the old CUI?"
)
SCT_HIST_NEW_STAT = col.ColName(
   "NEWSTAT",
   int,
   "NOT SURE: This looks like some sort of status code for the new Snomed CT"
   " CUI, there are 7 possible values: 0,1,3,4,5,10,11"
)
SCT_HIST_IS_AMBIGUOUS = col.ColName(
   "IS_AMBIGUOUS",
   int,
   "NOT SURE - Some sort of flag for ambiguity, there are 4 possible values:"
   " 0,1,2,3"
)
SCT_HIST_IDATE = col.ColName(
   "IDATE",
   str,
   "NOT SURE: presumably the date when the old cui was replaced with the new"
   " cui: YYYYMMDD"
)

# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
SCT_HREL = Table(
    "sct_hrel",
    "NOT SURE - This looks like another relationship table between Snomed "
    "CTs. I am not clear how this related to sct_rel as it is part of the "
    "Snomed history database it could track previous relationship definitions?"
)

SCT_HREL_CUI = col.ColName(
   "CUI",
   str,
   "The first CUI in the relationship"
)
SCT_HREL_REL = col.ColName(
   "REL",
   str,
   "The Snomed CT relationship CUI"
)
SCT_HREL_CUI2 = col.ColName(
   "CUI2",
   str,
   "The second CUI in the relationship"
)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
SCT_META = Table(
    "sct_meta",
    "NOT SURE: It could be some track of the mappings between SnomedCT and"
    " CTV3"
)

SCT_META_CUI = col.ColName(
   "CUI",
   str,
   "The Snomed CT code"
)
SCT_META_STAT = col.ColName(
   "STAT",
   str,
   "The Snomed CT status - empty"
)
SCT_META_RDATE = col.ColName(
   "RDATE",
   str,
   "NOT SURE: A date field in YYYYMMDD"
)
SCT_META_ADATE = col.ColName(
   "ADATE",
   str,
   "NOT SURE: A date field in YYYYMMDD"
)
SCT_META_IDATE = col.ColName(
   "IDATE",
   str,
   "NOT SURE: A date field in YYYYMMDD"
)
SCT_META_CTV3_ID = col.ColName(
   "CTV3ID",
   str,
   "A read CTV3 code that maps to presumably maps to Snomed CT"
)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
SCT_SUBST = Table(
    "sct_subst",
    "NOT SURE: This table is empty"
)

SCT_SUBST_OLD_CUI = col.ColName(
   "OLDCUI",
   str,
   "NOT SURE: "
)
SCT_SUBST_OLD_STAT = col.ColName(
   "OLDSTAT",
   str,
   "NOT SURE: "
)
SCT_SUBST_NEW_CUI = col.ColName(
   "NEWCUI",
   str,
   "NOT SURE: "
)
SCT_SUBST_NEW_STAT = col.ColName(
   "NEWSTAT",
   str,
   "NOT SURE: "
)
SCT_SUBST_IS_AMBIGUOUS = col.ColName(
   "IS_AMBIGUOUS",
   str,
   "NOT SURE: "
)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
SCT_QT = Table(
    "sct_queries",
    "NOT SURE: but this is a large table at 16988776 data rows. It was the "
    "only table in the Snomed Query Access database so it could facilitate "
    "queries against Snomed"
)

SCT_QT_SUPERTYPE_ID = col.ColName(
   "SupertypeID",
   str,
   "NOT SURE: A super type Snomed CT code"
)
SCT_QT_SUBTYPE_ID = col.ColName(
   "SubtypeID",
   str,
   "NOT SURE: A sub type Snomed CT code"
)
SCT_QT_PROVENANCE = col.ColName(
   "Provenance",
   int,
   "NOT SURE: Can take 4 possible values 0,1,2,3"
)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
SCT_TOKEN_INDEX = Table(
    "sct_token_index",
    "This was part of the lexicon access database and I am assuming it "
    "facilitates the full text searching of the Snomed CT terms. It is big at"
    " 16977392 data rows. It looks to contain index values for tokenisation "
    "of the Snomed CT terms (but not the actual tokens themselves)"
)

SCT_TOKEN_INDEX_TID = col.ColName(
   "TID",
   int,
   "I think this is a foreign key that maps back to the tokens table. So in"
   " essence it maps a unique token string to a Snomed CT concept code"
)
SCT_TOKEN_INDEX_CUI = col.ColName(
   "CUI",
   str,
   "The Snomed CT concept code containing the token"
)
SCT_TOKEN_INDEX_SOURCE = col.ColName(
   "SOURCE",
   int,
   "NOT SURE: Has 3 possible values: 1,2,3"
)
SCT_TOKEN_INDEX_TOKNUM = col.ColName(
   "TOKNUM",
   int,
   "I am assuming that this is the token location in the respective Snomed "
   "CUI, so it it the first, second, third token etc... values in this "
   "column range from 1-125"
)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
SCT_TOKENS = Table(
    "sct_tokens",
    "The unique tokens that occur across the Snomed CT terms"
)

SCT_TOKENS_TID = col.ColName(
   "TID",
   int,
   "The primary key"
)
SCT_TOKENS_TOKEN = col.ColName(
   "TOKEN",
   str,
   "The token string"
)
SCT_TOKENS_NUM = col.ColName(
   "NUM",
   int,
   "The number of times that the token occurs in Snomed CT terms"
)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
SCT_WEQ = Table(
    "sct_token_weq",
    "NOT SURE"
)

SCT_WEQ_TID = col.ColName(
   "TID",
   int,
   "The token ID, a foreign key mapping back to the tokens table"
)
SCT_WEQ_EQT = col.ColName(
   "EQT",
   int,
   "NOT SURE"
)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
MANUAL_INCLUDES = Table(
    "include_mappings",
    "Manually defined mappings between CUIs in coding systems"
)
MANUAL_EXCLUDES = Table(
    "exclude_mappings",
    "Manually defined exclusions between CUIs in coding systems"
)

MANUAL_SOURCE_CUI = col.ColName(
    "source_cui",
    str,
    ""
)
MANUAL_SOURCE_CODING_SYSTEM = col.ColName(
    "source_coding_system",
    str,
    ""
)
MANUAL_TARGET_CUI = col.ColName(
    "target_cui",
    str,
    ""
)
MANUAL_TARGET_CODING_SYSTEM = col.ColName(
    "target_coding_system",
    str,
    ""
)
MANUAL_BIDIR = col.ColName(
    "bidirectional",
    tc.parse_bool,
    ""
)
MANUAL_OVERWRITE = col.ColName(
    "overwrite",
    tc.parse_bool,
    ""
)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
UKBB_CODE_POOL = Table(
    "ukbb_code_pool",
    "A table of all the UKBB clinical codes and their coding systems"
)

UKBB_CODE_POOL_CUI = col.ColName(
    "cui",
    str,
    "A clinical concept code that exists in the UKBB clinical data"
)
UKBB_CODE_POOL_CODING_SYSTEM = col.ColName(
    "coding_system",
    tc.parse_coding_system,
    "The clinical coding system of the CUI, should be one of opcs3,opcs4,"
    "icd9,icd10,read2,ctv3"
)


# START HERE
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
BNF = Table(
    "bnf",
    "A table of BNF codes"
)

BNF_PRES_CODE = col.ColName(
    'BNF_Presentation_Code',
    str,
    "The full BNF presentation code for the medication, dressing or appliance."
    "E.g., Yaltormin SR 500mg tablets, an antidiabetic drug with the chemical"
    "substance Metformin Hydrochloride, has a BNF code of 0601022B0BPAAAS."
)

BNF_PRES = col.ColName(
    "BNF_Presentation",
    str,
    "Provides more information on the product, e.g. whether it is a capsule,"
    "tablet or liquid, what the strength of the drug is, its volume (if liquid"
    "), sizes for various dressings, etc. Characters 12 and 13 of the BNF_"
    "Presentation_Code denote this. Yaltormin SR 500mg tablets are shown as AA"
    " (0601022B0BPAAAS), Yaltormin SR 750mg are shown as AB, and Yaltormin SR"
    " 1000mg as AC. Letters denoting strength do not always refer to the same "
    "formulation in other drugs – so AA in this example does not always refer"
    " to 500mg tablets for other chemicals. Characters 14 and 15 of the BNF"
    "_Presentation_Code relate to equivalent products. To illustrate, "
    "characters 14 and 15 of Yaltormin SR 500mg are coded with AS "
    "(0601022B0BPAAAS). All other products with the chemical substance "
    "Metformin Hydrochloride, with a dosage of 500mg, e.g. Meijumet 500mg,"
    " will be coded with AS. This structure is useful. If we want to look at"
    " the prescribing of all antidiabetic drugs, we can search our database"
    " for any items prescribed that begin with 060102. If we want to look "
    "at all generics in a paragraph, we can search under BNF_Product for AA."
)

BNF_PRODUCT = col.ColName(
    "BNF_Product",
    str,
    "Provides product name which can be proprietary e.g. Yaltormin SR, or the "
    "generic variant of the drug. Characters 10 and 11 of the BNF_Presentation"
    "_Code denote the product name. Yaltormin SR 500mg tablets are shown as BP"
    " (0601022B0BPAAAS), as are all other dosages of Yaltormin SR. The generic"
    " variant of a drug is always coded as AA."
)

BNF_CHEMICAL_SUBSTANCE = col.ColName(
    "BNF_Chemical_Substance",
    str,
    "Provides the international non-proprietary name, which is the official,"
    " generic and non-proprietary name given to a pharmaceutical drug or its"
    " active ingredient. Characters 8 and 9 of the BNF_Presentation_Code"
    " denote the chemical substance. The chemical substance for Yaltormin SR"
    " 500mg tablets is Metformin Hydrochloride, shown as 0601022B0BPAAAS. "
    "Note: Chapters 20 to 23 contain no chemical name, as these are dressings"
    " and appliances."
)

BNF_SUB_PARA = col.ColName(
    "BNF_Subparagraph",
    str,
    "Relates to the subparagraph of the paragraph, e.g. biguanides. Character"
    " 7 of the BNF_Presentation_Code denote the subparagraph. Subparagraph 2 "
    "of paragraph 2 in section 1 of chapter 6 relates to biguanides. Hence,"
    " this is shown as 0601022B0BPAAAS."
)

BNF_PARA = col.ColName(
    "BNF_Paragraph",
    str,
    "Shows the paragraph of the section in the BNF, e.g. antidiabetic drugs."
    " Characters 5 and 6 of the BNF_Presentation_Code denote the paragraph. "
    "Paragraph 2 of section 1 in chapter 6 covers antidiabetic drugs. Hence,"
    " this is shown as 0601022B0BPAAAS."
)

BNF_SECTION = col.ColName(
    "BNF_Section",
    str,
    "Relates to the section of the chapter in the BNF, e.g. drugs used in"
    " diabetes. Characters 3 and 4 of the BNF_Presentation_Code denote the"
    " chapter section. Section 1 of chapter 6 covers drugs used in diabetes."
    " Hence, this is shown as 0601022B0BPAAAS."
)

BNF_CHAPTER = col.ColName(
    "BNF_Chapter",
    str,
    "The associated BNF chapter, e.g. the endocrine system. Characters 1 and 2"
    " of the BNF_Presentation_Code denote the chapter. Chapter 6 covers the"
    " endocrine system. Hence, this is shown as 0601022B0BPAAAS"
)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
DMD = Table(
    "dmd",
    "drud used in the NHS?"
)
DMD_CUI = col.ColName(
    "concept_id",
    int,
    "dm+d code"
)
DMD_TERM = col.ColName(
    "term",
    str,
    "Description of the dm+d code"
)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
ICD9 = Table(
    "icd9",
    "ICD9 lookup tables"
)

ICD9_CUI = col.ColName(
    "ICD9",
    str,
    "ICD9 code"
)
ICD9_TERM = col.ColName(
    "DESCRIPTION_ICD9",
    str,
    "Description of the ICD9 code"
)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
ICD10_UKBB = Table(
    "icd10",
    "ICD9 lookup tables"
)
ICD10_UKBB_CUI = col.ColName(
    "ICD10_CODE",
    str,
    "ICD10 code"
)
ICD10_UKBB_ALT_CUI = col.ColName(
    "ALT_CODE",
    str,
    "ICD-10 Code, alternate form: This form strips the decimal point from the"
    " code and appends the filler X where the 3 character category is "
    "undivided. For example: A00.1 = A001; A46 = A46X; A15.0 = A150; I70.00"
    " = I7000"
)
ICD10_UKBB_USAGE = col.ColName(
    "USAGE",
    str,
    "Dagger / Asterisk indication"
)
ICD10_UKBB_USAGE_UK = col.ColName(
    "USAGE_UK",
    int,
    "Dagger / Asterisk indication"
)
ICD10_UKBB_TERM = col.ColName(
    "DESCRIPTION",
    str,
    "Description of the ICD10 code - Longest preferred rubric"
)
ICD10_UKBB_MOD4 = col.ColName(
    "MODIFIER_4",
    str,
    "4th character modifier suffix"
)
ICD10_UKBB_MOD5 = col.ColName(
    "MODIFIER_5",
    str,
    "5th character modifier suffix"
)
ICD10_UKBB_QUAL = col.ColName(
    "QUALIFIERS",
    str,
    "Dual classification (asterisk codes)"
)
ICD10_UKBB_GENDER = col.ColName(
    "GENDER_MASK",
    str,
    "Gender mask: Identifies single sex conditions"
)
ICD10_UKBB_MIN_AGE = col.ColName(
    "MIN_AGE",
    str,
    "Minimum age that applies to this code"
)
ICD10_UKBB_MAX_AGE = col.ColName(
    "MAX_AGE",
    str,
    "Maximum age that applies to this code"
)
ICD10_UKBB_TREE_DESCRIPTION = col.ColName(
    "TREE_DESCRIPTION",
    str,
    "This data field also contains descriptions that are functional in the"
    " context of the 3 character descriptions of their parent category, and"
    " are thus suitable for presentation where that context is available. For"
    " example: C00 Malignant neoplasm of lip -> C00.0 External upper lip ->"
    " C00.1 External lower lip -> C00.2 External lip, unspecified"
)

# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
RCT_UKBB = Table(
    "rct_ukbb",
    "Read2 codes from UKBB"
)
RCT_UKBB_CUI = col.ColName(
    "read_code",
    str,
    "Read V2 code"
)
RCT_UKBB_TUI = col.ColName(
    "term_code",
    tc.parse_read2_tui,
    "There is more than one way of describing the same clinical concept. For"
    " example, a ‘Myocardial Infarction’ may be referred to as a ‘Heart"
    " Attack’. In Read V2, ‘term codes’ are typically synonyms that are"
    " different text descriptions of the same thing. Those marked with ‘0’ "
    "denote the preferred term. Those marked with ‘11’, ‘12’ ... are "
    "synonyms. term_description Over time, some synonyms have been added"
    " that have a different meaning to the preferred term. Use of such "
    "synonyms where their meaning is different from the preferred term "
    "should be avoided."
)
RCT_UKBB_TERM = col.ColName(
    "term_description",
    str,
    "Description of the Read V2 code"
)

# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
RCT_UKBB_DRUGS = Table(
    "rct_ukbb_drugs",
    "drug related read codes"
)
RCT_UKBB_DRUGS_CUI = col.ColName(
    "read_code",
    str,
    "Read V2 code"
)
RCT_UKBB_DRUGS_TERM = col.ColName(
    "term_description",
    str,
    "Description of the Read V2 code"
)
RCT_UKBB_DRUGS_STATUS = col.ColName(
    "status_flag",
    int,
    "No information could be sourced for the meaning of this variable"
)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
CTV3_UKBB = Table(
    "ctv3_ukbb",
    "Read CTV3 lookup table"
)
CTV3_UKBB_CUI = col.ColName(
    "read_code",
    str,
    "Read CTV3 code."
)
CTV3_UKBB_TERM = col.ColName(
    "term_description",
    str,
    "Description of the Read Code."
)
CTV3_UKBB_TYPE = col.ColName(
    "description_type",
    str,
    "Descriptions come in two categories, preferred (P) and synonymous (S)."
    " The preferred term is the accepted official terminology, and the "
    "synonymous terms provide alternative definitions to this terminology. To"
    " illustrate, the concept of asthma can be described as 'Asthma' (this is"
    " the default, or preferred term), but can also be described as "
    "'Bronchial Asthma' (a synonym)."
)
CTV3_UKBB_STATUS = col.ColName(
    "status",
    str,
    "Codes are marked as Current (C), Optional (O) or Extinct (E). No "
    "information could be found for values marked as R."
)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
OPCS3 = Table(
    "opcs3",
    "OPCS3 lookup table"
)
OPCS3_CUI = col.ColName(
    "coding",
    str,
    ""
)
OPCS3_TERM = col.ColName(
    "meaning",
    str,
    ""
)
OPCS3_NODE = col.ColName(
    "node_id",
    int,
    ""
)
OPCS3_PARENT = col.ColName(
    "parent_id",
    int,
    ""
)
OPCS3_SELECT = col.ColName(
    "selectable",
    str,
    ""
)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
OPCS4_UKBB = Table(
    "opcs4_ukbb",
    "OPCS4 lookup table"
)
OPCS4_UKBB_CUI = col.ColName(
    "coding",
    str,
    ""
)
OPCS4_UKBB_TERM = col.ColName(
    "meaning",
    str,
    ""
)
OPCS4_UKBB_NODE = col.ColName(
    "node_id",
    int,
    ""
)
OPCS4_UKBB_PARENT = col.ColName(
    "parent_id",
    int,
    ""
)
OPCS4_UKBB_SELECT = col.ColName(
    "selectable",
    str,
    ""
)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
ICD9_ICD10_MAP = Table(
    "icd9_icd10_map",
    "Mappings between ICD9 and ICD10"
)

ICD9_ICD10_MAP_CUI1 = col.ColName(
    "ICD9",
    str,
    "ICD9 code"
)
ICD9_ICD10_MAP_TERM1 = col.ColName(
    "DESCRIPTION_ICD9",
    str,
    "Description of the ICD-9 code"
)
ICD9_ICD10_MAP_CUI2 = col.ColName(
    "ICD10",
    str,
    "ICD10 code"
)
ICD9_ICD10_MAP_TERM2 = col.ColName(
    "DESCRIPTION_ICD10",
    str,
    "Description of the ICD-10 code"
)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
CTV3_ICD10_MAP = Table(
    "ctv3_icd10_ukbb",
    "UKBB mapping table between Read CTV3 and ICD10 - assuming it is "
    "bidirectional"
)
CTV3_ICD10_READ3_CODE = col.ColName(
    "read_code",
    str,
    "Read CTV3 code."
)
CTV3_ICD10_ICD10_CODE = col.ColName(
    "icd10_code",
    str,
    "Mapped ICD-10 code."
)
CTV3_ICD10_MAP_STATUS = col.ColName(
    "mapping_status",
    tc.parse_ukbb_read_mapping_type,
    "Denotes the nature of the mapping. Contains letters E, G, D, R, A and U."
    "E = Exact one-to-one mapping. There is an exact match between host and "
    "target codes. There are no alternatives. G = Target concept more general."
    " The mapping is correct, but Read coded concept is more detailed. There "
    "are no alternatives. D = Default mapping. Indicates either an alternative"
    " that is most acceptable in the absence of other information or a partial"
    " mapping in cases where one Read Code maps onto a pair of target codes. "
    "There is not necessarily a default among a set of alternatives (other "
    "than in OPCS-4 and ICD-10). R = Requires checking. This alternative "
    "mapping must be checked against the default that has also been supplied "
    "in the table. (Different rubric from default). A = Alternative mapping. "
    "All alternatives that are not marked as D or R fall into this category. "
    "(Same rubric as default). U = Unspecified. Not used."
)
CTV3_ICD10_REFINE = col.ColName(
    "refine_flag",
    str,
    "Denotes whether or not the target code is sufficiently detailed to be"
    " acceptable. 3-character ICD codes are usually not acceptable, for "
    "example. Covers addition of 4th and 5th digit extensions in ICD, 4th "
    "character in OPCS-4. C = Completely refined. M = Mandatory to refine "
    "further. P = Possible but not mandatory to refine further"
)
CTV3_ICD10_ADD_CODE = col.ColName(
    "add_code_flag",
    str,
    "Denotes whether or not the target system specifies that extra codes might"
    " be added to the target code (e.g. aetiology codes in ICD and site codes"
    " in OPCS-4). C = Complete. No further codes need be added. M = Mandatory"
    " to add a further code. P = Possible but not mandatory to add a further"
    " code"
)
CTV3_ICD10_ELEMENT_NO = col.ColName(
    "element_num",
    str,
    "A Read Code may need several target codes for a complete mapping, but "
    "each of these may have alternatives. Therefore, each set of alternatives"
    " is given a distinct element number. Element numbers start at 0, "
    "incrementing by 1. There are rarely more than two sets."
)
CTV3_ICD10_BLOCK_NO = col.ColName(
    "block_num",
    str,
    "A block is a complete set of target codes for a mapping from any one Read"
    " Code (including all alternatives to the suggested codes). There are a"
    " number of occasions where more than one block exists for a Read Code."
    " This occurs, for example, when a Read Code maps either to a single"
    " target code or to a target code plus a second code (which may itself "
    "have alternatives). Blocks are numbered successively 0, 1, 2...Note that"
    " elements exist within blocks and that usually there is only one block."
)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
CTV3_ICD9_MAP = Table(
    "ctv3_icd9_ukbb",
    "UKBB mapping table between Read CTV3 and ICD9 - assuming it is "
    "bidirectional"
)
CTV3_ICD9_READ3_CODE = col.ColName(
    "read_code",
    str,
    "Read CTV3 code."
)
CTV3_ICD9_ICD9_CODE = col.ColName(
    "icd9_code",
    str,
    "Mapped ICD-9 code."
)
CTV3_ICD9_MAP_STATUS = col.ColName(
    "mapping_status",
    str,
    "Denotes the nature of the mapping. Contains letters E, G, D, R, A and U. "
    "E = Exact one-to-one mapping. There is an exact match between host and "
    "target codes. There are no alternatives. G = Target concept more general."
    " The mapping is correct, but Read coded concept is more detailed. There"
    " are no alternatives. D = Default mapping. Indicates either an "
    "alternative that is most acceptable in the absence of other information"
    " or a partial mapping in cases where one Read code maps onto a pair of"
    " target codes. There is not necessarily a default among a set of "
    "alternatives (other than in OPCS-4 and ICD-10). R = Requires checking. "
    "This alternative mapping must be checked against the default that has"
    " also been supplied in the table. (Different rubric from default). A"
    " = Alternative mapping. All alternatives that are not marked as D or R"
    " fall into this category. (Same rubric as default). U = Unspecified."
    " Not used."
)
CTV3_ICD9_REFINE = col.ColName(
    "refine_flag",
    str,
    "Denotes whether or not the target code is sufficiently detailed to be"
    " acceptable. 3-character ICD codes are usually not acceptable, for "
    "example. Covers addition of 4th and 5th digit extensions in ICD, 4th"
    " character in OPCS-4. C = Completely refined. M = Mandatory to refine"
    " further. P = Possible but not mandatory to refine further"
)
CTV3_ICD9_ADD_CODE = col.ColName(
    "add_code_flag",
    str,
    "Denotes whether or not the target system specifies that extra codes "
    "might be added to the target code (e.g. aetiology codes in ICD and site"
    " codes in OPCS-4). C = Complete. No further codes need be added. M ="
    " Mandatory to add a further code. P = Possible but not mandatory to add"
    " a further code."
)
CTV3_ICD9_ELEMENT_NO = col.ColName(
    "element_num",
    str,
    "A Read code may need several target codes for a complete mapping, but"
    " each of these may have alternatives. Therefore, each set of alternatives"
    " is given a distinct element number. Element numbers start at 0, "
    "incrementing by 1. There are rarely more than two sets."
)
CTV3_ICD9_BLOCK_NO = col.ColName(
    "block_num",
    str,
    "A block is a complete set of target codes for a mapping from any one"
    " Read code (including all alternatives to the suggested codes). There "
    "are a number of occasions where more than one block exists for a Read "
    "code. This occurs, for example, when a Read code maps either to a single"
    " target code or to a target code plus a second code (which may itself "
    "have alternatives). Blocks are numbered successively 0, 1, 2... Note "
    "that elements exist within blocks and that usually there is only one "
    "block."
)

# read_ctv3_opcs4.txt.gz
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
CTV3_OPCS4_MAP = Table(
    "ctv3_opcs4_ukbb",
    "UKBB mapping table between Read CTV3 and OPCS4 - assuming it is "
    "bidirectional"
)
CTV3_OPCS4_READ3_CODE = col.ColName(
    "read_code",
    str,
    "Read CTV3 code."
)
CTV3_OPCS4_OPCS4_CODE = col.ColName(
    "opcs4_code",
    str,
    "Mapped OPCS-4 code."
)
CTV3_OPCS4_MAP_STATUS = col.ColName(
    "mapping_status",
    str,
    "Denotes the nature of the mapping. Contains letters E, G, D, R, A and U. "
    "E = Exact one-to-one mapping. There is an exact match between host and "
    "target codes. There are no alternatives. G = Target concept more general."
    " The mapping is correct, but Read coded concept is more detailed. There "
    "are no alternatives. D = Default mapping. Indicates either an alternative"
    " that is most acceptable in the absence of other information or a partial"
    " mapping in cases where one Read Code maps onto a pair of target codes. "
    "There is not necessarily a default among a set of alternatives (other"
    " than in OPCS-4 and ICD-10). R = Requires checking. This alternative "
    "mapping must be checked against the default that has also been supplied"
    " in the table. (Different rubric from default). A = Alternative mapping."
    " All alternatives that are not marked as D or R fall into this category."
    " (Same rubric as default). U = Unspecified. Not used."
)
CTV3_OPCS4_REFINE = col.ColName(
    "refine_flag",
    str,
    "Denotes whether or not the target code is sufficiently detailed to be"
    " acceptable. 3-character ICD codes are usually not acceptable, for "
    "example. Covers addition of 4th and 5th digit extensions in ICD, 4th"
    " character in OPCS-4. C = Completely refined. M = Mandatory to refine"
    " further. P = Possible but not mandatory to refine further"
)
CTV3_OPCS4_ADD_CODE = col.ColName(
    "add_code_flag",
    str,
    "Denotes whether or not the target system specifies that extra codes might"
    " be added to the target code (e.g. aetiology codes in ICD and site codes"
    " in OPCS-4). C = Complete. No further codes need be added. M = Mandatory"
    " to add a further code. P = Possible but not mandatory to add a further "
    "code."
)
CTV3_OPCS4_ELEMENT_NO = col.ColName(
    "element_num",
    str,
    "A Read Code may need several target codes for a complete mapping, but "
    "each of these may have alternatives. Therefore, each set of alternatives"
    " is given a distinct element number. Element numbers start at 0, "
    "incrementing by 1. There are rarely more than two sets."
)
CTV3_OPCS4_BLOCK_NO = col.ColName(
    "block_num",
    str,
    "A block is a complete set of target codes for a mapping from any one "
    "Read Code (including all alternatives to the suggested codes). There are"
    " a number of occasions where more than one block exists for a Read Code."
    " This occurs, for example, when a Read Code maps either to a single "
    "target code or to a target code plus a second code (which may itself "
    "have alternatives). Blocks are numbered successively 0, 1, 2...Note "
    "that elements exist within blocks and that usually there is only one"
    " block."
)

# read_ctv3_read_v2.txt.gz
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
CTV3_RCT_UKBB_MAP = Table(
    "ctv3_rct_ukbb",
    "UKBB mapping table between Read CTV3 and Read V2"

)
CTV3_RCT_UKBB_READ3_CODE = col.ColName(
    "READV3_CODE",
    str,
    "Read CTV3 code."
)
CTV3_RCT_UKBB_CTV3_CODE = col.ColName(
    "TERMV3_CODE",
    str,
    "Read CTV3 term code"
)
CTV3_RCT_UKBB_READ3_DESC_TYPE = col.ColName(
    "TERMV3_TYPE",
    str,
    "Descriptions come in two categories, preferred (P) and synonymous (S). "
    "The preferred term is the accepted official terminology, and the "
    "synonymous terms provide alternative definitions to this terminology. To"
    " illustrate, the concept of asthma can be described as 'Asthma' (this is"
    " the default, or preferred term), but can also be described as 'Bronchial"
    " Asthma' (a synonym)"
)
CTV3_RCT_UKBB_READ3_DESC = col.ColName(
    "TERMV3_DESC",
    str,
    "Description of the Read CTV3 code"
)
CTV3_RCT_UKBB_READ2_CODE = col.ColName(
    "READV2_CODE",
    str,
    "Read V2 code"
)
CTV3_RCT_UKBB_READ2_DESC = col.ColName(
    "READV2_DESC",
    str,
    "Description of the associated Read V2 preferred or synonymous term."
)
CTV3_RCT_UKBB_READ2_DESC_ORDER = col.ColName(
    "TERMV2_ORDER",
    str,
    "Similar to the term_code variable in the read_v2_lkp file, those marked"
    " with ‘00’ denote the preferred term. Those marked with ‘11’, ‘12’ ..."
    " are synonyms"
)
CTV3_RCT_UKBB_READ2_DESC_TYPE = col.ColName(
    "TERMV2_TYPE",
    str,
    "See TERMV3_TYPE description"
)
CTV3_RCT_UKBB_READ2_ALT_DESC = col.ColName(
    "TERMV2_DESC",
    str,
    "Description of the associated Read V2 preferred or synonymous term."
)
CTV3_RCT_UKBB_IS_ASSURED = col.ColName(
    "IS_ASSURED",
    int,
    "The current clinical assurance status of each map assertion between a"
    " source CTV3 concept and term ID pair and a target READ2 concept and term"
    " ID pair, where 0 = Not assured and 1 = Assured. A semi-automatic quality"
    " assurance process was agreed with the NHS GP Systems of Choice (GPSoC)"
    " programme in October 2010 and subsequently with the Joint GP IT "
    "Committee. Under this process, maps are automatically assured if the"
    " 5-Byte READ target concept has a legitimate description that is exactly"
    " lexically equivalent to at least one of the three string-length variant"
    " strings encoded for by the original CTV3 TermID."
)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
RCT_ICD10_UKBB_MAP = Table(
    "rct_icd10_ukbb",
    "UKBB mapping table between Read V2 and ICD10 - assuming bidirectional"

)
RCT_ICD10_UKBB_READ2_CODE = col.ColName(
    "read_code",
    str,
    "Read V2 code"
)
RCT_ICD10_UKBB_ICD10_CODE = col.ColName(
    "icd10_code",
    str,
    "Mapped ICD-10 code"
)
RCT_ICD10_UKBB_ICD10_DEF = col.ColName(
    "icd10_code_def",
    int,
    "Signifies, e.g., whether or not the Read V2 code matches to a single "
    "ICD-10 code, if the ICD-10 code is a parent code that links to several"
    " child codes, if the ICD-10 code is asterisk or dagger code, a "
    "dagger-asterisk combination, etc."
)

# read_v2_icd9.txt.gz
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
RCT_ICD9_UKBB_MAP = Table(
    "rct_icd9_ukbb",
    "UKBB mapping table between Read V2 and ICD9 - assuming bidirectional"

)
RCT_ICD9_UKBB_READ2_CODE = col.ColName(
    "read_code",
    str,
    "Read V2 code"
)
RCT_ICD9_UKBB_ICD9_CODE = col.ColName(
    "icd9_code",
    str,
    "Mapped ICD-9 code"
)
RCT_ICD9_UKBB_ICD9_DEF = col.ColName(
    "icd9_code_def",
    int,
    "Signifies, e.g., whether or not the Read V2 code matches to a single "
    "ICD-9 code, if the ICD-9 code is a parent code that links to several "
    "child codes, if the ICD-9 code is asterisk or dagger code, a "
    "dagger-asterisk combination, etc."
)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
RCT_OPCS4_UKBB_MAP = Table(
    "rct_OPCS4_ukbb",
    "UKBB mapping table between Read V2 and OPCS4 - assuming bidirectional"

)
RCT_OPCS4_UKBB_READ2_CODE = col.ColName(
    "read_code",
    str,
    "Read V2 code"
)
RCT_OPCS4_UKBB_OPCS4_CODE = col.ColName(
    "opcs_4.2_code",
    str,
    "opcs_code_def"
)
RCT_OPCS4_UKBB_OPCS4_DEF = col.ColName(
    "opcs_code_def",
    int,
    "Signifies, e.g., whether or not the Read V2 code matches to a single "
    "OPCS-4 code, if the OPCS-4 code is a parent code that links to several"
    " child codes, etc."
)

# read_v2_read_ctv3.txt.gz
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
RCT_CTV3_UKBB_MAP = Table(
    "rct_ctv3_ukbb",
    "UKBB mapping table between Read V2 and Read CTV"

)
RCT_CTV3_UKBB_CHAPTER = col.ColName(
    "CHAPTER",
    str,
    "Read V2 chapter"
)
RCT_CTV3_UKBB_READ2_CODE = col.ColName(
    "READV2_CODE",
    str,
    "Read V2 code"
)
RCT_CTV3_UKBB_READ2_DESC = col.ColName(
    "READV2_DESC",
    str,
    "Description of the Read V2 code"
)
RCT_CTV3_UKBB_READ2_ALT_DESC = col.ColName(
    "TERMV2_DESC",
    str,
    "There is more than one way of describing the same clinical concept. For"
    " example, a ‘Myocardial Infarction’ may be referred to as a ‘Heart "
    "Attack’. In this instance, the term descriptions are typically synonyms"
    " that are different text descriptions of the same thing"
)
RCT_CTV3_UKBB_READ2_DESC_ORDER = col.ColName(
    "TERMV2_ORDER",
    str,
    "As in TERMV2_DESC, descriptions come in two categories, preferred and"
    " synonymous. The preferred term is the accepted official terminology, "
    "and the synonymous terms provide alternative definitions to this "
    "terminology. In relation to TERMV2_DESC, those marked with ‘00’ denote"
    " the preferred term. Those marked with ‘11’, ‘12’ ... are synonyms"
)
RCT_CTV3_UKBB_READ2_DESC_TYPE = col.ColName(
    "TERMV2_TYPE",
    str,
    "P = preferred term, S = synonym"
)
RCT_CTV3_UKBB_READ3_CODE = col.ColName(
    "READV3_CODE",
    str,
    "Read CTV3 code"
)
RCT_CTV3_UKBB_CTV3_CODE = col.ColName(
    "TERMV3_CODE",
    str,
    "The five character alphanumeric code for a CTV3 Term. Individual CTV3"
    " concept codes, such as ‘X20QM’ in Table 2, can have multiple different"
    " terms associated with them. Each term is available in 30-, 60- and"
    " 198- character variants and each such triad has its own five-character"
    " term code (e.g. ‘Y21Eu’, ‘Y21Ev’, ‘Y21Ew’ and ‘Y21Ex’). Of all the"
    " term codes associated with a given concept code, one is the "
    "‘preferred’ term for that concept and the others ‘synonyms’."
)
RCT_CTV3_UKBB_READ3_DESC_TYPE = col.ColName(
    "TERMV3_TYPE",
    str,
    "P = preferred term, S = synonym"
)
RCT_CTV3_UKBB_READ3_DESC = col.ColName(
    "TERMV3_DESC",
    str,
    "Description of the Read CTV3 code"
)
RCT_CTV3_UKBB_IS_ASSURED = col.ColName(
    "IS_ASSURED",
    int,
    "No description"
)
