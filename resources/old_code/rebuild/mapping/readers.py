from ukbb_tools import base_readers as br
from ukbb_tools.mapper import file_defs as fd
from openpyxl import load_workbook
import pprint as pp


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class MappingFile(object):
    """
    A handler for the UKBB excel mapping file that usually has the name
    `all_lkps_maps.xlsx`. This uses the `openpyxl` workbook to manipulate the
    Excel workbook
    """
    EXPECTED_WORKSHEETS = ['bnf_lkp', 'dmd_lkp', 'icd9_lkp', 'icd10_lkp',
                           'icd9_icd10', 'read_v2_lkp', 'read_v2_drugs_lkp',
                           'read_v2_drugs_bnf', 'read_v2_icd9',
                           'read_v2_icd10', 'read_v2_opcs4',
                           'read_v2_read_ctv3', 'read_ctv3_lkp',
                           'read_ctv3_icd9', 'read_ctv3_icd10',
                           'read_ctv3_opcs4', 'read_ctv3_read_v2']

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, infile):
        """
        Parameters
        ----------
        infile : str
            the path to the Excel workbook
        """
        self._infile = infile

        self._is_open = False
        self._using_context_manager = False
        self._is_iterating = False

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __enter__(self):
        """
        Entry point for the context manager
        """
        self.open()
        self._using_context_manager = True
        return self

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __exit__(self, *args):
        """
        Entry point for the context manager

        *args
            Error arguments should the context manager error exit
        """
        self.close()
        self._using_context_manager = False

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __iter__(self):
        """
        Initialise the iterator
        """
        if self._is_open is False:
            self.open()

        # make sure we are at the start
        self._is_iterating = True
        return self

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __next__(self):
        """
        The next row in the file
        """
        if self._is_open is False:
            self.open()
        self._is_iterating = True
        return self._next_row()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def open(self):
        """
        Open the file. Note that this is agnostic for gzipped files
        """
        if self._is_open is True:
            raise IOError('file already open')

        # Attempt to open
        # self._book = xlrd.open_workbook(self._infile, on_demand=True)
        self._book = load_workbook(self._infile)
        self._check_worksheets()
        self._current_ws = self._book[self.__class__.EXPECTED_WORKSHEETS[0]]
        self._is_open = True

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def close(self):
        """
        Close the file
        """
        if self._is_open is False and self._using_context_manager is False:
            raise IOError('file not open')

        # self._book.close()
        self._is_open = False

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_worksheets(self):
        """
        return the worksheets in the Excel workbook
        """
        return self._book.sheet_names()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def iterate_over(self, worksheet):
        """
        Iterate over a workbook
        """
        self._current_ws = self._book[worksheet]
        for row in self._current_ws.values:
            yield row

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _check_worksheets(self):
        """
        Make sure the expected worksheets are present
        """
        worksheets = self._book.sheetnames
        # pp.pprint(worksheets)
        for i in self.__class__.EXPECTED_WORKSHEETS:
            if i not in worksheets:
                raise ValueError("missing expected worksheet: '{0}'".format(i))


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class SctReader(fd.SctDef, br.BaseFileReader):
    """
    A file iterator for the Snomed CT codes file
    """
    pass


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Ctv3Reader(fd.Ctv3Def, br.BaseFileReader):
    """
    A file iterator for the Snomed CT codes file
    """
    pass


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class RctReader(fd.RctDef, br.BaseFileReader):
    """
    A file iterator for the Snomed CT codes file
    """
    pass


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Icd10Reader(fd.Icd10Def, br.BaseFileReader):
    """
    A file iterator for the Snomed CT codes file
    """
    pass


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Opcs4Reader(fd.Opcs4Def, br.BaseFileReader):
    """
    A file iterator for the Snomed CT codes file
    """
    pass


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class ManualIncludesReader(fd.ManualIncludesDef, br.BaseFileReader):
    """
    A reader for a manual includes file
    """
    pass


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class ManualExcludesReader(fd.ManualExcludesDef, br.BaseFileReader):
    """
    A reader for a manual excludes file
    """
    pass


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class UkbbCodePoolReader(fd.UkbbCodePoolDef, br.BaseFileReader):
    """
    A reader for a ukbb code pool file
    """
    pass


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Opcs3Reader(fd.Opcs3Def, br.BaseFileReader):
    """
    A file iterator for the OPCS3 codes file
    """
    pass


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Opcs4UkbbReader(fd.Opcs4UkbbDef, br.BaseFileReader):
    """
    A file iterator for the OPCS4 codes file from the UKBB
    """
    pass


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Icd9Reader(fd.Icd9Def, br.BaseFileReader):
    """
    A file iterator for the ICD9 codes file
    """
    pass


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Icd10UkbbReader(fd.Icd10UkbbDef, br.BaseFileReader):
    """
    A file iterator for the ICD10 codes file from UKBB
    """
    pass


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class RctUkbbReader(fd.RctUkbbDef, br.BaseFileReader):
    """
    A file iterator for the read version 2 codes file from UKBB
    """
    pass


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class RctDrugsUkbbReader(fd.RctDrugsUkbbDef, br.BaseFileReader):
    """
    A file iterator for the read version 2 drug codes file from UKBB
    """
    pass


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Ctv3UkbbReader(fd.Ctv3UkbbDef, br.BaseFileReader):
    """
    A file iterator for the read CTV3 codes file from UKBB
    """
    pass


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Icd9Icd10MapReader(fd.Icd9Icd10MapDef, br.BaseFileReader):
    """
    A reader for the ICD9 to ICD10 mapping file from UKBB
    """
    pass


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Ctv3Icd10MapReader(fd.Ctv3Icd10MapDef, br.BaseFileReader):
    """
    A reader for the CTV3 to ICD10 mapping file from UKBB
    """
    pass


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Ctv3Icd9MapReader(fd.Ctv3Icd9MapDef, br.BaseFileReader):
    """
    A reader for the CTV3 to ICD9 mapping file from UKBB
    """
    pass


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Ctv3Opcs4MapReader(fd.Ctv3Opcs4MapDef, br.BaseFileReader):
    """
    A reader for the CTV3 to OPCS4 mapping file from UKBB
    """
    pass


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def load_includes_file(file_path, cache=None, **kwargs):
    """
    load a manual mapping file into the existing cache of manual includes

    Parameters
    ----------
    file_path : str
        The path to the manual includes file
    cache : set or NoneType, optional, default: NoneType
        Any existing includes to add the file contents to if NoneType then
        a set is created internally
    **kwargs
        Any keyword arguments to the base reader such CSV kwargs and encoding

    Returns
    -------
    cache : set
        Any existing includes with the include definitions added from the
        manual includes file
    """
    if cache is None:
        cache = set()
    with ManualIncludesReader(file_path, **kwargs) as infile:
        for row in infile:
            cache.update(
                ManualIncludesReader.get_mapping(row)
            )
    return cache


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def load_excludes_file(file_path, cache=None, **kwargs):
    """
    load a manual mapping excludes file into the existing cache of manual
    excludes

    Parameters
    ----------
    file_path : str
        The path to the manual excludes file
    cache : dict or NoneType, optional, default: NoneType
        Any existing excludes to add the file contents to if NoneType then
        a dict is created internally
    **kwargs
        Any keyword arguments to the base reader such CSV kwargs and encoding

    Returns
    -------
    cache : dict
        Any existing excludes with the exclude definitions added from the
        manual excludes file
    """
    if cache is None:
        cache = dict()

    with ManualExcludesReader(file_path, **kwargs) as infile:
        for row in infile:
            for exclude in ManualExcludesReader.get_mapping(row):
                try:
                    cache[exclude[0]]
                except KeyError:
                    cache[exclude[0]] = set([exclude[1]])
    return cache
