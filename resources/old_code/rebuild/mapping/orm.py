"""
Database classes for the UKBB mapper database
"""

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Boolean, Column, Integer, String, Text, Float, \
    SmallInteger, ForeignKey, Index
from sqlalchemy.orm import relationship
from pyaddons import sqlalchemy_helper

Base = declarative_base()


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class CodingSystem(Base):
    """
    A representation of the coding_systems table
    """
    __tablename__ = 'coding_systems'

    coding_system_id = Column(
        Integer,
        nullable=False,
        primary_key=True,
        autoincrement=True
    )
    coding_system_name = Column(
        String(15)
    )

    # -------------------------------------------------------------------------
    concepts = relationship(
        "Concept",
        back_populates='coding_systems'
    )
    s_mappings = relationship(
        "Mapping",
        back_populates='s_coding_system',
        primaryjoin='CodingSystem.coding_system_id == Mapping.s_coding_system_id'
    )
    t_mappings = relationship(
        "Mapping",
        back_populates='t_coding_system',
        primaryjoin='CodingSystem.coding_system_id == Mapping.t_coding_system_id'
    )
    # -------------------------------------------------------------------------

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return sqlalchemy_helper.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Concept(Base):
    """
    A representation of the coding_systems table
    """
    __tablename__ = 'concepts'

    concept_id = Column(
        Integer,
        nullable=False,
        primary_key=True,
        autoincrement=True
    )
    cui = Column(
        String(255),
        nullable=False,
        index=True
    )
    coding_system_id = Column(
        Integer,
        ForeignKey('coding_systems.coding_system_id'),
        nullable=False
    )
    display_term = Column(
        String(255)
    )

    # -------------------------------------------------------------------------
    coding_systems = relationship(
        "CodingSystem",
        back_populates='concepts'
    )
    terms = relationship(
        "Term",
        back_populates='concept'
    )
    s_mappings = relationship(
        "Mapping",
        back_populates='s_concept',
        primaryjoin='Concept.concept_id == Mapping.s_concept_id'

    )
    t_mappings = relationship(
        "Mapping",
        back_populates='t_concept',
        primaryjoin='Concept.concept_id == Mapping.t_concept_id'
    )
    # -------------------------------------------------------------------------

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return sqlalchemy_helper.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class StatusCode(Base):
    """
    A representation of the status_codes table
    """
    __tablename__ = 'status_codes'

    status_code_id = Column(
        Integer,
        nullable=False,
        primary_key=True,
        autoincrement=True
    )
    status_code = Column(
        String(2),
        nullable=False
    )
    status_code_desc = Column(
        String(75),
        nullable=False
    )

    # -------------------------------------------------------------------------
    terms = relationship(
        "Term",
        back_populates='status_code'
    )
    # -------------------------------------------------------------------------

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return sqlalchemy_helper.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class CodeType(Base):
    """
    A representation of the code_types table
    """
    __tablename__ = 'code_types'

    code_type_id = Column(
        Integer,
        nullable=False,
        primary_key=True,
        autoincrement=True
    )
    code_type = Column(
        String(1),
        nullable=False
    )
    code_type_desc = Column(
        String(75),
        nullable=False
    )

    # -------------------------------------------------------------------------
    terms = relationship(
        "Term",
        back_populates='code_type'
    )
    # -------------------------------------------------------------------------

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return sqlalchemy_helper.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Term(Base):
    """
    A representation of the coding_systems table
    """
    __tablename__ = 'terms'

    term_id = Column(
        Integer,
        nullable=False,
        primary_key=True,
        autoincrement=True
    )
    concept_id = Column(
        Integer,
        ForeignKey('concepts.concept_id'),
        nullable=False,
        index=True
    )
    status_code_id = Column(
        Integer,
        ForeignKey('status_codes.status_code_id'),
        nullable=False,
    )
    code_type_id = Column(
        Integer,
        ForeignKey('code_types.code_type_id'),
        nullable=False,
    )
    tui = Column(
        String(255),
        nullable=False,
        index=True
    )
    term = Column(
        String(255),
        nullable=False,
        index=True
    )

    # -------------------------------------------------------------------------
    status_code = relationship(
        "StatusCode",
        back_populates='terms'
    )
    code_type = relationship(
        "CodeType",
        back_populates='terms'
    )
    concept = relationship(
        "Concept",
        back_populates='terms'
    )
    # -------------------------------------------------------------------------

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return sqlalchemy_helper.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class MappingType(Base):
    """
    A representation of the mapping_types table 
    """
    __tablename__ = 'mapping_types'

    mapping_type_id = Column(
        Integer,
        nullable=False,
        primary_key=True,
        autoincrement=True
    )
    mapping_type = Column(
        String(1),
        nullable=False,
        unique=True
    )
    mapping_type_desc = Column(
        String(50)
    )
    # -------------------------------------------------------------------------
    mappings = relationship(
        "Mapping",
        back_populates='mapping_type'
    )
    # -------------------------------------------------------------------------


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Mapping(Base):
    """
    A representation of the mapping_types table 
    """
    __tablename__ = 'mappings'

    mapping_id = Column(
        Integer,
        nullable=False,
        primary_key=True,
        autoincrement=True
    )
    s_concept_id = Column(
        Integer,
        ForeignKey('concepts.concept_id'),
        index=True
    )
    s_coding_system_id = Column(
        Integer,
        ForeignKey('coding_systems.coding_system_id')
    )
    t_concept_id = Column(
        Integer,
        ForeignKey('concepts.concept_id'),
        index=True
    )
    t_coding_system_id = Column(
        Integer,
        ForeignKey('coding_systems.coding_system_id'),
        nullable=False
    )
    mapping_type_id = Column(
        Integer,
        ForeignKey('mapping_types.mapping_type_id'),
    )

    # -------------------------------------------------------------------------
    mapping_type = relationship(
        "MappingType",
        back_populates='mappings'
    )
    s_concept = relationship(
        "Concept",
        back_populates='s_mappings',
        foreign_keys=[s_concept_id]
    )
    t_concept = relationship(
        "Concept",
        back_populates='t_mappings',
        foreign_keys=[t_concept_id]
    )
    s_coding_system = relationship(
        "CodingSystem",
        back_populates='s_mappings',
        foreign_keys=[s_coding_system_id]
    )
    t_coding_system = relationship(
        "CodingSystem",
        back_populates='t_mappings',
        foreign_keys=[t_coding_system_id]
    )
    # -------------------------------------------------------------------------


# # @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# class BnfLkp(Base):
#     """
#     A representation of the `bnf_lkp` table
#     """
#     __tablename__ = 'bnf_lkp'

#     bnf_lkp_id = Column(Integer, nullable=False, primary_key=True,
#                         autoincrement=True)
#     bnf_presentation_code = Column(String(255), index=True)
#     bnf_presentation = Column(String(100))
#     bnf_product = Column(String(50), index=True)
#     bnf_chemical_substance = Column(String(50), index=True)
#     bnf_subparagraph = Column(String(50))
#     bnf_paragraph = Column(String(50))
#     bnf_section = Column(String(50))
#     bnf_chapter = Column(String(50))

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     def __repr__(self):
#         return sqlalchemy_helper.print_orm_obj(self)


# # @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# class DmdLkp(Base):
#     """
#     A representation of the `dmd_lkp` table
#     """
#     __tablename__ = 'dmd_lkp'

#     dmd_lkp_id = Column(Integer, nullable=False, primary_key=True,
#                         autoincrement=True)
#     concept_id = Column(String(500), index=True)
#     term = Column(String(500), index=True)

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     def __repr__(self):
#         return sqlalchemy_helper.print_orm_obj(self)


# # @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# class Icd9Lkp(Base):
#     """
#     A representation of the `icd9_lkp` table
#     """
#     __tablename__ = 'icd9_lkp'

#     icd9_lkp_id = Column(Integer, nullable=False, primary_key=True,
#                          autoincrement=True)
#     icd_v9_code = Column(String(100), index=True)
#     icd_v9_desc = Column(String(255), index=True)

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     def __repr__(self):
#         return sqlalchemy_helper.print_orm_obj(self)


# # @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# class Icd10Lkp(Base):
#     """
#     A representation of the `icd10_lkp` table
#     """
#     __tablename__ = 'icd10_lkp'

#     icd10_lkp_id = Column(Integer, nullable=False, primary_key=True,
#                           autoincrement=True)
#     icd_v10_code = Column(String(255), index=True)
#     alt_code = Column(String(10), index=True)
#     usage = Column(String(10))
#     usage_uk = Column(SmallInteger)
#     description = Column(String(150), index=True)
#     modifier_4 = Column(String(100))
#     modifier_5 = Column(String(100))
#     qualifiers = Column(String(10))
#     gender_mask = Column(Integer, default=0)
#     min_age = Column(Integer, default=0)
#     max_age = Column(Integer, default=0)
#     tree_description = Column(String(100))

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     def __repr__(self):
#         return sqlalchemy_helper.print_orm_obj(self)


# # @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# class Icd9Icd10(Base):
#     """
#     A representation of the `icd9_icd10` table
#     """
#     __tablename__ = 'icd9_icd10'

#     icd9_icd10_id = Column(Integer, nullable=False, primary_key=True,
#                            autoincrement=True)
#     icd_v9_code = Column(String(255), index=True)
#     icd_v9_desc = Column(String(100), index=True)
#     icd_v10_code = Column(String(10), index=True)
#     icd_v10_desc = Column(String(100), index=True)

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     def __repr__(self):
#         return sqlalchemy_helper.print_orm_obj(self)


# # @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# class ReadV2Lkp(Base):
#     """
#     A representation of the `read_v2_lkp` table
#     """
#     __tablename__ = 'read_v2_lkp'

#     readv2_lkp_id = Column(Integer, nullable=False, primary_key=True,
#                            autoincrement=True)
#     read_v2_code = Column(String(255), index=True)
#     term_v2_code = Column(String(10))
#     term_v2_desc = Column(String(255), index=True)

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     def __repr__(self):
#         return sqlalchemy_helper.print_orm_obj(self)


# # @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# class ReadV2DrugsLkp(Base):
#     """
#     A representation of the `read_v2_drugs_lkp` table
#     """
#     __tablename__ = 'read_v2_drugs_lkp'

#     read_v2_drugs_lkp_id = Column(Integer, nullable=False, primary_key=True,
#                                   autoincrement=True)
#     read_v2_code = Column(String(255), index=True)
#     term_v2_desc = Column(String(255), index=True)
#     status_flag = Column(SmallInteger)

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     def __repr__(self):
#         return sqlalchemy_helper.print_orm_obj(self)


# # @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# class ReadV2DrugsBnf(Base):
#     """
#     A representation of the `read_v2_drugs_bnf` table
#     """
#     __tablename__ = 'read_v2_drugs_bnf'

#     read_v2_drugs_bnf_id = Column(Integer, nullable=False, primary_key=True,
#                                   autoincrement=True)
#     read_v2_code = Column(String(50), index=True)
#     bnf_code = Column(String(255), index=True)

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     def __repr__(self):
#         return sqlalchemy_helper.print_orm_obj(self)


# # @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# class ReadV2Icd9(Base):
#     """
#     A representation of the `read_v2_icd9` table
#     """
#     __tablename__ = 'read_v2_icd9'

#     read_v2_icd9_id = Column(Integer, nullable=False, primary_key=True,
#                              autoincrement=True)
#     read_v2_code = Column(String(255), index=True)
#     icd_v9_code = Column(String(50), index=True)
#     icd_v9_def = Column(SmallInteger)

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     def __repr__(self):
#         return sqlalchemy_helper.print_orm_obj(self)


# # @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# class ReadV2Icd10(Base):
#     """
#     A representation of the `read_v2_icd10` table
#     """
#     __tablename__ = 'read_v2_icd10'

#     read_v2_icd10_id = Column(Integer, nullable=False, primary_key=True,
#                               autoincrement=True)
#     read_v2_code = Column(String(255), index=True)
#     icd_v10_code = Column(String(50), index=True)
#     icd_v10_def = Column(SmallInteger)

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     def __repr__(self):
#         return sqlalchemy_helper.print_orm_obj(self)


# # @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# class ReadV2Opcs4(Base):
#     """
#     A representation of the `read_v2_opcs4` table
#     """
#     __tablename__ = 'read_v2_opcs4'

#     read_v2_opcs4_id = Column(Integer, nullable=False, primary_key=True,
#                               autoincrement=True)
#     read_v2_code = Column(String(255), index=True)
#     opcs_v4_code = Column(String(255), index=True)
#     opcs_v4_def = Column(SmallInteger)

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     def __repr__(self):
#         return sqlalchemy_helper.print_orm_obj(self)


# # @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# class ReadV2ReadCtv3(Base):
#     """
#     A representation of the `read_v2_read_ctv3` table
#     """
#     __tablename__ = 'read_v2_read_ctv3'

#     read_v2_read_ctv3_id = Column(Integer, nullable=False, primary_key=True,
#                                   autoincrement=True)
#     chapter = Column(String(255))
#     read_v2_code = Column(String(10), index=True)
#     read_v2_desc = Column(String(255), index=True)
#     term_v2_desc = Column(String(255), index=True)
#     term_v2_order = Column(String(10))
#     term_v2_type = Column(String(10))
#     read_ctv3_code = Column(String(10), index=True)
#     term_v3_code = Column(String(10), index=True)
#     term_v3_type = Column(String(10))
#     term_v3_desc = Column(String(255), index=True)
#     is_assured = Column(SmallInteger)

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     def __repr__(self):
#         return sqlalchemy_helper.print_orm_obj(self)


# # @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# class ReadCtv3Lkp(Base):
#     """
#     A representation of the `read_ctv3_lkp` table
#     """
#     __tablename__ = 'read_ctv3_lkp'

#     read_ctv3_lkp_id = Column(Integer, nullable=False, primary_key=True,
#                               autoincrement=True)
#     read_ctv3_code = Column(String(255), index=True)
#     term_ctv3_desc = Column(String(255), index=True)
#     desc_type = Column(String(10))
#     status_flag = Column(String(10))

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     def __repr__(self):
#         return sqlalchemy_helper.print_orm_obj(self)


# # @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# class ReadCtv3Icd9(Base):
#     """
#     A representation of the `read_ctv3_icd9` table
#     """
#     __tablename__ = 'read_ctv3_icd9'

#     read_ctv3_icd9_id = Column(Integer, nullable=False, primary_key=True,
#                                autoincrement=True)
#     read_ctv3_code = Column(String(255), index=True)
#     icd_v9_code = Column(String(20), index=True)
#     mapping_status = Column(String(10))
#     refine_flag = Column(String(10))
#     add_code_flag = Column(String(10))
#     element_number = Column(Integer)
#     block_number = Column(Integer)

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     def __repr__(self):
#         return sqlalchemy_helper.print_orm_obj(self)


# # @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# class ReadCtv3Icd10(Base):
#     """
#     A representation of the `read_ctv3_icd10` table
#     """
#     __tablename__ = 'read_ctv3_icd10'

#     read_ctv3_icd10_id = Column(Integer, nullable=False, primary_key=True,
#                                 autoincrement=True)
#     read_ctv3_code = Column(String(255), index=True)
#     icd_v10_code = Column(String(20), index=True)
#     mapping_status = Column(String(10))
#     refine_flag = Column(String(10))
#     add_code_flag = Column(String(10))
#     element_number = Column(Integer)
#     block_number = Column(Integer)

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     def __repr__(self):
#         return sqlalchemy_helper.print_orm_obj(self)


# # @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# class ReadCtv3Opcs4(Base):
#     """
#     A representation of the `read_ctv3_opcs4` table
#     """
#     __tablename__ = 'read_ctv3_opcs4'

#     read_ctv3_opcs4_id = Column(Integer, nullable=False, primary_key=True,
#                                 autoincrement=True)
#     read_ctv3_code = Column(String(500), index=True)
#     opcs_v4_code = Column(String(10), index=True)
#     mapping_status = Column(String(10))
#     refine_flag = Column(String(10))
#     add_code_flag = Column(String(10))
#     element_number = Column(Integer)
#     block_number = Column(Integer)

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     def __repr__(self):
#         return sqlalchemy_helper.print_orm_obj(self)


# # @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# class ReadCtv3ReadV2(Base):
#     """
#     A representation of the `read_ctv3_read_v2` table
#     """
#     __tablename__ = 'read_ctv3_read_v2'

#     read_ctv3_read_v2_id = Column(Integer, nullable=False, primary_key=True,
#                                   autoincrement=True)
#     read_ctv3_code = Column(String(255), index=True)
#     term_v3_code = Column(String(10), index=True)
#     term_v3_type = Column(String(10))
#     term_v3_desc = Column(String(255), index=True)
#     read_v2_code = Column(String(10), index=True)
#     read_v2_desc = Column(String(255), index=True)
#     term_v2_order = Column(String(10))
#     term_v2_type = Column(String(10))
#     term_v2_desc = Column(String(255), index=True)
#     is_assured = Column(SmallInteger)

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     def __repr__(self):
#         return sqlalchemy_helper.print_orm_obj(self)


# # @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# class OpcsV3Lkp(Base):
#     """
#     A representation of the `opcs_v3_lkp` table
#     """
#     __tablename__ = 'opcs_v3_lkp'

#     opcs_v3_lkp_id = Column(Integer, nullable=False, primary_key=True,
#                             autoincrement=True)
#     opcs_v3_code = Column(String(10), index=True)
#     opcs_v3_desc = Column(String(255), index=True)
#     node_id = Column(Integer)
#     parent_id = Column(Integer)
#     selectable = Column(String(10))

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     def __repr__(self):
#         return sqlalchemy_helper.print_orm_obj(self)


# # @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# class OpcsV4Lkp(Base):
#     """
#     A representation of the `opcs_v4_lkp` table
#     """
#     __tablename__ = 'opcs_v4_lkp'

#     opcs_v4_lkp_id = Column(Integer, nullable=False, primary_key=True,
#                             autoincrement=True)
#     opcs_v4_code = Column(String(10), index=True)
#     opcs_v4_desc = Column(String(255), index=True)
#     node_id = Column(Integer)
#     parent_id = Column(Integer)
#     selectable = Column(String(10))

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     def __repr__(self):
#         return sqlalchemy_helper.print_orm_obj(self)


# # @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# class UkbbDataDict(Base):
#     """
#     A representation of the `ukbb_data_dict` table
#     """
#     __tablename__ = 'ukbb_data_dict'

#     ukbb_data_dict_id = Column(Integer, nullable=False, primary_key=True,
#                                autoincrement=True)
#     data_path = Column(Text)
#     category_id = Column(Integer, index=True)
#     field_id = Column(Integer, index=True)
#     field_desc = Column(Text)
#     n_samples = Column(Integer)
#     n_items = Column(Integer)
#     stability = Column(String(255))
#     data_type = Column(String(255))
#     data_units = Column(String(255))
#     item_type = Column(String(255))
#     strata = Column(String(255))
#     sexed = Column(String(255))
#     n_instances = Column(Integer)
#     array = Column(Integer)
#     coding_id = Column(Integer)
#     notes = Column(Text)
#     link = Column(String(255))

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     def __repr__(self):
#         return sqlalchemy_helper.print_orm_obj(self)


# # @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# class UkbbDataEnumCodes(Base):
#     """
#     A representation of the `ukbb_data_dict` table
#     """
#     __tablename__ = 'ukbb_data_enum_codes'

#     ukbb_data_enum_codes_id = Column(Integer, nullable=False, primary_key=True,
#                                      autoincrement=True)
#     coding_id = Column(Integer, index=True)
#     code_enum = Column(String(255), index=True)
#     desc = Column(Text, index=True)

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     def __repr__(self):
#         return sqlalchemy_helper.print_orm_obj(self)
