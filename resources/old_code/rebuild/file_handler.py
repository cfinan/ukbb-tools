"""
Classes for handing the common UKBB file types
"""
from pyaddons import gzopen, file_helper, file_index
from simple_progress import progress
import os
import sys
import csv
import warnings
import pprint as pp


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def extract_eid_from_bgen_sample_file(bgen_sample_file, **csv_kwargs):
    """
    Extract all the eids (UKBB sampl IDs) from a BGEN style samples file. I
    could (and maybe should) just go direct to the BGEN file to get this info

    Parameters
    ----------
    bgen_sample_file : str
        The path to the sample file
    **csv_kwargs
        Any csv.reader keyword arguments. If none are supplied then the dialect
        if sniffed

    Yields
    ------
    eid : str
        The sample ID
    """
    if len(csv_kwargs) == 0:
        csv_kwargs['dialect'] = sniff_delimiter(bgen_sample_file)

    with open(bgen_sample_file) as sample_in:
        reader = csv.reader(sample_in, **csv_kwargs)

        # Skip the two header rows
        next(reader)
        next(reader)

        for row in reader:
            eid = row[0]

            # The eid should be a digit
            try:
                eid = int(eid)
            except TypeError as e:
                raise ValueError("does not look like a eid: "
                                 "'{0}'".format(eid)) from e

            yield eid


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_header_names(header):
    """
    Extract the column names from ColName named tuples

    Parameters
    ----------
    header : list of :obj:`ColName`
        A list of `ColName` named tuples that contain the header information

    Returns
    -------
    col_names : list of str
        The extracted column names
    """
    return [i.name for i in header]


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_clinical_code_idx(header, clinical_code_columns):
    """
    Find the column index of the clinical code column

    Parameters
    ----------
    header : list of str
        A list column name strings
    clinical_code_columns : list of tuple
        Each tuple has the structure of `[0]`:`ColName` and
        `[1]`:`output_name`, where output name is the clinical code type that
        will be used in the output files

    Returns
    -------
    clinical_code_columns : list of tuple
        Each tuple has the structure of `[0]`: column name string
        `[1]` column name index `[2]`:`output_name`, where output name is the
        clinical code type that will be used in the output files
    """
    clinical_code_idx = []
    for column, outname in clinical_code_columns:
        clinical_code_idx.append((column.name,
                                  header.index(column.name),
                                  outname))
    return clinical_code_idx


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def sample_union(*args):
    """
    """
    samples = set(args[0].values)
    for i in range(1, len(args)):
        samples = samples.union(args[i].values)
    return list(samples)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def sample_intersection(*args):
    """
    """
    samples = set(args[0].values)
    for i in range(1, len(args)):
        samples = samples.intersection(args[i].values)
    return list(samples)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def read_clinical_codes_file(code_file, verbose=False):
    """
    A temporary function to read the UKBB codes file during development, this
    is so I do not have to map all the codes on each run while I test the
    latter stages of the build process

    Parameters
    ----------
    code_file : str
        The path to the code file that will be read in. It is assumed that the
        code file will be compressed, tab delimited and has a single line
        header

    Returns
    -------
    code_mappings : dict
        The clinical code mappings, this is in a similar format to what
        is extracted by the clinical code mapper
    """
    casts = {
        'code_id': int,
        'min_n_values': int,
        'max_n_values': int,
        'n_occurs': int,
        'n_is_empty': int,
        'n_is_string': int,
        'n_is_int': int,
        'n_is_float': int,
    }

    ratios = [(3, 0, 0), (0, 3, 0), (0, 0, 3), (2, 1, 0), (2, 0, 1), (1, 2, 0),
              (0, 2, 1), (1, 0, 2), (0, 1, 2), (1, 1, 1)]
    ratios = [(i, '.'.join([str(j) for j in i])) for i in ratios]

    with gzopen.gzip_fh(code_file) as infile:
        reader = csv.reader(infile, delimiter=ukbbc.GLOBAL_OUT_DELIMITER)
        header = next(reader)
        prog = progress.RateProgress(verbose=verbose, file=sys.stderr,
                                     default_msg="reading codes file")

        codes = {}
        for row in prog.progress(reader):
            row = file_index.make_none_type(row)
            row = dict([(header[i], row[i]) for i in range(len(header))])
            code_key = (row['ukbb_sab'], row['ukbb_code'])

            for col, cast_func in casts.items():
                row[col] = cast_func(row[col])

            row['ratios'] = \
                dict(
                    [(key, int(row[str_key]))
                     for key, str_key in ratios]
                )
            cui_mapping = (
                row['umls_cui'],
                row['umls_sab'],
                row['umls_term'],
                row['umls_sty']
            )

            ukbb_entries = []
            if row['ukbb_terms'] is not None:
                ukbb_entries = row['ukbb_terms'].split('|')

            try:
                # First attempt to append to an existing entry
                # so multiple CUIs can be associated with a mapping
                # codes[code_key]['ukbb'] = ukbb_entries
                codes[code_key]['cuis'].append(cui_mapping)
            except KeyError:
                # This is the first occurrence
                row['ukbb'] = ukbb_entries
                row['cuis'] = [cui_mapping]
                codes[code_key] = row
    return codes


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def read_samples_file(sample_file, verbose=False):
    """
    Slurp the contents of a processed samples file into memory. This has mainly
    been used during development when running things bit by bit. However, there
    is no reason why it can't be used properly.

    Parameters
    ----------
    sample_file : str
        The path to the processed samples file

    Returns
    -------
    sample_set : dict
        All the processed samples in memory. The keys of the dict are eids
        (ints) and the values are further dicts with the header column names
        as keys and the row/column values as values. Dates are converted into
        `DateHolder` `namedtuples`
    """
    casts = {
        ukbbc.EID.name: int,
        ukbbc.SAM_HAS_WITHDRAWN.name: parse_bool,
        ukbbc.SAM_IS_GENOTYPED.name: parse_bool
    }
    opt_casts = {
        ukbbc.SAM_DATE_OF_BIRTH_INT.name: int,
        ukbbc.SAM_DATE_OF_ASSESS_INT.name: int,
        ukbbc.SAM_SR_SEX.name: int,
        ukbbc.SAM_N_ASSESSMENTS.name: int,
        ukbbc.SAM_GEN_SEX.name: int,
        ukbbc.SAM_DATE_OF_DEATH_INT.name: int,
        ukbbc.SAM_AGE_OF_ASSESS.name: float,
        ukbbc.SAM_AGE_OF_DEATH.name: float,
        ukbbc.SAM_DATE_OF_BIRTH.name: parse_global_date,
        ukbbc.SAM_DATE_OF_ASSESS.name: parse_global_date,
        ukbbc.SAM_DATE_OF_DEATH.name: parse_global_date,
        ukbbc.SAM_IS_CAUCASIAN.name: parse_bool
    }

    sample_set = {}
    with gzopen.gzip_fh(sample_file) as infile:
        reader = csv.reader(infile, delimiter=ukbbc.GLOBAL_OUT_DELIMITER)
        header = next(reader)
        prog = progress.RateProgress(verbose=verbose, file=sys.stderr,
                                     default_msg="reading sample file")

        for row in prog.progress(reader):
            row = file_index.make_none_type(row)
            row = dict([(header[i], row[i]) for i in range(len(header))])

            for col, cast_func in casts.items():
                row[col] = cast_func(row[col])

            for col, cast_func in opt_casts.items():
                try:
                    row[col] = cast_func(row[col])
                except TypeError:
                    pass

            sample_set[row[ukbbc.EID.name]] = row

    return sample_set




# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# TODO: defunct
class SamplesFile(UkbbCodesIndexReader):
    """
    A class representing the processed samples file
    """
    DATE_FORMATS = [ukbbc.GLOBAL_DATE_FORMAT]
    HEADER = get_header_names(ukbbc.SAMPLE_FILE_HEADER)

    INT_COLS = [ukbbc.EID]

    INT_COLS_OPT = [ukbbc.SAM_DATE_OF_BIRTH_INT,
                    ukbbc.SAM_DATE_OF_ASSESS_INT,
                    ukbbc.SAM_SR_SEX,
                    ukbbc.SAM_N_ASSESSMENTS,
                    ukbbc.SAM_GEN_SEX,
                    ukbbc.SAM_DATE_OF_DEATH_INT]

    FLOAT_COLS_OPT = [ukbbc.SAM_AGE_OF_ASSESS,
                      ukbbc.SAM_AGE_OF_DEATH]

    DATE_COLS_OPT = [ukbbc.SAM_DATE_OF_BIRTH,
                     ukbbc.SAM_DATE_OF_ASSESS,
                     ukbbc.SAM_DATE_OF_DEATH]

    BOOL_COLS = [ukbbc.SAM_HAS_WITHDRAWN,
                 ukbbc.SAM_IS_GENOTYPED]
    BOOL_COLS_OPT = [ukbbc.SAM_IS_CAUCASIAN]


