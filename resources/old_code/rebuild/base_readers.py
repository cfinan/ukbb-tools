"""
.. include:: ../docs/base_readers.md
"""
from ukbb_tools import common
from pyaddons import file_index, gzopen
import os
import csv
import pprint as pp


# # @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# class ClinCodeExtractBase(object):
#     """
#     """
#     CLINICAL_CODES = []

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     @classmethod
#     def get_clinical_code(cls, row):
#         """
#         Extract the clinical code

#         Parameters
#         ----------
#         row : list or dict
#             A single data row to extract all the clinical codes from
#         """
#         codes = {}
#         values = []
#         for key, ret_key in cls.CLINICAL_CODES:
#             # first treat as a dict
#             try:
#                 code = row[key]
#                 len(code)
#                 codes[ret_key] = code
#             except (KeyError, TypeError):
#                 # The code is NoneType so we skip
#                 pass

#         return codes, values


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class UkbbIndexReader(file_index.TypedIndexReader):
    """
    The base class for UKBB file handling - do not call directly
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def values(self):
        """
        Return the a list of values in the index
        """
        return [int(i) for i in super().values]

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def ordered_values(self):
        """
        Return the a list of values in the index
        """
        return sorted(self.values)


# # @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# class UkbbCodesIndexReader(UkbbIndexReader, ClinCodeExtractBase):
#     """
#     The base class for UKBB file handling - do not call directly
#     """
#     pass


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class BaseFileReader(object):
    """
    An index reader that will return fetched rows as dictionaries where the
    keys are column names and the values are formatted to the correct type
    base defined by the user. This should be sub-classed and the subclass
    should define column name -> datatype mappings
    """
    NONE_VALUES = ['']
    EXPECTED_HEADER = []

    # Keys should be column names and the values should be cast functions. If
    # these casts fail then an error will be raised
    CASTS = {}

    # Keys should be column names and the values should be optional cast
    # functions. If these casts fail then an error will not be raised
    OPT_CASTS = {}

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, infile, header_rows=1, encoding='utf8',
                 sniff_bytes=2000000, **csvkwargs):
        """
        Parameters
        ----------
        infile : str
            The input file path
        return_dict : bool, optional, default: False
            Should returned rows be retuned as a dictionary
        delimiter : str, optional, default: NoneType
            The delimiter of the file, if NoneType then the delimiter is
            detected with `csv.Sniffer`. However, it can be specifed should
            sniffing not work
        """
        self._infile = os.path.expanduser(infile)
        self._encoding = encoding
        self._header_rows = header_rows
        self._sniff_bytes = sniff_bytes
        self._csv_kwargs = csvkwargs
        self._is_open = False
        self._header = []
        self._line_no = 0

        if self._header_rows < 1:
            raise ValueError("must have at least 1 header row")

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __enter__(self):
        """
        Entry point for the context manager
        """
        self.open()
        self._using_context_manager = True
        return self

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __exit__(self, *args):
        """
        Entry point for the context manager

        *args
            Error arguments should the context manager exit in discrace
        """
        self.close()
        self._using_context_manager = False

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __iter__(self):
        """
        Initialise the iterator
        """
        # make sure we are at the start
        self._is_iterating = True
        return self

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __next__(self):
        """
        The next row in the file
        """
        self._is_iterating = True
        return self._next_row()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def header(self):
        """
        Return the header row, this is the last row in all the header rows
        """
        return self._header[-1]

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def open(self):
        """
        Open the file. Note that this is agnostic for gzipped files
        """
        if self._is_open is True:
            raise IOError('file already open')

        csv_args = {}
        self._fobj = None
        self._csv_kwargs = common.sniff_delimiter(
            self._infile,
            encoding=self._encoding,
            **self._csv_kwargs
        )

        self._fobj = gzopen.gzip_agnostic(
            self._infile,
            encoding=self._encoding
        )
        self._reader = csv.reader(self._fobj, **self._csv_kwargs)

        try:
            self._process_header()
        except StopIteration as e:
            raise StopIteration(
                "can't process header, this probably means the file is empty"
            ) from e

        self._is_open = True

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def reset(self):
        """
        Reset the file to the beginning and reprocess the header
        """
        if self._is_open is False:
            raise IOError('file not open')

        self._fobj.seek(0)
        self._line_no = 0
        self._process_header()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def close(self):
        """
        Close the file
        """
        if self._is_open is False and self._using_context_manager is False:
            raise IOError('file not open')

        self._fobj.close()
        self._is_opened = False
        self._fobj = None
        self._reader = None

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _process_header(self):
        """
        Process the header from the file and make sure it is valid

        Raises
        ------
        ValueError
            If the header of the file does not match the expected header
        """
        self._line_no += 1

        self._header = [next(self._reader) for i in range(self._header_rows)]
        self._header_len = len(self.header)

        # pp.pprint(self.__class__.HEADER)
        # pp.pprint(self.header)
        if self.header != self.__class__.HEADER:
            raise ValueError(
                "the header in: '{0}' is not the expected format".format(
                    self._infile))

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _next_row(self):
        """
        return the next row in the file
        """
        self._line_no += 1
        return self._format_row(
            self._row_as_dict(
                common.make_none_type(
                    next(self._reader),
                    none_values=self.__class__.NONE_VALUES
                )
            )
        )
        # while True:
        #     try:
        #         self._line_no += 1
        #         return self._row_process_method(
        #             self._format_row(next(self._reader)))
        #     except (IndexError, UnicodeDecodeError) as e:
        #         warnings.warn("at line '{0}: {1}'".format(self._line_no,
        #                                                   str(e)))

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _row_as_dict(self, row):
        """
        Make the row into the dictionary

        Parameters
        ----------
        row : list of str
            The data row as read in by `csv.Reader`

        Returns
        -------
        row : dict
            The row formatted as a dict, each key is a column name and each
            value is a data value
        """
        try:
            return dict([(col_name, row[idx])
                         for idx, col_name in enumerate(self.header)])
        except IndexError as e:
            raise IndexError(
                "row length != header length: {0} != {1} at line {2}".format(
                    len(row), self._header_len, self._line_no
                )
            ) from e

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _format_row(self, row):
        """
        Format the row. This checks the row length is what is required and then
        will perform any casts or specific data reformating on the row

        Parameters
        ----------
        row : list of str
            The data row to process

        Raises
        ------
        IndexError
            If the length of the data row is not what is expected
        """
        # print(self.__class__)
        for key, cast_func in self.__class__.CASTS.items():
            try:
                row[key] = cast_func(row[key])
            except Exception as e:
                pp.pprint(row)
                print(key)
                raise e.__class__(
                    "can't format data for '{0}' -> '{1}".format(
                        key, cast_func)
                ) from e

        for key, cast_func in self.__class__.OPT_CASTS.items():
            try:
                row[key] = cast_func(row[key])
            except Exception as e:
                # print(e)
                pass

        return row
