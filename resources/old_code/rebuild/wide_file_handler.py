"""Parsers and cmd-line too for the subset of the UK BioBank wide phenotype
file
"""
from ukbb_tools import (
    __version__,
    __name__ as pkg_name,
    constants as con,
    columns as col,
    # data_dict as dd,
    type_casts,
    wide_file_common as wfc,
    wide_file_index as wfi,
    wide_file_extract as wfe
)
from ukbb_tools.data_dict import build as dd
from pyaddons import gzopen
from simple_progress import progress
from contextlib import contextmanager
from datetime import datetime
import builtins
import os
import csv
import sys
import warnings
import argparse
import itertools
import pprint as pp

MSG_PREFIX = '[info]'
MSG_OUT = sys.stderr


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main():
    """
    The main entry point to build an indexed wide file
    """
    # Initialise and parse the command line arguments
    parser = init_cmd_args()
    args = parse_cmd_args(parser)

    # Start a msg outputter, this will respect the verbosity and will output to
    # STDERR
    m = progress.Msg(prefix='', verbose=args.verbose, file=MSG_OUT)
    m.msg("= UKBB wide file subset ({0} v{1}) =".format(
        pkg_name, __version__))
    m.prefix = MSG_PREFIX

    # Make sure the wide file can be opened
    builtins.open(args.wide_file_path).close()

    # Extract any fields or eids. Note that some of them may be in files
    args.eids = _get_eids(args.eids)
    args.fields = _get_fields(args.fields, args.field_file)

    # We have to have something to subset
    if sum([len(args.eids), len(args.fields)]) == 0:
        raise ValueError("you do not want to subset anything? Why not "
                         "then: cp {0} <NEW_FILE>? :-)".format(
                             args.wide_file_path))
    m.msg_atts(args)

    # Perform the subset, this will write directly to STDOUT
    subset_wide_file(
        args.wide_file_path,
        args.eids,
        args.fields,
        args.long
    )
    m.msg("*** END ***")


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def init_cmd_args():
    """
    Initialise the command line arguments and return the parsed arguments

    Returns
    -------
    parser : :obj:`argparse.ArgumentParser`
        The argument parser with the arguments set up
    """
    parser = argparse.ArgumentParser(
        description="Subset samples and columns from a UKBB wide file")

    # The wide format UKBB data fields filex
    parser.add_argument(
        'wide_file_path', type=str,
        help="The path to UKBB wide file"
    )
    parser.add_argument(
        'eids', type=str, nargs="*",
        help="0 or more sample IDs to be extracted. If none are supplied"
        " then it is assumed that all samples are required. A file of sample"
        " IDs can also be supplied"
    )
    parser.add_argument(
        '--fields', type=str, nargs="+",
        help="One or more fields or column IDs to extract from the wide file."
        " Suppying none will extract all of them. A field ID is an integer a"
        " column name is the field ID, instance ID and array number combined "
        "in a string with the structure: <FID>-<INST>.<ARRAY>. Both can be "
        "supplied at the same time. If a field ID is supplied, all columns "
        "matching that ID will be extracted. If any columns are not present, "
        "then they are ignored and a warning is issued"
    )
    parser.add_argument(
        '--field-file', type=str,
        help="A file with field IDs or column names in it, there shoudl be "
        "one per line. This can be used inconjunction with --fields"
    )
    parser.add_argument('-v', '--verbose',  action="store_true",
                        help="Give more output")
    parser.add_argument(
        '-l', '--long',  action="store_true",
        help="Return the results in long format and not wide format. Long "
        "format results are also supplied with dates/times relative to "
        "baseline"
    )

    return parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_cmd_args(parser):
    """
    Parse the command line arguments

    Parameters
    ----------
    parser : :obj:`argparse.ArgumentParser`
        The argument parser with the arguments set up

    Returns
    -------
    args : :obj:`argparse.Namespace`
        The argparse namespace object
    """
    args = parser.parse_args()
    args.fields = args.fields or []

    # Make sure ~/ etc... is expanded
    for i in ['wide_file_path']:
        setattr(args, i, os.path.expanduser(getattr(args, i)))

    return args


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _get_eids(eids):
    """
    Extract all the eids into a list of unique eids. The first EID may well be
    a file of eids in which case to will be read in and added to the list

    Parameters
    ----------
    list of str
        A list of eids where the first eid in the list may well be a file
        containing eids

    Returns
    -------
    eids : list of int
        Any eids that the user has passed
    """
    all_eids = []
    try:
        first_eid = eids.pop(0)
        all_eids = [int(i) for i in _read_list_file(first_eid)]
    except FileNotFoundError:
        # Not an eid
        all_eids.append(int(first_eid))
    except IndexError:
        # pop from an empty list - no eids were provided
        pass
    all_eids.extend(eids)
    return list(set(all_eids))


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _get_fields(field_ids, field_file):
    """
    Extract all the eids into a list of unique eids. The first EID may well be
    a file of eids in which case to will be read in and added to the list

    Parameters
    ----------
    list of str
        A list of eids where the first eid in the list may well be a file
        containing eids

    Returns
    -------
    fields : list of int
        Any field IDs that the user has passed
    columns : list of str
        Any column names that the user has supplied
    """

    try:
        field_ids.extend(_read_list_file(field_file))
    except FileNotFoundError:
        # No file
        pass
    return field_ids


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _read_list_file(filename):
    """
    Attempt to read in a file that has single entries in it, one on each line.
    The file is read into memory

    Parameters
    ----------
    filename : str
        The path to the list file

    Returns
    -------
    list of str
        A list of the contents of each line
    """
    try:
        filename = os.path.expanduser(filename)
    except TypeError as e:
        raise FileNotFoundError("NoneTypes can't be found") from e

    file_rows = []
    with builtins.open(filename) as infile:
        for i in infile:
            file_rows.append(i.strip())

    return file_rows


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~--
def subset_wide_file(wide_file, eids, fields, use_long_format):
    """Perform the subset of the wide file.

    Parameters
    ----------
    wide_file : `str`
        The path to the wide file
    eids : `list` of `int`
        Any sample IDs that need to be extracted
    fields : `list` of `int`
        Any field IDs (groups of colnames) that need to be extracted, or any
        specific column names that need extracting
    use_long_format : `bool`
        Do we want to output the long mode
    """
    mode = 'raw'
    output_format = 'flat'
    if use_long_format is True:
        mode = 'numeric'
        output_format = 'long'

    out_header = True
    with open(wide_file, mode=mode, format=output_format) as infile:
        with sys.stdout as outfile:
            writer = csv.writer(outfile, delimiter="\t")

            if use_long_format is False:
                if out_header is True:
                    writer.writerow([col.EID.name] + infile.subset_header)

                for row in infile.fetch(eids=eids, fields=fields):
                    writer.writerow(row)
                    # out_header = False
            else:
                if out_header is True:
                    writer.writerow(infile.HEADER)

                for row in infile.fetch(eids=eids, fields=fields,
                                        nonetype=False):
                    try:
                        # This is testing if the value is a date field, if so
                        # we are outputting the pretty format date. If not
                        # then we output whatever the value is
                        row[-1] = row[-1].pretty_date
                    except AttributeError:
                        pass

                    writer.writerow(row)
                    # out_header = False


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@contextmanager
def open(wide_file, mode='raw', format='flat', **kwargs):
    """
    Providing an "open" call to wide file parsing

    Parameters
    ----------
    wide_file : str
        The path to the wide file
    mode : str, optional, default: raw
        The parsing mode, `raw`, `numeric`, `values` and `full`
    format : str, optional, default: flat
        The format of the output rows `flat`, `dict`, `nested`, `long`
    **kwargs
        Keyword arguments that can be supplied to `BaseWideFileParser`


    index_file : str or NoneType, optional, default: NoneType
        An alternative location for an index file. `NoneType` means that the
        path of any index files is the same as the wide_file but with `.idx`
        suffix
    connect_uri : :obj:`sqlalchemy.engine.url.URL` or str or NoneType,
    optional, default: NoneType
        Connect to an alternative data dictionary. If NoneType then the
        package data dictionary is used



    eids : list of int
        One or more sample IDs that you want to extract from the file
    fields : NoneType or list of str or int, optional, default: NoneType
        Either field IDs or column names that you want to extract from the
        file. Field IDs should be ints (or castable to ints) and the column
        names should be of the format: <FIELD_ID>-<INSTANCE>.<ARRAY>. Anything
        that does not satisfy these requirments will generate an error.
        NoneType means that you want all columns returned
    nonetype : bool, optional, default: True
        Return missing data columns. The UKBB wide file is sparse due to it's
        structure. Set this to False if you want to remove all missing data
        columns from parsed rows. This is only available in `dict` and `nested`
        formats
    """
    # Code to acquire resource, e.g.:
    parser_class = get_parser(mode, format)
    parser = parser_class(wide_file, **kwargs)

    try:
        # Open the index and supply it
        parser.open()
        yield parser
    except Exception:
        raise
    finally:
        # close the index
        parser.close()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_parser(mode, format):
    """
    Return a parser class based on the mode and the format values

    Parameters
    ----------
    mode : str, optional, default: raw
        The parsing mode, `raw`, `numeric`, `values` and `full`
    format : str, optional, default: flat
        The format of the output rows `flat`, `dict`, `nested`

    Returns
    -------
    parser_class : :obj:`Class`
        A parser class that matches the mode and format values
    """
    mode_class = None
    format_class = None

    if mode == 'raw':
        mode_class = RawMode
    elif mode == 'numeric':
        mode_class = NumericMode
    elif mode == 'full':
        mode_class = FullMode
    else:
        raise ValueError("unknown mode '{0}'".format(mode))

    if format == 'flat':
        format_class = FlatFormatter
    elif format == 'dict':
        format_class = DictFormatter
    elif format == 'nested':
        format_class = NestedFormatter
    elif format == 'long':
        format_class = LongFormatter
    else:
        raise ValueError("unknown format '{0}'".format(format))

    mode = mode.capitalize()
    format = format.capitalize()
    return type(
        '{0}{1}Parser'.format(mode, format),
        (format_class, mode_class, BaseWideFileParser),
        {}
    )


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class BaseWideFileParser(object):
    """
    A base parser class that is designed to be merged with mode classes and
    format classes.

    Parameters
    ----------
    infile : `str`
        The input file path
    connect_uri : `sqlalchemy.engine.url.URL` or `str` or `NoneType`, optional, default: `NoneType`
        Connect to an alternative data dictionary. If NoneType then the
        package data dictionary is used
    index_file : `str` or `NoneType`, optional, default: `NoneType`
        An alternative location for an index file. `NoneType` means that
        the path of any index files is the same as the wide_file but with
        .idx suffix
    no_index : `bool`, optional, default: `False`
        Do not use an index even if one exists. This is useful to turn on
        if all of the eids are required, as it will be quicker sequentially
        looking through the file than traversing the index
    **csv_kwargs
        Arguments that will be passed through to python `csv.reader`

    Notes
    -----
    Whilst is has all the core functionality it should not be used in isolation
    and should always be mixed-in with a mode class and a format class.

    Examples
    --------
    Create a parser from the mode class and the formatter class

    >>> class ParserClass(BaseWideFileParser, ModeClass, FormatterClass):
    >>>     pass
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, infile, connect_uri=None, index_file=None,
                 no_index=False, **csv_kwargs):
        self._infile = os.path.expanduser(infile)
        self._csv_kwargs = csv_kwargs
        self._connect_uri = connect_uri

        # Make sure the index is set up correctly (if we have it)
        self._set_index(index_file)

        # Make sure we have defined how we want to read in the file
        self._csv_kwargs = wfc.get_csv_kwargs(infile, **csv_kwargs)
        self._no_index = no_index

        # Initialise all the variables used in the object
        self._fobj = None
        self._reader = None
        self._header = []
        self._header_len = 0
        self._line_no = 0
        self._is_open = False
        self._using_context_manager = False
        self._is_iterating = False
        self._data_col_start = 0
        self._data_header = None

        # Values that are considered empty data in the wide file
        self._empty_data = ['']

        # If any requested fields can't be located in the data dictionary
        # then the get set here
        self.missing_fields = set()

        # mappings between datatypes in the database and functions that will
        # convert the data to the correct type
        self._cast_map = self.get_cast_map()

        # Connect to the data dictionary
        self._data_dict_interface = dd.DataDictLookup(
            connect_uri=self._connect_uri
        )

        # Will cache field ID lookups inside the object
        self.FIELD_ID_CACHE = {}

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __enter__(self):
        """
        Entry point for the context manager
        """
        # Opening the file, will set the header up
        self.open()
        self._using_context_manager = True
        return self

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __exit__(self, *args):
        """
        Entry point for the context manager

        *args
            Error arguments should the context manager exit in discrace
        """
        self.close()
        self._using_context_manager = False

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def header(self):
        """
        Return the full header of the file irrespective of any subsetting that
        has been attempted
        """
        return self._header

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def open(self):
        """
        Open the file. Note that this is agnostic for gzipped files
        """
        # Mkae sure the file is not open as we do not want to open twice
        self._check_file_closed()
        self._fobj = None

        # Re initialise the variables associated with the file
        self._header = []
        self._header_len = 0
        self._line_no = 0
        self._pops = [0]
        self._data_header_tuples = []
        self.bad_values = []
        self.subset_header = []
        self.subset_header_tuples = []
        self.subset_header_idx = []
        self.subset_field_ids = []

        # Here we open the file differnently depending on if the index is
        # available. Note, in the current implementation, if the user wants
        # all somples then it still goes via the index. This may not be the
        # best approach
        # Obey the user no index overide even if the an index is available
        if self._index_available is False or self._no_index is True:
            # Here we ensure we set the index available to False as it could be
            # True with no_index == True, in which case other fuctions might
            # try and use it
            self._index_available = False

            # compression agnostic opening
            self._fobj = gzopen.gzip_fh(self._infile)
            self._reader = csv.reader(self._fobj, **self._csv_kwargs)
        else:
            # When we have the index available the file object is the index
            self._fobj = wfi.IndexReader(
                self._infile,
                index_file=self._index_file
            )
            self._fobj.open()
            self._reader = None

        self._process_header()
        self._is_open = True

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def close(self):
        """
        Close the file
        """
        self._check_file_open()

        self._fobj.close()
        self._is_opened = False
        self._fobj = None
        self._reader = None

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def fetch(self, eids=None, fields=None, nonetype=True):
        """
        fetch rows from the UKBB wide file with the option to extract specific
        eids, field and/or columns. If the wide file has been indexed
        then this will be used to extract any specific eids. Note that both
        field_ids and column name extractions can be selected at the same time.

        Parameters
        ----------
        eids : NoneType or list of int
            Specific UKBB sample IDs that are required. If `NoneType` then all
            sample IDs are required
        fields : NoneType or list of int or str
            Specific UKBB field IDs or column names from the wide file. If
            NoneType then it is assumed that all columns are required. Note
            that the difference between field IDs and colnames is that one
            field_id may equate to many column names as it could have multiple
            instances and arrays. So `field_ids` is a quality of life feature
            so the user does not have to care to know about the data dictionary
            A column name is a specific UKBB wide file column name. A column
            name is the specific field_id, instance number and array number
            column in the header
        nonetype : bool, optional, default: True
            Should NoneType (missing data) values be retained . As the
            wide_file structure is naturally sparse it is sometimes beneficial
            to filter NoneType values. Note that in some formats this option
            will have no effect

        Yields
        ------
        row : dict
            A parsed and processed row from the wide file. each row represents
            all the measures in the wide file that are available for an
            individual sample
        """
        # Change fileds to an empty list if it is NoneType
        fields = fields or []
        eids = eids or []

        # Make sure that the wide file has been opened
        self._check_file_open()

        # Seek back to the header position
        self._reset_file()

        # Make sure all the fetch specific values are initialised
        self.bad_values = []
        self.subset_header = []
        self.subset_header_tuples = []
        self.subset_header_idx = []
        self.subset_field_ids = []

        # This sets up how we want to deal with empty values
        # This dynamically assigns a method call to a variable self.test_value.
        # What method is assigned will depend on the nonetype value. if it is
        # True then a method call will be used that will raise a
        # wide_file_common.BlankDataError when an empty value is encountered
        # when nonetype is False then a method call is used that will raise
        # a wide_file_common.SkipDataError when it encounters an empty value
        # This allows the flow control to be altered depending on the nonetype
        # value and prevents an if statement being used in billions of calls
        self._assign_data_test_method(nonetype)

        # The row fetch method will change depending on if we want everything
        # or specific IDs and if the file has been indexed or not
        row_fetch_method = self._get_row_fetch_method(eids=eids)

        # This method will be used to filter to the required columns. What
        # method is used will depend on the fields value. If it is valid then
        # a method is returned that will filter out the correct columns
        # otherwise a dummy call that does nothig is used
        col_subset_method = \
            self._get_column_subset_method(fields)

        # Now setup all the lookups against the data dictionary
        # The blank list being supplied here
        field_id_info = self.get_field_data()
        # pp.pprint(field_id_info)
        self.set_field_data(field_id_info)

        # The row is all the data fields and the leading_cols is the eid and
        # withdrawn column (if present)
        for row, eid in row_fetch_method(eids):
            row = col_subset_method(row)
            # row = self.format_row(row, int(leading_cols[0]))

            yield self.format_row(row, eid)

        # Issue any bad values warning messages
        self._test_bad_values(nonetype)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_cast_map(self):
        """
        Return the function calls that will be used to process the different
        data types

        Returns
        -------
        cast_map : dict
            Calls to functions that will format the data types. The keys of
            the dict are data types as coding in the data dictionary the values
            are function calls
        """
        return {
            dd.INTEGER_TYPE: dummy_call,
            dd.FLOAT_TYPE: dummy_call,
            dd.ENUM_SINGLE_TYPE: dummy_call,
            dd.ENUM_MULTIPLE_TYPE: dummy_call,
            dd.DATE_TYPE: dummy_call,
            dd.TIME_TYPE: dummy_call,
            dd.STRING_TYPE: dummy_call,
        }

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_field_data(self):
        """
        Get the field data that will determine how the various columns will be
        parsed. this should be overwritten to query out the data or supply
            placeholders depending on what the user wants.

        Returns
        -------
        field_id_info : list or `wide_file_common.FieldId`
            The field ID info that will be used to determine how to parse the
            data
        """
        return []

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def set_field_data(self, field_id_info):
        """
        After the user has passed the field IDs that want to subset and they
        have been turned into `data_dict.FieldId` tuples, either by querying
        the data dictionary or by generating placeholders for non-formatting
        parsers. This is then responsible for updating the cache with them and
        cross referencing them with the header of the file to make sure all the
        columns in the header are represented (and making a note of those that
        are not). Finally it, interrogates the `data_dict.FieldId` tuples to
        determine the casting function needed for the field. All of this
        information is then available in `obj._subset_header_parse`. Any
        columns that are not present in the fields are logged in
        `obj.missing_fields`.

        Parameters
        ----------
        field_id_info : list of :obj:`data_dict.FieldId`
            The descriptions of the fields. These may come from the data
            dictionary but also may be dummy descriptions in the case of
            naive parsers
        """
        missing_fields = 0
        # First update the cache
        for i in field_id_info:
            self.FIELD_ID_CACHE[i.field_id] = i

        # This will hold the same number of columns as we have in the header
        # and will be used to cast all the fields to the correct format
        self._subset_header_parse = []
        for fid, n_instances, array in self.subset_header_tuples:
            try:
                # Is the requested column present in the cache
                fid_info = self.FIELD_ID_CACHE[fid]
            except KeyError:
                # Not present so we will add a dummy one and log the field ID
                # of the missing field
                missing_fields += 1
                fid_info = dd.FieldId(
                    field_id=fid,
                    field_text='DUMMY_FIELD_ID',
                    field_desc="dummy field_id",
                    data_type='str',
                    data_units=None,
                    coding_id=None,
                    date_field_id=con.BASELINE,
                    coding_enum=None
                )
                self.missing_fields.add(fid)
                # Add the dummy record to the field ID cache
                self.FIELD_ID_CACHE[fid] = fid_info

            cast_func = self._cast_map[fid_info.data_type]
            self._subset_header_parse.append(
                (cast_func, fid_info)
            )
        if missing_fields > 0:
            warnings.warn(
                "There are {0} missing fields, these will be set to str, use"
                " obj.missing_fields to see them".format(missing_fields)
            )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def format_row(self, row, eid):
        """
        Format the row into the desired format, this also controls the
        conversion of the data types. Different formatting classes will overide
        this.

        Parameters
        ----------
        row : list of str
            A row that needs formatting and converting the type, this row will
            only have field ID data in it and not eid or withdrawn status
        eid : int
            The UKBB sample ID, this will be added into the row

        Returns
        -------
        row : list
            A linear row of data. The data types will vary depending on the
            mode class
        """
        raise NotImplementedError(
            "Do not use the BaseClass without a FormatClass, and do not call"
            " super().format_row() from the format class"
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def cast_value(self, eid, value, instance, array, value_info):
        """
        Attempt to cast a value into the correct type based on the value info.
        If the cast can't be completed then the null value is returned and the
        bad value is stored internally in the object.

        Parameters
        ----------
        value : str
            A value that needs to be cast into the correct format
        value_info : tuple
            The field information that contains the cast function at [0] and
            the `wide_file_common.FieldId` at [1]

        Returns
        -------
        cast_value : `Any`
            The value cast to the required format. If the cast can't be
            completed, then the NULL value is returned and the data is stored
            in bad_values
        """
        try:
            # TODO: Catch cast errors
            return value_info[0](value, value_info[1])
        except ValueError:
            if value_info[1].field_id > 0:
                self.bad_values.append(
                        )
                print(value_info[0])
                print(value_info[1])
                print(value)
                raise

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_null(self):
        """
        Will return a value that represents a null value in the dataset. This
        should be overiden by the formatter class

        Returns
        -------
        null : str
            A value that represents the null value in the dataset
        """
        raise NotImplementedError(
            "Do not use the BaseClass without a ModeClass, and do not call"
            " super().get_null() from the mode class"
        )

        return ''

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _get_row_fetch_method(self, eids):
        """
        Get the method used to fetch the rows based on if specific eids have
        been requested and if the file is indexed. Note that both field_ids and
        column name extractions can be used at the same time.

        Parameters
        ----------
        eids : list of int
            Specific UKBB sample IDs that are required. If empty then all
            sample IDs are required

        Returns
        -------
        row_fetch_method : function
            The function used to fetch the rows from the wide file
        """
        row_fetch_method = self._yield_all_rows
        if len(eids) > 0 and self._index_available is True:
            # Specific EIDs but with indexing
            row_fetch_method = self._yield_index_rows
        elif len(eids) > 0 and self._index_available is False:
            # Specific EIDs but with no indexing
            row_fetch_method = self._yield_eid_rows
        elif len(eids) == 0 and self._index_available is True:
            # Here we are trying to scan through the file but using the index
            # this is not the most efficient method so we will issue a warnings
            # but will still do it
            warnings.warn(
                "selecting all eids using an index is not efficient, try"
                " setting use_index=False"
            )
            row_fetch_method = self._yield_all_rows_index

        return row_fetch_method

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _get_column_subset_method(self, fields):
        """
        Get the method used to subset columns from the rows in the wide file.
        Note that both field_ids and column name extractions can be used at
        the same time. This function also sets the subset_header and other
        variables related to the subset that are also public to the user

        Parameters
        ----------
        fields : NoneType or list of int or str
            Specific UKBB field IDs or column names from the wide file. If
            NoneType then it is assumed that all columns are required. Note
            that the difference between field IDs and colnames is that one
            field_id may equate to many column names as it could have multiple
            instances and arrays. So `field_ids` is a quality of life feature
            so the user does not have to care to know about the data dictionary
            A column name is a specific UKBB wide file column name. A column
            name is the specific field_id, instance number and array number
            column in the header

        Returns
        -------
        column_subset_method : function
            The function used to subset columns from the rows from the wide
            file
        """
        # Partition any field IDs into field_ids and columns
        field_ids, colnames = wfc.filter_fields(fields)

        # If no field/column subsetting is happening then we want all the
        # columns so we setup the subset variables to reflect that even though
        # they will not be used in the subset process, but the user may need
        # them
        if len(field_ids) == 0 and len(colnames) == 0:
            return self._get_no_col_subset()

        # If we get here then either the field_ids are defined or the columns
        # are defined, so the objective now is to convert them to column
        # matching numbers
        all_columns = set()
        if len(field_ids) > 0:
            all_columns.update(self._map_field_ids_to_columns(field_ids))

        if len(colnames) > 0:
            all_columns.update(colnames)

        if len(all_columns) > 0:
            all_column_numbers = self._map_columns_to_indexes(all_columns)
            # print(all_columns)
            # print(all_column_numbers)
            # Now set up the subset headers for the columns that we want
            # and make sure the column subsetting method is set correctly
            return self._get_col_subset(all_column_numbers)
        else:
            # This will mean that we could not map the field IDs back to
            # column names
            raise ValueError("supplied field_ids match no columns")

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _get_no_col_subset(self):
        """
        In the event that no columns have been selected for subsetting, this
        sets up the extraction lists for all the columns in the header and
        returns the correct function to call to do no column extraction
        (essentially a dummy call)

        Returns
        -------
        dummy_call : function
            A function that will just accept and return the row
        """
        # Setup the subset variables for "all columns"
        self.subset_header = self._data_header.copy()
        self.subset_header_tuples = self._data_header_tuples.copy()
        self.subset_header_idx = list(range(len(self._data_header)))
        self.subset_field_ids = \
            list(set([i[0] for i in self._data_header_tuples]))

        # this will get all the parse information from the data dictionary
        # and can take some time when there are lots of columns
        # self._set_subset_parse()
        return self._no_subset

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _get_col_subset(self, columns_idx):
        """
        In the event that we have columns we want to extract from each row.
        This will get the method we want to use to do it and setup all the
        extraction lists that are used to extract the columns

        Parameters
        ----------
        column_idx : list or int
            The indexes of the columns (column numbers) that we want to extract

        Returns
        -------
        dummy_call : function
            A function that will just accept and return the row
        """
        # Build all the subset lests. The subset_header_idx is the one that
        # will actually be used to extract the data values
        colno = sorted(list(columns_idx))
        self.subset_header = [self._data_header[idx] for idx in colno]
        self.subset_header_tuples = [self._data_header_tuples[idx] for idx in colno]
        self.subset_header_idx = colno
        self.subset_field_ids = \
            sorted(list(set([i[0] for i in self.subset_header_tuples])))

        # If we get here then we have found something so we are safe to set
        # the column subset method to use the column indexes to extract the
        # column data
        return self._col_subset

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _no_subset(self, row):
        """
        A dummy call that performs no column subsetting at all

        Parameters
        ----------
        row : list of str
            A row that will be passed through the method

        Returns
        -------
        row : list of str
            A row that has been read with `csv.reader`, no type conversion has
            been applied at this point
        """
        return row

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _col_subset(self, row):
        """
        Subset specific column indexes from a row. These have been identified
        and stored in `obj.subset_header_idx`

        Parameters
        ----------
        row : list of str
            A row that will be subset

        Returns
        -------
        row : list of str
            A row that has been read with `csv.reader`, no type conversion has
            been applied at this point. But specific columns have been
            extracted from the row
        """
        return [row[i] for i in self.subset_header_idx]

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _map_field_ids_to_columns(self, field_ids):
        """

        """
        # Will hold the IDs of fields that we can't find in the data
        missing_fids = set()
        found_fids = set()
        colnames = []

        # We search in the header tuples as these have already been split
        # into field IDs, we store the index of all the columns that match
        # on field ID. We also store the field IDs that have been matched
        # so we can use them to workout what field IDs have not been
        # matched
        for idx, i in enumerate(self._data_header_tuples):
            if i[0] in field_ids:
                found_fids.add(i[0])
                colnames.append(self._data_header[idx])

        missing_fids = set(field_ids).difference(found_fids)

        # If we can't find any columns of fields then issue warnings
        if len(missing_fids) > 0:
            warnings.warn(
                'fields not found: {0}'.format(
                    ",".join([str(i) for i in missing_fids])
                )
            )

        return colnames

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _map_columns_to_indexes(self, column_names):
        """

        """
        # Now attempt to map any column names back to column indexes in the
        # header
        all_col_idx = []
        missing_column_names = []
        for i in column_names:
            try:
                all_col_idx.append(self._data_header.index(i))
            except ValueError:
                missing_column_names.append(i)

        # Error out of we do not find any columns or matching field IDS
        if len(all_col_idx) == 0:
            raise ValueError("no columns found!")

        if len(missing_column_names) > 0:
            warnings.warn(
                'columns not found: {0}'.format(
                    ",".join([str(i) for i in missing_column_names])
                )
            )
        return sorted(all_col_idx)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _check_file_closed(self):
        """
        Make sure that the file is not open and error out if it is

        Raises
        ------
        IOError
            If the file is open
        """
        if self._is_open is True:
            raise IOError('UKBB wide file already open')

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _process_header(self):
        """
        Process the header from the file and make sure it is valid. Also,
        compare to the data dictionary

        Raises
        ------
        IndexError
            If the header of the file does not match the expected header
        """
        # When we have read the header in we have also read a line, i.e. lines
        # are referenced from the beginning of the file not the data
        if self._index_available is False:
            self._line_no += 1
            self._header = next(self._reader)
        else:
            self._line_no = None
            self._header = self._fobj.header

        # If we have a withdrawn column as the second column then we will want
        # to remove the first and second column from each row
        self._data_header = \
            wfc.check_header(list(self._header))
        self._remove_eid(self._data_header)

        # We store the length so it can be compared to each row to make sure
        # they are of the expected length
        self._header_len = len(self._header)
        self._data_header_len = len(self._data_header)

        # Split the header column into component tuples
        self._data_header_tuples = wfc.split_header(self._header)

        # TODO: Remove this, it was put here for debugging
        for idx, i in enumerate(self._data_header_tuples):
            wanted = "{0}-{1}.{2}".format(*i)
            if self._data_header[idx] != wanted:
                raise IndexError("header missaligned, {0} != {1}".format(
                    self._data_header[idx], wanted))

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _set_index(self, index_file):
        """
        See if the index file is available and if we should use it for sample
        subsets.

        Parameters
        ----------
        index_file : str or NoneType
            The potential path to the index file. If NoneType then we look for
            the path based on the infile path
        """
        # Make sure the path to the index file is set
        self._index_file = wfc.get_index_path(
            self._infile,
            index_file=index_file
        )

        try:
            builtins.open(self._index_file).close()
            self._index_available = True
        except FileNotFoundError:
            self._index_available = False

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _remove_eid(self, row):
        """
        Removes the lead column from the row and returns it. This will
        be the eid column. Note that the row is not returned as the removal is
        done in place


        Parameters
        ----------
        row : list or str
            The row to remove the lead column

        Returns
        -------
        eid : str
            The eid column value (the first column in the row)
        """
        # return [row.pop(0) for i in range(self._data_col_start)]
        return row.pop(0)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _check_file_open(self):
        """
        Make sure that the file is open and error out if it is not

        Raises
        ------
        IOError
            If the file is closed
        """
        if self._is_open is False:
            raise IOError('UKBB wide file not open, either use the context '
                          'manager or obj.open()')

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _reset_file(self):
        """
        Go back to where the data starts. I do not seem to be able to get the
        tell for here the data starts (I get an error). So instead I will seek
        back to the start of the file and move forward a row.
        """
        if self._index_available is False:
            self._fobj.seek(0)
            next(self._fobj)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _assign_data_test_method(self, nonetype):
        """
        Assign a method to test is a data value is valid the assigned methods
        will either raise a `wide_file_common.SkipDataError` or a
        `wide_file_common.BlankDataError` in response to NULL or empty data

        Parameters
        ----------
        nonetype : bool
            A value to indicate if nonetype values are required or not. True
            means include them, False means exclude them
        """
        if nonetype is False:
            self.test_value = self._skip_empty_data
        else:
            self.test_value = self._flag_empty_data

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _skip_empty_data(self, value):
        """
        Assess the data value and raise a SkipDataError error if it is an
        empty value

        Parameters
        ----------
        value : str
            A value to assess if it is empty

        Returns
        -------
        value : str
            A value to assess if it is empty

        Raises
        ------
        wide_file_common.SkipDataError
            If the value is empty
        """
        if value in self._empty_data:
            raise wfc.SkipDataError("empty")

        return value

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _flag_empty_data(self, value):
        """
        Assess the data value and raise a BlankDataError error if it is an
        empty value

        Parameters
        ----------
        value : str
            A value to assess if it is empty

        Returns
        -------
        value : str
            A value to assess if it is empty

        Raises
        ------
        wide_file_common.BlankDataError
            If the value is empty
        """
        if value in self._empty_data:
            raise wfc.BlankDataError("empty")

        return value

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _fetch_row(self):
        """
        Fetch a row from the file. This also increments the line number that
        we are on

        Returns
        -------
        row : list of str
            A row that has been read with `csv.reader`, no type conversion has
            been applied at this point
        leading_cols : list or str
            A list of the non-field ID columns. This will certaily have the
            `eid` column at [0] and may have the `withdrawn` column at [1]

        Raises
        ------
        StopIteration
            When we are at the end of the file
        IndexError
            If the length of the data row is not what is expected
        """
        self._line_no += 1

        try:
            # print("====")
            row = next(self._reader)
            # print("ROW LEN: {0}".format(len(row)))
            eid = self._remove_eid(row)

            # print("HEADER LEN: {0}".format(len(self._header)))           
            # print("ROW LEN: {0}".format(len(row)))
            # print("====")
            # Check the row length
            if len(row) != self._data_header_len:
                raise IndexError(
                    "{0}: row data has '{1}' columns expected: '{2}'".format(
                        self._line_no,
                        len(row), self._data_header_len
                    )
                )

            return row, int(eid)
        except StopIteration:
            # As we are preincrementing, if we reach the end we have to
            # subtract one
            self._line_no -= 1
            raise

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _yield_all_rows(self, *args):
        """
        Yield all the rows (unformatted) in order from the file

        Parameters
        ----------
        *args
            Any arguments that have to be parsed to keep the interface the
            same between all three row yielding methods. Has no effect

        Yields
        ------
        row : list of str
            A row that has been read with `csv.reader`, no type conversion has
            been applied at this point
        leading_cols : list or str
            A list of the non-field ID columns. This will certaily have the
            `eid` column at [0] and may have the `withdrawn` column at [1]
        """
        while True:
            try:
                yield self._fetch_row()
            except StopIteration:
                break

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _yield_eid_rows(self, wanted_eids):
        """
        Fetch specific EIDs when the file has NOT been indexed

        Parameters
        ----------
        eids : list of int
            Specific UKBB sample IDs that are required. If `NoneType` then all
            sample IDs are required

        Yields
        ------
        row : list of str
            A row that has been read with `csv.reader`, no type conversion has
            been applied at this point
        leading_cols : list or str
            A list of the non-field ID columns. This will certaily have the
            `eid` column at [0] and may have the `withdrawn` column at [1]
        """
        while True:
            try:
                row, eid = self._fetch_row()

                try:
                    # Is the sample ID of the current row one of the ones
                    # that we require? If not then this will fail with a
                    # value error
                    idx = wanted_eids.index(eid)
                    wanted_eids.pop(idx)
                    yield row, eid

                    # See if there are any more eids left to process if not
                    # then we may as well exit
                    if len(wanted_eids) == 0:
                        break
                except ValueError:
                    # Not needed
                    pass
            except StopIteration:
                # EOF
                break

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _yield_index_rows(self, eids):
        """
        Fetch specific EIDs when the file has been indexed

        Parameters
        ----------
        eids : list of int
            Specific UKBB sample IDs that are required. If `NoneType` then all
            sample IDs are required

        Yields
        ------
        row : list of str
            A row that has been read with `csv.reader`, no type conversion has
            been applied at this point
        eid : int
            The `eid` column (sample ID)
        """
        for row in self._fobj.fetch(eids):
            eid = self._remove_eid(row)
            yield row, int(eid)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _yield_all_rows_index(self, *args):
        """
        This will yield all the samples contained in the index. This is not
        the most efficient way of doing things but it will work

        Parameters
        ----------
        *args
            Any arguments that have to be parsed to keep the interface the
            same between all three row yielding methods. Has no effect

        Yields
        ------
        row : list of str
            A row that has been read with `csv.reader`, no type conversion has
            been applied at this point
        eid : int
           The `eid` column (sample ID)
        """
        # Get all the available EIDs from the index and then fetch them one by
        # one
        eids = list(self._fobj.idx.keys())

        for row in self._fobj.fetch(eids):
            # extract the sample ID from the rest of the data
            eid = self._remove_eid(row)

            # Now we sanity check the row to make sure that it is of the
            # expected length
            if len(row) != len(self._data_header):
                raise IndexError("row data length '{0}' != header data length "
                        "'{0}'".format(len(row), len(self._header))
                        )
            yield row, int(eid)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _test_bad_values(self, nonetype):
        """
        This issues warnings depending on if there are any bad values stored
        inside the object. The warning message will vary slightly depending on
        if we are parsing out NoneType values for removing them. bad_values are
        data values that can't be cast to the correct format
        """
        if len(self.bad_values) > 0:
            msg = (
                "there are '{0}' bad values in the dataset. use:"
                " 'obj.bad_values' to see them.".format(len(self.bad_values))
            )

            if nonetype is True:
                msg = ("{0} These will be set to 'NoneType' in the "
                       "data".format(msg))
            else:
                msg = ("{0} These will be removed from the data".format(msg))
            warnings.warn(msg)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def search_field_info_cache(self, field_ids):
        """
        This gets the field_id descriptions and enumerations. As this can be
        quite an expensive operation they are cached at the class level for
        future use. If field IDs are not found in the data dictionart=y then a
        warning is issued and a dummy field ID is put in place with a string
        data type

        Parameters
        ----------
        field_ids : list of int
            The field IDs that we want the information for
        connect_uri : :obj:`sqlalchemy.engine.url.URL` or str or NoneType,
        optional, default: NoneType
            Connect to an alternative data dictionary. If NoneType then the
            package data dictionary is used

        Returns
        -------
        field_id_data : list of :obj:`data_dict.FieldId`
            Field ID named tuple that contains information about the field
            including any enumerations
        """
        present = []
        not_present = []

        for i in field_ids:
            if i not in self.FIELD_ID_CACHE:
                not_present.append(i)
            else:
                present.append(i)

        return present, not_present


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class RawMode(object):
    """
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    def get_cast_map(self):
        """
        Return the function calls that will be used to process the different
        data types

        Returns
        -------
        cast_map : dict
            Calls to functions that will format the data types. The keys of
            the dict are data types as coding in the data dictionary the values
            are function calls
        """
        cast_map = super().get_cast_map()
        return cast_map

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_field_data(self):
        """
        After the subset field IDs have been set. This uses them to query the
        data dictionary to get the enumeration information and data type
        information for all of the fields. This will applied to all the
        columns that we want to parse out of the file
        """
        field_info = super().get_field_data()

        present_fid, not_present_fid = self.search_field_info_cache(
            self.subset_field_ids
        )

        for fid in not_present_fid:
            fid_info = dd.FieldId(
                field_id=fid,
                field_text=None,
                field_desc="placeholder field for 0 parsing",
                data_type='str',
                data_units=None,
                coding_id=None,
                date_field_id=53,
                coding_enum=None
            )
            field_info.append(fid_info)
            # self.FIELD_ID_CACHE[fid] = fid_info
        return field_info

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_null(self):
        """
        Will return a value that represents a null value in the dataset. This
        should be overiden by the formatter class

        Returns
        -------
        null : str
            A value that represents the null value in the dataset
        """
        return ''


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class NumericMode(object):
    """
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    def get_cast_map(self):
        """
        Return the function calls that will be used to process the different
        data types

        Returns
        -------
        cast_map : dict
            Calls to functions that will format the data types. The keys of
            the dict are data types as coding in the data dictionary the values
            are function calls
        """
        cast_map = super().get_cast_map()
        cast_map[dd.FLOAT_TYPE] = parse_float
        cast_map[dd.INTEGER_TYPE] = parse_int
        cast_map[dd.DATE_TYPE] = parse_date
        cast_map[dd.TIME_TYPE] = parse_date
        return cast_map

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_field_data(self):
        """
        After the subset field IDs have been set. This uses them to query the
        data dictionary to get the enumeration information and data type
        information for all of the fields. This will applied to all the
        columns that we want to parse out of the file
        """
        field_info = super().get_field_data()

        present_fid, not_present_fid = self.search_field_info_cache(
            self.subset_field_ids
        )

        field_info = []
        for fid in self._data_dict_interface.get_field_id(not_present_fid):
            field_info.append(fid)

        return field_info

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_null(self):
        """
        Will return a value that represents a null value in the dataset. This
        should be overiden by the formatter class

        Returns
        -------
        null : str
            A value that represents the null value in the dataset
        """
        return None


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class FullMode(object):
    """
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_cast_map(self):
        """
        Return the function calls that will be used to process the different
        data types

        Returns
        -------
        cast_map : dict
            Calls to functions that will format the data types. The keys of
            the dict are data types as coding in the data dictionary the values
            are function calls
        """
        # mappings between datatypes in the database and functions that will
        # convert the data to the correct type
        cast_map = super().get_cast_map()
        cast_map[dd.FLOAT_TYPE] = parse_float
        cast_map[dd.INTEGER_TYPE] = parse_int
        cast_map[dd.ENUM_SINGLE_TYPE] = parse_enum
        cast_map[dd.ENUM_MULTIPLE_TYPE] = parse_enum
        cast_map[dd.DATE_TYPE] = parse_date
        cast_map[dd.TIME_TYPE] = parse_date

        return cast_map

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_field_data(self):
        """
        After the subset field IDs have been set. This uses them to query the
        data dictionary to get the enumeration information and data type
        information for all of the fields. This will applied to all the
        columns that we want to parse out of the file
        """
        field_info = super().get_field_data()

        present_fid, not_present_fid = self.search_field_info_cache(
            self.subset_field_ids
        )

        field_info = []
        for fid in self._data_dict_interface.get_field_id_enum(
                not_present_fid
        ):
            field_info.append(fid)

        return field_info

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_null(self):
        """
        Will return a value that represents a null value in the dataset. This
        should be overiden by the formatter class

        Returns
        -------
        null : str
            A value that represents the null value in the dataset
        """
        return None


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class FlatFormatter(object):
    """
    The flat formatter will return a row that is a linear list of values
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    def fetch(self, eids=None, fields=None, **kwargs):
        """
        A wrap around the fetch method to exclude the nonetype argument

        Parameters
        ----------
        eids : NoneType or list of int
            Specific UKBB sample IDs that are required. If `NoneType` then all
            sample IDs are required
        fields : NoneType or list of int or str
            Specific UKBB field IDs or column names from the wide file. If
            NoneType then it is assumed that all columns are required. Note
            that the difference between field IDs and colnames is that one
            field_id may equate to many column names as it could have multiple
            instances and arrays. So `field_ids` is a quality of life feature
            so the user does not have to care to know about the data dictionary
            A column name is a specific UKBB wide file column name. A column
            name is the specific field_id, instance number and array number
            column in the header
       **kwargs
            Other keyword arguments to the parent class (will be ignored)
        """
        for row in super().fetch(eids=eids, fields=fields):
            yield row

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def format_row(self, row, eid):
        """
        Format the row into the desired format, this also controls the
        conversion of the data types. Different formatting classes will overide
        this.

        Parameters
        ----------
        row : list of str
            A row that needs formatting and converting the type, this row will
            only have field ID data in it and not eid or withdrawn status
        eid : int
            The UKBB sample ID, this will be added into the row

        Returns
        -------
        row : list
            A linear row of data. The data types will vary depending on the
            mode class
        """
        for idx, i in enumerate(row):
            try:
                value = self.test_value(i)
                row[idx] = self._subset_header_parse[idx][0](
                        value,
                        self._subset_header_parse[idx][1]
                    )
            except wfc.BlankDataError:
                row[idx] = self.get_null()
            except ValueError:
                if self._subset_header_parse[idx][1].field_id > 0:
                    self.bad_values.append(
                        [
                            eid,
                            value,
                            self._subset_header_parse[idx][1].field_id,
                            self._subset_header_parse[idx][1].data_type,
                        ]
                    )
                row[idx] = self.get_null()

        row.insert(0, eid)
        return row


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class DictFormatter(object):
    """
    The dict formatter expresses each row as a dictionary with the keys being
    the column names expressed as tuples and the values being the data values
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    def format_row(self, row, eid):
        """
        Format the row into the desired format, this also controls the
        conversion of the data types. Different formatting classes will overide
        this.

        Parameters
        ----------
        row : list of str
            A row that needs formatting and converting the type, this row will
            only have field ID data in it and not eid or withdrawn status
        eid : int
            The UKBB sample ID, this will be added into the row

        Returns
        -------
        row : list
            A linear row of data. The data types will vary depending on the
            mode class
        """
        dict_row = {'eid': eid}
        for idx, i in enumerate(row):
            try:
                colname = self.subset_header_tuples[idx]
                value = self.test_value(i)
                # value = self.cast_value(value, self._subset_header_parse[idx])
                dict_row[colname] = self._subset_header_parse[idx][0](
                        value,
                        self._subset_header_parse[idx][1]
                        )
                # dict_row[colname] = self._subset_header_parse[idx][0](
                    # value,
                    # self._subset_header_parse[idx][1]
                # )
            except wfc.BlankDataError:
                dict_row[colname] = self.get_null()
            except wfc.SkipDataError:
                pass
            except ValueError:
                if self._subset_header_parse[idx][1].field_id > 0:
                    self.bad_values.append(
                        [
                            eid,
                            value,
                            self._subset_header_parse[idx][1].field_id,
                            self._subset_header_parse[idx][1].data_type,
                        ]
                    )
                try:
                    # Note that we are introducing a NULL value even when we
                    # might be explicitly removing them
                    self.test_value('')
                except wfc.BlankDataError:
                    dict_row[colname] = self.get_null()
                except wfc.SkipDataError:
                    pass 
        return dict_row


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class NestedFormatter(object):
    """
    The nested formatter expresses each row as multiple nested dictionaries.
    The outer layer of nesting has the field IDs as keys with the values being
    a further dictionary of colname tuples as keys (the same as the
    DictFormatter). This allows lookups based on the field IDs
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    def format_row(self, row, eid):
        """
        Format the row into the desired format, this also controls the
        conversion of the data types. Different formatting classes will overide
        this.

        Parameters
        ----------
        row : list of str
            A row that needs formatting and converting the type, this row will
            only have field ID data in it and not eid or withdrawn status
        eid : int
            The UKBB sample ID, this will be added into the row

        Returns
        -------
        row : dict of dict
            A nested dictionary with the field IDs as keys and a further dict
            as values. The nested dict has column tuple keys
        """
        dict_row = {'eid': eid}
        for idx, i in enumerate(row):
            try:
                colname = self.subset_header_tuples[idx]
                value = self.test_value(i)
                dict_row.setdefault(colname[0], {})

                # value = self.cast_value(value, self._subset_header_parse[idx])
                dict_row[colname[0]][colname] = \
                    self._subset_header_parse[idx][0](
                            value,
                            self._subset_header_parse[idx][1]
                        )
            except wfc.BlankDataError:
                dict_row.setdefault(colname[0], {})
                dict_row[colname[0]][colname] = self.get_null()
            except wfc.SkipDataError:
                pass
            except ValueError:
                if self._subset_header_parse[idx][1].field_id > 0:
                    self.bad_values.append(
                        [
                            eid,
                            value,
                            self._subset_header_parse[idx][1].field_id,
                            self._subset_header_parse[idx][1].data_type,
                        ]
                    )
                try:
                    # Note that we are introducing a NULL value even when we
                    # might be explicitly removing them
                    self.test_value('')
                except wfc.BlankDataError:
                    dict_row[colname[0]][colname] = self.get_null()
                except wfc.SkipDataError:
                    pass
        return dict_row


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class LongFormatter(DictFormatter):
    """
    The long formatter will convert a wide row into a long format much more
    similar to what will be observed in a relational database
    """
    HEADER = ['eid', 'baseline_date', 'baseline_int_date', 'field_text',
              'field_id', 'instance_id', 'array_id',
              'relevant_date_field_text', 'relevant_date_field_id',
              'relevant_date_field_instance_id', 'relevant_date_array_id',
              'relevant_date', 'relevant_int_date', 'event_days_from_baseline',
              'age_years_at_event', 'is_fallback_date', 'value']

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def fetch(self, **kwargs):
        """
        The long format fetch method will wrap the generic one and transpose
        the wide data. However, in doing so the long format can add more
        information on the data. Such as most relevant date and the days
        relative to baseline.

        Parameters
        ----------
        **kwargs
            Keyword arguments accepted by `BaseWideFileParser`
        """
        # This will hold all the requested field and any additional ones that
        # we want to get for matching dates. Remember fields can be a mixture
        # of field IDs and column names
        fields = kwargs.pop('fields', [])

        # Just in case the user supplies a fields=None
        fields = fields or []

        # if the fields is empty then we are requiesting all fields so the
        # "extra" date fields that we need to output the most relevant date and
        # calculate the rel_date_minus_baseline column will already be in the
        # data
        if len(fields) > 0:
            date_fields = self._get_date_fields(fields)
            keep_columns = self._get_keep_columns(fields)

            # Create a final set of search fields from the original ones that
            # the user supplied and the additional most relevant date fields
            kwargs['fields'] = list(set(fields+date_fields))
        else:
            keep_columns = self._data_header_tuples

        # The super class will do the hard work
        for row in super().fetch(**kwargs):
            # The base line for the sample, this should be in everything and
            # if it is missing then something bad has happened and we do not
            # want to continue
            try:
                baseline_int = row[(con.BASELINE, 0, 0)].int_date
                baseline_pretty = row[(con.BASELINE, 0, 0)].pretty_date
            except AttributeError:
                # An attribute error means that the date can't be calculated
                # as it is a string. The reason it is a string is because
                # the parse mode is raw
                baseline_int = self.get_null()
                baseline_pretty = row[(con.BASELINE, 0, 0)]
            try:
                # Also we want to calculate the date of birth, these
                # fields should be
                month = row[(dd.VITAL_STATS_FID.MONTH_OF_BIRTH.value, 0, 0)]
                year = row[(dd.VITAL_STATS_FID.YEAR_OF_BIRTH.value, 0, 0)]
                dob = type_casts.process_date(
                    datetime(year, int(month), 1)
                )
            except (AttributeError, KeyError):
                # No date of birth info or raw mode
                dob = self.get_null()

            # Loop through all the columns that we want to output. Here i will
            # be a tuple of the column name (fid, instance, array) and will be
            # used to lookup against a row dictionary with analogous tuple keys
            for i in keep_columns:
                try:
                    value = row[i]
                except KeyError:
                    # The KeyError might occur here for several reasons. If the
                    # user if filtering NoneType values then some of the
                    # requested columns will not be available
                    continue

                # For code clarity
                fid, inst, array = i
                field_text = self.FIELD_ID_CACHE[fid].field_text
                outrow = [
                            row['eid'],
                            baseline_pretty,
                            baseline_int,
                            field_text,
                            fid,
                            inst,
                            array
                        ]

                try:
                    date_field_id = self.FIELD_ID_CACHE[fid].date_field_id
                except KeyError as e:
                    if e.args[0] == 0:
                        continue

                # To calculate the date offset, we attempt to use the array
                # number and the instance ID of the date field ID for the field
                # we are interested in. If that fails then we, fall back to the
                # baseline for that instance ID. If that fails then we move
                # onto the previous instance ID for the date_field_id, and we
                # continue like this until we reach BASELINE, 0, 0 which
                # everything should have
                test_array = list(range(array, -1, -1))
                test_instances = list(range(inst, -1, -1))
                date_fields = [date_field_id, con.BASELINE]
                # Indicates is the row entry has it's optimal event date 0,
                # or non optional 1 or absolutely no information -1
                fallback_date = 0
                for darray, dinst, dfid in itertools.product(test_array,
                                                             test_instances,
                                                             date_fields):
                    date_field = None
                    date_pretty = None
                    date_int = None
                    offset = None
                    dftext = None
                    age_at_event = None
                    fallback_date = None
                    age_at_event = None

                    try:
                        # This line will throw a KeyError, which will loop
                        # around
                        date_field = row[(dfid, dinst, darray)]
                        date_pretty = date_field.pretty_date
                        date_int = date_field.int_date
                        offset = date_int - baseline_int
                        dftext = self.FIELD_ID_CACHE[dfid].field_text
                        age_at_event = wfe.calc_age_of_event(date_field, dob)
                        break
                    except KeyError:
                        fallback_date = 1
                    except AttributeError:
                        # Raw mode
                        date_pretty = date_field.pretty_date
                        date_int = self.get_null()
                        dinst = self.get_null()
                        dftext = self.get_null()
                        dfid = self.get_null()
                        offset = self.get_null()
                        fallback_date = -1
                        age_at_event = self.get_null()
                        break

                    # try:
                    #     age_at_event = wfe.calc_age_of_event(date_field, dob)
                    # except (AttributeError, KeyError):
                    #     age_at_event = self.get_null()

                outrow.extend([dftext, dfid, dinst, darray, date_pretty,
                               date_int, offset, age_at_event, fallback_date,
                               value])
                yield outrow

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _get_date_fields(self, fields):
        """
        Get any extra date fields that we may need in order to calculate
        relatave days etc. So these maybe fields that have not been requested
        by the user but we need them. We also need to return only the columns
        that the user has asked for and not our additional columns.

        Parameters
        ----------
        fields : list of str or int
            The field or column data that we want to extract

        Returns
        -------
        date_fields : list of int
            The field IDs that match the most relevant date of the columns that
            the user has selected
        """
        # Extract the fields that we need, their columns and their
        # relevant dates. The requested columns will be placed in out_cols
        field_ids, columns = wfc.filter_fields(fields)
        column_fids = [i[2] for i in wfc.split_header(columns)]

        # Now get the relevant date field ID for the requested fields
        date_fields = [i.date_field_id for i in self._data_dict_interface.
                       get_field_id_orm(list(set(field_ids + column_fids)))
                       ]

        # Make sure that the baseline is added to the required date fields
        date_fields.append(con.BASELINE)
        date_fields.append(dd.VITAL_STATS_FID.MONTH_OF_BIRTH.value)
        date_fields.append(dd.VITAL_STATS_FID.YEAR_OF_BIRTH.value)
        return date_fields

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _get_keep_columns(self, fields):
        """
        Take a list of fields and column names and return a list of column name
        tuples that are the columns that we want to keep. We do this as we will
        be querying extra date columns to output the most relevant date

        Parameters
        ----------
        fields : list of str or int
            The field or column data that we want to extract

        Returns
        -------
        columns : list of tuple
            The columns that will be pulled out of the data, we will want to
            ensure that only these columns will be yielded and not any date
            columns that the user did not specifically request
        """
        # Split the fields into field IDs (ints) and column names (strings)
        field_ids, columns = wfc.filter_fields(fields)

        # Get tuple column names for all the columns that contain any of the
        # field IDs that we want
        field_id_cols = [i for i in self._data_header_tuples
                         if i[0] in field_ids]

        # Now split any columns that the user has requested and add then to
        # the columns that are derived from the field IDs
        field_id_cols.extend(wfc.split_header(columns))

        # Make sure we return a unique list of columns
        return list(set(field_id_cols))


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def dummy_call(value, *args):
    """
    A dummy call that can be used to pass through arguments when method calls
    are dynamically allocated and you want a "catch all"

    Parameters
    ----------
    arg : `Any`
        An argument to return

    Returns
    -------
    arg : `Any`
        The same argument that was passed
    """
    return value


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_int(value, *args):
    """
    Parse the value into an integer

    Parameters
    ----------
    value : str
        The value to cast
    *args
        No effect, present to give a uniform interface to all the casting
        methods

    Returns
    -------
    cast_value : int
        The value cast to an int
    """
    return int(value)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_float(value, *args):
    """
    Parse the value into an float

    Parameters
    ----------
    value : str
        The value to cast
    *args
        No effect, present to give a uniform interface to all the casting
        methods

    Returns
    -------
    cast_value : float
        The value cast to an float
    """
    return float(value)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_enum(value, *args):
    """
    Parse the value into an integer

    Parameters
    ----------
    value : str
        The value to cast
    *args
        The enumeration class that acts as a lookup for the value

    Returns
    -------
    cast_value : :obj:`Enum`
        The value assigned to an enumeration value
    """
    enum_class = args[0].coding_enum
    return enum_class(value)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_date(value, *args):
    """
    Parse the value into an date (or specifically a date holder

    Parameters
    ----------
    value : str
        The value to cast
    *args
        No effect, present to give a uniform interface to all the casting
        methods

    Returns
    -------
    cast_value : `type_casts.DateHolder`
        The value cast to a date and stroed 3 ways in a date holder
    """
    return type_casts.extract_date(value)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == '__main__':
    main()
