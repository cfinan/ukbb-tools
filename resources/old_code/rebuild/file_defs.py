"""
.. include:: ../docs/file_defs.md
"""
from ukbb_tools import columns as col, constants as con, \
    __version__, __name__ as pkg_name
from simple_progress import progress
import argparse
import sys
import os
import pprint as pp


# The prefix for verbose messages and the location to write the messages
MSG_PREFIX = '[info]'
MSG_OUT = sys.stderr


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class ClinCodeExtractBase(object):
    """
    """
    CLINICAL_CODES = []

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @classmethod
    def get_clinical_code(cls, row):
        """
        Extract the clinical code

        Parameters
        ----------
        row : list or dict
            A single data row to extract all the clinical codes from
        """
        codes = {}
        values = []
        for key, ret_key in cls.CLINICAL_CODES:
            # first treat as a dict
            try:
                code = row[key]
                len(code)
                codes[ret_key] = code
            except (KeyError, TypeError):
                # The code is NoneType so we skip
                pass

        return codes, values


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class HesOperationsDef(ClinCodeExtractBase):
    """
    The definition for the HES operations file
    """
    NAME = con.HES_OPERATIONS_FILE

    COLUMNS = [
        col.EID,
        col.HES_OPER_INSTANCE_IDX,
        col.HES_OPER_ARRAY_IDX,
        col.HES_OPER_LEVEL,
        col.HES_OPER_OPERATION_DATE,
        col.HES_OPER_OPCS3,
        col.HES_OPER_OPCS3_NOTES,
        col.HES_OPER_OPCS4,
        col.HES_OPER_OPCS4_NOTES,
        col.HES_OPER_POST_OP_DUR,
        col.HES_OPER_PRE_OP_DUR
    ]

    HEADER = [i.name for i in COLUMNS]

    # The tuple is as follows:
    # (The column name in the file,
    #  The name it will be known as after processing)
    CLINICAL_CODES = [
        (col.HES_OPER_OPCS3.name, con.OPCS3_CODE_TYPE_NAME),
        (col.HES_OPER_OPCS4.name, con.OPCS3_CODE_TYPE_NAME)
    ]

    CASTS = {
        col.EID.name: col.EID.type,
        col.HES_OPER_INSTANCE_IDX.name: col.HES_OPER_INSTANCE_IDX.type,
        col.HES_OPER_ARRAY_IDX.name: col.HES_OPER_ARRAY_IDX.type,
        col.HES_OPER_LEVEL.name: col.HES_OPER_LEVEL.type
    }

    OPT_CASTS = {
        col.HES_OPER_POST_OP_DUR.name: col.HES_OPER_POST_OP_DUR.type,
        col.HES_OPER_PRE_OP_DUR.name: col.HES_OPER_PRE_OP_DUR.type,
        col.HES_OPER_OPERATION_DATE.name: col.HES_OPER_OPERATION_DATE.type
    }


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class GpClinicalDef(ClinCodeExtractBase):
    """
    A handler for the GP clinical file
    """
    NAME = con.GP_CLINICAL_FILE

    COLUMNS = [
        col.EID,
        col.GP_DATA_PROVIDER,
        col.GP_EVENT_DATE,
        col.GP_READ_TWO,
        col.GP_READ_THREE,
        col.GP_VALUE_ONE,
        col.GP_VALUE_TWO,
        col.GP_VALUE_THREE
    ]

    HEADER = [i.name for i in COLUMNS]

    CLINICAL_CODES = [
        (col.GP_READ_TWO.name, con.READ2_CODE_TYPE_NAME),
        (col.GP_READ_THREE.name, con.CTV3_CODE_TYPE_NAME)
    ]

    CASTS = {
        col.EID.name: col.EID.type,
        col.GP_DATA_PROVIDER.name: col.GP_DATA_PROVIDER.type
    }

    OPT_CASTS = {
        col.GP_EVENT_DATE.name: col.GP_EVENT_DATE.type
    }

    # This is used in the clinical codes class method for the GP file
    VALUES = []
    for i in [col.GP_VALUE_ONE, col.GP_VALUE_TWO, col.GP_VALUE_THREE]:
        VALUES.append((i.name, HEADER.index(i.name)))

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @classmethod
    def get_clinical_code(cls, row):
        """
        """
        codes, values = super().get_clinical_code(row)

        for key, idx in cls.VALUES:
            try:
                value = row[key]
                len(value)
                values.append(value)
            except (KeyError, TypeError):
                # The value is NoneType so we skip
                pass

        return codes, values


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class HesDiagnosisDef(ClinCodeExtractBase):
    """
    A handler for the HES diagnosis file
    """
    NAME = con.HES_DIAGNOSIS_FILE

    COLUMNS = [
        col.EID,
        col.HES_DIAG_INSTANCE_IDX,
        col.HES_DIAG_ARRAY_IDX,
        col.HES_DIAG_LEVEL,
        col.HES_DIAG_ICD9,
        col.HES_DIAG_ICD9_NOTES,
        col.HES_DIAG_ICD10,
        col.HES_DIAG_ICD10_NOTES
    ]

    HEADER = [i.name for i in COLUMNS]

    CLINICAL_CODES = [
        (col.HES_DIAG_ICD9.name, con.ICD9_CODE_TYPE_NAME),
        (col.HES_DIAG_ICD10.name, con.ICD10_CODE_TYPE_NAME)
    ]

    CASTS = {
        col.EID.name: col.EID.type,
        col.HES_DIAG_INSTANCE_IDX.name: col.HES_DIAG_INSTANCE_IDX.type,
        col.HES_DIAG_ARRAY_IDX.name: col.HES_DIAG_ARRAY_IDX.type,
        col.HES_DIAG_LEVEL.name: col.HES_DIAG_LEVEL.type,
    }


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class HesEventsDef(object):
    """
    A handler for the HES events file
    """
    NAME = con.HES_EVENTS_FILE

    COLUMNS = [
        col.EID,
        col.HES_EVT_INSTANCE_IDX,
        col.HES_EVT_DATA_SOURCE,
        col.HES_EVT_DATA_SUB_SOURCE,
        col.HES_EVT_EPISODE_START,
        col.HES_EVT_EPISODE_END,
        col.HES_EVT_EPISODE_DURATION,
        col.HES_EVT_BED_YEAR,
        col.HES_EVT_EPISODE_STATUS,
        col.HES_EVT_EPISODE_TYPE,
        col.HES_EVT_EPISODE_ORDER,
        col.HES_EVT_SPELL_IDX,
        col.HES_EVT_SPELL_SEQUENCE,
        col.HES_EVT_SPELL_BEGIN,
        col.HES_EVT_SPELL_END,
        col.HES_EVT_SPELL_DURATION,
        col.HES_EVT_PCT_CODE,
        col.HES_EVT_GP_PCT,
        col.HES_EVT_CAT,
        col.HES_EVT_ADMIT_DECISION_DATE,
        col.HES_EVT_ADMIT_WAIT_TIME,
        col.HES_EVT_ADMIT_DATE,
        col.HES_EVT_ADMIT_METHOD_UNI,
        col.HES_EVT_ADMIT_METHOD,
        col.HES_EVT_ADMIT_SOURCE_UNI,
        col.HES_EVT_ADMIT_SOURCE,
        col.HES_EVT_FIRST_REGULAR_DAY,
        col.HES_EVT_PATIENT_CLASS_UNI,
        col.HES_EVT_PATIENT_CLASS,
        col.HES_EVT_INTENDED_MANAGE_UNI,
        col.HES_EVT_INTENDED_MANAGE,
        col.HES_EVT_MAIN_SPEC_UNI,
        col.HES_EVT_MAIN_SPEC,
        col.HES_EVT_TREAT_SPEC_UNI,
        col.HES_EVT_TREAT_SPEC,
        col.HES_EVT_OPER_STATUS,
        col.HES_EVT_DIS_DATE,
        col.HES_EVT_DIS_METHOD_UNI,
        col.HES_EVT_DIS_METHOD,
        col.HES_EVT_DIS_DEST_UNI,
        col.HES_EVT_DIS_DEST,
        col.HES_EVT_CARER_SUPPORT
    ]

    HEADER = [i.name for i in COLUMNS]

    OPT_CASTS = {
        col.HES_EVT_EPISODE_START.name: col.HES_EVT_EPISODE_START.type,
        col.HES_EVT_EPISODE_END.name: col.HES_EVT_EPISODE_END.type,
        col.HES_EVT_ADMIT_DATE.name: col.HES_EVT_ADMIT_DATE.type,
        col.HES_EVT_ADMIT_DECISION_DATE.name:
        col.HES_EVT_ADMIT_DECISION_DATE.type,
        col.HES_EVT_DIS_DATE.name: col.HES_EVT_DIS_DATE.type,
        col.EID.name: col.EID.type,
        col.HES_EVT_INSTANCE_IDX.name: col.HES_EVT_INSTANCE_IDX.type,
        col.HES_EVT_DATA_SUB_SOURCE.name: col.HES_EVT_DATA_SUB_SOURCE.type,
        col.HES_EVT_EPISODE_DURATION.name: col.HES_EVT_EPISODE_DURATION.type,
        col.HES_EVT_BED_YEAR.name: col.HES_EVT_BED_YEAR.type,
        col.HES_EVT_EPISODE_STATUS.name: col.HES_EVT_EPISODE_STATUS.type,
        col.HES_EVT_EPISODE_TYPE.name: col.HES_EVT_EPISODE_TYPE.type,
        col.HES_EVT_EPISODE_ORDER.name: col.HES_EVT_EPISODE_ORDER.type,
        col.HES_EVT_SPELL_IDX.name: col.HES_EVT_SPELL_IDX.type,
        col.HES_EVT_SPELL_SEQUENCE.name: col.HES_EVT_SPELL_SEQUENCE.type,
        col.HES_EVT_SPELL_BEGIN.name: col.HES_EVT_SPELL_BEGIN.type,
        col.HES_EVT_SPELL_DURATION.name: col.HES_EVT_SPELL_DURATION.type,
        col.HES_EVT_CAT.name: col.HES_EVT_CAT.type,
        col.HES_EVT_ADMIT_WAIT_TIME.name: col.HES_EVT_ADMIT_WAIT_TIME.type,
        col.HES_EVT_ADMIT_METHOD_UNI.name: col.HES_EVT_ADMIT_METHOD_UNI.type,
        col.HES_EVT_ADMIT_METHOD.name: col.HES_EVT_ADMIT_METHOD.type,
        col.HES_EVT_FIRST_REGULAR_DAY.name: col.HES_EVT_FIRST_REGULAR_DAY.type,
        col.HES_EVT_PATIENT_CLASS_UNI.name: col.HES_EVT_PATIENT_CLASS_UNI.type,
        col.HES_EVT_INTENDED_MANAGE_UNI.name:
        col.HES_EVT_INTENDED_MANAGE_UNI.type,
        col.HES_EVT_INTENDED_MANAGE.name: col.HES_EVT_INTENDED_MANAGE.type,
        col.HES_EVT_MAIN_SPEC_UNI.name: col.HES_EVT_MAIN_SPEC_UNI.type,
        col.HES_EVT_TREAT_SPEC_UNI.name: col.HES_EVT_TREAT_SPEC_UNI.type,
        col.HES_EVT_OPER_STATUS.name: col.HES_EVT_OPER_STATUS.type,
        col.HES_EVT_DIS_METHOD_UNI.name: col.HES_EVT_DIS_METHOD_UNI.type,
        col.HES_EVT_DIS_METHOD.name: col.HES_EVT_DIS_METHOD.type,
        col.HES_EVT_DIS_DEST_UNI.name: col.HES_EVT_DIS_DEST_UNI.type,
        col.HES_EVT_CARER_SUPPORT.name: col.HES_EVT_CARER_SUPPORT.type
    }


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class GpRegistrationsDef(object):
    """
    A handler for the GP registrations file
    """
    NAME = con.GP_REGISTRATIONS_FILE

    COLUMNS = [
        col.EID,
        col.GP_REG_DATA_PROVIDER,
        col.GP_REG_REG_DATE,
        col.GP_REG_UNREG_DATE
    ]

    HEADER = [i.name for i in COLUMNS]

    CASTS = {
        col.EID.name: col.EID.type,
        col.GP_REG_DATA_PROVIDER.name: col.GP_REG_DATA_PROVIDER.type
    }

    OPT_CASTS = {
        col.GP_REG_REG_DATE.name: col.GP_REG_REG_DATE.type,
        col.GP_REG_UNREG_DATE.name: col.GP_REG_UNREG_DATE.type
    }


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# ----------------------------- OUTPUT FILE DEFS ------------------------------
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class SamplesDef(object):
    """
    A handler for the samples file that is built from the wide file
    """
    COLUMNS = [
        col.EID,
        col.SAM_DATE_OF_BIRTH,
        col.SAM_DATE_OF_BIRTH_INT,
        col.SAM_DATE_OF_ASSESS,
        col.SAM_DATE_OF_ASSESS_INT,
        col.SAM_AGE_OF_ASSESS,
        col.SAM_SR_SEX,
        col.SAM_GEN_SEX,
        col.SAM_DATE_OF_DEATH,
        col.SAM_DATE_OF_DEATH_INT,
        col.SAM_AGE_OF_DEATH,
        col.SAM_N_ASSESSMENTS,
        col.SAM_IS_CAUCASIAN,
        col.SAM_IS_GENOTYPED,
        col.SAM_HAS_WITHDRAWN
    ]

    HEADER = [i.name for i in COLUMNS]

    CASTS = {
        col.EID.name: col.EID.type,
        col.SAM_HAS_WITHDRAWN.name: col.SAM_HAS_WITHDRAWN.type,
        col.SAM_IS_GENOTYPED.name: col.SAM_IS_GENOTYPED.type
    }

    OPT_CASTS = {
        col.SAM_DATE_OF_BIRTH.name: col.SAM_DATE_OF_BIRTH.type,
        col.SAM_DATE_OF_BIRTH_INT.name: col.SAM_DATE_OF_BIRTH_INT.type,
        col.SAM_DATE_OF_ASSESS.name: col.SAM_DATE_OF_ASSESS.type,
        col.SAM_DATE_OF_ASSESS_INT.name: col.SAM_DATE_OF_ASSESS_INT.type,
        col.SAM_SR_SEX.name: col.SAM_SR_SEX.type,
        col.SAM_N_ASSESSMENTS.name: col.SAM_N_ASSESSMENTS.type,
        col.SAM_GEN_SEX.name: col.SAM_GEN_SEX.type,
        col.SAM_DATE_OF_DEATH.name: col.SAM_DATE_OF_DEATH.type,
        col.SAM_DATE_OF_DEATH_INT.name: col.SAM_DATE_OF_DEATH_INT.type,
        col.SAM_AGE_OF_ASSESS.name: col.SAM_AGE_OF_ASSESS.type,
        col.SAM_AGE_OF_DEATH.name: col.SAM_AGE_OF_DEATH.type,
        col.SAM_IS_CAUCASIAN.name: col.SAM_IS_CAUCASIAN.type
    }


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class ClinicalCodesDef(object):
    """
    A handler for the GP registrations file
    """
    COLUMNS = [
        col.CLIN_CODE_IDX,
        col.CLIN_CODE_UKBB_SAB,
        col.CLIN_CODE_UKBB_CODE,
        col.CLIN_CODE_UMLS_CUI,
        col.CLIN_CODE_UMLS_SAB,
        col.CLIN_CODE_UMLS_TERM,
        col.CLIN_CODE_UMLS_STY,
        col.CLIN_CODE_UKBB_TERM,
        col.CLIN_CODE_MIN_N_VALUES,
        col.CLIN_CODE_MAX_N_VALUES,
        col.CLIN_CODE_N_OCCURS,
        col.CLIN_CODE_N_IS_EMPTY,
        col.CLIN_CODE_N_IS_STRING,
        col.CLIN_CODE_N_IS_INTEGER,
        col.CLIN_CODE_N_IS_FLOAT,
        col.CLIN_CODE_RATIO_300,
        col.CLIN_CODE_RATIO_030,
        col.CLIN_CODE_RATIO_003,
        col.CLIN_CODE_RATIO_210,
        col.CLIN_CODE_RATIO_201,
        col.CLIN_CODE_RATIO_120,
        col.CLIN_CODE_RATIO_021,
        col.CLIN_CODE_RATIO_102,
        col.CLIN_CODE_RATIO_012,
        col.CLIN_CODE_RATIO_111
    ]

    HEADER = [i.name for i in COLUMNS]

    CASTS = {
        col.CLIN_CODE_IDX.name: col.CLIN_CODE_IDX.type,
        col.CLIN_CODE_MIN_N_VALUES.name: col.CLIN_CODE_MIN_N_VALUES.type,
        col.CLIN_CODE_MAX_N_VALUES.name: col.CLIN_CODE_MAX_N_VALUES.type,
        col.CLIN_CODE_N_OCCURS.name: col.CLIN_CODE_N_OCCURS.type,
        col.CLIN_CODE_N_IS_EMPTY.name: col.CLIN_CODE_N_IS_EMPTY.type,
        col.CLIN_CODE_N_IS_STRING.name: col.CLIN_CODE_N_IS_STRING.type,
        col.CLIN_CODE_N_IS_INTEGER.name: col.CLIN_CODE_N_IS_INTEGER.type,
        col.CLIN_CODE_N_IS_FLOAT.name: col.CLIN_CODE_N_IS_FLOAT.type,
        col.CLIN_CODE_RATIO_300.name: col.CLIN_CODE_RATIO_300.type,
        col.CLIN_CODE_RATIO_030.name: col.CLIN_CODE_RATIO_030.type,
        col.CLIN_CODE_RATIO_003.name: col.CLIN_CODE_RATIO_003.type,
        col.CLIN_CODE_RATIO_210.name: col.CLIN_CODE_RATIO_210.type,
        col.CLIN_CODE_RATIO_201.name: col.CLIN_CODE_RATIO_201.type,
        col.CLIN_CODE_RATIO_120.name: col.CLIN_CODE_RATIO_120.type,
        col.CLIN_CODE_RATIO_021.name: col.CLIN_CODE_RATIO_021.type,
        col.CLIN_CODE_RATIO_102.name: col.CLIN_CODE_RATIO_102.type,
        col.CLIN_CODE_RATIO_012.name: col.CLIN_CODE_RATIO_012.type,
        col.CLIN_CODE_RATIO_111.name: col.CLIN_CODE_RATIO_111.type
    }

    OPT_CASTS = {
        col.CLIN_CODE_UKBB_TERM.name: col.CLIN_CODE_UKBB_TERM.type,
        col.CLIN_CODE_UMLS_STY.name: col.CLIN_CODE_UMLS_STY.type,
        col.CLIN_CODE_UMLS_TERM.name: col.CLIN_CODE_UMLS_TERM.type,
    }


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class ClinicalDataDef(object):
    """
    Header and data type definitions for the clinical_data file
    """
    COLUMNS = [
        col.CLIN_DATA_EVENT_IDX,
        col.CLIN_DATA_EPISODE_IDX,
        col.EID,
        col.CLIN_DATA_EVENT_START_STR,
        col.CLIN_DATA_EVENT_START_INT,
        col.CLIN_DATA_AGE_AT_EVENT,
        col.CLIN_DATA_DAYS_FROM_BASELINE,
        col.CLIN_DATA_DAYS_PRE_EVENT,
        col.CLIN_DATA_DAYS_POST_EVENT,
        col.CLIN_DATA_CLINICAL_CODE,
        col.CLIN_DATA_CLINICAL_CODE_SYSTEM,
        col.CLIN_DATA_CLINICAL_CODE_DESC,
        col.CLIN_DATA_UMLS_CUI,
        col.CLIN_DATA_EVENT_LEVEL,
        col.CLIN_DATA_CARE_TYPE,
        col.CLIN_DATA_RECORD_TYPE,
        col.CLIN_DATA_DATA_PROVDER,
        col.CLIN_DATA_DATA_PROVDER_FORMAT,
        col.CLIN_DATA_PCT,
        col.CLIN_DATA_DAYS_TO_PCT,
        col.CLIN_DATA_PCT_IS_GP,
        col.CLIN_DATA_IS_CLINICAL_MEAS,
        col.CLIN_DATA_NUMERICS,
        col.CLIN_DATA_NORM_NUMERICS,
        col.CLIN_DATA_UNITS,
        col.CLIN_DATA_STRINGS,
        col.CLIN_DATA_CLINICAL_NOTES
    ]

    HEADER = [i.name for i in COLUMNS]

    CASTS = {
        col.CLIN_DATA_EVENT_IDX.name: col.CLIN_DATA_EVENT_IDX.type,
        col.CLIN_DATA_EPISODE_IDX.name: col.CLIN_DATA_EPISODE_IDX.type,
        col.EID.name: col.EID.type,
        col.CLIN_DATA_CLINICAL_CODE_DESC.name:
        col.CLIN_DATA_CLINICAL_CODE_DESC.type,
        col.CLIN_DATA_EVENT_LEVEL.name: col.CLIN_DATA_EVENT_LEVEL.type,
        col.CLIN_DATA_DATA_PROVDER_FORMAT.name:
        col.CLIN_DATA_DATA_PROVDER_FORMAT.type,
        col.CLIN_DATA_IS_CLINICAL_MEAS.name:
        col.CLIN_DATA_IS_CLINICAL_MEAS.type,
        col.CLIN_DATA_NUMERICS.name: col.CLIN_DATA_NUMERICS.type,
        col.CLIN_DATA_NORM_NUMERICS.name: col.CLIN_DATA_NORM_NUMERICS.type,
        col.CLIN_DATA_UNITS.name: col.CLIN_DATA_UNITS.type,
        col.CLIN_DATA_STRINGS.name: col.CLIN_DATA_STRINGS.type,
        col.CLIN_DATA_CLINICAL_NOTES.name: col.CLIN_DATA_CLINICAL_NOTES.type
    }

    OPT_CASTS = {
        col.CLIN_DATA_DAYS_FROM_BASELINE.name:
        col.CLIN_DATA_DAYS_FROM_BASELINE.type,
        col.CLIN_DATA_DAYS_TO_PCT.name: col.CLIN_DATA_DAYS_TO_PCT.type,
        col.CLIN_DATA_PCT_IS_GP.name: col.CLIN_DATA_PCT_IS_GP.type,
        col.CLIN_DATA_EVENT_START_STR.name: col.CLIN_DATA_EVENT_START_STR.type,
        col.CLIN_DATA_EVENT_START_INT.name: col.CLIN_DATA_EVENT_START_INT.type,
        col.CLIN_DATA_AGE_AT_EVENT.name: col.CLIN_DATA_AGE_AT_EVENT.type,
        col.CLIN_DATA_DAYS_PRE_EVENT.name: col.CLIN_DATA_DAYS_PRE_EVENT.type,
        col.CLIN_DATA_DAYS_POST_EVENT.name: col.CLIN_DATA_DAYS_POST_EVENT.type
    }


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class UkbbEnumDef(object):
    """
    Header and data type definitions for the clinical_data file
    """
    COLUMNS = [
        col.EID,
        col.UKBB_ENUM_BASELINE_DATE,
        col.UKBB_ENUM_BASELINE_INT_DATE,
        col.UKBB_ENUM_FIELD_TEXT,
        col.UKBB_ENUM_FIELD_ID,
        col.UKBB_ENUM_INST_ID,
        col.UKBB_ENUM_ARRAY_ID,
        col.UKBB_ENUM_REL_DATE_FIELD_TEXT,
        col.UKBB_ENUM_REL_DATE_FIELD_ID,
        col.UKBB_ENUM_REL_DATE_FIELD_INST_ID,
        col.UKBB_ENUM_REL_DATE_ARRAY_ID,
        col.UKBB_ENUM_REL_DATE,
        col.UKBB_ENUM_REL_INT_DATE,
        col.UKBB_ENUM_EVENT_DAYS_FROM_BASELINE,
        col.UKBB_ENUM_AGE_YEARS_AT_EVENT,
        col.UKBB_ENUM_IS_FALLBACK_DATE,
        col.UKBB_ENUM_VALUE
    ]

    HEADER = [i.name for i in COLUMNS]

    CASTS = {
        col.EID.name: col.EID.type,
        col.UKBB_ENUM_BASELINE_DATE.name: col.UKBB_ENUM_BASELINE_DATE.type,
        col.UKBB_ENUM_BASELINE_INT_DATE.name:
        col.UKBB_ENUM_BASELINE_INT_DATE.type,
        col.UKBB_ENUM_FIELD_ID.name: col.UKBB_ENUM_FIELD_ID.type,
        col.UKBB_ENUM_INST_ID.name: col.UKBB_ENUM_INST_ID.type,
        col.UKBB_ENUM_ARRAY_ID.name: col.UKBB_ENUM_ARRAY_ID.type,
        col.UKBB_ENUM_REL_DATE_ARRAY_ID.name:
        col.UKBB_ENUM_REL_DATE_ARRAY_ID.type,
        col.UKBB_ENUM_REL_DATE.name:
        col.UKBB_ENUM_REL_DATE.type,
        col.UKBB_ENUM_IS_FALLBACK_DATE.name:
        col.UKBB_ENUM_IS_FALLBACK_DATE.type
    }

    OPT_CASTS = {
        col.UKBB_ENUM_REL_DATE_FIELD_ID.name:
        col.UKBB_ENUM_REL_DATE_FIELD_ID.type,
        col.UKBB_ENUM_REL_DATE_FIELD_INST_ID.name:
        col.UKBB_ENUM_REL_DATE_FIELD_INST_ID.type,
        col.UKBB_ENUM_REL_INT_DATE.name:
        col.UKBB_ENUM_REL_INT_DATE.type,
        col.UKBB_ENUM_EVENT_DAYS_FROM_BASELINE.name:
        col.UKBB_ENUM_EVENT_DAYS_FROM_BASELINE.type,
        col.UKBB_ENUM_AGE_YEARS_AT_EVENT.name:
        col.UKBB_ENUM_AGE_YEARS_AT_EVENT.type
    }


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class UkbbDateDef(object):
    """
    Header and data type definitions for the clinical_data file
    """
    COLUMNS = [
        col.EID,
        col.UKBB_DATE_BASELINE_DATE,
        col.UKBB_DATE_BASELINE_INT_DATE,
        col.UKBB_DATE_FIELD_TEXT,
        col.UKBB_DATE_FIELD_ID,
        col.UKBB_DATE_INST_ID,
        col.UKBB_DATE_ARRAY_ID,
        col.UKBB_DATE_REL_DATE_FIELD_TEXT,
        col.UKBB_DATE_REL_DATE_FIELD_ID,
        col.UKBB_DATE_REL_DATE_FIELD_INST_ID,
        col.UKBB_DATE_REL_DATE_ARRAY_ID,
        col.UKBB_DATE_REL_DATE,
        col.UKBB_DATE_REL_INT_DATE,
        col.UKBB_DATE_EVENT_DAYS_FROM_BASELINE,
        col.UKBB_DATE_AGE_YEARS_AT_EVENT,
        col.UKBB_DATE_IS_FALLBACK_DATE,
        col.UKBB_DATE_VALUE
    ]

    HEADER = [i.name for i in COLUMNS]

    CASTS = {
        col.EID.name: col.EID.type,
        col.UKBB_DATE_BASELINE_DATE.name: col.UKBB_DATE_BASELINE_DATE.type,
        col.UKBB_DATE_BASELINE_INT_DATE.name:
        col.UKBB_DATE_BASELINE_INT_DATE.type,
        col.UKBB_DATE_FIELD_ID.name: col.UKBB_DATE_FIELD_ID.type,
        col.UKBB_DATE_INST_ID.name: col.UKBB_DATE_INST_ID.type,
        col.UKBB_DATE_ARRAY_ID.name: col.UKBB_DATE_ARRAY_ID.type,
        col.UKBB_DATE_REL_DATE_ARRAY_ID.name:
        col.UKBB_DATE_REL_DATE_ARRAY_ID.type,
        col.UKBB_DATE_REL_DATE.name:
        col.UKBB_DATE_REL_DATE.type,
        col.UKBB_DATE_IS_FALLBACK_DATE.name:
        col.UKBB_DATE_IS_FALLBACK_DATE.type,
        col.UKBB_DATE_VALUE.name:
        col.UKBB_DATE_VALUE.type
    }

    OPT_CASTS = {
        col.UKBB_DATE_REL_DATE_FIELD_ID.name:
        col.UKBB_DATE_REL_DATE_FIELD_ID.type,
        col.UKBB_DATE_REL_DATE_FIELD_INST_ID.name:
        col.UKBB_DATE_REL_DATE_FIELD_INST_ID.type,
        col.UKBB_DATE_REL_INT_DATE.name:
        col.UKBB_DATE_REL_INT_DATE.type,
        col.UKBB_DATE_EVENT_DAYS_FROM_BASELINE.name:
        col.UKBB_DATE_EVENT_DAYS_FROM_BASELINE.type,
        col.UKBB_DATE_AGE_YEARS_AT_EVENT.name:
        col.UKBB_DATE_AGE_YEARS_AT_EVENT.type
    }


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class UkbbStringDef(object):
    """
    Header and data type definitions for the clinical_data file
    """
    COLUMNS = [
        col.EID,
        col.UKBB_STR_BASELINE_DATE,
        col.UKBB_STR_BASELINE_INT_DATE,
        col.UKBB_STR_FIELD_TEXT,
        col.UKBB_STR_FIELD_ID,
        col.UKBB_STR_INST_ID,
        col.UKBB_STR_ARRAY_ID,
        col.UKBB_STR_REL_DATE_FIELD_TEXT,
        col.UKBB_STR_REL_DATE_FIELD_ID,
        col.UKBB_STR_REL_DATE_FIELD_INST_ID,
        col.UKBB_STR_REL_DATE_ARRAY_ID,
        col.UKBB_STR_REL_DATE,
        col.UKBB_STR_REL_INT_DATE,
        col.UKBB_STR_EVENT_DAYS_FROM_BASELINE,
        col.UKBB_STR_AGE_YEARS_AT_EVENT,
        col.UKBB_STR_IS_FALLBACK_DATE,
        col.UKBB_STR_VALUE
    ]

    HEADER = [i.name for i in COLUMNS]

    CASTS = {
        col.EID.name: col.EID.type,
        col.UKBB_STR_BASELINE_DATE.name: col.UKBB_STR_BASELINE_DATE.type,
        col.UKBB_STR_BASELINE_INT_DATE.name:
        col.UKBB_STR_BASELINE_INT_DATE.type,
        col.UKBB_STR_FIELD_ID.name: col.UKBB_STR_FIELD_ID.type,
        col.UKBB_STR_INST_ID.name: col.UKBB_STR_INST_ID.type,
        col.UKBB_STR_ARRAY_ID.name: col.UKBB_STR_ARRAY_ID.type,
        col.UKBB_STR_REL_DATE_ARRAY_ID.name:
        col.UKBB_STR_REL_DATE_ARRAY_ID.type,
        col.UKBB_STR_REL_DATE.name:
        col.UKBB_STR_REL_DATE.type,
        col.UKBB_STR_IS_FALLBACK_DATE.name:
        col.UKBB_STR_IS_FALLBACK_DATE.type
    }

    OPT_CASTS = {
        col.UKBB_STR_REL_DATE_FIELD_ID.name:
        col.UKBB_STR_REL_DATE_FIELD_ID.type,
        col.UKBB_STR_REL_DATE_FIELD_INST_ID.name:
        col.UKBB_STR_REL_DATE_FIELD_INST_ID.type,
        col.UKBB_STR_REL_INT_DATE.name:
        col.UKBB_STR_REL_INT_DATE.type,
        col.UKBB_STR_EVENT_DAYS_FROM_BASELINE.name:
        col.UKBB_STR_EVENT_DAYS_FROM_BASELINE.type,
        col.UKBB_STR_AGE_YEARS_AT_EVENT.name:
        col.UKBB_STR_AGE_YEARS_AT_EVENT.type
    }


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class UkbbNumericDef(object):
    """
    Header and data type definitions for the clinical_data file
    """
    COLUMNS = [
        col.EID,
        col.UKBB_NUM_BASELINE_DATE,
        col.UKBB_NUM_BASELINE_INT_DATE,
        col.UKBB_NUM_FIELD_TEXT,
        col.UKBB_NUM_FIELD_ID,
        col.UKBB_NUM_INST_ID,
        col.UKBB_NUM_ARRAY_ID,
        col.UKBB_NUM_REL_DATE_FIELD_TEXT,
        col.UKBB_NUM_REL_DATE_FIELD_ID,
        col.UKBB_NUM_REL_DATE_FIELD_INST_ID,
        col.UKBB_NUM_REL_DATE_ARRAY_ID,
        col.UKBB_NUM_REL_DATE,
        col.UKBB_NUM_REL_INT_DATE,
        col.UKBB_NUM_EVENT_DAYS_FROM_BASELINE,
        col.UKBB_NUM_AGE_YEARS_AT_EVENT,
        col.UKBB_NUM_IS_FALLBACK_DATE,
        col.UKBB_NUM_VALUE
    ]

    HEADER = [i.name for i in COLUMNS]

    CASTS = {
        col.EID.name: col.EID.type,
        col.UKBB_NUM_BASELINE_DATE.name: col.UKBB_NUM_BASELINE_DATE.type,
        col.UKBB_NUM_BASELINE_INT_DATE.name:
        col.UKBB_NUM_BASELINE_INT_DATE.type,
        col.UKBB_NUM_FIELD_ID.name: col.UKBB_NUM_FIELD_ID.type,
        col.UKBB_NUM_INST_ID.name: col.UKBB_NUM_INST_ID.type,
        col.UKBB_NUM_ARRAY_ID.name: col.UKBB_NUM_ARRAY_ID.type,
        col.UKBB_NUM_REL_DATE_ARRAY_ID.name:
        col.UKBB_NUM_REL_DATE_ARRAY_ID.type,
        col.UKBB_NUM_REL_DATE.name:
        col.UKBB_NUM_REL_DATE.type,
        col.UKBB_NUM_IS_FALLBACK_DATE.name:
        col.UKBB_NUM_IS_FALLBACK_DATE.type,
        col.UKBB_NUM_VALUE.name:
        col.UKBB_NUM_VALUE.type
    }

    OPT_CASTS = {
        col.UKBB_NUM_REL_DATE_FIELD_ID.name:
        col.UKBB_NUM_REL_DATE_FIELD_ID.type,
        col.UKBB_NUM_REL_DATE_FIELD_INST_ID.name:
        col.UKBB_NUM_REL_DATE_FIELD_INST_ID.type,
        col.UKBB_NUM_REL_INT_DATE.name:
        col.UKBB_NUM_REL_INT_DATE.type,
        col.UKBB_NUM_EVENT_DAYS_FROM_BASELINE.name:
        col.UKBB_NUM_EVENT_DAYS_FROM_BASELINE.type,
        col.UKBB_NUM_AGE_YEARS_AT_EVENT.name:
        col.UKBB_NUM_AGE_YEARS_AT_EVENT.type
    }


DEF_CLASSES = [
    HesOperationsDef,
    GpClinicalDef,
    HesDiagnosisDef,
    HesEventsDef,
    GpRegistrationsDef
    # SamplesDef,
    # ClinicalCodesDef,
    # ClinicalDataDef,
    # UkbbEnumDef,
    # UkbbDateDef,
    # UkbbNumericDef,
    # UkbbStringDef
]


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main():
    """
    The main entry point for the script
    """
    # Initialise and parse the command line arguments
    parser = init_cmd_args()
    args = parse_cmd_args(parser)

    # Start a msg output, this will respect the verbosity and will output to
    # STDERR
    m = progress.Msg(prefix='', verbose=args.verbose, file=MSG_OUT)
    m.msg("= UKBB Info ({0} v{1}) =".format(
        pkg_name, __version__))
    m.prefix = MSG_PREFIX
    m.msg_atts(args)

    if args.col_number is True:
        output_column_number(args)
    elif args.out_format == 'long':
        output_long_format(args)
    elif args.out_format == 'wide':
        output_wide_format(args)
    else:
        raise ValueError(
            "unknown output format: '{0}'".format(args.out_format)
        )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def init_cmd_args():
    """
    Initialise the command line arguments and return the parsed arguments

    Returns
    -------
    args : :obj:`argparse.Namespace`
        The argparse namespace object
    """
    parser = argparse.ArgumentParser(
        description="Get info on columns in the UKBB data files"
    )

    # The wide format UKBB data fields file
    parser.add_argument(
        'file_type',
        type=str,
        help="A section heading for a file in the config file"
    )
    parser.add_argument(
        'columns',
        nargs='*',
        type=str,
        help="Any specific columns you are interested in, if none are given"
        " then information on all columns is retuned. If -n is used then "
        "columns must be a single column name"
    )
    parser.add_argument(
        '-o',
        '--out-format',
        choices=['wide', 'long'],
        default='wide',
        help="The output format of the column descriptions either 'wide' "
        "(the default) or 'long'"
    )
    parser.add_argument(
        '-v',
        '--verbose',
        action="store_true",
        help="give more output"
    )
    parser.add_argument(
        '-n',
        '--col-number',
        action="store_true",
        help="Return the column number (1-based) for a column name in the"
        " file"
    )

    return parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_cmd_args(parser):
    """
    Initialise the command line arguments and return the parsed arguments

    Returns
    -------
    args : :obj:`argparse.Namespace`
        The argparse namespace object
    """
    args = parser.parse_args()

    # Make sure the columns argument is correct for the col_number argument
    if args.col_number is True and len(args.columns) != 1:
        raise ValueError("must supply a single column name when --col-number "
                         "is used")

    args.file_def_name, args.file_def_index, args.file_def_class = \
        get_parsing_definition(
            args.file_type
        )
    return args


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_parsing_definition(name):
    """
    When given a name, this will look through all the known definitions and
    attempt to find a matching one.

    Parameters
    ----------
    name : str
        A section heading name from the config file. This may or may not be
        suffixed with an index value, i.e. hes_events_file.eid

    Returns
    -------
    file_def_name : str
        The file definition name that is a processed form of `name`
    file_def_idx : str or NoneType
        Any index column suffix that is associated with the `name` that has
        been passed
    file_def_class : `FileDefinition`
        A file definition class that provides access to the column data for
        file definition name

    Raises
    ------
    ValueError
        If when processing the `name` there is > 1 `.` delimiter
    KeyError
        If the processed `name` does not have a matching file definition class
    """
    name_components = name.split('.')
    if len(name_components) > 2:
        raise ValueError("unexpected number of delimiters in name")

    file_def_name = name_components[0]

    try:
        file_def_idx = name_components[1]
    except IndexError:
        file_def_idx = None

    for i in DEF_CLASSES:
        if i.NAME == file_def_name:
            return file_def_name, file_def_idx, i

    raise KeyError("no definitions for name: '{0}'".format(file_def_name))


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def output_column_number(args):
    """

    """
    for idx, i in yield_columns(args.file_def_class.COLUMNS, args.columns):
        print(idx+1)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def output_long_format(args):
    """

    """
    print("file_type\tkey\tvalue")

    for idx, i in yield_columns(args.file_def_class.COLUMNS, args.columns):
        print(
            "{0}\t{1}\t{2}".format(
                args.file_def_class.NAME, "col_no", idx+1
            )
        )
        print(
            "{0}\t{1}\t{2}".format(
                args.file_def_class.NAME, "name", i.name
            )
        )
        print(
            "{0}\t{1}\t{2}".format(
                args.file_def_class.NAME, "parse_func", i.type.__name__
            )
        )
        print(
            "{0}\t{1}\t{2}".format(
                args.file_def_class.NAME, "desc", i.desc
            )
        )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def output_wide_format(args):
    """

    """
    print("col_no\tcol_name\tparse_func\tdescription")
    for idx, i in yield_columns(args.file_def_class.COLUMNS, args.columns):
        print(
            "{0}\t{1}\t{2}\t{3}".format(
                idx+1, i.name, i.type.__name__, i.desc
            )
        )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def yield_columns(all_columns, required_columns):
    """

    """
    if len(required_columns) == 0:
        for idx, i in enumerate(all_columns):
            yield idx, i
    else:
        for idx, i in enumerate(all_columns):
            if i.name in required_columns:
                required_columns.pop(required_columns.index(i.name))
                yield idx, i

            if len(required_columns) == 0:
                break

        if len(required_columns) > 0:
            raise KeyError(
                "columns not found: '{0}'".format(",".join(required_columns))
            )


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == '__main__':
    main()
