"""
.. include:: ../docs/clinical_code_parser.md
"""
from ukbb_tools import file_handler, common, umls_mapper
from simple_progress import progress

# TODO: Remove in production
unit_set = {}


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class ClinicalCodeParser(object):
    """
    Extract a unique set of clinical codes from the various sources. The
    sources that we use are:
    1. HES diagnosis data
    2. HES Operations/Procedures data
    3. Primary Care diagnosis and measures

    The will extract the codes and some basic summary stats for each code,
    such as, number of occurrences, is it associated with a value. Finally an
    attempt is made to map all the codes back to UMLS concepts so they are
    standardised
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, hes_diag, hes_oper, gp_clinical, umls_sm, ukbb_map_sm,
                 verbose=False):
        """
        Parameters
        ----------
        hes_diag : tuple
            The path to the hes diagnosis file should be at [0] and the csv
            parameters and encoding should be in a dict at [1]
        hes_oper : tuple
            The path to the HES operations file should be at [0] and the csv
            parameters and encoding should be in a dict at [1]
        gp_clinical : tuple
            The path to the GP clinical file should be at [0] and the csv
            parameters and encoding should be in a dict at [1]
        umls_sm : :obj:`sqlalchemy.Sessionmaker`
            The sessionmaker for the UMLS database
        ukbb_map_sm : :obj:`sqlalchemy.Sessionmaker`
            The sessionmaker for the UKBB mapping database
        verbose, optional, default: False
            Output the progress at all stages
        """
        self.hes_diag_reader = None
        self.hes_oper_reader = None
        self.gp_clinical_reader = None
        self.umls_sm = umls_sm
        self.ukbb_map_sm = ukbb_map_sm
        self.sample_eids = []
        self.verbose = verbose
        self._open = False

        self.hes_diag_reader = file_handler.HesDiagnosisFile(
            hes_diag[0], cache=True, **hes_diag[1]
        )

        self.hes_oper_reader = file_handler.HesOperationsFile(
            hes_oper[0], cache=True, **hes_oper[1]
        )

        self.gp_clinical_reader = file_handler.GpClinicalFile(
            gp_clinical[0], cache=True, **gp_clinical[1]
        )

        # Create a mapper
        self.mapper = umls_mapper.UkbbToUmlsMapper(
            umls_sm,
            ukbb_map_sm
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __enter__(self):
        """
        Entry point for the context manager, must return self
        """
        self.open()
        return self

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __exit__(self, *args):
        """
        Exit the context manager

        Parameters
        ----------
        *args
            Arguments passed to the context manager when exiting the scope
            of the context manager
        """
        self.close()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def open(self):
        """
        Open all the index readers and get a union of all the samples that
        exist in all of them
        """
        # Make sure we are not open already
        self._check_is_not_open()

        self.hes_diag_reader.open()
        self.hes_oper_reader.open()
        self.gp_clinical_reader.open()

        self.sample_eids = file_handler.sample_union(
            self.hes_diag_reader,
            self.hes_oper_reader,
            self.gp_clinical_reader
        )
        self.sample_eids.sort()

        # Indicate that we have been opened
        self._open = True

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def close(self):
        """
        Close all index files
        """
        # Make sure that we are opened
        self._check_is_open()

        for reader in [self.hes_diag_reader, self.hes_oper_reader,
                       self.gp_clinical_reader]:
            try:
                reader.close()
            except AttributeError:
                # NoneType has not been opened yet
                pass
        # Indicate that we are now closed
        self._open = False

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def parse(self):
        """
        Parse all the codes for all the samples, this will loop through all
        of the EIDS and attempt to map all of the clinical codes into the UMLS
        it will also gather basic stats on the clinical codes, such as number
        of occurrences, number with empty values etc..
        """
        self.code_stats = {}
        self.cui_counts = {}
        prog = progress.RemainingProgress(
            len(self.sample_eids),
            verbose=self.verbose
        )
        count = 1
        target = -1
        for i in prog.progress(self.sample_eids):
            self._parse_eid(i)
            if count == target:
                break
            count += 1

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _parse_eid(self, eid):
        """
        Parse a sample ID from all of the index files

        Parameters
        ----------
        eid : int
            A single sample ID to extract from all of the index files
        """
        self._current_eid_stats = {}
        self._parse_codes(self.hes_diag_reader, eid)
        self._parse_codes(self.hes_oper_reader, eid)
        self._parse_codes(self.gp_clinical_reader, eid)

        # Now transfer the current eid_stats to the cui counts
        for cui, count in self._current_eid_stats.items():
            try:
                self.cui_counts[cui[0]][0] += 1
                self.cui_counts[cui[0]][1] += count
            except KeyError:
                self.cui_counts[cui[0]] = [1, count, cui[1], cui[2]]

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _parse_codes(self, reader, eid):
        """
        Parse a sample ID from a single index file

        Parameters
        ----------
        reader : :obj:`file_handler.UkbbIndexReader`
            An object that inherits from `file_handler.UkbbIndexReader`
        eid : int
            A single sample ID to extract from the index file
        """
        # Get the rows for the sample from the reader. This returns rows which
        # is a list of dicts, where the keys of the dict represent columns in
        # the file represented by the reader
        rows = common.fetch_sample_data(reader, eid)

        # Do something with the rows
        for row in rows:
            # Each reader class has a get_clinical_code class method that takes
            # a row derived from the object and returns a tuple of clinical
            # codes. Element [0] of the tuple is a dict with coding system as
            # keys and clinical code as values. Element [1] of the tuple is
            # a list values that have been assigned to the codes. Note in
            # the HES data this will be empty for all codes
            codes, code_values = reader.__class__.get_clinical_code(row)

            # Loop through all the codes and process them
            for ont, code in codes.items():
                # We parse the clinical coding system (ontology), code and any
                # values that have been associated with the code
                self._process_code(ont, code, values=code_values)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _process_code(self, ontology, code, values=[]):
        """
        process the code data and store it

        Parameters
        ----------
        ontology : str
            The ontology that the code belongs to. This can also be referred
            to as the clinical coding system
        code : str
            The ID of the code that we are processing
        values : list of str, optional, default: []
            Values that are associated with the code in the data file. Note
            that for HES files this will be empty
        """
        try:
            # Has the code already been mapped, if so, then this
            # will work and we just need to update the existing entry
            # and not do any database lookups as we are caching all the codes
            existing_stats = self.code_stats[(ontology, code)]
        except KeyError:
            # The KeyError is generated when the code/code_type
            # combination has not been seen before. In which case
            # we attempt a mapping. The mapper returns any CUIs that
            # match the code and also UKBB match strings for a sanity
            # check, these are string terms that are derived from
            # searching for the term in the UKBB mapping database
            match_codes, search_log, ukbb_entries = self.mapper.map_code(
                code, ontology
            )

            # Initialise some empty stats
            existing_stats = self._get_empty_stats()

            # Add in the cui mappings the query log and any CUI mappings
            existing_stats['cuis'] = match_codes
            existing_stats['log'] = search_log
            existing_stats['ukbb'] = ukbb_entries

            # Add it to the cache of the codes, so next time we do not have
            # to query the database
            self.code_stats[(ontology, code)] = existing_stats

        # Now update the statistics
        # This updates counts for the code/ontology(code_type) combination
        self._update_mapping_stats(existing_stats, values)

        # This updates counts for the CUI mapping to the code
        self._update_current_eid_stats(existing_stats['cuis'])

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _get_empty_stats(self):
        """
        Get a blank mapping statistic entry

        Returns
        -------
        empty_stats : dict
            An empty mapping statistics entry that will be assigned to an
            ontology/code combination
        """
        return {
            'n_occurs': 0,
            'cuis': None,
            'log': None,
            'ukbb': None,
            'n_is_float': 0,
            'n_is_int': 0,
            'n_is_string': 0,
            'n_is_empty': 0,
            'min_n_values': 0,
            'max_n_values': 0,
            'ratios': {}
        }

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _update_mapping_stats(self, existing_stats, code_values):
        """
        """
        existing_stats['n_occurs'] += 1
        existing_stats['min_n_values'] = \
            min(existing_stats['min_n_values'], len(code_values))

        existing_stats['max_n_values'] = \
            max(existing_stats['max_n_values'], len(code_values))

        ratio = [0, 0, 0]
        if len(code_values) == 0:
            existing_stats['n_is_empty'] += 1
            ratio[2] = 3

        for v in code_values:
            try:
                int(v)
                existing_stats['n_is_int'] += 1
                ratio[1] += 1
                continue
            except ValueError:
                pass

            try:
                float(v)
                existing_stats['n_is_float'] += 1
                ratio[1] += 1
                continue
            except ValueError:
                pass

            try:
                unit_set[v] += 1
            except KeyError:
                unit_set[v] = 1

            existing_stats['n_is_string'] += 1
            ratio[0] += 1
        ratio = tuple(ratio)

        try:
            existing_stats['ratios'][ratio] += 1
        except KeyError:
            existing_stats['ratios'][ratio] = 1

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _update_current_eid_stats(self, match_codes):
        """
        """
        for cui in match_codes:
            # print(cui)
            try:
                self._current_eid_stats[cui] += 1
            except KeyError:
                self._current_eid_stats[cui] = 1

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _check_is_open(self):
        """
        Check if the object has been opened already, if it has not raise an
        error

        Raises
        ------
        IOError
            If the object is not already open
        """
        if self._open is False:
            raise IOError("not open")

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _check_is_not_open(self):
        """
        Check if the object has been opened already, if it has raise an error

        Raises
        ------
        IOError
            If the object is already open
        """
        if self._open is True:
            raise IOError("is already open")
