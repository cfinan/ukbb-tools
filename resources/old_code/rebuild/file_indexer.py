"""
.. include:: ../docs/module.md
"""
from ukbb_tools import __version__, __name__ as pkg_name, config, file_defs, \
    constants as con, common
from pyaddons import file_index, file_helper
from simple_progress import progress
import argparse
import sys
import os
import re
import shutil
import pprint as pp


# Move a load of these to constants
# The prefix for verbose messages and the location to write the messages
MSG_PREFIX = '[info]'
MSG_OUT = sys.stderr


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main():
    """
    The main entry point for the script
    """
    # Initialise and parse the command line arguments
    parser = init_cmd_args()
    args = parse_cmd_args(parser)

    # Start a msg output, this will respect the verbosity and will output to
    # STDERR
    m = progress.Msg(prefix='', verbose=args.verbose, file=MSG_OUT)
    m.msg("= UKBB file indexer ({0} v{1}) =".format(
        pkg_name, __version__))
    m.prefix = MSG_PREFIX
    m.msg_atts(args)

    # Locate the config file and open the config object
    config_file = config.get_config_file(config_file=args.config_file)
    config.check_config_permissions(config_file)

    config_obj = index_ukbb_file(
        config_file,
        args.config_section,
        args.index_column,
        outdir=args.outdir,
        tmp_dir=args.tmp_dir,
        stdin=args.stdin,
        verbose=args.verbose,
        key_size=args.key_size
    )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def init_cmd_args():
    """
    Initialise the command line arguments and return the parsed arguments

    Returns
    -------
    args : :obj:`argparse.Namespace`
        The argparse namespace object
    """
    parser = argparse.ArgumentParser(
        description="An indexer for UK BioBank files. Can accept input from"
        "STDIN (in which case the --stdin flag should be used) or read the"
        "file detailed under the config_section. This will update"
        " the config file with a new entry for the indexed file. The new "
        "section will have the structure <config_secton>.<index_column> ."
        "Please note that indexing of the file will mean that the content "
        "has to be encoded as latin1. However, all efforts are made to "
        "prevent UnicodeDecodeError with dropping offending characters being"
        " the last resort to prevent UnicodeDecodeError"
    )

    parser.add_argument(
        'config_section',
        type=str,
        help="The section heading in the config file that corresponds to "
        "the file that needs to be indexed"
    )
    parser.add_argument(
        'index_column',
        type=str,
        help="A file column to build the index on. The input file should"
        "be sorted on this column"
    )
    parser.add_argument('-c', '--config-file', type=str,
                        help="An alternative path to the config file")
    parser.add_argument(
        '--outdir',
        type=str,
        help="The output directory for the indexed file, if not specified"
        " then the output directory will be the same as the input directory"
        " of the file specified under the config_section. The output "
        "directory must already exist."
    )
    parser.add_argument(
        '-s', '--stdin',  action="store_true",
        help="Should input be read from STDIN rather than the file path in"
        " the config section"
    )
    parser.add_argument(
        '-t', '--tmp-dir', type=str,
        help="A temp directory location (default: system temp)"
    )
    parser.add_argument(
        '-k', '--key-size', type=int, default=2000,
        help="The length of the key in characters (default: 2000)"
    )
    parser.add_argument('-v', '--verbose',  action="store_true",
                        help="give more output")

    return parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_cmd_args(parser):
    """
    Initialise the command line arguments and return the parsed arguments

    Returns
    -------
    args : :obj:`argparse.Namespace`
        The argparse namespace object
    """
    args = parser.parse_args()

    # Check directories exist
    for i in ['outdir', 'tmp_dir']:
        try:
            setattr(args, i, os.path.expanduser(getattr(args, i)))

            if not os.path.isdir(getattr(args, i)):
                raise NotADirectoryError(
                    "{1} is not a directory: {0}".format(
                        getattr(args, i), i
                    )
                )
        except TypeError:
            # Not defined using default
            pass

    return args


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def index_ukbb_file(config_file, section_header, index_column, outdir=None,
                    tmp_dir=None, stdin=False, verbose=False, key_size=2000):
    """
    Create an index of the UKBB file from the section header
    """
    config_obj = config.read_config_file(config_file)

    section = config.get_section_data(config_obj, section_header)

    file_def_name, file_def_index, file_def_class = None, None, None
    col_idx = None
    if not re.match(r'^{0}'.format(con.WIDE_FILE), section_header):
        file_def_name, file_def_index, file_def_class = \
            file_defs.get_parsing_definition(
                section_header
            )
        col_idx = [idx for idx, i in file_defs.yield_columns(
            file_def_class.COLUMNS, [index_column]
        )][0]
    else:
        col_idx = 0

    # Extract all the section data from the file
    csv_kwargs, encoding, nlines = config.exract_file_section_data(section)

    # Set the default encoding - we can't pass NoneType
    encoding = encoding or 'utf-8'

    # Make sure the file name is an absolute path
    file_name = os.path.expanduser(section['file'])

    # Generate the output file name and the index file name
    outfile, index_file = _get_output_names(
        file_name,
        index_column,
        outdir=outdir
    )

    # If we are taking input from STDIN then adjust
    if stdin is True:
        file_name = '-'

    file_index.build_index_file(
        file_name,
        outfile,
        index_file,
        skip_lines=1,
        index_col=col_idx+1,
        sniff_bytes=2000000,
        verbose=verbose,
        check_sort=True,
        commit_every=100000,
        key_size=key_size,
        encoding=encoding,
        **csv_kwargs
    )

    idx_section = "{0}.{1}".format(section_header, index_column)

    if csv_kwargs['delimiter'] == "\t":
        csv_kwargs['delimiter'] = r'\t'
    csv_kwargs['encoding'] = 'latin1'
    csv_kwargs['file'] = outfile
    config.update_config_section(
        idx_section,
        csv_kwargs,
        config_obj=config_obj
    )

    config.write_config_file(config_file, config_obj)
    return config_obj


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _get_output_names(file_name, index_column, outdir=None):
    """
    """
    file_dir, file_base_name, file_ext = file_helper.get_file_components(
        file_name
    )

    outdir = outdir or file_dir
    outfile_name = "{0}.{1}_idx{2}".format(
        file_base_name,
        index_column,
        file_ext
    )

    if not outfile_name.endswith('.gz'):
        outfile_name = "{0}.gz".format(outfile_name)

    outfile = os.path.join(outdir, outfile_name)
    index_file = "{0}.idx".format(outfile)
    return outfile, index_file


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == '__main__':
    main()
