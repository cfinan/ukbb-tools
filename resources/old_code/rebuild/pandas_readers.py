"""
.. include:: ../docs/pandas_readers.md
"""
from ukbb_tools import file_readers as fr
from pyaddons import file_index as fi
from operator import itemgetter
import pandas as pd
import warnings
import re
import os


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _read_index(reader_class, infile, index_values, regex=False,
                dry_run=False, **kwargs):
    """
    Generic reader to read from an index file into a `pandas.DataFrame`, this
    is not meant to be used directly.

    Parameters
    ----------
    reader_class : :obj:`base_reader.UkbbIndexReader`
        An reader class (not object) that inherits from
        `base_reader.UkbbIndexReader` and a `file_defs` definition
    infile : str
        A path to an index file that is the correct format for `reader_class`
    index_values : list of str
        The values we want to search in the index. These can be regular
        expression strings, in which case they should be raw strings.
    regexp : bool, optional, default: False
        Should the `index_values` be treated as regular expressions. As we
        can't search the index directly using a regex (SQLite does not support
        them). This, will traverse the index database matching each regex in
        turn. For large queries this may be time consuming.
    dry_run : bool, optional, default: False
        A poorly constructed regex, could result in many rows being returned
        and excessive memory consumption. `dry_run` allows the user to look
        to see the number rows would have been returned had the query been
        conducted.
    **kwargs
        These are arguments to `pyaddons.IndexReader` and are the file encoding
        and any arguments to the python `csv.Reader` function

    Returns
    -------
    df : :obj:`pandas.DataFrame`
        If `dry_run` is `False` this will be a `pandas.DataFrame` containing
        data rows from the respective data file. If `dry_run` is `True`, this
        will be a `pandas.DataFrame` with the columns `idx` (counter), `search`
        (the search string or regex), `index_value` (matching data value in
        the index), `nrows` (the number of rows returned for the `index_value`
        if a full search was conducted).
    """
    query_results = None
    if regex is True or dry_run is True:
        # We need to get a raw database connection to query the index in the
        # case of regexp or dryrun
        conn, infile_obj, reader, header_rows, index_metadata = \
            fi.IndexReader.open_index(infile, **kwargs)
        infile_obj.close()

        # The data frame of matching index values and their row counts
        query_results = _query_index(conn, index_values, regex=regex,
                                     warnings=not dry_run)
        conn.close()

        # Turn the index values that we want to search for (for real) into
        # a list
        index_values = query_results['index_value'].tolist()

    if dry_run is True:
        # Stop here if we are dry running
        return query_results

    try:
        # Now setup the Specific IndexReader
        idx = reader_class(infile, **kwargs)
        idx.open()

        # This is an empty data frame that will be returned in the case of no
        # matches. This is geerated to have the same column names as a data
        # frame that has matches, so that the returned structure is consistent
        null_data_frame = pd.DataFrame([], columns=idx.header)

        # In this instance
        if index_values == 0:
            idx.close()
            return null_data_frame

        df = pd.DataFrame(
            _read_index_values(idx, index_values)
        )
    finally:
        idx.close()

    if df.shape[0] == 0:
        return null_data_frame
    return df


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _query_index(conn, index_values, regex=False, warnings=True):
    """
    Query the index and fetch a list of matches and the number of rows in
    each match. This is used in the case of a regexp query against an index
    (as SQLite does not support regexp queries) and in the case of a dry run

    Parameters
    ----------
    conn : `sqlite3.Connection`
        A database connection with the index
    index_values : list of str
        Data values to query against the index
    regexp : bool, optional, default : False
        Do we want to regexp against the index, if so we have to iterate
        through the index and apply regular expressions to each indexed value
    warnings : bool, optional, default: True
        Issue warnings when no data is found or when some of the requested
        values are not present
    """
    # This tracks index values that do not have any matches in the index, so
    # we can issue a warning if needed
    found_map = [False] * len(index_values)

    # The cursor to issue the query with and the very simple query
    cur = conn.cursor()
    sql = "SELECT value, nlines FROM index_data"
    cur.execute(sql)

    # The function that will be used to evaluate if an index_value has a
    # match in the index, we switch this out depending on if we are using
    # regexps or not
    test_func = _direct_match
    if regex is True:
        test_func = _regexp_match

    # Hold the matching values, this will be tuples of the idx
    # (order of searches), query value, the matching value in the index
    # and the number of expected rows
    matching_index_values = []
    # Loop through the index from start to finish
    for i in cur:
        # then all or search values
        for idx, search in enumerate(index_values):
            try:
                # The matching functions will raise an IndexError if our
                # search value does not match the current row in the index
                # so if we have a match will store a tuple of the matching
                # data
                matching_index_values.append(test_func(idx, i, search))
                # Indicate that a search value has matched something, this
                # will be evaluated after all searches have been completed
                found_map[idx] = True
            except IndexError:
                pass

    # Any search value that has not matched anything in the Index will not be
    # in matching values, however, if we are dry running, we still want to
    # know about these and assigned then a row count of 0. So we evaluate
    # the found_map for False values. The missing_values list will hold the
    # actual values of the missing values so if we are not dry running we
    # can report them to the user.
    missing_values = []
    for i in range(len(found_map)):
        if found_map[i] is False:
            matching_index_values.append((i, index_values[i], None, 0))
            missing_values.append(index_values[i])

    # We will return the search statistics in the order they were given so
    # the user does not have any surprises
    matching_index_values.sort(key=itemgetter(0)),

    query_results = pd.DataFrame(
        matching_index_values,
        columns=['idx', 'search', 'index_value', 'matching_rows']
    )

    # Do not need the idx column after the sort
    query_results.drop('idx', inplace=True, axis=1)

    # We have two different warnings, the first is indicating nothing matched
    # as this may mean the file is not indexed on the expected column. The
    # second, lists the specific values not matched
    if warnings is True and sum(found_map) == 0:
        warnings.warn("no data found!")
    elif warnings is True:
        missing_values = query_results.loc[
            query_results.matching_rows == 0, 'search'
        ].tolist()
        _issue_warning(missing_values)

    return query_results


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _direct_match(idx, search, query):
    """
    Perform a direct equality match between the user query and the index search

    Parameters
    ----------
    idx : int
        The order in the search, in the event of a match this is essentially
        passed back in the tuple to be used as a sort key at a late date
    search : tuple of str and int
        The index data in the index with the index value at [0] and the number
        of rows associated with that index value at [1]
    query : str
        The query string passed by the user

    Returns
    -------
    idx : int
        The order in the search
    query : str
        The query string passed by the user
    index_value : str
        The value in the index that matches the query. In this case direct
        equality is matched
    nrows : int
        The number of rows associated with the index value

    Raises
    ------
    IndexError
        If the query does not match the index value
    """
    if search[0] != query:
        raise IndexError("no match")
    return idx, query, search[0], search[1]


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _regexp_match(idx, search, query):
    """
    Perform a regex search between the user query regular expression string
    and the index search

    Parameters
    ----------
    idx : int
        The order in the search, in the event of a match this is essentially
        passed back in the tuple to be used as a sort key at a late date
    search : tuple of str and int
        The index data in the index with the index value at [0] and the number
        of rows associated with that index value at [1]
    query : str
        The query regular expression string passed by the user. This should be
        a raw string

    Returns
    -------
    idx : int
        The order in the search
    query : str
        The query string passed by the user
    index_value : str
        The value in the index that matches the query. In this case it is a
        value that matches the user defined regular expression
    nrows : int
        The number of rows associated with the index value

    Raises
    ------
    IndexError
        If the regular expression query does not match the index value
    """
    if not re.search(query, search[0]):
        raise IndexError("no match")
    return idx, query, search[0], search[1]


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _read_index_values(index_obj, values):
    """
    A generator that searches the index for the index values. This yields
    `dicts` directly into a `pandas.DataFrame`. This issues a warning if any of
    the values do not match in the index.

    Parameters
    ----------
    index_obj : :obj:`base_reader.UkbbIndexReader`
        A instantiated and opened reader object (not object) that inherits
        from `base_reader.UkbbIndexReader` and a `file_defs` definition
    values : list of str
        values to search for in the index

    Yields
    ------
    dict
        The keys of the dict are column names in the file and the values are
        the data values for a row match
    """
    missing_values = []
    for value in values:
        try:
            for row in index_obj.fetch_value(value):
                yield row
        except KeyError:
            missing_values.append(value)
    # This only issues warnings if there are missing values
    _issue_warning(missing_values)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _issue_warning(missing_values):
    """
    Issue warnings if needed. The warning message will contain a , delimited
    string of values that did not match against the index

    Parameters
    ----------
    missing_values : list of str
        Values that did not match the index, if length is 0 then no warnings
        are given
    """
    if len(missing_values) > 0:
        warnings.warn(
            "following values not in index: {0}".format(
                ",".join(missing_values)
                )
            )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _process_args(*args):
    """
    Process bulk arguments into the input file and the values to search for
    in the index

    Parameters
    ----------
    *args
        The input file should be a [0] and everything else should be a value
        to search for in the index

    Returns
    -------
    infile : str
        The full path to the input file
    index_values : list of str
        At least one value to search for in the index

    Raises
    ------
    IndexError
        If there are no values to query
    """
    infile = args[0]
    index_values = args[1:]

    if len(index_values) == 0:
        raise IndexError("no values to query")

    return os.path.expanduser(infile), index_values


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def read_clinical_data(*args, **kwargs):
    """
    A reader to read from an indexed `clinical_data` file into a
    `pandas.DataFrame`. This utilises the `file_readersr.ClinicalDataIdx`
    class to perform the query and parse the results

    Parameters
    ----------
    infile : str
        A path to an index file that is the correct format for `reader_class`
    *index_values : str
        At least one value to search in the index. These can be regular
        expression strings, in which case they should be raw strings.
    regexp : bool, optional, default: False
        Should the `index_values` be treated as regular expressions. As we
        can't search the index directly using a regex (SQLite does not support
        them). This, will traverse the index database matching each regex in
        turn. For large queries this may be time consuming.
    dry_run : bool, optional, default: False
        A poorly constructed regex, could result in many rows being returned
        and excessive memory consumption. `dry_run` allows the user to look
        to see the number rows would have been returned had the query been
        conducted.
    **kwargs
        These are arguments to `pyaddons.IndexReader` and are the file encoding
        and any arguments to the python `csv.Reader` function

    Returns
    -------
    df : :obj:`pandas.DataFrame`
        If `dry_run` is `False` this will be a `pandas.DataFrame` containing
        data rows from the respective data file. If `dry_run` is `True`, this
        will be a `pandas.DataFrame` with the columns `idx` (counter), `search`
        (the search string or regex), `index_value` (matching data value in
        the index), `nrows` (the number of rows returned for the `index_value`
        if a full search was conducted).
    """
    infile, index_values = _process_args(*args)
    return _read_index(fr.ClinicalDataIdx, infile, index_values, **kwargs)
