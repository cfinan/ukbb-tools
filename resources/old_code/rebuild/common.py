"""
.. include:: ../docs/common.md
"""
# from datetime import datetime, timedelta
from pyaddons import gzopen
from datetime import timedelta
from contextlib import contextmanager
import tempfile
import os
import shutil
import csv
import sys


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Cache(object):
    """
    UkbbDbBuilder uses `session.bulk_insert_mappings` to insert the data into
    the database as it is a lot faster. For example, using orm objects
    directly to load the ~140M rows in `clinical_data` takes about 1d 9h,
    however using `session.bulk_insert_mappings` takes about 4.5h. The
    downside of this is that SQLAlchemy does not manage the ID creation.
    Therefore, objects in the simple lookup tables are cached in this cache.
    Also, the cache manages and tracks ID creation. When created, the cache
    takes an id_column argument that should correspond to a key in the dict
    objects that are added to it. When a new dict object is added a key is
    given alongside it. If the key matches an entry in the cache then the ID
    of that entry is returned. If the key does not match anything in the cache
    then it is added alongside the next ID in the sequence. That ID is also
    added to the dict object in the id_column and the dict object is stored in
    a list of newly added objects. Then when it is time to add the data to the
    database the `flush()` method is called. This will return all the new dict
    objects that have been added since the last call to `flush()` and the.
    new dict objects are removed. The whole process is repeated until the data
    build is finished.
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, table, id_col):
        """
        Parameters
        ----------
        id_col : str
            The column name in newly added dict objects that should hold the
            ID value if the dict object
        """
        self.table = table

        # The ID column that should be in all newly added dict objects
        self._id_col = id_col

        # Holds all the key values associated with the newly added dict
        # objects
        self._keys = {}

        # The current ID counter, this is incremented only when a new key that
        # has not been seen before is added
        self._id_no = 0

        # Will hold all the newly added dict objects and is emptied when
        # flush() is called
        self._new_keys = []

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def add(self, obj, key):
        """
        Adds/checks the key associated with the dict object against keys
        stored in the cache. If `key` exists then the ID of that key is
        returned if it does not exist then the `id_col` of the dict object is
        given the next ID in the sequence and the key is stored alongside the
        id. Newly added dict objects are stored and can be retrieved with the
        `flush()` method.

        Parameters
        ----------
        obj : dict
            A dict that has the `id_col` as one of it's keys. The initial value
            if the `id_col` in the dict can be `NoneType` (or anything) as it
            will be overwritten with an ID if the key associated with the dict
            has not been seen before
        key : `str` or `tuple` or `NoneType`
            The key should be a string or `tuple` or NoneType. It should not
            be anything (other than `NoneType`) that will fail with a `len()`
            call. If the key is `NoneType` then this will result in `NoneType`
            being returned as it is assumed that the data is missing.

        Returns
        -------
        id : int or NoneType
            Returns the ID associated with the key (and the dict). A NoneType
            is returned if the key is `NoneType`

        Raises
        ------
        KeyError
            If the `id_col` is not in the dict object being added
        """
        # To raise a KeyError on no id_col. I have to do this here as otherwise
        # we can't distinguish between a no key KeyError and no ID KeyError.
        # The former is normal operation and the latter is an error
        obj[self._id_col]

        try:
            # If NoneType (or int) will induce a TypeError
            len(key)
            # If we get here then the key is not NoneType
            return self._keys[key]
        except KeyError:
            # key not in the cache so add it
            self._id_no += 1

            # Add the ID to the dict object id_col
            obj[self._id_col] = self._id_no

            # Associate the ID with the key
            self._keys[key] = self._id_no

            # Add the dict object to the new keys
            self._new_keys.append(obj)
            return self._id_no
        except TypeError:
            return None

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def is_new(self, key):
        """
        Determine if a key would be a new key if it was added to the cache

        Parameters
        ----------
        key : `str` or `tuple` or `NoneType`
            The key should be a string or `tuple` or NoneType. It should not
            be anything (other than `NoneType`) that will fail with a `len()`
            call. If the key is `NoneType` then this will result in `NoneType`
            being returned as it is assumed that the data is missing.

        Returns
        -------
        is_new : bool
            `True` if the key would be a new key `False` if not
        """
        try:
            # If NoneType (or int) will induce a TypeError
            len(key)
            # Induce a KeyError
            self._keys[key]
            return True
        except KeyError:
            return False
        except TypeError:
            return False

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_id(self, key):
        """
        Get an ID for a Key

        Parameters
        ----------
        key : `str` or `tuple` or `NoneType`
            The key should be a string or `tuple` or NoneType. It should not
            be anything (other than `NoneType`) that will fail with a `len()`
            call. If the key is `NoneType` then this will result in `NoneType`
            being returned as it is assumed that the data is missing.

        Returns
        -------
        id : int
            The ID corresponding to the key
        """
        self._keys[key]

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def flush(self):
        """
        Retrieve all newly added dict objects since the last call to flush. A
        call to flush will clear all the newly added dict objects

        Returns
        -------
        newly_added : list of dict
            The dict objects that have been added since the last call to
            `flush()`
        """
        return_keys = self._new_keys
        self._new_keys = []
        return return_keys

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def len(self):
        """
        """
        return len(self._new_keys)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def fetch_sample_data(*args):
    """
    Attempt to fetch the sample data from an indexed file and return an empty
    list if not preset

    Parameters
    ----------
    *args
        multiple :obj:`file_handler.UkbbIndexReader` objects and the last
        argument must be an eid to search
    eid : int
        The sample ID to fetch

    Returns
    -------
    sample_data : list of dict
        Each dict represents a row of data in the sample file. With the keys
        being column names and the values being column data. If the sample
        is not present then the list will be empty
    """
    all_rows = []

    # Make sure that we actually have some files
    if len(args) <= 1:
        raise "no files or eid"

    # Loop through all the index readers in the arguments
    for i in range(len(args) - 1):
        try:
            all_rows.append([row for row in args[i].fetch_value(args[-1])])
        except KeyError as e:
            if e.args[0].endswith('not found in index'):
                # If we can't find the sample one of the data then append a
                # blank list
                all_rows.append([])
            else:
                raise
    return all_rows


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_days_from_baseline(date_of_assessment, date_of_event):
    """
    Determine the number of days relative to the baseline. Negative numbers
    mean that an event occurred before baseline. Positive numbers mean it
    occurred after baseline, with 0 meaning that it occurred on baseline

    Parameters
    ----------
    date_of_assessment : :obj:`datetime.datetime`
        A date object representing the participant's first assessment
    date_of_event : :obj:`datetime.datetime`
        A date representing the date of the event

    Returns
    -------
    days_from_baseline : int
        The number of days relative to the baseline
    """
    return (date_of_event - date_of_assessment).days


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_age_at_event(date_of_birth, event_date):
    """
    Calculate the participant age in years when the event took place

    Parameters
    ----------
    date_of_birth : :obj:`datetime.datetime`
        A date object representing the participant's date of birth
    event_date : :obj:`datetime.datetime`
        A date representing the date of the event

    Returns
    -------
    age_at_event : float
        The participant's age when the event occurred
    """
    return (event_date - date_of_birth)/timedelta(days=365.25)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_tempfile(**kwargs):
    """
    Create a temp file and return the name only

    Parameters
    ----------
    *args
        Any arguments to `tempfile.mkstemp`

    Returns
    -------
    tempfile_name : str
        The path to the tempfile, where the wide file will be written prior
        to moving it to the final location
    """
    tfobj, tfn = tempfile.mkstemp(**kwargs)
    os.close(tfobj)
    return tfn


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def replace_none_type(row, missing=''):
    """
    Replace values that are `NoneType` with a string based missing value

    Parameters
    ----------
    row : list
        A row of values to assess

    Returns
    -------
    row : list
        A row where NoneType are replaced with `missing`
    """
    return [i if i is not None else None for i in row]


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def make_none_type(row, none_values=['']):
    """
    For a single row make '' into `NoneType`

    Parameters
    ----------
    row : list or dict
        A single data row with missing data represented as ''

    Returns
    -------
    row : list
        A single data row with '' made into `NoneType`

    """
    return [None if i.strip() in none_values else i for i in row]


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def finialise_files(file_obj, final_loc):
    """
    """
    file_obj.close()
    shutil.move(file_obj.name, final_loc)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def clean_files(file_obj, final_loc):
    """
    """
    file_obj.close()
    shutil.move(file_obj.name, final_loc)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def sniff_delimiter(infile, encoding='utf8', read_bytes=2000000, **csv_kwargs):
    """
    Detect the delimiter in the infile and return a dialect with that delimiter
    in it

    Parameters
    ----------
    infile : str
        The input file path
    bytes : int, optional, default: 8128
        The number of bytes to sniff from the begining of the file, this is
        used to detect the delimiter

    Returns
    -------
    dialect : :obj:`csv.Dialect`
        The csv dialect object containing the delimiter - this can be used as
        an argument to `csv.Reader`
    """
    if len(csv_kwargs) == 0:
        try:
            csvfile = gzopen.gzip_agnostic(infile, encoding=encoding)
            csv_kwargs['dialect'] = \
                csv.Sniffer().sniff(csvfile.read(read_bytes))
        finally:
            csvfile.close()
    return csv_kwargs


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@contextmanager
def stdopen(filename, mode='rt', method=open, use_tmp=False, tmp_dir=None,
            **kwargs):
    """
    Provide either an opened file or STDIN/STDOUT if filename is not a file.

    Parameters
    ----------
    filename : str or :obj:`sys.stdin` or NoneType
        The filename to open. If `sys.stdin`, '-', '' or `NoneType` then
        `sys.stdin` is yielded otherwise the file is opened with `method`
    mode : str
        Should be the usual ``w/wt/wb/r/rt/rb` '' is interpreted as read
    method : :obj:`func`
        The open method to use (uses the standard `open` as a default)
    **kwargs
        Any other kwargs passed to method

    Yields
    ------
    fobj : :obj:`File` or `sys.stdin` or `sys.stdout`
        A place to read or write depending on mode
    """
    if mode == '' or 'r' in mode or mode is None:
        # Reading
        if filename not in [sys.stdin, '-', '', None]:
            fobj = method(filename, mode, **kwargs)
            yield fobj
            fobj.close()
        else:
            if 'b' in mode:
                yield sys.stdin.buffer
            else:
                yield sys.stdin
    elif 'w' in mode:
        # Writing
        if filename not in [sys.stdout, '-', '', None]:
            if use_tmp is True:
                tmpout = get_tempfile(dir=tmp_dir)

                try:
                    fobj = method(tmpout, mode, **kwargs)
                    yield fobj
                except Exception:
                    fobj.close()
                    os.unlink(tmpout)
                    raise
                fobj.close()
                shutil.move(tmpout, filename)
            else:
                fobj = method(filename, mode, **kwargs)
                yield fobj
                fobj.close()
        else:
            if 'b' in mode:
                yield sys.stdout.buffer
            else:
                yield sys.stdout
    else:
        raise ValueError("unknown mode: {0}".format(mode))


