"""
.. include:: ../docs/columns.md
"""
from ukbb_tools import type_casts as tc
from collections import namedtuple

# I will define all the expected column names in the files using this
# namedtuple - I love namedtuples
ColName = namedtuple('ColName', ['name', 'type', 'desc'])


# ############################ INPUT FILES ####################################

# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
EID = ColName(
    "eid",
    int,
    "The sample ID column for UK Biobank"
)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
GP_DATA_PROVIDER = ColName(
    'data_provider',
    int,
    'The data provider for the GP data  - I think there are four fo them'
)

GP_EVENT_DATE = ColName(
    'event_dt',
    tc.parse_standard_date,
    'The event date in dd/mm/yyyy'
)

GP_READ_TWO = ColName(
    'read_2',
    str,
    'The read version 2 code for the GP visit'
)

GP_READ_THREE = ColName(
    'read_3',
    str,
    'The read version 3 code for the GP visit'
)

GP_VALUE_ONE = ColName(
    'value1',
    str,
    'A possible first value associated with the GP visit'
)

GP_VALUE_TWO = ColName(
    'value2',
    str,
    'A possible second value associated with the GP visit'
)

GP_VALUE_THREE = ColName(
    'value3',
    str,
    'A possible third value associated with the GP visit'
)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
HES_EVT_INSTANCE_IDX = ColName(
    'ins_index',
    int,
    "A numerical index which together with the eid uniquely identifies the"
    " corresponding records between the hes events table and hes_operations "
    "and diagnosis table"
)

HES_EVT_DATA_SOURCE = ColName(
    'dsource',
    str,
    'Inpatient record origin string coding 1970 either HES (England), PEDW'
    ' (Wales) or SMR (Scotland). This can affect the coding used for some '
    'data fields. 40022'
)

HES_EVT_DATA_SUB_SOURCE = ColName(
    'source',
    int,
    'Inpatient record format Integer Coding 263. A finer split of the format of'
    ' data released from each dsource above. This can also affect the coding'
    ' used for some data fields, 41253'
)

HES_EVT_EPISODE_START = ColName(
    'epistart',
    tc.parse_condensed_date,
    'Episode start date. Note, there are no data for Scottish records with'
    ' source=10.'
)

HES_EVT_EPISODE_END = ColName(
    'epiend',
    tc.parse_condensed_date,
    'Episode end date, Date. Note, there are no data for Scottish records with'
    ' source = 10'
)

HES_EVT_EPISODE_DURATION = ColName(
    'epidur',
    int,
    'Duration of episode, Integer, the difference in days between the episode'
    ' start date and the episode end date, where both are given'
)

HES_EVT_BED_YEAR = ColName(
    'bedyear',
    int,
    'Duration of episode within HES data year, Integer. This is only available'
    ' for English hospital records (dsource=HES).'
)

HES_EVT_EPISODE_STATUS = ColName(
    'epistat',
    int,
    'Episode status, Integer HES: Coding 237 PEDW: N/A SMR: N/A. Whether the'
    ' episode had finished before the end of the HES data-year. An unfinished'
    ' episode would be replaced by its finished version when the data is next'
    ' updated. Data are only available from English inpatient data'
    '(dsource=HES).'
)

HES_EVT_EPISODE_TYPE = ColName(
    'epitype',
    int,
    'Episode type, Integer HES: Coding 238 PEDW: N/A SMR: N/A. Hospital '
    'episode type identifies whether the period of care in hospital was, for'
    ' example, a general episode, a maternity episode or a psychiatric '
    'episode. Only present in English inpatient data (dsource=HES) - 41231'
)

HES_EVT_EPISODE_ORDER = ColName(
    'epiorder',
    int,
    'Episode order, Integer HES: Coding 236 PEDW: Coding 236 SMR: N/A. The '
    'position of this episode within a spell (starting at 1). Data are not '
    'available from Scottish inpatient data (dsource=SMR),'
)

HES_EVT_SPELL_IDX = ColName(
    'spell_index',
    int,
    'Spell index, Integer. A numerical index indicating which spell this '
    'episode belongs to (where possible). Spells will not generally be indexed'
    ' in chronological order - 41235'
)

HES_EVT_SPELL_SEQUENCE = ColName(
    'spell_seq',
    int,
    'Spell sequence, Integer. A numerical index, starting from 0, giving the'
    ' chronological position within its spell that this episode occupies '
    '(where possible).'
)

HES_EVT_SPELL_BEGIN = ColName(
    'spelbgin',
    int,
    'Beginning of spell indicator, Integer HES: Coding 234 PEDW: N/A SMR: N/A.'
    ' An indicator that this episode is the first in a spell. Only present in'
    ' English inpatient data (dsource=HES).'
)

HES_EVT_SPELL_END = ColName(
    'spelend',
    str,
    'End of spell indicator, String HES: Coding 235 PEDW: N/A SMR: N/A. An '
    'indicator that this episode is the last in a spell. Only present in '
    'English inpatient data (dsource=HES).'
)

HES_EVT_SPELL_DURATION = ColName(
    'speldur',
    int,
    'Duration of spell, Integer. Only present in English inpatient data '
    '(dsource=HES).'
)

HES_EVT_PCT_CODE = ColName(
    'pctcode',
    str,
    'Current PCT responsible for patient, String Coding 239. The PCT code '
    'identifies the administrative body responsible for the patient. Only '
    'present in English inpatient data (dsource=HES) - 41229'
)

HES_EVT_GP_PCT = ColName(
    'gpprpct',
    str,
    'PCT where patients GP was registered, String. Coding 239. The PCT code '
    'identifies the administrative body where the patients GP practice is '
    'registered. Only present in English inpatient data (dsource=HES) - 41230'
)

HES_EVT_CAT = ColName(
    'category',
    int,
    'Administrative and legal status of patient, Integer. Coding 201. '
    'Administrative and legal status identifies the type of patient and '
    'whether they have been formally detained under the Mental Health Act or '
    'not. Only present in some English inpatient data, where source=6 - 41232'
)

HES_EVT_ADMIT_DECISION_DATE = ColName(
    'elecdate',
    tc.parse_condensed_date,
    'Date of decision to admit to hospital, Date. No data available from Wales'
    ' (dsource=PEDW).'
)

HES_EVT_ADMIT_WAIT_TIME = ColName(
    'elecdur',
    int,
    'Waiting time for hospital admission, Integer. In the Scottish inpatient '
    'data 0 is a valid value. No data are available from Wales (dsource=PEDW).'
)

HES_EVT_ADMIT_DATE = ColName(
    'admidate',
    tc.parse_condensed_date,
    'Date of admission to hospital, Date. There are no data available for this'
    ' field in the Scottish inpatient data apart from where source=10.'
)

HES_EVT_ADMIT_METHOD_UNI = ColName(
    'admimeth_uni',
    int,
    'Methods of admission to hospital (recoded). Integer. Coding 264. A field'
    ' providing a unified coding across the various sources for the admimeth '
    'field - 41249'
)

HES_EVT_ADMIT_METHOD = ColName(
    'admimeth',
    int,
    'Methods of admission to hospital, Integer. HES: Coding 202 PEDW: Coding '
    '252 SMR: Coding 241 (source 10), Coding 242 (sources 15 & 28). Method of '
    'admission to hospital identifies how a patient was admitted, e.g. an '
    'elective admission from a waiting list or an emergency admission, etc'
    ' - 41212'
)

HES_EVT_ADMIT_SOURCE_UNI = ColName(
    'admisorc_uni',
    int,
    'Sources of admission to hospital (recoded), Integer. Coding 265. A field '
    'providing a unified coding across the various sources for the admisorc '
    'field - 41251'
)

HES_EVT_ADMIT_SOURCE = ColName(
    'admisorc',
    int,
    'Sources of admission to hospital, Integer HES: Coding 203 PEDW: Coding '
    '253 SMR: Coding 244 (source 10), Coding 243 (sources 15 & 28). Source of '
    'admission identifies whether a patient came to the hospital from their '
    'usual place of residence or elsewhere - 41233'
)

HES_EVT_FIRST_REGULAR_DAY = ColName(
    'firstreg',
    int,
    'First regular day or night of admission to hosp, Integer. Coding 204. '
    'Only present in English inpatient data (dsource=HES).'
)

HES_EVT_PATIENT_CLASS_UNI = ColName(
    'classpat_uni',
    int,
    'Patient classification on admission (recoded), Integer. Coding 266. A '
    'field providing a unified coding across the various sources for the '
    'classpat field - 41247'
)

HES_EVT_PATIENT_CLASS = ColName(
    'classpat',
    int,
    'Patient classification on admission, Integer HES: Coding 208 PEDW: Coding'
    ' 21 SMR: Coding 246 (source 10), Coding 245 (sources 15 & 28). Patient '
    'classification on admission identifies the type of patient, e.g. whether '
    'they are a regular attender or a maternity admission, etc - 41209'
)

HES_EVT_INTENDED_MANAGE_UNI = ColName(
    'intmanag_uni',
    int,
    'Intended management of patient (recoded), Integer. Coding 271. A field '
    'providing a unified coding across the various sources for the intmanag '
    'field - 41244'
)

HES_EVT_INTENDED_MANAGE = ColName(
    'intmanag',
    int,
    'Intended management of patient, Integer. HES: Coding 209 PEDW: Coding '
    '260 SMR: N/A. The intended management of a patient is their planned '
    'duration of hospital stay, i.e. whether an overnight stay was '
    'anticipated or not. Only present in English inpatient data '
    '(dsource=HES) - 41206'
)

HES_EVT_MAIN_SPEC_UNI = ColName(
    'mainspef_uni',
    int,
    'Main speciality of consultant (recoded), Integer. Coding 270. A field '
    'providing a unified coding across the various sources for the mainspef '
    'field - 41245'
)

HES_EVT_MAIN_SPEC = ColName(
    'mainspef',
    str,
    'Main speciality of consultant, String HES: Coding 210 PEDW: Coding 258 '
    'SMR: N/A. The main speciality of the consultant is the specialism under '
    'which the consultant is contracted e.g. cardiology or geriatric medicine,'
    ' etc. Only present in English inpatient data (dsource=HES) - 41207'
)

HES_EVT_TREAT_SPEC_UNI = ColName(
    'tretspef_uni',
    int,
    'Treatment speciality of consultant (recoded), Integer. Coding 269. A '
    'field providing a unified coding across the various sources for the '
    'tretspef field - 41246'
)

HES_EVT_TREAT_SPEC = ColName(
    'tretspef',
    str,
    'Treatment speciality of consultant, String HES: Coding 211 PEDW: Coding '
    '257 SMR: Coding 250 (source 10), Coding 251 (sources 15 & 28). The '
    'treatment speciality of the consultant is the specialism under which the '
    'consultant was working e.g. cardiology or geriatric medicine, etc - 41208'
)

HES_EVT_OPER_STATUS = ColName(
    'operstat',
    int,
    'Operation status, Integer HES: Coding 207 PEDW: N/A SMR: N/A. Whether an '
    'operation/procedure was carried out as part of this episode. Only present'
    ' in English inpatient data (dsource=HES).'
)

HES_EVT_DIS_DATE = ColName(
    'disdate',
    tc.parse_condensed_date,
    'Date of discharge from hospital, Date. No data available for Scottish '
    'inpatient records where source is 15 or 28.'
)

HES_EVT_DIS_METHOD_UNI = ColName(
    'dismeth_uni',
    int,
    'Method of discharge from hospital (recoded), Integer. Coding 268. A field'
    ' providing a unified coding across the various sources for the dismeth '
    'field - 41250'
)

HES_EVT_DIS_METHOD = ColName(
    'dismeth',
    int,
    'Method of discharge from hospital, Integer. HES: Coding 206 PEDW: Coding '
    '256 SMR: Coding 249 (source 10), Coding 248 (sources 15 & 28). Method of '
    'discharge from hospital identifies how a patient was discharged i.e. '
    'whether this was on clinical advice, the patient died in hospital or the '
    'patient discharged themselves - 41213'
)

HES_EVT_DIS_DEST_UNI = ColName(
    'disdest_uni',
    int,
    'Destination on discharge from hospital (recoded), Integer. Coding 267. A '
    'field providing a unified coding across the various sources for the '
    'disdest field - 41248'
)

HES_EVT_DIS_DEST = ColName(
    'disdest',
    int,
    'Destination on discharge from hospital, Integer HES: Coding 205 PEDW: '
    'Coding 255 SMR: Coding 247. Destination on discharge identifies whether a'
    ' patient was due to return to their usual place of residence on leaving '
    'hospital, or whether they were due to go elsewhere - 41211'
)

HES_EVT_CARER_SUPPORT = ColName(
    'carersi',
    int,
    'Carer support indicator, Integer. Coding 227. Carer support indicator '
    'identifies whether a carer is available to the patient at their usual '
    'place of residence. Only present in English inpatient data '
    '(dsource=HES) - 41214'
)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
GP_REG_DATA_PROVIDER = ColName(
    'data_provider',
    int,
    'The data provider for the registration data 1=England(Vision), '
    '2=Scotland, 3=England (TPP), 4=Wales'
)

GP_REG_REG_DATE = ColName(
    'reg_date',
    tc.parse_condensed_date,
    'The date when someone registered at the practice'
)

GP_REG_UNREG_DATE = ColName(
    'deduct_date',
    tc.parse_condensed_date,
    'The date when someone un-registered at the practice'
)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
HES_DIAG_INSTANCE_IDX = ColName(
    'ins_index',
    int,
    "A numerical index which together with the eid uniquely identifies the"
    " corresponding records between the hes events table and hes_operations "
    "and diagnosis table"
)

HES_DIAG_ARRAY_IDX = ColName(
    'arr_index',
    int,
    'A numerical index which together with the eid & ins_index uniquely'
    ' identifies this record, i.e. eid, ins_index & arr_index together'
    ' form a primary key for this table.'
)

HES_DIAG_LEVEL = ColName(
    'level',
    int,
    'The primary diagnosis is the main condition treated or investigated'
    ' during the relevant episode. A secondary diagnosis is a clinically'
    ' relevant contributory factor or issue that impacts on the primary'
    ' diagnosis (including chronic conditions). An external cause is an '
    'environmental event causing injury, poisoning or other type of adverse'
    ' effect. '
)

HES_DIAG_ICD9 = ColName(
    'diag_icd9',
    str,
    'Diagnoses are coded according to the International Classification '
    'of Diseases version-9 (ICD 9).'
)

HES_DIAG_ICD9_NOTES = ColName(
    'diag_icd9_nb',
    str,
    'Notes of ICD9 diagnosis'
)

HES_DIAG_ICD10 = ColName(
    'diag_icd10',
    str,
    'Diagnoses are coded according to the International Classification '
    'of Diseases version-10 (ICD 10).'
)

HES_DIAG_ICD10_NOTES = ColName(
    'diag_icd10_nb',
    str,
    'Notes for ICD10 diagnosis'
)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
HES_OPER_INSTANCE_IDX = ColName(
    'ins_index',
    int,
    'A numerical index which together with the eid uniquely identifies '
    'the corresponding record in the main HESIN table.'
)


HES_OPER_ARRAY_IDX = ColName(
    'arr_index',
    int,
    'A numerical index which together with the eid & ins_index uniquely'
    ' identifies this record, i.e. eid, ins_index & arr_index together '
    'form a primary key for this table.'
)

HES_OPER_LEVEL = ColName(
    'level',
    int,
    '1. Main operation, 2. Secondary operation'
)

HES_OPER_OPERATION_DATE = ColName(
    'opdate',
    tc.parse_standard_date,
    'Date of the operation'
)

HES_OPER_OPCS3 = ColName(
    'oper3',
    str,
    'Operative procedures are coded according to the Office of'
    ' Population Censuses and Surveys Classification of Interventions '
    'and Procedures, version 3 (OPCS-3).'
)

HES_OPER_OPCS3_NOTES = ColName(
    'oper3_nb',
    str,
    'Notes of OPCS3 operation, currently no data are available.'
)

HES_OPER_OPCS4 = ColName(
    'oper4',
    str,
    'Operative procedures are coded according to the Office of '
    'Population Censuses and Surveys Classification of Interventions'
    ' and Procedures, version 4 (OPCS-4).'
)

HES_OPER_OPCS4_NOTES = ColName(
    'oper4_nb',
    str,
    'Notes for OPCS4 operation, currently no data are available'
)

HES_OPER_POST_OP_DUR = ColName(
    'posopdur',
    int,
    'Only present in English inpatient data (dsource = HES).'
)

HES_OPER_PRE_OP_DUR = ColName(
    'preopdur',
    int,
    'Only present in English inpatient data (dsource = HES).'
)

# @@@@@@@@@@@@@@@@@@@@@@@@ OUTPUT FILE COLUMNS @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
SAM_DATE_OF_BIRTH = ColName(
    'date_of_birth',
    tc.parse_global_date,
    "The date of the birth"
)

SAM_DATE_OF_BIRTH_INT = ColName(
    'date_of_birth_int',
    int,
    "The date of the birth as an integer (days since the 01/01/1900"
)

SAM_DATE_OF_ASSESS = ColName(
    'date_of_assess',
    tc.parse_global_date,
    "The date of the first UK Biobank assessment"
)

SAM_DATE_OF_ASSESS_INT = ColName(
    'date_of_assess_int',
    int,
    "The date of the first UK Biobank assessment as an integer (days since the"
    " 01/01/1900"
)

SAM_AGE_OF_ASSESS = ColName(
    'age_of_assess',
    float,
    "The participant's age at first assessment"
)

SAM_SR_SEX = ColName(
    'self_reported_sex',
    int,
    "The participant's self reported sex"
)

SAM_GEN_SEX = ColName(
    'genetic_sex',
    int,
    "The participant's self genetic sex"
)

SAM_DATE_OF_DEATH = ColName(
    'date_of_death',
    tc.parse_global_date,
    "If the participant has passed away, this is their date of death"
)

SAM_DATE_OF_DEATH_INT = ColName(
    'date_of_death_int',
    int,
    "If the participant has passed away, this is their date of death as an "
    "integer (days since the 01/01/1900"
)

SAM_AGE_OF_DEATH = ColName(
    'age_of_death',
    float,
    "If the participant has passed away, this is their age at time of death"
)

SAM_N_ASSESSMENTS = ColName(
    'no_of_assess',
    int,
    "The number of follow up assessments the participant has had"
)

SAM_IS_CAUCASIAN = ColName(
    'is_caucasian',
    tc.parse_bool,
    "Has the sample been genetically identified as caucasian"
)

SAM_IS_GENOTYPED = ColName(
    'is_genotyped',
    tc.parse_bool,
    "Has the sample been genotyped"
)

SAM_HAS_WITHDRAWN = ColName(
    'has_withdrawn',
    tc.parse_bool,
    "Has the participant withdrawn from the UK Biobank study"
)

# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
CLIN_DATA_EVENT_IDX = ColName(
    'event_idx',
    int,
    'Unique ID for the row, primary key'
)

CLIN_DATA_EPISODE_IDX = ColName(
    'episode_idx',
    int,
    'A unique ID for the clinical episode. In this context episodes are any'
    ' interaction with the healthcare system, i.e. primary, secondary and '
    'this index will be the same for all codes and values that apply to that'
    ' episode. The value is unique per episode and they run sequentially but'
    ' span the clinical measures and the diagnosis tables'
)

CLIN_DATA_EVENT_START_STR = ColName(
    'event_start_str',
    tc.parse_global_date,
    'The start date of the event as a string of the format YYYY-MM-DD'
)

CLIN_DATA_EVENT_START_INT = ColName(
    'event_start_int',
    int,
    'The start date of the event as an integer. This is the number of days '
    'the event occurred after 1900-1-1'
)

CLIN_DATA_AGE_AT_EVENT = ColName(
    'age_at_event',
    float,
    'The age of the participant (in years) at the time of the event. This is'
    'accurate within a month as only the month/year of birth is known'
)

CLIN_DATA_DAYS_FROM_BASELINE = ColName(
    'days_from_baseline',
    int,
    'The number of days that the event occurred relative to baseline. Baseline'
    ' is defined as the date of the participant\'s first attendance at the '
    'assessment centre field_id:53-0-0. This can be used to derive incident '
    'and prevalent events, with incident events being positive and prevalent '
    'events being negative'
)

CLIN_DATA_DAYS_PRE_EVENT = ColName(
    'days_pre_event',
    int,
    'The number of days into the episode that the event occurred. This is '
    'designed to get around having a separate episodes table and may change'
    ' in future'
)

CLIN_DATA_DAYS_POST_EVENT = ColName(
    'days_post_event',
    int,
    'The number of days after the event that the episode ended the. This is '
    'designed to get around having a separate episodes table and may change'
    ' in future'
)

CLIN_DATA_CLINICAL_CODE = ColName(
    'clinical_code',
    str,
    'The clinical code that describes the event'
)

CLIN_DATA_CLINICAL_CODE_SYSTEM = ColName(
    'clinical_code_system',
    str,
    'The clinical coding system that was used describes the event'
)

CLIN_DATA_CLINICAL_CODE_DESC = ColName(
    'clinical_code_desc',
    tc.parse_split_str,
    'A free text description giving the definition of the clinical code. This '
    'field is pipe `|` delimited in the output text file'
)

CLIN_DATA_UMLS_CUI = ColName(
    'umls_cui',
    str,
    'UMLS concept mappings for the clinical code. This field is pipe `|`'
    ' delimited in the output text file. Note that non-specific clinical codes'
    'can have many UMLS concept mappings'
)

CLIN_DATA_EVENT_LEVEL = ColName(
    'event_level',
    int,
    'The level of the event within the entire episode. Is it the primary'
    'event in the episode or the secondary event'
)

CLIN_DATA_CARE_TYPE = ColName(
    'care_type',
    str,
    'is the event tried to primary care (general practitioner - GP) or '
    'secondary care (hospitals)'
)

CLIN_DATA_RECORD_TYPE = ColName(
    'record_type',
    str,
    'The source file of the event'
)

CLIN_DATA_DATA_PROVDER = ColName(
    'data_provider',
    str,
    'The data provider of the raw event data'
)

CLIN_DATA_DATA_PROVDER_FORMAT = ColName(
    'data_provider_format',
    int,
    'The format of the data from the data provider'
)

CLIN_DATA_PCT = ColName(
    'primary_care_trust_code',
    str,
    'The primary care trust that the event happened under. Note this may be'
    ' taken from an actual episode, or for records from the GP, it may be the'
    'PCT of the nearest hospital event to the GP record. The time difference '
    'between these can be found in `days_to_pct`. In addition, where possible,'
    ' the pct code is taken from the PCT code of the hospital where the event'
    ' took place. However, sometimes this is not described. Therefore, in these'
    ' cases the PCT of the GP that the participant was registered under at the'
    ' time of the event is used, if this is the case, then `pct_code_is_gp`'
    ' will be set to 1. In many instances, both PCT values will be the same.'
    ' However, it is possible that if the event happened when the participant'
    ' was on holiday then the PCT value could be inaccurate.'
)

CLIN_DATA_DAYS_TO_PCT = ColName(
    'days_to_pct_code',
    int,
    'The time frame (in days) between the event and the hospital event that '
    'the pct_code was taken from'
)

CLIN_DATA_PCT_IS_GP = ColName(
    'pct_code_is_gp',
    tc.parse_bool,
    'Was the PCT code taken from the PCT where the partipant\'s GP is '
    'registered'
)

CLIN_DATA_IS_CLINICAL_MEAS = ColName(
    'is_clinical_measure',
    tc.parse_bool,
    'Is the event a clinical measure event rather than a diagnosis or procedure'
)

CLIN_DATA_NUMERICS = ColName(
    'numeric_values',
    tc.parse_split_float,
    'Any numeric values associated with the record, these may be pipe | '
    'delimited'
)

CLIN_DATA_NORM_NUMERICS = ColName(
    'norm_numeric_values',
    tc.parse_split_float,
    'Holds clinical measures that have been unit normalised'
)

CLIN_DATA_UNITS = ColName(
    'unit_values',
    tc.parse_split_str,
    'Any values that look like units associated with the record, these may be'
    ' pipe | delimited'
)

CLIN_DATA_STRINGS = ColName(
    'string_values',
    tc.parse_split_str,
    'Any strings associated with the record, these may be pipe | '
    'delimited'
)

CLIN_DATA_CLINICAL_NOTES = ColName(
    'clinical_notes',
    tc.parse_split_str,
    'Any clinical notes associated with the record, these may be pipe | '
    'delimited'
)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
CLIN_CODE_IDX = ColName(
    "code_id",
    int,
    "The primary key column from the codes table"
)

CLIN_CODE_UKBB_SAB = ColName(
    'ukbb_sab',
    str,
    "The clinical code type (source abbreviation) from the UKBB data"
)

CLIN_CODE_UKBB_CODE = ColName(
    'ukbb_code',
    str,
    "The clinical code UKBB data"
)

CLIN_CODE_UMLS_CUI = ColName(
    'umls_cui',
    str,
    "The UMLS concept ID"
)

CLIN_CODE_UMLS_SAB = ColName(
    'umls_sab',
    str,
    "The UMLS concept ID"
)

CLIN_CODE_UMLS_TERM = ColName(
    'umls_term',
    tc.parse_split_str,
    "The UMLS term - human readable form"
)

CLIN_CODE_UMLS_STY = ColName(
    'umls_sty',
    tc.parse_split_str,
    "The UMLS semantic type"
)

CLIN_CODE_UKBB_TERM = ColName(
    'ukbb_terms',
    tc.parse_split_str,
    "The UKBB term - human readbale form"
)

CLIN_CODE_MIN_N_VALUES = ColName(
    'min_n_values',
    int,
    "The minimum number of value columns containing data that have been seen "
    "for a code"
)

CLIN_CODE_MAX_N_VALUES = ColName(
    'max_n_values',
    int,
    "The maximum number of value columns containing data that have been seen "
    "for a code"
)

CLIN_CODE_N_OCCURS = ColName(
    'n_occurs',
    int,
    "The total number of occurences that a code hase been observed in any of "
    "the data tables"
)

CLIN_CODE_N_IS_EMPTY = ColName(
    'n_is_empty',
    int,
    "The total number of value columns that are empty for a code"
)

CLIN_CODE_N_IS_STRING = ColName(
    'n_is_string',
    int,
    "The number of value columns that contain a string"
)

CLIN_CODE_N_IS_INTEGER = ColName(
    'n_is_integer',
    int,
    "The total number of value columns that are empty for a code"
)

CLIN_CODE_N_IS_FLOAT = ColName(
    'n_is_float',
    int,
    "The number of value columns that contain a string"
)

CLIN_CODE_RATIO_300 = ColName(
    'ratio_3_0_0',
    int,
    "The number of rows for the mapping that have a ratio of 3 string values"
    " to 0 numeric values and 0 empty values "
)

CLIN_CODE_RATIO_030 = ColName(
    'ratio_0_3_0',
    int,
    "The number of rows for the mapping that have a ratio of 0 string values"
    " to 3 numeric values and 0 empty values "
)

CLIN_CODE_RATIO_003 = ColName(
    'ratio_0_0_3',
    int,
    "The number of rows for the mapping that have a ratio of 0 string values"
    " to 0 numeric values and 3 empty values "
)

CLIN_CODE_RATIO_210 = ColName(
    'ratio_2_1_0',
    int,
    "The number of rows for the mapping that have a ratio of 2 string values"
    " to 1 numeric values and 0 empty values "
)

CLIN_CODE_RATIO_201 = ColName(
    'ratio_2_0_1',
    int,
    "The number of rows for the mapping that have a ratio of 2 string values"
    " to 0 numeric values and 1 empty values "
)

CLIN_CODE_RATIO_120 = ColName(
    'ratio_1_2_0',
    int,
    "The number of rows for the mapping that have a ratio of 1 string values"
    " to 2 numeric values and 0 empty values "
)

CLIN_CODE_RATIO_021 = ColName(
    'ratio_0_2_1',
    int,
    "The number of rows for the mapping that have a ratio of 1 string values"
    " to 0 numeric values and 2 empty values "
)

CLIN_CODE_RATIO_102 = ColName(
    'ratio_1_0_2',
    int,
    "The number of rows for the mapping that have a ratio of 1 string values"
    " to 0 numeric values and 2 empty values "
)

CLIN_CODE_RATIO_012 = ColName(
    'ratio_0_1_2',
    int,
    "The number of rows for the mapping that have a ratio of 0 string values"
    " to 1 numeric values and 2 empty values "
)

CLIN_CODE_RATIO_111 = ColName(
    'ratio_1_1_1',
    int,
    "The number of rows for the mapping that have a ratio of 1 string values"
    " to 0 numeric values and 1 empty values "
)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
relavnt_date_text = (
    "Each field is given a relevant date with which it is"
    " associated, as the wide format does not intrinsically"
    " link measurements with dates. This is used to calculate"
    " the various time points for the measure. Some of these"
    " are meaningless, i.e when the data point is a date"
    " itself. Nevertheless, each data point is tagged with a"
    " relevant date."
)

UKBB_ENUM_BASELINE_DATE = ColName(
    'baseline_date',
    tc.parse_global_date,
    "The baseline date for the sample as a date string YYYY-MM-DD"
)
UKBB_ENUM_BASELINE_INT_DATE = ColName(
    'baseline_int_date',
    int,
    "The baseline date for the sample as an integer"
)
UKBB_ENUM_FIELD_TEXT = ColName(
    'field_text',
    str,
    "The field text (meaning) for the field ID for the row"
)
UKBB_ENUM_FIELD_ID = ColName(
    'field_id',
    int,
    "The main UKBB field ID for the data in the row"
)
UKBB_ENUM_INST_ID = ColName(
    'instance_id',
    int,
    "The UKBB instance ID (repeat measure) for the data in the row"
)
UKBB_ENUM_ARRAY_ID = ColName(
    'array_id',
    int,
    "The UKBB array ID (some data points are part of a larger"
    " collection of data) for the data in the row"
)
UKBB_ENUM_REL_DATE_FIELD_TEXT = ColName(
    'relevant_date_field_text',
    str,
    "{0}  This is the field description for the relevant date data "
    "point.".format(relavnt_date_text)
)
UKBB_ENUM_REL_DATE_FIELD_ID = ColName(
    'relevant_date_field_id',
    int,
    "{0} This is the field ID for the relevant date data "
    "point".format(relavnt_date_text)
)
UKBB_ENUM_REL_DATE_FIELD_INST_ID = ColName(
    'relevant_date_field_instance_id',
    int,
    "{0} This is the instance ID for the relevant date field. Usually this"
    " will be matched to the instance ID of the data field, as it could be"
    " a repeat measure at a different time point".format(relavnt_date_text)
)
UKBB_ENUM_REL_DATE_ARRAY_ID = ColName(
    'relevant_date_array_id',
    int,
    "{0} This is the array ID for the relevant date field. Usually this"
    " will be matched to the array ID of the data field, as it could be"
    " a repeat measure at a different time point".format(relavnt_date_text)
)
UKBB_ENUM_REL_DATE = ColName(
    'relevant_date',
    tc.parse_global_date,
    "{0} This is the relevant date expressed as a string: "
    "YYYY-MM-DD".format(relavnt_date_text)
)
UKBB_ENUM_REL_INT_DATE = ColName(
    'relevant_int_date',
    int,
    "{0} This is the relevant date expressed as an integer: "
    "YYYY-MM-DD".format(relavnt_date_text)
)
UKBB_ENUM_EVENT_DAYS_FROM_BASELINE = ColName(
    'event_days_from_baseline',
    int,
    "Using the relevant date. This is the number of days between the "
    "relevant date and the baseline"
)
UKBB_ENUM_AGE_YEARS_AT_EVENT = ColName(
    'age_years_at_event',
    float,
    "Using the relevant date. This is the age of the participant when the "
    "data point was generated"
)
UKBB_ENUM_IS_FALLBACK_DATE = ColName(
    'is_fallback_date',
    int,
    "In many cases the data for the relavent date will not be available. In"
    " which case a fallback date is used. Firstly, it will fallback to a "
    "previous array ID for the field_id/instance_id, then it will fallback to"
    " a previous instance ID. if that is not possible, then it will fallback"
    " to baseline date"
)
UKBB_ENUM_VALUE = ColName(
    'value',
    str,
    "The actual value for the data point. In the case of the Enum data, this"
    " should map back to categories in the category ID table"
)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
UKBB_DATE_BASELINE_DATE = ColName(
    'baseline_date',
    tc.parse_global_date,
    "The baseline date for the sample as a date string YYYY-MM-DD"
)
UKBB_DATE_BASELINE_INT_DATE = ColName(
    'baseline_int_date',
    int,
    "The baseline date for the sample as an integer"
)
UKBB_DATE_FIELD_TEXT = ColName(
    'field_text',
    str,
    "The field text (meaning) for the field ID for the row"
)
UKBB_DATE_FIELD_ID = ColName(
    'field_id',
    int,
    "The main UKBB field ID for the data in the row"
)
UKBB_DATE_INST_ID = ColName(
    'instance_id',
    int,
    "The UKBB instance ID (repeat measure) for the data in the row"
)
UKBB_DATE_ARRAY_ID = ColName(
    'array_id',
    int,
    "The UKBB array ID (some data points are part of a larger"
    " collection of data) for the data in the row"
)
UKBB_DATE_REL_DATE_FIELD_TEXT = ColName(
    'relevant_date_field_text',
    str,
    "{0}  This is the field description for the relevant date data "
    "point.".format(relavnt_date_text)
)
UKBB_DATE_REL_DATE_FIELD_ID = ColName(
    'relevant_date_field_id',
    int,
    "{0} This is the field ID for the relevant date data "
    "point".format(relavnt_date_text)
)
UKBB_DATE_REL_DATE_FIELD_INST_ID = ColName(
    'relevant_date_field_instance_id',
    int,
    "{0} This is the instance ID for the relevant date field. Usually this"
    " will be matched to the instance ID of the data field, as it could be"
    " a repeat measure at a different time point".format(relavnt_date_text)
)
UKBB_DATE_REL_DATE_ARRAY_ID = ColName(
    'relevant_date_array_id',
    int,
    "{0} This is the array ID for the relevant date field. Usually this"
    " will be matched to the array ID of the data field, as it could be"
    " a repeat measure at a different time point".format(relavnt_date_text)
)
UKBB_DATE_REL_DATE = ColName(
    'relevant_date',
    tc.parse_global_date,
    "{0} This is the relevant date expressed as a string: "
    "YYYY-MM-DD".format(relavnt_date_text)
)
UKBB_DATE_REL_INT_DATE = ColName(
    'relevant_int_date',
    int,
    "{0} This is the relevant date expressed as an integer: "
    "YYYY-MM-DD".format(relavnt_date_text)
)
UKBB_DATE_EVENT_DAYS_FROM_BASELINE = ColName(
    'event_days_from_baseline',
    int,
    "Using the relevant date. This is the number of days between the "
    "relevant date and the baseline"
)
UKBB_DATE_AGE_YEARS_AT_EVENT = ColName(
    'age_years_at_event',
    float,
    "Using the relevant date. This is the age of the participant when the "
    "data point was generated"
)
UKBB_DATE_IS_FALLBACK_DATE = ColName(
    'is_fallback_date',
    int,
    "In many cases the data for the relavent date will not be available. In"
    " which case a fallback date is used. Firstly, it will fallback to a "
    "previous array ID for the field_id/instance_id, then it will fallback to"
    " a previous instance ID. if that is not possible, then it will fallback"
    " to baseline date"
)
UKBB_DATE_VALUE = ColName(
    'value',
    tc.parse_global_date,
    "The actual date value in DDDD-MM-YY of the data point"
)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
UKBB_STR_BASELINE_DATE = ColName(
    'baseline_date',
    tc.parse_global_date,
    "The baseline date for the sample as a date string YYYY-MM-DD"
)
UKBB_STR_BASELINE_INT_DATE = ColName(
    'baseline_int_date',
    int,
    "The baseline date for the sample as an integer"
)
UKBB_STR_FIELD_TEXT = ColName(
    'field_text',
    str,
    "The field text (meaning) for the field ID for the row"
)
UKBB_STR_FIELD_ID = ColName(
    'field_id',
    int,
    "The main UKBB field ID for the data in the row"
)
UKBB_STR_INST_ID = ColName(
    'instance_id',
    int,
    "The UKBB instance ID (repeat measure) for the data in the row"
)
UKBB_STR_ARRAY_ID = ColName(
    'array_id',
    int,
    "The UKBB array ID (some data points are part of a larger"
    " collection of data) for the data in the row"
)
UKBB_STR_REL_DATE_FIELD_TEXT = ColName(
    'relevant_date_field_text',
    str,
    "{0}  This is the field description for the relevant date data "
    "point.".format(relavnt_date_text)
)
UKBB_STR_REL_DATE_FIELD_ID = ColName(
    'relevant_date_field_id',
    int,
    "{0} This is the field ID for the relevant date data "
    "point".format(relavnt_date_text)
)
UKBB_STR_REL_DATE_FIELD_INST_ID = ColName(
    'relevant_date_field_instance_id',
    int,
    "{0} This is the instance ID for the relevant date field. Usually this"
    " will be matched to the instance ID of the data field, as it could be"
    " a repeat measure at a different time point".format(relavnt_date_text)
)
UKBB_STR_REL_DATE_ARRAY_ID = ColName(
    'relevant_date_array_id',
    int,
    "{0} This is the array ID for the relevant date field. Usually this"
    " will be matched to the array ID of the data field, as it could be"
    " a repeat measure at a different time point".format(relavnt_date_text)
)
UKBB_STR_REL_DATE = ColName(
    'relevant_date',
    tc.parse_global_date,
    "{0} This is the relevant date expressed as a string: "
    "YYYY-MM-DD".format(relavnt_date_text)
)
UKBB_STR_REL_INT_DATE = ColName(
    'relevant_int_date',
    int,
    "{0} This is the relevant date expressed as an integer: "
    "YYYY-MM-DD".format(relavnt_date_text)
)
UKBB_STR_EVENT_DAYS_FROM_BASELINE = ColName(
    'event_days_from_baseline',
    int,
    "Using the relevant date. This is the number of days between the "
    "relevant date and the baseline"
)
UKBB_STR_AGE_YEARS_AT_EVENT = ColName(
    'age_years_at_event',
    float,
    "Using the relevant date. This is the age of the participant when the "
    "data point was generated"
)
UKBB_STR_IS_FALLBACK_DATE = ColName(
    'is_fallback_date',
    int,
    "In many cases the data for the relavent date will not be available. In"
    " which case a fallback date is used. Firstly, it will fallback to a "
    "previous array ID for the field_id/instance_id, then it will fallback to"
    " a previous instance ID. if that is not possible, then it will fallback"
    " to baseline date"
)
UKBB_STR_VALUE = ColName(
    'value',
    str,
    "The actual value for the data point."
)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
UKBB_NUM_BASELINE_DATE = ColName(
    'baseline_date',
    tc.parse_global_date,
    "The baseline date for the sample as a date string YYYY-MM-DD"
)
UKBB_NUM_BASELINE_INT_DATE = ColName(
    'baseline_int_date',
    int,
    "The baseline date for the sample as an integer"
)
UKBB_NUM_FIELD_TEXT = ColName(
    'field_text',
    str,
    "The field text (meaning) for the field ID for the row"
)
UKBB_NUM_FIELD_ID = ColName(
    'field_id',
    int,
    "The main UKBB field ID for the data in the row"
)
UKBB_NUM_INST_ID = ColName(
    'instance_id',
    int,
    "The UKBB instance ID (repeat measure) for the data in the row"
)
UKBB_NUM_ARRAY_ID = ColName(
    'array_id',
    int,
    "The UKBB array ID (some data points are part of a larger"
    " collection of data) for the data in the row"
)
UKBB_NUM_REL_DATE_FIELD_TEXT = ColName(
    'relevant_date_field_text',
    str,
    "{0}  This is the field description for the relevant date data "
    "point.".format(relavnt_date_text)
)
UKBB_NUM_REL_DATE_FIELD_ID = ColName(
    'relevant_date_field_id',
    int,
    "{0} This is the field ID for the relevant date data "
    "point".format(relavnt_date_text)
)
UKBB_NUM_REL_DATE_FIELD_INST_ID = ColName(
    'relevant_date_field_instance_id',
    int,
    "{0} This is the instance ID for the relevant date field. Usually this"
    " will be matched to the instance ID of the data field, as it could be"
    " a repeat measure at a different time point".format(relavnt_date_text)
)
UKBB_NUM_REL_DATE_ARRAY_ID = ColName(
    'relevant_date_array_id',
    int,
    "{0} This is the array ID for the relevant date field. Usually this"
    " will be matched to the array ID of the data field, as it could be"
    " a repeat measure at a different time point".format(relavnt_date_text)
)
UKBB_NUM_REL_DATE = ColName(
    'relevant_date',
    tc.parse_global_date,
    "{0} This is the relevant date expressed as a string: "
    "YYYY-MM-DD".format(relavnt_date_text)
)
UKBB_NUM_REL_INT_DATE = ColName(
    'relevant_int_date',
    int,
    "{0} This is the relevant date expressed as an integer: "
    "YYYY-MM-DD".format(relavnt_date_text)
)
UKBB_NUM_EVENT_DAYS_FROM_BASELINE = ColName(
    'event_days_from_baseline',
    int,
    "Using the relevant date. This is the number of days between the "
    "relevant date and the baseline"
)
UKBB_NUM_AGE_YEARS_AT_EVENT = ColName(
    'age_years_at_event',
    float,
    "Using the relevant date. This is the age of the participant when the "
    "data point was generated"
)
UKBB_NUM_IS_FALLBACK_DATE = ColName(
    'is_fallback_date',
    int,
    "In many cases the data for the relavent date will not be available. In"
    " which case a fallback date is used. Firstly, it will fallback to a "
    "previous array ID for the field_id/instance_id, then it will fallback to"
    " a previous instance ID. if that is not possible, then it will fallback"
    " to baseline date"
)
UKBB_NUM_VALUE = ColName(
    'value',
    float,
    "The actual value for the data point"
)

# OLD don't think I need
# INSTANCE_IDX = ColName(
#     'ins_index',
#     int,
#     "A numerical index which together with the eid uniquely identifies the"
#     " corresponding records between the hes events table and hes_operations "
#     "and diagnosis table"
# )

# ARRAY_IDX = ColName(
#     'arr_index',
#     int,
#     "A numerical index which together with the eid & ins_index uniquely "
#     "identifies this record, i.e. eid, ins_index & arr_index together form a"
#     " primary key for this table"
# )

# EVENT_LEVEL = ColName(
#     'level',
#     int,
#     "Classification of operation/procedure: (1) Main operation/diagnosis (2) "
#     "Secondary operation/diagnosis"
# )

# OPERATION_DATE = ColName(
#     'opdate',
#     str,
#     "Date of operation"
# )

# OPCS_THREE = ColName(
#     'oper3',
#     str,
#     "Operative procedures are coded according to the Office of Population "
#     "Censuses and Surveys Classification of Interventions and Procedures, "
#     "version 3 (OPCS-3)."
# )

# OPCS_THREE_NOTES = ColName(
#     'oper3_nb',
#     str,
#     "Currently no data are available."
# )

# OPCS_FOUR = ColName(
#     'oper4',
#     str,
#     "Operative procedures are coded according to the Office of Population "
#     "Censuses and Surveys Classification of Interventions and Procedures, "
#     "version 4 (OPCS-4)."
# )

# OPCS_FOUR_NOTES = ColName(
#     'oper4_nb',
#     str,
#     "Currently no data are available."
# )

# POST_OPERATIVE_DURATION = ColName(
#     'posopdur',
#     int,
#     "Only present in English inpatient data (dsource = 'HES')"
# )

# PRE_OPERATIVE_DURATION = ColName(
#     'preopdur',
#     int,
#     "Only present in English inpatient data (dsource = 'HES')"
# )


# # Processed Event table columns
# EVT_IDX = ColName(
#     'event_idx',
#     int,
#     "The primary key index for the event data"
# )

# EVT_ORDER = ColName(
#     'event_order',
#     int,
#     "The rank order of events for a  participant"
# )

# EVT_DAYS_FROM_BASELINE = ColName(
#     'event_days_from_baseline',
#     int,
#     "The number of days that the event occurred relative to baseline"
# )

# EVT_AGE_AT_EVENT = ColName(
#     'age_at_event',
#     float,
#     "The participant age when the event started"
# )

# EVT_START_DATE = ColName(
#     'event_start_date',
#     str,
#     "The human readable start date of the event"
# )

# EVT_START_DATE_INT = ColName(
#     'event_start_date_int',
#     int,
#     "The start date of the event represented as days since the 01/01/1900"
# )

# EVT_END_DATE = ColName(
#     'event_end_date',
#     str,
#     "The human readable end date of the event"
# )

# EVT_END_DATE_INT = ColName(
#     'event_end_date_int',
#     int,
#     "The end date of the event represented as days since the 01/01/1900"
# )

# EVT_DURATION = ColName(
#     'event_duration',
#     int,
#     "The duration of the event in days "
# )

# EVT_PROVIDER = ColName(
#     'event_provider',
#     int,
#     "The source of the event data"
# )

# EVT_UKBB_INSTANCE_IDX = ColName(
#     'ukbb_ins_idx',
#     int,
#     "The UKBB instance index that is used in combination with the eid to link"
#     " events and codes"
# )


# EVT_UKBB_INSTANCE_IDX = ColName(
#     'ukbb_ins_idx',
#     int,
#     "The UKBB instance index that is used in combination with the eid to link"
#     " events and codes"
# )

# EVT_TYPE = ColName(
#     'event_type',
#     str,
#     "The string type of an event"
# )

# EVT_TYPE_ID = ColName(
#     'event_type_id',
#     int,
#     "The bits of an event"
# )

# EVT_DAYS_PRE_EVENT = ColName(
#     'days_pre_event',
#     str,
#     "The number of relevant days before the event"
# )

# EVT_DAYS_POST_EVENT = ColName(
#     'days_post_event',
#     int,
#     "The number of relevant days after the event"
# )

