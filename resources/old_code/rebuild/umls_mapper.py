"""
functions for mapping UKBB codes into UMLS CUIs
"""
from umls_tools import umls_query
from ukbb_tools import ukbb_mapper_orm as uo
import re
import pprint as pp

STOP_WORDS = ['other']


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class UkbbToUmlsMapper(object):
    """
    Handle the mapping between UKBB clinical codes and the UMLS CUIs
    """
    UMLS = 'umls'
    UKBB = 'ukbb_mapper'
    ICD10 = 'icd10'
    ICD9 = 'icd9'
    OPCS4 = 'opcs4'
    OPCS3 = 'opcs3'
    TERM = 'any_term'
    READ3 = 'read3'
    READ2 = 'read2'

    SAB_MAP = {ICD10: ['ICD10'], ICD9: ['ICD9CM'], READ3: ['RCD']}
    ALLOWED_CODE_TYPES = [ICD10, ICD9]

    ICD_DOT_REGEXP = re.compile(r'^([A-Z]\d\d)(\d+)X?')

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, umls_session_maker, ukbb_mapper_session_maker):
        """
        Parameters
        ----------
        umls_session_maker : :obj:`sqlalchemy.Sessionmaker`
            The sessionmaker for the UMLS database
        ukbb_mapper_session_maker : :obj:`sqlalchemy.Sessionmaker`
            The sessionmaker for the UKBB mapping database
        """
        # Store the sesson makers
        self._umls_sm = umls_session_maker
        self._ukbb_map_sm = ukbb_mapper_session_maker

        # Initiate the session
        self._umls_session = self._umls_sm()
        self._ukbb_session = self._ukbb_map_sm()

        # Derive a query object
        self._umlsq = umls_query.UmlsQueryOrm(self._umls_sm())

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def map_code(self, code, code_type):
        """
        Map a code to a UMLS CUI
        """
        if code_type == self.__class__.READ3:
            return self.map_read_three_code(code)
        elif code_type == self.__class__.ICD10:
            return self.map_icd_ten_code(code)
        elif code_type == self.__class__.READ2:
            return self.map_read_two_code(code)
        elif code_type == self.__class__.OPCS4:
            return self.map_opcs_four_code(code)
        elif code_type == self.__class__.ICD9:
            return self.map_icd_nine_code(code)
        elif code_type == self.__class__.OPCS3:
            return self.map_opcs_three_code(code)
        else:
            raise ValueError("unknown code type '{0}'".format(code_type))

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def map_read_three_code(self, code, subsearch_idx=1):
        # Will log the searches and number of hits returned
        search_log = []

        # We assign the code to another variable as we will introduce a . in it
        # if required for searching the UMLS
        search_code = code

        # Now do an ICD10 lookup against the UKBB mapper. This looks for the
        # raw UKBB entry and these terms will be searched against the UMLS
        # should no UMLS mappings be found. Note that we check for a case match
        # in python
        ukbb_strings = [i.term_ctv3_desc for i in self.search_ukbb_read3(code)]

        # Log the serach and the number of results. The fields are:
        # 0. The number of the search in the mapping chain (subserach_idx)
        # 1. The code we want to map
        # 2. The code in the current search
        # 3. The database baing searched
        # 4. The code type (SAB) being searched
        # 5. The number of unique search matches found
        # 6. FLags that are specific to the search
        search_log.append([subsearch_idx,
                           code,
                           code,
                           self.__class__.UKBB,
                           self.__class__.READ3,
                           len(ukbb_strings),
                           0])
        subsearch_idx += 1

        # Now search the umls for the code
        match_codes = self._umlsq.match_code(
            search_code,
            sab=self.__class__.SAB_MAP[self.__class__.READ3])

        # Make sure we have absolutely exact match
        match_codes = check_code_case(match_codes, code)

        # Make sure that we have only unique matches
        match_codes = extract_unique_cuis(match_codes)

        # Now extract the information we need from the match, this includes
        # it's preferred term and semantic types
        processed_codes = process_cuis(match_codes)

        # Log the serach and the number of results
        search_log.append([subsearch_idx,
                           code,
                           search_code,
                           self.__class__.UMLS,
                           self.__class__.READ3,
                           len(match_codes),
                           0])

        # If we have not found anything in the UMLS then attempt to search
        # with the actual term strings extracted from the UKBB mapper
        if len(processed_codes) == 0:
            str_codes, str_search_log = self._string_search(
                ukbb_strings,
                code,
                subsearch_idx=subsearch_idx)
            processed_codes.extend(str_codes)
            search_log.extend(str_search_log)

        return processed_codes, search_log, ukbb_strings

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def map_read_two_code(self, code, subsearch_idx=1):
        # Will log the seraches and number of hits returned
        search_log = []

        # Will hold the final processed CUIs
        processed_codes = []

        # Holds UMLS cuis that have been mapped, we use this to check for
        # mappings to avoid returning duplicated CUIs
        # found_cuis = set([])

        # Now do an ICD10 lookup against the UKBB mapper. This looks for the
        # raw UKBB entry and these terms will be searched against the UMLS
        # should no UMLS mappings be found
        ukbb_strings = [i.term_v2_desc for i in self.search_ukbb_read2(code)]

        # Log the serach and the number of results. The fields are:
        # 0. The number of the search in the mapping chain (subserach_idx)
        # 1. The code we want to map
        # 2. The code in the current search
        # 3. The database baing searched
        # 4. The code type (SAB) being searched
        # 5. The number of unique search matches found
        # 6. FLags that are specific to the search
        search_log.append([subsearch_idx,
                           code,
                           code,
                           self.__class__.UKBB,
                           self.__class__.READ2,
                           len(ukbb_strings),
                           0])
        # Make sure we have incremented our search log counter
        subsearch_idx += 1

        str_codes, str_search_log = self._string_search(
            ukbb_strings,
            code,
            subsearch_idx=subsearch_idx)
        processed_codes.extend(str_codes)
        search_log.extend(str_search_log)

        return processed_codes, search_log, ukbb_strings

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def map_opcs_four_code(self, code, subsearch_idx=1):
        """
        Map an OPCS4 code into the UMLS. The mapping proceeds as follows:
        1. Find the ICD9 term for the ICD9 code from the UKBB mapper
        2. Map the ICD9 code to the ICD10 code(s) using the UKBB mapper
        3. Map the ICD10 code into the UMLS (as in `map_icd_ten_code`
        4. If there are still no mappings then use the ICD9 terms from (1)
           and search the UMLS

        Parameters
        ----------
        code : str
            An OPCS4 clinical code
        subsearch_idx : int, optional, default: 1
            An integer representing the position in the "search chain" as lots
            of these search functions will call each other, this allows them
            to indicate the start of the numbering in the search log

        Returns
        -------
        mapped_codes : list of tuple
            Each tuple represents a mapping into the UMLS. Note that there map
            not be a 1:1 mapping between the OPCS4 code and UMLS CUI. Within the
            tuple:
            `[0]`: The UMLS concept
            `[1]`: The UMLS SAB (source abbreviation) of the mapping
            `[2]`: The UMLS preferred term
            `[3]`: A list of the UMLS semantic types for the CUI
        search_log : list of list
            The searches that have been carried out to get (or attempt to get)
            the mapping. The elements in the sub lists are as follows:
            `[0]`: The number of the search in the mapping chain (subsearch_idx)
            `[1]`: The code we want to map
            `[2]`: The code in the current search
            `[3]`: The database baing searched
            `[4]`: The code type (SAB) being searched
            `[5]`: The number of unique search matches found
            `[6]`: Flags that are specific to the search
        ukbb_strings : list of str
            The OPCS4 terms that correspond to the OPCS4 code, these are
            extracted from the UKBB mapper database
        """
        # Will log the seraches and number of hits returned
        search_log = []

        # Will hold the final processed CUIs
        processed_codes = []

        # Holds UMLS cuis that have been mapped, we use this to check for
        # mappings to avoid returning duplicated CUIs
        found_cuis = set([])

        # Now do an ICD10 lookup against the UKBB mapper. This looks for the
        # raw UKBB entry and these terms will be searched against the UMLS
        # should no UMLS mappings be found
        ukbb_strings = [i.opcs_v4_desc for i in self.search_ukbb_opcs4(code)]

        # Log the serach and the number of results. The fields are:
        # 0. The number of the search in the mapping chain (subserach_idx)
        # 1. The code we want to map
        # 2. The code in the current search
        # 3. The database baing searched
        # 4. The code type (SAB) being searched
        # 5. The number of unique search matches found
        # 6. FLags that are specific to the search
        search_log.append([subsearch_idx,
                           code,
                           code,
                           self.__class__.UKBB,
                           self.__class__.OPCS4,
                           len(ukbb_strings),
                           0])
        # Make sure we have incremented our search log counter
        subsearch_idx += 1

        # We start by mapping the ICD9 codes to ICD10 codes and then we search
        # for these in the UMLS
        read_three_mappings = self.opcs_four_to_read_three(code)

        # There could be multiple ICD10 mappings to the ICD9 code. i here is
        # a ORM object
        for i in read_three_mappings:
            # Attempt to map into the UMLS via the ICD10 codes
            cui_codes, code_log, code_ukbb_strings = \
                self.map_read_three_code(i.read_ctv3_code,
                                         subsearch_idx=subsearch_idx)

            # If we are mapping multiple ICD10 codes in different iterations of
            # this loop, there is a possibility that they will return the same
            # or overlapping CUIs. Therefore we will only capture unique
            # concepts
            used = 0
            for cui, sab, term, stypes in cui_codes:
                # Do we have it already if not then process it
                if cui not in found_cuis:
                    found_cuis.add(cui)
                    processed_codes.append((cui, sab, term, stypes))
                    used += 1

                # if we have used some of the mappings from this iteration
                # then we store the search log, if not then we ignore it
                if used > 0:
                    # Add the crossmapping flags to the log
                    flag = '|'.join([i.mapping_status,
                                     i.refine_flag,
                                     i.add_code_flag])
                    for c in code_log:
                        c[-1] = flag
                    search_log.extend(code_log)
                    subsearch_idx = len(search_log) + 1

        # If we have not found anything in the UMLS then attempt to search
        # with the actual term strings extracted from the UKBB mapper
        if len(processed_codes) == 0:
            str_codes, str_search_log = self._string_search(
                ukbb_strings,
                code,
                subsearch_idx=subsearch_idx)
            processed_codes.extend(str_codes)
            search_log.extend(str_search_log)

        return processed_codes, search_log, ukbb_strings

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def map_opcs_three_code(self, code, subsearch_idx=1):
        """
        Map an OPCS3 code into the UMLS. The mapping proceeds as follows:
        1. Find the OPCS3 term for the OPCS3 code from the UKBB mapper
        2. Map the ICD9 code to the ICD10 code(s) using the UKBB mapper
        3. Map the ICD10 code into the UMLS (as in `map_icd_ten_code`
        4. If there are still no mappings then use the ICD9 terms from (1)
           and search the UMLS

        Parameters
        ----------
        code : str
            An OPCS3 clinical code
        subsearch_idx : int, optional, default: 1
            An integer representing the position in the "search chain" as lots
            of these search functions will call each other, this allows them
            to indicate the start of the numbering in the search log

        Returns
        -------
        mapped_codes : list of tuple
            Each tuple represents a mapping into the UMLS. Note that there map
            not be a 1:1 mapping between the OPCS4 code and UMLS CUI. Within the
            tuple:
            `[0]`: The UMLS concept
            `[1]`: The UMLS SAB (source abbreviation) of the mapping
            `[2]`: The UMLS preferred term
            `[3]`: A list of the UMLS semantic types for the CUI
        search_log : list of list
            The searches that have been carried out to get (or attempt to get)
            the mapping. The elements in the sub lists are as follows:
            `[0]`: The number of the search in the mapping chain (subsearch_idx)
            `[1]`: The code we want to map
            `[2]`: The code in the current search
            `[3]`: The database baing searched
            `[4]`: The code type (SAB) being searched
            `[5]`: The number of unique search matches found
            `[6]`: Flags that are specific to the search
        ukbb_strings : list of str
            The OPCS3 terms that correspond to the OPCS3 code, these are
            extracted from the UKBB mapper database. However, this function
            queries into OPCS4 so it could be an OPCS4 string
        """
        # Will log the seraches and number of hits returned
        search_log = []

        # Will hold the final processed CUIs
        processed_codes = []

        # Now do an OPCS3 lookup against the UKBB mapper. This looks for the
        # raw UKBB entry and these terms will be searched against the UMLS
        # as no mapping exists between OPCS3 and 4
        ukbb_strings = [i.opcs_v3_desc for i in self.search_ukbb_opcs3(code)]

        # We have some strings so search them against the UMLS directly to
        # see if we come back with any CUIs
        str_codes = []
        if len(ukbb_strings) > 0:
            str_codes, str_search_log = self._string_search(
                ukbb_strings,
                code,
                subsearch_idx=subsearch_idx)
            processed_codes.extend(str_codes)
            search_log.extend(str_search_log)

        # If we have not found anything, I have noticed that many OPCS3 codes
        # actually look like OPCS4 codes and nothing like OPCS3 codes
        if len(str_codes) == 0:
            # Make sure we have incremented our search log counter
            subsearch_idx += 1
            processed_codes, search_log, ukbb_strings = \
                self.map_opcs_four_code(code, subsearch_idx=subsearch_idx)

        return processed_codes, search_log, ukbb_strings

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def map_icd_ten_code(self, code, subsearch_idx=1):
        """
        Map an ICD10 code
        """
        # Will log the seraches and number of hits returned
        search_log = []

        # We assign the code to another variable as we will introduce a . in it
        # if required for searching the UMLS
        search_code = code

        # Now do an ICD10 lookup against the UKBB mapper. This looks for the
        # raw UKBB entry and these terms will be searched against the UMLS
        # should no UMLS mappings be found
        ukbb_strings = [i.description for i in self.search_ukbb_icd10(code)]

        # Log the serach and the number of results. The fields are:
        # 0. The number of the search in the mapping chain (subserach_idx)
        # 1. The code we want to map
        # 2. The code in the current search
        # 3. The database baing searched
        # 4. The code type (SAB) being searched
        # 5. The number of unique search matches found
        # 6. FLags that are specific to the search
        search_log.append([subsearch_idx,
                           code,
                           code,
                           self.__class__.UKBB,
                           self.__class__.ICD10,
                           len(ukbb_strings),
                           0])

        subsearch_idx += 1

        # The UKBB codes to not have the . format but the UMLS ones do, so if
        # this code does not have a . format then add one
        if self.__class__.ICD_DOT_REGEXP.match(code):
            search_code = self.__class__.ICD_DOT_REGEXP.sub(r'\1.\2', code)

        # Now search the umls for the code
        match_codes = self._umlsq.match_code(
            search_code,
            sab=self.__class__.SAB_MAP[self.__class__.ICD10])

        # Make sure that we have only unique matches
        match_codes = extract_unique_cuis(match_codes)

        # Now extract the information we need from the match, this includes
        # it's preferred term and semantic types
        processed_codes = process_cuis(match_codes)

        # Log the serach and the number of results
        search_log.append([subsearch_idx,
                           code,
                           search_code,
                           self.__class__.UMLS,
                           self.__class__.ICD10,
                           len(match_codes),
                           0])

        # If we have not found anything in the UMLS then attempt to search
        # with the actual term strings extracted from the UKBB mapper
        if len(processed_codes) == 0:
            str_codes, str_search_log = self._string_search(
                ukbb_strings,
                code,
                subsearch_idx=subsearch_idx)
            processed_codes.extend(str_codes)
            search_log.extend(str_search_log)

        return processed_codes, search_log, ukbb_strings

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def map_icd_nine_code(self, code, subsearch_idx=1):
        """
        Map an ICD9 code into the UMLS. The mapping proceeds as follows:
        1. Find the ICD9 term for the ICD9 code from the UKBB mapper
        2. Map the ICD9 code to the ICD10 code(s) using the UKBB mapper
        3. Map the ICD10 code into the UMLS (as in `map_icd_ten_code`
        4. If there are still no mappings then use the ICD9 terms from (1)
           and search the UMLS

        Parameters
        ----------
        code : str
            An ICD9 clinical code
        subsearch_idx : int, optional, default: 1
            An integer representing the position in the "search chain" as lots
            of these search functions will call each other, this allows them
            to indicate the start of the numbering in the search log

        Returns
        -------
        mapped_codes : list of tuple
            Each tuple represents a mapping into the UMLS. Note that there map
            not be a 1:1 mapping between ICD9 code and UMLS CUI. Within the
            tuple:
            `[0]`: The UMLS concept
            `[1]`: The UMLS SAB (source abbreviation) of the mapping
            `[2]`: The UMLS preferred term
            `[3]`: A list of the UMLS semantic types for the CUI
        search_log : list of list
            The searches that have been carried out to get (or attempt to get)
            the mapping. The elements in the sub lists are as follows:
            `[0]`: The number of the search in the mapping chain (subsearch_idx)
            `[1]`: The code we want to map
            `[2]`: The code in the current search
            `[3]`: The database baing searched
            `[4]`: The code type (SAB) being searched
            `[5]`: The number of unique search matches found
            `[6]`: Flags that are specific to the search
        ukbb_strings : list of str
            The ICD9 terms that correspond to the ICD9, these are extracted
            from the UKBB mapper database
        """
        # Will log the seraches and number of hits returned
        search_log = []

        # Will hold the final processed CUIs
        processed_codes = []

        # Holds UMLS cuis that have been mapped, we use this to check for
        # mappings to avoid returning duplicated CUIs
        found_cuis = set([])

        # Now do an ICD10 lookup against the UKBB mapper. This looks for the
        # raw UKBB entry and these terms will be searched against the UMLS
        # should no UMLS mappings be found
        ukbb_strings = [i.icd_v9_desc for i in self.search_ukbb_icd9(code)]

        # Log the serach and the number of results. The fields are:
        # 0. The number of the search in the mapping chain (subserach_idx)
        # 1. The code we want to map
        # 2. The code in the current search
        # 3. The database baing searched
        # 4. The code type (SAB) being searched
        # 5. The number of unique search matches found
        # 6. FLags that are specific to the search
        search_log.append([subsearch_idx,
                           code,
                           code,
                           self.__class__.UKBB,
                           self.__class__.ICD9,
                           len(ukbb_strings),
                           0])
        # Make sure we have incremented our search log counter
        subsearch_idx += 1

        # We start by mapping the ICD9 codes to ICD10 codes and then we search
        # for these in the UMLS
        icd_ten_mappings = self.icd_nine_to_icd_ten(code)

        # There could be multiple ICD10 mappings to the ICD9 code. i here is
        # a ORM object
        for i in icd_ten_mappings:
            # Attempt to map into the UMLS via the ICD10 codes
            icd_ten_cui_codes, icd_ten_log, icd_ten_ukbb_strings = \
                self.map_icd_ten_code(i.icd_v10_code,
                                      subsearch_idx=subsearch_idx)

            # If we are mapping multiple ICD10 codes in different iterations of
            # this loop, there is a possibility that they will return the same
            # or overlapping CUIs. Therefore we will only capture unique
            # concepts
            used = 0
            for cui, sab, term, stypes in icd_ten_cui_codes:
                # Do we have it already if not then process it
                if cui not in found_cuis:
                    found_cuis.add(cui)
                    processed_codes.append((cui, sab, term, stypes))
                    used += 1

                # if we have used some of the mappings from this iteration
                # then we store the search log, if not then we ignore it
                if used > 0:
                    search_log.extend(icd_ten_log)
                    subsearch_idx = len(search_log) + 1

        # If we have not found anything in the UMLS then attempt to search
        # with the actual term strings extracted from the UKBB mapper
        if len(processed_codes) == 0:
            str_codes, str_search_log = self._string_search(
                ukbb_strings,
                code,
                subsearch_idx=subsearch_idx)
            processed_codes.extend(str_codes)
            search_log.extend(str_search_log)

        return processed_codes, search_log, ukbb_strings

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def search_ukbb_read3(self, code):
        """
        Query the UKBB mapper for a read3 code. Read codes are case sensitive
        and it is unclear if the database will be case sensitive so the
        returned codes are checked in python before returning
        """
        q = self._ukbb_session.query(uo.ReadCtv3Lkp).\
            filter(uo.ReadCtv3Lkp.read_ctv3_code == code)
        return [i for i in q.all() if i.read_ctv3_code == code]

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def search_ukbb_read2(self, code):
        """
        Query the UKBB mapper for an ICD10 code. Read codes are case sensitive
        and it is unclear if the database will be case sensitive so the
        returned codes are checked in python before returning
        """
        q = self._ukbb_session.query(uo.ReadV2Lkp).\
            filter(uo.ReadV2Lkp.read_v2_code == code)
        return [i for i in q.all() if i.read_v2_code == code]

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def search_ukbb_icd10(self, code):
        """
        Query the UKBB mapper for an ICD10 code
        """
        q = self._ukbb_session.query(uo.Icd10Lkp).\
            filter(uo.Icd10Lkp.alt_code == code)
        return q.all()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def search_ukbb_icd9(self, code):
        """
        Query the UKBB mapper for an ICD10 code
        """
        q = self._ukbb_session.query(uo.Icd9Lkp).\
            filter(uo.Icd9Lkp.icd_v9_code == code)
        return q.all()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def search_ukbb_opcs4(self, code):
        """
        Query the UKBB mapper for an OPCS4 code
        """
        q = self._ukbb_session.query(uo.OpcsV4Lkp).\
            filter(uo.OpcsV4Lkp.opcs_v4_code == code)
        return q.all()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def search_ukbb_opcs3(self, code):
        """
        Query the UKBB mapper for an OPCS4 code
        """
        q = self._ukbb_session.query(uo.OpcsV3Lkp).\
            filter(uo.OpcsV3Lkp.opcs_v3_code == code)
        return q.all()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def opcs_four_to_read_three(self, code):
        """

        """
        q = self._ukbb_session.query(uo.ReadCtv3Opcs4).\
            filter(uo.ReadCtv3Opcs4.opcs_v4_code == code)
        return q.all()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def icd_nine_to_icd_ten(self, code):
        """
        Convert an ICD9 code to an ICD10 code
        """
        q = self._ukbb_session.query(uo.Icd9Icd10).\
            filter(uo.Icd9Icd10.icd_v9_code == code)
        return q.all()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def read_two_to_read_three(self, code):
        """
        Convert a read2 code to a read3 code. Read codes are case sensitive
        and it is unclear if the database will be case sensitive so the
        returned codes are checked in python before returning
        """
        q = self._ukbb_session.query(uo.ReadCtv3ReadV2).\
            filter(uo.ReadCtv3ReadV2.read_v2_code == code)
        return [i for i in q.all() if i.read_v2_code == code]

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _string_search(self, search_strings, code, subsearch_idx=1):
        """

        """
        processed_codes = []
        search_log = []
        all_string_matches = []
        for i in search_strings:
            if i.lower() in STOP_WORDS:
                continue

            str_matches = extract_unique_cuis(self._umlsq.match_term(i))
            all_string_matches.extend(str_matches)

            # Log the serach and the number of results
            search_log.append([subsearch_idx,
                               code,
                               i,
                               self.__class__.UMLS,
                               self.__class__.TERM,
                               len(str_matches),
                               0])
        unique_matches = extract_unique_cuis(all_string_matches)
        processed_codes = process_cuis(unique_matches)

        return processed_codes, search_log


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def extract_unique_cuis(matches):
    """
    From all the matches extract matches with unique CUIs, this will take the
    first CUI from the match it sees

    Parameters
    ----------
    matches : :obj:`umls_orm.MrConso`
        This is intended for an MrConso object but will work on other types with
        a `cui` attribute

    Returns
    -------
    unique_matches : :obj:`umls_orm.MrConso`
        Matches that are unique for their `cui` attribute. Their type will be
        whatever object type was passed
    """
    unique_matches = []
    seen_cuis = set()
    for m in matches:
        if m.cui not in seen_cuis:
            unique_matches.append(m)
            seen_cuis.add(m.cui)
    return unique_matches


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def process_cuis(matches):
    """
    Process the CUI matches to ID the unique preferred term and the semantic
    types for the match

    Parameters
    ----------
    matches : :obj:`umls_orm.MrConso`
        This is intended for an MrConso object but will work on other types with
        a `cui` attribute

    Returns
    -------
    unique_matches : :obj:`umls_orm.MrConso`
        Matches that are unique for their `cui` attribute. Their type will be
        whatever object type was passed
    """
    processed_matches = []
    for m in matches:
        # stys = [s.sty for s in m.sty]
        processed_matches.extend([(m.cui, m.sab, u.term, u.semantic_types)
                                  for u in m.unique_umls_cui])
    return processed_matches


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def check_code_case(matches, check_code):
    """
    
    """
    return [m for m in matches if m.code == check_code]
