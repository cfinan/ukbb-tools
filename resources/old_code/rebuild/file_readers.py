"""
.. include:: ../docs/file_readers.md
"""
from ukbb_tools import base_readers as br, file_defs as fd
from collections import namedtuple
import pprint as pp


# The file specific arguments are parsed to the MultiReader classes via this
# ReaderDefinition named tuple. This just makes things a bit cleaner
ReaderDefinition = namedtuple(
    'ReaderDefinition',
    ['reader', 'name', 'key_col', 'reset']
)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class MultiReader(object):
    """
    A `MultiReader` class will iterate through several files at once based on
    a key column. Rows matching the key column values that are higher in the
    sort order are returned before rows that are lower in the key column
    values sort order. For this to function, the files are required to be
    sorted on the key column. This class just ensures that the rows from all
    the files that match a key value are returned in the same batch. So they
    can be processed alongside each other.
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, *reader_defs):
        """
        Parameters
        ----------
        *readers
            One or more `ReaderDefinition` objects to process in batches

        Raises
        ------
        ValueError
            If no `ReaderDefinition` named tuples are passed
        KeyError
            If the names of the `ReaderDefinition` named tuples are not unique
        TypeError
            If the `reader` attributes do not contain objects that inherit from
            `BaseFileReader`
        """
        self._readers = list(reader_defs)

        # Make sure we have at least one reader, admittedly it is pointless
        # with a single reader but should still work
        if len(self._readers) < 1:
            raise ValueError("no readers passed")

        # Make sure that the names are unique
        names = [i.name for i in self._readers]

        # Make sure all the names are unique
        if len(names) != len(set(names)):
            raise KeyError(
                "the names of the reader definitions should be unique"
            )

        # TODO Should I check if the reader objects are shared in definitions?

        # Make sure all the readers inherit from BaseFileReader
        for i in self._readers:
            if not issubclass(i.reader.__class__, br.BaseFileReader):
                raise TypeError(
                    "readers should inherit from BaseFileReader: {0}".format(
                        i.reader.__class__)
                )

        # initialise object variables
        self._is_open = False
        self._using_context_manager = False
        self._next_rows = []
        self._curr_rows = {}
        self._seen_keys = set()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __enter__(self):
        """
        Entry point for the context manager
        """
        self.open()
        self._using_context_manager = True
        return self

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __exit__(self, *args):
        """
        Entry point for the context manager

        *args
            Error arguments should the context manager exit in discrace
        """
        self.close()
        self._using_context_manager = False

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __iter__(self):
        """
        Initialise the iterator. This will iterate in batches of rows
        matching the key highest in the sort order

        Returns
        -------
        batch : dict of lists
            The keys of the dict are the names from the `ReaderDefinition`
            objects and each list contains the rows matching the current key
            from their respective readers
        """
        return self

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __next__(self):
        """
        The next batch of rows matching the current highest key in the sort
        order.

        Returns
        -------
        batch : dict of lists
            The keys of the dict are the names from the `ReaderDefinition`
            objects and each list contains the rows matching the current key
            from their respective readers
        """
        return self._next_batch()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def header(self):
        """
        Return a batch of headers from all the readers

        Returns
        -------
        batch : dict of lists
            The keys of the dict are the names from the `ReaderDefinition`
            objects and each list contains the header column names (strings)
        """
        headers = {}
        for i in self._readers:
            headers[i.name] = i.header
        return headers

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def open(self):
        """
        Open all the readers

        Raises
        ------
        IOError
            If the MultiReader is already open
        """
        # If this has already been called then error out
        if self._is_open is True:
            raise IOError('all readers already open')

        # Loop through all the readers and open in turn
        for i in self._readers:
            try:
                # There is a possibility that the reader is already open
                # in which case we pass silently through
                i.reader.open()
            except IOError as e:
                if e.args[0] != 'file already open':
                    raise

            # If the user wants to reset the state of the reader to the start
            # of the file then do it
            if i.reset is True:
                i.reader.reset()

        # We initially want to gather rows from all the readers and then work
        # out what readers will be providing rows for return in the first batch
        self._prime_rows()
        self._set_next_keys()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _prime_rows(self):
        """
        Load up the first row

        Raises
        ------
        StopIteration
            If the reader have no data in them
        """
        for i in self._readers:
            try:
                # Get the first data row from the reader
                self._curr_rows[i] = next(i.reader)
            except StopIteration:
                # This essentially means there is no data row for that reader
                # so we close it and dispose of it
                self._close_reader(i)

        # Make sure we have some readers left after priming
        self._check_finished()

        if len(self._curr_rows) == 0:
            raise StopIteration("no data left")

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _check_finished(self):
        """
        Checks to see if we have finished processing all the readers

        Raises
        ------
        StopIteration
            If all the readers have been closed and disposed of
        """
        if len(self._readers) == 0:
            msg = "all readers finished"
            raise StopIteration(msg)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _close_reader(self, reader_def):
        """
        Controls the sequence of events required to close a reader and remove
        it from the `MultiReader`
        """
        # Call the readers close method
        reader_def.reader.close()

        # Now we want to remove the reader from the list of all readers so
        # it no longer features
        pop_idx = self._readers.index(reader_def)
        self._readers.pop(pop_idx)
        try:
            # Make sure any current rows referencing to that reader
            # are removed
            del self._curr_rows[reader_def]
        except KeyError:
            pass

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _next_batch(self):
        """
        Get the next batch of rows from readers that have key values the
        highest in the sort order

        Returns
        -------
        batch : dict of lists
            The keys of the dict are the names from the `ReaderDefinition`
            objects and each list contains the rows matching the current key
            from their respective readers
        """
        # Make sure we have not finished
        self._check_finished()

        # This will hold all the rows to be returned in the batch, these will
        # be keysed by the RecordDefinition name
        return_rows = {}

        # next keys only has the RecordDefinitons of the records that have
        # the highest sort order keys in their current record
        for i in self._next_keys:
            # Initialise from the current row
            return_rows[i.name] = [self._curr_rows[i]]

            try:
                # Get the next row in the reader. This could raise
                # a StopIteration if there are no more rows to return
                curr_row = next(i.reader)

                # If the key column of the row just extracted matches
                # the key column value that we currently require then it
                # gets added to the batch of the current record and we
                # attempt to get the next row (which also could fail with
                # a stop iteration)
                while curr_row[i.key_col] == self._curr_key:
                    return_rows[i.name].append(curr_row)
                    curr_row = next(i.reader)
                # If we have got here then the current row that we have does
                # not belong in the current batch and as we have not raised
                # a StopIteration we are not at the end yet. So we store the
                # row for evaluation when defining the next required key
                self._curr_rows[i] = curr_row
            except StopIteration:
                # This essentially means there is no data row for that reader
                # so we close it and dispose of it
                self._close_reader(i)

        # Now define the next required key and readers that have the current
        # rows loaded that satisfy that criteria
        self._set_next_keys()
        return return_rows

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _set_next_keys(self):
        """
        Look at the key columns in the current rows and determine the files
        with have the keys that are next in the sort order. These are
        assigned to `self._next_keys`
        """
        # sort so the currently loaded rows with key column values that are
        # highest in the sort order are at the top of the returned list of
        # tuples. In this x is a tuple with the RecordDefinition at [0] and
        # the current row at [1] and we sort on the key column defined in the
        # key_col attribute of the RecordDefinition
        try:
            # This could raise an index error if we are on the very last
            # iteration and there is no data in self._curr_rows. In this case
            # we just return as this method will not be called again
            key_order = sorted(
                self._curr_rows.items(),
                key=lambda x: x[1][x[0].key_col]
            )
        except IndexError:
            if len(self._curr_rows) == 0:
                return
            # If we get here then the IndexError was raised for another
            # undefined reason
            raise

        # Initialise the key that we are looking for which is the key column
        # value of the first element
        # TODO: on last batch, implement an MultiReader test:
        #           File "/home/rmjdcfi/miniconda3/bin/ukbb_db_builder", line 33, in <module>
        #     sys.exit(load_entry_point('ukbb-tools', 'console_scripts', 'ukbb_db_builder')())
        #   File "/home/rmjdcfi/code/ukbb_tools/ukbb_tools/db_build.py", line 76, in main
        #     builder.build()
        #   File "/home/rmjdcfi/code/ukbb_tools/ukbb_tools/db_build.py", line 634, in build
        #     self._insert_ukbb_data()
        #   File "/home/rmjdcfi/code/ukbb_tools/ukbb_tools/db_build.py", line 1062, in _insert_ukbb_data
        #     for batch in prog.progress(infiles):
        #   File "/home/rmjdcfi/code/simple_progress/simple_progress/progress.py", line 746, in progress
        #     for i in obj:
        #   File "/home/rmjdcfi/code/ukbb_tools/ukbb_tools/file_readers.py", line 127, in __next__
        #     return self._next_batch()
        #   File "/home/rmjdcfi/code/ukbb_tools/ukbb_tools/file_readers.py", line 290, in _next_batch
        #     self._set_next_keys()
        #   File "/home/rmjdcfi/code/ukbb_tools/ukbb_tools/file_readers.py", line 322, in _set_next_keys
        #     self._curr_key = key_order[0][1][key_order[0][0].key_col]
        # IndexError: list index out of range
        self._curr_key = key_order[0][1][key_order[0][0].key_col]

        # This is a sanity check to make sure we have not already seen the key
        # if the files are sorted correctly then we should not have
        if self._curr_key in self._seen_keys:
            raise IndexError("key has already been processed, are your "
                             "files sorted on the key columns?")

        # Now define the readers that have rows matching the currently required
        # key and add the key column value to the seen keys so it will error
        # out if encountered again
        self._next_keys = [r for r, v in key_order
                           if v[r.key_col] == self._curr_key]
        self._seen_keys.add(self._curr_key)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def close(self):
        """
        Close all the readers
        """
        if self._is_open is False and self._using_context_manager is False:
            raise IOError('file not open')

        # If all the readers have been disposed of during the run then this
        # will be empty
        for i in self._readers:
            i.reader.close()

        self._is_opened = False


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class HesOperationsIdx(fd.HesOperationsDef, br.UkbbIndexReader):
    """
    The index reader for the input HES operations file
    """
    pass


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class GpClinicalIdx(fd.GpClinicalDef, br.UkbbIndexReader):
    """
    A index reader for the input GP clinical file
    """
    pass


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class HesDiagnosisIdx(fd.HesDiagnosisDef, br.UkbbIndexReader):
    """
    A index reader for the input HES diagnosis file
    """
    pass


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class HesEventsIdx(fd.HesEventsDef, br.UkbbIndexReader):
    """
    A index reader for the input HES events file
    """
    pass


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class GpRegistrationsIdx(fd.GpRegistrationsDef, br.UkbbIndexReader):
    """
    A index reader for the input GP registrations file
    """
    pass


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# ----------------------------- OUTPUT FILE DEFS ------------------------------
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class SamplesIdx(fd.SamplesDef, br.UkbbIndexReader):
    """
    A index reader for the output samples file
    """
    pass


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class ClinicalCodesIdx(fd.ClinicalCodesDef, br.UkbbIndexReader):
    """
    A index reader for the output clinical codes file
    """
    pass


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class ClinicalDataIdx(fd.ClinicalDataDef, br.UkbbIndexReader):
    """
    A file iterator for the output clinical data file
    """
    pass


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class UkbbEnumIdx(fd.UkbbEnumDef, br.UkbbIndexReader):
    """
    A index reader for the output UKBB long format enums file
    """
    pass


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class UkbbStringIdx(fd.UkbbStringDef, br.UkbbIndexReader):
    """
    A index reader for the UKBB long format strings file
    """
    pass


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class UkbbDateIdx(fd.UkbbDateDef, br.UkbbIndexReader):
    """
    A index reader for the UKBB long format dates file
    """
    pass


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class UkbbNumericIdx(fd.UkbbNumericDef, br.UkbbIndexReader):
    """
    A index reader for the UKBB long format numerics file
    """
    pass


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class SamplesReader(fd.SamplesDef, br.BaseFileReader):
    """
    A file iterator for the output samples file
    """
    pass


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class ClinicalCodesReader(fd.ClinicalCodesDef, br.BaseFileReader):
    """
    A file iterator for the output clinical codes file
    """
    pass


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class ClinicalDataReader(fd.ClinicalDataDef, br.BaseFileReader):
    """
    A file iterator for the output clinical data file
    """
    pass


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class UkbbEnumReader(fd.UkbbEnumDef, br.BaseFileReader):
    """
    A file iterator for the UKBB long format enums file
    """
    pass


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class UkbbStringReader(fd.UkbbStringDef, br.BaseFileReader):
    """
    A file iterator for the UKBB long format strings file
    """
    pass


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class UkbbDateReader(fd.UkbbDateDef, br.BaseFileReader):
    """
    A file iterator for the UKBB long format dates file
    """
    pass


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class UkbbNumericReader(fd.UkbbNumericDef, br.BaseFileReader):
    """
    A file iterator for the UKBB long format numerics file
    """
    pass
