"""
process prescriptions
"""
from ukbb_tools import __version__, __name__ as pkg_name, pheno_normaliser
from pyaddons import file_helper
from simple_progress import progress
from operator import itemgetter
import pprint as pp
import argparse
import csv
import re
import tempfile
import shutil
import sys
import os
import datetime

# form (tablet/liquid) and total_amount (strength * packsize * no of tablets
OUT_HEADER = ['drug_strength', 'units', 'pack_size', 'pack_quantity',
              'form', 'total_amount']
DRUG_COL = 0
DOSE_COL = 1

LIQUID = 'liquid'
BALLOON = 'balloon'
CREAM = 'cream'
WIPE = 'wipe'
STICK = 'stick'
DRESSING = 'dressing'
TABLET = 'tablet'
POWDER = 'powder'

PERSCRIPTION_FORMS = [
    (r'(?:liquid\s+)?parafin', LIQUID),
    (r'(?!liquid\s+)?parafin', CREAM),
    (r'syringe(s)?', LIQUID),
    (r'balloon', BALLOON),
    (r'dressin(g)?(s)?', DRESSING),
    (r'cream(s)?', CREAM),
    (r'foam', CREAM),
    (r'cr', CREAM),
    (r'oint(?:ment)?', CREAM),
    (r'spray', LIQUID),
    (r'wipe(s)?', WIPE),
    (r'stick(s)?', STICK),
    (r'wrap(s)?', DRESSING),
    (r'suture(s)?', DRESSING),
    (r'drop(s)?', LIQUID),
    (r'solution(s)?', LIQUID),
    (r'(?!gel).+sachet', POWDER),
    (r'gel', LIQUID),
    (r'gauze', DRESSING),
    (r'lint', DRESSING),
    (r'cotton', DRESSING),
    (r'tab(s)?', TABLET),
    (r'tablet(s)?', TABLET),
    (r'tablet\(s\)', TABLET),
    (r'powder\(s\)', POWDER),
    (r'pwdr\(s\)', POWDER)
]

form_regexp = re.compile(r'''
                        \b(?P<FORM>
                        (?:liquid\s+)?parafin|
                        syringe|
                        balloon|
                        dressing|
                        cream|
                        foam|
                        spray|
                        wipe|
                        sticks|
                        wrap|
                         )\b''', re.VERBOSE | re.IGNORECASE)

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_medicine(drug, dose):
    """
    Parse the actual drug to determine
    """
    return drug, dose


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def init_cmd_args():
    """
    Initialise the command line arguments and return the parsed arguments

    Returns
    -------
    args : :obj:`argparse.Namespace`
        The argparse namespace object
    """
    parser = argparse.ArgumentParser(description="process UKBB phenotypes")

    parser.add_argument('presfile', type=str,
                        help="The prescription file")

    parser.add_argument('outfile', type=str, nargs='?',
                        help="The output file, if not supplied output to"
                             "STDOUT")
    return parser.parse_args()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_prescriptions(infile, outfile):
    """

    :return:
    """
    dialect = pheno_normaliser.sniff_delimiter(infile)

    with open(infile) as infile:
        reader = csv.Reader(infile, dialect=dialect)
        header = next(reader)

        with open(outfile, 'wt') as outcsv:
            writer = csv.writer(outcsv, dialect=dialect)
            writer.writerow(OUT_HEADER)
            for row in reader:
                parsed = parse_medicine(row[DRUG_COL], row[DOSE_COL])
                pp.pprint(parsed)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main():
    """
    The main entry point for the script
    """
    args = init_cmd_args()
    m = progress.Msg(prefix='', verbose=args.verbose, file=sys.stderr)
    m.msg("= prescription parser ({0} v{1}) =".format(pkg_name, __version__))
    m.prefix = '[info]'
    m.msg_atts(args)
    parse_prescriptions(args.infile, args.outfile)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == '__main__':
    main()
