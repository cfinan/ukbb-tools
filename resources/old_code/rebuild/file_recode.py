"""
.. include:: ../docs/file_recode.md
"""
from ukbb_tools import config, constants as ukbbc, file_handler, \
    __version__, __name__ as pkg_name, umls_mapper, \
    temp_db_orm as torm, wide_file_handler as wfh, data_dict as dd, \
    wide_file_extract as wfe, wide_file_common as wfc, \
    clinical_code_parser, event_parser, unit_parser, common, \
    db_build
from pyaddons import recode_file
import argparse
import sys
import os
import warnings
import pprint as pp

# Move a load of these to constants
# The prefix for verbose messages and the location to write the messages
MSG_PREFIX = '[info]'
MSG_OUT = sys.stderr


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main():
    """
    The main entry point for the script
    """
    # Initialise and parse the command line arguments
    args = init_cmd_args()

    # Start a msg output, this will respect the verbosity and will output to
    # STDERR
    m = progress.Msg(prefix='', verbose=args.verbose, file=MSG_OUT)
    m.msg("= UKBB file recode ({0} v{1}) =".format(pkg_name, __version__))
    m.prefix = MSG_PREFIX
    m.msg_atts(args)

    # setup access to the config file
    config_obj, umls_map_sm, ukbb_map_sm, temp_sm, temp_db_name = initialise(
        config_file=args.config_file, in_memory=args.high_mem,
        tmpdir=args.tmp_dir)

    # Process the UKBB phenotype files. The arguments here are:
    # 1. The output directory
    # 2. The config file object
    # 3. The sessionmaker for the UMLS database
    # 4. The sessionmaker for the UKBB mapper database
    # 5. The sessionmaker for the temp database
    # 6. The wide UKBB fields file
    # 7. The path to the HES event date table
    # 8. The path to the HES diagnosis table
    # 9. The path to the HES operations table
    # 10. The path to the GP data table
    # 11. verbose
    # 12. The temporary directory location
    try:
        process_files(
            args.outdir,
            config_obj,
            umls_map_sm,
            ukbb_map_sm,
            temp_sm,
            args.ukbb_wide_file,
            args.hes_events,
            args.hes_diag,
            args.hes_oper,
            args.gp_clinic,
            args.gp_registrations,
            verbose=args.verbose,
            tmp_root=args.tmp_dir
        )
    finally:
        umls_map_sm.close_all()
        ukbb_map_sm.close_all()
        temp_sm.close_all()
        # TODO: Comment back in for production
        # shutil.move(temp_db_name, '/scratch/ukbb/processed_data/ukbb_db.db')


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def init_cmd_args():
    """
    Initialise the command line arguments and return the parsed arguments

    Returns
    -------
    args : :obj:`argparse.Namespace`
        The argparse namespace object
    """
    parser = argparse.ArgumentParser(description="process UKBB phenotypes")

    # The wide format UKBB data fields filex
    parser.add_argument('outdir', type=str,
                        help="The output directory to write the finished "
                        "files")
    parser.add_argument('--tmp-dir', type=str,
                        help="A temp directory location "
                        "(default: system temp")
    parser.add_argument('--high-mem', action='store_true',
                        help="Use in memory storage as opposed to tmp")
    parser.add_argument('--config-file', type=str,
                        help="An alternative path to the config file")
    parser.add_argument('--hes-events', type=str,
                        help="The HES event date file")
    parser.add_argument('--hes-diag', type=str,
                        help="The HES diagnonsis file")
    parser.add_argument('--hes-oper', type=str,
                        help="The HES operations file")
    parser.add_argument('--gp-clinic', type=str,
                        help="The GP clinical file")
    parser.add_argument('--gp-registrations', type=str,
                        help="The GP registrations file")
    parser.add_argument('--ukbb-wide-file', type=str,
                        help="The 'wide' UKBB phenotype file")
    parser.add_argument('-v', '--verbose',  action="store_true",
                        help="give more output")

    args = parser.parse_args()

    for i in ['tmp_dir']:
        try:
            setattr(args, i, os.path.expanduser(getattr(args, i)))
        except TypeError:
            # It will probably be NoneType (the system tmp location
            pass

    return args


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def initialise(config_file=None, in_memory=False, tmpdir=None):
    """
    Initialise the config file and the UKBB mapping database connection

    Parameters
    ----------
    config_file : str or NoneType, optional, default: NoneType
        The path to the config file. If `NoneType` then the `UKBB_CONFIG`
        environment variable is checked for the config file path. If that is
        not set then finally the default location of `~/.ukbb.cnf` is checked
        if the file is not there then we error out

    Returns
    -------
    config_obj : :obj: `configparser`
        A configparser object that provides access to the confg file
    umls_map_sm : :obj:`sqlalchemy.Sessionmaker`
        Use to generate sessions to query/write to the mapping database
    ukbb_map_sm : :obj:`sqlalchemy.Sessionmaker`
        Use to generate sessions to query/write to the mapping database

    Raises
    ------
    FileNotFoundError
        If either the UMLS or the UKBB mapping database do not exist
    """
    # attempt to loacate the path to the config file, if successful then make
    # sure the permissions are set correcly (only user readable). Finally, if
    # all ok then read in the config file
    config_file = config.get_config_file(config_file=config_file)
    config.check_config_permissions(config_file)
    config_obj = config.read_config_file(config_file)


    return config_obj


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def process_files(outdir, config_obj, wide_file=None, hes_events_file=None,
                  hes_diagnosis_file=None, hes_operations_file=None,
                  gp_clinical_file=None, gp_registrations_file=None,
                  verbose=False, tmp_dir=None):
    """
    Initiate the processing of all the UKBB phenotype files

    Parameters
    ----------
    outdir : str
        The output directory
    config_obj : :obj:`configparser.Config`
        The config file object to provide assess to options
    umls_session_maker : :obj:`sqlalchemy.Sessionmaker`
        The sessionmaker for the UMLS database
    ukbb_mapper_session_maker : :obj:`sqlalchemy.Sessionmaker`
        The sessionmaker for the UKBB mapping database
    temp_db_session_maker : :obj:`sqlalchemy.Sessionmaker`
        The sessionmaker for the temp database that will be used to hold
        intermediate data
    wide_file : str, optional, default: NoneType
        The path to the wide UKBB wide fields file
    sample_file : str, optional, default: NoneType
        The path to a sample file that is associated with the BGEN files. This
        provides the "total" samples
    hes_events_file : str, optional, default: NoneType
        The path to the hes events file, this has the dates of operation events
    hes_diagnosis_file : str, optional, default: NoneType
        The path to the hes diagnonsis file
    hes_operations_file : str, optional, default: NoneType
        The path to the hes operations file, this file has proceedures and
        hospital stay length in it
    gp_clinical_file : str, optional, default: NoneType
        The GP clinical diagnosis and measures file
    gp_registrations_file : str, optional, default: NoneType
        The GP registration date file
    verbose, optional, default: False
        Output the progress at all stages
    tmp_root : str or NoneType, optional, default: NoneType
        The location of `tmp` if `NoneType` then system `tmp` is used
    """
    # Get all the output files and if they need parsing
    # TODO: In development everything needs building
    outfiles = get_output_file_names(outdir)

    # Check that all input files exist and give a sane error message if
    # not
    infiles = config.get_input_files(
        config_obj,
        wide_file=wide_file,
        hes_events_file=hes_events_file,
        hes_diagnosis_file=hes_diagnosis_file,
        hes_operations_file=hes_operations_file,
        gp_clinical_file=gp_clinical_file,
        gp_registrations_file=gp_registrations_file
    )

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_output_file_names(outdir):
    """
    Determine all the output file names and if they are current or not. So the
    idea, here is that if the process gets truncated then we will continue from
    the previous position and not start from the begining

    Parameters
    ----------
    outdir : str
        The output dirctory for the file

    Returns
    -------
    outfiles : dict
        The keys of the dictionary are the file type and values are tuples
        containing the full path at [0] and if the file needs building at [1]
    """
    outfiles = {}
    return outfiles


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == '__main__':
    main()
