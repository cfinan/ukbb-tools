"""
.. include:: ../docs/unit_parser.md
"""
from ukbb_tools import constants as con
import re
import pprint as pp


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def collapse_clin_codes_to_cui(clinical_codes):
    """
    Collapse the mapping and data type counts of the clinical codes to be
    represented by a single cui. This will allow the assessment of data types
    by cui and will hopefully give a better chance of accurately partitioning
    the events into diagnosis/procedures and clinical_measures

    Parameters
    ----------
    clinical_codes : dict of dict
        The existing clinical code mappings. The keys are tuples of
        (ontology, clinical_code) and the values are a dict that contains
        UMLS mappings and the counts for the different data types and ratio
        of data types observed amongst all those clinical code entries
    """
    cui_counts = {}
    for code, mappings in clinical_codes.items():
        # First make sure that the ontology/code is stored in the mapping
        mappings['clinical_code'] = code

        try:
            for cui, sab, desc, sty in mappings['cuis']:
                try:
                    existing_counts = cui_counts[cui]
                    existing_counts['mapped_to'].append(mappings)
                    existing_counts['code_types'].add(code[0])

                    # Update the ratios
                    for ratio in mappings['ratios'].keys():
                        existing_counts['ratios'][ratio] += \
                            mappings['ratios'][ratio]

                    # Update the other counts
                    for key in ['n_is_empty', 'n_is_float', 'n_is_int',
                                'n_is_string', 'n_occurs']:
                        existing_counts[key] += mappings[key]

                    # Max and min counts
                    existing_counts['max_n_values'] = max(
                        existing_counts['max_n_values'],
                        mappings['max_n_values']
                    )
                    existing_counts['min_n_values'] = min(
                        existing_counts['min_n_values'],
                        mappings['min_n_values']
                    )
                except KeyError:
                    existing_counts = dict(
                        [(i, mappings[i]) for i in
                         ['n_is_empty', 'n_is_float', 'n_is_int',
                          'n_is_string', 'n_occurs', 'max_n_values',
                          'min_n_values']]
                    )

                    # existing_counts = mappings.copy()
                    existing_counts['ratios'] = \
                        existing_counts['ratios'] = mappings['ratios'].copy()
                    existing_counts['mapped_to'] = [mappings]
                    existing_counts['code_types'] = set([code[0]])
                    cui_counts[cui] = existing_counts
        except TypeError:
            # No UMLS concept mappings so we ignore
            pass
    return cui_counts


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def classify_clinical_codes(clinical_codes):
    """
    classify clinical codes as `diagnosis` or `clinical_measures`

    Parameters
    ----------
    clinical_codes : dict of dict
        The existing clinical code mappings. The keys are tuples of
        (ontology, clinical_code) and the values are a dict that contains
        UMLS mappings and the counts for the different data types and ratio
        of data types observed amongst all those clinical code entries

    Returns
    -------
    clinical_codes : dict of dict
        The clinical code mappings with an additional key added: `is_measure`.
        This will be `True` if the code is a clinical measure and `False` if
        it is a diagnosis or procedure.
    """
    # codes = [
    #     con.ICD_NINE_CODE_TYPE_NAME,
    #     con.ICD_TEN_CODE_TYPE_NAME,
    #     con.OPCS_FOUR_CODE_TYPE_NAME,
    #     con.OPCS_THREE_CODE_TYPE_NAME
    # ]
    codes = [
        con.READ_THREE_CODE_TYPE_NAME,
        con.READ_TWO_CODE_TYPE_NAME
    ]

    cui_codes = collapse_clin_codes_to_cui(clinical_codes)

    for cui, code_counts in cui_codes.items():
        # This is a very simple classification system, will probably
        # become more complex and I will add code exceptions in there
        if len(code_counts['code_types'].intersection(codes)) == 0:
            # if cui == 'C0001948':
                # print("In one")
                # pp.pprint(code_counts)

            code_counts['is_measure'] = False
        elif code_counts['n_occurs'] == code_counts['n_is_empty']:
            # if cui == 'C0001948':
            #     print("In two")
            #     pp.pprint(code_counts)

            code_counts['is_measure'] = False
        elif (code_counts['n_is_int'] + code_counts['n_is_float']) > 0:
            # if cui == 'C0001948':
            #     print("In three")
            #     pp.pprint(code_counts)

            code_counts['is_measure'] = True
        else:
            # if cui == 'C0001948':
            #     print("In four")
            #     pp.pprint(code_counts)
            code_counts['is_measure'] = False

    # Now have a look at the codes that are not mapped to UKBB
    for code, code_counts in clinical_codes.items():
        # A very similar classification system for the non mapped codes
        if len(code_counts['cuis']) == 0:
            if code_counts['n_occurs'] == code_counts['n_is_empty']:
                code_counts['is_measure'] = False
            elif (code_counts['n_is_int'] + code_counts['n_is_float']) > 0:
                code_counts['is_measure'] = True
            else:
                code_counts['is_measure'] = False
    return cui_codes


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def partition_code_values(values):
    """
    When given values associated with a clinical code, this places them into
    categories: numeric, units, string, notes . The difference between string
    notes is that notes are long string and may represent medical notes in the
    clinical record. string, is an unspecified short string

    Parameters
    ----------
    values : list of str
        Values to partition
    """
    numeric = []
    units = []
    string = []
    notes = []

    for i in values:
        clean_val = re.sub(r'\s+', '', i)
        try:
            int(clean_val)
            numeric.append(clean_val)
            continue
        except ValueError:
            pass

        try:
            float(clean_val)
            numeric.append(clean_val)
            continue
        except ValueError:
            pass

        # Now test for units
        if is_units(clean_val) is True:
            units.append(clean_val)
        elif is_clinical_note(i):
            notes.append(i)
        else:
            string.append(i)

    return numeric, units, string, notes


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def is_units(value):
    """
    Test if the value looks like a unit definition
    """
    if re.search(r'[/^]', value):
        return True

    if value == '%':
        return True

    return False


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def is_clinical_note(value):
    """
    Test if the value looks like a unit definition
    """
    if len(value) > 10 and re.search(r'\s+', value) and \
       re.search(r'[A-Za-z]', value):
        return True

    return False
