"""
.. include:: ../docs/type_casts.md
"""
from ukbb_tools import constants as con
from collections import namedtuple
from datetime import datetime


# This will hold the parsed dates out of the file in the various formats
# that we want them
DateHolder = namedtuple('DateHolder', ['date_obj', 'pretty_date', 'int_date'])


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_ukbb_read_mapping_type(d):
    """
    Alter some of the mapping type codes so they do not clash with others that
    are already in the mapping table
    """
    if d == 'U':
        return 'X'
    elif d == 'A':
        return 'L'
    return d


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_read2_tui(d):
    """
    """
    # print("in here1")
    if d == '0':
        # print("in here2")
        return '00'
    return d


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_coding_system(d):
    """
    Parse the clinical coding system string. This ensures that it is lower case
    and that it is one of the allowed clinical coding systems

    Parameters
    ----------
    d : str
        The data point to test

    Returns
    -------
    d : str
        The case lowered valid clinical coding system
    """
    d = d.lower()
    if d not in con.ALL_CODING_SYSTEMS:
        raise ValueError("unknown coding system: {0}".format(d))
    return d


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_read2ctv3_map_type(d):
    """
    """
    convert = {'N': 'E', 'O': 'E', 'R': 'E', 'S': 'U', 'A': 'U'}
    try:
        return convert[d[1]]
    except (KeyError, IndexError):
        return 'N'


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def process_date(d):
    """
    Process the date object into the formats that we want it.

    Parameters
    ----------
    d : :obj: `datetime.DateTime()`
        The date object

    Returns
    -------
    date_formats : :obj:`DateHolder`
        A `DateHolder` named tuple that has the date object, the pretty date
        that is standarsed and the integer date which is expressed as number
        of days since the 1,1,1900
    """
    date_pretty = d.strftime(con.GLOBAL_DATE_FORMAT)
    date_int = (d - con.EPOCH_DATE).days
    return DateHolder(date_obj=d, pretty_date=date_pretty, int_date=date_int)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def extract_date(date_string,
                 formats=["%Y-%m-%d", "%Y/%m/%d", "%Y-%m-%dT%H:%M:%S"]):
    """
    process a date string into a date object with respect to the required
    date formats in the file

    Parameters
    ----------
    date_string : str
        The string representation of the date
    formats : list of str
        The potential formats of the date, each format is tried in turn
        to see if we can parse the date

    Returns
    -------
    date_formats : :obj:`DateHolder`
        A `DateHolder` named tuple that has the date object, the pretty date
        that is standarsed and the integer date which is expressed as number
        of days since the 1,1,1900

    Raises
    ------
    ValueError
        If the date can't be processed
    TypeError
        If the date can't be processed
    """
    error = None
    for i in formats:
        try:
            return process_date(datetime.strptime(date_string, i))
        except ValueError as e:
            error = e
        except TypeError as e:
            error = e
            break

    raise error.__class__(
        "unknown date type: '{0}'".format(date_string)) from error


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_standard_date(date_string):
    """
    A date parsing function that is primarily used by the file parsers. This
    one extracts date strings in the format: `"%d/%m/%Y"`

    Parameters
    ----------
    date_string : str
        The date string to be parsed

    Returns
    -------
    processed_date : :obj:`DateHolder`
        A DateHolder  named tuple of the processed date. This has the date
        represented in 3 ways, as an int, as a standardised string and as a
        date object
    """
    return datetime.strptime(date_string, "%d/%m/%Y")
    # return extract_date(date_string, formats=["%d/%m/%Y"])


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_yymm(date_string):
    """
    A date parsing function that is primarily used by the file parsers. This
    one extracts date strings in the format: `"%d/%m/%Y"`

    Parameters
    ----------
    date_string : str
        The date string to be parsed

    Returns
    -------
    processed_date : :obj:`DateHolder`
        A DateHolder  named tuple of the processed date. This has the date
        represented in 3 ways, as an int, as a standardised string and as a
        date object
    """
    return datetime.strptime(date_string, "%y%m")


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_condensed_date(date_string):
    """
    A date parsing function that is primarily used by the file parsers. This
    one extracts date strings in the format: `%Y%m%d`

    Parameters
    ----------
    date_string : str
        The date string to be parsed

    Returns
    -------
    processed_date : :obj:`DateHolder`
        A DateHolder  named tuple of the processed date. This has the date
        represented in 3 ways, as an int, as a standardised string and as a
        date object
    """
    return datetime.strptime(date_string, '%Y%m%d')

    # return extract_date(
    #     date_string,
    #     formats=['%Y%m%d']
    # )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_str_bool(value):
    """
    Parse a string into a boolean value

    Parameters
    ----------
    value : str
        The value to parse into a bool

    Returns
    -------
    parsed_value : bool
        The parsed value
    """
    value = value.lower()
    if value == 'true':
        return True
    elif value == 'false':
        return False
    else:
        return None


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_bool(value):
    """
    Parse a string into a boolean value

    Parameters
    ----------
    value : str
        The value to parse into a bool

    Returns
    -------
    parsed_value : bool
        The parsed value
    """
    return bool(int(value))


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_global_date(date_string):
    """
    A date parsing function that is primarily used by the file parsers. This
    one extracts date strings in the format: `%Y-%m-%d`

    Parameters
    ----------
    date_string : str
        The date string to be parsed

    Returns
    -------
    processed_date : :obj:`DateHolder`
        A DateHolder  named tuple of the processed date. This has the date
        represented in 3 ways, as an int, as a standardised string and as a
        date object
    """
    return datetime.strptime(date_string, con.GLOBAL_DATE_FORMAT)
    # return extract_date(
    #     date_string,
    #     formats=[con.GLOBAL_DATE_FORMAT]
    # )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_split_str(delim_str):
    """
    Take a string and split it into a list based on the internal field
    delimiter
    """
    try:
        return [i.strip() for i in delim_str.split(con.INTERNAL_FIELD_DELIMITER)]
    except AttributeError:
        return []


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_split_float(delim_str):
    """
    Take a string and split it into a list or floats based on the internal
    field delimiter
    """
    try:
        return [float(i.strip())
                for i in delim_str.split(con.INTERNAL_FIELD_DELIMITER)]
    except AttributeError:
        return []
