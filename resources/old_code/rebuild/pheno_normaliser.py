"""
.. include:: ../docs/pheno_normaliser.md
"""
from ukbb_tools import config, constants as ukbbc, file_handler, \
    __version__, __name__ as pkg_name, umls_mapper, \
    temp_db_orm as torm, wide_file_handler as wfh, data_dict as dd, \
    wide_file_extract as wfe, wide_file_common as wfc, \
    clinical_code_parser, event_parser, unit_parser, common, \
    db_build
from sqlalchemy import and_
from pyaddons import file_helper
from simple_progress import progress
from operator import itemgetter
from collections import namedtuple
import pprint as pp
import argparse
import csv
import tempfile
import shutil
import sys
import os
import datetime
import gzip
import warnings

DEFAULT_COMMIT_LINES = 50000

# The prefix of the temp directory that is created during parsing
TEMP_DIR_PREFIX = '.ukbb'

# Move a load of these to constants
# The prefix for verbose messages and the location to write the messages
MSG_PREFIX = '[info]'
MSG_OUT = sys.stderr

CODE_TABLE_HEADER = ['code_id',
                     'ukbb_sab',
                     'ukbb_code',
                     'umls_cui',
                     'umls_sab',
                     'umls_term',
                     'umls_sty',
                     'ukbb_terms',
                     'min_n_values',
                     'max_n_values',
                     'n_occurs',
                     'n_is_empty',
                     'n_is_string',
                     'n_is_int',
                     'n_is_float']

SEARCH_LOG_HEADER = ['code_id',
                     'subsearch_idx',
                     'search_code',
                     'search_term',
                     'search_location',
                     'search_type',
                     'nresults',
                     'flags']

Event = namedtuple(
    'Event',
    ['eid', 'event_start', 'event_end', 'event_duration', 'event_type',
     'event_type_id', 'age_at_event', 'days_from_baseline', 'days_pre_event',
     'days_post_event', 'event_provider', 'event_instance']
)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main():
    """
    The main entry point for the script
    """
    # Initialise and parse the command line arguments
    args = init_cmd_args()

    # Start a msg output, this will respect the verbosity and will output to
    # STDERR
    m = progress.Msg(prefix='', verbose=args.verbose, file=MSG_OUT)
    m.msg("= UKBB pheno_normaliser ({0} v{1}) =".format(pkg_name, __version__))
    m.prefix = MSG_PREFIX
    m.msg_atts(args)

    # setup access to the config file
    config_obj, umls_map_sm, ukbb_map_sm, temp_sm, temp_db_name = initialise(
        config_file=args.config_file, in_memory=args.high_mem,
        tmpdir=args.tmp_dir)

    # Process the UKBB phenotype files. The arguments here are:
    # 1. The output directory
    # 2. The config file object
    # 3. The sessionmaker for the UMLS database
    # 4. The sessionmaker for the UKBB mapper database
    # 5. The sessionmaker for the temp database
    # 6. The wide UKBB fields file
    # 7. The path to the HES event date table
    # 8. The path to the HES diagnosis table
    # 9. The path to the HES operations table
    # 10. The path to the GP data table
    # 11. verbose
    # 12. The temporary directory location
    try:
        process_files(
            args.outdir,
            config_obj,
            umls_map_sm,
            ukbb_map_sm,
            temp_sm,
            args.ukbb_wide_file,
            args.hes_events,
            args.hes_diag,
            args.hes_oper,
            args.gp_clinic,
            args.gp_registrations,
            verbose=args.verbose,
            tmp_root=args.tmp_dir
        )
    finally:
        umls_map_sm.close_all()
        ukbb_map_sm.close_all()
        temp_sm.close_all()
        # TODO: Comment back in for production
        # shutil.move(temp_db_name, '/scratch/ukbb/processed_data/ukbb_db.db')


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def init_cmd_args():
    """
    Initialise the command line arguments and return the parsed arguments

    Returns
    -------
    args : :obj:`argparse.Namespace`
        The argparse namespace object
    """
    parser = argparse.ArgumentParser(description="process UKBB phenotypes")

    # The wide format UKBB data fields filex
    parser.add_argument('outdir', type=str,
                        help="The output directory to write the finished "
                        "files")
    parser.add_argument('--tmp-dir', type=str,
                        help="A temp directory location "
                        "(default: system temp")
    parser.add_argument('--high-mem', action='store_true',
                        help="Use in memory storage as opposed to tmp")
    parser.add_argument('--config-file', type=str,
                        help="An alternative path to the config file")
    parser.add_argument('--hes-events', type=str,
                        help="The HES event date file")
    parser.add_argument('--hes-diag', type=str,
                        help="The HES diagnonsis file")
    parser.add_argument('--hes-oper', type=str,
                        help="The HES operations file")
    parser.add_argument('--gp-clinic', type=str,
                        help="The GP clinical file")
    parser.add_argument('--gp-registrations', type=str,
                        help="The GP registrations file")
    parser.add_argument('--ukbb-wide-file', type=str,
                        help="The 'wide' UKBB phenotype file")
    parser.add_argument('-v', '--verbose',  action="store_true",
                        help="give more output")

    args = parser.parse_args()

    for i in ['tmp_dir']:
        try:
            setattr(args, i, os.path.expanduser(getattr(args, i)))
        except TypeError:
            # It will probably be NoneType (the system tmp location
            pass

    return args


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def initialise(config_file=None, in_memory=False, tmpdir=None):
    """
    Initialise the config file and the UKBB mapping database connection

    Parameters
    ----------
    config_file : str or NoneType, optional, default: NoneType
        The path to the config file. If `NoneType` then the `UKBB_CONFIG`
        environment variable is checked for the config file path. If that is
        not set then finally the default location of `~/.ukbb.cnf` is checked
        if the file is not there then we error out

    Returns
    -------
    config_obj : :obj: `configparser`
        A configparser object that provides access to the confg file
    umls_map_sm : :obj:`sqlalchemy.Sessionmaker`
        Use to generate sessions to query/write to the mapping database
    ukbb_map_sm : :obj:`sqlalchemy.Sessionmaker`
        Use to generate sessions to query/write to the mapping database

    Raises
    ------
    FileNotFoundError
        If either the UMLS or the UKBB mapping database do not exist
    """
    # attempt to loacate the path to the config file, if successful then make
    # sure the permissions are set correcly (only user readable). Finally, if
    # all ok then read in the config file
    config_file = config.get_config_file(config_file=config_file)
    config.check_config_permissions(config_file)
    config_obj = config.read_config_file(config_file)

    # Get the ULMS database connector that will be used to map UKBB clinical
    # codes into the UMLS
    umls_map_sm = config.get_umls_connection(config_obj)

    # Get the connection to the UKBB mapping database that we want to build
    # eventually I want this to error out if it exists - as we should be
    # building from scratch
    ukbb_map_sm = config.get_ukbb_mapping_connection(config_obj,
                                                     must_exist=True)

    temp_db_sm, temp_db_name = config.get_temp_database(
        in_memory=in_memory,
        tmpdir=tmpdir
    )

    return config_obj, umls_map_sm, ukbb_map_sm, temp_db_sm, temp_db_name


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def process_files(outdir, config_obj, umls_session_maker,
                  ukbb_mapper_session_maker, temp_db_session_maker,
                  wide_file=None, hes_events_file=None,
                  hes_diagnosis_file=None, hes_operations_file=None,
                  gp_clinical_file=None, gp_registrations_file=None,
                  verbose=False, tmp_root=None):
    """
    Initiate the processing of all the UKBB phenotype files

    Parameters
    ----------
    outdir : str
        The output directory
    config_obj : :obj:`configparser.Config`
        The config file object to provide assess to options
    umls_session_maker : :obj:`sqlalchemy.Sessionmaker`
        The sessionmaker for the UMLS database
    ukbb_mapper_session_maker : :obj:`sqlalchemy.Sessionmaker`
        The sessionmaker for the UKBB mapping database
    temp_db_session_maker : :obj:`sqlalchemy.Sessionmaker`
        The sessionmaker for the temp database that will be used to hold
        intermediate data
    wide_file : str, optional, default: NoneType
        The path to the wide UKBB wide fields file
    sample_file : str, optional, default: NoneType
        The path to a sample file that is associated with the BGEN files. This
        provides the "total" samples
    hes_events_file : str, optional, default: NoneType
        The path to the hes events file, this has the dates of operation events
    hes_diagnosis_file : str, optional, default: NoneType
        The path to the hes diagnonsis file
    hes_operations_file : str, optional, default: NoneType
        The path to the hes operations file, this file has proceedures and
        hospital stay length in it
    gp_clinical_file : str, optional, default: NoneType
        The GP clinical diagnosis and measures file
    gp_registrations_file : str, optional, default: NoneType
        The GP registration date file
    verbose, optional, default: False
        Output the progress at all stages
    tmp_root : str or NoneType, optional, default: NoneType
        The location of `tmp` if `NoneType` then system `tmp` is used
    """
    # A temp directory will be used to output all intermediate files
    tmp_dir = tempfile.mkdtemp(prefix=TEMP_DIR_PREFIX, dir=tmp_root)

    # Get all the output files and if they need parsing
    # TODO: In development everything needs building
    outfiles = get_output_file_names(outdir)

    # Check that all input files exist and give a sane error message if
    # not
    infiles = config.get_input_files(
        config_obj,
        wide_file=wide_file,
        hes_events_file=hes_events_file,
        hes_diagnosis_file=hes_diagnosis_file,
        hes_operations_file=hes_operations_file,
        gp_clinical_file=gp_clinical_file,
        gp_registrations_file=gp_registrations_file
    )

    # pp.pprint(outfiles)
    # pp.pprint(infiles)

    # TODO: comment out for production <<
    samples_test_file = ('/scratch/ukbb/processed_data/'
                         'samples_20200720_0.1.0a1.txt.gz')
    sample_set = file_handler.read_samples_file(
        samples_test_file,
        verbose=verbose
    )
    codes_test_file = ('/scratch/ukbb/processed_data/'
                       'clinical_codes_20200804_0.1.0a1.txt.gz')
    clin_code_map = file_handler.read_clinical_codes_file(
        codes_test_file,
        verbose=verbose
    )
    # >>
    # pp.pprint(clin_code_map)
    try:
        # # Get the sample vital stats
        # # TODO: comment back in for production <<
        # sample_set = build_sample_file(
        #     outfiles[ukbbc.SAMPLE_FILE_NAME][0],
        #     tmp_dir,
        #     wide_file,
        #     ukbb_mapper_session_maker,
        #     verbose=verbose
        # )
        # builder = db_build.UkbbDbBuilder(
        #     temp_db_session_maker,
        #     list(sample_set.values())
        # )
        # # Now output the whole UKBB wide file lot in long format
        # # build_long_tables(
        #     wide_file,
        #     tmp_dir,
        #     outfiles[ukbbc.LONG_FILE_NAME_NUMERIC][0],
        #     outfiles[ukbbc.LONG_FILE_NAME_ENUM][0],
        #     outfiles[ukbbc.LONG_FILE_NAME_STRING][0],
        #     outfiles[ukbbc.LONG_FILE_NAME_DATES][0],
        #     verbose=verbose
        # )
        # # >>

        # # TODO: comment back in for production <<
        # # Extract the clinical codes and map back to the UMLS. Note that
        # # currently all the clinical codes are cached in memory and a copy
        # # is written to disk as well
        # with clinical_code_parser.ClinicalCodeParser(
        #         infiles['hes_diagnosis_file'],
        #         infiles['hes_operations_file'],
        #         infiles['gp_clinical_file'],
        #         umls_session_maker, ukbb_mapper_session_maker,
        #         verbose=verbose) as code_parser:
        #     code_parser.parse()

        #     # Output the mapped codes and the search log to file
        #     output_codes(
        #         outfiles[ukbbc.CODE_FILE_NAME][0],
        #         outfiles[ukbbc.CODE_MAPPING_LOG_FILE_NAME][0],
        #         code_parser.code_stats
        #     )

        #     output_cuis(
        #         outfiles[ukbbc.CUI_FILE_NAME][0],
        #         code_parser.cui_counts
        #     )
        # # >>

        parse_events(
            sample_set,
            clin_code_map,
            infiles['hes_events_file'],
            infiles['hes_diagnosis_file'],
            infiles['hes_operations_file'],
            infiles['gp_clinical_file'],
            infiles['gp_registrations_file'],
            outfiles[ukbbc.CLIN_DATA_FILE_NAME][0],
            outfiles[ukbbc.GP_REGISTER_FILE_NAME][0],
            verbose=verbose,
            tmp_root=tmp_root
        )
    finally:
        # Make sure we remove the temp directory, this happens irrespective
        # of sussess or failure
        shutil.rmtree(tmp_dir)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_output_file_names(outdir):
    """
    Determine all the output file names and if they are current or not. So the
    idea, here is that if the process gets truncated then we will continue from
    the previous position and not start from the begining

    Parameters
    ----------
    outdir : str
        The output dirctory for the file

    Returns
    -------
    outfiles : dict
        The keys of the dictionary are the file type and values are tuples
        containing the full path at [0] and if the file needs building at [1]
    """
    outfiles = {}
    # The current date will be used to make the files, this may change in
    # future
    cur_date = datetime.datetime.now()

    for prefix in [ukbbc.SAMPLE_FILE_NAME,
                   ukbbc.LONG_FILE_NAME_NUMERIC,
                   ukbbc.LONG_FILE_NAME_STRING,
                   ukbbc.LONG_FILE_NAME_ENUM,
                   ukbbc.LONG_FILE_NAME_DATES,
                   ukbbc.CODE_FILE_NAME,
                   ukbbc.CODE_MAPPING_LOG_FILE_NAME,
                   ukbbc.CUI_FILE_NAME,
                   ukbbc.GP_REGISTER_FILE_NAME,
                   ukbbc.CLIN_DATA_FILE_NAME]:
        # (out_file_name, needs_building?)
        # Create the file name, during development I will hard set the
        # needs building flag to True
        # TODO: Test if file exists for production and adjust needs building
        #  accordingly
        needs_building = True
        outfiles[prefix] = (create_file_name(
            outdir,
            prefix,
            cur_date,
            __version__
        ), needs_building)
    return outfiles


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def create_file_name(outdir, prefix, cur_date, package_version):
    """
    A helper function to create file names

    Parameters
    ----------
    outdir : str
        The output dirctory for the file
    prefix : str
        Essentially this is the file type
    cur_date : :obj: `datetime.datetime`
        The current date
    pacakge_version : str
        The version number of the package used to create the file

    Returns
    -------
    file_name : str
        The formatted file name, currently this is:
        `/dir/prefix_YYYYMMDD_version.txt.gz`. where version is the ukbb_tools
        package version number
    """
    cur_date_str = cur_date.strftime(ukbbc.OUFILE_DATE_FORMAT)

    return os.path.join(
        os.path.expanduser(outdir),
        '{0}_{1}_{2}.txt.gz'.format(prefix, cur_date_str, package_version)
        )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def build_sample_file(out_sample_file, tmp_dir, wide_file,
                      ukbb_mapper_session_maker,
                      verbose=False):
    """
    Construct a fie with the sample information in it. We will define the
    following fields:

    Parameters
    ----------
    out_sample_file : str
        The output sample file name
    tmp_dir : str
        The tmp directory where intermediate files are stored
    wide_file : str
        The wide UKBB data fields file
    ukbb_mapper_session_maker : :obj:`sqlalchemy.Sessionmaker`
        A session maker object to the UKBB mapping database. This is used to
        obtain the data dictionary for the UKBB wide file, to enable proper
        parsing
    verbose : bool, optional, default: False
        Shall I output the progress for building the sample file
    """

    vital_stats = get_sample_vital_stats(
        wide_file,
        ukbb_mapper_session_maker,
        verbose=verbose
    )
    with file_helper.FlexiWriter(
            out_sample_file,
            force_gzip=True,
            dir=tmp_dir,
            delimiter=ukbbc.GLOBAL_OUT_DELIMITER) as outfile:
        header = [i.name for i in ukbbc.SAMPLE_FILE_HEADER]
        outfile.writerow(header)

        for eid in sorted(vital_stats.keys()):
            dob_pretty = vital_stats[eid][ukbbc.SAM_DATE_OF_BIRTH.name].\
                pretty_date
            dob_int = vital_stats[eid][ukbbc.SAM_DATE_OF_BIRTH.name].\
                int_date
            doa_pretty = vital_stats[eid][ukbbc.SAM_DATE_OF_ASSESS.name].\
                pretty_date
            doa_int = vital_stats[eid][ukbbc.SAM_DATE_OF_ASSESS.name].\
                int_date
            sr_sex = vital_stats[eid][ukbbc.SAM_SR_SEX.name].value
            is_caucasian = int(vital_stats[eid]
                               [ukbbc.SAM_IS_CAUCASIAN.name])
            is_genotyped = int(vital_stats[eid]
                               [ukbbc.SAM_IS_GENOTYPED.name])
            has_withdrawn = int(vital_stats[eid]
                                [ukbbc.SAM_HAS_WITHDRAWN.name])

            try:
                gen_sex = vital_stats[eid][ukbbc.SAM_GEN_SEX.name].value
            except AttributeError:
                gen_sex = ''

            try:
                dod_pretty = vital_stats[eid][ukbbc.SAM_DATE_OF_DEATH.name].\
                    pretty_date
                dod_int = vital_stats[eid][ukbbc.SAM_DATE_OF_DEATH.name].\
                    int_date
            except AttributeError:
                dod_pretty = ''
                dod_int = ''

            row = [
                vital_stats[eid][ukbbc.EID.name],
                dob_pretty,
                dob_int,
                doa_pretty,
                doa_int,
                vital_stats[eid][ukbbc.SAM_AGE_OF_ASSESS.name] or '',
                sr_sex,
                gen_sex,
                dod_pretty,
                dod_int,
                vital_stats[eid][ukbbc.SAM_AGE_OF_DEATH.name] or '',
                vital_stats[eid][ukbbc.SAM_N_ASSESSMENTS.name] or '',
                is_caucasian,
                is_genotyped,
                has_withdrawn
            ]
            outfile.writerow(row)

    return vital_stats


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_sample_vital_stats(wide_file, verbose=False):
    """
    Extract all the vital statistics into a nested dict

    Parameters
    ----------
    wide_file : str
        The wide UKBB data fields file
    verbose : bool, optional, default: False
        Shall I output the progress for building the sample file

    Returns
    -------
    sample_rows : dict of dict
        The outer dict has sample IDs (eids as keys) and a dict of vital stats
        as values
    """
    vital_stats_fids = [i.value for i in dd.VITAL_STATS_FID
                        if i != dd.VITAL_STATS_FID.EID]
    found_rows = {}
    n = -1
    counter = 1
    with wfh.open(wide_file, format="nested", mode="full") as infile:
        prog = progress.RateProgress(verbose=verbose, file=sys.stderr)
        for row in prog.progress(infile.fetch(fields=vital_stats_fids,
                                              nonetype=False)):
            vital_stats = wfe.sample_vital_stats(row)
            found_rows[row[dd.VITAL_STATS_FID.EID.value]] = vital_stats
            if n == counter:
                break
            counter += 1
    return found_rows


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def build_long_tables(wide_file, tmp_dir, long_numeric_file, long_enum_file,
                      long_str_file, long_date_file, verbose=False):
    """
    Turn the UKBB wide file into 4 long files partitioned on data types,
    numeric (floats and int), enums (category), strings and dates

    Parameters
    ----------
    wide_file : str
        The wide UKBB data fields file
    tmp_dir : str
        The tmp directory where intermediate files are stored
    long_numeric_file : str
        The name of the file where numeric (int and float) data will be stored
    long_enum_file : str
        The name of the file where categorical data will be stored
    long_str_file : str
        The name of the file where string data will be stored
    long_date_file : str
        The name of the file where date data will be stored
    verbose : bool, optional, default: False
        Shall I output the progress for building the sample file
    """
    # Only used for debugging
    n = -1000
    counter = 1

    try:
        # The workflow here is to write everything to temp files first and then
        # copy it to the final location upon successful completion

        # Create temp files for each of the four output files
        temp_long_numeric = gzip.open(wfc.get_tempfile(dir=tmp_dir), 'wt')
        temp_long_enum  = gzip.open(wfc.get_tempfile(dir=tmp_dir), 'wt')
        temp_long_date = gzip.open(wfc.get_tempfile(dir=tmp_dir), 'wt')
        temp_long_string = gzip.open(wfc.get_tempfile(dir=tmp_dir), 'wt')

        # Create csv writers for each of the 4 output files
        writer_long_numeric = csv.writer(
            temp_long_numeric,
            delimiter=ukbbc.GLOBAL_OUT_DELIMITER
        )
        writer_long_enum = csv.writer(
            temp_long_enum,
            delimiter=ukbbc.GLOBAL_OUT_DELIMITER
        )
        writer_long_date = csv.writer(
            temp_long_date,
            delimiter=ukbbc.GLOBAL_OUT_DELIMITER
        )
        writer_long_str = csv.writer(
            temp_long_string,
            delimiter=ukbbc.GLOBAL_OUT_DELIMITER
        )

        # Output from the wide file in full long format. The full format will
        # allow data type detection and the long format will produce all the
        # required columns
        with wfh.open(wide_file, format="long", mode="full") as infile:
            writer_long_numeric.writerow(infile.__class__.HEADER)
            writer_long_enum.writerow(infile.__class__.HEADER)
            writer_long_date.writerow(infile.__class__.HEADER)
            writer_long_str.writerow(infile.__class__.HEADER)

            prog = progress.RateProgress(verbose=verbose, file=sys.stderr)
            for row in prog.progress(infile.fetch(nonetype=False)):
                try:
                    # Use process of elimination to work out where things will
                    # be output
                    # This will be an Enum
                    row[-1] = row[-1].value
                    writer_long_enum.writerow(row)
                    continue
                except AttributeError:
                    pass

                try:
                    # This will be an date
                    row[-1] = row[-1].pretty_date
                    writer_long_date.writerow(row)
                    continue
                except AttributeError:
                    pass

                try:
                    # This will be a string
                    len(row[-1])
                    writer_long_str.writerow(row)
                    continue
                except TypeError:
                    pass

                # This will be an int or a float
                writer_long_numeric.writerow(row)
                if n == counter:
                    break
                counter += 1
    except Exception:
        # Remove tmp long files and raise
        temp_long_numeric.close()
        temp_long_enum.close()
        temp_long_date.close()
        temp_long_string.close()
        os.unlink(temp_long_numeric.name)
        os.unlink(temp_long_enum.name)
        os.unlink(temp_long_date.name)
        os.unlink(temp_long_string.name)
        raise

    # Successful exit
    # Close before moving
    temp_long_numeric.close()
    temp_long_enum.close()
    temp_long_date.close()
    temp_long_string.close()

    # Move temp long files to the final location
    shutil.move(temp_long_numeric.name, long_numeric_file)
    shutil.move(temp_long_enum.name, long_enum_file)
    shutil.move(temp_long_date.name, long_date_file)
    shutil.move(temp_long_string.name, long_str_file)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def output_codes(code_file, search_log_file, codes):
    """
    Write all the mapped codes to file

    Parameters
    ----------
    outfile : str
        The path to the output file
    codes : dict
        The codes cache, hopefully with some mappings in it. The keys are
        tuples of (code_type, code) and the values are dicts with the number
        of occurances (no_occurs), cui mappings (cuis) and UKBB term strings
        (ukbb).
    """
    # TODO: remove in production
    with gzip.open('{0}.units'.format(code_file), 'wt') as units_out:
        units_out.write('string_value\tn_occurs\n')
        for unit, n_unit in sorted(clinical_code_parser.unit_set.items(),
                                   reverse=True,
                                   key=itemgetter(1)):
            units_out.write('{0}\t{1}\n'.format(unit, n_unit))

    with gzip.open(code_file, 'wt') as code_out:
        code_writer = csv.writer(code_out,
                                 delimiter=ukbbc.GLOBAL_OUT_DELIMITER)
        ratios = [
            (3, 0, 0), (0, 3, 0), (0, 0, 3), (2, 1, 0), (2, 0, 1), (1, 2, 0),
            (0, 2, 1), (1, 0, 2), (0, 1, 2), (1, 1, 1)
        ]
        ratio_header = ['.'.join([str(j) for j in i]) for i in ratios]
        code_writer.writerow(CODE_TABLE_HEADER + ratio_header)

        with gzip.open(search_log_file, 'wt') as sl_out:
            log_writer = csv.writer(sl_out,
                                    delimiter=ukbbc.GLOBAL_OUT_DELIMITER)
            log_writer.writerow(SEARCH_LOG_HEADER)

            code_idx = 1
            for code, mapping in codes.items():
                ukbb_entries = None
                if len(mapping['ukbb']) > 0:
                    ukbb_entries = '|'.join(set(mapping['ukbb']))

                ratio_values = []
                for i in ratios:
                    try:
                        ratio_values.append(mapping['ratios'][i])
                    except KeyError:
                        ratio_values.append(0)

                if len(mapping['cuis']) > 0:
                    for cui, sab, term, stypes in mapping['cuis']:
                        code_writer.writerow([code_idx, code[0], code[1],
                                              cui, sab, term,
                                              stypes, ukbb_entries,
                                              mapping['min_n_values'],
                                              mapping['max_n_values'],
                                              mapping['n_occurs'],
                                              mapping['n_is_empty'],
                                              mapping['n_is_string'],
                                              mapping['n_is_int'],
                                              mapping['n_is_float']] +
                                             ratio_values)
                else:
                    code_writer.writerow([code_idx, code[0], code[1],
                                          None, None, None,
                                          None, ukbb_entries,
                                          mapping['min_n_values'],
                                          mapping['max_n_values'],
                                          mapping['n_occurs'],
                                          mapping['n_is_empty'],
                                          mapping['n_is_string'],
                                          mapping['n_is_int'],
                                          mapping['n_is_float']] +
                                         ratio_values)

                for log_row in mapping['log']:
                    log_writer.writerow([code_idx] + log_row)
                code_idx += 1


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def output_cuis(cui_file, cui_counts):
    """
    Write all the cui counts to file

    Parameters
    ----------
    outfile : str
        The path to the output file
    codes : dict
        The codes cache, hopefully with some mappings in it. The keys are
        tuples of (code_type, code) and the values are dicts with the number
        of occurances (no_occurs), cui mappings (cuis) and UKBB term strings
        (ukbb).
    """
    print(cui_file)
    with gzip.open(cui_file, 'wt') as code_out:
        code_writer = csv.writer(code_out,
                                 delimiter=ukbbc.GLOBAL_OUT_DELIMITER)
        code_writer.writerow(['cui', 'sab', 'desc', 'n_samples', 'n_events'])

        for cui in sorted(cui_counts.keys()):
            code_writer.writerow(
                [
                    cui,
                    cui_counts[cui][2],
                    cui_counts[cui][3],
                    cui_counts[cui][0],
                    cui_counts[cui][1]
                ]
            )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_events(sample_set, clin_code_map, hes_events_file,
                 hes_diagnosis_file, hes_operations_file, gp_clinical_file,
                 gp_registrations_file, out_diagnosis_file, out_register_file,
                 verbose=False, tmp_root=None, age_dp=2):
    """
    Parse the clinical events. The diagnosis/procedures and the gp
    registrations will be processed into tables. The clinical measures will
    end up in a temp table ready for unit processing.
    """
    # We want to represent the codes by CUI and not just assess
    # (ontology, clinical_code). The mapping dicts are references so
    # any updates to them will transmit down, then we will produce a
    # composite mapping and collapse all the (ontology, clinical_code)
    # counts for a CUI
    cui_map = unit_parser.classify_clinical_codes(clin_code_map)

    # Get the output header for the diagnosis and procedures file
    diag_proc_header = [i.name for i in ukbbc.CLIN_DATA_HEADER]

    # TODO: Comment back in for production <<
    # Now parse the events in turn
    with event_parser.EventParser(
            sample_set,
            hes_events_file,
            hes_diagnosis_file,
            hes_operations_file,
            gp_clinical_file,
            gp_registrations_file,
            error_on_missing_eid=False) as events:
        prog = progress.RemainingProgress(len(events.sample_eids))

        # Create temp files
        tmp_diagnosis = common.get_tempfile(dir=tmp_root)
        tmp_gp_register = common.get_tempfile(dir=tmp_root)
        diagnosis_obj = gzip.open(tmp_diagnosis, 'wt')
        gp_register_obj = gzip.open(tmp_gp_register, 'wt')

        # Create temp database table
        try:
            diagnosis_writer = csv.writer(
                diagnosis_obj,
                delimiter=ukbbc.GLOBAL_OUT_DELIMITER,
                lineterminator=os.linesep
            )

            # Write the headers to file
            diagnosis_writer.writerow(diag_proc_header)

            for eid, register in prog.progress(events.parse()):
                # pp.pprint(register)
                # Loop through all the events that the EID has had
                for evt in eid:
                    event_row = _build_event_row(
                        evt, clin_code_map, cui_map, age_dp=age_dp
                    )
                    diagnosis_writer.writerow(
                        common.replace_none_type(
                            [event_row[i] for i in diag_proc_header]
                        )
                    )

        except Exception:
            # # TODO: comment back in for production
            # # close and delete
            # common.clean_files(diagnosis_obj, out_diagnosis_file)
            # common.clean_files(gp_register_obj, out_register_file)
            # TODO: comment out for production
            common.finialise_files(diagnosis_obj, out_diagnosis_file)
            common.finialise_files(gp_register_obj, out_register_file)
            raise

        # close and move to the final location
        common.finialise_files(diagnosis_obj, out_diagnosis_file)
        common.finialise_files(gp_register_obj, out_register_file)
    # >>


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _build_event_row(evt, clin_code_map, cui_map, age_dp=2):
    """
    Return a clinical code lookup key for the event. This is simply a tuple
    of (clinical_code_system, clinical_code)

    Parameters
    ----------
    evt : dict
        The event to classify, must have a `clinical_code` and
        `clinical_code_system` keys

    Returns
    -------
    lookup_key : tuple
        The clinical_code_key and the clinical_code for the event
    """
    try:
        # Convert the pct is GP into an int from a bool
        evt[ukbbc.CLIN_DATA_PCT_IS_GP.name] = int(
            evt[ukbbc.CLIN_DATA_PCT_IS_GP.name]
        )
    except TypeError:
        # NoneType
        pass

    # Round the ages to 2dp, this will not fail if
    # NoneType
    evt[ukbbc.CLIN_DATA_AGE_AT_EVENT.name] = _safe_round(
        evt[ukbbc.CLIN_DATA_AGE_AT_EVENT.name], age_dp
    )

    # Get the clinical code description for the event
    lookup_key = _get_clinical_code_lookup_key(evt)

    try:
        evt[ukbbc.CLIN_DATA_CLINICAL_CODE_DESC.name] = \
            clin_code_map[lookup_key]['ukbb_terms']

        # Get UMLS concepts that map to the clinical code
        evt[ukbbc.CLIN_DATA_UMLS_CUI.name] = \
            [i[0] for i in clin_code_map[lookup_key]['cuis']]
    except KeyError:
        evt[ukbbc.CLIN_DATA_UMLS_CUI.name] = ''
        evt[ukbbc.CLIN_DATA_CLINICAL_CODE_DESC.name] = ''

    # Partition the values in to ints, floats, strings and
    # notes
    numeric, units, string, notes = \
        unit_parser.partition_code_values(
            evt['codes'][1]
        )
    if _is_clinical_measure(evt,
                            cui_map,
                            clin_code_map) is True:
        evt[ukbbc.CLIN_DATA_IS_CLINICAL_MEAS.name] = 1
    else:
        evt[ukbbc.CLIN_DATA_IS_CLINICAL_MEAS.name] = 0

    # Insert a blank norm_numeric_values column, this will be filled in
    # after unit processing
    evt[ukbbc.CLIN_DATA_NORM_NUMERICS.name] = ''

    # Join the CUI mappings with a pipe |
    # (the default internal field delimiter)
    evt[ukbbc.CLIN_DATA_UMLS_CUI.name] = \
        _join_internal_fields(
            evt[ukbbc.CLIN_DATA_UMLS_CUI.name]
        )

    evt[ukbbc.CLIN_DATA_NUMERICS.name] = \
        _join_internal_fields(
            numeric
        )

    evt[ukbbc.CLIN_DATA_UNITS.name] = \
        _join_internal_fields(
            units
        )

    evt[ukbbc.CLIN_DATA_STRINGS.name] = \
        _join_internal_fields(
            string
        )

    evt[ukbbc.CLIN_DATA_CLINICAL_NOTES.name] = \
        _join_internal_fields(
            notes
        )
    return evt


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _is_clinical_measure(evt, cui_map, clin_code_map):
    """
    Determine if an event is a clinical measure or a diagnosis or procedure.
    This uses the cui map and the clinical code maps that have been classified
    by `unit_parser.classify_clinical_codes`

    Parameters
    ----------
    evt : dict
        The event to classify, must have a `clinical_code` and
        `clinical_code_system` keys
    cui_map : dict of dict
        The clinical codes that have been grouped by UMLS cui. The keys are
        UMLS concept names and the values are dicts that have counts for the
        number of times the CUI occurs and a list containing the clinical
        codes that map to the cui
    clin_code_map : dict of dict
        The clinical codes observed in all the events the keys are tuples of
        (clinical coding system, clinical code) and the values are counts and
        UMLS concepts that map to them

    Returns
    -------
    is_clinical_meas : bool
        Will be `True` if the code represents a clinical measure or `False` if
        not
    """
    lookup_key = _get_clinical_code_lookup_key(evt)

    try:
        for cui, sab, desc, sty in clin_code_map[lookup_key]['cuis']:
            return cui_map[cui]['is_measure']
    except TypeError:
        # No cuis (NoneType)
        return clin_code_map[lookup_key]['is_measure']
    except KeyError:
        return False


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _get_clinical_code_lookup_key(evt):
    """
    Return a clinical code lookup key for the event. This is simply a tuple
    of (clinical_code_system, clinical_code)

    Parameters
    ----------
    evt : dict
        The event to classify, must have a `clinical_code` and
        `clinical_code_system` keys

    Returns
    -------
    lookup_key : tuple
        The clinical_code_key and the clinical_code for the event
    """
    return (evt[ukbbc.CLIN_DATA_CLINICAL_CODE_SYSTEM.name],
            evt[ukbbc.CLIN_DATA_CLINICAL_CODE.name])


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _join_internal_fields(field, delimiter=ukbbc.INTERNAL_FIELD_DELIMITER):
    """
    Join a field list on a delimiter, if it is NoneType then return an empty
    string

    Parameters
    ----------
    field : list of str
        The fields to join
    delimiter : optional, default: |

    Returns
    -------
    join_field : str
        The field joined on the delimiter or '' if the field was empty
    """
    if field is not None:
        return delimiter.join([str(i) for i in field])
    else:
        # If it is NoneType then just return an empty string
        return ''


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _safe_round(value, dp=2):
    """
    Perform rounding but do not fail on TypeError and just return None

    Parameters
    ----------
    value : float
        The value to be rounded
    dp : int, optional, default: 2
        The decimal places to round to

    Returns
    -------
    rounded_value : float
        The value rounded to dp
    """
    try:
        return round(value, dp)
    except TypeError:
        return None


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == '__main__':
    main()
