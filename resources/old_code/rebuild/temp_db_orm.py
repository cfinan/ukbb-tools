
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Boolean, Column, Integer, String, Float, Date, Text, ForeignKey, SmallInteger
from sqlalchemy.orm import relationship
from pyaddons import sqlalchemy_helper

Base = declarative_base()


# # @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# class TempEvent(Base):
#     """
#     A representation of the `temp_event` table
#     """
#     __tablename__ = 'temp_event'

#     event_pk = Column(Integer, nullable=False, primary_key=True,
#                       autoincrement=True)
#     event_idx = Column(Integer, nullable=False)
#     event_order = Column(Integer, nullable=False)
#     event_hash = Column(Integer, unique=True, index=True)
#     eid = Column(Integer, index=True)
#     event_start_date = Column(Date)
#     event_start_date_int = Column(Integer)
#     event_end_date = Column(Date)
#     event_end_date_int = Column(Integer)
#     event_duration = Column(Integer)
#     event_type = Column(String(20))
#     event_type_id = Column(Integer)
#     event_provider = Column(Integer)
#     event_instance = Column(Integer)
#     age_at_event = Column(Float)
#     days_from_baseline = Column(Integer)
#     days_pre_event = Column(Integer)
#     days_post_event = Column(Integer)

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     def __repr__(self):
#         return sqlalchemy_helper.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Sample(Base):
    """
    The main samples table this has the vital stats for the samples in it
    """
    __tablename__ = 'sample'

    eid = Column(Integer, nullable=False, primary_key=True)
    date_of_birth = Column(Date, nullable=False)
    date_of_birth_int = Column(Integer, nullable=False)
    date_of_assessment = Column(Date, nullable=False)
    date_of_assessment_int = Column(Integer, nullable=False)
    age_of_assess = Column(Float, nullable=False)
    self_reported_sex = Column(SmallInteger)
    genetic_sex = Column(SmallInteger)
    date_of_death = Column(Date)
    date_of_death_int = Column(Integer)
    age_of_death = Column(Float)
    no_of_assessment = Column(SmallInteger, nullable=False)
    is_caucasian = Column(Boolean)
    is_genotyped = Column(Boolean)
    has_withdrawn = Column(Boolean)

    clinical_data = relationship(
        "ClinicalData",
        back_populates=__tablename__
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return sqlalchemy_helper.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class CareType(Base):
    """
    A representation of the `temp_event` table
    """
    __tablename__ = 'care_type'

    care_type_id = Column(
        Integer,
        nullable=False,
        primary_key=True,
        autoincrement=True
    )
    care_type_desc = Column(String(15))

    clinical_data = relationship(
        "ClinicalData",
        back_populates=__tablename__
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return sqlalchemy_helper.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class RecordType(Base):
    """
    A representation of the `temp_event` table
    """
    __tablename__ = 'record_type'

    record_type_id = Column(Integer, nullable=False, primary_key=True,
                            autoincrement=True)
    record_type_desc = Column(String(15))

    clinical_data = relationship(
        "ClinicalData",
        back_populates=__tablename__
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return sqlalchemy_helper.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class DataProvider(Base):
    """
    A representation of the `temp_event` table
    """
    __tablename__ = 'data_provider'

    data_provider_id = Column(Integer, nullable=False, primary_key=True,
                              autoincrement=True)
    data_provider_desc = Column(String(15))

    clinical_data = relationship(
        "ClinicalData",
        back_populates=__tablename__
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return sqlalchemy_helper.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class DataProviderFormat(Base):
    """
    A representation of the `temp_event` table
    """
    __tablename__ = 'data_provider_format'

    data_provider_format_id = Column(Integer, nullable=False, primary_key=True,
                                     autoincrement=True)
    data_provider_format_desc = Column(String(15))

    clinical_data = relationship(
        "ClinicalData",
        back_populates=__tablename__
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return sqlalchemy_helper.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Pct(Base):
    """
    A representation of the `temp_event` table
    """
    __tablename__ = 'pct'

    pct_id = Column(
        Integer,
        nullable=False,
        primary_key=True,
        autoincrement=True
    )
    nhs_pct_id = Column(String(10))
    pct_desc = Column(String(15))

    clinical_data = relationship(
        "ClinicalData",
        back_populates=__tablename__
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return sqlalchemy_helper.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class ClinicalCode(Base):
    """
    A representation of the `temp_event` table
    """
    __tablename__ = 'clinical_code'

    clinical_code_id = Column(
        Integer,
        nullable=False,
        primary_key=True,
        autoincrement=True
    )
    clinical_code_name = Column(
        String(255),
        index=True
    )
    clinical_code_system = Column(String(255))
    ncuis = Column(Integer)

    # --------
    clinical_data = relationship(
        "ClinicalData",
        back_populates=__tablename__
    )
    clinical_code_desc = relationship(
        "ClinicalCodeDesc",
        back_populates=__tablename__
    )
    clinical_code_to_cui = relationship(
        "ClinicalCodeToCui",
        back_populates=__tablename__
    )
    # -------

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return sqlalchemy_helper.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class ClinicalCodeDesc(Base):
    """
    A representation of the `temp_event` table
    """
    __tablename__ = 'clinical_code_desc'

    clinical_code_desc_id = Column(
        Integer,
        nullable=False,
        primary_key=True,
        autoincrement=True
    )
    clinical_code_id = Column(
        Integer,
        ForeignKey('clinical_code.clinical_code_id')
    )
    clinical_code_desc = Column(
        String(255),
        index=True
    )

    # ------
    clinical_code = relationship(
        "ClinicalCode",
        back_populates=__tablename__
    )
    # ------

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return sqlalchemy_helper.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Cui(Base):
    """
    A representation of the `temp_event` table
    """
    __tablename__ = 'cui'

    cui_id = Column(Integer, nullable=False, primary_key=True,
                    autoincrement=True)
    cui = Column(String(10), nullable=False, index=True)
    cui_desc = Column(String(255), index=True)

    # ------
    cui_to_sty = relationship(
        "CuiToSty",
        back_populates=__tablename__
    )
    clinical_code_to_cui = relationship(
        "ClinicalCodeToCui",
        back_populates=__tablename__
    )
    # ------

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return sqlalchemy_helper.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Sty(Base):
    """
    A representation of the `temp_event` table
    """
    __tablename__ = 'sty'

    sty_id = Column(Integer, nullable=False, primary_key=True,
                    autoincrement=True)
    sty_desc = Column(String(255), nullable=False)

    # ------
    cui_to_sty = relationship(
        "CuiToSty",
        back_populates=__tablename__
    )
    # ------

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return sqlalchemy_helper.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class CuiToSty(Base):
    """
    A representation of the `temp_event` table
    """
    __tablename__ = 'cui_to_sty'

    cui_to_sty_id = Column(
        Integer,
        nullable=False,
        primary_key=True,
        autoincrement=True
    )
    cui_id = Column(
        Integer,
        ForeignKey('cui.cui_id')
    )
    sty_id = Column(
        Integer,
        ForeignKey('sty.sty_id')
    )

    # ------
    cui = relationship(
        "Cui",
        back_populates=__tablename__
    )
    sty = relationship(
        "Sty",
        back_populates=__tablename__
    )
    # ------

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return sqlalchemy_helper.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class ClinicalCodeToCui(Base):
    """
    A representation of the `temp_event` table
    """
    __tablename__ = 'clinical_code_to_cui'

    code_to_cui_pk = Column(Integer, nullable=False, primary_key=True,
                            autoincrement=True)
    clinical_code_id = Column(
        Integer,
        ForeignKey('clinical_code.clinical_code_id')
    )
    cui_id = Column(
        Integer,
        ForeignKey('cui.cui_id')
    )

    # ------
    clinical_code = relationship(
        "ClinicalCode",
        back_populates=__tablename__
    )
    cui = relationship(
        "Cui",
        back_populates=__tablename__
    )
    # ------

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return sqlalchemy_helper.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class ClinicalData(Base):
    """
    A representation of the `temp_event` table
    """
    __tablename__ = 'clinical_data'

    event_idx = Column(Integer, nullable=False, primary_key=True)
    episode_idx = Column(Integer, nullable=False)
    eid = Column(
        Integer,
        ForeignKey('sample.eid')
    )
    event_start_str = Column(Date)
    event_start_int = Column(Integer)
    age_at_event = Column(Float)
    days_from_baseline = Column(Integer)
    days_pre_event = Column(Integer)
    days_post_event = Column(Integer)
    clinical_code_id = Column(
        Integer,
        ForeignKey('clinical_code.clinical_code_id')
    )
    event_level = Column(Integer)
    care_type_id = Column(
        Integer,
        ForeignKey('care_type.care_type_id')
    )
    record_type_id = Column(
        Integer,
        ForeignKey('record_type.record_type_id')
    )
    data_provider_id = Column(
        Integer,
        ForeignKey('data_provider.data_provider_id')
    )
    data_provider_format_id = Column(
        Integer,
        ForeignKey('data_provider_format.data_provider_format_id')
    )
    pct_id = Column(
        Integer,
        ForeignKey('pct.pct_id')
    )
    days_to_pct = Column(Integer)
    pct_is_gp = Column(Integer)
    is_clinical_measure = Column(Integer)
    n_numerics = Column(Integer)
    n_units = Column(Integer)
    n_strings = Column(Integer)
    n_notes = Column(Integer)

    # ~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~
    sample = relationship(
        "Sample",
        back_populates=__tablename__
    )

    numeric = relationship(
        "Numeric",
        back_populates=__tablename__
    )
    unit = relationship(
        "Unit",
        back_populates=__tablename__
    )
    string = relationship(
        "String",
        back_populates=__tablename__
    )
    note = relationship(
        "Note",
        back_populates=__tablename__
    )
    clinical_code = relationship(
        "ClinicalCode",
        back_populates=__tablename__
    )
    care_type = relationship(
        "CareType",
        back_populates=__tablename__
    )
    record_type = relationship(
        "RecordType",
        back_populates=__tablename__
    )
    data_provider = relationship(
        "DataProvider",
        back_populates=__tablename__
    )
    data_provider_format = relationship(
        "DataProviderFormat",
        back_populates=__tablename__
    )
    pct = relationship(
        "Pct",
        back_populates=__tablename__
    )
    # ~~~~~~

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return sqlalchemy_helper.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Numeric(Base):
    """
    The numerics table stores all the numerical clinical data as floats and
    is linked back to the main clinical data table
    """
    __tablename__ = 'numeric'

    numeric_id = Column(Integer, nullable=False, primary_key=True,
                        autoincrement=True)
    event_idx = Column(Integer, ForeignKey('clinical_data.event_idx'))
    raw_value = Column(Float)
    standard_value = Column(Float)
    units = Column(String(30))
    flag = Column(Boolean, nullable=False)

    clinical_data = relationship(
        "ClinicalData",
        back_populates=__tablename__
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return sqlalchemy_helper.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Unit(Base):
    """
    The units table stores any units that may have been stored in the clinical
    data record
    """
    __tablename__ = 'unit'

    units_id = Column(Integer, nullable=False, primary_key=True,
                      autoincrement=True)
    event_idx = Column(Integer, ForeignKey('clinical_data.event_idx'))
    unit_value = Column(String(255))

    clinical_data = relationship(
        "ClinicalData",
        back_populates=__tablename__
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return sqlalchemy_helper.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class String(Base):
    """
    This holds miscellaneous string data that is associated with the clinical
    _data record and does not appear to be a clinical note or a unit of
    measurement
    """
    __tablename__ = 'string'

    units_id = Column(Integer, nullable=False, primary_key=True,
                      autoincrement=True)
    event_idx = Column(Integer, ForeignKey('clinical_data.event_idx'))
    string_value = Column(String(255))

    clinical_data = relationship(
        "ClinicalData",
        back_populates=__tablename__
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return sqlalchemy_helper.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Note(Base):
    """
    This holds data that appears to be clinical notes tagged to the clinical
    data record. It links back to the clinical_data table
    """
    __tablename__ = 'note'

    units_id = Column(Integer, nullable=False, primary_key=True,
                      autoincrement=True)
    event_idx = Column(Integer, ForeignKey('clinical_data.event_idx'))
    note = Column(Text)

    clinical_data = relationship(
        "ClinicalData",
        back_populates=__tablename__
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return sqlalchemy_helper.print_orm_obj(self)
