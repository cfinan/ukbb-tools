"""
.. include:: ../docs/event_parser.md
"""
from ukbb_tools import file_handler, common, constants as con
from collections import namedtuple
import warnings
import pprint as pp

# A representation of primary care trust data, used in the comparison of
# Pct dates with event dates to assign the nearest Pct (in time) to the
# event
Pct = namedtuple('Pct', [con.CLIN_DATA_EVENT_START_INT.name,
                         con.CLIN_DATA_PCT.name,
                         con.CLIN_DATA_PCT_IS_GP.name])

# In the event that a single event has multiple codes this ordering
# is used to prioritise codes from a coding system to get a single code
# for an event.
CODE_SYSTEM_ORDER = [
    con.ICD_TEN_CODE_TYPE_NAME,
    con.READ_THREE_CODE_TYPE_NAME,
    con.OPCS_FOUR_CODE_TYPE_NAME,
    con.ICD_NINE_CODE_TYPE_NAME,
    con.READ_TWO_CODE_TYPE_NAME,
    con.OPCS_THREE_CODE_TYPE_NAME
]

# Values for the care_type, these are here and not in constants as they
# are unlikely to be used outside of this module
CARE_TYPE_PRIMARY = 'primary'
CARE_TYPE_SECONDARY = 'secondary'


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class EventParser(object):
    """
    Handle event parsing from the UKBB clinical files. The
    sources that we use are:
    1. HES Events file
    2. HES diagnosis data
    3. HES Operations/Procedures data
    4. Primary Care diagnosis and measures
    """
    # TODO: Move these to the data dictionary
    GP_DATA_SOURCES = {0: 'unknown',
                       1: 'england_vision',
                       2: 'scotland',
                       3: 'england_tpp',
                       4: 'wales'}

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @staticmethod
    def _extract_data_provider(evt):
        """
        Extract the nested tuple data provider into two separate fields in the
        event: `data_provider` and `data_provider_format`

        Parameters
        ----------
        evt : dict
            The event must have the key `data_provider` containing a tuple of
            data provider information with the provider name at 0 and the data
            provider format value at 1
            `data_provider_format`

        Returns
        -------
        evt : dict
            The return is not strictly necessary as the fields are added in
            place.
        """
        evt[con.CLIN_DATA_DATA_PROVDER_FORMAT.name] = \
            evt[con.CLIN_DATA_DATA_PROVDER.name][1]
        evt[con.CLIN_DATA_DATA_PROVDER.name] = \
            evt[con.CLIN_DATA_DATA_PROVDER.name][0]

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @staticmethod
    def _extract_pct_data(episodes):
        """
        Extract a namedtuple of PCT data from each HES episode

        Parameters
        ----------
        episodes : dict
            Episodes to extract the PCT data from

        Returns
        -------
        pct_data : list of :obj:`Pct`
            A named tuple of primary care trust IDs and dates
        """
        return [Pct(i[con.CLIN_DATA_EVENT_START_INT.name],
                    i[con.CLIN_DATA_PCT.name],
                    i[con.CLIN_DATA_PCT_IS_GP.name])
                for i in episodes.values()]

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @staticmethod
    def _merge_diagnosis_to_episode(eid, instance_idx, diagnosis, episode):
        """
        This merges diagnosis events into HES episodes to assign times to the
        diagnosis

        Parameters
        ----------
        eid : int
            The current sample ID, this is saved into the event
        instance_idx : int
            The current instance_idx, this is saved into the event and is used
            later on to generate the episode_idx to uniquely ID the episode
            within a participant
        diagnosis : dict
            The data for a single diagnosis event from a person, must match
            the eid and instance_idx of the episode
        episode : dict
            The data for a single episode event from a person, must match
            the eid and instance_idx of the diagnosis

        Returns
        -------
        diagnosis : dict
            The diagnosis with common fields merged into it. Note that the
            return is not strictly necessary as the modifications are in place
        """
        # Merge the episode fields that are common to diagnosis and operations
        # from the episode into the diagnosis event
        diagnosis = EventParser._merge_common_to_episode(
            eid,
            instance_idx,
            diagnosis,
            episode
        )

        # These are calculations that are specific to diagnosis
        diagnosis['event_start'] = episode['event_start']
        diagnosis[con.CLIN_DATA_EVENT_START_STR.name] = \
            episode[con.CLIN_DATA_EVENT_START_STR.name]
        diagnosis[con.CLIN_DATA_EVENT_START_INT.name] = \
            episode[con.CLIN_DATA_EVENT_START_INT.name]

        try:
            # We set the diagnosis always occur at the start of an episode
            # unless we do not have an end date then we set to None
            diagnosis[con.CLIN_DATA_DAYS_PRE_EVENT.name] = 0
            diagnosis[con.CLIN_DATA_DAYS_POST_EVENT.name] = \
                (episode['event_end_int'] -
                 episode[con.CLIN_DATA_EVENT_START_INT.name])
        except TypeError:
            diagnosis[con.CLIN_DATA_DAYS_PRE_EVENT.name] = None
            diagnosis[con.CLIN_DATA_DAYS_POST_EVENT.name] = None

        return diagnosis

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @staticmethod
    def _merge_operation_to_episode(eid, instance_idx, operation, episode):
        """
        This merges diagnosis events into HES episodes to assign times to the
        diagnosis

        Parameters
        ----------
        eid : int
            The current sample ID, this is saved into the event
        instance_idx : int
            The current instance_idx, this is saved into the event and is used
            later on to generate the episode_idx to uniquely ID the episode
            within a participant
        operation : dict
            The data for a single operation event from a person, must match
            the eid and instance_idx of the episode
        episode : dict
            The data for a single episode event from a person, must match
            the eid and instance_idx of the operation

        Returns
        -------
        operation : dict
            The operation with common fields merged into it. Note that the
            return is not strictly necessary as the modifications are in place
        """
        # Merge the episode fields that are common to diagnosis and operations
        # from the episode into the diagnosis event
        operation = EventParser._merge_common_to_episode(
            eid,
            instance_idx,
            operation,
            episode
        )

        try:
            # These calculations are specific to operations
            operation[con.CLIN_DATA_DAYS_PRE_EVENT.name] = \
                (operation[con.CLIN_DATA_EVENT_START_INT.name] -
                 episode[con.CLIN_DATA_EVENT_START_INT.name])
            operation[con.CLIN_DATA_DAYS_POST_EVENT.name] = \
                (episode['event_end_int'] -
                 operation[con.CLIN_DATA_EVENT_START_INT.name])
        except TypeError:
            # One of the fields is NoneType
            operation[con.CLIN_DATA_DAYS_PRE_EVENT.name] = None
            operation[con.CLIN_DATA_DAYS_POST_EVENT.name] = None

        return operation

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @staticmethod
    def _merge_common_to_episode(eid, instance_idx, evt, episode):
        """
        Merge the episode fields that are common to diagnosis and operations
        from the episode into the event

        Parameters
        ----------
        eid : int
            The current sample ID, this is saved into the event
        instance_idx : int
            The current instance_idx, this is saved into the event and is used
            later on to generate the episode_idx to uniquely ID the episode
            within a participant
        evt : dict
            The data for a single event from a person, must match the eid and
            instance_idx of the episode
        episode : dict
            The data for a single episode event from a person, must match
            the eid and instance_idx of the event

        Returns
        -------
        event : dict
            The event with common fields merged into it. Note that the return
            is not strictly necessary as the modifications are in place
        """
        evt[con.EID.name] = eid
        evt['eid_event_idx'] = instance_idx
        evt[con.CLIN_DATA_CARE_TYPE.name] = \
            episode[con.CLIN_DATA_CARE_TYPE.name]
        evt[con.CLIN_DATA_PCT.name] = \
            episode[con.CLIN_DATA_PCT.name]
        evt[con.CLIN_DATA_PCT_IS_GP.name] = \
            episode[con.CLIN_DATA_PCT_IS_GP.name]
        evt[con.CLIN_DATA_DATA_PROVDER.name] = \
            episode[con.CLIN_DATA_DATA_PROVDER.name]
        evt[con.CLIN_DATA_DAYS_TO_PCT.name] = 0
        return evt

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _extract_date_data(evt, field):
        """
        Attempt to extract a `file_helper.DateHolder` from the evt and return
        it's component parts. One complication is that dates are frequently
        missing in UKBB data so this method will handle the error and ensure
        NoneTypes are returned instead.

        Parameters
        ----------
        evt : dict
            The event that contains the field with may have a
            `file_helper.DateHolder` or NoneType in it

        Returns
        -------
        event_start : :obj:`datetime.Date` or NoneType
            A python date object representing the date of the event or
            `NoneType` if the event has no date
        event_start_str : str or NoneType
            A string representing the date of the event, this has the format:
            YYYY-MM-DD or `NoneType` if the event has no date
        event_start_int : int or NoneType
            An int representing the date of the event, ths is the number of
            days the event took place after 1900-1-1 or `NoneType` if the
            event has no date
        """
        try:
            # Attempt to get the date of the operation. This could
            # potentially be undefined and if it is use NoneType
            holder = evt[field]
            return holder.date_obj, holder.pretty_date, holder.int_date
        except AttributeError:
            return None, None, None

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, sample_vitals, hes_events, hes_diag, hes_oper,
                 gp_clinical, gp_register, error_on_missing_eid=True):
        """
        Parameters
        ----------
        sample_vitals : dict
            A cache of the sample vital statistics with the eid (int) being
            the key and a dict of vital statistics for the sample as values
        hes_events : tuple
            The path to the hes events file should be at [0] and the csv
            parameters and encoding should be in a dict at [1]
        hes_diag : tuple
            The path to the hes diagnosis file should be at [0] and the csv
            parameters and encoding should be in a dict at [1]
        hes_oper : tuple
            The path to the HES operations file should be at [0] and the csv
            parameters and encoding should be in a dict at [1]
        gp_clinical : tuple
            The path to the GP clinical file should be at [0] and the csv
            parameters and encoding should be in a dict at [1]
        gp_register : tuple
            The path to the GP registrations file should be at [0] and the csv
            parameters and encoding should be in a dict at [1]
        error_on_missing_eid : bool, optional, default: True
            raise a KeyError if any of the clinical event EIDs are not present
            in the sample master list (sample_vitals)
        """
        # Initialise them all to None so that if creating any of them
        # should fail then at least they have been defined for the close()
        # method of the context manager
        self.hes_events_reader = None
        self.hes_diag_reader = None
        self.hes_oper_reader = None
        self.gp_clinical_reader = None
        self.gp_register_reader = None
        self.sample_vitals = sample_vitals
        self._error_on_missing_eid = error_on_missing_eid
        # Indicate that we are not open
        self._open = False

        # The event_idx is a unique counter assigned to every event
        self._event_idx = 0

        # The episode Idx is a unique counter assigned to every interaction
        # with the health care system
        self._episode_idx = 0

        # Initialise all the various reader objects that we will be
        # sequentially getting samples from
        self._init_readers(
            hes_events,
            hes_diag,
            hes_oper,
            gp_clinical,
            gp_register
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _init_readers(self, hes_events, hes_diag, hes_oper,
                      gp_clinical, gp_register):
        """
        Initialise all the index file readers so we can sequentially
        extract samples from them

        Parameters
        ----------
        hes_events : tuple
            The path to the hes events file should be at [0] and the csv
            parameters and encoding should be in a dict at [1]
        hes_diag : tuple
            The path to the hes diagnosis file should be at [0] and the csv
            parameters and encoding should be in a dict at [1]
        hes_oper : tuple
            The path to the HES operations file should be at [0] and the csv
            parameters and encoding should be in a dict at [1]
        gp_clinical : tuple
            The path to the GP clinical file should be at [0] and the csv
            parameters and encoding should be in a dict at [1]
        gp_register : tuple
            The path to the GP registrations file should be at [0] and the
            csv parameters and encoding should be in a dict at [1]
        """
        self.hes_events_reader = file_handler.HesEventsFile(
            hes_events[0],
            **hes_events[1]
        )

        self.hes_diag_reader = file_handler.HesDiagnosisFile(
            hes_diag[0],
            **hes_diag[1]
        )

        self.hes_oper_reader = file_handler.HesOperationsFile(
            hes_oper[0],
            **hes_oper[1]
        )

        self.gp_clinical_reader = file_handler.GpClinicalFile(
            gp_clinical[0],
            **gp_clinical[1]
        )

        self.gp_register_reader = file_handler.GpRegistrationsFile(
            gp_register[0],
            **gp_register[1]
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __enter__(self):
        """
        Entry point for the context manager, must return self
        """
        self.open()
        return self

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __exit__(self, *args):
        """
        Exit the context manager

        Parameters
        ----------
        *args
            Arguments passed to the context manager when exiting the scope
            of the context manager
        """
        self.close()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def open(self):
        """
        Open all the index readers and get a union of all the samples that
        exist in all of them
        """
        # Make sure we are not open already
        self._check_is_not_open()

        # Open all the index readers
        self.hes_events_reader.open()
        self.hes_diag_reader.open()
        self.hes_oper_reader.open()
        self.gp_clinical_reader.open()
        self.gp_register_reader.open()

        # Get all of the sample EIDs available in all of the index readers
        self.sample_eids = file_handler.sample_union(
            self.hes_events_reader,
            self.hes_diag_reader,
            self.hes_oper_reader,
            self.gp_clinical_reader
        )
        self.sample_eids.sort()

        # Indicate that we have been opened
        self._open = True

        # Re-initialise the counters
        self._event_idx = 0
        self._episode_idx = 0

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def close(self):
        """
        Close all index files. If using the context manager then this will be
        called automatically. If not, then please call this method before you
        exit
        """
        # Make sure that we are opened
        self._check_is_open()

        for reader in [self.hes_events_reader, self.hes_diag_reader,
                       self.hes_oper_reader, self.gp_clinical_reader,
                       self.gp_register_reader]:
            try:
                reader.close()
            except AttributeError:
                # NoneType means that the reader has not been opened yet. This
                # could happen if the user is not using the context manager
                pass

        # Indicate that we are now closed
        self._open = False

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def parse(self):
        """
        Parse the events from the multiple sources as batches of all the events
        combined for a participant (eid)

        Yields
        ------
        events : list of dict
            The events from all sources, normalised to have the same column
            names (keys) and in chronological order
        gp_registration : list of dict
            Each dict contains the GP registration data with a PCT code tied
            to it and the dates formatted as ints and pretty dates
        """
        counter = 1
        break_point = -1000
        for eid in self.sample_eids:
            try:
                yield self._parse_eid(eid)
            except KeyError as e:
                error_msg = ("EID: {0} is on in the sample master data, this"
                             " should have a superset of EIDs".format(eid))
                if self._error_on_missing_eid is True:
                    raise KeyError(error_msg) from e
                else:
                    warnings.warn(error_msg)
                    continue
            if counter == break_point:
                break
            counter += 1

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _parse_eid(self, eid):
        """
        Parse a sample ID from all of the index files

        Parameters
        ----------
        eid : int
            A single sample ID to extract from all of the index files
        """
        # This is to induce a KeyError if the EID is not present in the sample
        # master list, the parse function will handle it with respect to the
        # error_on_missing_eid argument
        self.sample_vitals[eid]

        # This gets all of the rows from the data relating to a single EID
        hes_event_rows, hes_diag_rows, hes_oper_rows, \
            gp_clinical_rows, gp_register_rows = common.fetch_sample_data(
                self.hes_events_reader,
                self.hes_diag_reader,
                self.hes_oper_reader,
                self.gp_clinical_reader,
                self.gp_register_reader,
                eid
            )

        episodes = self._parse_hes_event_episode(hes_event_rows)

        # Ignore data points where PCT is not defined
        pct_data = EventParser._extract_pct_data(episodes)

        diagnosis = self._parse_hes_diagnosis(hes_diag_rows)
        operations = self._parse_hes_operations(hes_oper_rows)

        # 
        all_hes = self._merge_hes_events(episodes, diagnosis, operations)
        # pp.pprint(all_hes)
        gp_records = self._parse_gp_records(gp_clinical_rows, pct_data)

        # Combine the HES and the GP records
        gp_records.extend(all_hes)

        # Put all the events in chronological and duration order. NoneType
        # dates will be sent to the bottom of the list.
        all_eid_events = sort_events(gp_records)

        episode_gp_cache = {}
        episode_hes_cache = {}

        # Here all_eid_events is a list of dicts with each dict element
        # representing an event in chronological order
        for evt in all_eid_events:
            # Extract a single clinical code from all the clinical codes
            # there really should only be a single code but this is just
            # to make sure
            evt[con.CLIN_DATA_CLINICAL_CODE_SYSTEM.name],\
                evt[con.CLIN_DATA_CLINICAL_CODE.name] = filter_clinical_codes(
                    evt['codes'][0]
                )

            # Increment the event index, this is the primary key for all the
            # events
            self._event_idx += 1
            evt[con.CLIN_DATA_EVENT_IDX.name] = self._event_idx

            # For the episode_idx if the record is a GP record we only
            # increment the episode index if the date is different from any.
            # stored in the episode_idx_cache. For the HES data we do it
            # based on the instance_idx not being in the episode_idx_cache,
            # if that has not been seen before then we increment. We probably
            # can get away with storing integer dates and instance_idxs as
            # keys in the same dict (i.e. not overwriting each other) as the
            # instance idx's are really small in comparison to the integer
            # dates, however, I won't take the chance.

            # This is the value that I will use as a key to lookup in the
            # cache
            lookup_cache_key = evt[con.CLIN_DATA_EVENT_START_INT.name]
            cache = episode_gp_cache
            if evt[con.CLIN_DATA_CARE_TYPE.name] == CARE_TYPE_SECONDARY:
                # In the event we have secondary data, swap the cache and the
                # lookup key
                lookup_cache_key = evt[con.CLIN_DATA_EVENT_START_INT.name]
                cache = episode_hes_cache

            try:
                # Now lookup in the cache
                evt[con.CLIN_DATA_EPISODE_IDX.name] = \
                    cache[lookup_cache_key]
            except KeyError:
                # Not in the cache, so increment the episode_idx and add it
                # to the cache
                self._episode_idx += 1
                cache[lookup_cache_key] = self._episode_idx
                evt[con.CLIN_DATA_EPISODE_IDX.name] = cache[lookup_cache_key]

            # Now expand the event provider into the name and the data source
            EventParser._extract_data_provider(evt)

            try:
                # Now calculate the relative times
                evt[con.CLIN_DATA_DAYS_FROM_BASELINE.name] = \
                    common.get_days_from_baseline(
                        self.sample_vitals[evt[con.EID.name]]
                        [con.SAM_DATE_OF_ASSESS.name].date_obj,
                        evt['event_start']
                )

                evt[con.CLIN_DATA_AGE_AT_EVENT.name] = \
                    common.get_age_at_event(
                    self.sample_vitals[evt[con.EID.name]]
                    [con.SAM_DATE_OF_BIRTH.name].date_obj,
                    evt['event_start']
                )
            except TypeError:
                # No dates defined for the sample
                evt[con.CLIN_DATA_DAYS_FROM_BASELINE.name] = None
                evt[con.CLIN_DATA_AGE_AT_EVENT.name] = None

        return gp_records, gp_register_rows

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _parse_hes_event_episode(self, rows):
        """
        Parses the event episode data for rows from a single EID. This data is
        the data that is derived from the hesin.txt table and is episode spells
        the diagnosis and the operations events should all map back to an
        episode spell.

        Parameters
        ----------
        rows : list of dict
            Each dict represents a hospital episode, this is the data output
            from the `file_helper.HesEventsFile` index reader object.

        Returns
        -------
        episodes : dict of dict
            The keys are tuples of (eid, instance_idx). This uniquely IDs an
            episode. Each dict value represents an hospital episode. The
            episodes have a start and an end date (in most cases) but no
            clinical codes.
        """
        episodes = {}
        for row in rows:
            # The episode key is the mechanism to match the same hospital
            # episodes across all the various HES datasets
            episode_key = (row[con.EID.name], row[con.INSTANCE_IDX.name])

            # We gather the PCT information from the PCT code. However,
            # sometimes they are not defined so we use the GP registration
            # PCT code to infill (and flag that we have)
            pct = row[con.HES_EVT_PCT_CODE.name]
            pct_is_gp = False
            if pct is None:
                pct = row[con.HES_EVT_GP_PCT.name]
                pct_is_gp = True

            # Attempt to get the date of the operation. This could
            # potentially be undefined and if it is use NoneType
            event_start, event_start_str, event_start_int = \
                EventParser._extract_date_data(
                    row,
                    con.HES_EVT_EPISODE_START.name
                )

            event_end, event_end_str, event_end_int = \
                EventParser._extract_date_data(
                    row,
                    con.HES_EVT_EPISODE_END.name
                )

            # Now put together the episode
            episode = {
                con.CLIN_DATA_CARE_TYPE.name: CARE_TYPE_SECONDARY,
                con.CLIN_DATA_DATA_PROVDER.name:
                (row[con.HES_EVT_DATA_SOURCE.name],
                 row[con.HES_EVT_DATA_SUB_SOURCE.name]),
                con.CLIN_DATA_PCT.name: pct,
                con.CLIN_DATA_PCT_IS_GP.name: pct_is_gp,
                'event_start': event_start,
                con.CLIN_DATA_EVENT_START_STR.name: event_start_str,
                con.CLIN_DATA_EVENT_START_INT.name: event_start_int,
                'event_end': event_end,
                'event_end_str': event_end_str,
                'event_end_int': event_end_int
            }
            episodes[episode_key] = episode
        return episodes

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _parse_hes_diagnosis(self, rows):
        """
        Process the component parts from the HES diagnosis and return

        Parameters
        ----------
        rows : list of dict
            Data rows from the HES diagnosis file that have been extracted
            with the HesDiagnosisFile

        Returns
        -------
        diagnosis : dict of list
            The keys for the dict are the episode key (a tuple of
            (eid, instance_index)). These match up unique episodes across the
            various HES files. Each episode can have multiple diagnosis events
            associated with it, hence the list values.
        """
        # Will hold and return all the processed diagnosis
        diagnosis = {}
        for row in rows:
            # Extract the clinical codes from the row
            codes = self.hes_diag_reader.__class__.get_clinical_code(row)

            # The episode key is the mechanism to match the same hospital
            # episodes across all the various HES datasets
            episode_key = (row[con.EID.name], row[con.INSTANCE_IDX.name])

            # Create a diagnosis
            diag_row = {
                'codes': codes,
                'record_type': 'diagnosis',
                'event_level': row[con.HES_DIAG_LEVEL.name]
            }

            try:
                # >1 diagnosis can exist for each episode key, so we make
                # sure we have initialised lists as the values
                diagnosis[episode_key].append(diag_row)
            except KeyError:
                # Not initialised yet, so initialise
                diagnosis[episode_key] = [diag_row]

        return diagnosis

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _parse_hes_operations(self, rows):
        """
        Parse and standardise the HES operations extracted for the current
        participant

        Parameters
        ----------
        rows : list of dict
            Rows for the current participant that have been returned by the
            operations reader. The keys of the dicts are column names and the
            values are column data

        Returns
        -------
        operations : dict of list
            The keys for the dict are the episode key (a tuple of
            (eid, instance_index)). These match up unique episodes across the
            various HES files. Each episode can have multiple operation events
            associated with it, hence the list values.
        """
        # Hold all the extracted and processed HES operations events
        operations = {}
        for row in rows:
            # The episode key is the mechanism to match the same hospital
            # episodes across all the various HES datasets
            episode_key = (row[con.EID.name], row[con.INSTANCE_IDX.name])

            # Attempt to get the date of the operation. This could
            # potentially be undefined and if it is use NoneType
            event_start, event_start_str, event_start_int = \
                EventParser._extract_date_data(
                    row,
                    con.HES_OPER_OPERATION_DATE.name
                )

            codes = self.hes_oper_reader.__class__.get_clinical_code(row)
            oper_row = {
                'codes': codes,
                'event_start': event_start,
                con.CLIN_DATA_EVENT_START_STR.name: event_start_str,
                con.CLIN_DATA_EVENT_START_INT.name: event_start_int,
                con.CLIN_DATA_RECORD_TYPE.name: 'operation',
                con.CLIN_DATA_EVENT_LEVEL.name: row[con.HES_DIAG_LEVEL.name],
                con.CLIN_DATA_DAYS_PRE_EVENT.name:
                row[con.HES_OPER_PRE_OP_DUR.name],
                con.CLIN_DATA_DAYS_POST_EVENT.name:
                row[con.HES_OPER_POST_OP_DUR.name]
            }

            try:
                operations[episode_key].append(oper_row)
            except KeyError:
                operations[episode_key] = [oper_row]
        return operations

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _merge_hes_events(self, episodes, diagnosis, operations):
        """
        Merge all the events extracted and normalised from the various files
        into a sequence of events

        Parameters
        ----------
        episodes : dict of dict
            The keys are tuples of (eid, instance_idx). This uniquely IDs an
            episode. Each dict value represents an hospital episode. The
            episodes have a start and an end date (in most cases) but no
            clinical codes.
        diagnosis : dict of list of dict
            Diagnosis represent ICD9 and ICD10 codes. The keys are tuples of
            (eid, instance_idx). This uniquely IDs an episode. Each list
            contains events (dicts) tied to the episode. Diagnosis, do not have
            any dates, they are gathered from the events table.
        operations : dict of list of dict
            Operations contain OPCS3/4 codes for operations that are tied to
            the event. . The keys are tuples of (eid, instance_idx). This
            uniquely IDs an episode. Operations also have a date attached to
            them that will be some time point with in the episode and
            represents the date of the actual procedure.

        Returns
        -------
        all_events : list of dict
            All the HES events combined into a single list
        """
        # Get a union of all the keys, we can probably just use episodes
        # but just in case. This is pretty nasty, will have to look up a
        # better way. The keys are EID/instance_idx tuples that uniquely ID
        # an episode
        all_keys = set(list(episodes.keys()) + list(diagnosis.keys()) + \
                       list(operations.keys()))

        # Will store all the processed HES diagnosis and operation events
        # for the current person
        all_events = []

        # TODO: Flag samples for which there are no diagnosis and operation
        #  events as surely these must be errors

        # This is essentially looping through all the unique episodes
        for key in all_keys:
            try:
                for evt in diagnosis[key]:
                    evt = EventParser._merge_diagnosis_to_episode(
                        key[0], key[1], evt, episodes[key]
                    )
                    all_events.append(evt)
            except KeyError:
                # There are no diagnosis events in the current episode
                pass

            try:
                for evt in operations[key]:
                    evt = EventParser._merge_operation_to_episode(
                        key[0], key[1], evt, episodes[key]
                    )
                    all_events.append(evt)
            except KeyError:
                # There are no operation events in the current episode
                pass

        return all_events

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _parse_gp_records(self, rows, pct_data):
        """
        Process the GP data into a standard format and assign the closest PCT
        record to the GP record

        Parameters
        ----------
        rows : list of dict
            Data rows from the GP clinical file that have been extracted with
            `file_handler.GpClinicalFile`
        pct_data : list of tuple
            The primary care trust data that has been extracted from the
            hesin.txt data. Each tuple has the format

        Returns
        -------
        gp_records : list of dict
            A standardised list of GP records
        """

        gp_records = []
        for row in rows:
            # pp.pprint(row)
            codes = self.gp_clinical_reader.__class__.get_clinical_code(row)

            try:
                # pp.pprint(row)
                pct = _get_nearest_pct(row['event_dt'].int_date, pct_data)
                # print(pct)
                days_to_pct = pct[1]
                pct_value = pct[2][1]
                pct_is_gp = pct[2][2]
                date_obj = row['event_dt'].date_obj
                date_str = row['event_dt'].pretty_date
                int_date = row['event_dt'].int_date
            except AttributeError:
                # There is no date defined, i.e. it is NoneType
                days_to_pct = None
                pct_value = None
                pct_is_gp = None
                date_obj = None
                date_str = None
                int_date = None

            # pp.pprint(codes)
            gp_records.append(
                {
                    'eid': row['eid'],
                    'eid_event_idx': None,
                    'record_type': 'gp',
                    'event_start': date_obj,
                    'event_start_str': date_str,
                    'event_start_int': int_date,
                    'event_level': 1,
                    'days_post_event': 0,
                    'days_pre_event': 0,
                    'care_type': 'primary',
                    'codes': codes,
                    'data_provider':
                    (self.__class__.GP_DATA_SOURCES[row['data_provider']], 0),
                    con.CLIN_DATA_DAYS_TO_PCT.name: days_to_pct,
                    con.CLIN_DATA_PCT.name: pct_value,
                    con.CLIN_DATA_PCT_IS_GP.name: pct_is_gp,
                }
            )
        return gp_records

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _check_is_open(self):
        """
        Check if the object has been opened already, if it has not raise an
        error

        Raises
        ------
        IOError
            If the object is not already open
        """
        if self._open is False:
            raise IOError("not open")

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _check_is_not_open(self):
        """
        Check if the object has been opened already, if it has raise an error

        Raises
        ------
        IOError
            If the object is already open
        """
        if self._open is True:
            raise IOError("is already open")


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _get_nearest_pct(search_date, pct_data):
    """
    return the nearest PCT time point to the search date

    Parameters
    ----------
    search_date : int
        The search date time point (number of days since 1-1-1900).
    pct_data : list of :obj:`Pct`
        A list containing `Pct` named tuples. This could also be empty if the
        participant has no HES events

    Returns
    -------
    min_abs_diff : int
        The minimal absolute differences
    signed_min_diff : int
        The potentially signed version of the minimal absolute difference
    pct_match : :obj:`Pct`
        The PCT information for the `Pct` with the minimal absolute difference
    """
    try:
        # We want to find the minimal difference so initialise the difference
        # with the first element of the PCT data
        diff = search_date - pct_data[0].event_start_int
    except (TypeError, IndexError):
        # The IndexError means there is no PCT data, so return an empty record
        # The TypeError means that the search_date is NoneType, either way
        # there is no near PCT data point
        return (None, None, Pct(None, None, None))

    # Initialise the minimal difference
    min_diff = (abs(diff), diff, pct_data[0])

    # Loop through the remainder of the data and test
    for i in pct_data:
        try:
            diff = search_date - i.event_start_int
        except TypeError:
            # This is i.event_start_int being NoneType
            # TODO: why is i.event_start_int NoneType??
            continue

        abs_diff = abs(diff)

        # Have to compare absolute differences
        if abs_diff < min_diff[0]:
            min_diff = (abs_diff, diff, i)
    return min_diff


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def sort_events(events):
    """
    Sort the events based on start date and duration. Events that have missing
    start date information will be placed last in the sort order. The sort
    occurs in place.

    Parameters
    ----------
    events : list of dict
        The events to sort. In order for the function to work the event
        dicts must have an `event_start_int`, `days_pre_event` and
        `days_post_event` keys.

    Returns
    -------
    sorted_events : list of dict
        Sorted events. Note that the return is not strictly necessary as the
        sort happens in place
    """
    # The first sort is to sort on event length and makes sure that
    # undefined length events and shorter events are at the bottom
    # the (pre_event or -1) + (post event or -1) is a bit of a fudge to stop
    # sort crashing if pre or post event is None, as they will get set to -1
    # and end up at the bottom of the sort order anyway
    events.sort(
        key=lambda x: (x[con.CLIN_DATA_EVENT_START_INT.name] is None,
                       (x[con.CLIN_DATA_DAYS_PRE_EVENT.name] or -1) +
                       (x[con.CLIN_DATA_DAYS_POST_EVENT.name] or -1)),
        reverse=True
    )

    # The second sort simply resorts on the integer event start date and again
    # ensures that undefined dates are at the bottom
    events.sort(
        key=lambda x: (x[con.CLIN_DATA_EVENT_START_INT.name] is None,
                       x[con.CLIN_DATA_EVENT_START_INT.name])
    )

    return events


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def filter_clinical_codes(codes):
    """
    When given multiple codes representing the same thing this filters them
    down to a single code based on the CODE_SYSTEM_ORDER module level list

    Parameters
    ----------
    codes : dict
        The keys of the dict should be coding systems and the values are the
        actual codes

    Returns
    -------
    coding_system : str
        The highest priority coding system (or the only one)
    code : str
        The code belonging to the highest priority coding system
    """
    def get_first_code(codes):
        try:
            lookup = next(iter(codes))
        except StopIteration:
            # This means that codes was empty for some reason
            return None, None
        return lookup, codes[lookup]

    # If there is only a single code then we return it
    if len(codes) == 1:
        return get_first_code(codes)

    # We get here if there is not a single code, loop through the
    # code system priority and return the first one we match
    for i in CODE_SYSTEM_ORDER:
        try:
            return codes[i]
        except KeyError:
            pass

    # The fallback is to return the first code in the dict. If the dict is
    # empty then this will generate an IndexError
    return get_first_code(codes)
