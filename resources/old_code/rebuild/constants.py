"""
Constants used in multiple modules
"""
from collections import namedtuple
import datetime
import os

# Withdrawn field
WITHDRAWN = (0, 0, 0)

# The baseline value for all subjects
BASELINE = 53

# Encode the data types
EventType = namedtuple('EventType', ['name', 'bits', 'desc'])

DATA_HES_VISIT = EventType(name="hospital_visit_event", bits=1 << 1,
                           desc="A hospital visit event")
DATA_HES_OPER = EventType(name="procedure_event", bits=1 << 2,
                          desc="A procedure event")
DATA_GP_VISIT = EventType(name="gp_visit_event", bits=1 << 3,
                          desc="A visit to the gp surgury")
DATA_GP_QUANT = EventType(name="gp_quant_event", bits=1 << 4,
                          desc="A quantitative measure taken via the GP")
DATA_GP_QUAL = EventType(name="gp_qual_event", bits=1 << 5,
                         desc="A qualitative measure taken via the GP")
DATA_GP_PRES = EventType(name="gp_pres_event", bits=1 << 6,
                         desc="A GP prescription event")
DATA_GP_BAD = EventType(name="gp_bad_event", bits=1 << 7,
                        desc="A GP event that is missing data")

# This is the source names that we will end up calling the code types
OPCS4_CODE_TYPE_NAME = 'opcs4'
OPCS3_CODE_TYPE_NAME = 'opcs3'
SNOMED_CODE_TYPE_NAME = 'snomed'
READ2_CODE_TYPE_NAME = 'read2'
READ2_DRUGS_CODE_TYPE_NAME = 'read2_drugs'
CTV3_CODE_TYPE_NAME = 'ctv3'
ICD9_CODE_TYPE_NAME = 'icd9'
ICD10_CODE_TYPE_NAME = 'icd10'
UMLS_CODE_TYPE_NAME = 'umls'
DMD_CODE_TYPE_NAME = 'dmd'
ALL_CODING_SYSTEMS = [
    OPCS4_CODE_TYPE_NAME,
    OPCS3_CODE_TYPE_NAME,
    SNOMED_CODE_TYPE_NAME,
    READ2_CODE_TYPE_NAME,
    CTV3_CODE_TYPE_NAME,
    ICD9_CODE_TYPE_NAME,
    ICD10_CODE_TYPE_NAME,
    UMLS_CODE_TYPE_NAME
]
# The global output delimiter of all files
GLOBAL_OUT_DELIMITER = "\t"
INTERNAL_FIELD_DELIMITER = '|'

LONG_FILE_NAME = 'ukbb_long'
LONG_FILE_NAME_NUMERIC = '{0}_numeric'.format(
    LONG_FILE_NAME
)
LONG_FILE_NAME_ENUM = '{0}_enum'.format(
    LONG_FILE_NAME
)
LONG_FILE_NAME_STRING = '{0}_string'.format(
    LONG_FILE_NAME
)
LONG_FILE_NAME_DATES = '{0}_dates'.format(
    LONG_FILE_NAME
)

SAMPLE_FILE_NAME = 'samples'
CODE_FILE_NAME = 'clinical_codes'
CUI_FILE_NAME = 'cui_counts'
CODE_MAPPING_LOG_FILE_NAME = 'clinical_codes_mapping_log'
# DIAG_PROC_FILE_NAME = 'diagnosis_procedures'
CLIN_DATA_FILE_NAME = 'clinical_data'
GP_REGISTER_FILE_NAME = 'gp_register'
# CLIN_MEASURE_FILE_NAME = 'clinical_measure'


# This is our reference date that we use to calculate interger date points from
# We are not using the real EPOCH date as there will be people in UKBB that are
# born before it
EPOCH_DATE = datetime.datetime(1900, 1, 1)
GLOBAL_DATE_FORMAT = "%Y-%m-%d"
OUFILE_DATE_FORMAT = '%Y%m%d'

# The ENVIRONMENT variable name
UKBB_CONFIG_ENVIR = 'UKBB_CONFIG'

# The UMLS db config section
UMLS_SECTION_NAME = 'umls_db_conn'
UMLS_ENTRY_PREFIX = 'umls.'

# The UKBB db config section
UKBB_SECTION_NAME = 'ukbb_db_conn'
UKBB_ENTRY_PREFIX = 'ukbb.'

TEMP_DB_PREFIX = '.ukbb'

# The UKBB db config section
UKBB_MAPPER_SECTION_NAME = 'ukbb_mapping_db_conn'
UKBB_MAPPER_ENTRY_PREFIX = 'ukbb_map.'

# The expected location of the config file
CONFIG_FILE_BASENAME = '.ukbb.cnf'
CONFIG_FILE_PATH = os.path.join(os.environ['HOME'], CONFIG_FILE_BASENAME)


# #############################################################################
# ############# Config File Section Headings For All Files Used ###############
# #############################################################################
WIDE_FILE = 'ukbb_wide'
HES_EVENTS_FILE = 'hes_events'
HES_DIAGNOSIS_FILE = 'hes_diagnosis'
HES_OPERATIONS_FILE = 'hes_operations'
GP_CLINICAL_FILE = 'gp_clinical'
GP_REGISTRATIONS_FILE = 'gp_registrations'
GP_PRESCRIPTIONS_FILE = 'gp_prescriptions'

ALL_FILE_SECTIONS_HEADINGS = [
    WIDE_FILE,
    HES_EVENTS_FILE,
    HES_DIAGNOSIS_FILE,
    HES_OPERATIONS_FILE,
    GP_CLINICAL_FILE,
    GP_REGISTRATIONS_FILE,
    GP_PRESCRIPTIONS_FILE
]
