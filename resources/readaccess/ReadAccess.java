package readaccess;

import readaccess.TableProcessor;
import com.healthmarketscience.jackcess.*;
import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.FileOutputStream;
import java.io.Writer;

import java.util.Set;
import java.util.zip.GZIPOutputStream;
import java.util.List;

import java.lang.ArrayIndexOutOfBoundsException;
import java.lang.System;
import java.lang.StringBuilder;
import java.lang.NullPointerException;

import java.nio.charset.StandardCharsets;


public class ReadAccess {
    public static String VERSION = "v0.1a";

    public static void main(String[] args) {
        // pre-declare
        String infile = "";
        String namespace = "";
        String outdir = "";
        String delimiter = "";
        System.out.println("=== ReadAccess "+ReadAccess.VERSION+" ===");

        // Three simple command line options
        try {
            infile = args[0];
            namespace = args[1];
            outdir = args[2];
            delimiter = args[3];
            System.out.println("[info] processing database: "+infile);
        }
        catch (ArrayIndexOutOfBoundsException e) {
            System.err.println("Usage: command [infile] [namespace] [outdir]");
            System.exit(1);
        }

        try {
            Database db = DatabaseBuilder.open(new File(infile));
            Set<String> all_tables = db.getTableNames();

            // Loop through all the tables
            for (String tableName : all_tables) {
                System.out.println("[info] processing "+tableName+"...");

                // get the table object from the name
                Table table = db.getTable(tableName);
                // this.setColumnData(table);

                TableProcessor tableProc = new TableProcessor(table);
                // System.out.println(tableProc.getHeaderRow(delimiter));
                String header = "";
                try {
                    header = tableProc.getHeaderRow(delimiter);
                } catch (IOException e) {
                    System.err.println("can't find header");
                    System.exit(1);
                }

                String fileName = outdir + File.separator +
                    tableName + "." +namespace + ".txt.gz";
                System.out.println("[info] outfile: " + fileName);

                FileOutputStream output = new FileOutputStream(fileName);
                try {
                    Writer writer = new OutputStreamWriter(
                        new GZIPOutputStream(output), "UTF-8"
                    );
                    try {
                        writer.write(header);

                        int c = 0;
                        for(Row row : table) {
                            String data = tableProc.getDataRow(row, delimiter);
                            writer.write(data);
                            if (c == -1) {
                                break;
                            }
                            c++;
                        }
                    } finally {
                        writer.close();
                    }
                } finally {
                    output.close();
                }
                // break;
            }

            // Table table = db.getTable("CTV3RCTMAP");
            // int counter = 0;
            // for(Row row : table) {
            //     // System.out.println("Look ma, a row: " + row);
            //     // Object value = row.get("SCUI");
            //     byte[] value = row.getBytes("SCUI");
            //     String s = new String(value);

            //     byte[] value2 = row.getBytes("STUI");
            //     String s2 = new String(value2);

            //     byte[] value3 = row.getBytes("TCUI");
            //     String s3 = new String(value3);
            //     System.out.println("SCUI: " + s + ", STUI: " + s2 + ", TCUI: " + s3);

            //     if (counter == 10) {
            //         break;
            //     }
            //     counter++;
                // for(Column column : table.getColumns()) {
                //     String columnName = column.getName();
                //     Object value = row.get(columnName);
                //     // System.out.println("Column " + columnName + "(" + column.getType() + "): "
                //     //                    + value + " (" + value.getClass() + ")");
                // }
                //            }
        }
        catch (IOException e) {
            // Do something here
            System.err.println("Can't read database");
            System.exit(1);
        }
    }
}


// // @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// class TableProcessor {
//     private Table table;
//     private String [] colNames;
//     private DataType [] colTypes;

//     // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//     public TableProcessor(Table table) {
//         this.table = table;
//         this.setColumnData();
//     }

//     // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//     private void setColumnData()
//     {
//         this.colNames = new String[this.table.getColumnCount()];
//         this.colTypes = new DataType[this.table.getColumnCount()];
//         int c = 0;
//         for(Column column : this.table.getColumns()) {
//             this.colNames[c] = column.getName();
//             this.colTypes[c] = column.getType();
//             c++;
//         }
//     }

//     // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//     public String getDataRow(Row row, String delimiter) throws IOException
//     {
//         // Get the header line from the table columns
//         StringBuilder dataRow = new StringBuilder();

//         try {
//             // Do not want a trailing delimiter
//             DataType dataType = this.colTypes[0];
//             String colName = this.colNames[0];
//             String dataPoint = this.getDataPoint(row, dataType, colName);
//             dataRow.append(dataPoint);
//         }
//         catch (ArrayIndexOutOfBoundsException e) {
//             throw new IOException("no columns");
//         }


//         for(int i=1; i< colNames.length; i++) {
//             DataType dataType = this.colTypes[i];
//             String colName = this.colNames[i];
//             dataRow.append(delimiter);
//             String dataPoint = this.getDataPoint(row, dataType, colName);
//             dataRow.append(dataPoint);
//         }
//         dataRow.append("\n");
//         return dataRow.toString();
//     }

//     // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//     private String getDataPoint(Row row, DataType dataTypeObj, String colName)
//     {
//         String dataType = dataTypeObj.toString();
//         String strData = "";
//         try {
//             switch (dataType) {
//             case "BINARY":
//                 byte[] bytesData = row.getBytes(colName);
//                 strData = new String(bytesData, StandardCharsets.UTF_8);
//                 break;
//             case "BYTE":
//                 Byte byteData = row.getByte(colName);
//                 // strData = new String(byteData);
//                 strData = byteData.toString();
//                 break;
//             case "LONG":
//                 Integer intData = row.getInt(colName);
//                 strData = intData.toString();
//                 break;
//             default:
//                 // row.getString(this.colNames[i])
//                 Object objData = row.get(colName);
//                 strData = objData.toString();
//             }
//         }
//         catch (NullPointerException e) {
//             strData = "";
//         }

//         // The byte arrays have nul characters in the so remove
//         return strData.replace("\0", "");
//     }

//     // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//     public String getHeaderRow(String delimiter) throws IOException
//     {
//         // Get the header line from the table columns
//         StringBuilder header = new StringBuilder();

//         try {
//             // Do not want a trailing delimiter
//             header.append(this.colNames[0]);
//         }
//         catch (ArrayIndexOutOfBoundsException e) {
//             throw new IOException("no columns");
//         }

//         for(int i=1; i< this.colNames.length; i++){
//             try {
//                 // String colName = allColumns[i].getName();
//                 header.append(delimiter);
//                 header.append(this.colNames[i]);
//             }
//             catch (ArrayIndexOutOfBoundsException e) {
//                 break;
//             }
//         }
//         header.append("\n");
//         return header.toString();
//     }
// }
