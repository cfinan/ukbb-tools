package readaccess;

import com.healthmarketscience.jackcess.*;
import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.FileOutputStream;
import java.io.Writer;

import java.util.Set;
import java.util.zip.GZIPOutputStream;
import java.util.List;

import java.lang.ArrayIndexOutOfBoundsException;
import java.lang.System;
import java.lang.StringBuilder;
import java.lang.NullPointerException;

import java.nio.charset.StandardCharsets;


// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
public class TableProcessor {
    private Table table;
    private String [] colNames;
    private DataType [] colTypes;

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    public TableProcessor(Table table) {
        this.table = table;
        this.setColumnData();
    }

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    private void setColumnData()
    {
        this.colNames = new String[this.table.getColumnCount()];
        this.colTypes = new DataType[this.table.getColumnCount()];
        int c = 0;
        for(Column column : this.table.getColumns()) {
            this.colNames[c] = column.getName();
            this.colTypes[c] = column.getType();
            c++;
        }
    }

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    public String getDataRow(Row row, String delimiter) throws IOException
    {
        // Get the header line from the table columns
        StringBuilder dataRow = new StringBuilder();

        try {
            // Do not want a trailing delimiter
            DataType dataType = this.colTypes[0];
            String colName = this.colNames[0];
            String dataPoint = this.getDataPoint(row, dataType, colName);
            dataRow.append(dataPoint);
        }
        catch (ArrayIndexOutOfBoundsException e) {
            throw new IOException("no columns");
        }


        for(int i=1; i< colNames.length; i++) {
            DataType dataType = this.colTypes[i];
            String colName = this.colNames[i];
            dataRow.append(delimiter);
            String dataPoint = this.getDataPoint(row, dataType, colName);
            dataRow.append(dataPoint);
        }
        dataRow.append("\n");
        return dataRow.toString();
    }

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    private String getDataPoint(Row row, DataType dataTypeObj, String colName)
    {
        String dataType = dataTypeObj.toString();
        String strData = "";
        try {
            switch (dataType) {
            case "BINARY":
                byte[] bytesData = row.getBytes(colName);
                strData = new String(bytesData, StandardCharsets.UTF_8);
                break;
            case "BYTE":
                Byte byteData = row.getByte(colName);
                // strData = new String(byteData);
                strData = byteData.toString();
                break;
            case "LONG":
                Integer intData = row.getInt(colName);
                strData = intData.toString();
                break;
            default:
                // row.getString(this.colNames[i])
                Object objData = row.get(colName);
                strData = objData.toString();
            }
        }
        catch (NullPointerException e) {
            strData = "";
        }

        // The byte arrays have nul characters in the so remove
        return strData.replace("\0", "");
    }

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    public String getHeaderRow(String delimiter) throws IOException
    {
        // Get the header line from the table columns
        StringBuilder header = new StringBuilder();

        try {
            // Do not want a trailing delimiter
            header.append(this.colNames[0]);
        }
        catch (ArrayIndexOutOfBoundsException e) {
            throw new IOException("no columns");
        }

        for(int i=1; i< this.colNames.length; i++){
            try {
                // String colName = allColumns[i].getName();
                header.append(delimiter);
                header.append(this.colNames[i]);
            }
            catch (ArrayIndexOutOfBoundsException e) {
                break;
            }
        }
        header.append("\n");
        return header.toString();
    }
}
