# Create an empty database with utf8 encoding
DROP DATABASE IF EXISTS ukbb_db;
CREATE DATABASE ukbb_db CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE ukbb_db;

CREATE TABLE coding_systems
(
  coding_system_id INT AUTO_INCREMENT PRIMARY KEY,
	coding_system_name VARCHAR(15) NULL
);

CREATE TABLE clinical_codes (
    clinical_code_id INT AUTO_INCREMENT PRIMARY KEY,
    clinical_code    VARCHAR(255) NULL COLLATE utf8_bin,
    coding_system_id           INT          NULL,
    ncui            INT          NOT NULL,
    min_n_value     INT          NOT NULL,
    max_n_value     INT          NOT NULL,
    n_occur         INT          NOT NULL,
    n_is_empty       INT          NOT NULL,
    n_is_string      INT          NOT NULL,
    n_is_integer     INT          NOT NULL,
    n_is_float       INT          NOT NULL,
    ratio_3_0_0      INT          NOT NULL,
    ratio_0_3_0      INT          NOT NULL,
    ratio_0_0_3      INT          NOT NULL,
    ratio_2_1_0      INT          NOT NULL,
    ratio_2_0_1      INT          NOT NULL,
    ratio_1_2_0      INT          NOT NULL,
    ratio_0_2_1      INT          NOT NULL,
    ratio_1_0_2      INT          NOT NULL,
    ratio_0_1_2      INT          NOT NULL,
    ratio_1_1_1      INT          NOT NULL,
    CONSTRAINT clinical_code_ibfk_1
    FOREIGN KEY (coding_system_id) REFERENCES coding_systems (coding_system_id)
);

CREATE INDEX ix_clinical_code_clinical_code
    ON clinical_codes (clinical_code);

CREATE INDEX coding_system_id
ON clinical_codes (coding_system_id);

