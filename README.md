# UKBB tools

__version__: `0.3.0a0`

##  Overview of UK BioBank
The UK BioBank is a cohort study with multiple follow ups. UK BioBank has conducted deep phenotyping on a large proportion of the cohort. This includes biomarker measures, questionnaire data and imaging data. A key feature of the UK BioBank is that the cohort has been genotyped and a subset has been whole exome sequenced (WES). with imputation and a large proportion of it has been linked to both hospital records (HES) and primary care data from general practitioners (GP). The contents of the UK BioBank data set are showing in the schematic below.

## Overview of ukbb-tools
Whilst UK BioBank is a phenomenal effort, the phenotypic data, particullarly the hospital data are not very tractable or usable in their raw form. The UK Biobank also has the RAP service that people can use. However, this service costs extra and the costing/billing is opaque. Therefore, UK BioBank tools aims to provide several interfaces to the UK BioBank dataset to make it both tractable and usable to geneticists, epidemiologists and data scientists. ukbb-tools has been developed in Python so should be useful to a wide range of the research community. In addition to various levels of API access, this package contains entry points for several command line scripts that mean users can accomplish many tasks without using Python at all.

There is [online](https://cfinan.gitlab.io/ukbb-tools/index.html) documentation for ukbb-tools.

## Installation instructions
At present, ukbb-tools is undergoing development and no packages exist yet on PyPi. Therefore it is recommended that you install via conda unless you have access to gwas-norm, in which case you can use a pip install from the git repository.

### Installation using conda
I maintain a conda package in my personal conda channel. To install from this please run:

```
conda install -c cfin -c bioconda -c conda-forge ukbb-tools
```

There are currently builds for Python v3.8, v3.9, v3.10 for Linux-64 and Mac-osX.

Please keep in mind that all development is carried out on Linux-64 and Python v3.8/v3.9. I do not own a Mac so can't test on one, the conda build does run some import tests but that is it.

### Installation using pip
You can install using pip from the root of the cloned repository, first clone and cd into the repository root:

```
git clone git@gitlab.com:cfinan/ukbb-tools.git
cd ukbb-tools
```

Install the dependencies:
```
python -m pip install --upgrade -r requirements.txt
```

Then install using pip
```
python -m pip install .
```

Or for an editable (developer) install run the command below from the root of the repository. The difference with this is that you can just to a `git pull` to update, or switch branches without re-installing:
```
python -m pip install -e .
```

### Conda dependencies
There are also conda yaml environment files in `./resources/conda/envs` that have the same contents as `requirements.txt` but for conda packages, so all the pre-requisites. I use this to install all the requirements via conda and then install the package as an editable pip install.

However, if you find these useful then please use them. There are Conda environments for Python v3.8, v3.9, v3.10.


## Change log

### version `0.2.0a0`
* SCRIPTS - Added a `ukbb-samples` script for trawling through files and extracting UK Biobank sample IDs.
* API - Added `ukbb_tools.samples` with an API endpoint for `ukbb_tools.samples.sample_search`.
* API - Added parsing module `ukbb_tools.parsers`.
* API - Added column definitions module `ukbb_tools.columns`.
* API - Added custom errors module `ukbb_tools.errors`.
* API - Added data type conversions module `ukbb_tools.type_casts`.
* DATA - Rebuilt the data dictionary database.
* TESTS - Added tests for samples, some type casts and one parser

### version `0.2.1a0`
* BUILD - Fixed [pyaddons](https://gitlab.com/cfinan/pyaddons) version number.

### version `0.3.0a0`
* API - Added `ukbb_tools.remap` module with API and script endpoint.
* API - Added `ukbb_tools.config` module with API and script endpoint.
* API - Moved original data dict build module to `ukbb_tools.data_dict.build_old`.
* API - Moved original data dict ORM module to `ukbb_tools.data_dict.orm_old`.
* API - Added updated data dictionary build module `ukbb_tools.data_dict.build`. Build now uses parsing interface.
* API - Added updated data dictionary ORM module  `ukbb_tools.data_dict.orm`.
* BUILD - Updated to use SQLAlchemy >= v2.
* DATA - Rebuilt the data dictionary database using new UKB data dictionary files.
* SCRIPTS - Added a `ukbb-remap` script remapping between sample IDs from different applications.
* SCRIPTS - Added a `ukbb-config` script providing cmd-line access to the ukbb-tools configuration file.
* SCRIPTS - Moved the old data dictionary build script to `ukbb-build-old-data-dict`.
