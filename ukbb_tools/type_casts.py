"""Contains a range of small atomic parsing functions, often attached to
`ukbb_tools.columns.ColName` types.
"""
from ukbb_tools import constants as con, errors
from collections import namedtuple
from datetime import datetime


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_bool(value):
    """Parse a integer 0/1 (possible as string type) into a Boolean value.

    Parameters
    ----------
    value : `str` or `int`
        The value to parse into a bool.

    Returns
    -------
    parsed_value : `bool`
        The parsed value.
    """
    value = int(value)
    if value < 0 or value > 1:
        raise ValueError("Integer Booleans should be 0/1")
    return bool(value)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_bool_none(value):
    """Parse a integer 0/1 (possible as string type) into a Boolean value.
    This, allows for a `NoneType` value.

    Parameters
    ----------
    value : `str` or `int` or `NoneType`
        The value to parse into a bool.

    Returns
    -------
    parsed_value : `bool` or `None`
        The parsed value.
    """
    try:
        return parse_bool(value)
    except TypeError:
        if value is not None:
            raise
        return None


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_sex(value):
    """Parse a sex value, this should be 1/2.

    Parameters
    ----------
    value : `str` or `int`
        The value to parse into a sex int.

    Returns
    -------
    parsed_value : `int`
        The parsed value.
    """
    value = int(value)
    if value < 1 or value > 2:
        raise ValueError("Integer sex values should be 1/2")
    return value


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_sex_none(value):
    """Parse a sex value, this should be 1/2.
    This, allows for a `NoneType` value.

    Parameters
    ----------
    value : `str` or `int` or `NoneType`
        The value to parse into a sex int.

    Returns
    -------
    parsed_value : `int` or `None`
        The parsed value.
    """
    try:
        return parse_sex(value)
    except TypeError:
        if value is not None and value != '0':
            raise
        return None


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_eid(value):
    """Parse an eid value (sample ID), this should be all digits with a length
    of 7.

    Parameters
    ----------
    value : `str`
        The EID value to parse/check.

    Returns
    -------
    parsed_value : `str``
        The parsed value.
    """
    value = str(value)
    if not value.isdigit() or len(value) != 7:
        raise errors.EidError("This does not look like an EID", value)
    return value


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_eid_withdrawn(value):
    """Parse an eid value (sample ID), this should be all digits with a length
    of 7.

    This version allows for the possibility that the EID is set to withdrawn,
    i.e. can be a negative integer.

    Parameters
    ----------
    value : `str`
        The EID value to parse/check.

    Returns
    -------
    parsed_value : `str``
        The parsed value.
    """
    try:
        parse_eid(value)
    except errors.EidError:
        try:
            if int(value) >= 0:
                raise
        except ValueError:
            raise
    return str(value)
