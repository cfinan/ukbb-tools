"""Contains a range of small atomic parsing functions for the data dictionary
files, often attached to `ukbb_tools.columns.ColName` types.
"""
from ukbb_tools.data_dict import constants as con


_SEX_LOOKUP = {
    0: 'unisex',
    1: 'male',
    2: 'female'
}
"""Mapping lookups to map from integer sex and unisex to strings (`dict`)
"""


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_data_type(value):
    """Parse the integer data type value to a string.

    Parameters
    ----------
    value : `str`
        The data type value to parse.

    Returns
    -------
    parsed_value : `str``
        The parsed value.

    Raises
    ------
    TypeError
        If the data type value is not one of the recognized types.
    """
    try:
        return con.NEW_DATA_TYPE_MAP[value]
    except KeyError as e:
        raise TypeError(f"Unknown data type value: {value}") from e


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_sex(value):
    """Parse a sex value, this should be 1/2.

    Parameters
    ----------
    value : `str` or `int`
        The value to parse into a sex int.

    Returns
    -------
    parsed_value : `str`
        The parsed value.
    """
    return _SEX_LOOKUP[int(value)]


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_item_type(value):
    """Parse the item type value to a string.

    Parameters
    ----------
    value : `str`
        The item type value to parse.

    Returns
    -------
    parsed_value : `str``
        The parsed value.

    Raises
    ------
    TypeError
        If the item type value is not one of the recognized types.
    """
    try:
        return con.ITEM_TYPE_MAP[value]
    except KeyError as e:
        raise TypeError(f"Unknown item type value: {value}") from e


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_strata(value):
    """Parse the strata value to a string.

    Parameters
    ----------
    value : `str`
        The strata value to parse.

    Returns
    -------
    parsed_value : `str``
        The parsed value.

    Raises
    ------
    TypeError
        If the strata value is not one of the recognized types.
    """
    try:
        return con.STRATA_MAP[value]
    except KeyError as e:
        raise TypeError(f"Unknown strata value: {value}") from e
