"""Downloaders and parsers for the data dictionary files.
"""
from ukbb_tools import parsers
from ukbb_tools.data_dict import columns as col
from pyaddons import utils
from urllib.request import urlretrieve
from tqdm.auto import tqdm
import shutil
import os
# import pprint as pp


PC = parsers.ParseColumn


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class TqdmUpTo(tqdm):
    """Provides `update_to(n)` which uses `tqdm.update(delta_n)`.
    """
    def update_to(self, b=1, bsize=1, tsize=None):
        """Update the progress monitor.

        Parameters
        ----------
        b : `int`, optional, default: `1`
            Blocks transferred so far.
        bsize : `int`, optional, default: `1`
            Size of each block
        tsize : `int`, optional, default: `NoneType`
            Total size
        """
        if tsize is not None:
            self.total = tsize
        self.update(b * bsize - self.n)  # will also set self.n = b * bsize


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class DataDictParser(parsers._BaseParser):
    """A handler for the data dictionary files.

    Parameters
    ----------
    number : `int`
        The file number, this is used in the download URL.
    name : `str`
        The file download name, this is for a human readable name.
    path : `str`
        The path where the data dictionary files will be downloaded.
    tmpdir : `str`, optional, default: `NoneType`
        A temp directory to use during file downloading. If not supplied then
        the system default temp location is used.
    indexes : `list` of `str`
        Paths to any file indexes. If not supplied then index files will be
        looked for based on the name of the infile. However, if their file name
        is non-standard, you can supply them here.
    has_header : `bool`, optional, default: `False`
        Does the input file have a header, if not then various checks will
        happen to determine if that is the case (warnings will be given).
        The column specifications will be used as the header columns.
    header : `list` or `ukbb_tools.parsers.ParseColumn`
        A header specification.
    header_check : `int`, optional, default: `1`
        What type of header checking should be performed.
        `ukbb_tools.LENGTH_HEADER_CHECK` will check that
        each data row is the same length as the header row.
        `ukbb_tools.MIN_LENGTH_HEADER_CHECK` will check that
        each data row is at least as long as the header row.
    encoding : `str`, optional, default: `utf8`
        The encoding of the input file. Only used if a file object has not been
        supplied.
    comment_char : `str`, optional, default: `NoneType`
        A comment character, any rows starting with this are skipped.
    skiplines : `int`, optional, default: `0`
        Skip this number of fixed lines before attempting to read any
        headers/data from the file. This is only used if a file object has not
        been supplied.
    use_dict : `bool`, optional, default: `False`
        Return rows as dictionaries rather than lists. Internally this will use
        `csv.DictReader` instead of `csv.reader`.
    **csv_kwargs
        Any keyword arguments usually supplied to Python `csv.reader` or
        `csv.DictReader`. If none are supplied then the dialect is attempted to
        be sniffed.
    """
    EXTENSION = ".txt"
    """The file extensions for the downloaded data dictionary files (`str`)
    """
    BASE_URL = "https://biobank.ctsu.ox.ac.uk/ukb/scdown.cgi?fmt=txt&id={0}"
    """The base URL to use when downloading the files, this contains a
    format placeholder (`str`)
    """

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, number, name, path, tmpdir=None, **kwargs):
        self.number = number
        self.name = name
        self.path = path
        self.tmpdir = tmpdir

        default_kwargs = dict(
            encoding=parsers.LATIN1, use_dict=True,
            has_header=True, header=self.COLUMNS, delimiter="\t",
            header_check=parsers.MIN_LENGTH_HEADER_CHECK,
            comment_char=None, skiplines=0
        )
        kwargs = {**default_kwargs, **kwargs}
        super().__init__(
            self.pathname, **kwargs
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def basename(self):
        """Get the basename of a data dictionary file, this has the format:
        ``<number>_<name>.<extension>`` (`str`).
        """
        return f"{self.number}_{self.name}{self.EXTENSION}"

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def url(self):
        """Get the download URL of a data dictionary file (`str`).
        """
        return self.BASE_URL.format(self.number)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def pathname(self):
        """Get the full pathname to a downloaded data dictionary file, this has
        the format: ``<PATH>/<number>_<name>.<extension>``.
        """
        return os.path.join(self.path, self.basename)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def exists(self):
        """Get the full pathname to a downloaded data dictionary file.

        Parameters
        ----------
        dirname : `str`
            The root directory to place the data dictionary file.

        Returns
        -------
        does_exist : `bool`
            An indicator if the file exists.
        """
        try:
            open(self.pathname).close()
            return True
        except FileNotFoundError:
            return False

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_file(self, verbose=False):
        """Get the full pathname to a downloaded data dictionary file. This
        will download the file if it does not already exist.

        Parameters
        ----------
        verbose : `bool`, optional, default: `False`
            Show download progress.

        Returns
        -------
        downloaded_file_path : `str`
            The path to the downloaded file.
        """
        file_path = self.pathname
        try:
            open(file_path).close()
            # Should also check the file age
            return file_path
        except FileNotFoundError:
            return self.download_file(verbose=verbose)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def download_file(self, verbose=False):
        """Download a data dictionary file.

        Parameters
        ----------
        verbose : `bool`, optional, default: `False`
            Show download progress.

        Returns
        -------
        downloaded_file_path : `str`
            The path to the downloaded file.
        """
        # Get a tempfile
        tmp_download = utils.get_temp_file(dir=self.tmpdir)
        outfile = self.pathname
        # Download
        desc = f"[info] downloading {self.basename}"
        try:
            with TqdmUpTo(unit='B', unit_scale=True, unit_divisor=1024,
                          miniters=1, desc=desc, disable=not verbose,
                          leave=False) as t:
                urlretrieve(
                    self.url, filename=tmp_download,
                    reporthook=t.update_to,
                    data=None
                )
                t.total = t.n
        except Exception:
            os.unlink(tmp_download)
            raise

        # move temp to final
        shutil.move(tmp_download, outfile)
        return outfile


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class EncodingLookupParser(DataDictParser):
    """The handler for the encoding dictionary file.

    Parameters
    ----------
    number : `int`
        The file number, this is used in the download URL.
    name : `str`
        The file download name, this is for a human readable name.
    path : `str`
        The path where the data dictionary files will be downloaded.
    tmpdir : `str`, optional, default: `NoneType`
        A temp directory to use during file downloading. If not supplied then
        the system default temp location is used.
    """
    COLUMNS = [
        PC(col.ENCODING_ID, allow_missing=False),
        PC(col.TITLE, allow_missing=False),
        PC(col.AVAILABILITY, allow_missing=False),
        PC(col.CODED_AS, allow_missing=False),
        PC(col.STRUCTURE, allow_missing=False),
        PC(col.NUM_MEMBERS, allow_missing=False),
        PC(col.DESCRIPTION, allow_missing=False),
    ]


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class EncodingValueParser(DataDictParser):
    """The handler for the encoding values files. This can handle multiple
    files.

    Parameters
    ----------
    number : `int`
        The file number, this is used in the download URL.
    name : `str`
        The file download name, this is for a human readable name.
    path : `str`
        The path where the data dictionary files will be downloaded.
    tmpdir : `str`, optional, default: `NoneType`
        A temp directory to use during file downloading. If not supplied then
        the system default temp location is used.
    """
    COLUMNS = [
        PC(col.ENCODING_ID, allow_missing=False),
        PC(col.VALUE, allow_missing=False),
        PC(col.MEANING, allow_missing=False),
        PC(col.SHOWCASE_ORDER, allow_missing=False),
    ]


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class HierEncodingValueParser(DataDictParser):
    """The handler for the hierarchical encoding values files. This can handle
    multiple files.

    Parameters
    ----------
    number : `int`
        The file number, this is used in the download URL.
    name : `str`
        The file download name, this is for a human readable name.
    path : `str`
        The path where the data dictionary files will be downloaded.
    tmpdir : `str`, optional, default: `NoneType`
        A temp directory to use during file downloading. If not supplied then
        the system default temp location is used.
    """
    COLUMNS = [
        PC(col.ENCODING_ID, allow_missing=False),
        PC(col.CODE_ID, allow_missing=False),
        PC(col.PARENT_CODE_ID, allow_missing=False),
        PC(col.VALUE, allow_missing=False),
        PC(col.MEANING, allow_missing=False),
        PC(col.SELECTABLE, allow_missing=False),
        PC(col.SHOWCASE_ORDER, allow_missing=False),
    ]


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class CategoryParser(DataDictParser):
    """The handler for the category dictionary lookup file.

    Parameters
    ----------
    number : `int`
        The file number, this is used in the download URL.
    name : `str`
        The file download name, this is for a human readable name.
    path : `str`
        The path where the data dictionary files will be downloaded.
    tmpdir : `str`, optional, default: `NoneType`
        A temp directory to use during file downloading. If not supplied then
        the system default temp location is used.
    """
    COLUMNS = [
        PC(col.CATEGORY_ID, allow_missing=False),
        PC(col.NAME, allow_missing=False),
        PC(col.AVAILABILITY, allow_missing=False),
        PC(col.GROUP_TYPE, allow_missing=False),
        PC(col.DESCRIPTION, allow_missing=False),
        PC(col.NOTES, allow_missing=False),
    ]


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class CategoryBrowseParser(DataDictParser):
    """The handler for the category browser (hierarchy) file.

    Parameters
    ----------
    number : `int`
        The file number, this is used in the download URL.
    name : `str`
        The file download name, this is for a human readable name.
    path : `str`
        The path where the data dictionary files will be downloaded.
    tmpdir : `str`, optional, default: `NoneType`
        A temp directory to use during file downloading. If not supplied then
        the system default temp location is used.
    """
    COLUMNS = [
        PC(col.PARENT_CODE_ID, allow_missing=False),
        PC(col.CHILD_CODE_ID, allow_missing=False),
        PC(col.SHOWCASE_ORDER, allow_missing=False),
    ]


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class FieldIdParser(DataDictParser):
    """The handler for the field ID lookup file.

    Parameters
    ----------
    number : `int`
        The file number, this is used in the download URL.
    name : `str`
        The file download name, this is for a human readable name.
    path : `str`
        The path where the data dictionary files will be downloaded.
    tmpdir : `str`, optional, default: `NoneType`
        A temp directory to use during file downloading. If not supplied then
        the system default temp location is used.
    """
    COLUMNS = [
        PC(col.FIELD_ID, allow_missing=False),
        PC(col.TITLE, allow_missing=False),
        PC(col.AVAILABILITY, allow_missing=False),
        PC(col.STABILITY, allow_missing=False),
        PC(col.PRIVATE, allow_missing=False),
        PC(col.VALUE_TYPE, allow_missing=False),
        PC(col.BASE_TYPE, allow_missing=False),
        PC(col.ITEM_TYPE, allow_missing=False),
        PC(col.STRATA, allow_missing=False),
        PC(col.INSTANCED, allow_missing=False),
        PC(col.ARRAYED, allow_missing=False),
        PC(col.SEXED, allow_missing=False),
        PC(col.UNITS, allow_missing=False),
        PC(col.MAIN_CATEGORY, allow_missing=False),
        PC(col.ENCODING_ID, allow_missing=False),
        PC(col.INSTANCE_ID, allow_missing=False),
        PC(col.INSTANCE_MIN, allow_missing=False),
        PC(col.INSTANCE_MAX, allow_missing=False),
        PC(col.ARRAY_MIN, allow_missing=False),
        PC(col.ARRAY_MAX, allow_missing=False),
        PC(col.NOTES, allow_missing=False),
        PC(col.DEBUT, allow_missing=False),
        PC(col.VERSION, allow_missing=False),
        PC(col.N_PARTICIPANTS, allow_missing=False),
        PC(col.ITEM_COUNT, allow_missing=False),
        PC(col.SHOWCASE_ORDER, allow_missing=False),
        PC(col.COST_DO, allow_missing=False),
        PC(col.COST_ON, allow_missing=False),
        PC(col.COST_SC, allow_missing=False),
    ]


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class FieldIdLinkParser(DataDictParser):
    """The handler for the field ID link file.

    Parameters
    ----------
    number : `int`
        The file number, this is used in the download URL.
    name : `str`
        The file download name, this is for a human readable name.
    path : `str`
        The path where the data dictionary files will be downloaded.
    tmpdir : `str`, optional, default: `NoneType`
        A temp directory to use during file downloading. If not supplied then
        the system default temp location is used.
    """
    COLUMNS = [
        PC(col.FIELD_ID_LOW, allow_missing=False),
        PC(col.FIELD_ID_HIGH, allow_missing=False),
        PC(col.LINK_DESCRIPTION, allow_missing=False),
    ]


# The file numbers/names of all the data dictionary files, only a subset of
# these will actually be used

FIELD_FILE = (1, "field", FieldIdParser)
"""A representation of the fields file
(`tuple`)
"""
ENCODING_FILE = (2, "encoding_dict", EncodingLookupParser)
"""A representation of the encodings file
(`tuple`)
"""
CATEGORY_FILE = (3, "category", CategoryParser)
"""A representation of the category file
(`tuple`)
"""
RETURNED_DATA_FILE = (4, "returned_data", None)
"""A representation of the returned data file
(`tuple`)
"""
INT_ENCODING_FILE = (5, "int_encoding_values", EncodingValueParser)
"""A representation of the integer encodings file
(`tuple`)
"""
STR_ENCODING_FILE = (6, "str_encoding_values", EncodingValueParser)
"""A representation of the string encodings file
(`tuple`)
"""
FLOAT_ENCODING_FILE = (7, "float_encoding_values", EncodingValueParser)
"""A representation of the float encodings file
(`tuple`)
"""
DATE_ENCODING_FILE = (8, "date_encoding_values", EncodingValueParser)
"""A representation of the date encodings file
(`tuple`)
"""
INSTANCE_DICT_FILE = (9, "instance_dict", None)
"""A representation of the instance dictionary file
(`tuple`)
"""
INSTANCE_VALUES_FILE = (10, "instance_values", None)
"""A representation of the instance values file
(`tuple`)
"""
HIER_INT_ENCODING_FILE = (11, "hierarch_int_encoding_values",
                          HierEncodingValueParser)
"""A representation of the hierarchical integer encodings file
(`tuple`)
"""
HIER_STR_ENCODING_FILE = (12, "hierarch_str_encoding_values",
                          HierEncodingValueParser)
"""A representation of the hierarchical string encodings file
(`tuple`)
"""
CATEGORY_BROWSE_FILE = (13, "cat_browse_tree", CategoryBrowseParser)
"""A representation of the category browse file
(`tuple`)
"""
DATA_FIELD_RECOMMEND_FILE = (14, "data_field_recommend", None)
"""A representation of the recommended data field file
(`tuple`)
"""
GENOTYPED_SNPS_FILE = (15, "genotyped_snps", None)
"""A representation of the genotyped snps file
(`tuple`)
"""
DATA_FIELD_SUMMARY_FILE = (16, "data_field_summary", None)
"""A representation of the field summary file
(`tuple`)
"""
DATA_PORTAL_TABLES_FILE = (17, "data_portal_tables", None)
"""A representation of the data portal tables file
(`tuple`)
"""
RECORD_TABLES_DATA_COL_FILE = (18, "record_tables_data_cols", None)
"""A representation of the record tables file
(`tuple`)
"""
PUBLICATIONS_FILE = (19, "publications", None)
"""A representation of the publications file
(`tuple`)
"""
TIME_ENCODING_FILE = (20, "time_encoding_values", EncodingValueParser)
"""A representation of the time encodings file
(`tuple`)
"""
DATA_FIELD_LINK_FILE = (21, "data_field_linkage", FieldIdLinkParser)
"""A representation of the data field linkage file
(`tuple`)
"""
INTERNAL_RESOURCES_FILE = (22, "internal_resources", None)
"""A representation of the internal resources file
(`tuple`)
"""
EXTERNAL_RESOURCES_FILE = (23, "external_resources", None)
"""A representation of the external resources file
(`tuple`)
"""
APP_PUBLISH_LINK_FILE = (24, "app_publish_link", None)
"""A representation of the application publishing link file
(`tuple`)
"""
FIELD_RESOURCE_LINK_FILE = (25, "field_resource_link", None)
"""A representation of the field resource link file
(`tuple`)
"""
CATEGORY_RESOURCE_LINK_FILE = (26, "category_resource_link", None)
"""A representation of the category resource link file
(`tuple`)
"""

ALL_DATA_DICT_FILES = [
    FIELD_FILE,
    ENCODING_FILE,
    CATEGORY_FILE,
    RETURNED_DATA_FILE,
    INT_ENCODING_FILE,
    STR_ENCODING_FILE,
    FLOAT_ENCODING_FILE,
    DATE_ENCODING_FILE,
    INSTANCE_DICT_FILE,
    INSTANCE_VALUES_FILE,
    HIER_INT_ENCODING_FILE,
    HIER_STR_ENCODING_FILE,
    CATEGORY_BROWSE_FILE,
    DATA_FIELD_RECOMMEND_FILE,
    GENOTYPED_SNPS_FILE,
    DATA_FIELD_SUMMARY_FILE,
    DATA_PORTAL_TABLES_FILE,
    RECORD_TABLES_DATA_COL_FILE,
    PUBLICATIONS_FILE,
    TIME_ENCODING_FILE,
    DATA_FIELD_LINK_FILE,
    INTERNAL_RESOURCES_FILE,
    EXTERNAL_RESOURCES_FILE,
    APP_PUBLISH_LINK_FILE,
    FIELD_RESOURCE_LINK_FILE,
    CATEGORY_RESOURCE_LINK_FILE,
]
"""All available data dictionary files
(`list` of `tuple`)
"""


USED_DATA_DICT_FILES = [
    ENCODING_FILE,
    INT_ENCODING_FILE,
    STR_ENCODING_FILE,
    FLOAT_ENCODING_FILE,
    DATE_ENCODING_FILE,
    TIME_ENCODING_FILE,
    HIER_INT_ENCODING_FILE,
    HIER_STR_ENCODING_FILE,
    CATEGORY_FILE,
    CATEGORY_BROWSE_FILE,
    FIELD_FILE,
    DATA_FIELD_LINK_FILE,
]
"""The data dictionary files that are actually used to build the database
(`list` of `tuple`)
"""
