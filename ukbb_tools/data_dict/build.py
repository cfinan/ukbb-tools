"""Build a UK Biobank data dictionary SQLite database from the files provided
by UK Biobank. Note this will overwrite any existing databases given to it.

If you wish to build the data dictionary against another database backend
please use the API, see the documentation for more details.
"""
from ukbb_tools import (
    __version__,
    __name__ as pkg_name,
    # To load the SQLite listener for foreign key pragma
    common,
    cmd_args,
    examples as ex
)
from ukbb_tools.data_dict import (
    orm as dd,
    constants as con,
    parsers,
    columns as col
)
from pyaddons import log, utils as pau
from tqdm import tqdm
import numpy as np
import sys
import os
import sqlalchemy
import argparse
import re
import shutil
# import warnings
# import pprint as pp


_SCRIPT_NAME = "ukbb-build-data-dict"
"""The name of the script (`str`)
"""

_DESC = __doc__
"""The program description (`str`)
"""


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main():
    """The main entry point for the script. For API use see
    ``ukbb_tools.data_dict.build.build_data_dict``.
    """
    # Initialise and parse the command line arguments
    parser = _init_cmd_args()
    args = _parse_cmd_args(parser)

    logger = log.init_logger(
        _SCRIPT_NAME, verbose=args.verbose
    )
    log.log_prog_name(logger, pkg_name, __version__)
    log.log_args(logger, args)

    # Make sure any previous files are wiped
    open(args.outdb, 'w').close()

    # Build an SQLalchemy connection URL
    try:
        # sqlalchemy < v1.4
        connection_url = sqlalchemy.engine.url.URL(
            'sqlite', database=str(args.outdb)
        )
    except TypeError:
        # sqlalchemy >= v1.4
        connection_url = sqlalchemy.URL.create(
            'sqlite', database=str(args.outdb)
        )

    # create the database engine, note that this does not connect until
    # it is asked to do something, so we use sqlalchemy_utils to enforce
    # the must_exist
    engine = sqlalchemy.create_engine(connection_url)

    # If we get here then we are good to continue, so we create a session
    sm = sqlalchemy.orm.sessionmaker(bind=engine)
    session = sm()

    try:
        # Initialise the building of the data dict database
        build_data_dict(session, tmpdir=args.tmpdir, verbose=args.verbose)
    except (BrokenPipeError, KeyboardInterrupt):
        # Python flushes standard streams on exit; redirect remaining
        # output to devnull to avoid another BrokenPipeError at shutdown
        devnull = os.open(os.devnull, os.O_WRONLY)
        os.dup2(devnull, sys.stdout.fileno())
        log.log_interrupt(logger)
    finally:
        session.close()
        log.log_end(logger)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _init_cmd_args():
    """Initialise the command line arguments and return the parsed arguments.

    Returns
    -------
    parser : `argparse.ArgumentParser`
        The argument parser with the arguments set up.
    """
    parser = argparse.ArgumentParser(
        description=_DESC
    )
    parser.add_argument(
        'outdb', type=str,
        help="The path to output the SQLite database"
    )
    cmd_args.add_tmpdir_arg(parser)
    cmd_args.add_verbose_arg(parser)
    return parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _parse_cmd_args(parser):
    """Parse the command line arguments

    Parameters
    ----------
    parser : `argparse.ArgumentParser`
        The argument parser with the arguments set up.

    Returns
    -------
    args : `argparse.Namespace`
        The argparse namespace object.
    """
    args = parser.parse_args()

    # Make sure ~/ etc... is expanded
    for i in ['outdb']:
        setattr(args, i, pau.get_full_path(getattr(args, i)))

    return args


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def build_data_dict(session, tmpdir=None, create_tables=True, verbose=False):
    """Build a copy of the UK Biobank data dictionary database.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        An SQLAlchemy session object. If ``create_tables`` is ``True``, then
        this should return the engine when ``Session.get_bind()`` is called.
    tmpdir : `str`, optional, default: `NoneType`
        A temp directory to use during file downloading. If not supplied then
        the system default temp location is used.
    create_tables : `bool`, optional, default: `True`
        Create all the tables in the database.
    verbose : `bool` or `int`, optional, default: `False`
        Log progress, a values > 1, will turn on progress monitoring.
    """
    logger = log.retrieve_logger(_SCRIPT_NAME, verbose=verbose)
    prog_verbose = log.progress_verbose(verbose=verbose)

    if create_tables is True:
        logger.info("creating tables")
        dd.create_all_tables(session)

    down_dir = get_download_dir()
    logger.info("downloading data dictionary tables")
    parser_obj = download_tables(get_download_dir(), verbose=prog_verbose)

    # We first build the codings table
    logger.info("building coding tables")
    _build_coding_lookup(
        session, parser_obj[parsers.ENCODING_FILE[1]], verbose=prog_verbose
    )
    _build_coding_enum(session, parser_obj, verbose=prog_verbose)

    logger.info("building data path tables")
    parent_to_child, child_to_parent, category_lookup, missing_categories = \
        _build_categories(
            session, parser_obj, verbose=prog_verbose
        )
    logger.info(f"categories missing from lookup: {len(missing_categories)}")

    # Now build the dictionary using the codings table that has been built
    logger.info("building field ID tables")
    field_id_lookup = _build_field_ids(
        session, parser_obj, child_to_parent, category_lookup,
        verbose=prog_verbose
    )

    logger.info("building field ID link tables")
    missing_ids = _build_field_id_link(
        session, parser_obj[parsers.DATA_FIELD_LINK_FILE[1]],
        field_id_lookup, verbose=prog_verbose
    )
    logger.info(f"fields missing from lookup: {len(missing_ids)}")

    # Remove the download directory when finished
    shutil.rmtree(down_dir)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_download_dir():
    """Download all the data dictionary tables (if needed). The tables are
    downloaded into the package directory.

    Returns
    -------
    download_dir : `str`
        The location to use to download the data dictionary files.
    """
    package_dir = common.get_package_dir()
    down_dir = os.path.join(package_dir, con.DOWNLOAD_DIR_NAME)
    os.makedirs(down_dir, exist_ok=True)
    return down_dir


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def download_tables(download_dir, verbose=False, tmpdir=None):
    """Download all the data dictionary tables (if needed). The tables are
    downloaded into the package directory.

    download_dir : `str`
        The location to download the data dictionary files.
    verbose : `bool`, optional, default: `False`
        Turn on progress monitoring.
    verbose : `str`, optional, default: `NoneType`
        A temp directory location to use when downloading the data dictionary
        tables.

    Returns
    -------
    parser_obj : `dict`
        All the parser objects for the downloaded tables. The keys are parser
        file basenames and the values are parser objects.
    """
    parser_obj = {}
    for idx, name, parser in parsers.USED_DATA_DICT_FILES:
        p = parser(idx, name, download_dir, tmpdir=tmpdir)
        p.get_file(verbose=verbose)
        parser_obj[name] = p
    return parser_obj


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _build_coding_lookup(session, parser, verbose=False):
    """Build the database codings table.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        A session object to interact with the database.
    parser : `ukbb_tools.data_dict.parsers.EncodingLookupParser`
        A parser object for the encoding lookup file.
    verbose : `bool`, optional, default: `False`
        Turn on progress monitoring.
    """
    tqdm_kwargs = dict(
        desc="[info] building coding ID lookup table",
        leave=False,
        unit=" row(s)",
        disable=not verbose
    )
    with parser as p:
        for row in tqdm(p, **tqdm_kwargs):
            row = p.format_dict_row(row)
            # pp.pprint(row)
            session.add(
                dd.CodingId(
                    coding_id=row[col.ENCODING_ID.name],
                    title=row[col.TITLE.name],
                    uctitle=_format_enum_name(row[col.TITLE.name]),
                    availability=row[col.AVAILABILITY.name],
                    data_type=row[col.CODED_AS.name],
                    structure=row[col.STRUCTURE.name],
                    num_members=row[col.NUM_MEMBERS.name],
                    desc=row[col.DESCRIPTION.name]
                )
            )
    session.commit()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _build_coding_enum(session, parser_obj, verbose=False):
    """Build the coding enum table.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        A session object to interact with the database.
    parser_obj : `dict`
        All the parser objects. The keys are parser file basenames and the
        values are parser objects.
    verbose : `bool`, optional, default: `False`
        Turn on progress monitoring.
    """
    _insert_non_hier_values(
        session, parser_obj[parsers.INT_ENCODING_FILE[1]],
        con.INTEGER_TYPE,
        verbose=verbose
    )
    _insert_hier_values(
        session, parser_obj[parsers.HIER_INT_ENCODING_FILE[1]],
        con.INTEGER_TYPE,
        verbose=verbose
    )
    _insert_non_hier_values(
        session, parser_obj[parsers.FLOAT_ENCODING_FILE[1]],
        con.FLOAT_TYPE,
        verbose=verbose
    )
    _insert_non_hier_values(
        session, parser_obj[parsers.DATE_ENCODING_FILE[1]],
        con.DATE_TYPE,
        verbose=verbose
    )
    _insert_non_hier_values(
        session, parser_obj[parsers.TIME_ENCODING_FILE[1]],
        con.TIME_TYPE,
        verbose=verbose
    )
    _insert_non_hier_values(
        session, parser_obj[parsers.STR_ENCODING_FILE[1]],
        con.STRING_TYPE,
        verbose=verbose
    )
    _insert_hier_values(
        session, parser_obj[parsers.HIER_STR_ENCODING_FILE[1]],
        con.STRING_TYPE,
        verbose=verbose
    )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _insert_non_hier_values(session, parser, data_type, verbose=False):
    """Add codings (encodings) in a non hierarchical file to the coding_ids
    table.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        A session object to interact with the database.
    parser : `ukbb_tools.data_dict.parsers.EncodingValueParser`
        A parser object for the encoding file.
    data_type : `str`
        The data type represented by the file/parser.
    verbose : `bool`, optional, default: `False`
        Turn on progress monitoring.

    Notes
    -----
    These are given the default, code_id/parent_code_id value of 0.
    """
    tqdm_kwargs = dict(
        desc=f"[info] Insert coding enum data: {parser.basename}",
        leave=False,
        unit=" row(s)",
        disable=not verbose
    )
    with parser as p:
        for row in tqdm(p, **tqdm_kwargs):
            row = p.format_dict_row(row)
            # pp.pprint(row)
            session.add(
                dd.CodingEnum(
                    coding_id=row[col.ENCODING_ID.name],
                    enum_value=row[col.VALUE.name],
                    enum_name=_format_enum_name(row[col.MEANING.name]),
                    enum_desc=row[col.MEANING.name],
                    data_type=data_type
                )
            )
    session.commit()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _insert_hier_values(session, parser, data_type, verbose=False):
    """Add codings (encodings) in a hierarchical file to the coding_ids
    table.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        A session object to interact with the database.
    parser : `ukbb_tools.data_dict.parsers.HierEncodingValueParser`
        A parser object for the encoding file.
    data_type : `str`
        The data type represented by the file/parser.
    verbose : `bool`, optional, default: `False`
        Turn on progress monitoring.

    Notes
    -----
    These are given the default, code_id/parent_code_id value in the file.
    """
    tqdm_kwargs = dict(
        desc=f"[info] Insert hierarchical coding enum data: {parser.basename}",
        leave=False,
        unit=" row(s)",
        disable=not verbose
    )
    with parser as p:
        for row in tqdm(p, **tqdm_kwargs):
            row = p.format_dict_row(row)
            # pp.pprint(row)
            session.add(
                dd.CodingEnum(
                    coding_id=row[col.ENCODING_ID.name],
                    code_id=row[col.CODE_ID.name],
                    parent_code_id=row[col.PARENT_CODE_ID.name],
                    enum_value=row[col.VALUE.name],
                    enum_name=_format_enum_name(row[col.MEANING.name]),
                    enum_desc=row[col.MEANING.name],
                    data_type=data_type
                )
            )
    session.commit()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _build_categories(session, parser_obj, verbose=False):
    """Build the database codings table.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        A session object to interact with the database.
    parser_obj : `dict`
        All the parser objects. The keys are parser file basenames and the
        values are parser objects.
    verbose : `bool`, optional, default: `False`
        Turn on progress monitoring.

    Returns
    -------
    parent_to_child
        The keys are parent category IDs and the values are lists of child
        category IDs.
    child_to_parent : `dict`
        The keys are child category IDs and the values are lists of parent
        category IDs.
    category_lookup : `set` of `int`
        A set of known category IDs.
    missing_categories : `set` of `int`
        A set of category IDs that are in the browse/relationships table but
        not in the category lookup table.
    """
    category_lookup = set()
    parser = parser_obj[parsers.CATEGORY_FILE[1]]
    tqdm_kwargs = dict(
        desc=f"[info] Insert category data: {parser.basename}",
        leave=False,
        unit=" row(s)",
        disable=not verbose
    )
    with parser as p:
        for row in tqdm(p, **tqdm_kwargs):
            row = p.format_dict_row(row)
            # pp.pprint(row)
            session.add(
                dd.DataPath(
                    data_path_id=row[col.CATEGORY_ID.name],
                    data_path_name=row[col.NAME.name],
                    availability=row[col.AVAILABILITY.name],
                    group_type=row[col.GROUP_TYPE.name],
                    desc=_remove_html_tags(row[col.DESCRIPTION.name]),
                    notes=_remove_html_tags(row[col.NOTES.name])
                )
            )
            category_lookup.add(row[col.CATEGORY_ID.name])
    session.commit()

    # Now load the relationships
    parser = parser_obj[parsers.CATEGORY_BROWSE_FILE[1]]
    tqdm_kwargs = dict(
        desc=f"[info] Insert category browse data: {parser.basename}",
        leave=False,
        unit=" row(s)",
        disable=not verbose
    )

    parent_to_child = {}
    child_to_parent = {}
    missing_categories = set()
    with parser as p:
        for row in tqdm(p, **tqdm_kwargs):
            row = p.format_dict_row(row)
            to_continue = False
            # pp.pprint(row)

            if row[col.PARENT_CODE_ID.name] not in category_lookup:
                missing_categories.add(row[col.PARENT_CODE_ID.name])
                to_continue = True

            if row[col.CHILD_CODE_ID.name] not in category_lookup:
                missing_categories.add(row[col.CHILD_CODE_ID.name])
                to_continue = True

            if to_continue is True:
                continue

            session.add(
                dd.DataPathRel(
                    parent_data_path_id=row[col.PARENT_CODE_ID.name],
                    child_data_path_id=row[col.CHILD_CODE_ID.name],
                )
            )

            try:
                parent_to_child[row[col.PARENT_CODE_ID.name]].append(
                    row[col.CHILD_CODE_ID.name]
                )
            except KeyError:
                parent_to_child[row[col.PARENT_CODE_ID.name]] = [
                    row[col.CHILD_CODE_ID.name]
                ]

            try:
                child_to_parent[row[col.CHILD_CODE_ID.name]].append(
                    row[col.PARENT_CODE_ID.name]
                )
            except KeyError:
                child_to_parent[row[col.CHILD_CODE_ID.name]] = [
                    row[col.PARENT_CODE_ID.name]
                ]

    session.commit()
    return (
        parent_to_child, child_to_parent, category_lookup, missing_categories
    )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _format_enum_name(enum_desc):
    """Format the enum description into an enum value.

    Parameters
    ----------
    enum_desc : `str`
        The enumeration name from the codings file.

    Returns
    -------
    enum_desc : `str`
        The enumeration value that has been formatted as described in the notes.

    Notes
    -----
    This is the name with all the non-word characters replaced with an _ and
    made upper case. Additionally bracketed examples are removed. Any missing
    `NaN` values are replaced with a text ``NO_VALUE``.
    """
    try:
        # Pandas imports mixed types and some are nans
        if np.isnan(enum_desc):
            return con.NO_VALUE
    except TypeError:
        pass

    enum_name = enum_desc.strip()

    # remove e.g. brackets
    enum_name = re.sub(r'\(e\.g\..+?\)', '_', enum_name)

    # non word characters and spaced are replaced with _
    enum_name = re.sub(r'[\W ]+', '_', enum_name).upper()

    # Strip any artefactual leading/lagging _
    enum_name = enum_name.strip('_')

    # Remove any multiple _
    enum_name = re.sub(r'_{2, }', '_', enum_name)

    return enum_name


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _remove_html_tags(text):
    """Remove HTML tags found in Notes fields. This removes start/end tags of:
    <br>, <ul>, <p>, <li>, <b>.

    Parameters
    ----------
    text : `str`
        The text to process.

    Returns
    -------
    processed_text : `str`
        The processed text.
    """
    return re.sub(r'</?(br|ul|p|li|b)>', '', text)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _build_field_ids(session, parser_obj, child_to_parent, category_lookup,
                     verbose=False):
    """Build all the tables releated to the field IDs.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        A session object to interact with the database.
    parser_obj : `dict`
        All the parser objects. The keys are parser file basenames and the
        values are parser objects.
    category_lookup : `set` of `int`
        A set of known category IDs.
    child_to_parent : `dict`
        The keys are child category IDs and the values are lists of parent
        category IDs.
    verbose : `bool`, optional, default: `False`
        Turn on progress monitoring.

    Returns
    -------
    field_id_lookup : `set` of `int`
        All the field IDs stored in the field_id lookup table. These can be used
        as a reference for pre-empting integrity errors.

    Notes
    -----
    This will load up mappings between fields and date fields from the previous
    data dictionary iteration. These are matched on field ID/field text. I will
    eventually add data on the field link table although it does not appear as
    comprehensive.
    """
    parser = parser_obj[parsers.FIELD_FILE[1]]
    tqdm_kwargs = dict(
        desc=f"[info] Insert field data: {parser.basename}",
        leave=False,
        unit=" row(s)",
        disable=not verbose
    )

    # Get the stored date mapping data
    date_map = ex.get_data("date_map")

    field_id_lookup = set()
    with parser as p:
        for row in tqdm(p, **tqdm_kwargs):
            row = p.format_dict_row(row)

            if row[col.MAIN_CATEGORY.name] not in category_lookup:
                row[col.MAIN_CATEGORY.name] = 0

            tree = _get_data_path(
                row[col.MAIN_CATEGORY.name],
                child_to_parent
            )

            field_text = _format_enum_name(row[col.TITLE.name])
            try:
                date_field = date_map[(row[col.FIELD_ID.name], field_text)]
            except KeyError:
                date_field = con.ATTEND_ASSESSMENT

            session.add(
                dd.FieldId(
                    field_id=row[col.FIELD_ID.name],
                    field_desc=row[col.TITLE.name],
                    field_text=field_text,
                    date_field_id=date_field,
                    n_instance=row[col.INSTANCE_MAX.name],
                    array=row[col.ARRAY_MAX.name],
                    coding_id=row[col.ENCODING_ID.name],
                    data_type=row[col.VALUE_TYPE.name],
                    data_unit=row[col.UNITS.name],
                    stability=row[col.STABILITY.name],
                    item_type=row[col.ITEM_TYPE.name],
                    strata=row[col.STRATA.name],
                    sexed=row[col.SEXED.name],
                    root_data_path_id=tree[0],
                    deepest_data_path_id=row[col.MAIN_CATEGORY.name],
                    data_path_level=len(tree),
                    data_path_str='.'.join([str(i) for i in tree]),
                    n_sample=row[col.N_PARTICIPANTS.name],
                    n_item=row[col.ITEM_COUNT.name],
                    note=_remove_html_tags(row[col.NOTES.name]),
                    url=con.FIELD_URL.format(row[col.FIELD_ID.name])
                )
            )
            field_id_lookup.add(row[col.FIELD_ID.name])
        session.commit()
    return field_id_lookup


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _get_data_path(start, child_to_parent):
    """Get the data path hierarchy for the field. In terms of the UKB files,
    this would be the category hierarchy.

    Parameters
    ----------
    start : `int`
        The deepest category for the field.
    child_to_parent : `dict`
        The keys are child category IDs and the values are lists of parent
        category IDs.

    Returns
    -------
    tree : `list` of `int`
        The category hierarchy. The zero element is the root and the -1 element
        is the `start` category ID.

    Raises
    ------
    IndexError
        If any of the child terms have > 1 parent (I have not seen this yet,
        it is more of a sanity check).
    """
    tree = [start]
    while True:
        old_start = start
        try:
            if len(child_to_parent[start]) > 1:
                raise IndexError(f"Lots of parents: {start}")
            start = child_to_parent[start][0]
        except KeyError:
            break

        if old_start == start:
            break
        tree.append(start)
    tree.reverse()
    return tree


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _build_field_id_link(session, parser, field_id_lookup, verbose=False):
    """Build all the tables releated to the field IDs.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        A session object to interact with the database.
    parser : `ukbb_tools.data_dict.parsers.FieldIdLinkParser`
        A parser object for the `data_field_linkage` file.
    field_id_lookup : `set`, of `int`
        Field IDs known in the field ID lookup table.
    verbose : `bool`, optional, default: `False`
        Turn on progress monitoring.

    Returns
    -------
    missing_ids : `set` of `int`
        Field IDs in the link tables that are not in the lookup tables.
    """
    missing_ids = set()

    tqdm_kwargs = dict(
        desc=f"[info] Insert field link data: {parser.basename}",
        leave=False,
        unit=" row(s)",
        disable=not verbose
    )
    with parser as p:
        for row in tqdm(p, **tqdm_kwargs):
            row = p.format_dict_row(row)
            to_continue = False
            if row[col.FIELD_ID_LOW.name] not in field_id_lookup:
                missing_ids.add(row[col.FIELD_ID_LOW.name])
                to_continue = True
            if row[col.FIELD_ID_HIGH.name] not in field_id_lookup:
                missing_ids.add(row[col.FIELD_ID_HIGH.name])
                to_continue = True

            if to_continue is True:
                continue

            session.add(
                dd.FieldIdLink(
                    field_id1=row[col.FIELD_ID_LOW.name],
                    field_id2=row[col.FIELD_ID_HIGH.name],
                    link_desc=row[col.LINK_DESCRIPTION.name],
                )
            )
        session.commit()
    return missing_ids
