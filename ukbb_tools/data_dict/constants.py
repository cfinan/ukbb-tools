"""Constants for the UK BioBank data dictionary.
"""
from enum import Enum
from collections import namedtuple


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# Constants used in the building of the NEW data dictionary
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

DOWNLOAD_DIR_NAME = "ukbb_data_dict"
"""The name of the download directory for the data dictionary files (`str`)
"""
FIELD_URL = 'http://biobank.ndph.ox.ac.uk/ukb/field.cgi?id={0}'
"""The default URL for looking up a field ID in UKB, note this is a format
string, so use with ``FIELD_URL.format(text)`` (`str`).
"""

# Sane names for the data types (some also used in the old DD)
NO_ENCODING_TYPE = 'none'
"""Name for the integer data type (`str`)
"""
COMMA_DELIMITED_TYPE = 'comma_delim'
"""Name for the integer data type (`str`)
"""
INTEGER_TYPE = 'int'
"""Name for the integer data type (`str`)
"""
FLOAT_TYPE = 'float'
"""Name for the integer data type (`str`)
"""
ENUM_SINGLE_TYPE = 'enum_single'
"""Name for the enumeration data type (`str`)
"""
ENUM_MULTIPLE_TYPE = 'enum_multiple'
"""Name for the enumeration data type (`str`)
"""
DATE_TYPE = 'date'
"""Name for the date data type (`str`)
"""
TIME_TYPE = 'time'
"""Name for the time data type (`str`)
"""
STRING_TYPE = 'str'
"""Name for the string data type (`str`)
"""

NEW_DATA_TYPE_MAP = {
    "0": NO_ENCODING_TYPE,
    "101": COMMA_DELIMITED_TYPE,
    "11": INTEGER_TYPE,
    "21": ENUM_SINGLE_TYPE,
    "22": ENUM_MULTIPLE_TYPE,
    "31": FLOAT_TYPE,
    "41": STRING_TYPE,
    "51": DATE_TYPE,
    "61": TIME_TYPE
}
"""Mappings between the new data dictionary data type values any my
re-definitions that were also used in the old database (`dict`)
"""

DATA_ITEM_TYPE = "data"
"""The name of the data item type (as found in the old database) (`str`)
"""
SAMPLES_ITEM_TYPE = "samples"
"""The name of the samples item type (as found in the old database) (`str`)
"""
BULK_ITEM_TYPE = "bulk"
"""The name of the bulk item type (as found in the old database) (`str`)
"""
RECORDS_ITEM_TYPE = "records"
"""The name of the records item type (as found in the old database) (`str`)
"""

ITEM_TYPE_MAP = {
    "0": DATA_ITEM_TYPE,
    "10": SAMPLES_ITEM_TYPE,
    "20": BULK_ITEM_TYPE,
    "30": RECORDS_ITEM_TYPE,
}
"""Mappings between the new data dictionary item type values any my
re-definitions that were also used in the old database (`dict`)
"""

PRIMARY_STRATA = "primary"
"""The name of the primary strata (as found in the old database) (`str`)
"""
AUX_STRATA = "auxillary"
"""The name of the auxillary strata (as found in the old database) (`str`)
"""
DERIVED_STRATA = "derived"
"""The name of the derived strata (as found in the old database) (`str`)
"""

STRATA_MAP = {
    "0": PRIMARY_STRATA,
    "2": AUX_STRATA,
    "3": DERIVED_STRATA,
}
"""Mappings between the new data dictionary strata values any my
re-definitions that were also used in the old database (`dict`)
"""


FILE_DELIMITER = "\t"
"""The delimiter for the codings/data dict file (`str`)
"""


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# Constants used in the building of the OLD data dictionary
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

NO_VALUE = "NO_VALUE"
"""The name for an enumeration that has no value (`str`)
"""
CODING_ID = 'coding_id'
"""The name for the coding ID field (`str`)
"""
ENUM_VALUE = 'enum_value'
"""The name for the coding ID enumeration value field (`str`)
"""
DESCRIPTION = 'desc'
"""The name for the coding ID description field (`str`)
"""
DATE_FIELD_ID = 'date_field_id'
"""The name for the data field ID field (`str`)
"""
CODING_FILE_COLUMNS = (
    ('Coding', 'Value', 'Meaning'),
    (CODING_ID, ENUM_VALUE, DESCRIPTION)
)
"""The column names for the codings file and their column names in the
 database. The first nested tuple are the column names in the codings
 file and the second one is the column names in the database
 (`tuple` of (`tuple`, `tuple`)).
"""

FIELD_ID_COLUMNS = (
    ('Path', 'Category', 'FieldID', 'Field', 'Participants', 'Items',
     'Stability', 'ValueType', 'Units', 'ItemType', 'Strata', 'Sexed',
     'Instances', 'Array', 'Coding', 'Notes', 'Link'),
    ('data_path', 'category', 'field_id', 'field_desc', 'n_sample', 'n_item',
     'stability', 'data_type', 'data_unit', 'item_type', 'strata', 'sexed',
     'n_instance', 'array', 'coding_id', 'note', 'url')
)
"""The column names for the field ID file (Data_Dictionary_Showcase) and
 their column names in the database. The first nested tuple are the column
 names in the codings file and the second one is the column names in the
 database (`tuple` of (`tuple`, `tuple`)).
"""

# Sane names for the data types
INTEGER_TYPE = 'int'
"""Name for the integer data type (`str`)
"""
FLOAT_TYPE = 'float'
"""Name for the integer data type (`str`)
"""
ENUM_SINGLE_TYPE = 'enum_single'
"""Name for the enumeration data type (`str`)
"""
ENUM_MULTIPLE_TYPE = 'enum_multiple'
"""Name for the enumeration data type (`str`)
"""
DATE_TYPE = 'date'
"""Name for the date data type (`str`)
"""
TIME_TYPE = 'time'
"""Name for the time data type (`str`)
"""
STRING_TYPE = 'str'
"""Name for the string data type (`str`)
"""

DATA_TYPE_MAP = {
    'Categorical single': ENUM_SINGLE_TYPE,
    'Categorical multiple': ENUM_MULTIPLE_TYPE,
    'Date': DATE_TYPE,
    'Time': TIME_TYPE,
    'Compound': STRING_TYPE,
    'Integer': INTEGER_TYPE,
    'Continuous': FLOAT_TYPE,
    'Text': STRING_TYPE
}
"""Mappings between the sane data type names and the UK BioBank data type
 names (`dict`).
"""

DATE_FIELDS = ['Date', 'Time']
"""Date and time field names (`list` of `str`)
"""
BLOOD_SAMPLE_TIME_FIELD = 3166
"""The field ID containing the date when a blood sample was taken (`int`)
"""
URINE_SAMPLE_TIME_FIELD = 20035
"""The field ID containing the date when a urine sample was taken (`int`)
"""
SALIVA_SAMPLE_TIME_FIELD = 20025
"""The field ID containing the date when a saliva sample was taken (`int`)
"""
CANCER_DIAGNOSIS_DATE = 40005
"""The field ID containing the date when a cancer diagnosis was made (`int`)
"""
ATTEND_ASSESSMENT = 53
"""The field ID number for the date someone attended the
 assessment centre (`int`)
"""
DATE_CONSENT = 200
"""The field ID containing the date when consent was obtained (`int`)
"""
DATE_OF_DEATH = 40000
"""The field ID containing the date of death (`int`)
"""


DATE_MAPPINGS_BY_DATA_PATH = {
    ('Additional exposures > Local environment > '
     'Greenspace and coastal proximity'): 22700,
    ('Additional exposures > Local environment'
     '> Home locations'): 22700,
    ('Additional exposures > Local environment >'
     ' Residential air pollution'): 22700,
    ('Additional exposures > Local environment >'
     ' Residential noise pollution'): 22700,
    ('Additional exposures > '
     'Physical activity measurement'): 110006,
    ('Additional exposures > Physical activity measurement'
     ' > Acceleration averages'): 90010,
    ('Additional exposures > Physical activity measurement'
     ' > Acceleration intensity distribution'): 90010,
    ('Additional exposures > Physical activity measurement'
     ' > Accelerometer calibration'): 90010,
    ('Additional exposures > Physical activity measurement'
     ' > Accelerometer wear time duration'): 90010,
    ('Additional exposures > Physical activity measurement'
     ' > Raw accelerometer statistics'): 90010,
    ('Biological samples > Assay results > Blood assays'
     ' > Blood biochemistry'): BLOOD_SAMPLE_TIME_FIELD,
    ('Biological samples > Assay results > Blood assays'
     ' > Blood count'): BLOOD_SAMPLE_TIME_FIELD,
    ('Biological samples > Sample inventory > '
     'Blood sample inventory'): BLOOD_SAMPLE_TIME_FIELD,
    ('Biological samples > Sample inventory > '
     'Saliva sample inventory'): SALIVA_SAMPLE_TIME_FIELD,
    ('Biological samples > Sample inventory > '
     'Urine sample inventory'): URINE_SAMPLE_TIME_FIELD,
    ('Online follow-up > Diet by 24-hour recall'): 110002,
    ('Online follow-up > Digestive health'): 21023,
    ('Online follow-up > Food (and other) preferences'): 20750,
    ('Online follow-up > Work environment'): 22500,
    ('Online follow-up > Work environment > Employment history'): 22500,
    ('Online follow-up > Mental health'): 20400,
    ('Online follow-up > Work environment'):  22500,
    ('Biological samples > Assay results > Blood assays > '
     'Infectious Diseases'): BLOOD_SAMPLE_TIME_FIELD,
    ('Biological samples > Assay results > Blood assays > Infectious '
     'Diseases > Infectious Disease Antigens'): BLOOD_SAMPLE_TIME_FIELD,
    "Online follow-up > Cognitive function online": 20140,
    ("Online follow-up > Cognitive function online > "
     "Fluid intelligence / reasoning"): 20135,
    "Online follow-up > Cognitive function online > Mood": 23079,
    "Online follow-up > Cognitive function online > Numeric memory": 20138,
    "Online follow-up > Cognitive function online > Pairs matching": 20134,
    ("Online follow-up > Cognitive function online > Symbol digit "
     "substitution"): 20137,
    "Online follow-up > Cognitive function online > Trail making": 20136,
    "Online follow-up > Diet by 24-hour recall": 110002,
    "Online follow-up > Digestive health": 21023,
    "Online follow-up > Food (and other) preferences": 20750,
    ("Biological samples > Assay results > "
     "Urine assays"): URINE_SAMPLE_TIME_FIELD
}
"""Mappings between specific data paths to predefined date field IDs. The data
 paths are not necessarily whole data paths but will match on the left edge.
 The keys are the data path strings and the values are date field ID
 integers (`dict`)
"""

DATE_MAPPINGS_UNDER_DATA_PATH = {
    'Online follow-up > Diet by 24-hour recall': 105010,
    'Online follow-up > Mental health': 20400,
    'Online follow-up > Work environment':  22500,
    'Population characteristics > Baseline characteristics': ATTEND_ASSESSMENT,
    'Assessment centre': ATTEND_ASSESSMENT
}
"""Mappings between specific data paths to predefined date field IDs. The data
 paths are not whole data paths but will match on the left edge. The keys are
 the data path strings and the values are date field ID integers (`dict`)
"""


# These are manual hardcoded relevant date mappings that could not be easily
# automated
MANUAL_MAPPINGS = {
    0: 191,
    # Commended out as 23049 is a categorical single variable, not date/time
    # 23048: 23049,
    40006: CANCER_DIAGNOSIS_DATE,
    40008: CANCER_DIAGNOSIS_DATE,
    40009: CANCER_DIAGNOSIS_DATE,
    40011: CANCER_DIAGNOSIS_DATE,
    40013: CANCER_DIAGNOSIS_DATE,
    40019: CANCER_DIAGNOSIS_DATE,
    40021: CANCER_DIAGNOSIS_DATE,
    40001: DATE_OF_DEATH,
    40002: DATE_OF_DEATH,
    40007: DATE_OF_DEATH,
    40010: DATE_OF_DEATH,
    40018: DATE_OF_DEATH,
    40020: DATE_OF_DEATH,
    30031: 30032,
    30033: 30032,
    30034: 30032,
    30051: 30052,
    30053: 30052,
    30054: 30052,
    30061: 30062,
    30063: 30062,
    30064: 30062,
}


FieldId = namedtuple(
    'FieldId',
    ['field_id', 'field_text', 'field_desc', 'data_type', 'data_units',
     'coding_id', 'date_field_id', 'coding_enum']
)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class VITAL_STATS_FID(Enum):
    """A collection of vital statistic field IDs
    """
    SAMPLE_WITHDRAWN = 0
    AGE_AT_ASSESSMENT = 21003
    ASSESSMENT_DATE = 53
    BLOOD_SAMPLE_TIME = 3166
    CANCER_DIAGNOSIS_DATE = 40005
    DATE_CONSENT = 200
    DATE_OF_DEATH = 40000
    EID = 'eid'
    ETHNICITY_SR = 21000
    GEN_SEX = 22001
    IS_CAUCASIAN = 22006
    MONTH_OF_BIRTH = 52
    SALIVA_SAMPLE_TIME = 20025
    SEX = 31
    URINE_SAMPLE_TIME = 20035
    YEAR_OF_BIRTH = 34
    GENOTYPING_BATCH = 22000
