"""Column definitions in various UK Biobank data dictionary files.
"""
from ukbb_tools import (
    type_casts as tc,
    columns
)
from ukbb_tools.data_dict import (
    type_casts as ddtc,
)

# Use main package named tuple
ColName = columns.ColName
"""A column name named tuple derived from the main package (`namedtuple`).
"""
_MISSING_VALUES = columns._MISSING_VALUES
"""Missing values that will apply to most contexts (`list`)
"""


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# Used in the encoding dictionary file
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
ENCODING_ID = ColName(
    "encoding_id",
    int,
    _MISSING_VALUES,
    "The encoding ID in the UK Biobank data dictionary. Encoding IDs represent"
    " how a particular data field is encoded. This was called coding_id in the"
    " original files and the built database."
)
"""The encoding ID column for UK Biobank data dictionary files
(`ukbb_tools.columns.ColName`)
"""
TITLE = ColName(
    "title",
    str,
    _MISSING_VALUES,
    "The title column for UK Biobank data dictionary files."
)
"""The title column for UK Biobank data dictionary files
(`ukbb_tools.columns.ColName`)
"""
AVAILABILITY = ColName(
    "availability",
    int,
    _MISSING_VALUES,
    "The availability column for UK Biobank data dictionary files."
)
"""The availability column for UK Biobank data dictionary files
(`ukbb_tools.columns.ColName`)
"""
CODED_AS = ColName(
    "coded_as",
    ddtc.parse_data_type,
    _MISSING_VALUES,
    "The coded as (data type) column for UK Biobank data dictionary files."
)
"""The coded as (data type) column for UK Biobank data dictionary files
(`ukbb_tools.columns.ColName`)
"""
STRUCTURE = ColName(
    "structure",
    int,
    _MISSING_VALUES,
    "The structure column for UK Biobank data dictionary files."
)
"""The structure column for UK Biobank data dictionary files
(`ukbb_tools.columns.ColName`)
"""
NUM_MEMBERS = ColName(
    "num_members",
    int,
    _MISSING_VALUES,
    "The number of members column for UK Biobank data dictionary files."
)
"""The number of members column for UK Biobank data dictionary files
(`ukbb_tools.columns.ColName`)
"""
DESCRIPTION = ColName(
    "descript",
    str,
    _MISSING_VALUES,
    "The description column for UK Biobank data dictionary files."
)
"""The description column for UK Biobank data dictionary files
(`ukbb_tools.columns.ColName`)
"""


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# Used in the hierarchical encoding value files
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
CODE_ID = ColName(
    "code_id",
    int,
    _MISSING_VALUES,
    "The code ID column for UK Biobank data dictionary files."
)
"""The code ID column for UK Biobank data dictionary files
(`ukbb_tools.columns.ColName`)
"""
PARENT_CODE_ID = ColName(
    "parent_id",
    int,
    _MISSING_VALUES,
    "The parent code ID column for UK Biobank data dictionary files."
)
"""The parent code ID column for UK Biobank data dictionary files
(`ukbb_tools.columns.ColName`)
"""
SELECTABLE = ColName(
    "selectable",
    tc.parse_bool,
    _MISSING_VALUES,
    "The is selectable column for UK Biobank data dictionary files."
)
"""The is selectable column for UK Biobank data dictionary files
(`ukbb_tools.columns.ColName`)
"""


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# Used in the hierarchical/non-hierarchical encoding value files
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
VALUE = ColName(
    "value",
    str,
    _MISSING_VALUES,
    "The value column for UK Biobank data dictionary files."
)
"""The value column for UK Biobank data dictionary files
(`ukbb_tools.columns.ColName`)
"""
MEANING = ColName(
    "meaning",
    str,
    _MISSING_VALUES,
    "The column name for a data value meaning."
)
"""The meaning column for UK Biobank data dictionary files
(`ukbb_tools.columns.ColName`)
"""
SHOWCASE_ORDER = ColName(
    "showcase_order",
    float,
    _MISSING_VALUES,
    "The column name for showcase order, I think this controls ordering of "
    "data on the UKB showcase website."
)
"""The showcase ordering column for UK Biobank data dictionary files
(`ukbb_tools.columns.ColName`)
"""


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# Used in the category lookup files
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
CATEGORY_ID = ColName(
    "category_id",
    int,
    _MISSING_VALUES,
    "The category identifier column for UK Biobank data dictionary files."
)
"""The category identifier column for UK Biobank data dictionary files
(`ukbb_tools.columns.ColName`)
"""
NAME = ColName(
    "title",
    str,
    _MISSING_VALUES,
    "The category name column for UK Biobank data dictionary files."
)
"""The category name column for UK Biobank data dictionary files
(`ukbb_tools.columns.ColName`)
"""
GROUP_TYPE = ColName(
    "group_type",
    int,
    _MISSING_VALUES,
    "The category group type column for UK Biobank data dictionary files."
)
"""The category group type column for UK Biobank data dictionary files
(`ukbb_tools.columns.ColName`)
"""
NOTES = ColName(
    "notes",
    str,
    _MISSING_VALUES,
    "The notes column for UK Biobank data dictionary files."
)
"""The notes column for UK Biobank data dictionary files
(`ukbb_tools.columns.ColName`)
"""


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# Used in the category browser (hierarchy) files
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
CHILD_CODE_ID = ColName(
    "child_id",
    int,
    _MISSING_VALUES,
    "The child category code ID column for UK Biobank data dictionary files."
)
"""The child category code ID column for UK Biobank data dictionary files
(`ukbb_tools.columns.ColName`)
"""


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# Used in the fields lookup files
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
FIELD_ID = ColName(
    "field_id",
    int,
    _MISSING_VALUES,
    "The field identifier column for UK Biobank data dictionary files."
)
"""The field identifier column for UK Biobank data dictionary files
(`ukbb_tools.columns.ColName`)
"""
STABILITY = ColName(
    "stability",
    int,
    _MISSING_VALUES,
    "The field stability column for UK Biobank data dictionary files."
)
"""The field stability column for UK Biobank data dictionary files
(`ukbb_tools.columns.ColName`)
"""
PRIVATE = ColName(
    "private",
    tc.parse_bool,
    _MISSING_VALUES,
    "The is private column for UK Biobank data dictionary files."
)
"""The is private column for UK Biobank data dictionary files
(`ukbb_tools.columns.ColName`)
"""
VALUE_TYPE = ColName(
    "value_type",
    ddtc.parse_data_type,
    _MISSING_VALUES,
    "The field data value type column for UK Biobank data dictionary files."
)
"""The field data value type column for UK Biobank data dictionary files
(`ukbb_tools.columns.ColName`)
"""
BASE_TYPE = ColName(
    "base_type",
    ddtc.parse_data_type,
    _MISSING_VALUES,
    "The field base data type column for UK Biobank data dictionary files."
)
"""The field base data type column for UK Biobank data dictionary files
(`ukbb_tools.columns.ColName`)
"""
ITEM_TYPE = ColName(
    "item_type",
    ddtc.parse_item_type,
    _MISSING_VALUES,
    "The field item type column for UK Biobank data dictionary files."
)
"""The field item type column for UK Biobank data dictionary files
(`ukbb_tools.columns.ColName`)
"""
STRATA = ColName(
    "strata",
    ddtc.parse_strata,
    _MISSING_VALUES,
    "The field strata column for UK Biobank data dictionary files."
)
"""The field strata column for UK Biobank data dictionary files
(`ukbb_tools.columns.ColName`)
"""
INSTANCED = ColName(
    "instanced",
    int,
    _MISSING_VALUES,
    "."
)
"""The field is instanced column for UK Biobank data dictionary files
(`ukbb_tools.columns.ColName`)
"""
ARRAYED = ColName(
    "arrayed",
    tc.parse_bool,
    _MISSING_VALUES,
    "."
)
"""The field is arrayed column for UK Biobank data dictionary files
(`ukbb_tools.columns.ColName`)
"""
SEXED = ColName(
    "sexed",
    ddtc.parse_sex,
    _MISSING_VALUES,
    "The field is sexed column for UK Biobank data dictionary files."
)
"""The field is sexed column for UK Biobank data dictionary files
(`ukbb_tools.columns.ColName`)
"""
UNITS = ColName(
    "units",
    str,
    _MISSING_VALUES,
    "The field units column for UK Biobank data dictionary files."
)
"""The field units column for UK Biobank data dictionary files
(`ukbb_tools.columns.ColName`)
"""
MAIN_CATEGORY = ColName(
    "main_category",
    int,
    _MISSING_VALUES,
    "The field category column for UK Biobank data dictionary files."
)
"""The field category column for UK Biobank data dictionary files
(`ukbb_tools.columns.ColName`)
"""
INSTANCE_ID = ColName(
    "instance_id",
    int,
    _MISSING_VALUES,
    "The field instance ID column for UK Biobank data dictionary files."
)
"""The field instance ID column for UK Biobank data dictionary files
(`ukbb_tools.columns.ColName`)
"""
INSTANCE_MIN = ColName(
    "instance_min",
    int,
    _MISSING_VALUES,
    "The field minimum instance number column for UK Biobank data "
    "dictionary files."
)
"""The field minimum instance number column for UK Biobank data dictionary
files (`ukbb_tools.columns.ColName`)
"""
INSTANCE_MAX = ColName(
    "instance_max",
    int,
    _MISSING_VALUES,
    "The field maximum instance number column for UK Biobank data dictionary"
    " files."
)
"""The field maximum instance number column for UK Biobank data dictionary
files (`ukbb_tools.columns.ColName`)
"""
ARRAY_MIN = ColName(
    "array_min",
    int,
    _MISSING_VALUES,
    "The field minimum array number column for UK Biobank data dictionary"
    " files."
)
"""The field minimum array number column for UK Biobank data dictionary
files (`ukbb_tools.columns.ColName`)
"""
ARRAY_MAX = ColName(
    "array_max",
    int,
    _MISSING_VALUES,
    "The field maximum array number column for UK Biobank data dictionary"
    " files."
)
"""The field maximum array number column for UK Biobank data dictionary
files (`ukbb_tools.columns.ColName`)
"""
DEBUT = ColName(
    "debut",
    str,
    _MISSING_VALUES,
    "The field debut column for UK Biobank data dictionary files."
)
"""The field debut column for UK Biobank data dictionary files
(`ukbb_tools.columns.ColName`)
"""
VERSION = ColName(
    "version",
    str,
    _MISSING_VALUES,
    "The field version column for UK Biobank data dictionary files."
)
"""The field version column for UK Biobank data dictionary files
(`ukbb_tools.columns.ColName`)
"""
N_PARTICIPANTS = ColName(
    "num_participants",
    int,
    _MISSING_VALUES,
    "The field number of participants column for UK Biobank data dictionary"
    " files."
)
"""The field number of participants column for UK Biobank data dictionary files
(`ukbb_tools.columns.ColName`)
"""
ITEM_COUNT = ColName(
    "item_count",
    int,
    _MISSING_VALUES,
    "The field number of items column for UK Biobank data dictionary files."
)
"""The field number of items column for UK Biobank data dictionary files
(`ukbb_tools.columns.ColName`)
"""
COST_DO = ColName(
    "cost_do",
    int,
    _MISSING_VALUES,
    "The cost DO column for UK Biobank data dictionary files, not sure what "
    "this mean."
)
"""The cost DO column for UK Biobank data dictionary files, not sure what this
means (`ukbb_tools.columns.ColName`)
"""
COST_ON = ColName(
    "cost_on",
    int,
    _MISSING_VALUES,
    "The cost ON column for UK Biobank data dictionary files, not sure what "
    "this mean."
)
"""The cost ON column for UK Biobank data dictionary files, not sure what this
means (`ukbb_tools.columns.ColName`)
"""
COST_SC = ColName(
    "cost_sc",
    int,
    _MISSING_VALUES,
    "The cost SC column for UK Biobank data dictionary files, not sure what "
    "this mean."
)
"""The cost SC column for UK Biobank data dictionary files, not sure what this
means (`ukbb_tools.columns.ColName`)
"""


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# Used in the fields-linkage files
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
FIELD_ID_LOW = ColName(
    "field_lo",
    int,
    _MISSING_VALUES,
    "The lowest ID field being linked column for UK Biobank data dictionary "
    "files."
)
"""The lowest ID field being linked column for UK Biobank data dictionary files
(`ukbb_tools.columns.ColName`)
"""
FIELD_ID_HIGH = ColName(
    "field_hi",
    int,
    _MISSING_VALUES,
    "The highest ID field being linked column for UK Biobank data dictionary."
)
"""The highest ID field being linked column for UK Biobank data dictionary
files (`ukbb_tools.columns.ColName`)
"""
LINK_DESCRIPTION = ColName(
    "description",
    str,
    _MISSING_VALUES,
    "The nature of the link between two fields column for UK Biobank data"
    " dictionary files."
)
"""The nature of the link between two fields column for UK Biobank data
dictionary files (`ukbb_tools.columns.ColName`)
"""
