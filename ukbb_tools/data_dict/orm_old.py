"""Classes to interact with the old data dictionary database. There are some
minor updates to this ORM definition in the new database, but the structure
is largely the same.
"""
from ukbb_tools import common

try:
    # SQLAlchemy > 1.4
    from sqlalchemy.orm import declarative_base
except ModuleNotFoundError:
    # SQLAlchemy <= 1.3
    from sqlalchemy.ext.declarative import declarative_base

from sqlalchemy import (
    Column, Integer, String, Text, ForeignKey
)
from sqlalchemy.orm import relationship

Base = declarative_base()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def create_all_tables(session):
    """Create all the ORM tables.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        A session where ``session.get_bind()`` will return a valid engine.
    """
    # Ensure all the tables are created
    Base.metadata.create_all(
        session.get_bind()
    )


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class CodingId(Base):
    """A representation of objects within the ``coding_ids`` table. This is an
    integrity reference table.

    Parameters
    ----------
    coding_id : `int`
        The UK BioBank coding ID primary key column.
    coding_enums : `ukbb_tools.data_dict.orm.CodingEnum`, optional, \
    default: `NoneType`
        The reference back the the ``coding_emums`` table.
    field_ids : `ukbb_tools.data_dict.orm.FieldId`, optional, \
    default: `NoneType`
        The reference back the the ``field_id`` table.

    Notes
    -----
    coding IDs are the link between fields and their enumerations (possible
    categorical values). Each field ID that has categorical variables should
    have a coding ID. This table is present as sometimes field IDs are assigned
    coding IDs that do not have any corresponding enumerations (and should
    have), so this is used as a validation lookup.
    """
    __tablename__ = 'coding_ids'

    coding_id = Column(
        Integer, nullable=False, primary_key=True,
        doc="The UK BioBank coding ID primary key column."
    )

    # Relationships
    coding_enums = relationship(
        "CodingEnum",
        back_populates='coding_ids',
        doc="The reference back the the ``coding_emums`` table."
    )
    field_ids = relationship(
        "FieldId",
        back_populates='coding_id_lookups',
        doc="The reference back the the ``field_id`` table."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return common.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class CodingEnum(Base):
    """A representation of ``coding_enums`` table. These have the enumerations
    (categories) for all the ``coding_ids``.

    Parameters
    ----------
    coding_enum_id : `int`
        An autoincrement primary key.
    coding_id : `int`, optional, default: `NoneType`
        A foreign key link back to the ``coding_id`` table. This is indexed.
        Foreign key to ``coding_ids.coding_id``.
    enum_value : `str`, optional, default: `NoneType`
        The enumeration code, this will be used in UK BioBank files. This is
        indexed (length 255).
    enum_name : `str`, optional, default: `NoneType`
        A clean name for the enumeration. This is indexed (length 255).
    enum_desc : `str`, optional, default: `NoneType`
        The enumeration name as given by UK BioBank (length text).
    coding_ids : `ukbb_tools.data_dict.orm.CodingId`, optional, \
    default: `NoneType`
        The reference back the the ``coding_id`` table.
    """
    __tablename__ = 'coding_enums'

    coding_enum_id = Column(
        Integer,
        nullable=False,
        primary_key=True,
        autoincrement=True,
        doc="An autoincrement primary key."
    )
    coding_id = Column(
        Integer,
        ForeignKey('coding_ids.coding_id'),
        index=True,
        doc="A foreign key link back to the ``coding_id`` table."
    )
    # I looked at the field and the max string length is 200 so I will add a
    # bit more for good measure
    enum_value = Column(
        String(255), index=True,
        doc="The enumeration code, this will be used in UK BioBank files."
    )
    enum_name = Column(
        String(255), index=True,
        doc="A clean name for the enumeration."
    )
    enum_desc = Column(
        Text,
        doc="The enumeration name as given by UK BioBank."
    )

    # Relationships
    coding_ids = relationship(
        "CodingId",
        back_populates='coding_enums',
        doc="The reference back the the ``coding_id`` table."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return common.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class DataPath(Base):
    """A representation of the ``data_paths`` lookup table.

    Parameters
    ----------
    data_path_id : `int`
        An autoincrement primary key.
    data_path_name : `str`, optional, default: `NoneType`
        The name for a data path as given by UK BioBank. This is indexed
        (length 255).
    field_ids : `ukbb_tools.data_dict.orm.FieldId`, optional, \
    default: `NoneType`
        The reference back the root data path in the ``data_path_rels`` table.
    field_ids : `ukbb_tools.data_dict.orm.FieldId`, optional, \
    default: `NoneType`
        The reference back the deepest data path for a field id in the
        ``data_path_rels`` table.
    data_path_rels : `ukbb_tools.data_dict.orm.DataPathRel`, optional, \
    default: `NoneType`
        The reference back the parent data paths ``data_path_rels`` table.
    data_path_rels : `ukbb_tools.data_dict.orm.DataPathRel`, optional, \
    default: `NoneType`
        The reference back the child data paths ``data_path_rels`` table.

    Notes
    -----
    A data path is a node in the hierarchy of UK BioBank data fields.
    """
    __tablename__ = 'data_paths'

    data_path_id = Column(
        Integer,
        nullable=False,
        primary_key=True,
        autoincrement=True,
        doc="An autoincrement primary key."
    )
    data_path_name = Column(
        String(255),
        index=True,
        doc="The name for a data path as given by UK BioBank."
    )

    # Relationships
    field_id_root_data_paths = relationship(
        'FieldId',
        primaryjoin="DataPath.data_path_id == FieldId.root_data_path_id",
        back_populates='root_data_paths',
        doc="The reference back the root data path in the ``data_path_rels``"
        " table."
    )
    field_id_deepest_data_paths = relationship(
        'FieldId',
        primaryjoin="DataPath.data_path_id == FieldId.deepest_data_path_id",
        back_populates='deepest_data_paths',
        doc="The reference back the deepest data path for a field id in the"
        " ``data_path_rels`` table."
    )
    parent_data_paths = relationship(
        'DataPathRel',
        primaryjoin="DataPath.data_path_id == DataPathRel.parent_data_path_id",
        back_populates='parent_data_paths',
        doc="The reference back the parent data paths ``data_path_rels`` "
        "table."
    )
    child_data_paths = relationship(
        'DataPathRel',
        primaryjoin="DataPath.data_path_id == DataPathRel.child_data_path_id",
        back_populates='child_data_paths',
        doc="The reference back the child data paths ``data_path_rels`` table."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return common.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class DataPathRel(Base):
    """A representation of the ``data_path_rels`` table. This stores the
    relationships between the data fields.

    Parameters
    ----------
    data_path_rel_id : `int`
        An autoincrement primary key.
    parent_data_path_id : `int`, optional, default: `NoneType`
        A foreign key link for the parent data path back to the ``data_paths``
        table. This is indexed. Foreign key to ``data_paths.data_path_id``.
    child_data_path_id : `int`, optional, default: `NoneType`
        A foreign key link for the child data path back to the ``data_paths``
        table. This is indexed. Foreign key to ``data_paths.data_path_id``.
    data_paths : `ukbb_tools.data_dict.orm.DataPath`, optional, \
    default: `NoneType`
        The reference back the parent data path in ``data_paths`` table.
    data_paths : `ukbb_tools.data_dict.orm.DataPath`, optional, \
    default: `NoneType`
        The reference back the child data path in ``data_paths`` table.
    """
    __tablename__ = 'data_path_rels'

    data_path_rel_id = Column(
        Integer,
        nullable=False,
        primary_key=True,
        autoincrement=True,
        doc="An autoincrement primary key."
    )
    parent_data_path_id = Column(
        Integer,
        ForeignKey('data_paths.data_path_id'),
        index=True,
        doc="A foreign key link for the parent data path back to the "
        "``data_paths`` table."
    )
    child_data_path_id = Column(
        Integer,
        ForeignKey('data_paths.data_path_id'),
        index=True,
        doc="A foreign key link for the child data path back to the "
        "``data_paths`` table."
    )

    # Relationships
    parent_data_paths = relationship(
        'DataPath',
        foreign_keys=[parent_data_path_id],
        back_populates='parent_data_paths',
        doc="The reference back the parent data path in ``data_paths`` table."
    )
    child_data_paths = relationship(
        'DataPath',
        foreign_keys=[child_data_path_id],
        back_populates='child_data_paths',
        doc="The reference back the child data path in ``data_paths`` table."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return common.print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class FieldId(Base):
    """A representation of the ``field_ids`` table.

    Parameters
    ----------
    field_id : `int`
        An autoincrement primary key.
    field_desc : `str`, optional, default: `NoneType`
        The field description as given by UK BioBank (length 255).
    field_text : `str`, optional, default: `NoneType`
        A cleaned up field description, with no spaces (length 255).
    date_field_id : `int`, optional, default: `NoneType`
        The UK BioBank ID for the field.
    n_instance : `int`, optional, default: `NoneType`
        The number of instances that should be expected for a field, these can
        be regarded as the number of repeat measures for a field.
    array : `int`, optional, default: `NoneType`
        The number of measures that are required to represent a data point
        entry. in the actual UK BioBank data, the array will be repeated for
        for each instance.
    coding_id : `int`, optional, default: `NoneType`
        The link back to any data codings for the field. . Foreign key to
        ``coding_ids.coding_id``.
    data_type : `str`, optional, default: `NoneType`
        The data type of the field (length 255).
    data_unit : `str`, optional, default: `NoneType`
        The units for the data field (length 255).
    stability : `str`, optional, default: `NoneType`
        The stability of the data field (length 255).
    item_type : `str`, optional, default: `NoneType`
        The type of information that is represented by the data field:
        ``bulk``, ``data``, ``records``, ``samples`` (length 255).
    strata : `str`, optional, default: `NoneType`
        Not sure (length 255).
    sexed : `str`, optional, default: `NoneType`
        Does the field apply to males, female or both (unisex) (length 255).
    root_data_path_id : `int`, optional, default: `NoneType`
        The ID of the root data path that applies to the field. Foreign key to
        ``data_paths.data_path_id``.
    deepest_data_path_id : `int`, optional, default: `NoneType`
        The ID of the deepest data path that applies to the field. Foreign key
        to ``data_paths.data_path_id``.
    data_path_level : `int`, optional, default: `NoneType`
        The level of the field in the data path tree.
    data_path_str : `str`, optional, default: `NoneType`
        A string representation of the data path ID route to the field. This is
        data path IDs concatenated with ``.`` (length 255).
    n_sample : `int`, optional, default: `NoneType`
        The number of samples that are available for a field.
    n_item : `int`, optional, default: `NoneType`
        Not sure.
    note : `str`, optional, default: `NoneType`
        A long form text description of the field (length text).
    url : `str`, optional, default: `NoneType`
        The URL link for the field in the UK BioBank showcase (length 255).
    data_paths : `ukbb_tools.data_dict.orm.DataPath`, optional, \
    default: `NoneType`
        The reference back the root data path in the ``data_paths`` table.
    data_paths : `ukbb_tools.data_dict.orm.DataPath`, optional, \
    default: `NoneType`
        The reference back the deepest data path in the ``data_paths`` table.
    coding_ids : `ukbb_tools.data_dict.orm.CodingId`, optional, \
    default: `NoneType`
        The reference back the ``coding_id`` table.

    Notes
    -----
    The fields are the actual data fields that are available in UK BioBank.
    These are analogous to the fields you see on the UK BioBank showcase.
    """
    __tablename__ = 'field_ids'

    field_id = Column(
        Integer,
        nullable=False,
        primary_key=True,
        doc="An autoincrement primary key."
    )
    field_desc = Column(
        String(255),
        doc="The field description as given by UK BioBank."
    )
    field_text = Column(
        String(255),
        doc="A cleaned up field description, with no spaces."
    )
    date_field_id = Column(
        Integer,
        doc="The UK BioBank ID for the field."
    )
    n_instance = Column(
        Integer,
        doc="The number of instances that should be expected for a field,"
        " these can be regarded as the number of repeat measures for a field."
    )
    array = Column(
        Integer,
        doc="The number of measures that are required to represent a data "
        "point entry. in the actual UK BioBank data, the array will be "
        "repeated for for each instance."
    )
    coding_id = Column(
        Integer,
        ForeignKey('coding_ids.coding_id'),
        doc="The link back to any data codings for the field. "
    )
    data_type = Column(
        String(255),
        doc="The data type of the field."
    )
    data_unit = Column(
        String(255),
        doc="The units for the data field."
    )
    stability = Column(
        String(255),
        doc="The stability of the data field."
    )
    item_type = Column(
        String(255),
        doc="The type of information that is represented by the data field:"
        " ``bulk``, ``data``, ``records``, ``samples``."
    )
    strata = Column(
        String(255),
        doc="Not sure."
    )
    sexed = Column(
        String(255),
        doc="Does the field apply to males, female or both (unisex)."
    )
    root_data_path_id = Column(
        Integer,
        ForeignKey('data_paths.data_path_id'),
        doc="The ID of the root data path that applies to the field."
    )
    deepest_data_path_id = Column(
        Integer,
        ForeignKey('data_paths.data_path_id'),
        doc="The ID of the deepest data path that applies to the field."
    )
    data_path_level = Column(
        Integer,
        doc="The level of the field in the data path tree."
    )
    data_path_str = Column(
        String(255),
        doc="A string representation of the data path ID route to the field."
        " This is data path IDs concatenated with ``.``"
    )
    n_sample = Column(
        Integer,
        doc="The number of samples that are available for a field."
    )
    n_item = Column(
        Integer,
        doc="Not sure."
    )
    note = Column(
        Text,
        doc="A long form text description of the field."
    )
    url = Column(
        String(255),
        doc="The URL link for the field in the UK BioBank showcase."
    )

    # Relationships
    root_data_paths = relationship(
        'DataPath',
        foreign_keys=[root_data_path_id],
        back_populates='field_id_root_data_paths',
        doc="The reference back the root data path in the ``data_paths`` "
        "table."
    )
    deepest_data_paths = relationship(
        'DataPath',
        foreign_keys=[deepest_data_path_id],
        back_populates='field_id_deepest_data_paths',
        doc="The reference back the deepest data path in the ``data_paths``"
        " table."
    )
    coding_id_lookups = relationship(
        'CodingId',
        foreign_keys=coding_id,
        back_populates='field_ids',
        doc="The reference back the ``coding_id`` table."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return common.print_orm_obj(self)
