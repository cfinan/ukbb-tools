"""A limited query API for the UK BioBank data dictionary database.
"""
from ukbb_tools.data_dict import constants as bc
from ukbb_tools.data_dict import orm as dd
from enum import Enum
import warnings

_CODING_ENUM_CACHE = dict()
"""This acts as a module level cache for coding enumerations that have been
extracted and built. This seems to be an expensive process (`dict`)
"""

# # @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# class DataDictLookup(object):
#     """
#     A query API against the data dictionary
#     """
#     FID_CACHE = {}
#     CODING_ENUM_CACHE = {}

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     def __init__(self, connect_uri=None):
#         """
#         Parameters
#         ----------
#         connect_uri : :obj:`sqlalchemy.engine.url.URL` or str or NoneType,
#         optional, default: NoneType
#             Connect to an alternative data dictionary. If NoneType then the
#             package data dictionary is used
#         """
#         if connect_uri is None:
#             path = 'data/data_dict.db'  # always use slash
#             filepath = pkg_resources.resource_filename(__name__, path)

#             # Build an SQLalchemy connection URL
#             connect_uri = sqlalchemy.engine.url.URL(
#                 'sqlite', database=filepath)

#         # create the database engine, note that this does not connect until it
#         # is asked to do something, so we use sqlalchemy_utils to enforce the
#         # must_exist
#         engine = sqlalchemy.create_engine(connect_uri)

#         # If we get here then we are good to continue, so we create a session
#         self._maker = sqlalchemy.orm.sessionmaker(bind=engine)

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_field_id_enum(session, fids):
    """Yield field ID enumerations.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        The SQLAlchemy session to query the database.
    fids : list of int
        Field IDs to search for in the database.

    Yields
    ------
    field_id : `ukbb_tools.data_dict.constants.FieldId`
        A field ID named tuple with the following attribute names:
        `field_id` (`int`), `field_desc` (`str`), `data_type` (`str`),
        `data_units` (`str` or `NoneType`), `coding_id` (`int` or
        `NoneType`), `date_field_id` (`int`), `coding_enum` (`Enum`)

    Notes
    -----
    These contain the information to parse out fields of UKBB data.
    Because of the building of the Enums, this can take a while to run.
    """
    # Make sure the Enums are build with the correct data type
    cast_map = {'int': int, 'float': float}

    # Query out matching field IDs
    sql = session.query(dd.FieldId).\
        filter(dd.FieldId.field_id.in_(fids))

    # Loop through all the matches
    for field in sql:
        fid = None
        data_type = field.data_type
        code_enum = None

        try:
            # if field.field_id == 2217:
            #     print(field)
            # The coding ID could be None, if the field has no
            # enumerations, this is to fire off a TypeError
            int(field.coding_id)

            # Se if we have previously built an Enum, we cache them at the
            # class level as they seem to be expensive to make
            code_enum = _CODING_ENUM_CACHE[field.coding_id]
        except KeyError:
            # Not cached, so will build from the database
            try:
                key_vals = []

                try:
                    # Make sure the Enums are represented in the same way
                    # the data will be, the vast majority will be strings
                    cast_to = cast_map[data_type]
                except KeyError:
                    cast_to = str

                # Build the enum mappings and cast the enum value
                for e in field.coding_ids.coding_enums:
                    key_vals.append([e.enum_name, cast_to(e.enum_value)])

                # Build the enum and store in the cache for next time
                code_enum = Enum(
                    'Coding{0}'.format(field.coding_id),
                    key_vals
                )
                _CODING_ENUM_CACHE[field.coding_id] = code_enum
            except AttributeError:
                # This means that there is an error in the data dictionary
                # these will get past SQLite engines during database
                # building
                warnings.warn("no coding enumerator for: '{0}' (will "
                              "be set to string)".format(
                                  field.coding_id))
                data_type = 'str'
                code_enum = None

        except TypeError:
            # No coding ID
            code_enum = None

        fid = bc.FieldId(
            field_id=field.field_id,
            field_desc=field.field_desc,
            field_text=field.field_text,
            data_type=data_type,
            data_units=field.data_unit,
            coding_id=field.coding_id,
            date_field_id=field.date_field_id,
            coding_enum=code_enum
        )
        # if field.field_id == 2217:
        #     print(fid)

        yield fid


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_field_id(session, fids):
    """Yield field ID data without enumerations of the codings.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        The SQLAlchemy session to query the database.
    fids : list of int
        Field IDs to search for in the database.

    Yields
    ------
    field_id : `ukbb_tools.data_dict.constants.FieldId`
        A field ID named tuple with the following attribute names:
        `field_id` (`int`), `field_desc` (`str`), `data_type` (`str`),
        `data_units` (`str` or `NoneType`), `coding_id` (`int` or
        `NoneType`), `date_field_id` (`int`), `coding_enum` (`NoneType`)
    """
    # Query out matching field IDs
    sql = session.query(dd.FieldId).\
        filter(dd.FieldId.field_id.in_(fids))

    # Loop through all the matches
    for field in sql:
        fid = bc.FieldId(
            field_id=field.field_id,
            field_desc=field.field_desc,
            field_text=field.field_text,
            data_type=field.data_type,
            data_units=field.data_unit,
            coding_id=field.coding_id,
            date_field_id=field.date_field_id,
            coding_enum=None
        )

        yield fid


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_field_id_orm(session, fids):
    """Yield field ID from the database.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        The SQLAlchemy session to query the database.
    fids : list of int
        Field IDs to search for in the database.

    Yields
    ------
    field_id : `ukbb_tools.data_dict.orm.FieldId`
        A field ID.
    """
    # Query out matching field IDs
    sql = session.query(dd.FieldId).\
        filter(dd.FieldId.field_id.in_(fids))

    # Loop through all the matches
    for field in sql:
        yield field
