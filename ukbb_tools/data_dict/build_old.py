"""Build a UK Biobank data dictionary SQLite database from the files provided
by UK Biobank. Note this will overwrite any existing databases given to it.

If you wish to build the data dictionary against another database backend
please use the API, see the documentation for more details.

This builds the legacy data dictionary from two files. These are no longer
available, so the new download/build script should be used instead. This will
eventually be deleted.
"""
from ukbb_tools import (
    __version__,
    __name__ as pkg_name,
)
from ukbb_tools.data_dict import (
    orm_old as dd,
    constants as con
)
from pyaddons import log, utils as pau
from tqdm import tqdm
import pandas as pd
import numpy as np
import sys
import os
import sqlalchemy
import argparse
import re
import warnings
# import pprint as pp


_SCRIPT_NAME = "ukbb-build-old-data-dict"
"""The name of the script (`str`)
"""

_DESC = __doc__
"""The program description (`str`)
"""


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main():
    """The main entry point for the script. For API use see
    ``ukbb_tools.data_dict.build.build_data_dict``.
    """
    # Initialise and parse the command line arguments
    parser = _init_cmd_args()
    args = _parse_cmd_args(parser)

    logger = log.init_logger(
        _SCRIPT_NAME, verbose=args.verbose
    )
    log.log_prog_name(logger, pkg_name, __version__)
    log.log_args(logger, args)

    # Make sure any previous files are wiped
    open(args.outdb, 'w').close()

    # Build an SQLalchemy connection URL
    connection_url = sqlalchemy.engine.url.URL(
        'sqlite', database=str(args.outdb)
    )

    # create the database engine, note that this does not connect until
    # it is asked to do something, so we use sqlalchemy_utils to enforce
    # the must_exist
    engine = sqlalchemy.create_engine(connection_url)

    # If we get here then we are good to continue, so we create a session
    sm = sqlalchemy.orm.sessionmaker(bind=engine)
    session = sm()

    try:
        # Initialise the building of the data dict database
        build_data_dict(session, args.data_dict, args.codings,
                        verbose=args.verbose)
    except (BrokenPipeError, KeyboardInterrupt):
        # Python flushes standard streams on exit; redirect remaining
        # output to devnull to avoid another BrokenPipeError at shutdown
        devnull = os.open(os.devnull, os.O_WRONLY)
        os.dup2(devnull, sys.stdout.fileno())
        log.log_interrupt(logger)
    finally:
        session.close()
        log.log_end(logger)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _init_cmd_args():
    """Initialise the command line arguments and return the parsed arguments.

    Returns
    -------
    parser : `argparse.ArgumentParser`
        The argument parser with the arguments set up.
    """
    parser = argparse.ArgumentParser(
        description=_DESC
    )

    # The wide format UKBB data fields filex
    parser.add_argument(
        'data_dict', type=str,
        help="The path to UKBB data dictionary file this should be the .tsv"
        " file"
    )
    parser.add_argument(
        'codings', type=str,
        help="The path to UKBB codings file, this should be the .tsv file"
    )
    parser.add_argument(
        'outdb', type=str,
        help="The path to output the SQLite database"
    )
    parser.add_argument(
        '-v', '--verbose',  action="count",
        help="Give more output, use -vv for progress monitoring"
    )

    return parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _parse_cmd_args(parser):
    """Parse the command line arguments

    Parameters
    ----------
    parser : `argparse.ArgumentParser`
        The argument parser with the arguments set up.

    Returns
    -------
    args : `argparse.Namespace`
        The argparse namespace object.
    """
    args = parser.parse_args()

    # Make sure ~/ etc... is expanded
    for i in ['data_dict', 'codings', 'outdb']:
        setattr(args, i, pau.get_full_path(getattr(args, i)))

    return args


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def build_data_dict(session, data_dict_path, codings_path, create_tables=True,
                    verbose=False):
    """Build a copy of the UK Biobank data dictionary database.

    Parameters
    ----------
    connection_url : `sqlalchemy.Session`
        An SQLAlchemy session object. If ``create_tables`` is ``True``, then
        this should return the engine when ``Session.get_bind()`` is called.
    data_dict_path : `str`
        The path to the tab separated data dictionary file.
    codings_path : `str`
        The path to the tab separated codings file.
    create_tables : `bool`, optional, default: `True`
        Should the tables be created before loading.
    verbose : `bool` or `int`, optional, default: `False`
        Log progress, a values > 1, will turn on progress monitoring.
    """
    logger = log.retrieve_logger(_SCRIPT_NAME, verbose=verbose)
    prog_verbose = log.progress_verbose(verbose=verbose)

    if create_tables is True:
        logger.info("creating tables")
        dd.create_all_tables(session)

    # We first build the codings table
    logger.info("building coding tables")
    _build_codings(session, codings_path, verbose=prog_verbose)

    # Now build the dictionary using the codings table that has been built
    logger.info("building field ID tables")
    _build_field_ids(session, data_dict_path, verbose=prog_verbose)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _build_codings(session, codings_path, verbose=False):
    """Build the database codings table.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        A session to interact with the database.
    codings_path : `str`
        The path to the codings file
    verbose : `bool`, optional, default: `False`
        Turn on progress monitoring.
    """
    codings = load_coding_file(codings_path)
    _build_coding_ids(session, codings, verbose=verbose)
    _build_coding_enums(session, codings, verbose=verbose)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def load_coding_file(codings_path, **kwargs):
    """Load the entire coding file into memory, it is quite small so this
    should not be too bad.

    Note that the delimiter is fixed to tab.

    Parameters
    ----------
    codings_path : `str`
        The path to the ``.tsv`` codings file.

    Returns
    -------
    codings : `pandas.DataFrame`
        The contents of the codings file but with the columns re-labelled.
    """
    codings = pd.read_csv(
        codings_path, sep=con.FILE_DELIMITER, **kwargs
    )

    # If the columns in the file do not match the expected columns
    if con.CODING_FILE_COLUMNS[0] != tuple(codings.columns.tolist()):
        raise KeyError(
            "Unexpected columns in coding file: "
            f"{','.join(codings.columns.tolist())}"
        )

    # Relabel the columns
    codings.columns = con.CODING_FILE_COLUMNS[1]
    return codings


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _build_coding_ids(session, codings, verbose=False):
    """Build the `coding_ids` reference table.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        A session object to interact with the database.
    codings : `pandas.DataFrame`
        The contents of the codings file but with the columns re-labelled.
    verbose : `bool`, optional, default: `False`
        Turn on progress monitoring.
    """
    # Extract a sorted unique list of coding IDs to write to the database
    coding_ids = np.sort(codings[con.CODING_ID].unique())

    # Make use the coding_ids table exists
    dd.CodingId.__table__.create(
        bind=session.get_bind(), checkfirst=True
    )

    tqdm_kwargs = dict(
        disable=not verbose, desc="[info] loading coding ids",
        unit=" id(s)", leave=False
    )
    for i in tqdm(coding_ids, **tqdm_kwargs):
        # Add the coding ID, I have to cast the np.int into a python int
        # otherwise, I get a datatype mismatch
        session.add(dd.CodingId(coding_id=int(i)))

    session.commit()
    session.close()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _build_coding_enums(session, codings, verbose=False):
    """Build the `coding_enums` (coding enumerations) reference table.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        A session object to interact with the database.
    codings : `pandas.DataFrame`
        The contents of the codings file but with the columns re-labelled.
    verbose : `bool`, optional, default: `False`
        Turn on progress monitoring.
    """
    # Make use the coding_ids table exists
    dd.CodingEnum.__table__.create(bind=session.get_bind(), checkfirst=True)

    tqdm_kwargs = dict(
        disable=not verbose, desc="[info] loading coding enums",
        unit=" enum(s)", total=codings.shape[0], leave=False
    )

    repeat_numbers = {}
    for row in tqdm(codings.itertuples(), **tqdm_kwargs):
        enum_name = _format_enum_name(row.desc)
        test_key = (row.coding_id, enum_name)

        try:
            rep_n = repeat_numbers[test_key]
            enum_name = '{0}_{1}'.format(enum_name, rep_n)
            repeat_numbers[test_key] += 1
        except KeyError:
            repeat_numbers[test_key] = 2

        session.add(
            dd.CodingEnum(
                coding_id=row.coding_id,
                enum_value=row.enum_value,
                enum_name=enum_name,
                enum_desc=row.desc
            )
        )

    session.commit()
    session.close()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _format_enum_name(enum_desc):
    """Format the enum description into an enum value.

    Parameters
    ----------
    enum_name : `str`
        The enumeration name from the codings file.

    Returns
    -------
    enum_name : `str`
        The enumeration value that has been formatted as described above.

    Notes
    -----
    This is the name with all the non-word characters replaced with an _ and
    made upper case. Additionally bracketed examples are removed. Any missing
    `NaN` values are replaced with a text ``NO_VALUE``.
    """
    try:
        # Pandas imports mixed types and some are nans
        if np.isnan(enum_desc):
            return con.NO_VALUE
    except TypeError:
        pass

    enum_name = enum_desc.strip()

    # remove e.g. brackets
    enum_name = re.sub(r'\(e\.g\..+?\)', '_', enum_name)

    # non word characters and spaced are replaced with _
    enum_name = re.sub(r'[\W ]+', '_', enum_name).upper()

    # Strip any artefactual leading/lagging _
    enum_name = enum_name.strip('_')

    # Remove any multiple _
    enum_name = re.sub(r'_{2, }', '_', enum_name)

    return enum_name


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _build_field_ids(session, data_dict_path, verbose=False):
    """Build all the tables releated to the field IDs.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        A session object to interact with the database.
    data_dict_path : `str`
        The path to the `Data_Dictionary_showcase.tsv` file.
    verbose : `bool`, optional, default: `False`
        Turn on progress monitoring.
    """
    field_ids = load_field_ids(data_dict_path)

    # Add any coding IDs present in the field ID file that are not in the
    # codings file
    _test_missing_coding_ids(session, field_ids)

    # Make sure the "withdrawn" field code is added
    field_ids = _insert_withdrawn_field_code(field_ids)

    # Get a mapping between the field_ids and the most relevant date
    get_most_relevant_date(field_ids)

    # Build the data path tables and their relationships
    _build_data_paths(session, field_ids, verbose=verbose)

    # Build the field ID table and cross referece with the data paths
    _build_field_id_lookup(session, field_ids)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def load_field_ids(data_dict_path, **kwargs):
    """Load the entire field_id file into memory, it is quite small so this
    should not be too bad.

    Note that the delimiter is fixed to tab. The field id file is also
    referred to as the Data dictionary file as well.

    Parameters
    ----------
    data_dict_path : `str`
        The path to the `Data_Dictionary_showcase.tsv` file.
    **kwargs
        Any other keyword arguments to ``pandas.read_csv``.

    Returns
    -------
    codings : `pandas.DataFrame`
        The contents of the codings file but with the columns re-labelled.
    """
    field_ids = pd.read_csv(
        data_dict_path, sep=con.FILE_DELIMITER, **kwargs
    )

    if con.FIELD_ID_COLUMNS[0] != tuple(field_ids.columns.tolist()):
        raise KeyError("unexpected columns in field_id file")

    # Relabel the columns
    field_ids.columns = con.FIELD_ID_COLUMNS[1]
    return field_ids


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _test_missing_coding_ids(session, field_ids):
    """This tests for (and inserts), coding IDs that are present in the field
    ID files but not represented in the codings file.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        A session object to interact with the database.
    field_ids : `pandas.DataFrame`
        A data frame of the field ID data to extract the data paths from.

    Notes
    -----
    This is a fudge to stop integrity errors when building, it does not get
    over the fact that the UKBioBank data dictionary is a bit off.
    """
    # Gather the coding IDs in the database and in the field IDs file
    coding_ids = [i.coding_id for i in session.query(dd.CodingId).all()]
    field_cid = []

    for i in field_ids.coding_id.unique():
        try:
            field_cid.append(int(i))
        except ValueError:
            pass

    missing = set(field_cid).difference(coding_ids)
    if len(missing) > 0:
        warnings.warn(
            'fields with undefined coding IDs (will be added back in):'
            f' {",".join([str(i) for i in missing])}'
        )

        # This is a fudge to add in codings that do not appear in the codings
        # file but are in the field IDs
        for i in missing:
            session.add(dd.CodingId(coding_id=i))
        session.commit()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _insert_withdrawn_field_code(field_ids):
    """Insert a custom withdrawn field into the field IDs.

    This has the field ID of 0 and represents if a UKBB sample has withdrawn
    from the study. This is calculated when 2 wide files are merged.

    Parameters
    ----------
    field_ids : `pandas.DataFrame`
        The field IDs that have been read in from the data dictionary file. We
        will add to these.
    """
    # First make sure that we do not have a field_id of 0 that would screw
    # things up
    if len(field_ids.loc[field_ids.field_id == 0, ]) > 0:
        raise IndexError("Unsafe to add UKBB custom field as field ID 0 "
                         "already exists")

    # We assign the coding ID to 1 which should be the TRUE in the TRUE/FALSE
    # Enum 34030. The relevant date is the date lost to follow up which is
    # manually set above
    withdrawn_field = {
        'data_path': 'Population characteristics > Ongoing characteristics',
        'category': 2,
        'field_id': 0,
        'field_desc': 'UKBB tools withdrawn field',
        'n_sample': 0,
        'n_item': 0,
        'stability': 'Custom',
        'data_type': 'Categorical single',
        'data_unit': '',
        'item_type': 'Data',
        'strata': 'Auxiliary',
        'sexed': 'Unisex',
        'n_instance': 0,
        'array': 0,
        'coding_id': 12,
        'note': ('A custom field used by UKBB tools when merging files it '
                  'indicate if a sample may have withdrawn from the study'),
        'url': None
    }
    # return field_ids.append(withdrawn_field, ignore_index=True)
    return pd.concat(
        [field_ids, pd.DataFrame([withdrawn_field])], ignore_index=True
    )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_most_relevant_date(field_ids):
    """This assigns predefined mappings between groups of field_ids and their
    most relevant date. These will be stored in the field_ids table.

    Parameters
    ----------
    field_ids : `pandas.DataFrame`
        A data frame of the field ID data to extract the data paths from.

    Returns
    -------
    field_ids : `pandas.DataFrame`
        A data frame of the field ID with a date_field_id column added. The
        return is not necessary as this happens in place.

    Raises
    ------
    TypeError
        If any of the assigned date fields are not actually date fields.

    Notes
    -----
    The most relevant date, is intended to be used as a baseline date for the
    measurement. This will default to the field ID when someone attended the
    assessment centre (53).
    """
    # We default everything to the date of assessement field
    field_ids[con.DATE_FIELD_ID] = con.ATTEND_ASSESSMENT

    # Now assign dates to paths under a specific path
    _assign_dates_under_data_path(field_ids)

    # Now assign dates matching a specific path
    _assign_dates_with_data_path(field_ids)

    # Now the assay dates
    _assign_dates_to_blood_biochemistry_assays(field_ids)
    _assign_dates_to_blood_count_measures(field_ids)
    _assign_dates_to_urine_biochemistry_measures(field_ids)

    # Now the fileds that have their relevant date one field ID above
    _assign_dates_to_algorithm_diseases(field_ids)
    _assign_dates_to_first_occurrences(field_ids)

    # The order that these functions are called is important as some of the
    # functions will intentionally overwrite data from the previous function
    # calls
    _assign_dates_to_date_field_id(field_ids)

    # Finally do the manual assignments
    _assign_dates_from_manual_map(field_ids)

    # Final check to make sure that all the assigned dates are actually date
    # fields
    date_fields = field_ids.loc[
        field_ids.field_id.isin(field_ids[con.DATE_FIELD_ID].unique()),
        'data_type'
    ].unique()

    if any(~pd.Series(date_fields).isin(con.DATE_FIELDS)) is True:
        raise TypeError("Non date fields mapped to date")

    return field_ids


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _assign_dates_under_data_path(field_ids):
    """Get date assignments based on date field that are applicable to child
    data paths.

    Parameters
    ----------
    field_ids : `pandas.DataFrame`
        A data frame of the field ID data to extract the data paths from.

    Returns
    -------
    field_ids : `pandas.DataFrame`
        A data frame of the field ID with a date_field_id column added. The
        return is not necessary as this happens in place.

    Raises
    ------
    KeyError
        If a data path query yields no matches to assign the field ID to.

    Notes
    -----
    The data paths being matched may go multiple levels into the data path
    tree and will label anything that level or below with the assigned date
    field ID. The date field IDs are manually assigned.
    """
    for data_path, date_field in con.DATE_MAPPINGS_UNDER_DATA_PATH.items():
        matches = (
            (field_ids.data_path.str.contains('^{0}'.format(data_path)))
            & ~(field_ids.data_path == data_path)
        )
        nrows = field_ids.loc[matches, con.DATE_FIELD_ID].shape[0]
        if nrows == 0:
            raise KeyError(f"No match for data path: {data_path}")
        field_ids.loc[matches, con.DATE_FIELD_ID] = date_field
    return field_ids


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _assign_dates_with_data_path(field_ids):
    """Set date assignments to a date field that are applicaable to whole
    data paths.

    Parameters
    ----------
    field_ids : `pandas.DataFrame`
        A data frame of the field ID data to extract the data paths from.

    Returns
    -------
    field_ids : `pandas.DataFrame`
        A data frame of the field ID with a date_field_id column added. The
        return is not necessary as this happens in place.

    Raises
    ------
    KeyError
        If a data path query yields no matches to assign the field ID to.

    Notes
    -----
    The data paths being matched will go multiple levels into the data path
    tree and will label anything that matches exactly to that data path. The
    date field IDs are manually assigned.
    """
    for data_path, date_field in con.DATE_MAPPINGS_BY_DATA_PATH.items():
        matches = field_ids.data_path == data_path

        # Make sure something matches
        if np.any(matches) is False:
            raise KeyError(f"No match for data path: {data_path}")

        field_ids.loc[matches, con.DATE_FIELD_ID] = date_field
    return field_ids


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _assign_dates_to_blood_biochemistry_assays(field_ids):
    """Get date assignments for the blood biochemistry processing (assays)
    fields.

    Parameters
    ----------
    field_ids : `pandas.DataFrame`
        A data frame of the field ID data to extract the data paths from.

    Returns
    -------
    field_ids : `pandas.DataFrame`
        A data frame of the field ID with a date_field_id column added. The
        return is not necessary as this happens in place.

    Notes
    -----
    Note that these are different to the actual results that will be referenced
    to when the blood was taken, where as the assays are referenced to when the
    assay was conducted.
    """
    results_path = (
        "Biological samples > Blood assays > Blood biochemistry"
    )
    assays_path = (
        "Biological samples > Blood assays > Blood biochemistry"
        " > Blood biochemistry processing"
    )

    return _assign_dates_to_assays_based_on_results(
        field_ids,
        results_path,
        _exact_results_query,
        assays_path,
        _exact_assay_query,
        error=True
    )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _assign_dates_to_assays_based_on_results(field_ids, results_path,
                                             results_query_func,
                                             assay_path, assay_query_func,
                                             result_query_args=[],
                                             assay_query_args=[],
                                             error=True):
    """Assign dates to assays based on results. Basically, match results to
    the assays that generated the results.

    Parameters
    ----------
    field_ids : `pandas.DataFrame`
        A data frame of the field ID data to extract the data paths from.
    results_path : `str`
        The path to where the result measures are located, this is a string
        representation of a hierarchical structure in UKBB.
    results_query_func : `func`
        A function that will return a boolean series that acts as a selector
        for assay results, which will intern form the prefixes for assay
        queries. The function should accept the following parameters:
        field IDs (`pandas.data_frame`), results data path and `*args`.
    assay_path : `str`
        The path to where the assay data are located, this is a string
        representation of a hierarchical structure in UKBB
    assay_query_func : `func`
        A function that will return a boolean series that acts as a selector
        for assay metadata, the metadata will also include the date the
        assay was completed. The function should accept the following
        parameters: field_IDs (`pandas.data_frame`), assay data path,
        assay_prefix (`str`) and `*args`.
    result_query_args : `list`, optional, default: `[]`
        Arguments that are passed through to `results_query_func`
    assay_query_args : `list``, optional, default: `[]`
        Arguments that are passed through to `assay_query_func`
    error : `bool`, optional, default: `True`
        Shall I error out if anything goes wrong? if False then a warning will
        be generated instead and the date mapping will be skipped.

    Returns
    -------
    field_ids : `pandas.DataFrame`
        A data frame of the field ID with a date_field_id column added. The
        return is not necessary as this happens in place

    Raises
    ------
    ValueError
        If the query for assay information does not produce any results or
        if the assay information contains more that one field which is a
        date or time data_type

    Notes
    -----
    This runs a small algorithm that will use the results to derive the
    names of what has been measured (results_path) and then use those names to
    find all the assay information for that result in the (measures_path). The
    date is then located in the measures path and applied to all the measures
    (but not the results - as these are dated to blood collection)
    """
    # In the vast majority of cases the results descriptions are used as
    # prefixes in all the assays descriptions, there are a couple of
    # exceptions though
    results_vector = results_query_func(
        field_ids,
        results_path,
        *result_query_args
    )
    assay_prefix = field_ids.loc[results_vector, 'field_desc'].tolist()

    if len(assay_prefix) == 0:
        raise ValueError("can't find results for '{0}'".format(results_path))

    for prefix in assay_prefix:
        assay_vector = assay_query_func(
            field_ids, assay_path, prefix, *assay_query_args
        )

        # Now get the assays that are prefixed with the current prefix
        assay_fields = field_ids.loc[assay_vector, ]

        if len(assay_fields) == 0:
            msg = "no assays found for '{0}'".format(prefix)

            if error is True:
                raise ValueError(msg)
            else:
                warnings.warn(msg)
                continue

        # Now find the assay date field amongst the assay fields
        date_field_id = assay_fields.loc[
            assay_fields.data_type.isin(['Date', 'Time']), 'field_id'
        ].to_list()

        if len(date_field_id) != 1:
            msg = "'{1}': needed 1 date field found: {0}".format(
                len(date_field_id),
                prefix
            )

            if error is True:
                raise ValueError(msg)
            else:
                warnings.warn(msg)
                continue

        # Now if we get here we are good to update all the date_field_ids
        # for the assay with the field_id that has been extracted
        field_ids.loc[
            field_ids.field_id.isin(assay_fields.field_id),
            con.DATE_FIELD_ID
        ] = date_field_id[0]

    return field_ids


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _exact_results_query(field_ids, results_path, *args):
    """Queries for exact matches between the result_path and the data_path
    field.

    Parameters
    ----------
    field_ids : `pandas.DataFrame`
        A data frame of the field ID data to extract the data paths from.
    results_path : `str`
        The path to where the result measures are located, this is a string
        representation of a hierarchical structure in UKBB.
    *args
        Any other arguments, has no effect.

    Returns
    -------
    matches : `pandas.Series`
        A boolean series that is used to select the correct rows from the
        filter_ids DataFrame.
    """
    return field_ids.data_path == results_path


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _exact_assay_query(field_ids, assay_path, assay_prefix, *args):
    """Queries for exact matches between the data_path and the assay_path
    string and the field description must start with the assay prefix.

    Parameters
    ----------
    field_ids : `pandas.DataFrame`
        A data frame of the field ID data to extract the data paths from
    assay_path : `str`
        The path to where the assay data are located, this is a string
        representation of a hierarchical structure in UKBB
    assay_prefix : `str`
        The name of the analyte that has been measured and that should be
        prefixed in the assay metadata field
    *args
        Any other arguments, has no effect.

    Returns
    -------
    matches : `pandas.Series`
        A boolean series that is used to select the correct rows from the
        filter_ids DataFrame.
    """
    # Now get the assays that are prefixed with the current prefix
    return (
        (field_ids.data_path == assay_path) & (
            field_ids.field_desc.str.contains(
                '^{0}'.format(re.escape(assay_prefix))
            )
        )
    )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _assign_dates_to_blood_count_measures(field_ids):
    """Get date assignments for the blood count processing (assays) fields.

    Parameters
    ----------
    field_ids : `pandas.DataFrame`
        A data frame of the field ID data to extract the data paths from.

    Returns
    -------
    field_ids : `pandas.DataFrame`
        A data frame of the field ID with a date_field_id column added. The
        return is not necessary as this happens in place.

    Notes
    -----
    Note that these are different to the actual results that will be referenced
    to when the blood was taken, where as the assays are referenced to when the
    assay was conducted.
    """
    results_path = (
        "Biological samples > Blood assays > Blood count"
    )
    assays_path = (
        "Biological samples > Blood assays > "
        "Blood count > Blood count processing"
    )

    return _assign_dates_to_assays_based_on_results(
        field_ids, results_path,
        _exact_results_query,
        assays_path,
        _exact_assay_query,
        error=False
    )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _assign_dates_to_urine_biochemistry_measures(field_ids):
    """Get date assignments for all the urine biochemistry measures.

    Parameters
    ----------
    field_ids : `pandas.DataFrame`
        A data frame of the field ID data to extract the data paths from.

    Returns
    -------
    field_ids : `pandas.DataFrame`
        A data frame of the field ID with a date_field_id column added. The
        return is not necessary as this happens in place.

    Notes
    -----
    This assigns the correct assay time to each assay.
    """
    results_path = 'Biological samples > Urine assays'
    assays_path = (
        "Biological samples > Urine assays > Urine processing"
    )

    return _assign_dates_to_assays_based_on_results(
        field_ids, results_path,
        _exact_results_query,
        assays_path,
        _exact_assay_query,
        error=False
    )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _assign_dates_to_algorithm_diseases(field_ids):
    """
    Assign dates to the report generation of the algorithmic defined
    diseases

    Parameters
    ----------
    field_ids : `pandas.DataFrame`
        A data frame of the field ID data to extract the data paths from

    Returns
    -------
    field_ids : `pandas.DataFrame`
        A data frame of the field ID with a date_field_id column added. The
        return is not necessary as this happens in place
    """
    data_path = 'Health-related outcomes > Algorithmically-defined outcomes'
    return _assign_stagered_date_fields(field_ids, data_path)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _assign_stagered_date_fields(field_ids, data_path):
    """This assigns a date field to a report field, where the date field occurs
    in the field_id before the report field.

    This would be a lot easier if I made the field_id an index.

    Parameters
    ----------
    field_ids : `pandas.DataFrame`
        A data frame of the field ID data to extract the data paths from.
    data_path : `str`
        The path containing the staggered date fields and report fields.

    Returns
    -------
    field_ids : `pandas.DataFrame`
        A data frame of the field ID with a date_field_id column added. The
        return is not necessary as this happens in place.
    """
    fids = np.sort(
        field_ids.loc[
            field_ids.data_path.str.contains(
                '^{0}'.format(data_path)
            ), 'field_id'
        ]
    )

    # Now massively efficient
    for i in range(0, len(fids), 2):
        data_type = field_ids.loc[
            field_ids.field_id == fids[i], 'data_type'].tolist()[0]

        if data_type != 'Date':
            raise ValueError("expecting lead element to be a date")

        field_ids.loc[
            field_ids.field_id == fids[i+1], con.DATE_FIELD_ID
        ] = fids[i]

    return field_ids


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _assign_dates_to_first_occurrences(field_ids):
    """Assign dates to the report generation of the first occurence of a
    particular disease.

    Parameters
    ----------
    field_ids : `pandas.DataFrame`
        A data frame of the field ID data to extract the data paths from.

    Returns
    -------
    field_ids : `pandas.DataFrame`
        A data frame of the field ID with a date_field_id column added. The
        return is not necessary as this happens in place.
    """
    data_path = 'Health-related outcomes > First occurrences'
    return _assign_stagered_date_fields(field_ids, data_path)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _assign_dates_to_date_field_id(field_ids):
    """Make sure all date/time fields are annotated with their respective
    date/time.

    Parameters
    ----------
    field_ids : `pandas.DataFrame`
        A data frame of the field ID data to extract the data paths from.

    Returns
    -------
    field_ids : `pandas.DataFrame`
        A data frame of the field ID with a date_field_id column added. The
        return is not necessary as this happens in place.
    """
    field_ids.loc[
        field_ids.data_type.isin(
            ['Date', 'Time']
        ), con.DATE_FIELD_ID
    ] = field_ids.loc[
        field_ids.data_type.isin(
            ['Date', 'Time']), 'field_id'
    ]
    return field_ids


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _assign_dates_from_manual_map(field_ids):
    """Assign date_field_ids based on atomic field_id: date_field_id mappings.

    Parameters
    ----------
    field_ids : `pandas.DataFrame`
        A data frame of the field ID data to extract the data paths from.

    Returns
    -------
    field_ids : `pandas.DataFrame`
        A data frame of the field ID with a date_field_id column added. The
        return is not necessary as this happens in place.

    Raises
    ------
    KeyError
        If any of the manual field IDs can't be found.
    """
    # Not very efficient but should work ok
    for fid, date_field in con.MANUAL_MAPPINGS.items():
        if field_ids.loc[field_ids.field_id == fid].shape[0] == 0:
            raise KeyError(f"fid not found: {fid}")

        field_ids.loc[field_ids.field_id == fid, con.DATE_FIELD_ID] = \
            date_field
    return field_ids


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _build_data_paths(session, field_ids, verbose=False):
    """build the data paths table.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        A session object to use to interact with the datbase.
    field_ids : `pandas.DataFrame`
        A data frame of the field ID data to extract the data paths from.
    """
    # Will hold all the individual data_path nodes that have been processed
    # into the database
    seen = set()

    # Make use the coding_ids table exists
    dd.DataPath.__table__.create(bind=session.get_bind(), checkfirst=True)

    tqdm_kwargs = dict(
        leave=False, unit=" path(s)", desc="[info] adding data paths",
        disable=not verbose, total=field_ids.data_path.nunique()
    )
    data_path_tree = {}
    for data_path in tqdm(field_ids.data_path.unique(), **tqdm_kwargs):
        data_path = _split_data_path(data_path)

        for node in data_path:
            if node not in seen:
                session.add(dd.DataPath(data_path_name=node))
                seen.add(node)
        _add_to_data_path_tree(data_path_tree, data_path)
    session.commit()
    session.close()

    # Now use the tree that has been constructed to build the relationships
    # table
    _build_data_path_rel(session, data_path_tree)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _split_data_path(data_path):
    """Split a data path string into it's individual nodes.

    Parameters
    ----------
    data_path : `str`
        A data path to process, it is expected the nodes are separated by
        ``>``.

    Returns
    -------
    data_path_nodes : `list` of `str`
        A list containing the individual nodes, with the first element being
        the root and the last element being the bottom level.
    """
    return re.split(r'\s*>\s*', data_path.strip())


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _add_to_data_path_tree(current_tree, data_path):
    """Mesh a data path into an existing data_path tree. Note that everything
    happens inplace so nothing is returned.

    Parameters
    ----------
    current_tree : `dict` of `dict`
        A nested dictionary of data paths that we want to add to.
    data_path : `list` or `str`
        The data_path that we want to add to the tree with the root at the
        first element and the bottom at the last element. Note that the list
        will be destroyed during the creation of the tree
    """
    try:
        # Remove the root element from the list
        root = data_path.pop(0)
    except IndexError:
        # Reached the end of the data_path so er unwind
        return

    # Make sure the level below exists
    current_tree.setdefault(root, {})

    # Pass the slightly smaller data_path and the next level down of the tree
    _add_to_data_path_tree(current_tree[root], data_path)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _build_data_path_rel(session, data_path_tree):
    """Build the data path relationship table.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        A session to interact with the database.
    data_path_tree : `dict` of `dict`
        A nested dictionary of data paths that we want to use to describe the
        relationships.
    """
    # Make use the coding_ids table exists
    dd.DataPathRel.__table__.create(bind=session.get_bind(), checkfirst=True)

    # .........................................................................
    def _add_to_database(session, tree):
        """Add the tree to the database. This function acts recursively to
        navigate the tree and build a flat relationships table
        """
        # This will be returned to form the child_ids of the iteration above
        # or to the original caller
        parent_ids = []
        for parent, children in tree.items():
            # TODO: get the parent ID and store it
            parent_node = _get_data_path_node(session, parent)

            # if there are children, then we process and add to the database
            if len(children) > 0:
                for child_id in _add_to_database(session, children):
                    session.add(
                        dd.DataPathRel(
                            parent_data_path_id=parent_node.data_path_id,
                            child_data_path_id=child_id
                        )
                    )

            parent_ids.append(parent_node.data_path_id)
        return parent_ids

    _add_to_database(session, data_path_tree)
    session.commit()
    session.close()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _get_data_path_node(session, node_name):
    """Get a data path node from the data path that matches a specific name.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        A session object connection to the database to perform the query.
    node_name : `str`
        The name of the data_path node that is to be queried against the
        database.

    Returns
    -------
    result : `data_dict_orm.DataPath`
        A data_path node representing the matching result.

    Raises
    ------
    TODO: Error names for sqlalchemy > 1 result or 0 results?
    """
    return session.query(dd.DataPath).\
        filter(dd.DataPath.data_path_name == node_name).one()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _build_field_id_lookup(session, field_ids):
    """Build a field_id lookup table that is linked to the data_paths and the
    coding_ids.

    Parameters
    ---------
    maker : `sqlalchemy.Sessionmaker`
        A session object to interact with the database.
    field_ids : `pandas.DataFrame`
        A data frame of the field ID data to extract the data paths from.
    """
    # Make use the coding_ids table exists
    dd.FieldId.__table__.create(bind=session.get_bind(), checkfirst=True)

    data_node_cache = {}
    for row in field_ids.sort_values('field_id').itertuples():
        root_id, deepest_id, levels, data_path_str = _get_data_path_fields(
            session, data_node_cache, row.data_path)

        session.add(
            dd.FieldId(
                field_id=row.field_id,
                field_desc=row.field_desc,
                field_text=_format_enum_name(row.field_desc),
                date_field_id=row.date_field_id,
                n_instance=row.n_instance,
                array=row.array,
                coding_id=row.coding_id,
                data_type=con.DATA_TYPE_MAP[row.data_type],
                data_unit=row.data_unit,
                stability=row.stability.lower(),
                item_type=row.item_type.lower(),
                strata=row.strata.lower(),
                sexed=row.sexed.lower(),
                root_data_path_id=root_id,
                deepest_data_path_id=deepest_id,
                data_path_level=levels,
                data_path_str=data_path_str,
                n_sample=row.n_sample,
                n_item=row.n_item,
                note=row.note,
                url=row.url
            )
        )

    session.commit()
    session.close()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _get_data_path_fields(session, data_node_cache, data_path):
    """Get all the data fields that are related to the data path.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        A session object connection to the database to perform the query.
    data_node_cache : `dict` or `list`
        A cache of mappings between split data paths (represented as tuples)
        and lists of data_path_ids that they map to. This is designed to
        minimise the number of database queries.
    data_path : `str`
        The string data path as represented in the UKBB data dict file
        (i.e. pre-splitting).

    Returns
    -------
    root_id : `int`
        The data_path_id if the root node in the data path.
    deepest_id : `int`
        The data_path_id if the deepest node in the data path.
    levels : `int`
        The number of levels in the data path.
    data_path_str `str`
        The data_path_ids represented as a concatinated string. separated
        with ``.``.
    """
    data_path_nodes = tuple(_split_data_path(data_path))

    try:
        data_path_ids = data_node_cache[data_path_nodes]
    except KeyError:
        data_path_ids = [_get_data_path_node(session, i).data_path_id
                         for i in data_path_nodes]
        data_node_cache[data_path_nodes] = data_path_ids

    root_id = data_path_ids[0]
    deepest_id = data_path_ids[-1]
    levels = len(data_path_nodes)
    data_path_str = '.'.join([str(i) for i in data_path_ids])

    return root_id, deepest_id, levels, data_path_str
