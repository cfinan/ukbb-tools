"""Constants used in the package.
"""
import os

# ENVIRONMENT VARIABLES #
UKBB_PACKAGE_ENV = "UKBB_PACKAGE_DIR"
"""The environment variable that controls where the package directory is
located (`str`).
"""
UKBB_CONFIG_ENV = "UKBB_CONFIG"
"""The environment variable that the user can use to define the location
of the configuration file.
"""

# FILE AND DIRECTORY LOCATIONS #
DEFAULT_CONFIG_NAME = ".ukbb-tools.toml"
"""The default UKBB-tools configuration file name (`str`)
"""

# FILE AND DIRECTORY LOCATIONS #
_PACKAGE_DIR_BASE = ".ukbb_tools"
"""The base name for the package directory, where various files will be
placed (`str`)
"""
PACKAGE_DIR_PATH = os.path.join(os.environ['HOME'], _PACKAGE_DIR_BASE)
"""The default full name of the directory, this is used if the user has not set
the package directory environment variable (`str`)
"""
