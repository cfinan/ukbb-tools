"""Custom error handlers.
"""


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class EidError(Exception):
    """A custom error raised when a bad EID is found.

    Parameters
    ----------
    message : `str`
        The error message.
    eid : `str`
        The EID value (appended to the message)
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, message, eid):
        message = f"{message}: {eid}"
        super().__init__(message)
        self.eid = eid


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class ResolveError(Exception):
    """A custom error raised when sample order can't be resolved.

    Parameters
    ----------
    message : `str`
        The error message.
    samples : `list` of `any`, optional, default: `NoneType`
        The failed sample set (if any)
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, message, samples=None):
        message = f"{message}"
        super().__init__(message)
        self.samples = samples

