"""Manipulate sample definition (``.sdef``) files.
"""
# Importing the version number (in __init__.py) and the package name
from ukbb_tools import (
    __version__,
    __name__ as pkg_name,
    # columns as col,
    # parsers,
    # type_casts as tc,
    # errors
)
from pyaddons import log, utils
# from pyaddons.flat_files import header
# from tqdm import tqdm
# from tqdm.contrib.logging import logging_redirect_tqdm
# from operator import itemgetter
# from pybgen import PyBGEN
# from glob import glob
# import numpy as np
# import pandas as pd
# import hashlib
# import itertools
import argparse
# import shelve
# import csv
import sys
import os
# import stdopen
# import re


# The name of the script
_SCRIPT_NAME = "ukbb-sdef"
"""The name of the script (`str`)
"""
# Use the module docstring for argparse
_DESC = __doc__
"""The program description (`str`)
"""
_UPDATE_OPTION = 'option'
"""The update option keyword (`str`)
"""
_STACK_OPTION = 'stack'
"""The stack option keyword (`str`)
"""
_UNION_OPTION = 'union'
"""The union option keyword (`str`)
"""
_NEGATE_OPTION = 'negate'
"""The negate option keyword (`str`)
"""
_INTERSECT_OPTION = 'intersect'
"""The intersect option keyword (`str`)
"""


# _GENETIC_SEX_FIELD = "22001-0.0"
# """The column name for the genetic sex in a basket file (`str`)
# """
# _DEFAULT_DELIMITER = "\t"
# """The default delimiter for the output file (`str`)
# """
# _WITHDRAWN_EID_REGEXP = re.compile(r'^-\d+$')
# """A compiled regular expression to match withdrawn EIDs in genotype based
# files (`re.Pattern`)
# """

# Make sure csv can deal with the longest lines possible
# csv.field_size_limit(sys.maxsize)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main():
    """The main entry point for the script. For API use see
    ``ukbb_tools.samples.sample_search``.
    """
    # Initialise and parse the command line arguments
    parser = _init_cmd_args()
    args = _parse_cmd_args(parser)

    # Use logger.info to issue log messages
    logger = log.init_logger(
        _SCRIPT_NAME, verbose=args.verbose
    )
    log.log_prog_name(logger, pkg_name, __version__)
    log.log_args(logger, args)

    if args.option == _UNION_OPTION:
        sdef_union()
    # # Read in the sample withdrawals
    # open_method = utils.get_open_method(args.withdrawals)
    # with open_method(args.withdrawals) as infile:
    #     withdrawals = [i.strip() for i in infile]

    # # Read in any files or directories to be excluded
    # excludes = _read_paths(args.exclude)

    # # Read in any include directories
    # inputs = []
    # if args.include is not None:
    #     inputs = _read_paths(args.include)

    # # Make any output directories as needed
    # outdir = os.path.dirname(args.outprefix)
    # os.makedirs(outdir, exist_ok=True)

    # sample_file = f"{args.outprefix}samples.sdef"
    # error_matrix_file = f"{args.outprefix}error-files.txt"
    # overlap_matrix_file = f"{args.outprefix}overlap-files.txt"
    # sample_equal_file = f"{args.outprefix}equivalence-files.txt"
    # bad_sample_file = f"{args.outprefix}bad-samples.txt"
    # bad_files_file = f"{args.outprefix}bad-files.txt"
    try:
        pass
    #     samples, bad_samples, error_matrix, overlap_matrix, bad_files, \
    #         equal_samples = sample_search(
    #             args.inputs + inputs, excludes=excludes, verbose=args.verbose,
    #             tmpdir=args.tmpdir, encoding=args.encoding,
    #             allow_bad_eid=args.allow_bad_eid, withdrawals=withdrawals
    #         )

    #     # Write the errors and overlaps
    #     if error_matrix is not None:
    #         error_matrix.to_csv(
    #             error_matrix_file, sep=_DEFAULT_DELIMITER, header=True,
    #             index=True
    #         )

    #     # Write the errors and overlaps
    #     if overlap_matrix is not None:
    #         overlap_matrix.to_csv(
    #             overlap_matrix_file, sep=_DEFAULT_DELIMITER, header=True,
    #             index=True
    #         )

    #     # Write any bad samples
    #     if len(bad_samples) > 0:
    #         with stdopen.open(
    #                 bad_sample_file, mode='wt', use_tmp=True,
    #                 tmpdir=args.tmpdir
    #         ) as outfile:
    #             writer = csv.writer(outfile, delimiter=_DEFAULT_DELIMITER,
    #                                 lineterminator=os.linesep)
    #             writer.writerow(['filename', col.EID.name])
    #             for i in bad_samples:
    #                 writer.writerow(i)

    #     # Write any bad files
    #     if len(bad_files) > 0:
    #         with stdopen.open(
    #                 bad_files_file, mode='wt', use_tmp=True, tmpdir=args.tmpdir
    #         ) as outfile:
    #             writer = csv.writer(outfile, delimiter=_DEFAULT_DELIMITER,
    #                                 lineterminator=os.linesep)
    #             writer.writerow(['filename', 'no_of_errors'])
    #             for i in bad_files:
    #                 writer.writerow(i)

    #     # Write any equivalent samples
    #     if len(equal_samples) > 0:
    #         with stdopen.open(
    #                 sample_equal_file, mode='wt', use_tmp=True,
    #                 tmpdir=args.tmpdir
    #         ) as outfile:
    #             writer = csv.writer(outfile, delimiter=_DEFAULT_DELIMITER,
    #                                 lineterminator=os.linesep)
    #             writer.writerow(['group_id', 'filename'])
    #             group_id = 1
    #             for key, files in equal_samples.items():
    #                 for f in files:
    #                     writer.writerow([group_id, f])
    #                 group_id += 1

    #     # Write out the resolved samples
    #     header = [i.name for i in parsers.SampleDefinitionFile.COLUMNS]

    #     # So we can log the total number of samples set to withdrawn
    #     total_withdrawn = 0

    #     with stdopen.open(
    #             sample_file, mode='wt', use_tmp=True, tmpdir=args.tmpdir
    #     ) as outfile:
    #         writer = csv.writer(outfile, delimiter=_DEFAULT_DELIMITER,
    #                             lineterminator=os.linesep)
    #         writer.writerow(header)
    #         for i in samples:
    #             i = list(i)
    #             i[2] = int(i[2])
    #             i[3] = int(i[3])
    #             total_withdrawn += i[3]
    #             writer.writerow(i)

        # logger.info(f"Total samples set to withdrawn: {total_withdrawn}")
    except (BrokenPipeError, KeyboardInterrupt):
        # Python flushes standard streams on exit; redirect remaining
        # output to devnull to avoid another BrokenPipeError at shutdown
        devnull = os.open(os.devnull, os.O_WRONLY)
        os.dup2(devnull, sys.stdout.fileno())
        log.log_interrupt(logger)
    finally:
        log.log_end(logger)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _init_cmd_args():
    """Initialise the command line arguments and return the parser.

    Returns
    -------
    args : `argparse.ArgumentParser`
        The argparse parser object with arguments added
    """
    parser = argparse.ArgumentParser(
        description=_DESC
    )
    subparsers = parser.add_subparsers(dest="option")

    update = subparsers.add_parser(
        _UPDATE_OPTION,
        help="update an ``.sdef`` file with data from other sdef files. The"
        " number and order of the samples stays the same."
    )
    add_update_parser(update)

    stack = subparsers.add_parser(
        _STACK_OPTION,
        help="Stack multiple ``.sdef`` files together, in the order they "
        "are supplied. Replicated samples in later files are skipped."
    )
    add_stack_parser(stack)

    union = subparsers.add_parser(
        _UNION_OPTION,
        help="Create a union of ``.sdef`` files. The relative order must be "
        "the same files"
    )
    add_union_parser(union)

    negate = subparsers.add_parser(
        _NEGATE_OPTION,
        help="Set withdrawn samples in an ``.sdef`` to sequential negative"
        " numbers."
    )
    add_negate_parser(negate)

    intersect = subparsers.add_parser(
        _INTERSECT_OPTION,
        help="Intersect samples from multiple ``.sdef`` files."
    )
    add_intersect_parser(intersect)

    parser.add_argument(
        '-w', '--withdrawals', type=str,
        help="The path to the UK Biobank withdrawal file. This should have"
        " valid sample identifoers, one per line, with no header."
    )
    parser.add_argument(
        '-o', '--outfile', type=str,
        help="The path to an output file, if it is not supplied then output "
        "will be to STDOUT."
    )
    parser.add_argument(
        '--allow-bad-eid', action="store_true",
        help="Allow bad EIDs (UK Biobank sample IDs) to be output. This is"
        " useful for creating sample definition files from genotype "
        ".sample/.fam files that have the sample IDs set to a negative value"
        " to indicate that they are missing."
    )
    parser.add_argument(
        '-v', '--verbose',  action="count",
        help="give more output, use -vv for progress monitoring"
    )
    return parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def add_update_parser(subparser):
    """A sub-parser arguments for the "update" parser.

    Parameters
    ----------
    parser : `argparse.ArgumentParser`
        The argparse parser to add the update specific arguments added.
    """
    subparser.add_argument(
        'input_file', type=str,
        help="The file who's attributes will be updated."
    )
    subparser.add_argument(
        'lookup_files', nargs="+", type=str,
        help="The files that will be used to update the input file."
    )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def add_stack_parser(subparser):
    """A sub-parser arguments for the "stack" parser.

    Parameters
    ----------
    parser : `argparse.ArgumentParser`
        The argparse parser to add the stack specific arguments added.
    """
    subparser.add_argument(
        'stack_files', nargs="+", type=str,
        help="The files that will be stacked in the order that they are given."
    )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def add_intersect_parser(subparser):
    """A sub-parser arguments for the "intersection" parser.

    Parameters
    ----------
    parser : `argparse.ArgumentParser`
        The argparse parser to add the intersection specific arguments added.
    """
    subparser.add_argument(
        'intersect_files', nargs="+", type=str,
        help="The files that will be intersected in the order that they are"
        " given."
    )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def add_union_parser(subparser):
    """A sub-parser arguments for the "union" parser.

    Parameters
    ----------
    parser : `argparse.ArgumentParser`
        The argparse parser to add the union specific arguments added.
    """
    subparser.add_argument(
        '-T', '--tmpdir', type=str,
        help="The temp directory to write the sample files to. If not supplied"
        " then the system tmp location is used."
    )
    subparser.add_argument(
        'union_files', nargs="+", type=str,
        help="The files that will be unified."
    )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def add_negate_parser(subparser):
    """A sub-parser arguments for the "negate" parser.

    Parameters
    ----------
    parser : `argparse.ArgumentParser`
        The argparse parser to add the negate specific arguments added.
    """
    subparser.add_argument(
        'negate_file', type=str,
        help="The file that will have the withdrawn samples negated."
    )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _parse_cmd_args(parser):
    """Parse the command line arguments.

    Parameters
    ----------
    parser : `argparse.ArgumentParser`
        The argparse parser object with arguments added.

    Returns
    -------
    args : `argparse.Namespace`
        The argparse namespace object containing the arguments
    """
    args = parser.parse_args()
    # args.inputs = [utils.get_full_path(i) for i in args.inputs]
    # args.outprefix = utils.get_full_path(args.outprefix)

    # TODO: You can perform checks on the arguments here if you want
    return args


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def sdef_union(infiles, withdrawls=None):
    """
    """
    pass


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def sdef_stack():
    """
    """
    pass


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def sdef_update():
    """
    """
    pass


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def sdef_negate():
    """
    """
    pass


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def sdef_intersect():
    """
    """
    pass
