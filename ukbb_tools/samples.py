"""For generating sample list. This will traverse directories and extract all
the sample IDs it finds. The sample order is maintained and if any of the
samples it finds are not in the same order it will log the errors. It is
able to extract samples from ``.bgen``, ``.vcf``, ``.sample``, ``.fam``,
``.tab``, ``.txt``, ``.csv`` and ``.sdef`` (sample definition) files. You
can supply a list of files or directories you want to include/exclude from
the process. This can be used to generate a sample order file or validate
sample order in a set of files. The text based formats can also be compressed
with a .gz file extension.

By default, this will output the largest sample set found. However, there is
an experimental complex mode that will attempt to align overlapping lists to
create a contiguous sample output.

The sample sex/withdrawn status will be merged from each list and any
differences will result in an error, apart from differences with missing
values. Flat column wide basket-like files with field IDs will be examined to
see if they have genetic sex description fields, if so the data is used in the
output.

By default, bad sample IDs (EIDs), will be excluded. In most instances these
are negative sample IDs found in ``.fam``, ``.sample`` and ``.vcf`` files.
If you are generative a sample list for mapping purposes then you will want to
use the ``--allow-bad-eid`` option to keep these in.

This outputs multiple files to an output directory supplied by the user. The
files will have the prefix name defined by the user.

1. sample_file - The sample order list (eid, sex 0,1,2 (unknown,male,female),
   is_genotyped 1/0 (yes/no), has_withdrawn 1/0 (yes/no)) ``*samples.sdef``.
2. sample_equal_file - The files that have equivalent sample sets.
   (group_id, filename). The samples in files with the same group IDs the same
   (in content and order). The number of unique group IDs will equal the number
   of files actually compared ``*equivalence-files.txt``.
3. bad_sample_file - Lists of sample IDs that do not conform to the EID
   specification of all digits and length 7 (eids treated as strings). Contains
   the filename, eid ``*bad-samples.txt``.
4. bad_files_file - Files that were removed from the matching process due to
   bad sample ordering, the algorithm will remove the worst offenders first
   until no sample ordering issues exist ``*bad-files.txt``.
5. error_matrix_file - A square matrix, detailing the pairwise number of sample
   order errors between pairs of files ``*error-files.txt``.
6. overlap_matrix_file - A square matrix, detailing the number of non-error
   sample overlaps between two files. This is different to the total
   overlapping samples between the files ``*overlap-files.txt``.
"""
# Importing the version number (in __init__.py) and the package name
from ukbb_tools import (
    __version__,
    __name__ as pkg_name,
    columns as col,
    parsers,
    type_casts as tc,
    errors
)
from pyaddons import log, utils
from pyaddons.flat_files import header
from tqdm import tqdm
from tqdm.contrib.logging import logging_redirect_tqdm
from operator import itemgetter
from pybgen import PyBGEN
from glob import glob
import numpy as np
import pandas as pd
import hashlib
import itertools
import argparse
import shelve
import csv
import sys
import os
import stdopen
import re
# import pprint as pp


# The name of the script
_SCRIPT_NAME = "ukbb-samples"
"""The name of the script (`str`)
"""
# Use the module docstring for argparse
_DESC = __doc__
"""The program description (`str`)
"""
_GENETIC_SEX_FIELD = "22001-0.0"
"""The column name for the genetic sex in a basket file (`str`)
"""
_DEFAULT_DELIMITER = "\t"
"""The default delimiter for the output file (`str`)
"""
_WITHDRAWN_EID_REGEXP = re.compile(r'^-\d+$')
"""A compiled regular expression to match withdrawn EIDs in genotype based
files (`re.Pattern`)
"""

# Make sure csv can deal with the longest lines possible
csv.field_size_limit(sys.maxsize)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class ListOrderResolver(object):
    """A handler for resolving the list order (if possible)

    Parameters
    ----------
    db_file : `str`
        The path to the shelve database. Where the lists will be stored.
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, tmpdir=None):
        self.tmpdir = tmpdir
        self.db_file = None
        self.align_file = None
        self.db = None
        self.align = None
        self.cache = set()
        self.bad_keys = []
        self.checksums = dict()
        self.errors = []
        self._error_matrix = None
        self._overlap_matrix = None
        self.removed_lists = []

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __enter__(self):
        """Enter the context manager.
        """
        self.open()
        return self

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __exit__(self, *args):
        """Exit the context manager.
        """
        self.close()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __len__(self):
        """The number of unique samples identified.
        """
        return len(self.cache)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def open(self):
        """Open the shelve database.
        """
        self.db_file = utils.get_temp_file(dir=self.tmpdir)
        os.unlink(self.db_file)
        self.db = shelve.open(self.db_file)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def close(self):
        """Close the shelve database.
        """
        self.db.close()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_data_size(self):
        """Get the sizes of all the samples sets added.

        Returns
        -------
        data_size : `list` of `tuple`
            The number of items in each of the lists that has been provided.
            The tuple is the list key name and the length.
        """
        data_size = []
        for k, v in self.db.items():
            data_size.append((k, len(v)))
        return data_size

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def nlists(self):
        """Get the number of lists that have been stored, this may differ from
        the number of lists added if there are identical lists added (`int`).
        """
        return len(self.db)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def add_list(self, key_name, data):
        """Add a list to the database.

        Parameters
        ----------
        key_name : `str`
            The name of the file where the samples were extracted from.
        data : `list` of `any`
            The list to store/check.
        """
        # Make sure we have to regenerate these, if new data is added. This is
        # not that sophisticated but does the job for now
        self._error_matrix = None
        self._overlap_matrix = None

        # If alignments have been performed then clear
        if self.align is not None:
            self.align.close()
            os.unlink(self.align_file)
            self.align = None

        data_hash = hashlib.md5()
        store_data = []
        for i in data:
            store_i = self.get_hash_data(i)
            data_hash.update(store_i.encode())
            store_data.append(store_i)
            self.add_to_cache(i)

        if len(set(store_data)) != len(store_data):
            raise ValueError(
                f"Non-consecutive duplicated data for: {key_name}"
            )

        hex_hash = data_hash.hexdigest()

        try:
            # Do we have this list already? If yes then we store the key
            self.checksums[hex_hash].append(key_name)
        except KeyError:
            # No then we store the list and add a hash entry for the file
            self.db[key_name] = store_data
            self.checksums[hex_hash] = [key_name]

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_hash_data(self, data):
        """Get the data components used to create a hash. The hash is used to
        identify identical lists so they are not processed.

        Parameters
        ----------
        data : `any`
           The data to extract components from, this could be anything but
           the default implementation assumes strings.

        Returns
        -------
        data : `str`
            The data to be used in the hash.

        Notes
        -----
        This is designed to be overridden if the data is a complex structure.
        """
        return data

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def add_to_cache(self, data):
        """Add the data to the cache, the cache is designed to store all
        unique data entries seen.

        Parameters
        ----------
        data : `any`
           The data to extract components from, this could be anything but
           the default implementation assumes strings.
        """
        self.cache.add(data)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def compare_lists(self, verbose=False):
        """Get an error matrix based on relative sample order between the
        lists.

        Parameters
        ----------
        verbose : `bool`, optional, default: `False`
            Report matrix creation progress.

        Returns
        -------
        error_matrix : `pandas.DataFrame`
            A matrix detailing the number of ordering errors observed between
            pairs of inputs.
        overlap_matrix : `pandas.DataFrame`
            A matrix detailing the number of overlapping items between
            pairs of inputs.

        Notes
        -----
        If the matrices have already been created then they will be returned.
        Matrices are cleared after each list is added.
        """
        data_size = dict(self.get_data_size())

        if len(data_size) <= 1:
            raise ValueError("not enough data to generate an error matrix")

        if self._error_matrix is not None and \
           self._overlap_matrix is not None:
            return self._error_matrix, self._overlap_matrix

        keys = sorted(data_size.keys())
        edf = pd.DataFrame(
            np.zeros((len(data_size), len(data_size)), dtype='int32'),
            index=keys,
            columns=keys
        )
        odf = pd.DataFrame(
            np.zeros((len(data_size), len(data_size)), dtype='int32'),
            index=keys,
            columns=keys
        )

        comps = list(itertools.combinations(keys, 2))
        tqdm_kwargs = dict(
            desc="[info] comparing files..",
            unit=" combs",
            leave=False,
            disable=not verbose,
            total=len(comps)
        )
        # We are comparing the samples that intersect between files so
        # test one half of the matrix and mirror.
        for iname, jname in tqdm(comps, **tqdm_kwargs):
            i = dict([(i, idx) for idx, i in enumerate(self.db[iname])])
            j = self.db[jname]
            prev_idx = 0
            errors = 0
            overlaps = 0
            tqdm_kwargs = dict(
                desc="[info] comparing samples..",
                unit=" samples",
                leave=False,
                disable=not verbose,
                total=len(j)
            )

            for x in tqdm(j, **tqdm_kwargs):
                try:
                    idx = i[x]
                    # idx = i.index(x)
                except KeyError:
                    continue
                if idx < prev_idx:
                    errors += 1
                overlaps += 1
                prev_idx = idx
            edf.loc[iname, jname] = errors
            edf.loc[jname, iname] = errors
            odf.loc[iname, iname] = len(i)
            odf.loc[jname, jname] = len(j)
            odf.loc[iname, jname] = overlaps
            odf.loc[jname, iname] = overlaps
        self._error_matrix, self._overlap_matrix = edf, odf
        return self._error_matrix, self._overlap_matrix

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def delete_error_lists(self, verbose=False):
        """This removes lists that have the most ordering errors until the
        remaining lists pass i.e. have the same relative order.

        Parameters
        ----------
        verbose : `bool`, optional, default: `False`
            Report matrix creation progress.

        Returns
        -------
        removed : `list` of `tuple`
            The key names and number of errors for each list that has been
            removed.

        Raises
        ------
        KeyError
            If all the datasets have errors in them.
        """
        error_matrix, overlaps_matrix = self.compare_lists(verbose=verbose)
        error_totals = error_matrix.sum()

        removed = []
        most_errors = error_totals.max()
        worst = error_totals.index[error_totals.argmax()]

        while most_errors > 0:
            error_matrix.drop(worst, axis=0, inplace=True)
            error_matrix.drop(worst, axis=1, inplace=True)
            del self.db[worst]
            removed.append((worst, most_errors))
            error_totals = error_matrix.sum()
            most_errors = error_totals.max()
            worst = error_totals.index[error_totals.argmax()]

        if len(removed) > 0 and error_matrix.shape[0] == 1:
            raise KeyError(
                "All error datasets removed, the relative order of all"
                " lists is likely different"
            )
        self.removed_lists.extend(removed)
        return removed

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def resolve(self, complex=False, verbose=False, missing=None):
        """Attempt to resolve the sample order from all the ingested samples.

        Parameters
        ----------
        complex : `bool`, optional, default: `True`
            Attempt to align the data in all the lists (see notes).
        verbose : `bool`, optional, default: `False`
            Report matrix creation progress and complex resolving.
        missing : `str`, optional, default: `NoneType`
            The value to use for missing (gaps) elements when aligning the
            lists. Only applies to complex resolution.

        Returns
        -------
        resolved_samples : `list` of `any`
            The resolved list.

        Notes
        -----
        This attempts to order the lists samples into their full order using
        the data from multiple lists. The implementation is not great, it will
        work if there is a lot of overlap, and the longest list has the end
        samples in it. It could be improved a lot. However, it is conservative
        and will fail rather than return crap. If you have any doubts then use
        complex=False, this will return the longest list. In all cases lists
        with incorrect ordering are removed. The IndexError will be raised if
        there are data points not in it.
        """
        if self.nlists == 0:
            raise ValueError("No lists have been added")

        # Single list then just return
        if self.nlists == 1:
            for k, v in self.db.items():
                return v

        # Remove data where the lists are out of sequence
        self.removed_lists = self.delete_error_lists()

        full_order = []
        if complex is True:
            try:
                full_order = self.solve(verbose=verbose, missing=missing)
            except (KeyError, ValueError) as e:
                # Turn complex resolution errors into a general resolve error
                raise errors.ResolveError(
                    f"Complex resolve failed: {str(e)}", samples=[]
                )
        else:
            data_size = sorted(
                self.get_data_size(),
                key=itemgetter(1),
                reverse=True
            )
            full_order = self.db[data_size[0][0]]

        if len(full_order) != len(self.cache):
            raise errors.ResolveError(
                "The samples have been ordered but do not represent all "
                "observed samples, possibly due to files being excluded:"
                f" len(list) == '{len(full_order)}',  len(seen) == "
                f"'{len(self.cache)}'", samples=full_order
            )
        return full_order

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @classmethod
    def calc_index_range(cls, master, test):
        """Check the elements in test against master. If the test elements do
        not exist in master determine their likely coordinate range.

        Parameters
        ----------
        master : `list`
            The master list that the test list will be compared against.
        test : `list`
            The list that will be compared against the master list.

        Returns
        -------
        no_index : `list` of `tuple`
            The values in test that are not present in the master list. Each
            tuple has the structure:

            0. The lowest index position the value could be in.
            1. The highest index position the value could be in.
            2. The value that is missing.

        Notes
        -----
        Missing values can have the same index range. They are returned such
        that they can be reversed and spliced into the master to give the test
        list values. This method is not actually used at the moment but might
        useful so I have kept it here.

        Examples
        --------
        A simple test:

        >>> master = ["05", "06", "07", "08", "09", "14", "15", "16"]
        >>> test = ["02", "04", "05", "09", "10", "11", "12", "13", "14"]
        >>> samples.SampleOrderResolver.calc_index_range(master, test)
        >>> [[0, 0, "02"], [0, 0, "04"], [5, 5, "10"], [5, 5, "11"],
        [5, 5, "12"], [5, 5, "13"]]

        >>> master = ["05", "06", "09", "13", "15", "16"]
        >>> test = ["02", "04", "05", "07", "09", "10", "11", "12", "13", "14"]
        >>> samples.SampleOrderResolver.calc_index_range(master, test)
        >>> [[0, 0, "02"], [0, 0, "04"],  [1, 2, "07"], [3, 3, "10"],
        [3, 3, "11"], [3, 3, "12"], [4, 6, "14"]]
        """
        curr_idx = -1
        no_idx = []
        for s in test:
            try:
                # Found
                idx = master.index(s)
            except ValueError:
                idx = None
                try:
                    start_idx = curr_idx+1
                    no_idx.append([start_idx, len(master), s])
                except IndexError:
                    no_idx.append([curr_idx+1, len(master), s])
                continue

            if idx < curr_idx:
                # Samples not in master order so file can't be trusted
                raise IndexError("sample out of order")

            for i in no_idx:
                i[1] = min(i[1], idx)

            # New current index
            curr_idx = idx

        return no_idx

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @classmethod
    def align_lists(cls, a, b, missing=None):
        """Attempt to align the elements of two lists.

        Parameters
        ----------
        align_a : `tuple`
            The first list alignment. [0] is the first non-missing element.
            [1] Is the number of gaps inserted in a and [2] is the aligned
            list
        align_b : `tuple`
            The second list alignment. [0] is the first non-missing element.
            [1] Is the number of gaps inserted in b and [2] is the aligned
            list
        missing : `str`, optional, default: `NoneType`
            The value to use for missing (gaps) elements.

        Notes
        -----
        This assumes the lists have unique elements.
        """
        # Create an index lookup for the first list, this is a lot faster then
        # the index method of the list but is brute force. We ignore any
        # missing values (i.e. from pre-aligned lists) but count them in the
        # number of values so we can detect duplications
        blkp = dict()
        missing_gaps = 0
        for idx, i in enumerate(b):
            if i == missing:
                missing_gaps += 1
                continue
            blkp[i] = idx

        if (len(blkp) + missing_gaps) != len(b):
            raise IndexError("Duplicated positions in list(b)")

        apointer = 0
        # Initialised to -1 to account for no b matches at the a=0
        # i.e. we insert after the bpointer
        bpointer = -1
        aoffset = 0
        boffset = 0
        # loop_idx = 0
        # loop_stop = 10
        a_gaps = 0
        b_gaps = 0
        found = False
        while True:
            try:
                i = a[apointer]
            except IndexError:
                break
            if i == missing:
                apointer += 1
                continue

            try:
                bidx = blkp[i]

                if bidx < bpointer:
                    raise ValueError(
                        "The 'a' and 'b' lists are out of alignment"
                    )
                bpointer = bidx
                found = True
            except KeyError:
                # If the a element is not found in b then we gap b by 1
                try:
                    bval = b[apointer]
                except IndexError:
                    bval = missing
                # if found is False:
                #     raise KeyError("lists not alignable")
                if bval != missing:
                    index_loc = (bpointer+boffset)+1
                    b[index_loc:index_loc] = [missing]
                    b_gaps += 1
                    boffset += 1
                apointer += 1

                # if loop_idx >= loop_stop:
                #     break
                # loop_idx += 1
                continue

            # 'a' element is in 'b', so we shift the 'a' element location to
            # match to the 'b' element location
            gap_len = (bidx + boffset) - apointer
            if gap_len >= 0:
                aoffset += gap_len
                a[apointer:apointer] = [missing] * gap_len
                a_gaps += gap_len
                apointer += (1 + gap_len)
            else:
                index_loc = (bpointer+boffset)
                gap_len = abs(gap_len)
                b[index_loc:index_loc] = [missing] * gap_len
                b_gaps += gap_len
                boffset += gap_len
                apointer += 1

            # if loop_idx >= loop_stop:
            #     break
            # loop_idx += 1

        if found is False:
            raise KeyError("Lists not able to be aligned")
        # Pad shortest list
        la = len(a)
        lb = len(b)
        if la < lb:
            a[la:la] = [missing] * (lb - la)
            a_gaps += (lb - la)
        elif lb < la:
            b[lb:lb] = [missing] * (la - lb)
            b_gaps += (la - lb)

        # First aligned is the index position of the non missing values
        # in either list, I could probably calculate these in the loop
        fa = None
        fb = None
        for i in range(len(a)):
            if fa is None and a[i] != missing:
                fa = i
            if fb is None and b[i] != missing:
                fb = i
            if fa is not None and fb is not None:
                break
        return (fa, a_gaps, a), (fb, b_gaps, b)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @classmethod
    def check_overlaps(cls, overlaps):
        """Check an overlaps matrix to make sure it has a chance of being
        resolved. Provide optimal combination for pairwise comparisons.

        Parameters
        ----------
        overlaps : `pandas.DataFrame`
            The overlaps matrix.

        Returns
        -------
        compare_combs : `list` of `tuple`
            Pairwise combinations if lists with the most overlaps. This can be
            used in the alignment process. Each tuple contains the list keys
            that are associated with each list.
        zero_combs : `list` of `tuple`
            Combinations of lists that do not have any overlaps. Each tuple
            contains the list keys that are associated with each list.

        Raises
        ------
        IndexError
            If the overlaps matrix indicates that the lists can't be resolved.
            This will happen if a single list does not overlap with all the
            other lists.
        """
        # print(overlaps)
        combs = []
        zero_combs = []
        # Get the total overlaps for each list not including self overlaps
        totals = (overlaps.sum(axis=1) - np.diag(overlaps)).sort_values(
            ascending=False
        )

        # Find lists that do not overlap with any other lists, these are
        # unsolvable
        no_overlaps = overlaps.index[totals == 0]
        if no_overlaps.size > 0:
            raise ValueError(
                f"Unsolvable lists, {no_overlaps.size} lists with no overlaps"
                " with any other lists: "
                f"{','.join([str(i) for i in no_overlaps])}"
            )

        group = [totals.index[0]]
        totals = dict([i for i in totals.iteritems()])
        seen = set(totals.keys())
        search_idx = 0

        while True:
            try:
                search = group[search_idx]
                seen.discard(search)
            except IndexError:
                break
            matches = overlaps.loc[
                ~overlaps.index.isin(group[:search_idx+1]), search
            ].sort_values(ascending=False)

            for idx, v in matches.iteritems():
                comb = (search, idx)
                rev_comb = (idx, search)
                if v > 0:
                    if comb not in combs and rev_comb not in combs:
                        combs.append(comb)

                    if idx in seen:
                        seen.discard(idx)
                        group.append(idx)
                elif v == 0:
                    if comb not in zero_combs and rev_comb not in zero_combs:
                        zero_combs.append(comb)
            search_idx += 1

        if len(seen) > 0:
            raise ValueError("Unsolvable lists, not all lists overlap")
        return combs, zero_combs

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def align_multiple_lists(self, missing=None, verbose=False):
        """Attempt to align he elements of many lists.

        Parameters
        ----------
        missing : `str`, optional, default: `NoneType`
            The value to use for missing (gaps) elements.
        verbose : `bool`, optional, default: `False`
            Report progress.

        Returns
        -------
        align : `shelve`
            The alignment shelve, the keys are the list names and the values
            are the aligned lists.

        Notes
        -----
        This assumes the lists have unique elements.
        """
        if self.align is None:
            self.align_file = utils.get_temp_file(dir=self.tmpdir)
            os.unlink(self.align_file)
            self.align = shelve.open(self.align_file)
        else:
            return self.align

        self.delete_error_lists(verbose=verbose)
        combs, zero_combs = self.check_overlaps(self._overlap_matrix)

        prog = tqdm(
            desc="[info] aligning lists...",
            unit="comparison(s)",
            leave=False,
            disable=not verbose
        )
        list_len = dict()
        max_len = 0

        master_start_idx = dict(
            [i for i in zip(self._overlap_matrix.index,
                            np.diag(self._overlap_matrix))]
        )
        master_combs = combs
        i, j = master_combs[0]
        master_start_idx[i] = 0
        master_start_idx[j] = 1
        total_gaps = 1
        try:
            while total_gaps > 0:
                combs = list(master_combs)
                start_idx = dict(master_start_idx)
                total_gaps = 0
                loop_idx = 0
                while True:
                    cur_idx = None

                    try:
                        k, v = min(start_idx.items(), key=itemgetter(1))
                    except ValueError:
                        if len(start_idx) > 0:
                            raise
                        break
                    for idx, c in enumerate(combs):
                        if k in c:
                            cur_idx = idx
                            i, j = c
                            if i != k:
                                j = i
                                i = k
                            break
                    try:
                        combs.pop(cur_idx)
                    except TypeError:
                        if cur_idx is not None:
                            raise
                        del start_idx[k]
                        loop_idx += 1
                        continue

                    try:
                        a = self.align[i]
                    except KeyError:
                        a = self.db[i]

                    try:
                        b = self.align[j]
                    except KeyError:
                        b = self.db[j]

                    x, y = self.align_lists(a, b, missing=missing)

                    aidx, agap, a = x
                    bidx, bgap, b = y
                    max_len = max(max_len, len(a), len(b))
                    list_len[i] = len(a)
                    list_len[j] = len(b)
                    self.align[i] = a
                    self.align[j] = b

                    try:
                        master_start_idx[i] = aidx
                        start_idx[i] = aidx
                    except KeyError:
                        pass

                    try:
                        master_start_idx[j] = bidx
                        start_idx[j] = bidx
                    except KeyError:
                        pass

                    total_gaps += (agap + bgap)
                    loop_idx += 1
                    prog.update(1)
        finally:
            prog.close()

        max_len = max(list_len.values())
        for k, v in list_len.items():
            if v != max_len:
                a = self.align[k]
                len(a)
                a[len(a):len(a)] = [missing] * (max_len - len(a))
                self.align[k] = a
        return self.align

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def solve(self, missing=None, verbose=False):
        """Attempt to align the elements of two lists.

        Parameters
        ----------
        missing : `str`, optional, default: `NoneType`
            The value to use for missing (gaps) elements.
        verbose : `bool`, optional, default: `False`
            Report progress.

        Parameters
        ----------
        complete_list : `list` of `any`
            The complete aligned list. Any regions that are ambiguous will
            contain the missing data value.

        Notes
        -----
        This assumes the lists have unique elements.
        """
        align = self.align_multiple_lists(missing=missing, verbose=verbose)
        align_keys = sorted(list(align.keys()))
        a = align[align_keys[0]]
        all_none = {}

        for i in align_keys[1:]:
            b = align[i]
            pscore_y = 1
            for i in range(len(a)):
                try:
                    x = a[i]
                    y = b[i]
                    score_x = x != missing
                    score_y = y != missing
                except IndexError:
                    raise IndexError(
                        "alignments have different lengths, this should not"
                        f" happen: {align_keys[0]}={len(a)}"
                        f" vs. {align_keys[i]}={len(b)} ")

                total_score = score_x + score_y
                if total_score == 0:
                    # Both missing, skip these as if they never existed
                    # But count to fudge around bad aligned regions
                    try:
                        all_none[i] += 1
                    except KeyError:
                        # Init to two as 1 will be in the A list
                        all_none[i] = 2
                    continue
                elif total_score == 2 and x != y:
                    # Error state, this is a bad alignment
                    raise ValueError(
                        f"Positions not the same: {i}={x} vs. {i}={y}"
                    )
                elif pscore_y == 1 and score_x == 0:
                    a[i] = y
                pscore_y = score_y

        nalign = len(align_keys)
        for i in sorted(
            [idx for idx, count in all_none.items()
             if count == nalign], reverse=True
        ):
            a.pop(i)
        return a


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class SampleOrderResolver(ListOrderResolver):
    """A handler for resolving the sample order (if possible)

    Parameters
    ----------
    db_file : `str`
        The path to the shelve database.
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, tmpdir=None):
        super().__init__(tmpdir=tmpdir)

        # This implementation uses a dictionary cache
        self.cache = dict()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_hash_data(self, data):
        """Get the data components used to create a hash. The hash is used to
        identify identical lists so they are not processed.

        Parameters
        ----------
        data : `tuple` or `list`
            The data to extract the hash value from:

            0. The sample ID
            1. Sample sex 1=male, 2=female, 0=unknown
            2. Is genotyped, True if yes, False if no
            3. The withdrawn status

        Returns
        -------
        data : `str`
            The data to be used in the hash (sample ID).
        """
        return data[0]

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def add_to_cache(self, data):
        """Add the data to the cache, the cache is designed to store all
        unique data entries seen.

        Parameters
        ----------
        data : `tuple` or `list`
            The data to add to the cache:

            0. The sample ID
            1. Sample sex 1=male, 2=female, 0=unknown
            2. Is genotyped, True if yes, False if no
            3. The withdrawn status

        Raises
        ------
        ValueError
            If any data values have the same sample ID but different
            non-missing sex values.
        """
        try:
            store_sex, store_geno, store_withdrawn = self.cache[data[0]]

            if store_sex == col.SAMPLE_SEX.missing[0]:
                store_sex = data[1]
            elif data[1] != col.SAMPLE_SEX.missing[0] and store_sex != data[1]:
                raise ValueError(
                    f"sample has different sex values: ({data[0]})"
                )

            store_geno |= data[2]
            store_withdrawn |= data[3]
            self.cache[data[0]] = (store_sex, store_geno, store_withdrawn)
        except KeyError:
            self.cache[data[0]] = (data[1], data[2], data[3])

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def resolve(self, **kwargs):
        """Attempt to resolve the sample order from all the ingested samples.

        Parameters
        ----------
        complex : `bool`, optional, default: `True`
            Attempt to align the data in all the lists (see notes).
        verbose : `bool`, optional, default: `False`
            Report matrix creation progress and complex resolving.

        Returns
        -------
        resolved_samples : `list` of `list`
            The resolved samples, each tuple has the values:

            0. The sample ID
            1. Sample sex 1=male, 2=female, 0=unknown
            2. Is genotyped, True if yes, False if no
            3. The withdrawn status

        Notes
        -----
        This attempts to order the lists samples into their full order using
        the data from multiple lists. The implementation is not great, it will
        work if there is a lot of overlap, and the longest list has the end
        samples in it. It could be improved a lot. However, it is conservative
        and will fail rather than return crap. If you have any doubts then use
        complex=False, this will return the longest list. In all cases lists
        with incorrect ordering are removed. The IndexError will be raised if
        there are data points not in it.
        """
        try:
            samples = super().resolve(**kwargs)
        except errors.ResolveError as e:
            samples = [[i, *self.cache[i]] for i in e.samples]
            e.samples = samples
            raise
        return [[i, *self.cache[i]] for i in samples]


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main():
    """The main entry point for the script. For API use see
    ``ukbb_tools.samples.sample_search``.
    """
    # Initialise and parse the command line arguments
    parser = _init_cmd_args()
    args = _parse_cmd_args(parser)

    # Use logger.info to issue log messages
    logger = log.init_logger(
        _SCRIPT_NAME, verbose=args.verbose
    )
    log.log_prog_name(logger, pkg_name, __version__)
    log.log_args(logger, args)

    # Read in the sample withdrawals
    open_method = utils.get_open_method(args.withdrawals)
    with open_method(args.withdrawals) as infile:
        withdrawals = [i.strip() for i in infile]

    # Read in any files or directories to be excluded
    excludes = _read_paths(args.exclude)

    # Read in any include directories
    inputs = []
    if args.include is not None:
        inputs = _read_paths(args.include)

    # Make any output directories as needed
    outdir = os.path.dirname(args.outprefix)
    os.makedirs(outdir, exist_ok=True)

    sample_file = f"{args.outprefix}samples.sdef"
    error_matrix_file = f"{args.outprefix}error-files.txt"
    overlap_matrix_file = f"{args.outprefix}overlap-files.txt"
    sample_equal_file = f"{args.outprefix}equivalence-files.txt"
    bad_sample_file = f"{args.outprefix}bad-samples.txt"
    bad_files_file = f"{args.outprefix}bad-files.txt"
    try:
        samples, bad_samples, error_matrix, overlap_matrix, bad_files, \
            equal_samples = sample_search(
                args.inputs + inputs, excludes=excludes, verbose=args.verbose,
                tmpdir=args.tmpdir, encoding=args.encoding,
                allow_bad_eid=args.allow_bad_eid, withdrawals=withdrawals
            )

        # Write the errors and overlaps
        if error_matrix is not None:
            error_matrix.to_csv(
                error_matrix_file, sep=_DEFAULT_DELIMITER, header=True,
                index=True
            )

        # Write the errors and overlaps
        if overlap_matrix is not None:
            overlap_matrix.to_csv(
                overlap_matrix_file, sep=_DEFAULT_DELIMITER, header=True,
                index=True
            )

        # Write any bad samples
        if len(bad_samples) > 0:
            with stdopen.open(
                    bad_sample_file, mode='wt', use_tmp=True,
                    tmpdir=args.tmpdir
            ) as outfile:
                writer = csv.writer(outfile, delimiter=_DEFAULT_DELIMITER,
                                    lineterminator=os.linesep)
                writer.writerow(['filename', col.EID.name])
                for i in bad_samples:
                    writer.writerow(i)

        # Write any bad files
        if len(bad_files) > 0:
            with stdopen.open(
                    bad_files_file, mode='wt', use_tmp=True, tmpdir=args.tmpdir
            ) as outfile:
                writer = csv.writer(outfile, delimiter=_DEFAULT_DELIMITER,
                                    lineterminator=os.linesep)
                writer.writerow(['filename', 'no_of_errors'])
                for i in bad_files:
                    writer.writerow(i)

        # Write any equivalent samples
        if len(equal_samples) > 0:
            with stdopen.open(
                    sample_equal_file, mode='wt', use_tmp=True,
                    tmpdir=args.tmpdir
            ) as outfile:
                writer = csv.writer(outfile, delimiter=_DEFAULT_DELIMITER,
                                    lineterminator=os.linesep)
                writer.writerow(['group_id', 'filename'])
                group_id = 1
                for key, files in equal_samples.items():
                    for f in files:
                        writer.writerow([group_id, f])
                    group_id += 1

        # Write out the resolved samples
        header = [i.name for i in parsers.SampleDefinitionFile.COLUMNS]

        # So we can log the total number of samples set to withdrawn
        total_withdrawn = 0

        with stdopen.open(
                sample_file, mode='wt', use_tmp=True, tmpdir=args.tmpdir
        ) as outfile:
            writer = csv.writer(outfile, delimiter=_DEFAULT_DELIMITER,
                                lineterminator=os.linesep)
            writer.writerow(header)
            for i in samples:
                i = list(i)
                i[2] = int(i[2])
                i[3] = int(i[3])
                total_withdrawn += i[3]
                writer.writerow(i)

        logger.info(f"Total samples set to withdrawn: {total_withdrawn}")
    except (BrokenPipeError, KeyboardInterrupt):
        # Python flushes standard streams on exit; redirect remaining
        # output to devnull to avoid another BrokenPipeError at shutdown
        devnull = os.open(os.devnull, os.O_WRONLY)
        os.dup2(devnull, sys.stdout.fileno())
        log.log_interrupt(logger)
    finally:
        log.log_end(logger)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _init_cmd_args():
    """Initialise the command line arguments and return the parser.

    Returns
    -------
    args : `argparse.ArgumentParser`
        The argparse parser object with arguments added
    """
    parser = argparse.ArgumentParser(
        description=_DESC
    )

    parser.add_argument(
        'withdrawals', type=str,
        help="The path to the UK Biobank withdrawal file. This should have"
        " valid sample identifoers, one per line, with no header."
    )
    parser.add_argument(
        'outprefix', type=str,
        help="An output directory with a prefix to be added to each output "
        "file, i.e. '/path/to/out/dir/file-prefix-'."
    )
    parser.add_argument(
        'inputs', nargs='+', type=str,
        help="One or more files/directories we want to traverse. Also see"
        " --include"
    )
    parser.add_argument(
        '--exclude', type=str,
        help="A path to a file containing either full file or directory"
        " paths that we do not want to search in, one per line."
    )
    parser.add_argument(
        '--include', type=str,
        help="A path to a file containing either full file or directory"
        " paths that we want to search in, one per line. These are "
        "combined with any given on the command line. Duplicates will"
        " be excluded."
    )
    parser.add_argument(
        '--encoding', type=str, default=parsers.UTF8,
        help="The encoding to use for text files."
    )
    parser.add_argument(
        '-T', '--tmpdir', type=str,
        help="The temp directory to write the sample files to. If not supplied"
        " then the system tmp location is used."
    )
    parser.add_argument(
        '-v', '--verbose',  action="count",
        help="give more output, use -vv for progress monitoring"
    )
    parser.add_argument(
        '--complex', action="store_true",
        help="Perform complex list merging, without this, the longest set of"
        " samples is taken, if this does not represent the full set an error"
        " will be logged. Note this is experimental"
    )
    parser.add_argument(
        '--allow-bad-eid', action="store_true",
        help="Allow bad EIDs (UK Biobank sample IDs) to be output. This is"
        " useful for creating sample definition files from genotype "
        ".sample/.fam files that have the sample IDs set to a negative value"
        " to indicate that they are missing."
    )
    return parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _parse_cmd_args(parser):
    """Parse the command line arguments.

    Parameters
    ----------
    parser : `argparse.ArgumentParser`
        The argparse parser object with arguments added.

    Returns
    -------
    args : `argparse.Namespace`
        The argparse namespace object containing the arguments
    """
    args = parser.parse_args()
    args.inputs = [utils.get_full_path(i) for i in args.inputs]
    args.outprefix = utils.get_full_path(args.outprefix)

    # TODO: You can perform checks on the arguments here if you want
    return args


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _read_paths(path_file):
    """Read in the includes/excludes file. If the file is NoneType an empty
    list is returned.

    Parameters
    ----------
    path_file : `str` or `NoneType`
        The path to the includes/excludes file or NoneType if no excludes file
        is supplied.

    Returns
    -------
    excludes : `list` of `str`
        Any file or directory excludes.
    """
    if path_file is None:
        return []

    return [i.strip() for i in open(path_file)]


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _sort_paths(paths):
    """Sort the excludes into file or directory excludes.

    Parameters
    ----------
    paths : `list` of `str`
        File and/or directories to be sorted.

    Returns
    -------
    file_paths : `list` of `str`
        Files to be included/excluded. If there are none this will be an empty
        list.
    dir_paths : `list` of `str`
        Directories to be included/excluded. If there are none this will be an
        empty list.
    """
    paths = [str(i) for i in paths]
    glob_paths = [i for i in paths if "*" in i]
    fixed_paths = [utils.get_full_path(i) for i in paths if "*" not in i]

    for i in glob_paths:
        for f in glob(i):
            fixed_paths.append(utils.get_full_path(i))

    file_paths = []
    dir_paths = []

    for i in fixed_paths:
        # They are PosixPaths here, so make sure they are set to str for
        # path comparisons
        # i = str(utils.get_full_path(i))

        try:
            open(i, 'rb').close()
            file_paths.append(i)
        except IsADirectoryError:
            dir_paths.append(i)
        except FileNotFoundError as e:
            raise FileNotFoundError(f"Can't find path: {i}") from e

    return file_paths, dir_paths


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def sample_search(inputs, excludes=None, verbose=False, tmpdir=None,
                  complex_resolve=False, encoding=parsers.UTF8,
                  allow_bad_eid=False, withdrawals=None):
    """Traverse the supplied directories grabbing UK Biobank sample IDs (eids).
    Skip and directories supplied in the excludes.

    Parameters
    ----------
    inputs : `list` of `str`
        Input files/directories to check. Must be at least one.
    excludes : `list` or `str`, optional, default: `NoneType`
        Any files or directories to exclude from the search.
    verbose : `bool` or `int`, optional, default: `False`
        Report progress if verbose > 1, If 1 then log messages only.
    tmpdir : `str`, optional, default: `NoneType`
        A temp directory to store the sample files, if not supplied then the
        system temp location is used.
    complex_resolve : `bool`, optional, default: `False`
        Use the complex resolver to attempt to order the samples.
    encoding : `str`, optional, default: `utf8`
        The encoding to use when opening the text files.
    allow_bad_eid : `bool`, optional, default: `False`
        Allow bad EIDs (UK Biobank sample IDs) to be output in the sample
        definition file. This is useful if you are just processing genotype
        files that contain negative numbers representing withdrawn samples.
    withdrawals : `list` of `str`, optional, default: `None`
        The sample IDs (EIDs) of withdrawn participants, these are output into
        the sample definition file.

    Returns
    -------
    samples : `list` of `str`
        Any UK Biobank sample IDs.
    bad_samples : `list` of (`str`, `str`)
        Details of any bad samples. Each tuple is the file name and sample ID.
    errors : `pd.DataFrame` or `NoneType`
        The error sample order error counts between the files. If only a single
        file was found this will be `NoneType`.
    overlaps : `pd.DataFrame` or `NoneType`
        The sample overlap counts between the files. If only a single file was
        found this will be `NoneType`.
    bad_files : `list` of `tuple`
        The files that were excluded due to sample order errors and their
        error count.
    equal_samples : `dict`
        The files that have exactly equivalent samples. The keys are sample
        list MD5 hashes and the values are lists of file names. The first file
        name in each list is the one that was processed.
    """
    logger = log.retrieve_logger(_SCRIPT_NAME, verbose=verbose)
    prog_verbose = log.progress_verbose(verbose=verbose)

    # Initialise excludes if NoneType
    excludes = excludes or []
    withdrawals = withdrawals or []

    # Sort the excludes into files or directories, this will also expand them
    # to full paths to be the same as input paths
    file_excludes, dir_excludes = _sort_paths(excludes)

    logger.info(f"search #inputs: {len(inputs)}")
    logger.info(f"excluding #files: {len(file_excludes)}")
    logger.info(f"excluding #dirs: {len(dir_excludes)}")
    logger.info(f"#withdrawals: {len(withdrawals)}")

    # Sort the excludes into files or directories, this will also expand them
    # to full paths to be the same as input paths
    infiles, indirs = _sort_paths(inputs)

    # Get a list of files that we want to search from directory paths that have
    # been given
    search_files = get_files(
        indirs, file_excludes=file_excludes, dir_excludes=dir_excludes,
        verbose=prog_verbose
    )

    # Get a list of files that we want to search from files that have been
    # given either directory or derived from globs
    search_files.extend(
        filter_files(
            infiles, file_excludes=file_excludes
        )
    )
    # print(len(search_files))
    # pp.pprint(search_files)

    # Make sure that we only have unique search files
    search_files = sorted(set(search_files))
    # print(len(search_files))

    if len(search_files) == 0:
        raise ValueError("No search files")

    logger.info(f"Found #search files: {len(search_files)}")

    tqdm_kwargs = dict(
        desc="[info] searching files...",
        leave=False,
        unit=" file(s)",
        disable=not prog_verbose
    )

    logger.info(f"Write DB to: {tmpdir}")

    all_bad_samples = []
    with logging_redirect_tqdm(loggers=[logger]):
        with SampleOrderResolver(tmpdir=tmpdir) as resolver:
            for f, ext in tqdm(search_files, **tqdm_kwargs):
                try:
                    samples, bad_samples = _FILE_TYPES[ext](
                        f, verbose=prog_verbose, encoding=encoding,
                        allow_bad_eid=allow_bad_eid
                    )
                    resolver.add_list(f, samples)
                    all_bad_samples.extend(bad_samples)
                    if len(bad_samples) > 0:
                        logger.error(
                            f"bad samples in {f}: {len(bad_samples)} samples"
                        )
                except Exception as e:
                    # raise
                    logger.error(
                        f"issue parsing {ext} file: {str(e)}: {f}"
                    )
            # Log the number of samples
            logger.info(f"Found #samples: {len(resolver)}")

            try:
                errors_matrix, overlap_matrix = resolver.compare_lists(
                    verbose=prog_verbose
                )
                # Make a copy as they are modified internally
                errors_matrix = errors_matrix.copy()
                overlap_matrix = overlap_matrix.copy()
            except ValueError:
                if resolver.nlists != 1:
                    raise
                errors_matrix, overlap_matrix = None, None

            try:
                samples = resolver.resolve(complex=complex_resolve,
                                           verbose=prog_verbose)
            except errors.ResolveError as e:
                # Did a complex resolve fail?
                if complex_resolve is True and len(e.samples) == 0:
                    logger.error(
                        f"{e.args[0]}, falling back to complex=False"
                    )
                    try:
                        # Try non-complex instead
                        samples = resolver.resolve(
                            complex=False, verbose=prog_verbose
                        )
                    except errors.ResolveError as e:
                        # Non-complex has failed
                        logger.error(e.args[0])
                        samples = e.samples
                else:
                    # Non-complex has failed the first time
                    logger.error(e.args[0])
                    samples = e.samples

            # Add in the sample withdrawals
            start_withdrawal_len = len(withdrawals)
            for i in samples:
                withdrawn = False
                if i[0] in withdrawals:
                    withdrawn = True
                    withdrawals.pop(withdrawals.index(i[0]))
                # Merge the with drawn status with what exists.
                i[3] |= withdrawn

            if len(withdrawals) > 0:
                logger.warning(
                    "Withdrawn samples not found in the sample list: "
                    f"{len(withdrawals)}/{start_withdrawal_len} samples"
                )

            bad_files = resolver.removed_lists
            equal_samples = resolver.checksums

    return (
        samples, all_bad_samples, errors_matrix, overlap_matrix,
        bad_files, equal_samples
    )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_files(indirs, file_excludes=None, dir_excludes=None, verbose=False):
    """Get all the files that we want to search.

    Parameters
    ----------
    indirs : `list` of `str`
        Input directories to check. Must be at least one.
    file_excludes : `list` or `str`, optional, default: `NoneType`
        Any files or directories to exclude from the search.
    dir_excludes : `list` or `str`, optional, default: `NoneType`
        Any directories to exclude from the search.
    verbose : `bool`, optional, default: `False`
        Report progress.

    Returns
    -------
    matching_paths : `list` of `tuple`
        The files that we want to search. The first element of the nested
        tuple is the full file path and the second is the file extension that
        was matched.
    """
    # Resolve the input directories to full paths
    indirs = [utils.get_full_path(i) for i in indirs]

    # Make sure some input directories have been supplied
    if len(indirs) == 0:
        return []
        # raise ValueError("No input directories")

    # Make sure the input directories are directories
    for i in indirs:
        if os.path.isdir(i) is False:
            raise NotADirectoryError(f"Root is not a directory: {i}")

    file_excludes = file_excludes or []
    dir_excludes = dir_excludes or []

    # Make sure the root paths are not in the excludes
    exclude_root = [i for i in file_excludes + dir_excludes if i in indirs]
    if len(exclude_root) > 1:
        raise ValueError(
            "One or more of your root directories is in the excludes"
        )

    extensions = list(_FILE_TYPES.keys())
    paths = []

    prog = tqdm(
        desc="[info] finding files...",
        leave=False,
        unit=" dirs",
        disable=not verbose
    )

    try:
        for root_dir in indirs:
            for root, dirs, files in os.walk(root_dir):
                if root in dir_excludes:
                    continue

                for f in files:
                    for e in extensions:
                        if f.lower().endswith(e):
                            full_path = os.path.join(root, f)
                            if full_path not in file_excludes:
                                paths.append((full_path, e))
                                prog.update(1)
    finally:
        prog.close()

    return paths


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def filter_files(infiles, file_excludes=None, verbose=False):
    """Get all the files that we want to search.

    Parameters
    ----------
    indirs : `list` of `str`
        Input directories to check. Must be at least one.
    file_excludes : `list` or `str`, optional, default: `NoneType`
        Any files or directories to exclude from the search.

    Returns
    -------
    matching_paths : `list` of `tuple`
        The files that we want to search. The first element of the nested
        tuple is the full file path and the second is the file extension that
        was matched.
    """
    extensions = list(_FILE_TYPES.keys())
    paths = []

    for f in infiles:
        f = str(f)
        if f in file_excludes:
            continue
        for e in extensions:
            if f.lower().endswith(e):
                paths.append((f, e))

    return paths


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_bgen(infile, verbose=False, encoding=parsers.UTF8,
               allow_bad_eid=False):
    """Parse the samples from a BGEN file.

    Parameters
    ----------
    infile : `str`
        The input bgen file.
    verbose : `bool`, optional, default: `False`
        Should progress be reported, this is ignored for this function.
    encoding : `str`, optional, default: `utf8`
        The encoding of the file (ignored).
    allow_bad_eid : `bool`, optional, default: `False`
        Allow bad EIDs (UK Biobank sample IDs) to be output in the sample
        definition file. This is useful if you are just processing genotype
        files that contain negative numbers representing withdrawn samples.

    Returns
    -------
    samples : `list` of `tuple`
        Each tuple has:

        0. The sample ID.
        1. The sex (set to 0 (unknown) for BGEN files).
        2. Is genotypes - set to True for BGEN files.
        3. The withdrawn status - samples IDs matching negative numbers are set
           to withdrawn.

    bad_samples : `list` of `tuple`
        Any bad sample identifiers that were found. Each tuple has:

        0. The input filename.
        1. The bad EID value that was found.
    """
    # Using pybgen as it is lightweight and I can skip index checking
    with PyBGEN(infile, _skip_index=True) as bgen:
        samples = bgen.samples

    if samples is None:
        raise ValueError(
            "BGEN file has no samples"
        )

    for s, i in zip(samples, range(1, len(samples))):
        if s != i:
            integer_samples = False
            break

    if integer_samples is True:
        raise ValueError(
            "BGEN file has integer samples, probably no sample data in the"
            " file"
        )

    parsed_samples = []
    bad_samples = []
    for i in samples:
        withdrawn = False
        try:
            tc.parse_eid(i)
        except errors.EidError as e:
            if allow_bad_eid is False:
                bad_samples.append((infile, e.eid))
                continue

            # We will update the withdrawn status if the bad EID is a
            # negative number
            if _WITHDRAWN_EID_REGEXP.match(e.eid):
                withdrawn = True

        parsed_samples.append((i, 0, True, withdrawn))
    return parsed_samples, bad_samples


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_sample(infile, verbose=False, encoding=parsers.UTF8,
                 allow_bad_eid=False):
    """Parse the samples from a BGEN sample file.

    Parameters
    ----------
    infile : `str`
        The input sample file.
    verbose : `bool`, optional, default: `False`
        Should progress be reported, this is ignored for this function.
    encoding : `str`, optional, default: `utf8`
        The encoding of the file.
    allow_bad_eid : `bool`, optional, default: `False`
        Allow bad EIDs (UK Biobank sample IDs) to be output in the sample
        definition file. This is useful if you are just processing genotype
        files that contain negative numbers representing withdrawn samples.

    Returns
    -------
    samples : `list` of `tuple`
        Each tuple has:

        0. The sample ID.
        1. The sex 1, 2 or -1 (unknown).
        2. Is genotypes - set to True for sample files.
        3. The withdrawn status - samples IDs matching negative numbers are set
           to withdrawn.

    bad_samples : `list` of `tuple`
        Any bad sample identifiers that were found. Each tuple has:

        0. The input filename.
        1. The bad EID value that was found.
    """
    samples = []
    bad_samples = []

    tqdm_kwargs = dict(
        desc=f"[info] parsing {os.path.basename(infile)}...",
        unit=" row(s)",
        leave=False,
        disable=not verbose
    )

    with parsers.BgenSampleFile(
            infile, encoding=encoding, use_dict=True
    ) as p:
        for row in tqdm(p, **tqdm_kwargs):
            withdrawn = False
            try:
                row = p.format_row(row)
            except errors.EidError as e:
                if allow_bad_eid is False:
                    bad_samples.append((infile, e.eid))
                    continue
                row = p.format_row(row, except_error=[errors.EidError])

                # We will update the withdrawn status if the bad EID is a
                # negative number
                if _WITHDRAWN_EID_REGEXP.match(e.eid):
                    withdrawn = True

            sex = row[col.SAMPLE_SEX.name]
            iid = row[col.SAMPLE_ID2.name]
            samples.append((iid, sex, True, withdrawn))
    return samples, bad_samples


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_fam(infile, verbose=False, encoding=parsers.UTF8,
              allow_bad_eid=False):
    """Parse the samples from a plank FAM file.

    Parameters
    ----------
    infile : `str`
        The input fam file.
    verbose : `bool`, optional, default: `False`
        Should progress be reported, this is ignored for this function.
    encoding : `str`, optional, default: `utf8`
        The encoding of the file.
    allow_bad_eid : `bool`, optional, default: `False`
        Allow bad EIDs (UK Biobank sample IDs) to be output in the sample
        definition file. This is useful if you are just processing genotype
        files that contain negative numbers representing withdrawn samples.

    Returns
    -------
    samples : `list` of `tuple`
        Each tuple has:

        0. The sample ID.
        1. The sex 1, 2 or -1 (unknown).
        2. Is genotypes - set to True for FAM files.
        3. The withdrawn status - samples IDs matching negative numbers are set
           to withdrawn.

    bad_samples : `list` of `tuple`
        Any bad sample identifiers that were found. Each tuple has:

        0. The input filename.
        1. The bad EID value that was found.
    """
    samples = []
    bad_samples = []

    tqdm_kwargs = dict(
        desc=f"[info] parsing {os.path.basename(infile)}...",
        unit=" row(s)",
        leave=False,
        disable=not verbose
    )

    with parsers.PlinkFamFile(
            infile, encoding=encoding, use_dict=True
    ) as p:
        for row in tqdm(p, **tqdm_kwargs):
            withdrawn = False
            try:
                row = p.format_row(row)
            except errors.EidError as e:
                if allow_bad_eid is False:
                    bad_samples.append((infile, e.eid))
                    continue
                row = p.format_row(row, except_error=[errors.EidError])

                # We will update the withdrawn status if the bad EID is a
                # negative number
                if _WITHDRAWN_EID_REGEXP.match(e.eid):
                    withdrawn = True

            sex = row[col.SAMPLE_SEX.name]
            iid = row[col.FAM_IID.name]
            samples.append((iid, sex, True, withdrawn))
    return samples, bad_samples


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_sdef(infile, verbose=False, encoding=parsers.UTF8,
               allow_bad_eid=False):
    """Parse the samples from a sample definition file.

    Parameters
    ----------
    infile : `str`
        The input fam file.
    verbose : `bool`, optional, default: `False`
        Should progress be reported, this is ignored for this function.
    encoding : `str`, optional, default: `utf8`
        The encoding of the file.
    allow_bad_eid : `bool`, optional, default: `False`
        Allow bad EIDs (UK Biobank sample IDs) to be output in the sample
        definition file. This is useful if you are just processing genotype
        files that contain negative numbers representing withdrawn samples.

    Returns
    -------
    samples : `list` of `tuple`
        Each tuple has:

        0. The sample ID.
        1. The sex 1, 2 or -1 (unknown).
        2. Is genotypes - set to True for FAM files.
        3. The withdrawn status - samples IDs matching negative numbers are set
           to withdrawn and will override what is in the existing file.

    bad_samples : `list` of `tuple`
        Any bad sample identifiers that were found. Each tuple has:

        0. The input filename.
        1. The bad EID value that was found.
    """
    samples = []
    bad_samples = []

    tqdm_kwargs = dict(
        desc=f"[info] parsing {os.path.basename(infile)}...",
        unit=" row(s)",
        leave=False,
        disable=not verbose
    )

    with parsers.SampleDefinitionFile(
            infile, encoding=encoding, use_dict=True
    ) as p:
        for row in tqdm(p, **tqdm_kwargs):
            withdrawn = False
            try:
                row = p.format_row(row)
            except errors.EidError as e:
                if allow_bad_eid is False:
                    bad_samples.append((infile, e.eid))
                    continue
                row = p.format_row(row, except_error=[errors.EidError])

                # We will update the withdrawn status if the bad EID is a
                # negative number
                if _WITHDRAWN_EID_REGEXP.match(e.eid):
                    withdrawn = True

            # Merge the withdrawn sample status
            row[3] |= withdrawn
            samples.append(row[:4])
    return samples, bad_samples


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_flat(infile, verbose=False, encoding=parsers.UTF8,
               allow_bad_eid=False):
    """Parse the samples from a flat file with the eid column as the first
    column.

    Parameters
    ----------
    infile : `str`
        The input fam file.
    verbose : `bool`, optional, default: `False`
        Should progress be reported, this is ignored for this function.
    encoding : `str`, optional, default: `utf8`
        The encoding of the file.
    allow_bad_eid : `bool`, optional, default: `False`
        Allow bad EIDs (UK Biobank sample IDs) to be output in the sample
        definition file. This is useful if you are just processing genotype
        files that contain negative numbers representing withdrawn samples.

    Returns
    -------
    samples : `list` of `tuple`
        Each tuple has:

        0. The sample ID.
        1. The sex 1, 2 or -1 (unknown).
        2. Is genotypes - set to True for FAM files.
        3. The withdrawn status - set to False.
    bad_samples : `list` of `tuple`
        Any bad sample identifiers that were found. Each tuple has:

        0. The input filename.
        1. The bad EID value that was found.
    """
    samples = []
    bad_samples = []

    sex_recode = {
        '': 0,
        '0': 2,
        '1': 1
    }

    tqdm_kwargs = dict(
        desc=f"[info] parsing {os.path.basename(infile)}...",
        unit=" row(s)",
        leave=False,
        disable=not verbose
    )

    open_method = utils.get_open_method(infile)
    fr, d, hh, sr = header.detect_header(
        infile, open_method=open_method
    )

    # I am deliberately only using the delimiter as the full dialect causes
    # errors
    with parsers.EidFlatFile(
            infile, encoding=encoding, use_dict=True, delimiter=d.delimiter
    ) as p:
        parse_sex = False
        if _GENETIC_SEX_FIELD in p.header_row:
            parse_sex = True

        for row in tqdm(p, **tqdm_kwargs):
            try:
                row = p.format_row(row)
            except errors.EidError as e:
                if allow_bad_eid is False:
                    bad_samples.append((infile, e.eid))
                    continue
                row = p.format_row(row, except_error=[errors.EidError])

            sex = col.SAMPLE_SEX.missing[0]

            if parse_sex is True:
                sex = row[_GENETIC_SEX_FIELD]
                try:
                    sex = sex_recode[sex]
                except KeyError as e:
                    raise KeyError(f"Bad genetic sex value: {sex}") from e
            iid = row[col.EID.name]
            samples.append((iid, sex, False, False))
    return samples, bad_samples


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_vcf(infile, verbose=False, encoding=parsers.UTF8,
              allow_bad_eid=False):
    """Parse the samples from a VCF file.

    Parameters
    ----------
    infile : `str`
        The input fam file.
    verbose : `bool`, optional, default: `False`
        Should progress be reported, this is ignored for this function.
    encoding : `str`, optional, default: `utf8`
        The encoding of the file.
    allow_bad_eid : `bool`, optional, default: `False`
        Allow bad EIDs (UK Biobank sample IDs) to be output in the sample
        definition file. This is useful if you are just processing genotype
        files that contain negative numbers representing withdrawn samples.

    Returns
    -------
    samples : `list` of `tuple`
        Each tuple has:

        0. The sample ID.
        1. The sex 1, 2 or -1 (unknown).
        2. Is genotypes - set to True for FAM files.
        3. The withdrawn status - samples IDs matching negative numbers are set
           to withdrawn.

    bad_samples : `list` of `tuple`
        Any bad sample identifiers that were found. Each tuple has:

        0. The input filename.
        1. The bad EID value that was found.
    """
    samples = []
    bad_samples = []

    with parsers.BasicVcfParser(
            infile, encoding=encoding, use_dict=True
    ) as p:
        for s in p.samples:
            withdrawn = False
            try:
                tc.parse_eid(s)
            except errors.EidError as e:
                if allow_bad_eid is False:
                    bad_samples.append((infile, e.eid))
                    continue

                # We will update the withdrawn status if the bad EID is a
                # negative number
                if _WITHDRAWN_EID_REGEXP.match(e.eid):
                    withdrawn = True

            samples.append((s, 0, True, withdrawn))

    return samples, bad_samples


_FILE_TYPES = {
    '.bgen': parse_bgen,
    '.sample': parse_sample,
    '.fam': parse_fam,
    '.sdef': parse_sdef,
    '.tab': parse_flat,
    '.csv': parse_flat,
    '.txt': parse_flat,
    '.vcf': parse_vcf,
    '.tab.gz': parse_flat,
    '.csv.gz': parse_flat,
    '.txt.gz': parse_flat,
    '.vcf.gz': parse_vcf,
}
"""The file extensions we will look for (keys) and parsing functions for the
file types (values) (`dict`)
"""


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == '__main__':
    main()
