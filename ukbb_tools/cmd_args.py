"""Functions to add command line arguments to argparse. Things that a common in
many scripts.
"""
from ukbb_tools import constants as con


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def add_config_arg(parser):
    """Add a configuration file argument to the argument parser.

    Parameters
    ----------
    parser : `argparse.ArgumentParser`
        The argparse parser object with arguments added.
    """
    parser.add_argument(
        '-C', '--config', type=str, default=None,
        help="A path to the UKBB-tools configuration file. If not provided "
        "then the following search strategy is used. First UKBB_CONFIG"
        "environment variable is checked for the default config file name"
        f" ({con.DEFAULT_CONFIG_NAME}), then the path defined in the "
        "UKBB_PACKAGE_ENV environment is checked, finally, the root of "
        "the home directory is checked. If not found then an error is "
        "raised."
    )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def add_infile_args(parser, infile_optional=True):
    """Add input file arguments to the argument parser.

    This adds comment character, encoding, skiplines and delimiter arguments.

    Parameters
    ----------
    parser : `argparse.ArgumentParser`
        The argparse parser object with arguments added.
    infile_optional : `bool`, optional, default: `True`
        This adds an optional input file flag, to indicate that the input file
        can come from STDIN.
    """
    if infile_optional is True:
        parser.add_argument(
            '-i', '--infile', type=str, default=None,
            help="The path to the input file. If not supplied then the input"
            " is assumed to come from STDIN. All input files are expected to"
            " have a header row."
        )
    parser.add_argument(
        '--comment-char', type=str, default=None,
        help="Skip any leading rows containing this comment character."
        "The default is to not have any comment character recognition."
    )
    parser.add_argument(
        '--encoding', type=str, default='UTF8',
        help="The encoding of the input file."
    )
    parser.add_argument(
        '--skiplines', type=int, default=0,
        help="Skip a fixed number of rows of the input file before identifying"
        " the header row."
    )
    parser.add_argument(
        '--delimiter', type=str, default="\t",
        help="The delimiter of the input file."
    )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def add_optional_outfile_args(parser):
    """Add output file arguments to the argument parser.

    Parameters
    ----------
    parser : `argparse.ArgumentParser`
        The argparse parser object with arguments added.
    """
    parser.add_argument(
        '-o', '--outfile', type=str, default=None,
        help="The path to the output file. If not supplied then the output"
        " is assumed go to STDOUT."
    )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def add_tmpdir_arg(parser):
    """Add a temporary directory option to the argument parser.

    Parameters
    ----------
    parser : `argparse.ArgumentParser`
        The argparse parser object with arguments added.
    """
    parser.add_argument(
        '-T', '--tmpdir', type=str, default=None,
        help="The path to a temporary directory to use. If not supplied then"
        " the default system location is used."
    )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def add_verbose_arg(parser):
    """Add a verbosity argument to the argument parser. This is added as a
    count argument.

    Parameters
    ----------
    parser : `argparse.ArgumentParser`
        The argparse parser object with arguments added.
    """
    parser.add_argument(
        '-v', '--verbose',  action="count",
        help="Give more output, use -vv for progress monitoring"
    )
