"""Common functions across the package
"""
from ukbb_tools import constants as con
from sqlalchemy.engine import Engine
from sqlalchemy import event
from pyaddons import utils
import os


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@event.listens_for(Engine, "connect")
def set_sqlite_pragma(dbapi_connection, connection_record):
    """Called when the engine connects. This sets up the foreign keys pragma
    for SQLite. Otherwise it is tolerant to integrity errors.
    """
    cursor = dbapi_connection.cursor()
    try:
        cursor.execute("PRAGMA foreign_keys=ON")
    except Exception:
        # Probably not SQLite but do not know how to tell!
        pass
    cursor.close()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def print_orm_obj(obj, exclude=[]):
    """Prints the contents of the SQL Alchemy ORM object.

    Parameters
    ----------
    obj : `sqlalchemy.ext.declarative.Base`
        The SQLAlchemy declarative base model object. This should have a
        __table__ attribute that contains the corresponding SQLAlchemy table
        object.
    exclude : `NoneType` or `list` or `str`, optional, default `NoneType`
        A list of attributes (these correspond to table columns) that you do
        not want to include in the string.

    Returns
    -------
    formatted_string : `str`
        A formatted string that can be printed. The string contains the
        attributes of the object and the data they contain.

    Notes
    -----
    This is designed to be called from an SQLAlchemy ORM model object and it
    in the __repr__ method. It simply frovides a formatted string that has the
    data in the object.
    """
    exclude = exclude or []

    attr_values = []
    for i in obj.__table__.columns:
        if i.name in exclude:
            continue
        val = getattr(obj, i.name)
        if val.__class__.__name__ != "method":
            attr_values.append("{0}={1}".format(i.name, val))

    # Turn outlist into a string and return
    return "<{0}({1})>".format(obj.__class__.__name__, ", ".join(attr_values))


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_package_dir(override_dir=None):
    """Get the package directory path.

    Parameters
    ----------
    override_dir : `str`, optional, default: `NoneType`
        A directory path that will be used in preference to any of the
        environment variables or package default locations. If given it will
        just be returned as a full path.

    Returns
    -------
    package_dir : `str`
        The full path to the package directory.
    """
    package_dir = None
    if override_dir is not None:
        package_dir = utils.get_full_path(override_dir)
    else:
        try:
            # Check if the environment variable is set
            package_dir = utils.get_full_path(os.environ[con.UKBB_PACKAGE_ENV])
        except KeyError:
            # Fall back to the package defined path
            package_dir = con.PACKAGE_DIR_PATH
    os.makedirs(package_dir, exist_ok=True)
    return package_dir
