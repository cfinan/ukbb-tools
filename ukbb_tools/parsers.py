"""Base parsers for parsing flat files.
"""
from ukbb_tools import columns as col, errors
from pyaddons import utils
from pyaddons.flat_files import header
from encodings import utf_8, latin_1
# from warnings import warn
import csv
import sys
import pathlib
# import pprint as pp


UTF8 = utf_8.getregentry().name
"""Constant for UTF8 encoding (`str`)
"""
LATIN1 = latin_1.getregentry().name
"""Constant for LATIN1 encoding (`str`)
"""
LENGTH_HEADER_CHECK = 1
"""A constant it indicate that each data row should be checked against
the header length (`int`).
"""
MIN_LENGTH_HEADER_CHECK = 2
"""A constant it indicate that each data row should be checked against
a minimum header length (`int`).
"""
HASH_COMMENT = '#'
"""A hash comment character (`str`).
"""

# Make sure csv can deal with the longest lines possible
csv.field_size_limit(sys.maxsize)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class ParseColumn(object):
    """A representation of a specific column in a file context.

    Parameters
    ----------
    column : `umls_tools.columns.ColName`
        A column names tuple that describes the column.
    do_parse : `bool`, optional, default: `True`
        A flag to indicate to the parser if this column should be parsed.
    allow_missing : `bool`, optional, default: `True`
        Should missing values be allowed when parsing. If True and a missing
        value is detected no error is thrown and the first value of the
        ``column`` missing attributes is returned. If there are none then
        NoneType is taken as the missing value.
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, column, do_parse=True, allow_missing=True):
        self.name = column.name
        self._parse = column.type
        self.missing = column.missing
        self.allow_missing = allow_missing
        self.do_parse = do_parse
        self.missing = column.missing or [None]

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def parse(self, value):
        """Parse the value.

        Parameters
        ----------
        value : `str`
            The value to parse.

        Returns
        -------
        parsed_value : `any`
            The parsed value, the value is parsed according to the column
            specification.

        Notes
        -----
        An exception will be raised if the value matches a missing
        specification and the column is not allowed missing values.
        """
        try:
            return self._parse(value)
        except Exception as e:
            if self.allow_missing is False:
                e.args = (
                    f"Parse error in column: '{self.name}': {e.args[0]}",
                    *e.args[1:]
                )
                raise
            elif self.allow_missing is True and value not in self.missing:
                e.args = (
                    f"Parse error in column: '{self.name}': {e.args[0]}",
                    *e.args[1:]
                )
                raise
            return self.missing[0]


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class _BaseParser(object):
    """A base parser, do not use directly.

    Parameters
    ----------
    infile : `str` or `file`
        The input file to parse, this can be supplied as a filename or as a
        file object. If a file object, then the context manager will not handle
        the opening/closing of the file.
    indexes : `list` of `str`
        Paths to any file indexes. If not supplied then index files will be
        looked for based on the name of the infile. However, if their file name
        is non-standard, you can supply them here.
    has_header : `bool`, optional, default: `False`
        Does the input file have a header, if not then various checks will
        happen to determine if that is the case (warnings will be given).
        The column specifications will be used as the header columns.
    header : `list` or `ukbb_tools.parsers.ParseColumn`
        A header specification.
    header_check : `int`, optional, default: `1`
        What type of header checking should be performed.
        `ukbb_tools.LENGTH_HEADER_CHECK` will check that
        each data row is the same length as the header row.
        `ukbb_tools.MIN_LENGTH_HEADER_CHECK` will check that
        each data row is at least as long as the header row.
    encoding : `str`, optional, default: `utf8`
        The encoding of the input file. Only used if a file object has not been
        supplied.
    comment_char : `str`, optional, default: `NoneType`
        A comment character, any rows starting with this are skipped.
    skiplines : `int`, optional, default: `0`
        Skip this number of fixed lines before attempting to read any
        headers/data from the file. This is only used if a file object has not
        been supplied.
    use_dict : `bool`, optional, default: `False`
        Return rows as dictionaries rather than lists. Internally this will use
        `csv.DictReader` instead of `csv.reader`.
    **csv_kwargs
        Any keyword arguments usually supplied to Python `csv.reader` or
        `csv.DictReader`. If none are supplied then the dialect is attempted to
        be sniffed.
    """

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, infile, indexes=None, has_header=True, header=None,
                 header_check=LENGTH_HEADER_CHECK, encoding=UTF8,
                 comment_char=None, skiplines=0, use_dict=False,
                 **csv_kwargs):
        self._infile = infile
        self._indexes = indexes
        self._has_header = has_header
        self._header = header
        self._encoding = encoding
        self._comment_char = comment_char
        self._skiplines = skiplines
        self._csv_kwargs = csv_kwargs
        self._row_number = 0
        self._use_dict = use_dict
        self._is_open = False
        self._is_iter = False
        self._header_parsed = False
        self._header_len = None
        self._header_row = None
        self._row_len_check_func = None
        self._parse_cols = []
        self.skipped_rows = []

        # Sort out the header check values
        if header_check == LENGTH_HEADER_CHECK:
            self._row_len_check_func = self.row_header_equal
        elif header_check == MIN_LENGTH_HEADER_CHECK:
            self._row_len_check_func = self.row_header_gt
        else:
            raise ValueError(f"Bad header check value: '{header_check}'")

        # Adjust the processing function depending on dict or list
        self.format_row = self.format_list_row
        if use_dict is True:
            self.format_row = self.format_dict_row

        # Determine if we should treat the input file as a file object or a
        # path that should be opened
        if not isinstance(self._infile, (str, pathlib.Path, pathlib.PosixPath)):
            raise TypeError("Input file should be a string or Path object")

        # Make sure expected header columns are defined and fish out any
        # columns that need parsing
        self._parse_column_defs()

        # Will hold the open file and CSV reader
        self._file_object = None
        self._file_reader = None

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        """Pretty printing
        """
        args = []
        for i in ['infile', 'has_header', 'encoding',
                  'comment_char', 'skiplines', 'row_number', 'is_open']:
            args.append(f"{i}='{getattr(self, f'_{i}')}'")
        return "<{0}({1})".format(self.__class__.__name__, ",".join(args))

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __enter__(self):
        """Enter the context manager.
        """
        self.open()
        return self

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __exit__(self, *args):
        """Exit the context manager, this will close the file, unless a file
        object has been supplied, in which case it is left.
        """
        self.close()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __iter__(self):
        """Inter iteration.
        """
        return self

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __next__(self):
        """Next row in iteration. Once iteration is started index seeking will
        be disabled.
        """
        self._is_iter = True

        try:
            row = next(self._file_reader)

            # Deal with comments
            # while row has a comment...
        except TypeError:
            if self._file_reader is None:
                raise TypeError(
                    "Reader not initialised, did you open the file?"
                )
            else:
                raise

        return row
        # return self._process_row(row)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def row_number(self):
        """Get the current row number in the file, this will not be accurate if
        the user manually alters the seek position in the file object (`int`)
        """
        return self._row_number

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def is_open(self):
        """Is the file open? (`bool`)
        """
        return self._is_open

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def header_row(self):
        """Get the header row (`list` of `str`)
        """
        return self._header_row

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def csv_kwargs(self):
        """Get the CSV keyword arguments, this returns a copy (`dict`)
        """
        return dict(self._csv_kwargs)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def file(self):
        """Get the file object (`File`)
        """
        return self._file_object

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _parse_column_defs(self):
        """Go through the header column definitions and extract the columns
        that we want to apply parsing to.

        Raises
        ------
        ValueError
            If there are no header columns supplied.
        """
        if len(self._header) == 0:
            raise ValueError("There are no header columns defined")

        # Now get the columns that need parsing
        self._parse_cols = []
        for idx, i in enumerate(self._header):
            if i.do_parse is True:
                self._parse_cols.append((idx, i))

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def row_header_equal(self, row):
        """Check that the row length is greater than or equal to the header
        length.

        Parameters
        ----------
        row : `list` or `dict`
            The row to check.

        Returns
        -------
        ok : `bool`
            True if the row passes False if not.
        """
        return len(self._header_row) == len(row)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def row_header_gt(self, row):
        """Check that the row length is greater than or equal to the header
        length.

        Parameters
        ----------
        row : `list` or `dict`
            The row to check.

        Returns
        -------
        ok : `bool`
            True if the row passes False if not.
        """
        return len(self._header_row) >= len(row)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def open(self):
        """Open the file.
        """
        if self._is_open is False:
            open_method = utils.get_open_method(self._infile)
            if self._has_header is False:
                fr, d, hh, sl = header.detect_header(
                    self._infile,
                    open_method=open_method,
                    encoding=self._encoding,
                    skiplines=self._skiplines,
                    comment=self._comment_char,
                    **self._csv_kwargs
                )
                self._header_row = [f"column{i}" for i in range(len(fr))]
                for idx, i in enumerate(self._header):
                    self._header_row[idx] = i.name

            # header_row is initialised to NoneType in __init__
            fieldnames = self._header_row

            self._file_object = open_method(
                self._infile, 'rt', encoding=self._encoding
            )

            self.skipped_rows = header.skip_to_header(
                self._file_object, skiplines=self._skiplines,
                comment=self._comment_char
            )

            if self._use_dict is False:
                self._file_reader = csv.reader(
                    self._file_object, **self._csv_kwargs
                )

                if self._has_header is True:
                    self._header_row = next(
                        csv.reader(
                            [self._file_object.readline()],
                            **self._csv_kwargs
                        )
                    )
            else:
                self._file_reader = csv.DictReader(
                    self._file_object, fieldnames=fieldnames,
                    **self._csv_kwargs
                )
                if self._has_header is True:
                    self._header_row = self._file_reader.fieldnames

            for i, j in zip(self._header, self._header_row[:len(self._header)]):
                if i.name != j:
                    raise ValueError(
                        f"Unexpected header value: '{i.name}' got '{j}'"
                    )

            # TODO: Search for indexes
            self._is_open = True

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def close(self):
        """Close the file. Calling this will close the file even if a file
        object has been supplied.
        """
        if self._is_open is True:
            self._file_object.close()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def format_list_row(self, row, except_error=[]):
        """Format a data row that has been supplied as a list.

        Parameters
        ----------
        row : `list` of `str`
            A row data row extracted from the file.
        except_error : `list` of `Exception`
            Exceptions to ignore during the row parsing.

        Returns
        -------
        formatted_row : `list`
            A processed data row. The row has been processed according to the
            specifications of the parser.
        """
        # Check the row length
        self._row_len_check_func(row)

        for idx, i in self._parse_cols:
            try:
                row[idx] = i.parse(row[idx])
            except Exception as e:
                if e.__class__ not in except_error:
                    raise
        return row

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def format_dict_row(self, row, except_error=[]):
        """Format a data row that has been supplied as a dict.

        Parameters
        ----------
        row : `dict`
            A row data row extracted from the file.
        except_error : `list` of `Exception`
            Exceptions to ignore during the row parsing.

        Returns
        -------
        formatted_row : `dict`
            A processed data row. The row has been processed according to the
            specifications of the parser.
        """
        # Check the row length
        self._row_len_check_func(row)

        for idx, i in self._parse_cols:
            try:
                row[i.name] = i.parse(row[i.name])
            except Exception as e:
                if e.__class__ not in except_error:
                    raise
        return row

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def fetch(self, colname, colvalues):
        """Fetch specific data rows by using the file index. This will only
        work if file indexes are specified/detected.

        Parameters
        ----------
        col_name : `str`
            An indexed column name.
        col_values : `list` of `any`
            One or more column values to search for.

        Notes
        -----
        Fetch will be disabled if iterating through the file.
        """
        # Check iteration
        if self._isiter is True:
            raise RuntimeError("Catch fetch from index if you are iterating")


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class BgenSampleFile(_BaseParser):
    """A parser for .sample files that are usually associated with BGEN files.

    Parameters
    ----------
    infile : `str` or `file`
        The input file to parse, this can be supplied as a filename or as a
        file object. If a file object, then the context manager will not handle
        the opening/closing of the file.
    encoding : `str`, optional, default: `utf8`
        The encoding of the input file. Only used if a file object has not been
        supplied.
    use_dict : `bool`, optional, default: `False`
        Return rows as dictionaries rather than lists. Internally this will use
        `csv.DictReader` instead of `csv.reader`.
    delimiter : `str`, optional, default: ` `
        The delimiter for the sample file, default is whitespace.

    Notes
    -----
    This expects the ID columns to be EIDs and tests for that.
    """
    COLUMNS = [
        ParseColumn(col.SAMPLE_ID1, allow_missing=False),
        ParseColumn(col.SAMPLE_ID2, allow_missing=False),
        ParseColumn(col.SAMPLE_MISS, allow_missing=False),
        ParseColumn(col.SAMPLE_SEX, allow_missing=True),
    ]

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, infile, encoding=UTF8, use_dict=False, delimiter=' '):
        super().__init__(
            infile, indexes=None, has_header=True, header=self.COLUMNS,
            header_check=MIN_LENGTH_HEADER_CHECK, encoding=encoding,
            comment_char=HASH_COMMENT, skiplines=0, use_dict=use_dict,
            delimiter=delimiter
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def desc_row(self):
        """Return the description row, this is always a list (`list`).
        """
        return list(self._desc_row)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def open(self):
        """Open the file.
        """
        super().open()
        self._desc_row = csv.reader(
            [self.file.readline()], **self.csv_kwargs
        )


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class PlinkFamFile(_BaseParser):
    """A parser for .fam files that are usually associated with Plink files.

    Parameters
    ----------
    infile : `str` or `file`
        The input file to parse, this can be supplied as a filename or as a
        file object. If a file object, then the context manager will not handle
        the opening/closing of the file.
    encoding : `str`, optional, default: `utf8`
        The encoding of the input file. Only used if a file object has not been
        supplied.
    use_dict : `bool`, optional, default: `False`
        Return rows as dictionaries rather than lists. Internally this will use
        `csv.DictReader` instead of `csv.reader`.
    delimiter : `str`, optional, default: ` `
        The delimiter for the sample file, default is whitespace.

    Notes
    -----
    This expects the ID columns to be EIDs and tests for that.
    """
    COLUMNS = [
        ParseColumn(col.FAM_FID, allow_missing=False),
        ParseColumn(col.FAM_IID, allow_missing=False),
        ParseColumn(col.FAM_FATHER, allow_missing=True),
        ParseColumn(col.FAM_MOTHER, allow_missing=True),
        ParseColumn(col.SAMPLE_SEX, allow_missing=True),
    ]

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, infile, encoding=UTF8, use_dict=False, delimiter=' '):
        super().__init__(
            infile, indexes=None, has_header=False, header=self.COLUMNS,
            header_check=MIN_LENGTH_HEADER_CHECK, encoding=encoding,
            comment_char=HASH_COMMENT, skiplines=0, use_dict=use_dict,
            delimiter=delimiter
        )


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class EidFlatFile(_BaseParser):
    """A parser for a flat delimited file that should have the column ``eid``
    as the first column.

    Parameters
    ----------
    infile : `str` or `file`
        The input file to parse, this can be supplied as a filename or as a
        file object. If a file object, then the context manager will not handle
        the opening/closing of the file.
    encoding : `str`, optional, default: `utf8`
        The encoding of the input file. Only used if a file object has not been
        supplied.
    use_dict : `bool`, optional, default: `False`
        Return rows as dictionaries rather than lists. Internally this will use
        `csv.DictReader` instead of `csv.reader`.
    delimiter : `str`, optional, default: `NoneType`
        The delimiter for the sample file, if not supplied and no dialect is
        supplied then the default will be a tab.
    dialect : `csv.Dialect`, optional, default: `NoneType`
        A CSV dialect object to use.
    """
    COLUMNS = [
        ParseColumn(col.EID, allow_missing=False)
    ]

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, infile, encoding=UTF8, use_dict=False, delimiter=None,
                 dialect=None):
        csv_kwargs = {}
        if dialect is None and delimiter is None:
            csv_kwargs['delimiter'] = "\t"
        elif dialect is None:
            csv_kwargs['dialect'] = dialect

        super().__init__(
            infile, indexes=None, has_header=True, header=self.COLUMNS,
            header_check=MIN_LENGTH_HEADER_CHECK, encoding=encoding,
            comment_char=HASH_COMMENT, skiplines=0, use_dict=use_dict,
            **csv_kwargs
        )


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class SampleDefinitionFile(_BaseParser):
    """A parser for a flat delimited file that should have the column ``eid``
    as the first column.

    Parameters
    ----------
    infile : `str` or `file`
        The input file to parse, this can be supplied as a filename or as a
        file object. If a file object, then the context manager will not handle
        the opening/closing of the file.
    encoding : `str`, optional, default: `utf8`
        The encoding of the input file. Only used if a file object has not been
        supplied.
    use_dict : `bool`, optional, default: `False`
        Return rows as dictionaries rather than lists. Internally this will use
        `csv.DictReader` instead of `csv.reader`.
    """
    COLUMNS = [
        ParseColumn(col.EID, allow_missing=False),
        ParseColumn(col.SAMPLE_SEX, allow_missing=True),
        ParseColumn(col.IS_GENOTYPED, allow_missing=False),
        ParseColumn(col.HAS_WITHDRAWN, allow_missing=False)
    ]

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, infile, encoding=UTF8, use_dict=False):
        super().__init__(
            infile, indexes=None, has_header=True, header=self.COLUMNS,
            header_check=MIN_LENGTH_HEADER_CHECK, encoding=encoding,
            comment_char=HASH_COMMENT, skiplines=0, use_dict=use_dict,
            delimiter="\t"
        )


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class BasicVcfParser(_BaseParser):
    """A very basic VCF parser, do not use for anything serious.

    Parameters
    ----------
    infile : `str` or `file`
        The input file to parse, this can be supplied as a filename or as a
        file object. If a file object, then the context manager will not handle
        the opening/closing of the file.
    encoding : `str`, optional, default: `utf8`
        The encoding of the input file. Only used if a file object has not been
        supplied.
    use_dict : `bool`, optional, default: `False`
        Return rows as dictionaries rather than lists. Internally this will use
        `csv.DictReader` instead of `csv.reader`.
    """
    COLUMNS = [
        ParseColumn(col.VCF_CHROM, allow_missing=False),
        ParseColumn(col.VCF_POS, allow_missing=True),
        ParseColumn(col.VCF_ID, allow_missing=False),
        ParseColumn(col.VCF_REF, allow_missing=False),
        ParseColumn(col.VCF_ALT, allow_missing=False),
        ParseColumn(col.VCF_QUAL, allow_missing=True),
        ParseColumn(col.VCF_FILTER, allow_missing=False),
        ParseColumn(col.VCF_INFO, allow_missing=False),
        ParseColumn(col.VCF_FORMAT, allow_missing=False),
    ]

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, infile, encoding=UTF8, use_dict=False):
        super().__init__(
            infile, indexes=None, has_header=True, header=self.COLUMNS,
            header_check=MIN_LENGTH_HEADER_CHECK, encoding=encoding,
            comment_char="##", skiplines=0, use_dict=use_dict,
            delimiter="\t"
        )
        self.samples = []

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def open(self):
        """Open a VCF file.
        """
        super().open()
        self.samples = list(self.header_row[len(self.COLUMNS):])


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def read_sdef_file(sample_file, allow_withdrawn_samples=False,
                   use_dict=False):
    """Read in a sample definition file. This allows negative EIDs (i.e.
    withdrawn sample IDs). This reads everything into memory.

    Parameters
    ----------
    sample_file : `str`
        The path to the sample definition file (`.sdef`).
    allow_withdrawn_samples : `bool`, optional, default: `False`
        Allow for sample IDs that look like withdrawn samples, these are
        negative integers and do not conform to the EID format.
    use_dict : `bool`, optional, default: `False`
        Read each row as a dictionary, not a list.

    Returns
    -------
    samples : `list` of (`list` or `dict`)
        The source IDs, each sub list has the structure `eid`, sex (0,1,2),
        is genotyped (`bool`), has withdrawn (`bool`).

    Notes
    -----
    The file can be gzip compressed.
    """
    sid_col = 0
    if use_dict is True:
        sid_col = col.EID.name

    # Read in target sdef
    with SampleDefinitionFile(sample_file) as sfile:
        samples = []
        for row in sfile:
            try:
                sfile.format_list_row(row)
            except errors.EidError as e:
                if allow_withdrawn_samples is True:
                    try:
                        if int(row[sid_col]) >= 0:
                            raise
                    except TypeError:
                        raise e
                    sfile.format_list_row(row, except_error=[errors.EidError])
                else:
                    raise
            samples.append(row)
    return samples


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def read_withdrawn_file(withdrawn_file):
    """Read in the withdrawn sample file. This reads everything into memory.

    Parameters
    ----------
    withdrawn_file : `str`
        The path to the withdrawn files.

    Returns
    -------
    withdrawn_ids : `set` of `str`
        The withdrawn sample IDs.

    Notes
    -----
    The file can be gzip compressed.
    """
    open_method = utils.get_open_method(withdrawn_file)
    test_col = ParseColumn(col.EID, allow_missing=False)
    withdrawn_ids = set()

    with open_method(withdrawn_file, 'rt') as infile:
        for row in infile:
            withdrawn_ids.add(test_col.parse(row.strip()))
    return withdrawn_ids
