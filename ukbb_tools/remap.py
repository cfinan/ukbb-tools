"""A tool for mapping sample IDs between different UKBiobank applications. This
can handle either wide format files where the sample IDs are within a header
row, or long format files where the sample IDs are within a column in the file.
"""
# Importing the version number (in __init__.py) and the package name
from ukbb_tools import (
    __version__,
    __name__ as pkg_name,
    cmd_args,
    constants as con,
    common,
    config as ukbbconf,
    parsers,
    errors,
    columns as col
)
from pyaddons import log, utils
from pyaddons.flat_files import header
from tqdm import tqdm
import stdopen
import csv
import argparse
import sys
import os
import pprint as pp


# The name of the script
_SCRIPT_NAME = "ukbb-remap"
"""The name of the script (`str`)
"""
_DESC = __doc__
"""The program description (`str`)
"""

# Make sure csv can deal with the longest lines possible
csv.field_size_limit(sys.maxsize)

_ERROR_CHOICE = 'error'
_WITHDRAWN_CHOICE = 'withdrawn'
_REMOVE_CHOICE = 'remove'
_LONG_FORMAT = 'long'
_WIDE_FORMAT = 'wide'
_DEFAULT_LONG_COLUMN = 'eid'
_DEFAULT_WIDE_COLUMN = 1
_DEFAULT_ENCODING = 'UTF8'


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main():
    """The main entry point for the script.
    """
    # Initialise and parse the command line arguments
    parser = _init_cmd_args()
    args = _parse_cmd_args(parser)

    # Use logger.info to issue log messages
    logger = log.init_logger(
        _SCRIPT_NAME, verbose=args.verbose
    )
    log.log_prog_name(logger, pkg_name, __version__)
    log.log_args(logger, args)

    try:
        open_method = utils.get_open_method(args.outfile, allow_none=True)
        out_kwargs = dict(
            mode='wt',
            method=open_method,
            tmpdir=args.tmpdir,
            use_tmp=True
        )
        with stdopen.open(args.outfile, **out_kwargs) as fobj:
            writer = csv.writer(fobj, delimiter=args.delimiter,
                                lineterminator=os.linesep)
            for row in remap(
                    args.target_app, source=args.source_app,
                    infile=args.infile, config=args.config,
                    comment=args.comment_char, encoding=args.encoding,
                    skiplines=args.skiplines, delimiter=args.delimiter,
                    verbose=args.verbose, input_format=args.format,
                    allow_unknown_sex=args.allow_unknown_sex,
                    sample_column=args.column,
                    missing_action=args.missing,
                    withdrawn_action=args.withdrawn
            ):
                writer.writerow(row)
    except (BrokenPipeError, KeyboardInterrupt):
        # Python flushes standard streams on exit; redirect remaining
        # output to devnull to avoid another BrokenPipeError at shutdown
        devnull = os.open(os.devnull, os.O_WRONLY)
        os.dup2(devnull, sys.stdout.fileno())


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _init_cmd_args():
    """Initialise the command line arguments and return the parser.

    Returns
    -------
    args : `argparse.ArgumentParser`
        The argparse parser object with arguments added
    """
    parser = argparse.ArgumentParser(
        description=_DESC
    )
    parser.add_argument(
        'target_app', type=str,
        help="The target application to map to. This can either be a path"
        " to a mapping file (.sdef), or an application number that is defined"
        " in the mapping section of the configuration file."
    )
    cmd_args.add_config_arg(parser)
    cmd_args.add_infile_args(parser)
    cmd_args.add_optional_outfile_args(parser)
    parser.add_argument(
        '-S', '--source-app', type=str, default=None,
        help="The source application to map from. This can either be a path"
        " to a mapping file (.sdef), or an application number that is defined"
        " in the mapping section of the configuration file. It can also be"
        " undefined, in which case the application number in the configuration"
        " file is used."
    )
    parser.add_argument(
        '-U', '--allow-unknown-sex', action='store_true',
        help="Do not check sex differences in samples where unknown sex is"
        " present (0 in the .sdef file). If this is not set then unknown sex"
        "samples will generate errors."
    )
    parser.add_argument(
        '-M', '--missing', type=str,
        choices=[_ERROR_CHOICE, _WITHDRAWN_CHOICE, _REMOVE_CHOICE],
        default=_WITHDRAWN_CHOICE,
        help="The action to take when a sample exists in the input file but"
        " not in the mapping file. The withdrawn option indicates that the "
        "sample ID should be set to withdrawn, this is a incremented negative"
        " integer value, i.e. the first withdrawn sample is set to -1, second"
        " to -2."
    )
    parser.add_argument(
        '-W', '--withdrawn', type=str,
        choices=[_WITHDRAWN_CHOICE, _REMOVE_CHOICE],
        default=_WITHDRAWN_CHOICE,
        help="The action to take when a sample exists in the input file but"
        " but has withdrawn from the study. The withdrawn option indicates "
        "that  the sample ID should be set to withdrawn, this is a incremented"
        " negative integer value, i.e. the first withdrawn sample is set to"
        " -1, second to -2."
    )
    parser.add_argument(
        '-F', '--format', type=str,
        choices=[_LONG_FORMAT, _WIDE_FORMAT],
        default=_LONG_FORMAT,
        help="The format of the input file, long format has the sample IDs"
        " (eids) in a column, such that each row contains a sample ID. Wide"
        " formats have the sample IDs in the header row, such that each "
        "sample ID is a separate column."
    )
    parser.add_argument(
        '-c', '--column', type=str,
        default='eid',
        help="The location of the sample IDs. If the ``--format`` is a long"
        " format then this is a column name (it defaults to ``eid``). If the"
        " location is a wide format this should be the number of columns "
        "(1-based) that occur before the samples appear in the header row, it"
        "defaults to 2 (the second column)."
    )
    cmd_args.add_tmpdir_arg(parser)
    cmd_args.add_verbose_arg(parser)
    return parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _parse_cmd_args(parser):
    """Parse the command line arguments.

    Parameters
    ----------
    parser : `argparse.ArgumentParser`
        The argparse parser object with arguments added.

    Returns
    -------
    args : `argparse.Namespace`
        The argparse namespace object containing the arguments
    """
    args = parser.parse_args()
    if args.format == _WIDE_FORMAT:
        try:
            args.column = int(args.column) - 1

            if args.column < 0:
                raise ValueError("bad column")
        except ValueError as e:
            raise ValueError(
                "--column value should be a positive integer if format is"
                " 'wide'"
            ) from e
    return args


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def remap(target, source=None, infile=None, config=None, comment=None,
          encoding=_DEFAULT_ENCODING, skiplines=0, delimiter="\t",
          input_format=_LONG_FORMAT, missing_action=_WITHDRAWN_CHOICE,
          withdrawn_action=_WITHDRAWN_CHOICE, allow_unknown_sex=False,
          sample_column=None, verbose=False):
    """Remap an input file from the source application to the target
    application.

    Parameters
    ----------
    target : `int` or `str`
        The target application number or the path to a sample definition
        (``.sdef``) file, that can be used for remapping to the target. If
        `target` is an application number then `config` can't be `NoneType`.
    source : `int` or `str`, optional, default: `NoneType`
        The source application number or the path to a sample definition
        (``.sdef``) file, that can be used for remapping from the source. If
        `NoneType`, then the application number from the config file will be
        used. If `source` is an application number then `config` can't be
        `NoneType`.
    infile : `str`, optional, default: `NoneType`
        The input file name, if not provided then input is assumed to be from
        STDIN. If a file path the file can be gzip compressed.
    config : `str`, optional, default: `NoneType`
        The path to a config file. This is optional, if the source and target
        are defined file paths and not config entries.
    comment : `str`, optional, default: `NoneType`
        A comment character that defines comment lines at the start of the
        input file, these will be skipped. If `NoneType`, then it is assumed
        that the start of the file has no comment lines.
    encoding : `str`, optional, default: `UTF8`
        The encoding of the input file.
    skiplines : `int`, optional, default: `0`
        A fixed number of lines at the start of the file that should be
        skipped.
    delimiter : `str`, optional, default: `\t`
        The delimiter of the input file.
    input_format : `str`, optional, default: `long`
        The format of the input file, can either be `long` or `wide`. Long
        formats have the sample IDs in a column where each sample ID occupies
        a row. Wide formats have each sample ID in the header row where, each
        sample ID occupies a column.
    missing_action : `str`, optional, default: `withdrawn`
        The action to take when a sample exists in the input file but not in
        the mapping file. The choices are `withdrawn`, `remove`, `error`. The
        withdrawn option set the sample ID to withdrawn, this is an
        incremented negative integer value, see Notes for details.
    withdrawn_action : `str`, optional, default: `withdrawn`
        The action to take when a sample exists in the input file but not in
        the mapping file. The choices are `withdrawn`, `remove`. The withdrawn
        option set the sample ID to withdrawn, this is an incremented negative
        integer value, see Notes for details.
    allow_unknown_sex : `bool`, optional, default: `False`
        Skip sex checks for samples that have unknown sex. If this is `False`,
        then any unknown sex samples will generate an error as the sex can't
        be verified between samples with equivalent positions. If this is
        `True`, then these are allowed.
    sample_column : `str` or `int`, optional, default: `NoneType`
        The location of the sample IDs. If the `input_format` is long, this
        should be a column name where the Ids are present. If the
        `input_format` is wide, this should be the 0-based column number where
        the sampleIDs start. This defaults to `eid` for long formats and `1`
        (second column) for wide formats.
    verbose : `bool`, optional, default: `False`
        Log progress.

    Raises
    ------
    ValueError
        If the input format is incorrect.
    """
    if input_format not in [_LONG_FORMAT, _WIDE_FORMAT]:
        raise ValueError(
            "The input_format should either be 'long' or 'wide' not:"
            f" '{input_format}'"
        )

    source_ids, target_ids, withdrawn_ids = get_mapping_files(
        target, source=source, config=config, verbose=verbose
    )

    mapper = generate_mapping_set(
        source_ids, target_ids, allow_unknown_sex=allow_unknown_sex,
        withdrawn_ids=withdrawn_ids, verbose=verbose
    )

    # Now perform the re-map
    open_method = utils.get_open_method(infile, allow_none=True)
    with stdopen.open(infile, method=open_method, encoding=encoding) as fobj:
        first_rows = header.skip_to_header(
            fobj, skiplines=skiplines, comment=comment, include_header=True
        )

        try:
            header_row = first_rows.pop(-1).strip()
        except IndexError as e:
            raise IndexError("Can't detect header row") from e

        for i in first_rows:
            yield [i.strip()]

        header_row = next(csv.reader([header_row], delimiter=delimiter))
        reader = csv.reader(fobj, delimiter=delimiter)

        remap_func = _remap_long_format
        if input_format == _WIDE_FORMAT:
            remap_func = _remap_wide_format

        for row in remap_func(
                    reader, header_row, mapper, sample_column=sample_column,
                    missing_action=missing_action, verbose=verbose,
                    withdrawn_action=withdrawn_action,
        ):
            yield row


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_mapping_files(target, source=None, config=None, verbose=False):
    """Get the paths for the mapping files needed in the remapping. This
    handles the logic of source and target being paths or config entries.

    Parameters
    ----------
    target : `int` or `str`
        The target application number or the path to a sample definition
        (``.sdef``) file, that can be used for remapping to the target. If
        `target` is an application number then `config` can't be `NoneType`.
    source : `int` or `str`, optional, default: `NoneType`
        The source application number or the path to a sample definition
        (``.sdef``) file, that can be used for remapping from the source. If
        `NoneType`, then the application number from the config file will be
         used. If `source` is an application number then `config` can't be
         `NoneType`.
    config : `str`, optional, default: `NoneType`
        The path to a config file. This is optional, if the source and target
        are defined file paths and not config entries.
    verbose : `bool`, optional, default: `False`
        Log progress.

    Returns
    -------
    source_ids : `list` of `list`
        The source IDs, each sub list has the structure `eid`, sex (0,1,2),
        is genotyped (`bool`), has withdrawn (`bool`).
    target_ids : `list` of `list`
        The source IDs, each sub list has the structure `eid`, sex (0,1,2),
        is genotyped (`bool`), has withdrawn (`bool`).
    withdrawn_ids : `set` of `str`
        The withdrawn sample Ids. These will be automatically loaded if the
        source application is the sample as the application in the config
        file. If the applications differ or the config is not given then this
        will be an empty set.

    Raises
    ------
    ValueError
        If the source or target are not file paths and no config file path is
        supplied.
    """
    logger = log.retrieve_logger(_SCRIPT_NAME, verbose=verbose)

    # If a config file is present then the withdrawn samples will be loaded as
    # well
    withdrawn = set()
    source_is_file = False
    if source is not None:
        source_is_file = _is_file(source, "source", verbose=verbose)
    else:
        logger.info(
            "Source is not defined, using the application number in the "
            "config file"
        )
    target_is_file = _is_file(target, "target", verbose=verbose)

    # If anything is not a file we must have the config defined
    # if (source_is_file & target_is_file) is False and config is None:
    #     raise ValueError(
    #         "config must be defined, if source/target "
    #         "applications are not sdef files"
    #     )

    # If anything needs querying from the config file, then we load it
    if (source_is_file & target_is_file) is False:
        cfg_file = ukbbconf.UkbbConfig(
            ukbbconf.find_config_file(cmd_arg=config)
        )
        logger.info(f"using config file: {cfg_file.name}")

        if source is None:
            # Get the application number from the config and use to get the
            # mapping file
            source = cfg_file.application
            logger.info(f"source application umber: {source}")

        if source_is_file is False:
            # If the source is from the same application as the withdrawn
            # sample list then load up the withdrawn samples just in case
            # the sdef files are not up-to-date
            if source == cfg_file.application:
                # Load up the withdrawn file
                withdrawn = parsers.read_withdrawn_file(cfg_file.withdrawn)

            # Fetch source file path
            source = cfg_file.mapping(source)

        if target_is_file is False:
            # Fetch target file path
            target = cfg_file.mapping(target)

    if source == target:
        raise ValueError("The source and target mapping files are the same")

    logger.info(f"Using source mapping file: {source}")
    logger.info(f"Using target mapping file: {target}")

    source_ids = parsers.read_sdef_file(
        source, allow_withdrawn_samples=True
    )
    target_ids = parsers.read_sdef_file(
        target, allow_withdrawn_samples=True
    )

    if len(withdrawn) > 0:
        logger.info(f"Using withdrawn file: {len(withdrawn)} samples")

    return source_ids, target_ids, withdrawn


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _is_file(test, test_type, verbose=False):
    """Test is the source/target mappings are files or config entries.

    Parameters
    ----------
    test : `str` or `int`
        The source/target mapping to test.
    test_type : `str`
        The mapping being tested, this is used for log/error messages.
    verbose : `bool`, optional, default: `False`
        Log the output of the test.

    Returns
    -------
    is_file : `bool`
        `True` for yes it is a file, `False` for no it is not a file.
    """
    logger = log.retrieve_logger(_SCRIPT_NAME, verbose=verbose)
    test = str(test)
    try:
        open(test).close()
        logger.info(f"The {test_type} mapping is a file")
        return True
    except FileNotFoundError:
        # Should be an application number, this will raise an error, if it is
        # simply a file that does not exist
        try:
            int(test)
            logger.info(f"The {test_type} mapping is an application number")
        except TypeError as e:
            raise FileNotFoundError(
                "This is interpreted as a file but it does not exist: "
                f"{test_type}={test}"
            ) from e
        return False


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def generate_mapping_set(source_ids, target_ids, allow_unknown_sex=False,
                         withdrawn_ids=None, verbose=False):
    """Attempt to generate a mapping set between the source and the target
    samples.

    Parameters
    ----------
    source_ids : `list` of `list`
        The source IDs, each sub list has the structure `eid`, sex (0,1,2),
        is genotyped (`bool`), has withdrawn (`bool`).
    target_ids : `list` of `list`
        The source IDs, each sub list has the structure `eid`, sex (0,1,2),
        is genotyped (`bool`), has withdrawn (`bool`).
    allow_unknown_sex : `bool`, optional, default: `False`
        Skip sex checks for samples that have unknown sex. If this is `False`,
        then any unknown sex samples will generate an error as the sex can't
        be verified between samples with equivalent positions. If this is
        `True`, then these are allowed.
    withdrawn_ids : `set` of `str`, optional, default: `NoneType`
        Any additional sample IDs to set to withdrawn.
    verbose : `bool`, optional, default: `False`
        Log the output.

    Returns
    -------
    mapper : `dict`
        The sample mapper. The keys are source sample IDs, the values are lists
        with the target ID information. Note that only valid EIDs are stored in
        the mapper, so any withdrawn (negative) EIDs will be removed here.

    Raises
    ------
    ValueError
        If any of the samples have unspecified sex and `allow_unknown_sex` is
        False, or if there are any sex discrepancy between equivalent samples.
    """
    logger = log.retrieve_logger(_SCRIPT_NAME, verbose=verbose)
    withdrawn_ids = withdrawn_ids or set()
    mapper = {}
    eid_col = col.EID
    eid_withdrawn_col = col.EID_WITHDRAWN

    if len(source_ids) != len(target_ids):
        logger.info(
            f"source and target samples different lengths, will attempt"
            f" genotyped filter: {len(source_ids)} vs. {len(target_ids)}"
        )
        filter_genotyped(source_ids)
        filter_genotyped(target_ids)

    if len(source_ids) != len(target_ids):
        raise IndexError(
            "source and target samples are of different lengths: "
            f"{len(source_ids)} vs. {len(target_ids)}"
        )
    else:
        logger.info(
            "source and target samples are same length: "
            f"{len(source_ids)} vs. {len(target_ids)}"
        )

    n_withdrawn = 0
    logger.info("validating sex information")
    for s, t in zip(source_ids, target_ids):
        # Need to test for valid EIDs in source and target, allowing for the
        # possibility of withdrawn samples
        # The source sample ID
        seid = s[0]
        eid_withdrawn_col.type(seid)
        eid_withdrawn_col.type(t[0])

        # ss (source sex), ts (target sex)
        ss, ts = s[1], t[1]
        if (ss == 0 or ts == 0) and allow_unknown_sex is False:
            raise ValueError(
                "Either source/target sex is unknown for samples:"
                f" {s[0]}({ss})/{t[0]}({ts})"
            )
        if ss != ts:
            if ss != 0 and ts != 0:
                raise ValueError(
                    "Sex discrepancy between source/target sex:"
                    f" {s[0]}({ss})/{t[0]}({ts})"
                )

        if seid in withdrawn_ids:
            s[3] = True

        # Merge the withdrawn status from the two sample sets
        s[3] = s[3] | t[3]
        t[3] = s[3] | t[3]
        n_withdrawn += s[3]

        try:
            # Store the mapping if the EID is valid
            eid_col.type(seid)
            mapper[seid] = t
        except errors.EidError:
            pass

    if len(mapper) == 0:
        raise IndexError("The mapper contains no samples")

    logger.info(f"#samples: {len(source_ids)}")
    logger.info(f"#withdrawn samples: {n_withdrawn}")
    return mapper


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def filter_genotyped(samples):
    """Filter samples from a set of sample IDs (read in from a ``.sdef`` file)
    to keep only those that have been genotyped. The filter happens in place.

    Parameters
    ----------
    samples : `list` of `list`
        The source IDs, each sub list has the structure `eid`, sex (0,1,2),
        is genotyped (`bool`), has withdrawn (`bool`).
    """
    idx = 0
    while True:
        try:
            if samples[idx][2] is False:
                samples.pop(idx)
                continue
        except IndexError:
            break
        idx += 1


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _remap_long_format(reader, header_row, mapper, sample_column=None,
                       missing_action=_WITHDRAWN_CHOICE,
                       withdrawn_action=_WITHDRAWN_CHOICE, verbose=False):
    """Perform remapping on a long format files, with sample IDs in rows.

    Parameters
    ----------
    reader : `csv.reader`
        The input file reader, ths will yield rows as lists.
    header_row : `list` of `str`
        The input row header, this should contain the sample ID column within
        it.
    mapper : `dict`
        The sample mapper. The keys are source sample IDs, the values are lists
        with the target ID information. Note that only valid EIDs are stored in
        the mapper, so any withdrawn (negative) EIDs will be removed here.
    sample_column : `str` or `int`, optional, default: `NoneType`
        The location of the sample IDs. This should be the column name where
        the Ids are present. This defaults to `eid`.
    missing_action : `str`, optional, default: `withdrawn`
        The action to take when a sample exists in the input file but not in
        the mapping file. The choices are `withdrawn`, `remove`, `error`. The
        withdrawn option set the sample ID to withdrawn, this is an
        incremented negative integer value, see Notes for details.
    withdrawn_action : `str`, optional, default: `withdrawn`
        The action to take when a sample exists in the input file but not in
        the mapping file. The choices are `withdrawn`, `remove`. The withdrawn
        option set the sample ID to withdrawn, this is an incremented negative
        integer value, see Notes for details.
    verbose : `bool` or `int`, optional, default: `False`
        Log progress, set to > 1 for input file progress monitoring.

    Yields
    ------
    row : `list` of `str`
        The input file rows. The first row yielded will always be the header
        row.

    Raises
    ------
    KeyError
        If the sample column can't be found in the header row. This will also
        be raised if a sample ID is not in the mapper and missing_action is set
        to `error`.
    EidError
        If any of the sample IDs do not onform to the EID format and are not
        set to withdrawn.
    ValueError
        If no EIDs were remapped.
    """
    prog_verbose = log.progress_verbose(verbose=verbose)
    logger = log.retrieve_logger(_SCRIPT_NAME, verbose=verbose)
    sample_column = sample_column or _DEFAULT_LONG_COLUMN
    nmissing = 0
    nwithdrawn = 0
    withdrawn_id = -1
    valid_eids = 0
    eid_col = col.EID_WITHDRAWN

    try:
        sample_column = header_row.index(sample_column)
    except ValueError as e:
        raise KeyError(
            f"Can't find sample column in header row: {sample_column}"
        ) from e
    yield header_row

    tqdm_kwargs = dict(
        desc="[info] remapping file...",
        unit=" row(s)",
        disable=not prog_verbose
    )

    for idx, row in enumerate(tqdm(reader, **tqdm_kwargs), 1):
        eid = row[sample_column]
        # Make sure the EID is valid
        eid_col.type(eid)
        is_valid = True
        is_wd = False

        try:
            new_eid, sex, is_geno, is_wd = mapper[eid]
        except KeyError:
            # Missing
            is_valid = False
            nmissing += 1
            if missing_action == _WITHDRAWN_CHOICE:
                new_eid = str(withdrawn_id)
                withdrawn_id -= 1
            elif missing_action == _ERROR_CHOICE:
                raise KeyError(
                    f"Can't find sample ID (eid) in mapper: {eid}"
                )
            else:
                continue

        if is_wd is True:
            nwithdrawn += 1
            is_valid = False
            if missing_action == _WITHDRAWN_CHOICE:
                new_eid = str(withdrawn_id)
                withdrawn_id -= 1
            else:
                continue

        valid_eids += is_valid
        row[sample_column] = new_eid
        yield row

    if valid_eids == 0:
        raise ValueError("No sample IDs were remapped")

    logger.info(f"#processed rows: {idx}")
    logger.info(f"#missing rows: {nmissing}")
    logger.info(f"#withdrawn rows: {nwithdrawn}")


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _remap_wide_format(reader, header_row, mapper, sample_column=None,
                       missing_action=_WITHDRAWN_CHOICE,
                       withdrawn_action=_WITHDRAWN_CHOICE, verbose=False):
    """Perform remapping on a long format files, with sample IDs in columns.

    Parameters
    ----------
    reader : `csv.reader`
        The input file reader, ths will yield rows as lists.
    header_row : `list` of `str`
        The input row header, this should contain the sample ID column within
        it.
    mapper : `dict`
        The sample mapper. The keys are source sample IDs, the values are lists
        with the target ID information. Note that only valid EIDs are stored in
        the mapper, so any withdrawn (negative) EIDs will be removed here.
    sample_column : `str` or `int`, optional, default: `NoneType`
        The location of the sample IDs. This should be the 0-based column
        number where the sampleIDs start. This defaults to  `1`
        (second column).
    missing_action : `str`, optional, default: `withdrawn`
        The action to take when a sample exists in the input file but not in
        the mapping file. The choices are `withdrawn`, `remove`, `error`. The
        withdrawn option set the sample ID to withdrawn, this is an
        incremented negative integer value, see Notes for details.
    withdrawn_action : `str`, optional, default: `withdrawn`
        The action to take when a sample exists in the input file but not in
        the mapping file. The choices are `withdrawn`, `remove`. The withdrawn
        option set the sample ID to withdrawn, this is an incremented negative
        integer value, see Notes for details.
    verbose : `bool` or `int`, optional, default: `False`
        Log progress, set to > 1 for input file progress monitoring.

    Yields
    ------
    row : `list` of `str`
        The input file rows. The first row yielded will always be the header
        row.

    Raises
    ------
    KeyError
        This will also be raised if a sample ID is not in the mapper and
        missing_action is set to `error`.
    EidError
        If any of the sample IDs do not onform to the EID format and are not
        set to withdrawn.
    ValueError
        If no EIDs were remapped or if the sample column is not > 0.
    """
    prog_verbose = log.progress_verbose(verbose=verbose)
    logger = log.retrieve_logger(_SCRIPT_NAME, verbose=verbose)
    sample_column = sample_column or _DEFAULT_WIDE_COLUMN
    nmissing = 0
    nwithdrawn = 0
    withdrawn_id = -1
    valid_eids = 0
    eid_col = col.EID_WITHDRAWN

    if sample_column < 0:
        raise ValueError(
            "sample_column should be a 0-based integer reflecting where the"
            " sample IDs start in the header row"
        )

    remove_idx = []
    new_header = list(header_row)[:sample_column]

    for idx, eid in enumerate(header_row[sample_column:], sample_column):
        # Make sure the EID is valid
        eid_col.type(eid)
        is_valid = True
        is_wd = False

        try:
            new_eid, sex, is_geno, is_wd = mapper[eid]
        except KeyError:
            # Missing
            is_valid = False
            nmissing += 1
            if missing_action == _WITHDRAWN_CHOICE:
                new_eid = str(withdrawn_id)
                withdrawn_id -= 1
            elif missing_action == _ERROR_CHOICE:
                raise KeyError(
                    f"Can't find sample ID (eid) in mapper: {eid}"
                )
            else:
                remove_idx.append(idx)
                continue

        if is_wd is True:
            is_valid = False
            nwithdrawn += 1
            if missing_action == _WITHDRAWN_CHOICE:
                new_eid = str(withdrawn_id)
                withdrawn_id -= 1
            else:
                remove_idx.append(idx)
                continue
        valid_eids += is_valid
        new_header.append(new_eid)

    yield new_header

    tqdm_kwargs = dict(
        desc="[info] remapping file...",
        unit=" row(s)",
        disable=not prog_verbose
    )
    remove_idx.sort(reverse=True)
    for idx, row in enumerate(tqdm(reader, **tqdm_kwargs), 1):
        for i in remove_idx:
            row.pop(i)
        yield row

    if valid_eids == 0:
        raise ValueError("No sample IDs were remapped")

    logger.info(f"#processed rows: {idx}")
    logger.info(f"#missing rows: {nmissing}")
    logger.info(f"#withdrawn rows: {nwithdrawn}")


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == '__main__':
    main()
