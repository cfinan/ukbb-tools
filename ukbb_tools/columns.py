"""Column definitions in various UK Biobank files.
"""
from ukbb_tools import type_casts as tc
from collections import namedtuple

# I will define all the expected column names in the files using this
# namedtuple
ColName = namedtuple('ColName', ['name', 'type', 'missing', 'desc'])


_MISSING_VALUES = [None, '']
"""Missing values that will apply to most contexts (`list`)
"""
_VCF_MISSING = ['.']
"""Missing values that will apply to VCF file columns (`list`)
"""

# ############################ INPUT FILES ####################################

# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
EID = ColName(
    "eid",
    tc.parse_eid,
    _MISSING_VALUES,
    "The sample ID column for UK Biobank"
)
"""The sample ID column for UK Biobank (`ukbb_tools.columns.ColName`)
"""
EID_WITHDRAWN = ColName(
    "eid",
    tc.parse_eid_withdrawn,
    _MISSING_VALUES,
    "The sample ID column for UK Biobank, that allows for the possibility of"
    " withdrawn samples, these will be negative integers."
)
"""The sample ID column for UK Biobank (`ukbb_tools.columns.ColName`)
"""


# @@@@@@@@@@@@@@@@@@@@@@@@@ Used in sample extraction file @@@@@@@@@@@@@@@@@@@@
IS_GENOTYPED = ColName(
    "is_genotyped",
    tc.parse_bool,
    _MISSING_VALUES,
    "Boolean indicator column for if a sample was genotyped as part of "
    "the project"
)
"""Boolean indicator column for if a sample was genotyped as part of
the project (`ukbb_tools.columns.ColName`)
"""
HAS_WITHDRAWN = ColName(
    "has_withdrawn",
    tc.parse_bool,
    _MISSING_VALUES,
    "Boolean indicator column for if a participant has withdrawn from the "
    "study"
)
"""Boolean indicator column for if a participant has withdrawn from the
study (`ukbb_tools.columns.ColName`)
"""


# @@@@@@@@@@@@@@@@@@@@@@@@@ VCF columns @@@@@@@@@@@@@@@@@@@@
VCF_CHROM = ColName(
    "#CHROM",
    str,
    _VCF_MISSING,
    "VCF chromosome column"
)
"""A VCF chromosome column (`ukbb_tools.columns.ColName`)
"""
VCF_POS = ColName(
    "POS",
    int,
    _VCF_MISSING,
    "VCF position column"
)
"""A VCF position column (`ukbb_tools.columns.ColName`)
"""
VCF_ID = ColName(
    "ID",
    str,
    _VCF_MISSING,
    "VCF variant ID column"
)
"""A VCF variant ID column (`ukbb_tools.columns.ColName`)
"""
VCF_REF = ColName(
    "REF",
    str,
    _VCF_MISSING,
    "VCF reference allele column"
)
"""VCF reference allele column (`ukbb_tools.columns.ColName`)
"""
VCF_ALT = ColName(
    "ALT",
    str,
    _VCF_MISSING,
    "VCF alternate alleles column"
)
"""VCF alternate alleles column (`ukbb_tools.columns.ColName`)
"""
VCF_QUAL = ColName(
    "QUAL",
    str,
    _VCF_MISSING,
    "VCF quality column"
)
"""A VCF quality column (`ukbb_tools.columns.ColName`)
"""
VCF_FILTER = ColName(
    "FILTER",
    str,
    _VCF_MISSING,
    "VCF filter column"
)
"""A VCF filter column (`ukbb_tools.columns.ColName`)
"""
VCF_INFO = ColName(
    "INFO",
    str,
    _VCF_MISSING,
    "VCF info ID column"
)
"""A VCF info column (`ukbb_tools.columns.ColName`)
"""
VCF_FORMAT = ColName(
    "FORMAT",
    str,
    _VCF_MISSING,
    "VCF format column"
)
"""A VCF format column (`ukbb_tools.columns.ColName`)
"""


# @@@@@@@@@@@@@@@@@@@@@@@@@@@ BGEN SAMPLE FILES @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
SAMPLE_ID1 = ColName(
    "ID_1",
    tc.parse_eid,
    _MISSING_VALUES,
    "The family ID column for a BGEN sample file"
)
"""The family ID column for a BGEN sample file (`ukbb_tools.columns.ColName`)
"""
SAMPLE_ID2 = ColName(
    "ID_2",
    tc.parse_eid,
    _MISSING_VALUES,
    "The sample ID column for a BGEN sample file"
)
"""The sample ID column for a BGEN sample file (`ukbb_tools.columns.ColName`)
"""
SAMPLE_MISS = ColName(
    "missing",
    tc.parse_bool,
    _MISSING_VALUES,
    "The Missing call frequency for a BGEN sample file."
)
"""The sample ID column for a BGEN sample file (`ukbb_tools.columns.ColName`)
"""
SAMPLE_SEX = ColName(
    "sex",
    tc.parse_sex,
    [0, '0'],
    "The sample sex column for a BGEN sample file, should be 1/2"
)
"""The sample ID column for a BGEN sample file (`ukbb_tools.columns.ColName`)
"""


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@ PLINK FAM FILES @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
FAM_FID = ColName(
    "FID",
    str,
    _MISSING_VALUES,
    "The family ID column for a Plink FAM file"
)
"""The family ID column for a Plink FAM file (`ukbb_tools.columns.ColName`)
"""
FAM_IID = ColName(
    "IID",
    tc.parse_eid,
    _MISSING_VALUES,
    "Within-family ID ('IID'; cannot be '0')"
)
"""The individual ID for a Plink FAM file (`ukbb_tools.columns.ColName`)
"""
FAM_FATHER = ColName(
    "FATHER_ID",
    str,
    ['0', 0],
    "Within-family ID of father ('0' if father isn't in dataset)"
)
"""The ID column the father in a Plink FAM file (`ukbb_tools.columns.ColName`)
"""
FAM_MOTHER = ColName(
    "MOTHER_ID",
    str,
    ['0', 0],
    "Within-family ID of mother ('0' if mother isn't in dataset)"
)
"""The sample ID column for a BGEN sample file (`ukbb_tools.columns.ColName`)
"""
SAMPLE_SEX = ColName(
    "sex",
    tc.parse_sex,
    [0, '0'],
    "The sample sex column for a Plink FAM file, should be 1/2 or 0 fro missing"
)
"""The sample sex column for a Plink FAM file (`ukbb_tools.columns.ColName`)
"""
