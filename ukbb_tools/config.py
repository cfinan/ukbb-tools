"""A handler for the UKBB-tools configuration file, this provides a command
line tool and API for querying the configuration file.
"""
# Importing the version number (in __init__.py) and the package name
from ukbb_tools import (
    __version__,
    __name__ as pkg_name,
    cmd_args,
    constants as con,
    common
)

try:
    _READ_MODE = 'rb'
    import tomllib
except ImportError:
    _READ_MODE = 'r'
    import toml as tomllib

import argparse
import sys
import os
# import pprint as pp

# The name of the script
_SCRIPT_NAME = "ukbb-config"
"""The name of the script (`str`)
"""
_DESC = (
    "A handler for the UKBB-tools configuration file, this provides command"
    " line access to the UKBB-tools configuration file"
)
"""The program description (`str`)
"""


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _test_int(section, key, value):
    """Test that the value is an integer.

    Parameters
    ----------
    section : `str`
        The section name to test.
    key : `str`
        The key name within a section to test.
    value : `int`
        The actual value that will be tested.
    """
    if not isinstance(value, int):
        raise TypeError(f"'{section}.{key}' should be: 'int'")


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _test_str(section, key, value):
    """Test that the value is an string.

    Parameters
    ----------
    section : `str`
        The section name to test.
    key : `str`
        The key name within a section to test.
    value : `int`
        The actual value that will be tested.
    """
    if not isinstance(value, str):
        raise TypeError(f"'{section}.{key}' should be: 'str'")


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class UkbbConfig(object):
    """Main handler for the configuration file.

    Parameters
    ----------
    config_path : `str`
        The path to the toml configuration file.
    """
    GENERAL_SECTION = 'general'
    """The name for the general section (`str`)
    """
    MAPPING_SECTION = 'mapping'
    """The name for the mapping section (`str`)
    """
    SEED_KEY = 'seed'
    """The name for the seed key (`str`)
    """
    APPLICATION_KEY = 'application'
    """The name for the application number key (`str`)
    """
    WITHDRAWN_KEY = 'withdrawn'
    """The name for the withdrawn file key (`str`)
    """
    ALLOWED_MAP = {
        GENERAL_SECTION: {
            SEED_KEY: _test_int,
            APPLICATION_KEY: _test_int,
            WITHDRAWN_KEY: _test_str
        }
    }
    """Mappings of allowed field combinations to their datatypes (`dict`)
    """

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, config_path):
        self.name = config_path
        self.conf_dict = self.load_toml(self.name)

        # These are required and will raise errors if not present
        self.application
        self.withdrawn

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def seed(self):
        """Fetch the value for the `seed` key in the config file.

        Returns
        -------
        seed : `int` of `NoneType`
            The value for the seed in the config file. If not defined
            `NoneType` is returned.
        """
        test_func = self.is_allowed(self.GENERAL_SECTION, self.SEED_KEY)

        try:
            seed = self.conf_dict[self.GENERAL_SECTION][self.SEED_KEY]
            test_func(self.GENERAL_SECTION, self.WITHDRAWN_KEY, seed)
            return seed
        except KeyError:
            return None

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def application(self):
        """Fetch the value for the `application` key in the config file.

        Returns
        -------
        application : `int`
            The value for the application in the config file.

        Raises
        ------
        KeyError
            If the application number is not an integer in the config file. Or if
            application is not defined.
        """
        test_func = self.is_allowed(self.GENERAL_SECTION, self.APPLICATION_KEY)

        try:
            app_no = self.conf_dict[self.GENERAL_SECTION][self.APPLICATION_KEY]
            test_func(self.GENERAL_SECTION, self.WITHDRAWN_KEY, app_no)
            return app_no
        except KeyError as e:
            raise KeyError(
                f"The {self.APPLICATION_KEY} is required in the config file."
            ) from e

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def withdrawn(self):
        """Fetch the value for the `withdrawn` key in the config file.

        Returns
        -------
        withdrawn : `str`
            The value for the withdrawn key in the config file.

        Raises
        ------
        KeyError
            If the withdrawn value is not an string in the config file. Or if
            withdrawn is not defined.
        """
        test_func = self.is_allowed(self.GENERAL_SECTION, self.WITHDRAWN_KEY)

        try:
            wpath = self.conf_dict[self.GENERAL_SECTION][self.WITHDRAWN_KEY]
            test_func(self.GENERAL_SECTION, self.WITHDRAWN_KEY, wpath)
            return wpath
        except KeyError as e:
            raise KeyError(
                f"The {self.WITHDRAWN_KEY} is required in the config file."
            ) from e

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @classmethod
    def is_allowed(cls, section, key):
        """Test that a section, key combination is allowed.

        Parameters
        ----------
        section : `str`
            The section name to test.
        key : `str`
            The key name within a section to test.

        Returns
        -------
        type_test : `function`
            The function that can be used to type test that value at the
            position in the config file.

        Raises
        ------
        KeyError
            If the section/key combination is not valid.
        """
        try:
            return cls.ALLOWED_MAP[section][key]
        except KeyError as e:
            raise KeyError(
                f"Not a valid section/key combination: {section}.{key}"
            ) from e

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @classmethod
    def load_toml(cls, infile):
        """Load a .toml file.

        Parameters
        ----------
        infile : `str`
            The path to a text toml file containing the theme information.

        Returns
        -------
        loaded_config : `dict`
            The config file.
        """
        with open(infile, _READ_MODE) as f:
            return tomllib.load(f)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def mapping(self, app_no):
        """Fetch a mapping file based on an application number.

        Parameters
        ----------
        app_no : `int`
            The application number to get the mapping file for.

        Returns
        -------
        mapping_file : `str`
            The path to the mapping file.

        Raises
        ------
        TypeError
            If `app_no` is not an integer (or can't be cast to one).
        KeyError
            If the `app_no` is not in the mapping file.
        """
        try:
            app_no = str(int(app_no))
        except TypeError as e:
            raise TypeError(
                f"Mapping application numbers should be integers: {app_no}"
            ) from e

        try:
            value = self.conf_dict[self.MAPPING_SECTION][app_no]
        except KeyError as e:
            raise KeyError(f"Unknown mapping application: {app_no}") from e

        # Make sure that the value is the correct type
        _test_str(self.MAPPING_SECTION, app_no, value)
        return value


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main():
    """The main entry point for the script.
    """
    # Initialise and parse the command line arguments
    parser = _init_cmd_args()
    args = _parse_cmd_args(parser)

    try:
        cfg_file = find_config_file(cmd_arg=args.config)
        cfg = UkbbConfig(cfg_file)
        value = None

        if args.option == UkbbConfig.GENERAL_SECTION:
            value = query_general_section(cfg, args.key_name)
        else:
            raise ValueError(f"Unknown option: {args.option}")
        print(value)
    except (BrokenPipeError, KeyboardInterrupt):
        # Python flushes standard streams on exit; redirect remaining
        # output to devnull to avoid another BrokenPipeError at shutdown
        devnull = os.open(os.devnull, os.O_WRONLY)
        os.dup2(devnull, sys.stdout.fileno())


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _init_cmd_args():
    """Initialise the command line arguments and return the parser.

    Returns
    -------
    args : `argparse.ArgumentParser`
        The argparse parser object with arguments added
    """
    parser = argparse.ArgumentParser(
        description=_DESC
    )
    subparsers = parser.add_subparsers(dest="option")

    general = subparsers.add_parser(
        UkbbConfig.GENERAL_SECTION,
        help="Query for key values from the 'general' section."
    )
    add_general_parser(general)

    return parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _parse_cmd_args(parser):
    """Parse the command line arguments.

    Parameters
    ----------
    parser : `argparse.ArgumentParser`
        The argparse parser object with arguments added.

    Returns
    -------
    args : `argparse.Namespace`
        The argparse namespace object containing the arguments
    """
    args = parser.parse_args()
    return args


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def add_general_parser(subparser):
    """Add the command arguments for querying the general section.

    Parameters
    ----------
    subparser : `argparse.ArgumentParser`
        The argument parser to add the general arguments to.
    """
    subparser.add_argument(
        'key_name', type=str,
        help="The key in the general section to search for."
    )
    cmd_args.add_config_arg(subparser)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def find_config_file(cmd_arg=None):
    """Attempt to locate the configuration file.

    Parameters
    ----------
    cmd_arg : `str`, optional, default: `NoneType`
        A user defined location for the configuration file.

    Returns
    -------
    cfg_file : `str`
        The path to the configuration file.

    Raises
    ------
    FileNotFoundError
        If the configuration file is not defined or available in any of the
        search locations.

    Notes
    -----
    If `cmd_arg` is not provided then the following search strategy is used.
    First ``UKBB_CONFIG``, environment variable is checked for the default
    config file name (`.ukbb-tools.toml`), then the path defined in the
    UKBB_PACKAGE_ENV environment is checked, finally, the root of the home
    directory is checked. If not found then an error is raised."
    """
    if cmd_arg is not None:
        try:
            open(cmd_arg).close()
            return cmd_arg
        except FileNotFoundError as e:
            _raise_config_not_found(cmd_arg, e)

    try:
        cfg_file = os.environ[con.UKBB_CONFIG_ENV]
        open(cfg_file).close()
        return cfg_file
    except FileNotFoundError as e:
        _raise_config_not_found(con.UKBB_CONFIG_ENV, e)
    except KeyError:
        # Not defined in environment variable
        pass

    package_dir = common.get_package_dir()
    try:
        cfg_file = os.path.join(package_dir, con.DEFAULT_CONFIG_NAME)
        open(cfg_file).close()
        return cfg_file
    except (KeyError, FileNotFoundError):
        pass

    try:
        cfg_file = os.path.join(
            os.environ['HOME'], con.DEFAULT_CONFIG_NAME
        )
        open(cfg_file).close()
        return cfg_file
    except (KeyError, FileNotFoundError) as e:
        _raise_config_not_found(cfg_file, e)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _raise_config_not_found(path_looked_up, err):
    """Raise an error about the config file path not being valid.

    Parameters
    ----------
    path_looked_up : `str`
        The config file path that was looked up but not valid.
    err : `FileNotFoundError`
        The file not found error that was raised initially.

    Raises
    ------
    FileNotFoundError
        Re-raised from original error
    """
    raise FileNotFoundError(
        f"The config file is set in {path_looked_up} but does not"
        " exist"
    ) from err


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def query_general_section(config, key):
    """Search for keys defined in the general section.

    Parameters
    ----------
    config : `ukbb_tools.config.UkbbConfig`
        The configuration object.
    key_name : `str`
        The key name in the general section to search for.

    Returns
    -------
    section_key_value : `any`
        The value located at the specific key in the general section.
    """
    try:
        return getattr(config, key)
    except AttributeError:
        # Check this combination is allowed
        config.is_allowed(config.GENERAL_SECTION, key)



# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == '__main__':
    main()
