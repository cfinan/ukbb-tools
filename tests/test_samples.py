"""Here are the actual test for the module: run with ``pytest test_module.py``.
There is no need to import conftest it happens automatically.
"""
from ukbb_tools import samples
import pandas as pd
import pytest
import numpy as np
import pprint as pp


_ALPHA = ["A", "B", "C", "D", "E", "F", "G"]
"""Used to give some character list keys for the tests, not essential but
useful for debugging (`list` of `str`)
"""

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.mark.parametrize(
    # These are the variabe names by which the parameters in the list will be
    # available in the test function
    "master, test, exp_res",
    # Each element in this list represents a single parameter
    [
        # Test0
        (
            ["01", "02", "03", "04", "05"],
            ["01", "02", "04"],
            [],
        ),
        # Test1
        (
            ["01", "02", "04"],
            ["02", "03", "04"],
            [[2, 2, "03"]],
        ),
        # Test2
        (
            ["01", "02", "04", "05"],
            ["02", "03", "05"],
            [[2, 3, "03"]],
        ),
        # Test3
        (
            ["01", "02", "04", "05"],
            ["04", "05", "06"],
            [[4, 4, "06"]],
        ),
        # Test4
        (
            ["02", "04", "05", "06"],
            ["01", "02", "06"],
            [[0, 0, "01"]],
        ),
        # Test5
        (
            ["02", "04", "05", "06"],
            ["01", "02", "06", "07"],
            [[0, 0, "01"], [4, 4, "07"]],
        ),
        # Test6
        (
            ["02", "04", "05", "06", "09", "10"],
            ["01", "02", "07", "11"],
            [[0, 0, "01"], [1, 6, "07"], [1, 6, "11"]],
        ),
        # Test7
        (
            ["02", "04", "05", "09", "10", "11"],
            ["05", "06", "07", "08", "09"],
            [[3, 3, "06"], [3, 3, "07"], [3, 3, "08"]],
        ),
        # Test8
        (
            ["02", "04", "05", "09", "10", "11", "12", "13", "14"],
            ["05", "06", "07", "08", "09", "14", "15", "16"],
            [[3, 3, "06"], [3, 3, "07"], [3, 3, "08"],
             [9, 9, "15"], [9, 9, "16"]],
        ),
        # Test9
        (
            ["01", "02", "04"],
            ["01", "02", "03", "04", "05"],
            [[2, 2, "03"], [3, 3, "05"]],
        ),
        # Test10
        (
            ["02", "03", "04"],
            ["01", "02", "04"],
            [[0, 0, "01"]],
        ),
        # Test11
        (
            ["02", "03", "05"],
            ["01", "02", "04", "05"],
            [[0, 0, "01"], [1, 2, "04"]],
        ),
        # Test12
        (
            ["04", "05", "06"],
            ["01", "02", "04", "05"],
            [[0, 0, "01"], [0, 0, "02"]],
        ),
        # Test13
        (
            ["02", "04", "05", "06"],
            ["01", "02", "06"],
            [[0, 0, "01"]],
        ),
        # Test14
        (
            ["01", "02", "07", "11"],
            ["02", "04", "05", "06", "09", "10"],
            [[2, 4, '04'], [2, 4, '05'], [2, 4, '06'], [2, 4, '09'],
             [2, 4, '10']]
        ),
        # Test15
        (
            ["05", "06", "07", "08", "09"],
            ["02", "04", "05", "09", "10", "11"],
            [[0, 0, "02"], [0, 0, "04"], [5, 5, "10"], [5, 5, "11"]],
        ),
        # Test16
        (
            ["05", "06", "07", "08", "09", "14", "15", "16"],
            ["02", "04", "05", "09", "10", "11", "12", "13", "14"],
            [[0, 0, "02"], [0, 0, "04"],  [5, 5, "10"], [5, 5, "11"],
             [5, 5, "12"], [5, 5, "13"]],
        ),
        # Test17
        (
            ["01", "02", "03", "04", "05"],
            ["01", "02", "04"],
            [],
        ),
        # Test18
        (
            ["05", "06", "09", "13", "15", "16"],
            ["02", "04", "05", "07", "09", "10", "11", "12", "13", "14"],
            [[0, 0, "02"], [0, 0, "04"],  [1, 2, "07"], [3, 3, "10"],
             [3, 3, "11"], [3, 3, "12"], [4, 6, "14"]],
        ),
    ]
)
def test_calc_index_range(master, test, exp_res):
    """Test the `samples.ListOrderResolver.calc_index_range` classmethod.
    """
    # TODO: Check for tied start/end values
    res = samples.ListOrderResolver.calc_index_range(
        master, test
    )
    assert exp_res == res, "wrong results"


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.mark.parametrize(
    # These are the variabe names by which the parameters in the list will be
    # available in the test function
    "test_data, exp_res",
    # Each element in this list represents a single parameter
    [
        # Test0
        (
            (
                ["05", "06", "09", "13", "15", "16"],
                ["05", "09", "13", "16"],
                ["05", "16"],
                ["05", "06", "13", "15"]
            ),
            pd.DataFrame(
                np.zeros((4, 4), dtype='int32'),
                index=['0', '1', '2', '3'],
                columns=['0', '1', '2', '3'],
            )
        ),
        # Test1
        (
            (
                ["05", "13", "09", "06", "15", "16"],
                ["05", "09", "13", "16"],
                ["05", "16"],
                ["05", "06", "13", "15"]
            ),
            pd.DataFrame(
                [
                    [0, 1, 0, 1],
                    [1, 0, 0, 0],
                    [0, 0, 0, 0],
                    [1, 0, 0, 0],
                ],
                index=['0', '1', '2', '3'],
                columns=['0', '1', '2', '3'],
                dtype='int32'
            )
        ),
        # Test2
        (
            (
                ["16", "13", "09", "06", "15", "05"],
                ["05", "09", "13", "16"],
                ["05", "16"],
                ["05", "06", "13", "15"]
            ),
            pd.DataFrame(
                [
                    [0, 3, 1, 2],
                    [3, 0, 0, 0],
                    [1, 0, 0, 0],
                    [2, 0, 0, 0],
                ],
                index=['0', '1', '2', '3'],
                columns=['0', '1', '2', '3'],
                dtype='int32'
            )
        ),
        # Test3
        (
            (
                ["16", "13", "09", "06", "15", "05"],
                ["13", "09", "05", "16"],
                ["05", "16"],
                ["05", "06", "13", "15"]
            ),
            pd.DataFrame(
                [
                    [0, 1, 1, 2],
                    [1, 0, 0, 1],
                    [1, 0, 0, 0],
                    [2, 1, 0, 0],
                ],
                index=['0', '1', '2', '3'],
                columns=['0', '1', '2', '3'],
                dtype='int32'
            )
        ),
        # Test4
        (
            (
                ["16", "13", "09", "06", "15", "05"],
                ["13", "16", "05", "09"],
                ["05", "16"],
                ["05", "06", "13", "15"]
            ),
            pd.DataFrame(
                [
                    [0, 2, 1, 2],
                    [2, 0, 1, 1],
                    [1, 1, 0, 0],
                    [2, 1, 0, 0],
                ],
                index=['0', '1', '2', '3'],
                columns=['0', '1', '2', '3'],
                dtype='int32'
            )
        ),
        # Test5
        (
            (
                ["16", "13", "09", "06", "15", "05"],
                ["13", "16", "05", "09"],
                ["06", "05", "16"],
                ["05", "06", "13", "15"]
            ),
            pd.DataFrame(
                [
                    [0, 2, 1, 2],
                    [2, 0, 1, 1],
                    [1, 1, 0, 1],
                    [2, 1, 1, 0],
                ],
                index=['0', '1', '2', '3'],
                columns=['0', '1', '2', '3'],
                dtype='int32'
            )
        ),
    ]
)
def test_compare_lists(tmpdir, test_data, exp_res):
    """Test the `samples.ListOrderResolver.compare_lists`.
    """
    with samples.ListOrderResolver(tmpdir=tmpdir) as resolver:
        for idx, i in enumerate(test_data):
            name = str(idx)
            resolver.add_list(name, i)
        # TODO: add test for overlaps
        error_matrix, overlaps_matrix = resolver.compare_lists()
        pd.testing.assert_frame_equal(error_matrix, exp_res)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.mark.parametrize(
    # These are the variabe names by which the parameters in the list will be
    # available in the test function
    "test_data, exp_nlists, removed",
    # Each element in this list represents a single parameter
    [
        # Test0
        (
            (
                ["05", "06", "09", "13", "15", "16"],
                ["05", "09", "13", "16"],
                ["05", "16"],
                ["05", "06", "13", "15"]
            ),
            4,
            [],
        ),
        # Test1
        (
            (
                ["05", "09", "13", "16"],
                ["05", "16"],
                ["05", "13", "09", "06", "15", "16"],
                ["05", "06", "13", "15"]
            ),
            3,
            [('2', 2)],
        ),
        # Test2
        (
            (
                ["16", "13", "09", "06", "15", "05"],
                ["05", "09", "13", "16"],
                ["05", "16"],
                ["05", "06", "13", "15"]
            ),
            3,
            [('0', 6)],
        ),
        # Test3
        (
            (
                ["16", "13", "09", "06", "15", "05"],
                ["13", "09", "05", "16"],
                ["05", "16"],
                ["05", "06", "13", "15"]
            ),
            2,
            [('0', 4), ('1', 1)],
        ),
        # Test4
        (
            (
                ["16", "13", "09", "06", "15", "05"],
                ["13", "16", "05", "09"],
                ["05", "16"],
                ["05", "06", "13", "15"]
            ),
            2,
            [('0', 5), ('1', 2)],
        ),
    ]
)
def test_delete_error_lists(tmpdir, test_data, exp_nlists, removed):
    """Test the `samples.ListOrderResolver.delete_error_lists`.
    """
    with samples.ListOrderResolver(tmpdir=tmpdir) as resolver:
        for idx, i in enumerate(test_data):
            name = str(idx)
            resolver.add_list(name, i)
        res_removed = resolver.delete_error_lists()
        assert resolver.nlists == exp_nlists, "wrong number of lists"

        for i, j in zip(removed, res_removed):
            assert i[0] == j[0], "wrong removed lists"
            assert i[1] == j[1], "wrong #errors reported"


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.mark.parametrize(
    # These are the variabe names by which the parameters in the list will be
    # available in the test function
    "test_data",
    # Each element in this list represents a single parameter
    [
        # Test0
        (
            ["16", "13", "09", "06", "15", "05"],
            ["13", "16", "05", "09"],
            ["06", "05", "16"],
            ["05", "06", "13", "15"]
        ),
    ]
)
def test_delete_error_lists_error(tmpdir, test_data):
    """Test the `samples.ListOrderResolver.delete_error_lists` raises an error
    when all datasets are removed.
    """
    with pytest.raises(KeyError) as the_error:
        with samples.ListOrderResolver(tmpdir=tmpdir) as resolver:
            for idx, i in enumerate(test_data):
                name = str(idx)
                resolver.add_list(name, i)
            resolver.delete_error_lists()
        assert the_error.match(
            "All error datasets removed, the relative order of all"
            " lists is likely different"
        ), "wrong error message"


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.mark.parametrize(
    # These are the variabe names by which the parameters in the list will be
    # available in the test function
    "test_data, exp_res",
    # Each element in this list represents a single parameter
    [
        # Test0
        (
            (
                ["16", "13", "09", "06", "15", "05"],
                ["13", "16", "05", "09"],
                ["06", "05", "16"],
                ["05", "06", "13", "15"]
            ),
            4
        ),
        # Test1
        (
            (
                ["16", "13", "09", "06", "15", "05"],
                ["13", "16", "05", "09"],
                ["16", "13", "09", "06", "15", "05"],
                ["13", "16", "05", "09"],
            ),
            2
        ),
        # Test2
        (
            (
                ["16", "13", "09", "06", "15", "05"],
                ["16", "13", "09", "06", "15", "05"],
                ["16", "13", "09", "06", "15", "05"],
                ["16", "13", "09", "06", "15", "05"],
                ["16", "13", "09", "06", "15", "05"],
                ["16", "13", "09", "06", "15", "05"],
            ),
            1
        ),
    ]
)
def test_list_number(tmpdir, test_data, exp_res):
    """Test the `samples.ListOrderResolver.add_list` does not add identical
    lists.
    """
    with samples.ListOrderResolver(tmpdir=tmpdir) as resolver:
        for idx, i in enumerate(test_data):
            name = str(idx)
            resolver.add_list(name, i)
        assert resolver.nlists == exp_res


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.mark.parametrize(
    # These are the variabe names by which the parameters in the list will be
    # available in the test function
    "test_data, exp_res",
    # Each element in this list represents a single parameter
    [
        # Test1
        (
            (
                ["16", "13", "09", "07", "15", "05"],
                ["16", "13", "09", "05"],
                ["06", "08", "16", "05"],
            ),
            ["06", "08", "16", "13", "09", "07", "15", "05"]
        ),
        # Test2
        (
            (
                ["01", "02", "03"],
                ["03", "04", "05"],
                ["05", "06", "07"],
                ["07", "08", "09"],
                ["09", "10", "11"],
                ["11", "12", "13"],
                ["13", "14", "15"],
            ),
            [
                "01", "02", "03", "04", "05", "06", "07", "08",
                "09", "10", "11", "12", "13", "14", "15"
            ]
        ),
    ]
)
def test_resolve(tmpdir, test_data, exp_res):
    """Test the `samples.ListOrderResolver.resolve` gives expected result.
    with complex=True
    """
    with samples.ListOrderResolver(tmpdir=tmpdir) as resolver:
        for idx, i in enumerate(test_data):
            name = str(idx)
            resolver.add_list(name, i)
        final = resolver.resolve(complex=True)
        assert final == exp_res, "wrong results"


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.mark.parametrize(
    # These are the variabe names by which the parameters in the list will be
    # available in the test function
    "test_data, exp_res",
    # Each element in this list represents a single parameter
    [
        # Test1
        (
            (
                [
                    ["16", 0, True, False],
                    ["13", 0, False, False],
                    ("09", 1, False, False),
                    ["07", 0, True, False],
                    ["15", 2, True, False],
                    ["05", 0, False, False]
                ],
                [
                    ("16", 0, False, False),
                    ["13", 2, True, True],
                    ("09", 1, True, False),
                    ["05", 0, False, False]
                ],
                [
                    ("06", 2, True, False),
                    ("08", 2, True, False),
                    ("16", 1, False, False),
                    ("05", 0, False, False)
                ],
            ),
            [
                ["06", 2, True, False],
                ["08", 2, True, False],
                ["16", 1, True, False],
                ["13", 2, True, True],
                ["09", 1, True, False],
                ["07", 0, True, False],
                ["15", 2, True, False],
                ["05", 0, False, False]
            ]
        ),
    ]
)
def test_sample_resolve(tmpdir, test_data, exp_res):
    """Test the `samples.SampleOrderResolver.resolve` gives expected result.
    """
    with samples.SampleOrderResolver(tmpdir=tmpdir) as resolver:
        for idx, i in enumerate(test_data):
            name = str(idx)
            resolver.add_list(name, i)
        final = resolver.resolve(complex=True)
        assert final == exp_res, "wrong results"


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.mark.parametrize(
    # These are the variabe names by which the parameters in the list will be
    # available in the test function
    "test_data, exp_res",
    # Each element in this list represents a single parameter
    [
        # Test0
        (
            (
                ["16", "13", "09", "05"],
                ["06", "08", "16", "05"],
            ),
            (
                (2, 2, [None, None, "16", "13", "09", "05"]),
                (0, 2, ["06", "08", "16", None, None, "05"]),
            ),
        ),
        # Test1
        (
            (
                ["06", "08", "16", "05"],
                ["16", "13", "09", "05"],
            ),
            (
                (0, 2, ["06", "08", "16", None, None, "05"]),
                (2, 2, [None, None, "16", "13", "09", "05"]),
            ),
        ),
        # Test2
        (
            (
                ["16", "13", "09", "05"],
                ["06", "08", "16", "05", "20"],
            ),
            (
                (2, 3, [None, None, "16", "13", "09", "05", None]),
                (0, 2, ["06", "08", "16", None, None, "05", "20"]),
            ),
        ),
        # Test3
        (
            (
                ["06", "08", "16", "05", "20"],
                ["16", "13", "09", "05"],
            ),
            (
                (0, 2, ["06", "08", "16", None, None, "05", "20"]),
                (2, 3, [None, None, "16", "13", "09", "05", None]),
            ),
        ),
        # Test4
        (
            (
                ["16", "13", "09", "05", "20", "10", "01", "07", "11"],
                ["06", "08", "16", "05", "20", "01"],
            ),
            (
                (2, 2, [None, None, "16", "13", "09", "05",
                        "20", "10", "01", "07", "11"]),
                (0, 5, ["06", "08", "16", None, None, "05",
                        "20", None, "01", None, None]),
            ),
        ),
        # Test5
        (
            (
                ["06", "08", "16", "05", "20", "01"],
                ["16", "13", "09", "05", "20", "10", "01", "07", "11"],
            ),
            (
                (0, 5, ["06", "08", "16", None, None, "05",
                 "20", None, "01", None, None]),
                (2, 2, [None, None, "16", "13", "09", "05",
                 "20", "10", "01", "07", "11"]),
            ),
        ),
        # Test6
        (
            (
                ["01", "02", "03"],
                ["03", "04", "05"],
            ),
            (
                (0, 2, ["01", "02", "03", None, None]),
                (2, 2, [None, None, "03", "04", "05"]),
            ),
        ),
        # Test7
        (
            (
                ["01", "02", "11", "03", "10", "04"],
                ["05", "02", "06", "03", "08", "04"],
            ),
            (
                (0, 3, ["01", None, "02", "11", None, "03", "10", None, "04"]),
                (1, 3, [None, "05", "02", None, "06", "03", None, "08", "04"]),
            ),
        ),
        # Test8
        (
            (
                ["05", "02", "06", "03", "08", "04"],
                ["01", "02", "11", "03", "10", "04"],
            ),
            (
                (0, 3, ["05", None, "02", "06", None, "03", "08", None, "04"]),
                (1, 3, [None, "01", "02", None, "11", "03", None, "10", "04"]),
            ),
        ),
        # Test9
        (
            (
                ["02", "05", "03", "06", "04", "08"],
                ["02", "01", "03", "11", "04", "10"],
            ),
            (
                (0, 3, ["02", "05", None, "03", "06", None, "04", "08", None]),
                (0, 3, ["02", None, "01", "03", None, "11", "04", None, "10"]),
            ),
        ),
        # Test10
        (
            (
                ["02", "05", "03", "06", "04", "08"],
                ["02", "21", "23", "25", "29", "30"],
            ),
            (
                (0, 5, ["02", "05", "03", "06", "04", "08",
                 None, None, None, None, None]),
                (0, 5, ["02", None, None, None, None, None,
                 "21", "23", "25", "29", "30"]),
            ),
        ),
        # Test11
        (
            (
                ["02", "05", "03", "06", "04", "08"],
                ["02", "21", "23", "25", "29", "08"],
            ),
            (
                (0, 4, ["02", "05", "03", "06", "04", None, None, None,
                        None, "08"]),
                (0, 4, ["02", None, None, None, None, "21", "23", "25",
                        "29", "08"]),
            ),
        ),
        # Test12
        (
            (
                ["02", "05", "03", "01", "06", "04", "08"],
                ["02", "21", "23", "01", "25", "29", "08"],
            ),
            (
                (0, 4, ["02", "05", "03", None, None, "01", "06",
                 "04", None, None, "08"]),
                (0, 4, ["02", None, None, "21", "23", "01", None,
                 None, "25", "29", "08"]),
            ),
        ),
        # Test13
        (
            (
                ["03", "04", "05"],
                ["01", "02", "03"],
            ),
            (
                (2, 2, [None, None, "03", "04", "05"]),
                (0, 2, ["01", "02", "03", None, None]),
            ),
        ),
        # Test14
        (
            (
                [None, None, "16", "13", "09", "05"],
                ["06", "08", "16", None, None, "05"],
            ),
            (
                (2, 0, [None, None, "16", "13", "09", "05"]),
                (0, 0, ["06", "08", "16", None, None, "05"]),
            ),
        ),
        # Test15
        (
            (
                ["06", "08", "16", None, None, "05"],
                [None, None, "16", "13", "09", "05"],
            ),
            (
                (0, 0, ["06", "08", "16", None, None, "05"]),
                (2, 0, [None, None, "16", "13", "09", "05"]),
            ),
        ),
        # Test16
        (
            (
                [None, None, "16", "13", "09", "05", None],
                ["06", "08", "16", None, None, "05", "20"],
            ),
            (
                (2, 0, [None, None, "16", "13", "09", "05", None]),
                (0, 0, ["06", "08", "16", None, None, "05", "20"]),
            ),
        ),
        # Test17
        (
            (
                ["06", "08", "16", None, None, "05", "20"],
                [None, None, "16", "13", "09", "05", None],
            ),
            (
                (0, 0, ["06", "08", "16", None, None, "05", "20"]),
                (2, 0, [None, None, "16", "13", "09", "05", None]),
            ),
        ),
        # Test18
        (
            (
                [None, None, "16", "13", "09", "05", "20",
                 "10", "01", "07", "11"],
                ["06", "08", "16", None, None, "05", "20",
                 None, "01", None, None],
            ),
            (
                (2, 0, [None, None, "16", "13", "09", "05", "20",
                 "10", "01", "07", "11"]),
                (0, 0, ["06", "08", "16", None, None, "05", "20",
                 None, "01", None, None]),
            ),
        ),
        # Test19
        (
            (
                ["06", "08", "16", None, None, "05", "20",
                 None, "01", None, None],
                [None, None, "16", "13", "09", "05", "20",
                 "10", "01", "07", "11"],
            ),
            (
                (0, 0, ["06", "08", "16", None, None, "05", "20",
                 None, "01", None, None]),
                (2, 0, [None, None, "16", "13", "09", "05", "20",
                 "10", "01", "07", "11"]),
            ),
        ),
        # Test20
        (
            (
                ["01", "02", "03", None, None],
                [None, None, "03", "04", "05"],
            ),
            (
                (0, 0, ["01", "02", "03", None, None]),
                (2, 0, [None, None, "03", "04", "05"]),
            ),
        ),
        # Test21
        (
            (
                [None, None, "03", "04", "05"],
                ["01", "02", "03", None, None],
            ),
            (
                (2, 0, [None, None, "03", "04", "05"]),
                (0, 0, ["01", "02", "03", None, None]),
            ),
        ),
        # Test22
        (
            (
                ["01", None, "02", "11", None, "03", "10", None, "04"],
                [None, "05", "02", None, "06", "03", None, "08", "04"],
            ),
            (
                (0, 0, ["01", None, "02", "11", None, "03", "10", None, "04"]),
                (1, 0, [None, "05", "02", None, "06", "03", None, "08", "04"]),
            ),
        ),
        # Test23
        (
            (
                ["05", None, "02", "06", None, "03", "08", None, "04"],
                [None, "01", "02", None, "11", "03", None, "10", "04"],
            ),
            (
                (0, 0, ["05", None, "02", "06", None, "03", "08", None, "04"]),
                (1, 0, [None, "01", "02", None, "11", "03", None, "10", "04"]),
            ),
        ),
        # Test24
        (
            (
                ["02", "05", None, "03", "06", None, "04", "08", None],
                ["02", None, "01", "03", None, "11", "04", None, "10"],
            ),
            (
                (0, 0, ["02", "05", None, "03", "06", None, "04", "08", None]),
                (0, 0, ["02", None, "01", "03", None, "11", "04", None, "10"]),
            ),
        ),
        # Test25
        (
            (
                ["02", "05", "03", "06", "04", "08",
                 None, None, None, None, None],
                ["02", None, None, None, None, None,
                 "21", "23", "25", "29", "30"],
            ),
            (
                (0, 0, ["02", "05", "03", "06", "04", "08",
                 None, None, None, None, None]),
                (0, 0, ["02", None, None, None, None, None,
                 "21", "23", "25", "29", "30"]),
            ),
        ),
        # Test26
        (
            (
                ["02", "05", "03", "06", "04", None, None, None, None, "08"],
                ["02", None, None, None, None, "21", "23", "25", "29", "08"],
            ),
            (
                (0, 0, ["02", "05", "03", "06", "04", None, None, None,
                        None, "08"]),
                (0, 0, ["02", None, None, None, None, "21", "23", "25", "29",
                        "08"]),
            ),
        ),
        # Test27
        (
            (
                ["02", "05", "03", None, None, "01", "06",
                 "04", None, None, "08"],
                ["02", None, None, "21", "23", "01", None,
                 None, "25", "29", "08"],
            ),
            (
                (0, 0, ["02", "05", "03", None, None, "01", "06",
                 "04", None, None, "08"]),
                (0, 0, ["02", None, None, "21", "23", "01", None,
                 None, "25", "29", "08"]),
            ),
        ),
        # Test28
        (
                (
                        [None, None, "03", "04", "05"],
                        ["05", "06", "07"],
                ),
                (
                        (2, 2, [None, None, "03", "04", "05", None, None]),
                        (4, 4, [None, None, None, None, "05", "06", "07"]),
                ),
        ),
    ]
)
def test_align_lists(test_data, exp_res):
    """Test the `samples.ListOrderResolver.align_lists` gives expected result.
    """
    exp_a, exp_b = exp_res
    exp_aidx, exp_agap, exp_aalign = exp_a
    exp_bidx, exp_bgap, exp_balign = exp_b

    # test_copy = [list(i) for i in test_data]
    a, b = samples.ListOrderResolver.align_lists(*test_data)
    aidx, agap, aalign = a
    bidx, bgap, balign = b
    assert aidx == exp_aidx, "wrong a index start"
    assert agap == exp_agap, "wrong a gaps"
    assert aalign == exp_aalign, "wrong a alignment"
    assert bidx == exp_bidx, "wrong a index start"
    assert bgap == exp_bgap, "wrong a gaps"
    assert balign == exp_balign, "wrong a alignment"


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.mark.parametrize(
    # These are the variabe names by which the parameters in the list will be
    # available in the test function
    "test_data, error_type, message",
    # Each element in this list represents a single parameter
    [
        # Test0
        (
            (
                ["16", "13", "09", "05"],
                ["06", "08", "05", "16"],
            ),
            ValueError,
            "The 'a' and 'b' lists are out of alignment"
        ),
        # Test1
        (
            (
                ["06", "08", "05", "16"],
                ["16", "13", "09", "05"],
            ),
            ValueError,
            "The 'a' and 'b' lists are out of alignment"
        ),
        # Test2
        (
            (
                ["01", "02", "03"],
                ["04", "05", "06"],
            ),
            KeyError,
            "Lists not able to be aligned"
        ),
        # Test3
        (
            (
                ["04", "05", "06"],
                ["01", "02", "03"],
            ),
            KeyError,
            "Lists not able to be aligned"
        ),
        # Test4
        (
            (
                ["01", "02", "03", None, None, None],
                [None, None, None, "04", "05", "06"],
            ),
            KeyError,
            "Lists not able to be aligned"
        ),
        # Test5
        (
            (
                [None, None, None, "04", "05", "06"],
                ["01", "02", "03", None, None, None],
            ),
            KeyError,
            "Lists not able to be aligned"
        ),
    ]
)
def test_align_lists_error(test_data, error_type, message):
    """Test the `samples.ListOrderResolver.align_lists` gives expected error.
    """
    with pytest.raises(error_type) as the_error:
        samples.ListOrderResolver.align_lists(*test_data)
    assert the_error.match(
        message
    )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.mark.parametrize(
    # These are the variabe names by which the parameters in the list will be
    # available in the test function
    "test_data, exp_res",
    # Each element in this list represents a single parameter
    [
        # Test0
        (
            (
                ["16", "13", "09", "07", "15", "05"],
                ["16", "13", "09", "05"],
                ["06", "08", "16", "05"],
            ),
            (
                ['-', '-', "16", "13", "09", "07", "15", "05"],
                ['-', '-', "16", "13", "09", '-', '-', "05"],
                ["06", "08", "16", '-', '-', '-', '-', "05"],
            ),
        ),
        # Test1
        (
            (
                ["01", "02", "03"],
                ["03", "04", "05"],
                ["05", "06", "07"],
                ["07", "08", "09"],
                ["09", "10", "11"],
                ["11", "12", "13"],
                ["13", "14", "15"],
            ),
            (
                ["01", "02", "03", '-', '-', '-', '-', '-', '-',
                 '-', '-', '-', '-', '-', '-'],
                ['-', '-', "03", "04", "05", '-', '-', '-', '-',
                 '-', '-', '-', '-', '-', '-'],
                ['-', '-', '-', '-', "05", "06", "07", '-', '-',
                 '-', '-', '-', '-', '-', '-'],
                ['-', '-', '-', '-', '-', '-', "07", "08", "09",
                 '-', '-', '-', '-', '-', '-'],
                ['-', '-', '-', '-', '-', '-', '-', '-', "09",
                 "10", "11", '-', '-', '-', '-'],
                ['-', '-', '-', '-', '-', '-', '-', '-', '-',
                 '-', "11", "12", "13", '-', '-'],
                ['-', '-', '-', '-', '-', '-', '-', '-', '-',
                 '-', '-', '-', "13", "14", "15"],
            ),
        ),
        # Test2
        (
            (
                ["01", "02", "03"],
                ["05", "06", "07"],
                ["03", "04", "05"],
            ),
            (
                ["01", "02", "03", '-', '-', '-', '-'],
                ['-', '-', '-', '-', "05", "06", "07"],
                ['-', '-', "03", "04", "05", '-', '-'],
            ),
        ),
        # Test3
        (
            (
                ["05", "06", "07"],
                ["01", "02", "03"],
                ["03", "04", "05"],
            ),
            (
                ['-', '-', '-', '-', "05", "06", "07"],
                ["01", "02", "03", '-', '-', '-', '-'],
                ['-', '-', "03", "04", "05", '-', '-'],
            ),
        ),
        # Test4
        (
            (
                ["01", "02", "03"],
                ["03", "04", "05"],
                ["02", "03", "05"],
                ["04", "06", "07", "08"],
                ["08", "09", "10"],
                ["06", "08", "10"],
            ),
            (
                ["01", "02", "03", '-', '-', '-', '-', '-', '-', '-'],
                ['-', '-', "03", "04", "05", '-', '-', '-', '-', '-'],
                ['-', "02", "03", '-', "05", '-', '-', '-', '-', '-'],
                ['-', '-', '-', '04', '-', "06", "07", "08", '-', '-'],
                ['-', '-', '-', '-', '-', '-', '-', "08", "09", "10"],
                ['-', '-', '-', '-', '-', "06", '-', "08", '-', "10"],
            ),
        ),
        # Test5 - The alignment has an extra blank column at the end, I am
        # not sure I can do anything about that in the current implementation
        (
            (
                ["08", "09", "10"],
                ["01", "02", "03"],
                ["03", "04", "05"],
                ["06", "08", "10"],
                ["02", "03", "05"],
                ["04", "06", "07", "08"],
            ),
            (
                ['-', '-', '-', '-', '-', '-', '-', "08", "09", "10", '-'],
                ["01", "02", "03", '-', '-', '-', '-', '-', '-', '-', '-'],
                ['-', '-', "03", "04", "05", '-', '-', '-', '-', '-', '-'],
                ['-', '-', '-', '-', '-', "06", '-', "08", '-', "10", '-'],
                ['-', "02", "03", '-', "05", '-', '-', '-', '-', '-', '-'],
                ['-', '-', '-', '04', '-', "06", "07", "08", '-', '-', '-'],
            ),
        ),
    ]
)
def test_align_multiple_lists(tmpdir, test_data, exp_res):
    """Test the `samples.ListOrderResolver.align_multiple_lists` gives
     expected alignment.
    """
    print("")
    with samples.ListOrderResolver(tmpdir=tmpdir) as resolver:
        for idx, i in enumerate(test_data):
            resolver.add_list(_ALPHA[idx], i)
        missing = "-"
        results = resolver.align_multiple_lists(missing=missing)
        assert [i for i in sorted(results.values())] == sorted(exp_res), \
            "wrong results"


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.mark.parametrize(
    # These are the variabe names by which the parameters in the list will be
    # available in the test function
    "test_data, exp_combs, exp_zero_combs",
    # Each element in this list represents a single parameter
    [
        # Test0
        (
            pd.DataFrame(
                [
                    [10, 5,  1,  7,  1],
                    [5, 10,  9,  5,  1],
                    [1,  9, 10,  0,  6],
                    [7,  5,  0, 10,  2],
                    [1,  1,  6,  2, 10],

                ],
                columns=['A', 'B', 'C', 'D', 'E'],
                index=['A', 'B', 'C', 'D', 'E']
            ),
            [
                ('B', 'C'), ('B', 'A'), ('B', 'D'), ('B', 'E'), ('C', 'E'),
                ('C', 'A'), ('A', 'D'), ('A', 'E'), ('D', 'E')
            ],
            [("C", "D")]
        ),
        # Test1
        (
            pd.DataFrame(
                [
                    [10, 5],
                    [5, 10],
                ],
                columns=['A', 'B'],
                index=['A', 'B']
            ),
            [
                ('A', 'B')
            ],
            []
        ),
        # Test2
        (
            pd.DataFrame(
                [
                    [10, 5, 10],
                    [5, 20, 3],
                    [10, 3, 15],
                ],
                columns=['A', 'B', 'C'],
                index=['A', 'B', 'C']
            ),
            [
                ('A', 'C'), ('A', 'B'), ('C', 'B')
            ],
            []
        ),
    ]
)
def test_check_overlaps(tmpdir, test_data, exp_combs, exp_zero_combs):
    """This tests that the ``ukbb_tools.samples.check_overlaps`` classmethod
    is working correctly.
    """
    combs, zero_combs = samples.ListOrderResolver.check_overlaps(test_data)
    assert combs == exp_combs, "wrong combinations"
    assert zero_combs == exp_zero_combs, "wrong zero combinations"


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.mark.parametrize(
    # These are the variabe names by which the parameters in the list will be
    # available in the test function
    "test_data",
    # Each element in this list represents a single parameter
    [
        # Test0 - A list that does not overlap with anything
        pd.DataFrame(
            [
                [10, 0,  0,  0,  0],
                [0, 10,  9,  5,  1],
                [0,  9, 10,  0,  6],
                [0,  5,  0, 10,  2],
                [0,  1,  6,  2, 10],
            ],
            columns=['A', 'B', 'C', 'D', 'E'],
            index=['A', 'B', 'C', 'D', 'E'],
        ),
        # Test1 - two distinct subsets of lists that do not overlap
        pd.DataFrame(
            [
                [3,  0,  0,  0,  2,  2],
                [0,  3,  2,  2,  0,  0],
                [0,  2,  3,  1,  0,  0],
                [0,  2,  1,  3,  0,  0],
                [2,  0,  0,  0,  3,  1],
                [2,  0,  0,  0,  1,  3],
            ],
            columns=['A', 'B', 'C', 'D', 'E', 'F'],
            index=['A', 'B', 'C', 'D', 'E', 'F'],
        )
    ]
)
def test_check_overlaps_error(tmpdir, test_data):
    """This tests that the ``ukbb_tools.samples.check_overlaps`` classmethod
    is errors out when a list with no overlaps with anything is found.
    """
    with pytest.raises(ValueError) as the_error:
        samples.ListOrderResolver.check_overlaps(test_data)
    assert the_error.value.args[0].startswith("Unsolvable lists"), \
        "wrong error message"


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.mark.parametrize(
    # These are the variabe names by which the parameters in the list will be
    # available in the test function
    "test_data, alignment, exp_res",
    # Each element in this list represents a single parameter
    [
        # Test0
        (
            (
                ["16", "13", "09", "07", "15", "05"],
                ["16", "13", "07", "15", "05"],
            ),
            (
                ["16", "13", "09", "07", "15", "05"],
                ["16", "13", None, "07", "15", "05"],
            ),
            ["16", "13", "09", "07", "15", "05"],
        ),
        # Test1
        (
            (
                ["16", "13", "07", "15", "05"],
                ["16", "13", "09", "07", "15", "05"],
            ),
            (
                ["16", "13", None, "07", "15", "05"],
                ["16", "13", "09", "07", "15", "05"],
            ),
            ["16", "13", "09", "07", "15", "05"],
        ),
        # Test2
        (
            (
                ["16", "13", "09", "07", "15", "05"],
                ["16", "13", "09", "05"],
                ["06", "08", "16", "05"],
            ),
            (
                [None, None, "16", "13", "09", "07", "15", "05"],
                [None, None, "16", "13", "09", None, None, "05"],
                ["06", "08", "16", None, None, None, None, "05"],
            ),
            ["06", "08", "16", "13", "09", "07", "15", "05"],
        ),
        # Test3
        (
            (
                ["01", "02", "03"],
                ["03", "04", "05"],
                ["05", "06", "07"],
                ["07", "08", "09"],
                ["09", "10", "11"],
                ["11", "12", "13"],
                ["13", "14", "15"],
            ),
            (
                ["01", "02", "03", None, None, None, None, None, None,
                 None, None, None, None, None, None],
                [None, None, "03", "04", "05", None, None, None, None,
                 None, None, None, None, None, None],
                [None, None, None, None, "05", "06", "07", None, None,
                 None, None, None, None, None, None],
                [None, None, None, None, None, None, "07", "08", "09",
                 None, None, None, None, None, None],
                [None, None, None, None, None, None, None, None, "09",
                 "10", "11", None, None, None, None],
                [None, None, None, None, None, None, None, None, None,
                 None, "11", "12", "13", None, None],
                [None, None, None, None, None, None, None, None, None,
                 None, None, None, "13", "14", "15"],
            ),
            ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10",
             "11", "12", "13", "14", "15"],
        ),
        # Test4
        (
            (
                ["01", "02", "03"],
                ["05", "06", "07"],
                ["03", "04", "05"],
            ),
            (
                ["01", "02", "03", None, None, None, None],
                [None, None, None, None, "05", "06", "07"],
                [None, None, "03", "04", "05", None, None],
            ),
            ["01", "02", "03", "04", "05", "06", "07"],
        ),
        # Test5
        (
            (
                ["05", "06", "07"],
                ["01", "02", "03"],
                ["03", "04", "05"],
            ),
            (
                [None, None, None, None, "05", "06", "07"],
                ["01", "02", "03", None, None, None, None],
                [None, None, "03", "04", "05", None, None],
            ),
            ["01", "02", "03", "04", "05", "06", "07"],
        ),
        # Test6 - this is solved but will not be resolved
        (
            (
                ["02", "03", "05"],
                ["03", "04", "05"],
                ["01", "02", "03"],
                ["04", "06", "07", "08"],
                ["08", "09", "10"],
                ["06", "08", "10"],
            ),
            (
                ["01", "02", "03", None, None, None, None, None, None, None],
                [None, None, "03", "04", "05", None, None, None, None, None],
                [None, "02", "03", None, "05", None, None, None, None, None],
                [None, None, None, "04", None, "06", "07", "08", None, None],
                [None, None, None, None, None, None, None, "08", "09", "10"],
                [None, None, None, None, None, "06", None, "08", None, "10"],
            ),
            ['01', '02', '03', '04', '05', None, '07', '08', '09', '10']
        ),
        # Test7
        (
            (
                ["02", "03", "05"],
                ["03", "04", "05"],
                ["01", "02", "03"],
                ["05", "06", "07", "08"],
                ["08", "09", "10"],
                ["06", "08", "10"],
            ),
            (
                ["01", "02", "03", None, None, None, None, None, None, None],
                [None, None, "03", "04", "05", None, None, None, None, None],
                [None, "02", "03", None, "05", None, None, None, None, None],
                [None, None, None, None, "05", "06", "07", "08", None, None],
                [None, None, None, None, None, None, None, "08", "09", "10"],
                [None, None, None, None, None, "06", None, "08", None, "10"],
            ),
            ['01', '02', '03', '04', '05', "06", '07', '08', '09', '10']
        ),
        # Test8 - this is solved but will not be resolved
        (
            (
                ["08", "09", "10"],
                ["01", "02", "03"],
                ["03", "04", "05"],
                ["06", "08", "10"],
                ["02", "03", "05"],
                ["04", "06", "07", "08"],
            ),
            (
                ['-', '-', '-', '-', '-', '-', '-', "08", "09", "10"],
                ["01", "02", "03", '-', '-', '-', '-', '-', '-', '-'],
                ['-', '-', "03", "04", "05", '-', '-', '-', '-', '-'],
                ['-', '-', '-', '-', '-', "06", '-', "08", '-', "10"],
                ['-', "02", "03", '-', "05", '-', '-', '-', '-', '-'],
                ['-', '-', '-', '04', '-', "06", "07", "08", '-', '-'],
            ),
            ['01', '02', '03', '04', '05', None, '07', '08', '09', '10']
        ),
    ]
)
def test_solve(tmpdir, test_data, alignment, exp_res):
    """Test the `samples.ListOrderResolver.resolve` gives expected result.
    """
    # print()
    with samples.ListOrderResolver(tmpdir=tmpdir) as resolver:
        for idx, i in enumerate(test_data):
            resolver.add_list(str(idx), i)
        result = resolver.solve()
        assert result == exp_res, "wrong result"


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.mark.parametrize(
    # These are the variabe names by which the parameters in the list will be
    # available in the test function
    "test_data",
    # Each element in this list represents a single parameter
    [
        # Test0 - Unsolvable error for lack of overlaps
        (
            ["06", "08", "10"],
            ["02", "03", "05"],
            ["03", "04", "05"],
            ["01", "02", "03"],
            ["06", "07", "08"],
            ["08", "09", "10"],
        ),
        (
                ["01", "02", "03"],
                ["03", "04", "05"],
                ["02", "03", "05"],
                ["06", "07", "08"],
                ["08", "09", "10"],
                ["06", "08", "10"],
        ),
    ]
)
def test_solve_error(tmpdir, test_data):
    """Test the `samples.ListOrderResolver.solve` gives expected error.
    """
    with samples.ListOrderResolver(tmpdir=tmpdir) as resolver:
        for idx, i in enumerate(test_data):
            resolver.add_list(str(idx), i)
        with pytest.raises(ValueError) as the_error:
            resolver.solve()
        assert the_error.match("Unsolvable lists, not all lists overlap")
