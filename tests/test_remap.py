"""Tests for the ``ukbb_tools.remap`` module.
"""
from ukbb_tools import remap, errors
import pytest
# import os
# import csv
# import pprint as pp


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def test_config_basic_wide(remap_config_basic_wide, comment_char, skiplines,
                           wide_sample_col):
    """Test the basic scenario where everything is present, no withdrawn or
    bad IDs.
    """
    cfg_file, infile, source, target, exp_results = remap_config_basic_wide
    results = []
    for row in remap.remap(
            target,
            source=source,
            infile=infile, config=cfg_file,
            comment=comment_char,
            skiplines=skiplines,
            verbose=False,
            input_format='wide',
            allow_unknown_sex=False,
            sample_column=wide_sample_col,
            missing_action='error',
            withdrawn_action='remove'
    ):
        results.append(row)

    for i, j in zip(results, exp_results):
        assert i == j, "Incorrect row returned"


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def test_config_application_basic_wide(remap_config_basic_wide, comment_char,
                                       skiplines, wide_sample_col):
    """Test the basic scenario where everything is present, no withdrawn or
    bad IDs. In this version the source is set to NoneType, so this tests
    that the application source is used and maps correctly.
    """
    cfg_file, infile, source, target, exp_results = remap_config_basic_wide
    results = []
    for row in remap.remap(
            target,
            source=None,
            infile=infile, config=cfg_file,
            comment=comment_char,
            skiplines=skiplines,
            verbose=False,
            input_format='wide',
            allow_unknown_sex=False,
            sample_column=wide_sample_col,
            missing_action='error',
            withdrawn_action='remove'
    ):
        results.append(row)

    for i, j in zip(results, exp_results):
        assert i == j, "Incorrect row returned"


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def test_config_application_error_wide(remap_config_source_error_wide,
                                       comment_char, skiplines,
                                       wide_sample_col):
    """Test the scenario where the application source mapping file is meant to
    be used but there is not a valid mapper for that application. This should
    raise a KeyError
    """
    cfg_file, infile, target, exp_results, error_app = \
        remap_config_source_error_wide

    with pytest.raises(KeyError) as err:
        for row in remap.remap(
                target,
                source=None,
                infile=infile, config=cfg_file,
                comment=comment_char,
                skiplines=skiplines,
                verbose=False,
                input_format='wide',
                allow_unknown_sex=False,
                sample_column=wide_sample_col,
                missing_action='error',
                withdrawn_action='remove'
        ):
            pass
    assert err.value.args[0] == f'Unknown mapping application: {error_app}'


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def test_config_withdrawn_wide(remap_config_withdrawn_wide, comment_char,
                               skiplines, wide_sample_col):
    """Test the scenario where both missing and withdrawn samples are
    correctly set to withdrawn status in a wide file.
    """
    cfg_file, infile, source, target, exp_results = remap_config_withdrawn_wide
    results = []
    for row in remap.remap(
            target,
            source=source,
            infile=infile, config=cfg_file,
            comment=comment_char,
            skiplines=skiplines,
            verbose=False,
            input_format='wide',
            allow_unknown_sex=True,
            sample_column=wide_sample_col,
            missing_action='withdrawn',
            withdrawn_action='withdrawn'
    ):
        results.append(row)

    for i, j in zip(results, exp_results):
        assert i == j, "Incorrect row returned"


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def test_config_remove_wide(remap_config_remove_wide, comment_char,
                            skiplines, wide_sample_col):
    """Test the scenario where both missing and withdrawn samples are
    correctly removed in a wide file.
    """
    cfg_file, infile, source, target, exp_results = remap_config_remove_wide
    results = []
    for row in remap.remap(
            target,
            source=source,
            infile=infile, config=cfg_file,
            comment=comment_char,
            skiplines=skiplines,
            verbose=False,
            input_format='wide',
            allow_unknown_sex=True,
            sample_column=wide_sample_col,
            missing_action='remove',
            withdrawn_action='remove'
    ):
        results.append(row)

    for i, j in zip(results, exp_results):
        assert i == j, "Incorrect row returned"


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def test_config_error_wide(remap_config_error_wide, comment_char,
                           skiplines, wide_sample_col):
    """Test the scenario where missing samples are correcly raise an error in
    a wide file remapping.
    """
    cfg_file, infile, source, target, error_sample = remap_config_error_wide
    with pytest.raises(KeyError) as err:
        for row in remap.remap(
                target,
                source=source,
                infile=infile, config=cfg_file,
                comment=comment_char,
                skiplines=skiplines,
                verbose=False,
                input_format='wide',
                allow_unknown_sex=True,
                sample_column=wide_sample_col,
                missing_action='error',
                withdrawn_action='withdrawn'
        ):
            pass
    assert err.value.args[0] == \
           f"Can't find sample ID (eid) in mapper: {error_sample}"


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def test_config_all_error_wide(remap_config_error_wide, comment_char,
                               skiplines, wide_sample_col):
    """Test the scenario where all samples are set to withdrawn when mapping.
    This should raise a ValueError. This is simulated by mixing up the source
    and target applications.
    """
    cfg_file, infile, source, target, error_sample = remap_config_error_wide
    with pytest.raises(ValueError) as err:
        for row in remap.remap(
                source,
                source=target,
                infile=infile, config=cfg_file,
                comment=comment_char,
                skiplines=skiplines,
                verbose=False,
                input_format='wide',
                allow_unknown_sex=True,
                sample_column=wide_sample_col,
                missing_action='withdrawn',
                withdrawn_action='withdrawn'
        ):
            pass
    assert err.value.args[0] == "No sample IDs were remapped"


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def test_config_basic_long(remap_config_basic_long, comment_char, skiplines,
                           long_sample_col):
    """Test the basic scenario where everything is present, no withdrawn or
    bad IDs.
    """
    cfg_file, infile, source, target, exp_results = remap_config_basic_long
    results = []
    for row in remap.remap(
            target,
            source=source,
            infile=infile, config=cfg_file,
            comment=comment_char,
            skiplines=skiplines,
            verbose=False,
            input_format='long',
            allow_unknown_sex=False,
            sample_column=long_sample_col,
            missing_action='error',
            withdrawn_action='remove'
    ):
        results.append(row)

    for i, j in zip(results, exp_results):
        assert i == j, "Incorrect row returned"


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def test_config_application_basic_long(remap_config_basic_long, comment_char,
                                       skiplines, long_sample_col):
    """Test the basic scenario where everything is present, no withdrawn or
    bad IDs. In this version, the source is set to NoneType, so this tests
    That the application source is used and remaps correctly.
    """
    cfg_file, infile, source, target, exp_results = remap_config_basic_long
    results = []
    for row in remap.remap(
            target,
            source=None,
            infile=infile, config=cfg_file,
            comment=comment_char,
            skiplines=skiplines,
            verbose=False,
            input_format='long',
            allow_unknown_sex=False,
            sample_column=long_sample_col,
            missing_action='error',
            withdrawn_action='remove'
    ):
        results.append(row)

    for i, j in zip(results, exp_results):
        assert i == j, "Incorrect row returned"


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def test_config_withdrawn_long(remap_config_withdrawn_long, comment_char,
                               skiplines, long_sample_col):
    """Test the scenario where both missing and withdrawn samples are
    correctly set to withdrawn status in a long file.
    """
    cfg_file, infile, source, target, \
        exp_results = remap_config_withdrawn_long
    results = []
    for row in remap.remap(
            target,
            source=source,
            infile=infile, config=cfg_file,
            comment=comment_char,
            skiplines=skiplines,
            verbose=False,
            input_format='long',
            allow_unknown_sex=True,
            sample_column=long_sample_col,
            missing_action='withdrawn',
            withdrawn_action='withdrawn'
    ):
        results.append(row)

    for i, j in zip(results, exp_results):
        assert i == j, "Incorrect row returned"


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def test_config_remove_long(remap_config_remove_long, comment_char,
                            skiplines, long_sample_col):
    """Test the scenario where both missing and withdrawn samples are
    correctly removed in a long file.
    """
    cfg_file, infile, source, target, \
        exp_results = remap_config_remove_long
    results = []
    for row in remap.remap(
            target,
            source=source,
            infile=infile, config=cfg_file,
            comment=comment_char,
            skiplines=skiplines,
            verbose=False,
            input_format='long',
            allow_unknown_sex=True,
            sample_column=long_sample_col,
            missing_action='remove',
            withdrawn_action='remove'
    ):
        results.append(row)

    for i, j in zip(results, exp_results):
        assert i == j, "Incorrect row returned"


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def test_config_error_long(remap_config_error_long, comment_char,
                           skiplines, long_sample_col):
    """Test the scenario where missing samples correctly raise a KeyError in a
    long file.
    """
    cfg_file, infile, source, target, error_sample = remap_config_error_long
    with pytest.raises(KeyError) as err:
        for row in remap.remap(
                target,
                source=source,
                infile=infile, config=cfg_file,
                comment=comment_char,
                skiplines=skiplines,
                verbose=False,
                input_format='long',
                allow_unknown_sex=True,
                sample_column=long_sample_col,
                missing_action='error',
                withdrawn_action='withdrawn'
        ):
            pass
    assert err.value.args[0] == \
           f"Can't find sample ID (eid) in mapper: {error_sample}"


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def test_config_all_error_long(remap_config_error_long, comment_char,
                               skiplines, long_sample_col):
    """Test the scenario where all samples are set to withdrawn when mapping.
    This should raise a ValueError. This is simulated by mixing up the source
    and target applications.
    """
    cfg_file, infile, source, target, error_sample = remap_config_error_long
    with pytest.raises(ValueError) as err:
        for row in remap.remap(
                source,
                source=target,
                infile=infile,
                config=cfg_file,
                comment=comment_char,
                skiplines=skiplines,
                verbose=False,
                input_format='long',
                allow_unknown_sex=True,
                sample_column=long_sample_col,
                missing_action='withdrawn',
                withdrawn_action='withdrawn'
        ):
            pass
    assert err.value.args[0] == "No sample IDs were remapped"


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def test_filter_genotyped():
    """Test the filtering genotype function.
    """
    # Removal is inplace so test the input directly
    results = [
        ['1000001', 1, False, False],
        ['1000002', 1, True, False],
        ['1000003', 1, False, False],
        ['1000004', 1, True, False],
        ['1000005', 1, True, False],
        ['1000006', 1, False, False],
    ]

    exp_results = [
        ['1000002', 1, True, False],
        ['1000004', 1, True, False],
        ['1000005', 1, True, False],
    ]

    remap.filter_genotyped(results)
    for i, j in zip(results, exp_results):
        assert i == j, "Incorrect row"


@pytest.mark.parametrize(
    "source,target,withdrawn,unknown_sex,exp_res",
    # Each element in this list represents a single parameter
    [
        (
            # Basic, everything matches up
            [['1000001', 1, False, False],
             ['1000002', 2, True, False],
             ['1000003', 1, False, False]],
            [['2000001', 1, False, False],
             ['2000002', 2, True, False],
             ['2000003', 1, False, False]],
            None,
            False,
            {'1000001': ['2000001', 1, False, False],
             '1000002': ['2000002', 2, True, False],
             '1000003': ['2000003', 1, False, False]},
        ),
        (
                # Overiding withdrawn, these should be merged in the
                # mapper
                [['1000001', 1, False, False],
                 ['1000002', 2, True, True],
                 ['1000003', 1, False, False]],
                [['2000001', 1, False, False],
                 ['2000002', 2, True, False],
                 ['2000003', 1, False, True]],
                None,
                False,
                {'1000001': ['2000001', 1, False, False],
                 '1000002': ['2000002', 2, True, True],
                 '1000003': ['2000003', 1, False, True]},
        ),
        (
                # A withdrawn source EID, this should not make it into the
                # mapper but the target one should
                [['1000001', 1, False, False],
                 ['-1', 2, True, True],
                 ['1000003', 1, False, False]],
                [['2000001', 1, False, False],
                 ['2000002', 2, True, False],
                 ['-2', 1, False, True]],
                None,
                False,
                {'1000001': ['2000001', 1, False, False],
                 '1000003': ['-2', 1, False, True]},
        ),
        (
                # Externally derived withdrawn, these should override
                [['1000001', 1, False, False],
                 ['1000002', 2, True, False],
                 ['1000003', 1, False, False]],
                [['2000001', 1, False, False],
                 ['2000002', 2, True, False],
                 ['2000003', 1, False, False]],
                set(['1000002', '1000003']),
                False,
                {'1000001': ['2000001', 1, False, False],
                 '1000002': ['2000002', 2, True, True],
                 '1000003': ['2000003', 1, False, True]},
        ),
        (
                # Unknown sex assignments should be ok when option set to True
                [['1000001', 1, False, False],
                 ['1000002', 0, True, False],
                 ['1000003', 1, False, False]],
                [['2000001', 1, False, False],
                 ['2000002', 2, True, False],
                 ['2000003', 0, False, False]],
                None,
                True,
                {'1000001': ['2000001', 1, False, False],
                 '1000002': ['2000002', 2, True, False],
                 '1000003': ['2000003', 0, False, False]},
        ),
        (
                # A mix of all the different options above
                [['1000001', 1, False, True],
                 ['1000002', 0, True, False],
                 ['1000003', 1, False, False],
                 ['-1', 1, False, False]],
                [['2000001', 1, False, False],
                 ['2000002', 0, True, True],
                 ['-1', 1, False, False],
                 ['2000004', 1, False, False]],
                set(['1000003']),
                True,
                {'1000001': ['2000001', 1, False, True],
                 '1000002': ['2000002', 0, True, True],
                 '1000003': ['-1', 1, False, True]},
        ),
        (
                # Test that when samples are different lengths the genotype
                # filter works correctly to give the same length.
                [['1000001', 1, True, False],
                 ['1000002', 2, True, False],
                 ['1000003', 1, False, False],
                 ['1000004', 2, False, False],
                 ['1000005', 2, True, False],
                 ['1000006', 1, True, False],
                 ['1000007', 2, False, False]],
                [['2000001', 1, True, False],
                 ['2000002', 2, True, False],
                 ['2000005', 2, True, False],
                 ['2000006', 1, True, False],
                 ['2000008', 1, False, True]],
                None,
                False,
                {'1000001': ['2000001', 1, True, False],
                 '1000002': ['2000002', 2, True, False],
                 '1000005': ['2000005', 2, True, False],
                 '1000006': ['2000006', 1, True, False]},
        ),
    ]
)
def test_generate_mapping_set(source, target, withdrawn, unknown_sex,
                              exp_res):
    """Test various permutations when creating a mapping set.
    """
    results = remap.generate_mapping_set(
        source, target, allow_unknown_sex=unknown_sex,
        withdrawn_ids=withdrawn
    )
    assert results == exp_res, "Wrong mapper returned"


@pytest.mark.parametrize(
    "source,target,withdrawn,unknown_sex,exp_error",
    # Each element in this list represents a single parameter
    [
        (
            # Test unknown sex errors when there are some present
            [['1000001', 0, False, False],
             ['1000002', 2, True, False],
             ['1000003', 1, False, False]],
            [['2000001', 0, False, False],
             ['2000002', 2, True, False],
             ['2000003', 1, False, False]],
            None,
            False,
            ValueError,
        ),
        (
            # Test sex descrepancy errors are raised
            [['1000001', 1, False, False],
             ['1000002', 2, True, False],
             ['1000003', 1, False, False]],
            [['2000001', 1, False, False],
             ['2000002', 2, True, False],
             ['2000003', 2, False, False]],
            None,
            False,
            ValueError,
        ),
        (
            # Test the empty mapper error is raised
            [['-1', 1, False, False],
             ['-2', 2, True, False],
             ['-3', 1, False, False]],
            [['2000001', 1, False, False],
             ['2000002', 2, True, False],
             ['2000003', 1, False, False]],
            None,
            False,
            IndexError,
        ),
        (
            # Test that an EID error is raised on bad source EID
            [['1000001', 1, False, False],
             ['1000002', 2, True, False],
             ['000003', 1, False, False]],
            [['2000001', 1, False, False],
             ['2000002', 2, True, False],
             ['2000003', 1, False, False]],
            None,
            False,
            errors.EidError,
        ),
        (
            # Test that an EID error is raised on bad target EID
            [['1000001', 1, False, False],
             ['1000002', 2, True, False],
             ['1000003', 1, False, False]],
            [['2000001', 1, False, False],
             ['20002', 2, True, False],
             ['2000003', 1, False, False]],
            None,
            False,
            errors.EidError,
        ),
        (
            # Test that the IndexError is raised when the samples are of
            # different lengths after genotype filtering
            [['1000001', 1, True, False],
             ['1000002', 2, True, False],
             ['1000003', 1, False, False],
             ['1000004', 2, False, False],
             ['1000005', 2, True, False],
             ['1000006', 1, True, False],
             ['1000007', 2, False, False]],
            [['2000001', 1, True, False],
             ['2000002', 2, True, False],
             ['2000005', 2, False, False],
             ['2000006', 1, True, False],
             ['2000008', 1, False, True]],
            None,
            False,
            IndexError
        ),
    ]
)
def test_generate_mapping_set_errors(source, target, withdrawn, unknown_sex,
                                     exp_error):
    """Test various error permutations when creating a mapping set.
    """
    with pytest.raises(exp_error):
        results = remap.generate_mapping_set(
            source, target, allow_unknown_sex=unknown_sex,
            withdrawn_ids=withdrawn
        )
