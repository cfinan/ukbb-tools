import pytest
import importlib


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.mark.parametrize(
    "import_loc",
    (
        'ukbb_tools',
        'ukbb_tools.cmd_args',
        'ukbb_tools.columns',
        'ukbb_tools.common',
        'ukbb_tools.config',
        'ukbb_tools.constants',
        'ukbb_tools.errors',
        'ukbb_tools.parsers',
        'ukbb_tools.remap',
        'ukbb_tools.samples',
        'ukbb_tools.type_casts',
        'ukbb_tools.data_dict',
        'ukbb_tools.data_dict.build',
        'ukbb_tools.data_dict.columns',
        'ukbb_tools.data_dict.constants',
        'ukbb_tools.data_dict.orm',
        'ukbb_tools.data_dict.parsers',
        'ukbb_tools.data_dict.query',
        'ukbb_tools.data_dict.type_casts',
        'ukbb_tools.example_data',
        'ukbb_tools.example_data.examples',
    )
)
def test_package_import(import_loc):
    """Test that the modules can be imported.
    """
    importlib.import_module(import_loc, package=None)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def test_alias_import():
    """Test that the modules can be imported.
    """
    from ukbb_tools import examples
