from ukbb_tools import config
from contextlib import contextmanager
import pytest
import os


_CFG_FILE = '.ukbb-tools.toml'
"""The default name for the config file (`str`).
"""
_CFG_ENV = 'UKBB_CONFIG'
"""The default name for the config file environment variable(`str`).
"""
_PACK_ENV = 'UKBB_PACKAGE_DIR'
"""The default name for the UKBB-tools package environment variable (`str`).
"""


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@contextmanager
def create_tmp_config(cfg_dir):
    """Create a temp placeholder config file that will be yielded and deleted
    on completion.

    Parameters
    ----------
    cfg_dir : `str`
        The directory to place the config file in.

    Yields
    ------
    cfg_file : `str`
        The path to the created config file.
    """
    cfg_path = os.path.join(cfg_dir, _CFG_FILE)
    open(cfg_path, 'wt').close()
    try:
        yield cfg_path
    finally:
        os.unlink(cfg_path)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _clear_env():
    """Clear all the the environment variables. This clears the config ENV,
    package ENV and HOME.
    """
    for i in [_CFG_ENV, _PACK_ENV, 'HOME']:
        try:
            del os.environ[i]
        except KeyError:
            pass


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def test_config_arg(tmpdir):
    """Test that correct config file is returned when supplied as a cmd_arg.
    """
    with create_tmp_config(tmpdir) as cfg_file:
        cfg_result = config.find_config_file(cmd_arg=cfg_file)
        assert cfg_result == cfg_file, "Wrong config file"


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def test_config_env(tmpdir):
    """Test that correct config file is returned when supplied as a
    config environment variable.
    """
    _clear_env()
    with create_tmp_config(tmpdir) as cfg_file:
        os.environ[_CFG_ENV] = cfg_file
        cfg_result = config.find_config_file(cmd_arg=None)
        assert cfg_result == cfg_file, "Wrong config file"


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def test_config_package(tmpdir):
    """Test that correct config file is returned when supplied as a
    UKBB-tools package dir environment variable.
    """
    _clear_env()
    with create_tmp_config(tmpdir) as cfg_file:
        os.environ[_PACK_ENV] = str(tmpdir)
        cfg_result = config.find_config_file(cmd_arg=None)
        assert cfg_result == cfg_file, "Wrong config file"


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def test_config_home(tmpdir):
    """Test that correct config file is returned when located in the root of
    home.
    """
    _clear_env()
    with create_tmp_config(tmpdir) as cfg_file:
        os.environ['HOME'] = str(tmpdir)
        cfg_result = config.find_config_file(cmd_arg=None)
        assert cfg_result == cfg_file, "Wrong config file"


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def test_config_error(tmpdir):
    """Test that a file not found error is raised when the config file is not
    found.
    """
    _clear_env()
    with pytest.raises(FileNotFoundError) as err:
        os.environ['HOME'] = str(tmpdir)
        config.find_config_file(cmd_arg=None)
