"""Fixtures (re-usable data components and settings) for tests
"""
from pyaddons import utils
import pytest
import os
import csv

try:
    _READ_MODE = 'rb'
    import tomllib
except ImportError:
    _READ_MODE = 'r'
    import toml as tomllib

_ROOT_DATASETS_DIR = os.path.join(os.path.dirname(__file__), "data")
"""The root path to the dataset files that are available (`str`)
"""
_REMAP_DATASETS_DIR = os.path.join(_ROOT_DATASETS_DIR, "remap")
"""The root path to the dataset files that are available (`str`)
"""
_DELIMITER = '\t'
"""The default delimiter of all the test files (`str`).
"""

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.fixture()
def comment_char():
    """The comment character of the test files.
    """
    return '##'

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.fixture()
def skiplines():
    """The number of fixed lines to skip in the test files.
    """
    return 1

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.fixture()
def wide_sample_col():
    """The location of the sample columns in the test wide files.
    """
    return 4


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.fixture()
def long_sample_col():
    """The location of the sample column in the test long files.
    """
    return 'eid'


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.fixture()
def tmpfile(tmpdir):
    """Yield the path to a temp file.
    """
    tmpfile = utils.get_tmp_file(dir=tmpdir, prefix="test_file")
    os.unlink(tmpfile)
    yield tmpfile


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.fixture()
def vcf_file():
    """Return the path to a small test VCF file.
    """
    yield os.path.join(_ROOT_DATASETS_DIR, "test.vcf")


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def make_config_file(config_file, withdrawn, *mappers, application=12345,
                     seed=1984):
    """Generate a test configuration file to work with.

    Parameters
    ----------
    config_file : `str`
        The path where the configuration file should be written.
    withdrawn : `str`
        The path to the withdrawn samples file that will be added to the
        configuration file.
    *mappers : `tuple`
        The application numbers and paths to the mapping (`.sdef`) files that
        will be written to the configuration file.
    application : `int`, optional, default: `12345`
        The application number of the configuration file.
    seed : `int`, optional, default: `1984`
        The seed for the configuration file.

    Returns
    -------
    cfg_file : `str`
        The path to the cofig file, this should be the same path that was
        passed.
    """
    cfg = dict(
        general=dict(seed=seed, application=application, withdrawn=withdrawn),
        mapping={}
    )
    for aid, mfile in mappers:
        cfg['mapping'][str(aid)] = mfile

    with open(config_file, 'wt') as toml_file:
        tomllib.dump(cfg, toml_file)
    return config_file


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def read_infile(infile):
    """Read a test input file, into a list. The delimiter is the module
    default delimiter.

    Parameters
    ----------
    infile : `str`
        The path to the input file to read in.

    Returns
    -------
    input_data : `list` of `list` of `str`
        The data in the input file.
    """
    with open(infile, 'rt') as fobj:
        reader = csv.reader(fobj, delimiter=_DELIMITER)
        return [row for row in reader]


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_expected_long_results(infile, data_start_row, exp_col):
    """Get some expected results using the long input file as a template.

    The IDs for the expected result data should be located in the input file
    at `exp_col` and the header row for the input file should be at
    `data_start`.

    Parameters
    ----------
    infile : `str`
        The path to the template input file.
    data_start_row `int`
        The index position of the header row for the data in the input file
        (0-based).
    exp_col `int`
        The index position of the expected output data column the input file
        (0-based).

    Returns
    -------
    expected_results : `list` of `list` of `str`
        The data in the input file after being transformed into the expected
        test output.
    """
    exp_results = read_infile(infile)
    for i in exp_results[data_start_row:]:
        i[0] = i[exp_col]
    return exp_results


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_expected_wide_results(infile, data_start_row, data_col_start,
                              exp_row):
    """Get some expected results using the wide input file as a template.

    The IDs for the expected result data should be located in the input file
    at `exp_col` and the header row for the input file should be at
    `data_start`.

    Parameters
    ----------
    infile : `str`
        The path to the template input file.
    data_start_row `int`
        The index position of the header row for the data in the input file
        (0-based).
    data_start_col `int`
        The index position in the header row where the sample columns start
        (0-based).
    exp_row `int`
        The index position of the expected output data row the input file
        (0-based).

    Returns
    -------
    expected_results : `list` of `list` of `str`
        The data in the input file after being transformed into the expected
        test output.
    """
    exp_results = read_infile(infile)
    exp_results[data_start_row][data_col_start:] = \
        exp_results[exp_row][data_col_start:]
    return exp_results


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.fixture()
def remap_config_basic_wide(tmpfile):
    """Gather the data for testing a basic remapping for a wide file.
    """
    source = 20001
    target = 10001
    cfg_file = make_config_file(
        tmpfile,
        os.path.join(_REMAP_DATASETS_DIR, 'empty-withdrawn.txt'),
        (source, os.path.join(_REMAP_DATASETS_DIR, 'test2.sdef')),
        (target, os.path.join(_REMAP_DATASETS_DIR, 'test1.sdef')),
        application=source
    )
    infile = os.path.join(_REMAP_DATASETS_DIR, 'test1-2-wide-basic.txt')
    exp_results = get_expected_wide_results(infile, 5, 4, -1)
    yield cfg_file, infile, source, target, exp_results
    os.unlink(cfg_file)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.fixture()
def remap_config_source_error_wide(tmpfile):
    """Gather the data for testing a source application that does not exist.
    """
    app_source = 20001
    map_source = 20002
    target = 10001
    cfg_file = make_config_file(
        tmpfile,
        os.path.join(_REMAP_DATASETS_DIR, 'empty-withdrawn.txt'),
        (map_source, os.path.join(_REMAP_DATASETS_DIR, 'test2.sdef')),
        (target, os.path.join(_REMAP_DATASETS_DIR, 'test1.sdef')),
        application=app_source
    )
    infile = os.path.join(_REMAP_DATASETS_DIR, 'test1-2-wide-basic.txt')
    exp_results = get_expected_wide_results(infile, 5, 4, -1)
    yield cfg_file, infile, target, exp_results, app_source
    os.unlink(cfg_file)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.fixture()
def remap_config_withdrawn_wide(tmpfile):
    """Gather the data for testing remapping for a wide file where
    missing/withdrawn samples are set to withdrawn.
    """
    source = 20001
    target = 10001
    cfg_file = make_config_file(
        tmpfile,
        os.path.join(_REMAP_DATASETS_DIR, 'empty-withdrawn.txt'),
        (source, os.path.join(_REMAP_DATASETS_DIR, 'test4.sdef')),
        (target, os.path.join(_REMAP_DATASETS_DIR, 'test3.sdef')),
        application=source
    )
    infile = os.path.join(_REMAP_DATASETS_DIR, 'test3-4-wide-withdrawn.txt')
    exp_results = get_expected_wide_results(infile, 2, 4, -1)
    yield cfg_file, infile, source, target, exp_results
    os.unlink(cfg_file)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.fixture()
def remap_config_remove_wide(tmpfile):
    """Gather the data for testing remapping for a wide file where
    missing/withdrawn samples are removed.
    """
    data_start = 2
    source = 20001
    target = 10001
    cfg_file = make_config_file(
        tmpfile,
        os.path.join(_REMAP_DATASETS_DIR, 'empty-withdrawn.txt'),
        (source, os.path.join(_REMAP_DATASETS_DIR, 'test4.sdef')),
        (target, os.path.join(_REMAP_DATASETS_DIR, 'test3.sdef')),
        application=source
    )
    infile = os.path.join(_REMAP_DATASETS_DIR, 'test3-4-wide-withdrawn.txt')
    exp_results = get_expected_wide_results(infile, 2, 4, -1)
    to_remove = []
    for idx, i in enumerate(exp_results[data_start]):
        try:
            if int(i) < 0:
                to_remove.append(idx)
        except ValueError:
            pass
    to_remove.sort(reverse=True)
    for i in exp_results[data_start:]:
        for j in to_remove:
            i.pop(j)
    yield cfg_file, infile, source, target, exp_results
    os.unlink(cfg_file)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.fixture()
def remap_config_error_wide(tmpfile):
    """Gather the data for testing remapping for a wide file where
    missing samples raise an error.
    """
    error_sample = "2000013"
    source = 20001
    target = 10001
    cfg_file = make_config_file(
        tmpfile,
        os.path.join(_REMAP_DATASETS_DIR, 'empty-withdrawn.txt'),
        (source, os.path.join(_REMAP_DATASETS_DIR, 'test4.sdef')),
        (target, os.path.join(_REMAP_DATASETS_DIR, 'test3.sdef')),
        application=source
    )
    infile = os.path.join(_REMAP_DATASETS_DIR, 'test3-4-wide-withdrawn.txt')
    yield cfg_file, infile, source, target, error_sample
    os.unlink(cfg_file)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.fixture()
def remap_config_basic_long(tmpfile):
    """Gather the data for testing a basic remapping for a long file.
    """
    source = 20001
    target = 10001
    cfg_file = make_config_file(
        tmpfile,
        os.path.join(_REMAP_DATASETS_DIR, 'empty-withdrawn.txt'),
        (source, os.path.join(_REMAP_DATASETS_DIR, 'test2.sdef')),
        (target, os.path.join(_REMAP_DATASETS_DIR, 'test1.sdef')),
        application=source
    )
    infile = os.path.join(_REMAP_DATASETS_DIR, 'test1-2-long-basic.txt')
    exp_results = get_expected_long_results(infile, 6, 5)
    yield cfg_file, infile, source, target, exp_results
    os.unlink(cfg_file)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.fixture()
def remap_config_withdrawn_long(tmpfile):
    """Gather the data for testing remapping for a long file where
    missing/withdrawn samples are set to withdrawn.
    """
    source = 20001
    target = 10001
    cfg_file = make_config_file(
        tmpfile,
        os.path.join(_REMAP_DATASETS_DIR, 'empty-withdrawn.txt'),
        (source, os.path.join(_REMAP_DATASETS_DIR, 'test4.sdef')),
        (target, os.path.join(_REMAP_DATASETS_DIR, 'test3.sdef')),
        application=source
    )
    infile = os.path.join(_REMAP_DATASETS_DIR, 'test3-4-long-withdrawn.txt')
    exp_results = get_expected_long_results(infile, 4, 5)
    yield cfg_file, infile, source, target, exp_results
    os.unlink(cfg_file)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.fixture()
def remap_config_remove_long(tmpfile):
    """Gather the data for testing remapping for a long file where
    missing/withdrawn samples are removed.
    """
    source = 20001
    target = 10001
    data_start = 4
    cfg_file = make_config_file(
        tmpfile,
        os.path.join(_REMAP_DATASETS_DIR, 'empty-withdrawn.txt'),
        (source, os.path.join(_REMAP_DATASETS_DIR, 'test4.sdef')),
        (target, os.path.join(_REMAP_DATASETS_DIR, 'test3.sdef')),
        application=source
    )
    infile = os.path.join(_REMAP_DATASETS_DIR, 'test3-4-long-withdrawn.txt')
    exp_results = get_expected_long_results(infile, data_start, 5)
    for i in range(len(exp_results) - 1, data_start, -1):
        try:
            if int(exp_results[i][0]) < 0:
                exp_results.pop(i)
        except ValueError:
            pass
    yield cfg_file, infile, source, target, exp_results
    os.unlink(cfg_file)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.fixture()
def remap_config_error_long(tmpfile):
    """Gather the data for testing remapping for a long file where
    missing samples raise an error.
    """
    error_sample = "2000013"
    source = 20001
    target = 10001
    data_start = 4
    cfg_file = make_config_file(
        tmpfile,
        os.path.join(_REMAP_DATASETS_DIR, 'empty-withdrawn.txt'),
        (source, os.path.join(_REMAP_DATASETS_DIR, 'test4.sdef')),
        (target, os.path.join(_REMAP_DATASETS_DIR, 'test3.sdef')),
        application=source
    )
    infile = os.path.join(_REMAP_DATASETS_DIR, 'test3-4-long-withdrawn.txt')
    yield cfg_file, infile, source, target, error_sample

    os.unlink(cfg_file)
