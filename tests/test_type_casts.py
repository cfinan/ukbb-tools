"""Tests for the `ukbb_tools.type_casts` module.
"""
from ukbb_tools import type_casts as tc, errors
import pytest


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.mark.parametrize(
    "value, error",
    (
        ('-1', True),
        ('1003456', False),
        ('-1003456', True),
        ('-100345', True),
    )
)
def test_parse_eid(value, error):
    """Test that the ``ukbb_tools.type_casts.parse_eid`` function is working as
    expected good+errors.
    """
    if error is True:
        with pytest.raises(errors.EidError) as the_error:
            tc.parse_eid(value)
        assert the_error.value.eid == value, "wrong error value"
    else:
        assert tc.parse_eid(value) == value, "wrong value"
