"""Tests for the parsing classes
"""
from ukbb_tools import parsers
import pytest


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.mark.parametrize(
    # These are the variabe names by which the parameters in the list will be
    # available in the test function
    "use_dict",
    # Each element in this list represents a single parameter
    [
        # Test0
        (
            False,
        ),
        # Test1
        (
            True,
        ),
    ]
)
def test_vcf_parser_samples(vcf_file, use_dict):
    """Test that the `ukbb_tools.parsers.BasicVcfParser` detects the correct
    samples.
    """
    exp_samples = ["NA00001", "NA00002", "NA00003"]
    with parsers.BasicVcfParser(vcf_file, use_dict=use_dict) as p:
        assert p.samples == exp_samples, "wrong samples"


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.mark.parametrize(
    # These are the variabe names by which the parameters in the list will be
    # available in the test function
    "use_dict",
    # Each element in this list represents a single parameter
    [
        # Test0
        (
            False,
        ),
        # Test1
        (
            True,
        ),
    ]
)
def test_vcf_parser_header(vcf_file, use_dict):
    """Test that the `ukbb_tools.parsers.BasicVcfParser` detects the correct
    header.
    """
    exp_header = [
        '##fileformat=VCFv4.1',
        '##fileDate=20090805',
        '##source=myImputationProgramV3.1',
        '##reference=file:///seq/references/1000GenomesPilot-NCBI36.fasta',
        '##contig=<ID=20,length=62435964,assembly=B36,md5=f126cdf8a6e0c7f3'
        '79d618ff66beb2da,species="Homo sapiens",taxonomy=x>',
        '##phasing=partial',
        '##INFO=<ID=NS,Number=1,Type=Integer,Description="Number of Samples '
        'With Data">',
        '##INFO=<ID=DP,Number=1,Type=Integer,Description="Total Depth">',
        '##INFO=<ID=AF,Number=A,Type=Float,Description="Allele Frequency">',
        '##INFO=<ID=AA,Number=1,Type=String,Description="Ancestral Allele">',
        '##INFO=<ID=DB,Number=0,Type=Flag,Description="dbSNP membership, build'
        ' 129">',
        '##INFO=<ID=H2,Number=0,Type=Flag,Description="HapMap2 membership">',
        '##FILTER=<ID=q10,Description="Quality below 10">',
        '##FILTER=<ID=s50,Description="Less than 50% of samples have data">',
        '##FORMAT=<ID=GT,Number=1,Type=String,Description="Genotype">',
        '##FORMAT=<ID=GQ,Number=1,Type=Integer,Description="Genotype Quality"'
        '>',
        '##FORMAT=<ID=DP,Number=1,Type=Integer,Description="Read Depth">',
        '##FORMAT=<ID=HQ,Number=2,Type=Integer,Description="Haplotype Quality"'
        '>'
    ]
    with parsers.BasicVcfParser(vcf_file, use_dict=use_dict) as p:
        assert p.skipped_rows == exp_header, "wrong header rows"
