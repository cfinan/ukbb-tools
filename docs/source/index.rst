.. ukbb-tools documentation master file, created by
   sphinx-quickstart on Thu Jan 28 09:21:27 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

ukbb-tools
==========

A Python based toolkit and API for handling UK BioBank data.

Contents
========

.. toctree::
   :maxdepth: 2
   :caption: Setup

   getting_started
   env_variables

.. toctree::
   :maxdepth: 2
   :caption: Programmer reference

   walkthrough
   file_formats
   data_dict
   scripts
   api

.. toctree::
   :maxdepth: 2
   :caption: Example code

   examples/dd_orm_usage

.. toctree::
   :maxdepth: 2
   :caption: Project admin

   contributing

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
