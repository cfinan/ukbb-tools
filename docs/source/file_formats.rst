============
File formats
============

This details the format of all the flat files that are produced by UKBB tools.

Sample definition files ``.sdef``
---------------------------------

.. _sdef_format:

This is a simple four column file that is used to store UK Biobank sample IDs (EIDs) in a defined order, either for mapping IDs between projects or for ordering the samples in output files in a defined way. Each row has a single sample and the sample IDs should be unique.

.. include:: ./data_dict/sdef.rst


Sample withdrawal file ``.txt``
-------------------------------

.. _withdrawn_format:

This is essentially a list file with no header and a single sample ID (EID) on each line corresponding to each participant that has withdrawn from the study.
