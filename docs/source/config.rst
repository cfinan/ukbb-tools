=============================
UK Biobank configuration file
=============================

.. _ukbb_config_background:

The UKBB tools allows for the user to setup a configuration file that contains some relevant information about the paths to key files and some other key information such as the application number. A configuration file, is generally considered as a configuration for a single UKBiobank application. The config file is entirely optional but it can simplify the command line arguments that are given to some programs.

The config file locations
-------------------------

There are several possibly locations that the config file can exist at. They are searched in the following order.

1. A user specified location via a ``--config`` flag to a script. All the scripts that can utilise a configuration file have this flag.
2. The location defined in ``UKBB_CONFIG`` :ref:`environment variable <env_var_config_file>`.
3. A file called ``.ukbb-tools.toml`` in the root of the :ref:`package directory <env_var_package_dir>`.
4. Finally, a file called ``.ukbb-tools.toml`` in the root of the home directory.

If the user specifies something that depends on the configuration file and it is not available. The a `FileNotFoundError` will be raised.

The config file format
----------------------

The configuration file should be defined in a `Toms Obvious Minimal Language <https://toml.io/en/>`_, or ``.toml`` format. This is a simple format that can be hand coded. Currently only two section headings are supported. These are detailed below

The ``[general]`` section
~~~~~~~~~~~~~~~~~~~~~~~~~

This contains some general fields that are applicable for the specific UKBiobank application.

* ``application``: This key contains a single integer value that corresponds to the applications ID number. This is required in all configuration files.
* ``withdrawn``: This key contains a single string that corresponds to the full path to the UKBiobank :ref:`withdrawals <withdrawn_format>` file. This is required and automatically loaded and applied by many scripts.
* ``seed``: This key contains a single integer value that corresponds to a specific random seed that will be used if the user wants to set a specific random seed. This is optional.

The ``[mapping]`` section
~~~~~~~~~~~~~~~~~~~~~~~~~

This contains integer keys relating to UKBiobank application identifiers and the values are full paths to `sample definition <sdef_format>` (``.sdef``) files that can be used to map from one UKBiobank application to another.

Example file
~~~~~~~~~~~~

Below is an example ``.ukbb-tools.toml`` configuration file:

.. code-block:: toml

   # The UKBB-tools config file, this should be toml format
   [general]
   application = 12113
   seed = 1984
   withdrawn = "/data/Ukbb/Withdrawn/withdraw12113_492_20240613.txt"

   [mapping]
   12113 = "/data/Ukbb/MappingSdef/app12113.sdef"
   9922 = "/data/Ukbb/MappingSdef/app9922.sdef"
   24711 = "/data/Ukbb/MappingSdef/app24711.sdef"
