======================
UKBB tools environment
======================

.. _env_variables:

There are a number of environment variables and directory locations that can be configured in the ukbb-tools package. These are detailed in this document.

The package directory
---------------------

.. _env_var_package_dir:

UKBB tools is distributed with a copy of the UK Biobank data dictionary. When this is first used it will be copied over to the package directory, where it can be used. By default the package directory is called ``.ukbb_tools`` and is located in the root of your home directory.

However, it is possible to change this location using the environment variable ``UKBB_PACKAGE_DIR``. This can be set in your ``~/.bashrc`` or ``~/.bash_profile`` by adding the following:

.. code-block::

   export UKBB_PACKAGE_DIR="/path/to/your/package/directory"


The configuration file location
-------------------------------

.. _env_var_config_file:

The location of the configuration file can also be set as an environment variable ``UKBB_CONFIG``. This can be set in your ``~/.bashrc`` or ``~/.bash_profile`` by adding the following:

.. code-block::

   export UKBB_CONFIG="/path/to/your/config/ukbb-tools.toml"

