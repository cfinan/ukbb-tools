==========================
UK Biobank data dictionary
==========================

.. _ukbb_dd_background:

The UKBB tools package contains a copy of the UK Biobank data dictionary as an SQLite database. If this is out of date, then the user has the option to :ref:`build <ukbb_dd_build>` a new copy. There is also the option to :ref:`build via the API <ukbb_dd_build_api>`.

Users do not necessarily need to query this directly but it can be browsed using utilities such as `dbeaver <https://dbeaver.io/>`_. Additionally, Pythonic access can be accomplished via `sqlite3 <https://docs.python.org/3.7/library/sqlite3.html>`_ or `SQLAlchemy <https://www.sqlalchemy.org/>`_ using :ref:`ORM classes <ukbb_dd_orm_api>`.

The raw data
------------

The current version of the UKBiobank data dictionary is based on updated files that the UKBiobak made available from the `UKBB website <https://biobank.ctsu.ox.ac.uk/crystal/exinfo.cgi?src=accessing_data_guide>`_. The old files, namely the data dictionary file and the codings file are no longer available. I have kept the script for these files (renamed to ``ukbb-build-old-data-dict``) for the moment but it will eventually be removed. The new build script ``ukbb-build-data-dict`` (and API), will download all the data required to build the data dictionary and the user does not need to supply anything, they only have to make sure the package directory is available.

The ``0-0.0`` field
-------------------

The old data dictionary added a dummy field designated ``0-0.0``  to indicate sample IDs that have withdrawn from the study. However, this is removed in the new script as withdrawn samples are generally handled a lot better now, so it is not really needed.

Data type fields
----------------

The old data dictionary made the following mappings in the data dictionary, the names of the data types have been changed to:

1. Categorical single -> ``enum_single``
2. Categorical multiple -> ``enum_multiple``
3. Date -> ``date``
4. Time -> ``time``
5. Compound -> ``str``
6. Integer -> ``int``
7. Continuous -> ``float``
8. Text -> ``str``

Whilst the new script does this slightly differently the end result is the same.

Linking fields and dates
------------------------

The original data dictionary files that were available were not that useful for relating fields with each other, for example, a measurement field and the date that the measure was taken. The original data dictionary build script attempted to link these up, some using simple algorithms, others using manual assignments with the default date for each field ID is the date of attending the assessment centre (field ID: 53). The new data dictionary files do contain information on linked fields. This is incorporated into the data dictionary database but is not fully utilised at the moment. Currently, the original field/date mappings defined in the previous build script are applied to the new data dictionary. This may change in future after I go through the linked field data. However, at a first glance the linked fields provided by UkBiobank do seem light on linking dates with fields. I have also noticed some fields/categories seem to be missing in the lookup tables for the new data, so I do not know how they handle referential integrity (the :ref:`new build script <ukbb_dd_build>` will give numbers of missing IDs).

Database schema
---------------

Below is a diagram of the currently implemented data dictionary schema.

.. image:: ./_static/data_dict_er.png
   :width: 500
   :alt: data-dictionary schema
   :align: center

.. include:: ./data_dict/data-dict-tables.rst
