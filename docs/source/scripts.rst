====================
Command line scripts
====================

There are several command line scripts that are installed with the package. These are detailed below.


UK Biobank data dictionary
==========================

.. _ukbb_dd_build:

A copy of the UK Biobank data dictionary database is provided with the package. However, there are scripts available if you want to build a new copy, potentially against a different database backend. However, please be aware, currently, this has only been tested against SQLite, although, it will probably work against MySQL/MariaDB. If you are building for a different database to SQLite then you will need to :ref:`use the API <ukbb_dd_build_api>`.

``ukbb-build-data-dict``
------------------------

.. argparse::
   :module: ukbb_tools.data_dict.build
   :func: _init_cmd_args
   :prog: ukbb-build-data-dict

Example usage
~~~~~~~~~~~~~

Here is a example build of the data dictionary. For more information about the various warnings please see the documentation on the :ref:`data dictionary <ukbb_dd_warnings>`.

.. code-block:: console

   $ ukbb-build-data-dict -vv ~/data-dict.db
   === ukbb-build-data-dict (ukbb_tools v0.2.1a0) ===
   [info] 11:34:17 > outdb value: /home/rmjdcfi/data-dict.db
   [info] 11:34:17 > tmpdir value: None
   [info] 11:34:17 > verbose value: 2
   [info] 11:34:17 > creating tables
   [info] 11:34:17 > downloading data dictionary tables
   [info] 11:34:27 > building coding tables
   [info] 11:35:45 > building data path tables
   [info] 11:35:46 > categories missing from lookup: 1
   [info] 11:35:46 > building field ID tables
   [info] 11:35:48 > building field ID link tables
   [info] 11:35:49 > fields missing from lookup: 4645
   [info] 11:35:49 > *** END ***


UK Biobank samples
==================

.. _ukbb_samples:

``ukbb-samples``
----------------

.. argparse::
   :module: ukbb_tools.samples
   :func: _init_cmd_args
   :prog: ukbb-samples

Example usage
~~~~~~~~~~~~~

Compare the samples extracted from a set of exome VCF files. This might be useful for creating a mapping file for exome data.

.. code-block:: console

   $ ukbb-samples -vv -T ~/Scratch/tmp/ --allow-bad-eid /projects/UKB/withdrawals/latest_withdrawals.txt ~/sample_outputs/vcf-samples- /projects/UKB/genotypes/exome/*.vcf.gz
   === ukbb-samples (ukbb_tools v0.1.3a0) ===
   [info] 14:09:25 > allow_bad_eid value: True
   [info] 14:09:25 > complex value: False
   [info] 14:09:25 > encoding value: utf-8
   [info] 14:09:25 > exclude value: None
   [info] 14:09:25 > include value: None
   [info] 14:09:25 > inputs length: 21
   [info] 14:09:25 > outprefix value: /home/user/sample_outputs/vcf-samples-
   [info] 14:09:25 > tmpdir value: /home/user/Scratch/tmp/
   [info] 14:09:25 > verbose value: 2
   [info] 14:09:25 > withdrawals value: /projects/UKB/withdrawals/latest_withdrawals.txt
   [info] 14:09:25 > search #inputs: 21
   [info] 14:09:25 > excluding #files: 0
   [info] 14:09:25 > excluding #dirs: 0
   [info] 14:09:25 > #withdrawals: 462
   [info] 14:09:25 > Found #search files: 21
   [info] 14:09:25 > Write DB to: /home/user/Scratch/tmp/
   [info] 14:09:37 > Found #samples: 200643
   [warning] 14:09:39 > Withdrawn samples not found in the sample list: 348/462 samples
   [info] 14:09:40 > Total samples set to withdrawn: 126
   [info] 14:09:40 > *** END ***

Compare the samples from .sample files. Here we can see that some of the withdrawn files have already been set to missing in the .sample files.

.. code-block:: console

   $ ukbb-samples -vv -T ~/Scratch/tmp/ --allow-bad-eid /projects/UKB/withdrawals/latest_withdrawals.txt ~/sample_outputs/bgen-samples- /projects/UKB/genotypes/imp/*.sample
   === ukbb-samples (ukbb_tools v0.1.3a0) ===
   [info] 14:09:25 > allow_bad_eid value: True
   [info] 14:09:25 > complex value: False
   [info] 14:09:25 > encoding value: utf-8
   [info] 14:09:25 > exclude value: None
   [info] 14:09:25 > include value: None
   [info] 14:09:25 > inputs length: 21
   [info] 14:09:25 > outprefix value: /home/user/sample_outputs/bgen-samples-
   [info] 14:09:25 > tmpdir value: /home/user/Scratch/tmp/
   [info] 14:09:25 > verbose value: 2
   [info] 14:09:25 > withdrawals value: /projects/UKB/withdrawals/latest_withdrawals.txt
   [info] 14:09:25 > search #inputs: 21
   [info] 14:09:25 > excluding #files: 0
   [info] 14:09:25 > excluding #dirs: 0
   [info] 14:09:25 > #withdrawals: 462
   [info] 14:09:25 > Found #search files: 22
   [info] 14:09:25 > Write DB to: /home/user/Scratch/tmp/
   [info] 14:09:37 > Found #samples: 487409
   [warning] 14:09:39 > Withdrawn samples not found in the sample list: 425/462 samples
   [info] 14:09:40 > Total samples set to withdrawn: 419
   [info] 14:09:40 > *** END ***


Extract all the valid file extensions from all sub-directories below ``/projects/UKB/phenotypes/baskets/downloads/all/``. This illustrates that bad files are ignored, either due to the header not being correct or some of the basket data is not UTF-8 encoded (the default). If you see these sorts of errors try ``--encoding "latin1"``. This also demonstrates some of the warnings and errors you might see. The errors in this case are not 100% fatal, you will still get an output but you sample list will be truncated.

.. code-block:: console

   $ ukbb-samples -vv -T ~/Scratch/tmp/ --allow-bad-eid /projects/UKB/withdrawals/latest_withdrawals.txt ~/sample_outputs/multiple-baskets- /projects/UKB/phenotypes/baskets/downloads/all/
   === ukbb-samples (ukbb_tools v0.1.3a0) ===
   [info] 14:11:18 > allow_bad_eid value: True
   [info] 14:11:18 > complex value: False
   [info] 14:11:18 > encoding value: utf-8
   [info] 14:11:18 > exclude value: None
   [info] 14:11:18 > include value: None
   [info] 14:11:18 > inputs length: 1
   [info] 14:11:18 > outprefix value: /home/user/sample_outputs/multiple-baskets-
   [info] 14:11:18 > tmpdir value: /home/user/Scratch/tmp/
   [info] 14:11:18 > verbose value: 2
   [info] 14:11:18 > withdrawals value: /projects/UKB/withdrawals/latest_withdrawals.txt
   [info] 14:11:18 > search #inputs: 1
   [info] 14:11:18 > excluding #files: 0
   [info] 14:11:18 > excluding #dirs: 0
   [info] 14:11:18 > #withdrawals: 462
   [info] 14:11:18 > Found #search files: 25
   [info] 14:11:18 > Write DB to: /home/rmjdcfi/Scratch/tmp/
   [error] 14:11:18 > issue parsing .csv file: Unexpected header value: 'eid' got '1000072 20205_2_0': /projects/UKB/phenotypes/baskets/downloads/all/bulkID_20205.csv
   [error] 14:13:04 > issue parsing .csv file: 'utf-8' codec can't decode byte 0x99 in position 3356: invalid start byte: /projects/UKB/phenotypes/baskets/downloads/all/ukb26332.csv
   [error] 14:13:35 > issue parsing .csv file: 'utf-8' codec can't decode byte 0x99 in position 5390: invalid start byte: /projects/UKB/phenotypes/baskets/downloads/all/ukb29341.enc_ukb.csv
   [error] 14:15:21 > issue parsing .csv file: 'utf-8' codec can't decode byte 0x99 in position 7141: invalid start byte: /projects/UKB/phenotypes/baskets/downloads/all/ukb29374.csv
   [error] 14:18:06 > issue parsing .csv file: 'utf-8' codec can't decode byte 0x99 in position 247: invalid start byte: /projects/UKB/phenotypes/baskets/downloads/all/ukb37076.csv
   [error] 14:19:34 > issue parsing .csv file: 'utf-8' codec can't decode byte 0x99 in position 6276: invalid start byte: /projects/UKB/phenotypes/baskets/downloads/all/ukb37077.csv
   [info] 14:41:06 > Found #samples: 502541
   [error] 14:41:40 > The samples have been ordered but do not represent all observed samples, possibly due to files being excluded: len(list) == '502536',  len(seen) == '502541'
   [warning] 14:42:59 > Withdrawn samples not found in the sample list: 111/462 samples
   [info] 14:43:30 > Total samples set to withdrawn: 351
   [info] 14:43:48 > *** END ***

.. _ukbb_remap:

``ukbb-remap``
--------------

.. argparse::
   :module: ukbb_tools.remap
   :func: _init_cmd_args
   :prog: ukbb-remap

Example usage
~~~~~~~~~~~~~

Remap samples in the input file ``ukb_app9922_ref_RF_20_0.5_final_clean_07092021.txt``, where are sample ID column is called ``'s'``. The source application (9922) mapping file is located in the config file and so is the target application 12113. This mapping is allowing of undefined sex (``0``) in the mapping file. The config file is located in one of the default locations.

.. code-block:: console

   $ ukbb-remap --allow-unknown-sex -vv -i ukb_app9922_ref_RF_20_0.5_final_clean_07092021.txt -c"s" -S 9922 12113 | gzip > ukb_app12113_ref_RF_20_0.5_final_clean_07092021.txt.gz
   === ukbb-remap (ukbb_tools v0.2.1a0) ===
   [info] 13:16:05 > allow_unknown_sex value: True
   [info] 13:16:05 > column value: s
   [info] 13:16:05 > comment_char value: None
   [info] 13:16:05 > config value: None
   [info] 13:16:05 > delimiter value: 
   [info] 13:16:05 > encoding value: UTF8
   [info] 13:16:05 > format value: long
   [info] 13:16:05 > infile value: ukb_app_9922_ref_RF_20_0.5_final_clean_07092021.txt
   [info] 13:16:05 > missing value: withdrawn
   [info] 13:16:05 > outfile value: None
   [info] 13:16:05 > skiplines value: 0
   [info] 13:16:05 > source_app value: 9922
   [info] 13:16:05 > target_app value: 12113
   [info] 13:16:05 > tmpdir value: None
   [info] 13:16:05 > verbose value: 2
   [info] 13:16:05 > withdrawn value: withdrawn
   [info] 13:16:05 > The source mapping is an application number
   [info] 13:16:05 > The target mapping is an application number
   [info] 13:16:05 > using config file: .ukbb-tools.toml
   [info] 13:16:05 > Using source mapping file: app9922.sdef
   [info] 13:16:05 > Using target mapping file: app12113.sdef
   [info] 13:16:10 > source and target samples are same length: 487409 vs. 487409
   [info] 13:16:10 > validating sex information
   [info] 13:16:11 > #samples: 487409
   [info] 13:16:11 > #withdrawn samples: 419
   [info] remapping file...: 38598 row(s) [00:01, 33354.16 row(s)/s]
   [info] 13:16:12 > #processed rows: 38598
   [info] 13:16:12 > #missing rows: 0
   [info] 13:16:12 > #withdrawn rows: 61

UK Biobank configuration file
=============================

.. _ukbb_config_script:

``ukbb-config``
---------------

.. argparse::
   :module: ukbb_tools.config
   :func: _init_cmd_args
   :prog: ukbb-config

Example usage
~~~~~~~~~~~~~

Get the seed value from a configuration file that is located in one of the default locations.

.. code-block:: console

    $ ukbb-config general seed
    1984

Get the UK Biobank application number from a configuration file that is located in a custom location.

.. code-block:: console

   $ ukbb-config general -c "/path/to/config.toml" application
   12113

