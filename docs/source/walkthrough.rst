=======================
UKBB tools walk-through
=======================

The UKBB tools package is a toolkit to perform some processing and subset of the UK Biobank data components that can be downloaded. It is primarily a suite of command line tools but the API is documented and many of the processing functions can be called via the API. There are also a developmental sub-setting API, that will allow the users to extract subsets of data into pandas data frames.

This walk through is intended for both data administrators, who have read/write access to the UK Biobank data and storage project space and end users, or analysts that will have read access to the processed files. In each section I have indicated the intended audience. I have put both in the same document as in many cases data administrators will also be end users as well.

This walk-through assumes you have `installed <https://gitlab.com/cfinan/ukbb-tools>`_ the package and downloaded the raw UK Biobank data including the latest list of sample withdrawals from UK Biobank.

1. Setting up your package directory
------------------------------------

*Administrators/Users*

Certain resources, such as the default data dictionary and configuration file location will be stored in a package directory. The default location is in a hidden directory within the root of your home directory ``~/.ukbb_tools``. However, you have the opportunity to set this to a location of your choosing. To learn more about this see the :ref:`environment <env_var_package_dir>` variables documentation.

2. Building the UK Biobank data dictionary
------------------------------------------

*Administrators/Users*

The UKBB tools package uses a copy of the UK Biobank data dictionary that has been built into an SQLite database. This is distributed with the package and is used to check data types and enumerations.

When you first use any tools that depend on it, it will be extracted to your package directory. for use. However, if you wish you can execute these commands from the Python console to get in place beforehand (this is optional).

.. code-block:: python

   from ukbb_tools import examples
   examples.get_data("data_dict")


The data dictionary is updated every time the package is updated. However, it is possible that it is not quite up-to-date or maybe you want to use a different database back end to SQLite. In either of these cases you may wish to re-build the data dictionary.

More information on the data dictionary can be found in the :ref:`data dictionary <ukbb_dd_background>` section and in the :ref:`script <ukbb_dd_build>` documentation.

When you have a copy of the data dictionary in place it will be used in the background by UKBB tools. However, you can also query it directly via an API as shown in the `code examples <https://cfinan.gitlab.io/ukbb-tools/examples/dd_orm_usage.html>`_

3. Building a UK Biobank mapping file
-------------------------------------

*Administrators/Users*

If you intend on mapping data between applications, for example, using a common set of genotypes, or some derived variables. You will need a mapping file, this is a file with your set of sample IDs ordered in a defined way such that the sample index position i.e. 1st sample ID, 2nd sample ID...Nth sample ID is the same between projects. The most straight forward way to achieve this is querying the downloaded UK Biobank ``*.sample`` files or ``*.fam`` files and producing a sample definition file (``.sdef`` :ref:`file extension <sdef_format>`), this obviously only applies to genotyped samples.

The genotyped sample files will contain non-standard negative sample IDs. These represent the withdrawn participants. UK Biobank, does not routinely remove their data from the genotype files, due to the relative computational cost of doing so. Instead, adding them as a negative sample identifier will tell most analyses software that the sample should be treated as missing.

The samples definition file can be produced using a command similar to the one below (for ``*.sample`` files:

.. code-block:: console

   ukbb-samples -vv --allow-bad-eid /path/to/latest_withdrawals.txt ~/sample_outputs/sample-mapping-bgen- /path/to/bgen/sample/files/*.sample

Or this for ``*.fam`` files.:

.. code-block:: console

   ukbb-samples -vv --allow-bad-eid "/path/to/latest_withdrawals.txt" ~/sample_outputs/sample-mapping-fam- /path/to/bgen/sample/files/*.fam

In both cases you should substitute the ``"/path/to/latest_withdrawals.txt"`` path to your actual path on your system and the same for the ``~/sample_outputs/sample-mapping-fam-`` and ``/path/to/bgen/sample/files/*.fam`` paths.

4. Setup a configuration file
-----------------------------

*Administrators/Users*

The UKBB-tools package supports a configuration file, that specifies key parameters and files that are used in the package. This is designed to make it easier for users to perform certain tasks such as remapping sample IDs. It allows users to abstract file handing away from command line input. However, the configuration file is optional and users can specify paths on the command line if they wish to.

More information on the configuration file can be found in the :ref:`config file <ukbb_config_background>` section and in the :ref:`script <ukbb_config_script>` documentation.

