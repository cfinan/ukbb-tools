.. _ukbb_tools_api:

Below are the modules that are in the package tools.

``ukbb_tools.common`` module
----------------------------

.. _ukbb_common_api:

.. automodule:: ukbb_tools.common
   :members:
   :undoc-members:
   :show-inheritance:

``ukbb_tools.constants`` module
-------------------------------

.. _ukbb_constants_api:

.. automodule:: ukbb_tools.constants
   :members:
   :undoc-members:
   :show-inheritance:

``ukbb_tools.columns`` module
-----------------------------

.. _ukbb_columns_api:

.. automodule:: ukbb_tools.columns
   :members:
   :undoc-members:
   :show-inheritance:

``ukbb_tools.type_casts`` module
--------------------------------

.. _ukbb_type_casts_api:

.. automodule:: ukbb_tools.type_casts
   :members:
   :undoc-members:
   :show-inheritance:

``ukbb_tools.parsers`` module
-----------------------------

.. _ukbb_parsers_api:

.. automodule:: ukbb_tools.parsers
   :members:
   :undoc-members:
   :show-inheritance:
   :inherited-members:
   :member-order: bysource

``ukbb_tools.samples`` module
-----------------------------

.. _ukbb_samples_api:

.. currentmodule:: ukbb_tools.samples
.. autofunction:: sample_search

``ukbb_tools.remap`` module
---------------------------

.. _ukbb_remap_api:

.. currentmodule:: ukbb_tools.remap
.. autofunction:: remap
