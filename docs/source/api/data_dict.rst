``ukbb_tools.data_dict`` API
============================

Currently, other than the ORM the only real API is a direct call to the build function.

``ukbb_tools.data_dict.build`` module
-------------------------------------

.. _ukbb_dd_build_api:

.. autofunction:: ukbb_tools.data_dict.build.build_data_dict

Example usage
~~~~~~~~~~~~~

Below is an example of using the API to build the data dictionary against a MySQL backend. Note that the database should already exist on the server before the build function is called, see `SQLALchemy utils <https://sqlalchemy-utils.readthedocs.io/en/latest/database_helpers.html#create-database>`_ for creating programmatically.

.. code-block:: python

   from ukbb_tools.data_dict import build
   import sqlalchemy

   # See: https://docs.sqlalchemy.org/en/20/core/connections.html
   connection_url = "mysql+pymysql://user:password@hostname:3306/ukbb_data_dict"

   engine = sqlalchemy.create_engine(connection_url)
   sm = sqlalchemy.orm.sessionmaker(bind=engine)
   session = sm()

   # Now build
   try:
       build.build_data_dict(session, verbose=True)
   finally:
       session.close()

.. _ukbb_dd_orm_api:

.. include:: ../data_dict/data-dict-orm.rst
